package promauto.utils;

import org.junit.Test;

import static org.junit.Assert.*;

public class NumbersTest {
    @Test
    public void asciiBCDToInt() throws Exception {

        assertEquals(123, Integer.parseInt("123") );

        int[] src = new int[] { 0x39, 0x31, 0x32, 0x33, 0x39 }; // "91239"
        assertEquals(123, Numbers.asciiToInt(src, 1,4));

    }

}