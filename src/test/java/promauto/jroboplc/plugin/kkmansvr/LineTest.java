package promauto.jroboplc.plugin.kkmansvr;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import promauto.jroboplc.core.ConfigurationYaml;
import promauto.jroboplc.core.TagTable;
import promauto.jroboplc.core.api.*;
import promauto.jroboplc.core.tags.TagRW;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;
import static promauto.jroboplc.plugin.kkmansvr.Line.*;
import static promauto.utils.CollectionUtils.*;

class SampleDozmanModule extends SampleModule {
    Tag tagCrc32;
    Tag tagIsEmptyCur;
    Tag tagIsEmptyTot;
    Tag tagSetEmptyCur;
    Tag tagSetEmptyTot;
    Tag tagWeightReq;
    Tag tagWeightCur;
    Tag tagWeightTot;
    Tag tagStable;
    Tag tagFinished;
    Tag tagRun;
    Tag tagStatus;
    Tag tagErrorFlag;

    public SampleDozmanModule(String name) {
        super(name);
        tagCrc32      = tagtable.createLong("Crc32", 0);
        tagIsEmptyCur  = tagtable.createBool("IsEmptyCur", false);
        tagIsEmptyTot  = tagtable.createBool("IsEmptyTot", false);
        tagSetEmptyCur = tagtable.createBool("SetEmptyCur", false);
        tagSetEmptyTot = tagtable.createBool("SetEmptyTot", false);
        tagWeightReq   = tagtable.createInt("WeightReq", 0);
        tagWeightCur   = tagtable.createInt("WeightCur", 0);
        tagWeightTot   = tagtable.createInt("WeightTot", 0);
        tagStable      = tagtable.createBool("Stable", false);
        tagFinished    = tagtable.createBool("Finished", false);
        tagRun         = tagtable.createBool("Run", false);
        tagStatus      = tagtable.createInt("Status", 0);
        tagErrorFlag   = tagtable.createInt("SYSTEM.ErrorFlag", 0);

    }
}


class LineTest {

    public static final int COMP_QNT = 3;
    Line line;
    SampleDozmanModule dozman0;
    SampleDozmanModule dozman1;

    DataService service;
    TagRepository repo;

    SampleModuleManager mm;
    TagTable tt;

    Tag tagTaskInstall;
    Tag tagTaskId;
    Tag tagRecipeId;
    Tag tagRecipeName;
    Tag tagCycleReq;
    Tag tagCycleCnt;
    Tag tagDisable;
    Tag tagUnloadStart;
    Tag tagUnloadFinished;
    Tag tagUnloadTimerCnt;
    Tag tagUnloadTimerSet;
    Tag tagRun;
    Tag tagCancel;
    Tag tagCommit;
    Tag tagStatus;
    Tag tagSumWeightReq;
    Tag tagSumWeightFin;
    Tag tagSumWeightCtl;
    Tag tagControlId;
    Tag tagControlName;
    Tag tagControlWeight;
    Tag tagControlStable;

    private Task task;

    static class ComponentTags {
        Tag tagCancel;
        Tag tagWeightCur;
        Tag tagDoserId;
        Tag tagDoserName;
        Tag tagManual;
        Tag tagProdId;
        Tag tagProdName;
        Tag tagWeightReq;
        Tag tagWeightFin;
        Tag tagStatus;
        Tag tagRun;
    }

    ComponentTags[] comps;
    ComponentTags comp0, comp1;



    @BeforeEach
    void setUp() throws SQLException {
        SampleEnvironment env = new SampleEnvironment();
        EnvironmentInst.set(env);
        env.setConfiguration(new ConfigurationYaml(""));

        mm = new SampleModuleManager();
        env.setModuleManager(mm);

        dozman0 = new SampleDozmanModule("dozman0");
        dozman1 = new SampleDozmanModule("dozman1");
        mm.modules.put(dozman0.getName(), dozman0);
        mm.modules.put(dozman1.getName(), dozman1);

        service = mock(DataService.class);
        when(service.syncDoser(anyInt(), eq("dozman0"), anyString(), anyBoolean())).thenReturn(1);
        when(service.syncDoser(anyInt(), eq("dozman1"), anyString(), anyBoolean())).thenReturn(2);
        repo = mock(TagRepository.class);

        line = new Line(service, "");
        line.setRepo(repo);

        Map<String,Object> conf = createMap(
                "lineId", 1,
                "lineName", "sample line",
                "compQnt", COMP_QNT,
                "dosers", createList(
                        createMap(
                                "name", "dozman0",
                                "manual", false,
                                "canControl", true
                        ),
                        createMap(
                                "name", "dozman1",
                                "manual", true,
                                "canControl", false
                        )
                )
        );

        tt = new TagTable();
        line.load(conf, tt);
        line.dosers.forEach(doser -> doser.refgr.disableCrc());

        tagTaskInstall    = tt.get("TaskInstall");
        tagTaskId         = tt.get("TaskId");
        tagRecipeId       = tt.get("RecipeId");
        tagRecipeName     = tt.get("RecipeName");
        tagCycleReq       = tt.get("CycleReq");
        tagCycleCnt       = tt.get("CycleCnt");
        tagDisable        = tt.get("Disable");
        tagUnloadStart    = tt.get("UnloadStart");
        tagUnloadFinished = tt.get("UnloadFinished");
        tagUnloadTimerCnt = tt.get("UnloadTimerCnt");
        tagUnloadTimerSet = tt.get("UnloadTimerSet");
        tagRun            = tt.get("Run");
        tagCancel         = tt.get("Cancel");
        tagCommit         = tt.get("Commit");
        tagStatus         = tt.get("Status");
        tagSumWeightReq   = tt.get("SumWeightReq");
        tagSumWeightFin   = tt.get("SumWeightFin");
        tagSumWeightCtl   = tt.get("SumWeightCtl");
        tagControlId      = tt.get("ControlId");
        tagControlName    = tt.get("ControlName");
        tagControlWeight  = tt.get("ControlWeight");
        tagControlStable  = tt.get("ControlStable");

        comps = new ComponentTags[COMP_QNT];
        for(int i=0; i<COMP_QNT; ++i ) {
           ComponentTags ct = new ComponentTags();
           comps[i] = ct;
           ct.tagCancel      = tt.get("Comp" + i + ".Cancel");
           ct.tagWeightCur   = tt.get("Comp" + i + ".WeightCur");
           ct.tagDoserId     = tt.get("Comp" + i + ".DoserId");
           ct.tagDoserName   = tt.get("Comp" + i + ".DoserName");
           ct.tagManual      = tt.get("Comp" + i + ".Manual");
           ct.tagProdId      = tt.get("Comp" + i + ".ProdId");
           ct.tagProdName    = tt.get("Comp" + i + ".ProdName");
           ct.tagWeightReq   = tt.get("Comp" + i + ".WeightReq");
           ct.tagWeightFin   = tt.get("Comp" + i + ".WeightFin");
           ct.tagStatus      = tt.get("Comp" + i + ".Status");
           ct.tagRun         = tt.get("Comp" + i + ".Run");
        }
        comp0 = comps[0];
        comp1 = comps[1];

    }

    void checkLine(int run, int status, int cycleCnt, int unloadStart, int sumWeightReq, int sumWeightFin, int sumWeightCtl) {
        assertEquals(run,           tagRun.getInt());
        assertEquals(status,        tagStatus.getInt());
        assertEquals(cycleCnt,      tagCycleCnt.getInt());
        assertEquals(unloadStart,   tagUnloadStart.getInt());
        assertEquals(sumWeightReq,  tagSumWeightReq.getInt());
        assertEquals(sumWeightFin,  tagSumWeightFin.getInt());
        assertEquals(sumWeightCtl,  tagSumWeightCtl.getInt());
    }

    void checkComp(int num, int run, int status, int weightReq, int weightFin) {
        assertEquals(run,           comps[num].tagRun.getInt());
        assertEquals(status,        comps[num].tagStatus.getInt());
        assertEquals(weightReq,     comps[num].tagWeightReq.getInt());
        assertEquals(weightFin,     comps[num].tagWeightFin.getInt());
    }

    void checkDozman(SampleDozmanModule d, int run, int setEmptyCur, int setEmptyTot, int weightReq) {
        assertEquals(run,           d.tagRun.getInt());
        assertEquals(setEmptyCur,   d.tagSetEmptyCur.getInt());
        assertEquals(setEmptyTot,   d.tagSetEmptyTot.getInt());
        assertEquals(weightReq,     d.tagWeightReq.getInt());
    }

    private void installTask1() throws SQLException {
        task = new Task();
        task.taskId = 1;
        task.lineId = 1;
        task.controlId = 1;
        task.cycleReq = 1;
        task.recipeId = 33;
        task.recipeName = "recipe33";
        task.products = new ArrayList<>();

        Task.Product tp1 = new Task.Product();
        tp1.doserId = 1;
        tp1.productId = 100;
        tp1.productName = "prod100";
        tp1.weightReq = 1000;

        Task.Product tp2 = new Task.Product();
        tp2.doserId = 2;
        tp2.productId = 200;
        tp2.productName = "prod200";
        tp2.weightReq = 2000;

        task.products.add(tp1);
        task.products.add(tp2);

        when(service.findTask(anyInt())).thenReturn(task);
        tagTaskInstall.setString("1");

        line.init();
        line.execute();
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    @Test
    void execute() throws SQLException {
        line.init();
        line.execute();
        checkLine(0, 0, 0, 0, 0, 0, 0);
        checkComp(0, 0, 0, 0, 0);
        checkComp(1, 0, 0, 0, 0);
        checkComp(2, 0, 0, 0, 0);
        checkDozman(dozman0, 0, 0, 0, 0);
        checkDozman(dozman1, 0, 0, 0, 0);
    }

    @Test
    void test_task_install_link_error() throws SQLException {
        mm.modules.remove("dozman1");
        line.init();
        line.execute();
        assertEquals(TASK_INSTALL_LINK_ERROR, tagTaskInstall.getString());
        checkLine(0, 0, 0, 0, 0, 0, 0);
    }

    @Test
    void test_task_install_not_found() throws SQLException {
        line.init();
        tagTaskInstall.setString("123");
        line.execute();
        assertEquals(TASK_INSTALL_NOT_FOUND, tagTaskInstall.getString());
        checkLine(0, 0, 0, 0, 0, 0, 0);
    }

    @Test
    void test_task_install_wrong_line() throws SQLException {
        Task task = new Task();
        task.lineId = 2;
        when(service.findTask(anyInt())).thenReturn(task);

        line.init();
        tagTaskInstall.setString("123");
        line.execute();
        assertEquals(TASK_INSTALL_WRONG_LINE, tagTaskInstall.getString());
        checkLine(0, 0, 0, 0, 0, 0, 0);
    }

    @Test
    void test_task_install_too_many_products() throws SQLException {
        Task task = new Task();
        task.lineId = 1;
        task.products = new ArrayList<>();
        task.products.add(null);
        task.products.add(null);
        task.products.add(null);
        task.products.add(null);

        when(service.findTask(anyInt())).thenReturn(task);

        line.init();
        tagTaskInstall.setString("123");
        line.execute();
        assertEquals(TASK_INSTALL_TOO_MANY_PRODUCTS, tagTaskInstall.getString());
        checkLine(0, 0, 0, 0, 0, 0, 0);
    }


    @Test
    void test_task_install_ok() throws SQLException {
        installTask1();
        assertEquals(TASK_INSTALL_OK, tagTaskInstall.getString());
        checkLine(0, 0, 0, 0, 3000, 0, 0);
        checkComp(0, 0, 0, 1000, 0);
        checkComp(1, 0, 0, 2000, 0);
        checkComp(2, 0, 0, 0, 0);
        checkDozman(dozman0, 0, 0, 0, 0);
        checkDozman(dozman1, 0, 0, 0, 0);
        assertEquals(1, tagTaskId.getInt());
        assertEquals(1, tagControlId.getInt());
        assertEquals("dozman0", tagControlName.getString());
        assertEquals(33, tagRecipeId.getInt());
        assertEquals("recipe33", tagRecipeName.getString());
        assertEquals(1, tagCycleReq.getInt());
    }


    @Test
    void test_run_without_task_installed() throws SQLException {
        line.init();
        tagRun.setInt(1);
        comp0.tagRun.setInt(1);
        line.execute();
        checkLine(0, 0, 0, 0, 0, 0, 0);
        checkComp(0, 0, 0, 0, 0);
    }

    @Test
    void test_attempt_run_when_disabled() throws SQLException {
        installTask1();

        tagDisable.setInt(1);
        line.execute();
        assertTrue(tagDisable.getBool());

    }

    @Test
    void test_run_line() throws SQLException {
        installTask1();
        tagRun.setInt(1);
        line.execute();
        checkLine(           1, 1, 0, 0, 3000, 0, 0);
        checkComp(0,    0, 0, 1000, 0);
        checkComp(1,    0, 0, 2000, 0);
        checkComp(2,    0, 0, 0, 0);
        checkDozman(dozman0, 0, 0, 1, 0);
        checkDozman(dozman1, 0, 0, 0, 0);
    }


    @Test
    void test_line_preparing() throws SQLException {
        installTask1();
        tagRun.setInt(1);
        line.execute();
        checkLine(           1, 1, 0, 0, 3000, 0, 0);
        checkDozman(dozman0, 0,  0, 1, 0);
        checkDozman(dozman1, 0,  0, 0, 0);

        dozman1.tagIsEmptyTot.setInt(1);
        line.execute();
        checkLine(           1, 1, 0, 0, 3000, 0, 0);

        dozman0.tagIsEmptyTot.setInt(1);
        line.execute();
        checkLine(           1, 2, 0, 0, 3000, 0, 0);
    }

    @Test
    void test_line_preparing_without_control_doser() throws SQLException {
        installTask1();
        tagRun.setInt(1);
        ((TagRW)tagControlId).setReadValInt(0);
        line.execute();
        line.execute();
        checkLine(           1, 2, 0, 0, 3000, 0, 0);
        checkDozman(dozman0, 0,  0, 0, 0);
        checkDozman(dozman1, 0,  0, 0, 0);

    }

    @Test
    void test_attempt_run_component_when_line_not_loading() throws SQLException {
        installTask1();
        tagRun.setInt(1);
        comp0.tagRun.setInt(1);
        line.execute();
        checkComp(0,    0, 0, 1000, 0);
    }

    @Test
    void test_attempt_run_comp_when_other_comp_running_same_doser() throws SQLException {
        installTask1();
        tagRun.setInt(1);
        comp0.tagRun.setInt(1);
        dozman0.tagIsEmptyTot.setInt(1);
        line.execute();
        checkLine(           1, 2, 0, 0, 3000, 0, 0);
        checkComp(0,    1, 1, 1000, 0);
        checkDozman(dozman0, 0, 1, 1, 1000);

        ((TagRW)comp1.tagDoserId).setReadValInt( comp0.tagDoserId.getInt() );
        comp1.tagRun.setInt(1);
        dozman1.tagIsEmptyTot.setInt(1);
        line.execute();
        checkLine(           1, 2, 0, 0, 3000, 0, 0);
        checkComp(1,    0, 0, 2000, 0);
    }


    @Test
    void test_line_loading_and_comp_preparing() throws SQLException {
        installTask1();
        tagRun.setInt(1);
        comp0.tagRun.setInt(1);
        dozman0.tagIsEmptyTot.setInt(1);
        dozman0.tagIsEmptyCur.setInt(1);
        dozman0.tagStatus.setInt(2);
        line.execute();
        checkLine(           1, 2, 0, 0, 3000, 0, 0);
        checkComp(0,    1, 1, 1000, 0);
        checkDozman(dozman0, 0, 1, 1, 1000);

        dozman0.tagIsEmptyCur.setInt(0);
        dozman0.tagStatus.setInt(0x80);
        line.execute();
        checkLine(           1, 2, 0, 0, 3000, 0, 0);
        checkComp(0,    1, 1, 1000, 0);
        checkDozman(dozman0, 0, 1, 1, 1000);

        dozman0.tagIsEmptyCur.setInt(1);
        dozman0.tagStatus.setInt(0x80);
        line.execute();
        checkLine(           1, 2, 0, 0, 3000, 0, 0);
        checkComp(0,    1, 2, 1000, 0);
        checkDozman(dozman0, 1, 1, 1, 1000);

        dozman0.tagSetEmptyCur.setInt(0);
        dozman0.tagSetEmptyTot.setInt(0);
        dozman0.tagStatus.setInt(1);
        line.execute();
        checkLine(           1, 2, 0, 0, 3000, 0, 0);
        checkComp(0,    1, 2, 1000, 0);
        checkDozman(dozman0, 1, 0, 0, 1000);

        checkComp(1,    0, 0, 2000, 0);
        checkDozman(dozman1, 0, 0, 0, 0);
    }


    @Test
    void test_line_loading_and_comp_loading() throws SQLException {
        installTask1();
        tagRun.setInt(1);
        comp0.tagRun.setInt(1);
        dozman0.tagIsEmptyTot.setInt(1);
        dozman0.tagIsEmptyCur.setInt(1);
        dozman0.tagStatus.setInt(0);
        line.execute();

        dozman0.tagWeightCur.setInt(1111);
        dozman0.tagFinished.setInt(0);
        line.execute();
        checkLine(           1, 2, 0, 0, 3000, 0, 0);
        checkComp(0,    1, 2, 1000, 0);
        checkDozman(dozman0, 1, 1, 1, 1000);

        dozman0.tagFinished.setInt(1);
        dozman0.tagStable.setInt(1);
        line.execute();
        checkLine(           1, 2, 0, 0, 3000, 1111, 0);
        checkComp(0,    0, 3, 1000, 1111);
        checkDozman(dozman0, 0, 1, 1, 1000);
    }


    @Test
    void test_line_loading_and_comp_loading_and_doser_not_connected() throws SQLException {
        installTask1();
        tagRun.setInt(1);
        comp0.tagRun.setInt(1);
        dozman0.tagIsEmptyTot.setInt(1);
        dozman0.tagIsEmptyCur.setInt(1);
        dozman0.tagStatus.setInt(0);
        line.execute();

        dozman0.tagWeightCur.setInt(1111);
        dozman0.tagFinished.setInt(1);
        dozman0.tagErrorFlag.setInt(1);
        line.execute();
        checkLine(           1, 2, 0, 0, 3000, 0, 0);
        checkComp(0,    1, 2, 1000, 0);
        checkDozman(dozman0, 1, 1, 1, 1000);
    }

    @Test
    void test_line_loading_and_comp_finished_run_again() throws SQLException {
        installTask1();
        tagRun.setInt(1);
        comp0.tagRun.setInt(1);
        dozman0.tagIsEmptyTot.setInt(1);
        dozman0.tagIsEmptyCur.setInt(1);
        dozman0.tagStatus.setInt(0);
        dozman0.tagWeightCur.setInt(1111);
        dozman0.tagFinished.setInt(1);
        dozman0.tagStable.setInt(1);
        line.execute();
        checkLine(           1, 2, 0, 0, 3000, 1111, 0);
        checkComp(0,    0, 3, 1000, 1111);
        checkDozman(dozman0, 0, 1, 1, 1000);

        comp0.tagRun.setInt(1);
        line.execute();
        checkComp(0,    0, 3, 1000, 1111);
    }


    @Test
    void test_line_loading_and_comp_loading_cancel_comp() throws SQLException {
        installTask1();
        tagRun.setInt(1);
        comp0.tagRun.setInt(1);
        dozman0.tagIsEmptyTot.setInt(1);
        dozman0.tagIsEmptyCur.setInt(1);
        dozman0.tagStatus.setInt(0);
        line.execute();
        checkLine(           1, 2, 0, 0, 3000, 0, 0);
        checkComp(0,    1, 2, 1000, 0);
        checkDozman(dozman0, 1, 1, 1, 1000);

        comp0.tagCancel.setInt(1);
        line.execute();
        checkComp(0,    0, 0, 1000, 0); // weightFin is not affected by cancel
    }


    @Test
    void test_line_loading_and_comp_finished_cancel_comp() throws SQLException {
        installTask1();
        tagRun.setInt(1);
        comp0.tagRun.setInt(1);
        dozman0.tagIsEmptyTot.setInt(1);
        dozman0.tagIsEmptyCur.setInt(1);
        dozman0.tagStatus.setInt(0);
        dozman0.tagWeightCur.setInt(1111);
        dozman0.tagFinished.setInt(1);
        dozman0.tagStable.setInt(1);
        line.execute();
        checkLine(           1, 2, 0, 0, 3000, 1111, 0);
        checkComp(0,    0, 3, 1000, 1111);
        checkDozman(dozman0, 0, 1, 1, 1000);

        comp0.tagCancel.setInt(1);
        line.execute();
        checkComp(0,    0, 0, 1000, 1111); // weightFin is not affected by cancel
    }


    @Test
    void test_line_loading_and_two_comp_finished() throws SQLException {
        installTask1();
        tagRun.setInt(1);
        comp0.tagRun.setInt(1);
        dozman0.tagIsEmptyTot.setInt(1);
        dozman0.tagIsEmptyCur.setInt(1);
        dozman0.tagStatus.setInt(0);
        dozman0.tagWeightCur.setInt(1111);
        dozman0.tagFinished.setInt(1);
        dozman0.tagStable.setInt(1);
        line.execute();
        checkLine(           1, 2, 0, 0, 3000, 1111, 0);
        checkComp(0,    0, 3, 1000, 1111);
        checkDozman(dozman0, 0, 1, 1, 1000);

        comp1.tagRun.setInt(1);
        dozman1.tagIsEmptyTot.setInt(1);
        dozman1.tagIsEmptyCur.setInt(1);
        dozman1.tagStatus.setInt(0);
        dozman1.tagWeightCur.setInt(2222);
        dozman1.tagFinished.setInt(1);
        dozman1.tagStable.setInt(1);
        line.execute();
        checkLine(           1, 2, 0, 0, 3000, 3333, 0);
        checkComp(1,    0, 3, 2000, 2222);
        checkDozman(dozman1, 0, 1, 0, 2000);
    }


    @Test
    void test_line_committing() throws SQLException {
        installTask1();
        tagRun.setInt(1);

        comp0.tagRun.setInt(1);
        dozman0.tagIsEmptyTot.setInt(1);
        dozman0.tagIsEmptyCur.setInt(1);
        dozman0.tagStatus.setInt(0);
        dozman0.tagWeightCur.setInt(1111);
        dozman0.tagWeightTot.setInt(4444);
        dozman0.tagFinished.setInt(1);
        dozman0.tagStable.setInt(1);

        comp1.tagRun.setInt(1);
        dozman1.tagIsEmptyTot.setInt(1);
        dozman1.tagIsEmptyCur.setInt(1);
        dozman1.tagStatus.setInt(0);
        dozman1.tagWeightCur.setInt(2222);
        dozman1.tagFinished.setInt(1);
        dozman1.tagStable.setInt(1);
        line.execute();
        checkLine(           1, 2, 0, 0, 3000, 3333, 0);
        line.execute();
        checkLine(           1, 3, 0, 0, 3000, 3333, 0);

        // attemp to commit without control stable
        dozman0.tagStable.setInt(0);
        tagCommit.setInt(1);
        line.execute();
        checkLine(           1, 3, 0, 0, 3000, 3333, 0);

        // commit will success
        ArgumentCaptor<Cycle> cycleCapture = ArgumentCaptor.forClass(Cycle.class);
        doNothing().when(service).saveCycle(cycleCapture.capture());

        dozman0.tagStable.setInt(1);
        tagCommit.setInt(1);
        line.execute();
        checkLine(           1, 4, 1, 1, 3000, 3333, 4444);
        checkComp(0,    0, 3, 1000, 1111);
        checkComp(1,    0, 3, 2000, 2222);

        Cycle cycle = cycleCapture.getValue();
        assertEquals(1, cycle.taskId);
        assertEquals(1, cycle.cycleCnt);
        assertEquals(3000, cycle.weightReq);
        assertEquals(3333, cycle.weightFin);
        assertEquals(4444, cycle.weightCtl);
        assertEquals(2, cycle.products.size());
        assertEquals(100, cycle.products.get(0).productId);
        assertEquals(1000, cycle.products.get(0).weightReq);
        assertEquals(1111, cycle.products.get(0).weightFin);
        assertEquals(200, cycle.products.get(1).productId);
        assertEquals(2000, cycle.products.get(1).weightReq);
        assertEquals(2222, cycle.products.get(1).weightFin);
    }


    @Test
    void test_line_committing_without_control_doser() throws SQLException {
        installTask1();
        tagRun.setInt(1);
        ((TagRW) tagStatus).setReadValInt(3);
        ((TagRW) tagRun).setReadValInt(1);
        ((TagRW) tagControlId).setReadValInt(0);

        ((TagRW)comp0.tagWeightFin).setReadValInt(1111);
        ((TagRW)comp1.tagWeightFin).setReadValInt(2222);
        dozman0.tagWeightTot.setInt(4444);
        dozman1.tagWeightTot.setInt(5555);

        line.execute();
        checkLine(           1, 3, 0, 0, 3000, 3333, 0);

        tagCommit.setInt(1);
        line.execute();
        checkLine(           1, 4, 1, 1, 3000, 3333, 0);
    }

    @Test
    void test_line_unloading() throws SQLException {
        installTask1();
        tagRun.setInt(1);

        comp0.tagRun.setInt(1);
        dozman0.tagIsEmptyTot.setInt(1);
        dozman0.tagIsEmptyCur.setInt(1);
        dozman0.tagStatus.setInt(0);
        dozman0.tagWeightCur.setInt(1111);
        dozman0.tagWeightTot.setInt(4444);
        dozman0.tagFinished.setInt(1);
        dozman0.tagStable.setInt(1);

        comp1.tagRun.setInt(1);
        dozman1.tagIsEmptyTot.setInt(1);
        dozman1.tagIsEmptyCur.setInt(1);
        dozman1.tagStatus.setInt(0);
        dozman1.tagWeightCur.setInt(2222);
        dozman1.tagFinished.setInt(1);
        dozman1.tagStable.setInt(1);

        line.execute(); // loading
        line.execute(); // committing
        dozman0.tagStable.setInt(1);
        tagCommit.setInt(1);
        line.execute(); // unloading

        tagUnloadFinished.setInt(1);
        line.execute();
        checkLine(           0, 0, 1, 0, 3000, 0, 0);
        checkComp(0,    0, 0, 1000, 0);
        checkComp(1,    0, 0, 2000, 0);
    }


    @Test
    void test_line_unloading_with_timer() throws SQLException {
        installTask1();
        tagRun.setInt(1);

        ((TagRW)tagStatus).setReadValInt(4);
        ((TagRW)tagRun).setReadValInt(1);
        line.execute();
        checkLine(           1, 4, 0, 1, 3000, 0, 0);

        tagUnloadTimerSet.setInt(3);
        tagUnloadFinished.setInt(1);
        line.execute();
        checkLine(           1, 4, 0, 1, 3000, 0, 0);
        line.execute();
        checkLine(           1, 4, 0, 1, 3000, 0, 0);
        line.execute();
        checkLine(           1, 4, 0, 1, 3000, 0, 0);
        line.execute();
        checkLine(           0, 0, 0, 0, 3000, 0, 0);
    }


    @Test
    void test_line_loading_cancel() throws SQLException {
        installTask1();
        tagRun.setInt(1);

        ((TagRW) tagStatus).setReadValInt(3);
        ((TagRW) tagRun).setReadValInt(1);
        line.execute();
        checkLine(1, 3, 0, 0, 3000, 0, 0);

        tagCancel.setInt(1);
        line.execute();
        checkLine(           0, 0, 0, 0, 3000, 0, 0);
    }




}