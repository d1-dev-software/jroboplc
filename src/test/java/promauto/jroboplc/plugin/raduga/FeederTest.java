package promauto.jroboplc.plugin.raduga;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import promauto.jroboplc.core.TagTable;
import promauto.jroboplc.core.api.EnvironmentInst;
import promauto.jroboplc.core.api.SampleEnvironment;
import promauto.jroboplc.core.tags.TagRWInt;

import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.*;

class FeederTest {

    Feeder feeder;
    DoserDummy doser;
    LineDummy line;
    TaskRepo tr;
    Context ctx;
    DataServiceDummy svc;

    Task task1;

    @BeforeEach
    void setUp() throws SQLException {
        SampleEnvironment env = new SampleEnvironment();
        EnvironmentInst.set(env);

        svc = new DataServiceDummy();
        svc.task = new Task();
        svc.task.id = 1;
        svc.task.presented = true;

        ctx = new Context();
        ctx.setEnvironment(env);
        ctx.setDataService(svc);
        ctx.tagtable = new TagTable();
//        ctx.setTaskrepo(new TaskRepo(ctx));
//        ctx.taskrepo.get(1);


        line = new LineDummy();
        doser = new DoserDummy(ctx, line, 1);
        feeder = new Feeder(ctx, doser, 1, "bunker1");

        doser.tagPassQty = new TagRWInt("", 0, 0);
        doser.tagPassCnt = new TagRWInt("", 0, 0);

        feeder.load();
        feeder.createRefs("mod1");
        DoserDummy.changeTagTypes(doser);

    }

    @Test
    void executeTest() throws SQLException {
        feeder.reset();
        check(0, 0, 0, -1, 0);

        // 1
        set(0, 0, 1000, 25);
        feeder.execute();
        check(0, 0, 1000, -1, 0);

        // 2
        doser.tagPassQty.setReadValInt(1);
        doser.tagPassCnt.setReadValInt(0);
        set(500, 500, 1000, 25);
        feeder.execute();
        check(500, 0, 1000, -1, 500);

        // 3
        feeder.beginCycle();
        feeder.execute();
        check(500, 0, 1000, 1000, 500);

        // 4
        set(500, 500, 1000, 25);
        feeder.execute();
        check(500, 0, 1000, 1000, 500);
        assertTrue(feeder.isReqWeightDiffer());

        set(500, 500, 1000, 500);
        assertFalse(feeder.isReqWeightDiffer());

        // 5
        svc.taskExecLastSumWeightEnd = 900L;
        line.taskId = 1;
        line.cycleId = 332211;
        set(500, 500, 1505, 25);
        feeder.execute();
        check(500, 505, 1505, 1000, 500);

        assertEquals(2, svc.taskExecs.size());
        Task.Exec te = svc.taskExecs.get(0);
        assertEquals(0, te.taskId);
        assertEquals(0, te.cycleId);
        assertEquals(100, te.weight);
        assertEquals(900, te.sumWeightBeg);
        assertEquals(1000, te.sumWeightEnd);

        te = svc.taskExecs.get(1);
        assertEquals(1, te.taskId);
        assertEquals(332211, te.cycleId);
        assertEquals(505, te.weight);
        assertEquals(1000, te.sumWeightBeg);
        assertEquals(1505, te.sumWeightEnd);

    }

    @Test
    void calcSetWeightPassTest() throws SQLException {
        feeder.reset();
        doser.tagPassQty.setReadValInt(7);
        doser.tagPassCnt.setReadValInt(0);
        set(501, 501, 1000, 25);
        feeder.execute();
        check(72, 0, 1000, -1, 72);

        doser.tagPassCnt.setReadValInt(1);
        feeder.execute();
        check(72, 0, 1000, -1, 72);

        doser.tagPassCnt.setReadValInt(2);
        feeder.execute();
        check(72, 0, 1000, -1, 72);

        doser.tagPassCnt.setReadValInt(3);
        feeder.execute();
        check(72, 0, 1000, -1, 72);

        doser.tagPassCnt.setReadValInt(4);
        feeder.execute();
        check(72, 0, 1000, -1, 72);

        doser.tagPassCnt.setReadValInt(5);
        feeder.execute();
        check(72, 0, 1000, -1, 72);

        doser.tagPassCnt.setReadValInt(6);
        feeder.execute();
        check(69, 0, 1000, -1, 69);
    }



    private void set(
            long rcpWeight,
            long setWeight,
            long sumWeightRef,
            long reqWeightRef

            ) {
        feeder.tagRcpWeight.setReadValLong(rcpWeight);
        feeder.tagSetWeight.setReadValLong(setWeight);
        feeder.refSumWeight.getValue().setLong(sumWeightRef);
        feeder.refReqWeight.getValue().setLong(reqWeightRef);
    }


    private void check(
            int setWeightPass,
            long exeWeight,
            long sumWeight,
            long sumWeightBeg,
            long setWeightRef
    ) {
        assertEquals(setWeightPass, feeder.tagSetWeightPass.getInt());
        assertEquals(exeWeight, feeder.tagExeWeight.getLong());
        assertEquals(sumWeight, feeder.tagSumWeight.getLong());
        assertEquals(sumWeightBeg, feeder.tagSumWeightBeg.getLong());
        assertEquals(setWeightRef, feeder.refSetWeight.getTag().getLong());

    }
}