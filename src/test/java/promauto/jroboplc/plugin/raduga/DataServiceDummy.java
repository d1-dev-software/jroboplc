package promauto.jroboplc.plugin.raduga;

import promauto.jroboplc.core.api.Database;

import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class DataServiceDummy implements DataService {

    public static class TaskCtl {
        int taskId;
        int cycleId;
        LocalDateTime dt;
        long weight;

        public TaskCtl(int taskId, int cycleId, LocalDateTime dt, long weight) {
            this.taskId = taskId;
            this.cycleId = cycleId;
            this.dt = dt;
            this.weight = weight;
        }
    }

    public Task task;
    public List<Task.Exec> taskExecs = new ArrayList<>();
    public List<TaskCtl> taskCtls = new ArrayList<>();

    public Long taskExecLastSumWeightEnd;
    private int doserId;
    private int feederId;
    private int bunkerId;
    private int lineId;
    private int paramId;
    private int cycleId;

    @Override
    public void setDb(Database db) {

    }

    @Override
    public Database getDb() {
        return null;
    }

    @Override
    public Statement createStatement() throws SQLException {
        return null;
    }

    @Override
    public void commit() throws SQLException {

    }

    @Override
    public void rollback() {

    }

    @Override
    public void startSync() {

    }

    @Override
    public void finishSync(boolean deleteOtherStuff) throws SQLException {

    }

    @Override
    public int syncLine(String name, String descr, boolean hidden) throws SQLException {
        return ++lineId;
    }

    @Override
    public int syncDoser(String name) throws SQLException {
        return ++doserId;
    }

    @Override
    public int syncDoser(String name, int prodtype) throws SQLException {
        return 0;
    }

    @Override
    public int syncFeeder(int doserId, int feederNum) throws SQLException {
        return ++feederId;
    }

    @Override
    public int syncBunker(String name) throws SQLException {
        return ++bunkerId;
    }

    @Override
    public int syncParam(String name, String descr, String valtype) throws SQLException {
        return ++paramId;
    }

    @Override
    public int syncDispenser(int lineId, String name, String descr) throws SQLException {
        return 0;
    }

    @Override
    public void syncLineDosers(int lineId, List<Integer> doserIds) throws SQLException {

    }

//    @Override
//    public void syncLineBunkers(int lineId, List<String> bunkers) throws SQLException {
//
//    }

    @Override
    public void syncLineParams(int lineId, List<Integer> paramIds) throws SQLException {

    }

    @Override
    public int syncFeederBunker(int feederId, String bunkerName) throws SQLException {
        return ++bunkerId;
    }

    @Override
    public int generateCycleId() throws SQLException {
        return ++cycleId;
    }

    @Override
    public Task getTask(int taskId) throws SQLException {
        return task;
    }

    @Override
    public void saveTaskExec(Task.Exec te) throws SQLException {

        Task.Exec te1 = new Task.Exec();
        te1.taskId = te.taskId;
        te1.feederId = te.feederId;
        te1.cycleId = te.cycleId;
        te1.inputProductId = te.inputProductId;
        te1.inputBunkerId = te.inputBunkerId;
        te1.dt = te.dt;
        te1.weight = te.weight;
        te1.sumWeightBeg = te.sumWeightBeg;
        te1.sumWeightEnd = te.sumWeightEnd;

        taskExecs.add(te1);
    }

    @Override
    public void saveTaskExecDsp(Task.ExecDsp te) throws SQLException {

    }


    @Override
    public Optional<Long> getTaskExecLastSumWeightEnd(int id) throws SQLException {
        if( taskExecLastSumWeightEnd == null )
            return Optional.empty();

        return Optional.of(taskExecLastSumWeightEnd);
    }

    @Override
    public void saveTaskCtl(int lineId, int taskId, int cycleId, LocalDateTime dt, long weight) throws SQLException {
        taskCtls.add(new TaskCtl(taskId, cycleId, dt, weight));
    }

//    @Override
//    public void saveTaskDtExec(int taskId, LocalDateTime dt) throws SQLException {
//
//    }
//
//    @Override
//    public void saveTaskDtFinish(int taskId, LocalDateTime dt) throws SQLException {
//
//    }

//    @Override
//    public void saveTaskSetWeightTot(int taskId, long totalSetWeight) throws SQLException {
//
//    }

    @Override
    public String getLastSql() {
        return "";
    }

    @Override
    public void setArcSize(int shortRecs, int shortDays, int longRecs, int longDays) {

    }

    @Override
    public String getProductName(int productId) throws SQLException {
        return "";
    }

//    @Override
//    public Optional<Shift> getShift(int shiftId) throws SQLException {
//        return Optional.empty();
//    }
}
