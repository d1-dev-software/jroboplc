package promauto.jroboplc.plugin.raduga;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import promauto.jroboplc.core.ConfigurationYaml;
import promauto.jroboplc.core.TagTable;
import promauto.jroboplc.core.api.EnvironmentInst;
import promauto.jroboplc.core.api.SampleEnvironment;
import promauto.jroboplc.core.tags.RefGroupDummy;
import promauto.jroboplc.core.tags.RefItemDummy;
import promauto.jroboplc.core.tags.TagInt;
import promauto.jroboplc.core.tags.TagRWInt;
import promauto.jroboplc.plugin.peripherial.PaGeliosDozkkmcModule;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static promauto.jroboplc.plugin.raduga.LineSimple.*;

class LineSimpleTest {

    LineSimple line;
    Context ctx;
    DataServiceDummy svc;

    Doser dos1, dos2, dos3;
    RefGroupDummy refgr1, refgr2, refgr3;

    @BeforeEach
    void setUp() {
        SampleEnvironment env = new SampleEnvironment();
        EnvironmentInst.set(env);
        env.setConfiguration(new ConfigurationYaml(""));

        svc = new DataServiceDummy();

        ctx = new Context();
        ctx.setEnvironment(env);
        ctx.setDataService(svc);
        ctx.tagtable = new TagTable();
//        ctx.setTaskrepo(new TaskRepo(ctx));

        Map<Object,Object> conf = new HashMap<>();
        conf.put("num", 1);
        conf.put("name", "line1");
        conf.put("descr", "line one");
        conf.put("type", "simple");
        conf.put("hidden", false);
        conf.put("autostart", false);

        Map<Object,Object> confDos1 = new HashMap<>();
        Map<Object,Object> confDos2 = new HashMap<>();
        Map<Object,Object> confDos3 = new HashMap<>();
        conf.put("dosers", new ArrayList<>(Arrays.asList(confDos1, confDos2, confDos3)));

        Map<Object,Object> confPar1 = new HashMap<>();
        Map<Object,Object> confPar2 = new HashMap<>();
        conf.put("params", new ArrayList<>(Arrays.asList(confPar1, confPar2)));

        confDos1.put("name", "dos1");
        confDos1.put("module", "mod1");
        confDos1.put("srcBunkers", new ArrayList<>(Arrays.asList("bunk11", "bunk12", "bunk13")));

        confDos2.put("name", "dos2");
        confDos2.put("module", "mod2");
        confDos2.put("srcBunkers", new ArrayList<>(Arrays.asList("bunk21")));

        confDos3.put("name", "dos3");
        confDos3.put("module", "mod3");
        confDos3.put("srcBunkers", new ArrayList<>(Arrays.asList("bunk31")));

        confPar1.put("name", "par1");
        confPar1.put("descr", "par one");
        confPar1.put("tagname", "tagname_par1");
        confPar1.put("valtype", "INT");

        confPar2.put("name", "par2");
        confPar2.put("descr", "par two");
        confPar2.put("tagname", "tagname_par2");
        confPar2.put("valtype", "DOUBLE");

        line = new LineSimple(ctx);
        line.load(conf);
        line.dosers.forEach(DoserDummy::changeTagTypes);

        dos1 = line.dosers.get(0);
        dos2 = line.dosers.get(1);
        dos3 = line.dosers.get(2);

        refgr1 = (RefGroupDummy) line.dosers.get(0).refgr;
        refgr2 = (RefGroupDummy) line.dosers.get(1).refgr;
        refgr3 = (RefGroupDummy) line.dosers.get(2).refgr;

        ctx.lines.add(line);
    }

    @Test
    void initTest() throws SQLException {
        assertTrue(line.prepare());
        assertTrue(line.init());
    }

    @Test
    void installTaskErrorsTest() throws SQLException {
        // 0 - no task
        check(STATE_NONE,0,0,0,0,0,0);
        line.execute();
        assertEquals(0, line.tagTaskId.getInt());

        // 1 - install task intended for another line
        svc.task = getTask();
        svc.task.id = 1;
        svc.task.lineName = "line2";
//        line.tagTaskId.setInt(1);
        line.tagTaskInstall.setInt(1);
        line.execute();
        assertEquals(0, line.tagTaskId.getInt());
        assertEquals(ERROR_TASK_HAS_WRONG_LINE, line.tagError.getString());
        check(STATE_NONE,0,0,0,0,0,0);

//        // 2 - there is no point in changing the value of tagTaskId until a reset is done
//        line.tagTaskInstall.setInt(2);
//        line.execute();
//        assertEquals(1, line.tagTaskId.getInt());
//        check(STATE_ERROR,0,0,0,0,0,0);
//
//        // 3 - reset
//        line.tagReset.setInt(1);
//        line.execute();
//        assertEquals(0, line.tagReset.getInt());
//        assertEquals(0, line.tagTaskId.getInt());
//        check(STATE_NONE,0,0,0,0,0,0);

        // 4 - install task with bad status
        svc.task = getTask();
        svc.task.id = 2;
        svc.task.status = Task.STATUS_EDIT;
        line.tagTaskInstall.setInt(2);
        line.execute();
        assertEquals(ERROR_TASK_HAS_WRONG_STATUS, line.tagError.getString());
        check(STATE_NONE,0,0,0,0,0,0);

//        // 5 - reset
//        line.tagReset.setInt(1);
//        line.execute();
//        check(STATE_NONE,0,0,0,0,0,0);

        // 6 - install task not found
        svc.task = getTask();
        svc.task.id = 3;
        svc.task.presented = false;
        line.tagTaskInstall.setInt(3);
        line.execute();
        assertEquals(ERROR_TASK_NOT_FOUND, line.tagError.getString());
        check(STATE_NONE,0,0,0,0,0,0);

//        // 7 - reset
//        line.tagReset.setInt(1);
//        line.execute();
//        check(STATE_NONE,0,0,0,0,0,0);

        // 8 - install task not found
        svc.task = getTask();
        svc.task.id = 4;
        // dos2 has only one feeder, but in task there are two
        svc.task.products.add(new Task.Product(4, "prod4", "dos2", 2, 400));
        line.tagTaskInstall.setInt(4);
        line.execute();
        assertEquals(ERROR_TASK_HAS_WRONG_BUNKER, line.tagError.getString());
        check(STATE_NONE,0,0,0,0,0,0);

        // 8 - reset
        line.tagReset.setInt(1);
        line.execute();
        check(STATE_NONE,0,0,0,0,0,0);

        // 9 - install task not found (once more time)
        svc.task = getTask();
        svc.task.id = 5;
        // line doesn't have dos3
        svc.task.products.add(new Task.Product(4, "prod4", "dos4", 1, 400));
        line.tagTaskInstall.setInt(5);
        line.execute();
        assertEquals(ERROR_TASK_HAS_WRONG_BUNKER, line.tagError.getString());
        check(STATE_NONE,0,0,0,0,0,0);

        // 8 - reset
        line.tagReset.setInt(1);
        line.execute();
        check(STATE_NONE,0,0,0,0,0,0);
    }


    @Test
    void cancelAndResetTest() throws SQLException {
        // 0 - no task
        check(STATE_NONE, 0, 0, 0, 0, 0, 0);
        line.execute();
        assertEquals(0, line.tagTaskId.getInt());

        // 1 - install task
        svc.task = getTask();
        svc.task.id = 1;
        assertEquals(Task.STATUS_READY, svc.task.status);
        line.tagTaskInstall.setInt(1);
        line.execute();
        assertEquals(1, line.tagTaskId.getInt());
//        assertEquals(Task.STATUS_INUSE, svc.task.status);
        check(STATE_IDLE, 0, 600, 0, 1000, 0, 0);

        // 2 - idle
        line.execute();
        check(STATE_IDLE, 0, 600, 0, 1000, 0, 0);

        // 3 - cancel
        line.tagCancel.setInt(1);
        line.execute();
        assertEquals(Task.STATUS_READY, svc.task.status);
        check(STATE_NONE, 0, 0, 0, 0, 0, 0);

        // 4 - install again
        line.tagTaskInstall.setInt(1);
        line.execute();
//        assertEquals(Task.STATUS_INUSE, svc.task.status);
        check(STATE_IDLE, 0, 600, 0, 1000, 0, 0);

        // 5 - reset
        line.tagReset.setInt(1);
        line.execute();
//        assertEquals(Task.STATUS_INUSE, svc.task.status);
        check(STATE_NONE, 0, 0, 0, 0, 0, 0);
    }


    @Test
    void setWeightTest() throws SQLException {
        // 1 - install task
        svc.task = getTask();
//        line.tagTaskId.setInt(1);
        line.tagTaskInstall.setInt(1);
        line.execute();
        check(STATE_IDLE, 0, 600, 0, 1000, 0, 0);

        // 2 - setweight
        line.tagSetWeight.setLong(700);
        line.execute();
        assertEquals(700, line.dosers.stream().mapToLong(Doser::getSetWeight).sum());
        check(STATE_IDLE, 0, 600, 0, 1000, 0, 0);
    }

    @Test
    void executeTest() throws SQLException {
        setDoserSumWeights(110000, 120000, 130000, 210000, 310000);
        line.execute();

        // 1 - install task
        svc.task = getTask();
        line.tagTaskInstall.setInt(1);
        line.execute();
        check(STATE_IDLE, 0, 600, 0, 1000, 0, 0);
        checkDoser(dos1, 1, 300, 300, 0, 1, 0, 0, 0, 0, 0, 0);
        checkDoser(dos2, 1, 300, 300, 0, 1, 0, 0, 0, 0, 0, 0);
        checkFeeder(dos1, 0, 100, 100, 100, 0, 110000, -1, 0);
        checkFeeder(dos1, 1, 200, 200, 200, 0, 120000, -1, 0);
        checkFeeder(dos2, 0, 300, 300, 300, 0, 210000, -1, 0);

        // 2 - start, one doser in alarm
        setDoserStatus(dos1, PaGeliosDozkkmcModule.STATUS_ALARM);
        line.tagStart.setInt(1);
        line.execute();
        check(STATE_PREPARING, 1, 600, 0, 1000, 0, 0);
        checkDoser(dos1, 1, 300, 300, 0, 1, 0, 0, 0, 0, 0, 0);
        checkDoser(dos2, 1, 300, 300, 0, 1, 0, 0, 1, 0, 0, 0);
        checkFeeder(dos1, 0, 100, 100, 100, 0, 110000, 110000, 100);
        checkFeeder(dos1, 1, 200, 200, 200, 0, 120000, 120000, 200);
        checkFeeder(dos2, 0, 300, 300, 300, 0, 210000, 210000, 300);

        // 3 - preparing, one doser not connected
        setDoserStatus(dos1, PaGeliosDozkkmcModule.STATUS_IDLE);
        dos1.refErrorFlag.getValue().setInt(1);
        line.execute();
        checkDoser(dos1, 0, 300, 300, 0, 1, 0, 0, 0, 0, 0, 0);
        checkDoser(dos2, 1, 300, 300, 0, 1, 0, 0, 1, 0, 0, 0);

        // 4 - preparing, all dosers ok
        dos1.refSendTask.getValue().setInt(111);
        dos2.refSendTask.getValue().setInt(222);
        dos1.refErrorFlag.getValue().setInt(0);
        line.execute();
        checkDoser(dos1, 1, 300, 300, 0, 1, 0, 0, 1, 0, 0, 0);
        checkDoser(dos2, 1, 300, 300, 0, 1, 0, 0, 1, 0, 0, 0);

        // 5 - preparing, one doser ready, but one is not
        setDoserStatus(dos1, PaGeliosDozkkmcModule.STATUS_TASK_READY);
        line.execute();
        check(STATE_PREPARING, 1, 600, 0, 1000, 0, 0);
        checkDoser(dos1, 1, 300, 300, 0, 1, 0, 0, 1, 0, 0, 0);
        checkDoser(dos2, 1, 300, 300, 0, 1, 0, 0, 1, 0, 0, 0);

        // 6 - send task, only one accepted the task
        ((RefItemDummy)dos1.refSendTask).tag = new TagRWInt("", 0, 0);
        setDoserStatus(dos2, PaGeliosDozkkmcModule.STATUS_TASK_READY);
        line.execute();
        check(STATE_SETTING, 1, 600, 0, 1000, 0, 0);
        checkDoser(dos1, 1, 300, 300, 0, 1, 0, 0, 1, 0, 0, 0);
        checkDoser(dos2, 1, 300, 300, 0, 1, 0, 1, 1, 0, 0, 0);

        // 7 - send task completed
        ((RefItemDummy)dos1.refSendTask).tag = new TagInt("", 1, 0);
        line.tagEnableLoad.setOff();
        line.execute();
        check(STATE_CAN_LOAD, 1, 600, 0, 1000, 0, 0);
        checkDoser(dos1, 1, 300, 300, 0, 1, 0, 1, 1, 0, 0, 0);
        checkDoser(dos2, 1, 300, 300, 0, 1, 0, 1, 1, 0, 0, 0);

        // 8 - waiting for load ready and load enabled
        clearCmds();
        line.execute();
        check(STATE_CAN_LOAD, 1, 600, 0, 1000, 0, 0);
        checkDoser(dos1, 1, 300, 300, 0, 1, 0, 0, 0, 0, 0, 0);
        checkDoser(dos2, 1, 300, 300, 0, 1, 0, 0, 0, 0, 0, 0);

        // 9 - waiting for load ready ...
        line.tagEnableLoad.setInt(1);
        clearCmds();
        line.execute();
        check(STATE_LOADING, 1, 600, 0, 1000, 0, 0);
        checkDoser(dos1, 1, 300, 300, 0, 1, 0, 0, 0, 1, 1, 0);
        checkDoser(dos2, 1, 300, 300, 0, 1, 0, 0, 0, 1, 1, 0);

        // 10 - waiting for ... load enabled
        line.tagEnableLoad.setInt(0);
        setDoserStatus(dos1, PaGeliosDozkkmcModule.STATUS_LOAD_READY);
        setDoserStatus(dos2, PaGeliosDozkkmcModule.STATUS_LOAD_READY);
        clearCmds();
        line.execute();
        check(STATE_LOADING, 1, 600, 0, 1000, 0, 0);
        checkDoser(dos1, 1, 300, 300, 0, 1, 0, 0, 0, 1, 1, 0);
        checkDoser(dos2, 1, 300, 300, 0, 1, 0, 0, 0, 1, 1, 0);

        // 11 - now sending cmd start task and load and waiting for unload ready
        line.tagEnableLoad.setInt(1);
        clearCmds();
        line.execute();
        check(STATE_LOADING, 1, 600, 0, 1000, 0, 0);
        checkDoser(dos1, 1, 300, 300, 0, 1, 0, 0, 0, 1, 1, 0);
        checkDoser(dos2, 1, 300, 300, 0, 1, 0, 0, 0, 1, 1, 0);

        // 12 - disabling load cmds
        line.tagDisable.setInt(1);
        clearCmds();
        line.execute();
        check(STATE_LOADING, 1, 600, 0, 1000, 0, 0);
        checkDoser(dos1, 1, 300, 300, 0, 1, 0, 0, 0, 1, 1, 0);
        checkDoser(dos2, 1, 300, 300, 0, 1, 0, 0, 0, 1, 1, 0);

        // 13 - one is unload ready, waiting for another one
        line.tagDisable.setInt(0);
        setDoserStatus(dos1, PaGeliosDozkkmcModule.STATUS_UNLOAD_READY);
        clearCmds();
        line.execute();
        check(STATE_LOADING, 1, 600, 0, 1000, 0, 0);
        checkDoser(dos1, 1, 300, 300, 0, 1, 0, 0, 0, 1, 1, 0);
        checkDoser(dos2, 1, 300, 300, 0, 1, 0, 0, 0, 1, 1, 0);

        // 14 - both are unload ready
        setDoserStatus(dos2, PaGeliosDozkkmcModule.STATUS_UNLOAD_READY);
        line.tagEnableUnload.setOff();
        clearCmds();
        line.execute();
        check(STATE_CAN_UNLOAD, 1, 600, 0, 1000, 0, 0);
        checkDoser(dos1, 1, 300, 300, 0, 1, 0, 0, 0, 0, 0, 0);
        checkDoser(dos2, 1, 300, 300, 0, 1, 0, 0, 0, 0, 0, 0);

        // 15 - waiting for enable unload
        line.tagEnableUnload.setInt(0);
        clearCmds();
        line.execute();
        check(STATE_CAN_UNLOAD, 1, 600, 0, 1000, 0, 0);
        checkDoser(dos1, 1, 300, 300, 0, 1, 0, 0, 0, 0, 0, 0);
        checkDoser(dos2, 1, 300, 300, 0, 1, 0, 0, 0, 0, 0, 0);

        // 16 - still waiting for enable unload
        line.tagEnableUnload.setInt(1);
        line.tagDisable.setInt(1);
        clearCmds();
        line.execute();
        check(STATE_CAN_UNLOAD, 1, 600, 0, 1000, 0, 0);
        checkDoser(dos1, 1, 300, 300, 0, 1, 0, 0, 0, 0, 0, 0);
        checkDoser(dos2, 1, 300, 300, 0, 1, 0, 0, 0, 0, 0, 0);

        // 17 - start unloading
        line.tagDisable.setInt(0);
        clearCmds();
        line.execute();
        check(STATE_UNLOADING, 1, 600, 0, 1000, 0, 0);
        checkDoser(dos1, 1, 300, 300, 0, 1, 0, 0, 0, 0, 0, 1);
        checkDoser(dos2, 1, 300, 300, 0, 1, 0, 0, 0, 0, 0, 1);

        // 18 - one is unload ready
        setDoserStatus(dos1, PaGeliosDozkkmcModule.STATUS_UNLOAD_READY);
        clearCmds();
        line.execute();
        check(STATE_UNLOADING, 1, 600, 0, 1000, 0, 0);
        checkDoser(dos1, 1, 300, 300, 0, 1, 0, 0, 0, 0, 0, 1);
        checkDoser(dos2, 1, 300, 300, 0, 1, 0, 0, 0, 0, 0, 1);

        // 19 - both are unload ready
        setDoserStatus(dos2, PaGeliosDozkkmcModule.STATUS_UNLOAD_READY);
        clearCmds();
        line.execute();
        check(STATE_UNLOADING, 1, 600, 0, 1000, 0, 0);
        checkDoser(dos1, 1, 300, 300, 0, 1, 0, 0, 0, 0, 0, 1);
        checkDoser(dos2, 1, 300, 300, 0, 1, 0, 0, 0, 0, 0, 1);

        // 20 - one finished unloading
        setDoserStatus(dos1, PaGeliosDozkkmcModule.STATUS_IDLE);
        clearCmds();
        line.execute();
        check(STATE_UNLOADING, 1, 600, 0, 1000, 0, 0);
        checkDoser(dos1, 1, 300, 300, 0, 1, 0, 0, 0, 0, 0, 1);
        checkDoser(dos2, 1, 300, 300, 0, 1, 0, 0, 0, 0, 0, 1);

        // 21 - one finished unloading
        clearCmds();
        line.execute();
        check(STATE_UNLOADING, 1, 600, 0, 1000, 0, 0);
        checkDoser(dos1, 1, 300, 300, 0, 1, 0, 0, 0, 0, 0, 1);
        checkDoser(dos2, 1, 300, 300, 0, 1, 0, 0, 0, 0, 0, 1);

        // 21 - fixing weight for bunk11
        clearCmds();
        setDoserSumWeights(110099, 120000, 130000, 210000, 310000);
        line.execute();
        check(STATE_UNLOADING, 1, 600, 99, 1000, 99, 10);
        checkDoser(dos1, 1, 300, 300, 99, 1, 0, 0, 0, 0, 0, 1);
        checkDoser(dos2, 1, 300, 300, 0, 1, 0, 0, 0, 0, 0, 1);

        // 21 - fixing weight for bunk12
        clearCmds();
        setDoserSumWeights(110099, 120202, 130000, 210000, 310000);
        line.execute();
        check(STATE_UNLOADING, 1, 600, 301, 1000, 301, 30);
        checkDoser(dos1, 1, 300, 300, 301, 1, 0, 0, 0, 0, 0, 1);
        checkDoser(dos2, 1, 300, 300, 0, 1, 0, 0, 0, 0, 0, 1);

        // 22 - fixing weight for bunk13 which is not in use, but also must be accounted
        clearCmds();
        setDoserSumWeights(110099, 120202, 130303, 210000, 310000);
        line.execute();
        check(STATE_UNLOADING, 1, 600, 604, 1000, 604, 60);
        checkDoser(dos1, 1, 300, 300, 604, 1, 0, 0, 0, 0, 0, 1);
        checkDoser(dos2, 1, 300, 300, 0, 1, 0, 0, 0, 0, 0, 1);

        // 23 - fixing weight for bunk21
        clearCmds();
        setDoserSumWeights(110099, 120202, 130303, 210304, 310000);
        line.execute();
        check(STATE_UNLOADING, 1, 600, 908, 1000, 908, 91);
        checkDoser(dos1, 1, 300, 300, 604, 1, 0, 0, 0, 0, 0, 1);
        checkDoser(dos2, 1, 300, 300, 304, 1, 0, 0, 0, 0, 0, 1);

        // 24 - fixing weight for bunk31 which is not in use, but also must be accounted
        clearCmds();
        setDoserSumWeights(110099, 120202, 130303, 210304, 310404);
        line.execute();
        check(STATE_UNLOADING, 1, 600, 1312, 1000, 1312, 131);
        checkDoser(dos1, 1, 300, 300, 604, 1, 0, 0, 0, 0, 0, 1);
        checkDoser(dos2, 1, 300, 300, 304, 1, 0, 0, 0, 0, 0, 1);
        checkDoser(dos3, 1, 0, 0, 404, 0, 0, 0, 0, 0, 0, 0);
//        assertEquals(Task.STATUS_FINISHED, svc.task.status);

        // 24.1
        clearCmds();
        setDoserStatus(dos2, PaGeliosDozkkmcModule.STATUS_IDLE);
        line.execute();
        check(STATE_CYCLE_FINISHED, 1, 600, 1312, 1000, 1312, 131);
        checkDoser(dos1, 1, 300, 300, 604, 1, 1, 0, 0, 0, 0, 0);
        checkDoser(dos2, 1, 300, 300, 304, 1, 1, 0, 0, 0, 0, 0);
        checkDoser(dos3, 1, 0, 0, 404, 0, 0, 0, 0, 0, 0, 0);

        // 24.2
        clearCmds();
        line.execute();
        check(STATE_IDLE, 1, 600, 0, 1000, 1312, 131);
        checkDoser(dos1, 1, 300, 300, 0, 1, 1, 0, 0, 0, 0, 0);
        checkDoser(dos2, 1, 300, 300, 0, 1, 1, 0, 0, 0, 0, 0);
        checkDoser(dos3, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

        // 25 - no auto reset
        clearCmds();
        line.execute();
///        check(STATE_TASK_FINISHED, 1, 600, 0, 1000, 1312, 131);
    }

    @Test
    void executeMultiCycleTest() throws SQLException {
        setDoserSumWeights(110000, 120000, 130000, 210000, 310000);

        // 1 - install task
        svc.task = getTask();
        line.tagTaskInstall.setInt(1);
        line.tagEnableLoad.setInt(1);
        line.tagEnableUnload.setInt(1);
        line.execute();
        check(STATE_IDLE, 0, 600, 0, 1000, 0, 0);

        // cycle 1
        line.tagStart.setInt(1);
        setDoserStatus(dos1, PaGeliosDozkkmcModule.STATUS_IDLE);
        setDoserStatus(dos2, PaGeliosDozkkmcModule.STATUS_IDLE);
        line.execute();
        check(STATE_PREPARING, 1, 600, 0, 1000, 0, 0);

        setDoserStatus(dos1, PaGeliosDozkkmcModule.STATUS_TASK_READY);
        setDoserStatus(dos2, PaGeliosDozkkmcModule.STATUS_TASK_READY);
        line.execute();
        check(STATE_LOADING, 1, 600, 0, 1000, 0, 0);

        setDoserStatus(dos1, PaGeliosDozkkmcModule.STATUS_UNLOAD_READY);
        setDoserStatus(dos2, PaGeliosDozkkmcModule.STATUS_UNLOAD_READY);
        line.execute();
        check(STATE_UNLOADING, 1, 600, 0, 1000, 0, 0);

        setDoserStatus(dos1, PaGeliosDozkkmcModule.STATUS_IDLE);
        setDoserStatus(dos2, PaGeliosDozkkmcModule.STATUS_IDLE);
        line.execute();
        check(STATE_CYCLE_FINISHED, 1, 600, 0, 1000, 0, 0);

        line.tagStart.setInt(0);
        setDoserSumWeights(110101, 120201, 130000, 210301, 310000);
        line.execute();
        check(STATE_IDLE, 1, 600, 0, 1000, 603, 60);

        // cycle 2
        line.tagStart.setInt(1);
        line.execute();
        check(STATE_PREPARING, 2, 600, 0, 1000, 603, 60);

        setDoserStatus(dos1, PaGeliosDozkkmcModule.STATUS_TASK_READY);
        setDoserStatus(dos2, PaGeliosDozkkmcModule.STATUS_TASK_READY);
        line.execute();
        check(STATE_LOADING, 2, 600, 0, 1000, 603, 60);

        setDoserStatus(dos1, PaGeliosDozkkmcModule.STATUS_UNLOAD_READY);
        setDoserStatus(dos2, PaGeliosDozkkmcModule.STATUS_UNLOAD_READY);
        line.execute();
        check(STATE_UNLOADING, 2, 600, 0, 1000, 603, 60);

        setDoserStatus(dos1, PaGeliosDozkkmcModule.STATUS_IDLE);
        setDoserStatus(dos2, PaGeliosDozkkmcModule.STATUS_IDLE);
        line.execute();
        check(STATE_CYCLE_FINISHED, 2, 600, 0, 1000, 603, 60);

        line.tagStart.setInt(0);
        setDoserSumWeights(110202, 120402, 130000, 210602, 310000);
        line.execute();
        check(STATE_IDLE, 2, 600, 0, 1000, 1206, 121);

        line.execute();
///        check(STATE_TASK_FINISHED, 2, 600, 0, 1000, 1206, 121);

        line.execute();
///        check(STATE_TASK_FINISHED, 2, 600, 0, 1000, 1206, 121);
    }


    @Test
    void executeMultiPassTest() throws SQLException {
        setDoserSumWeights(110000, 120000, 130000, 210000, 310000);

        dos1.refSetWeightMax.getValue().setLong(250);

        // 1 - install task
        svc.task = getTask();
        line.tagTaskInstall.setInt(1);
        line.tagEnableLoad.setInt(1);
        line.tagEnableUnload.setInt(1);
        line.execute();
        check(STATE_IDLE, 0, 600, 0, 1000, 0, 0);
        checkDoser(dos1, 1, 300, 300, 0, 2, 0, 0, 0, 0, 0, 0);
        checkDoser(dos2, 1, 300, 300, 0, 1, 0, 0, 0, 0, 0, 0);
        checkDoser(dos3, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

        // pass 1
        line.tagStart.setInt(1);
        setDoserStatus(dos1, PaGeliosDozkkmcModule.STATUS_IDLE);
        setDoserStatus(dos2, PaGeliosDozkkmcModule.STATUS_IDLE);
        clearCmds();
        line.execute();
        check(STATE_PREPARING, 1, 600, 0, 1000, 0, 0);
        checkDoser(dos1, 1, 300, 300, 0, 2, 0, 0, 1, 0, 0, 0);
        checkDoser(dos2, 1, 300, 300, 0, 1, 0, 0, 1, 0, 0, 0);
        checkDoser(dos3, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

        setDoserStatus(dos1, PaGeliosDozkkmcModule.STATUS_TASK_READY);
        setDoserStatus(dos2, PaGeliosDozkkmcModule.STATUS_TASK_READY);
        clearCmds();
        line.execute();
        check(STATE_LOADING, 1, 600, 0, 1000, 0, 0);
        checkDoser(dos1, 1, 300, 300, 0, 2, 0, 1, 1, 1, 1, 0);
        checkDoser(dos2, 1, 300, 300, 0, 1, 0, 1, 1, 1, 1, 0);
        checkDoser(dos3, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

        setDoserStatus(dos1, PaGeliosDozkkmcModule.STATUS_UNLOAD_READY);
        setDoserStatus(dos2, PaGeliosDozkkmcModule.STATUS_UNLOAD_READY);
        clearCmds();
        line.execute();
        check(STATE_UNLOADING, 1, 600, 0, 1000, 0, 0);
        checkDoser(dos1, 1, 300, 300, 0, 2, 0, 0, 0, 0, 0, 1);
        checkDoser(dos2, 1, 300, 300, 0, 1, 0, 0, 0, 0, 0, 1);
        checkDoser(dos3, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

        // pass 2
        setDoserStatus(dos1, PaGeliosDozkkmcModule.STATUS_IDLE);
        setDoserStatus(dos2, PaGeliosDozkkmcModule.STATUS_IDLE);
        clearCmds();
        line.execute();
        check(STATE_PREPARING_LOOP, 1, 600, 0, 1000, 0, 0);
        checkDoser(dos1, 1, 300, 300, 0, 2, 1, 0, 1, 0, 0, 0);
        checkDoser(dos2, 1, 300, 300, 0, 1, 1, 0, 0, 0, 0, 0);
        checkDoser(dos3, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

        clearCmds();
        line.execute();
        check(STATE_PREPARING_LOOP, 1, 600, 0, 1000, 0, 0);
        checkDoser(dos1, 1, 300, 300, 0, 2, 1, 0, 1, 0, 0, 0);
        checkDoser(dos2, 1, 300, 300, 0, 1, 1, 0, 0, 0, 0, 0);
        checkDoser(dos3, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

        setDoserStatus(dos1, PaGeliosDozkkmcModule.STATUS_TASK_READY);
        setDoserStatus(dos2, PaGeliosDozkkmcModule.STATUS_TASK_READY);
        clearCmds();
        line.execute();
        check(STATE_LOADING_LOOP, 1, 600, 0, 1000, 0, 0);
        checkDoser(dos1, 1, 300, 300, 0, 2, 1, 1, 1, 1, 1, 0);
        checkDoser(dos2, 1, 300, 300, 0, 1, 1, 0, 0, 0, 0, 0);
        checkDoser(dos3, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

        setDoserStatus(dos1, PaGeliosDozkkmcModule.STATUS_UNLOAD_READY);
        setDoserStatus(dos2, PaGeliosDozkkmcModule.STATUS_UNLOAD_READY);
        clearCmds();
        line.execute();
        check(STATE_UNLOADING_LOOP, 1, 600, 0, 1000, 0, 0);
        checkDoser(dos1, 1, 300, 300, 0, 2, 1, 0, 0, 0, 0, 1);
        checkDoser(dos2, 1, 300, 300, 0, 1, 1, 0, 0, 0, 0, 0);
        checkDoser(dos3, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

        setDoserSumWeights(110200, 120300, 130000, 210500, 310000);
        setDoserStatus(dos1, PaGeliosDozkkmcModule.STATUS_IDLE);
        setDoserStatus(dos2, PaGeliosDozkkmcModule.STATUS_IDLE);
        clearCmds();
        line.execute();
        check(STATE_CYCLE_FINISHED, 1, 600, 1000, 1000, 1000, 100);
        checkDoser(dos1, 1, 300, 300, 500, 2, 2, 0, 0, 0, 0, 0);
        checkDoser(dos2, 1, 300, 300, 500, 1, 1, 0, 0, 0, 0, 0);
        checkDoser(dos3, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

        line.tagStart.setInt(0);
        line.execute();
        check(STATE_IDLE, 1, 600, 0, 1000, 1000, 100);
        checkDoser(dos1, 1, 300, 300, 0, 2, 2, 0, 0, 0, 0, 0);
        checkDoser(dos2, 1, 300, 300, 0, 1, 1, 0, 0, 0, 0, 0);
        checkDoser(dos3, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

        line.execute();
///        check(STATE_TASK_FINISHED, 1, 600, 0, 1000, 1000, 100);
        checkDoser(dos1, 1, 300, 300, 0, 2, 2, 0, 0, 0, 0, 0);
        checkDoser(dos2, 1, 300, 300, 0, 1, 1, 0, 0, 0, 0, 0);
        checkDoser(dos3, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

    }



    /////////////////////////////////////////////// utils ////////////////////////////////////////////////////

    private Task getTask() {
        Task task = new Task(1, 1, "line1", 1, "recipe1", 0, Task.STATUS_READY);
        task.products.add(new Task.Product(1, "prod1", "dos1", 1, 100));
        task.products.add(new Task.Product(2, "prod2", "dos1", 2, 200));
        task.products.add(new Task.Product(3, "prod3", "dos2", 1, 300));
        return task;
    }

    private void setDoserStatus(
            Doser doser,
            int status
    ) {
        doser.refStatus.getTag().setInt(status);
    }

    private void setDoserSumWeights(
            long bunk11,
            long bunk12,
            long bunk13,
            long bunk21,
            long bunk31
    ) {
        refgr1.items.get("SumWeight1").getTag().setLong(bunk11);
        refgr1.items.get("SumWeight2").getTag().setLong(bunk12);
        refgr1.items.get("SumWeight3").getTag().setLong(bunk13);
        refgr2.items.get("SumWeight1").getTag().setLong(bunk21);
        refgr3.items.get("SumWeight1").getTag().setLong(bunk31);
    }

    private void check(
            int state,
            int cycleId,
            long rcpWeight,
            long exeWeight,
            long notinuse1,
            long exeWeightShift,
            int notinuse2
    ) {
        assertEquals(state, line.tagState.getInt(), "state");
        assertEquals(cycleId, line.tagCycleId.getInt(), "cycleId");
        assertEquals(rcpWeight, line.tagRcpWeight.getLong(), "rcpWeight");
        assertEquals(exeWeight, line.tagExeWeight.getLong(), "exeWeight");
//        assertEquals(setWeightTot, line.tagSetWeightTot.getLong(), "setWeightTot");
        assertEquals(exeWeightShift, line.tagExeWeightShf.getLong(), "exeWeightShift");
//        assertEquals(progressTot, line.tagProgressTot.getInt(), "progressTot");
    }

    private void checkDoser(
            Doser doser,
            int tagConnected,
            long tagRcpWeight,
            long tagSetWeight,
            long tagExeWeight,
            int tagPassQty,
            int tagPassCnt,
            int refSendTask,
            int refCmdStartCycle,
            int refCmdStartTask,
            int refCmdStartLoad,
            int refCmdStartUnload
    ) {
        assertEquals(tagRcpWeight,      doser.tagRcpWeight  .getLong(), "tagRcpWeight");
        assertEquals(tagSetWeight,      doser.tagSetWeight  .getLong(), "tagSetWeight");
        assertEquals(tagExeWeight,      doser.tagExeWeight  .getLong(), "tagExeWeight");
        assertEquals(tagConnected,      doser.tagConnected  .getInt(), "tagConnected");
        assertEquals(tagPassQty,        doser.tagPassQty    .getInt(), "tagPassQty");
        assertEquals(tagPassCnt,        doser.tagPassCnt    .getInt(), "tagPassCnt");
        assertEquals(refSendTask,       doser.refSendTask       .getValue().getInt(), "refSendTask");
        assertEquals(refCmdStartCycle,  doser.refCmdStartCycle  .getValue().getInt(), "refCmdStartCycle");
        assertEquals(refCmdStartTask,   doser.refCmdStartTask   .getValue().getInt(), "refCmdStartTask");
        assertEquals(refCmdStartLoad,   doser.refCmdStartLoad   .getValue().getInt(), "refCmdStartLoad");
        assertEquals(refCmdStartUnload, doser.refCmdStartUnload .getValue().getInt(), "refCmdStartUnload");
    }

    private void checkFeeder(
            Doser doser,
            int num,
            long tagRcpWeight,
            long tagSetWeight,
            long tagSetWeightPass,
            long tagExeWeight,
            long tagSumWeight,
            long tagSumWeightBeg,
            long refSetWeight
    ) {
        Feeder feeder = doser.feeders.get(num);
        assertEquals(tagRcpWeight,      feeder.tagRcpWeight.getLong(), "tagRcpWeight");
        assertEquals(tagSetWeight,      feeder.tagSetWeight.getLong(), "tagSetWeight");
        assertEquals(tagSetWeightPass,  feeder.tagSetWeightPass.getLong(), "tagSetWeightPass");
        assertEquals(tagExeWeight,      feeder.tagExeWeight.getLong(), "tagExeWeight");
        assertEquals(tagSumWeight,      feeder.tagSumWeight.getLong(), "tagSumWeight");
        assertEquals(tagSumWeightBeg,   feeder.tagSumWeightBeg.getLong(), "tagSumWeightBeg");
        assertEquals(refSetWeight,      feeder.refSetWeight.getValue().getLong(), "refSetWeight");
    }


    private void clearCmds() {
        Arrays.asList( dos1, dos2 ).forEach(d ->
                Arrays.asList( d.refSendTask, d.refCmdStartCycle, d.refCmdStartTask, d.refCmdStartLoad, d.refCmdStartUnload ).forEach(r ->
                        r.getValue().setInt(0)));
    }

}