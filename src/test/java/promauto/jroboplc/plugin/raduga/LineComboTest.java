package promauto.jroboplc.plugin.raduga;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import promauto.jroboplc.core.ConfigurationYaml;
import promauto.jroboplc.core.TagTable;
import promauto.jroboplc.core.api.EnvironmentInst;
import promauto.jroboplc.core.api.SampleEnvironment;
import promauto.jroboplc.core.tags.RefGroupDummy;
import promauto.jroboplc.plugin.peripherial.PaGeliosDozkkmcModule;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static promauto.jroboplc.plugin.raduga.LineCombo.*;

class LineComboTest {


    LineCombo line;
    LineCombo line2;
    LineCombo line3;
    Context ctx;
    DataServiceDummy svc;

    Doser dos1, dos2, dos3, dos4, dos5;
    CtlDoser dosC;
    RefGroupDummy refgr1, refgr2, refgr3, refgr4, refgr5;

    @BeforeEach
    void setUp() {
        SampleEnvironment env = new SampleEnvironment();
        EnvironmentInst.set(env);
        env.setConfiguration(new ConfigurationYaml(""));

        svc = new DataServiceDummy();

        ctx = new Context();
        ctx.setEnvironment(env);
        ctx.setDataService(svc);
        ctx.tagtable = new TagTable();
//        ctx.setTaskrepo(new TaskRepo(ctx));

        Map<Object,Object> conf = new HashMap<>();
        conf.put("num", 1);
        conf.put("name", "line1");
        conf.put("descr", "line one");
        conf.put("type", "combo");
        conf.put("hidden", false);
        conf.put("autostart", false);
        conf.put("ctlDoserModule", "modC");

        Map<Object,Object> confDos1 = new HashMap<>();
        Map<Object,Object> confDos2 = new HashMap<>();
        Map<Object,Object> confDos3 = new HashMap<>();
        conf.put("dosers", new ArrayList<>(Arrays.asList(confDos1, confDos2, confDos3)));

        Map<Object,Object> confDos4 = new HashMap<>();
        Map<Object,Object> confDos5 = new HashMap<>();
        conf.put("microDosers", new ArrayList<>(Arrays.asList(confDos4, confDos5)));

        confDos1.put("name", "dos1");
        confDos1.put("module", "mod1");
        confDos1.put("srcBunkers", new ArrayList<>(Arrays.asList("bunk11", "bunk12", "bunk13")));

        confDos2.put("name", "dos2");
        confDos2.put("module", "mod2");
        confDos2.put("srcBunkers", new ArrayList<>(Arrays.asList("bunk21")));

        confDos3.put("name", "dos3");
        confDos3.put("module", "mod3");
        confDos3.put("srcBunkers", new ArrayList<>(Arrays.asList("bunk31")));

        confDos4.put("name", "dos4");
        confDos4.put("module", "mod4");
        confDos4.put("srcBunkers", new ArrayList<>(Arrays.asList("bunk41", "bunk42")));

        confDos5.put("name", "dos5");
        confDos5.put("module", "mod5");
        confDos5.put("srcBunkers", new ArrayList<>(Arrays.asList("bunk51")));


        line = new LineCombo(ctx);
        line.load(conf);
        line.dosers.forEach(DoserDummy::changeTagTypes);

        dos1 = line.dosers.get(0);
        dos2 = line.dosers.get(1);
        dos3 = line.dosers.get(2);
        dos4 = line.dosers.get(3);
        dos5 = line.dosers.get(4);
        dosC = line.ctlDoser;

        refgr1 = (RefGroupDummy) line.dosers.get(0).refgr;
        refgr2 = (RefGroupDummy) line.dosers.get(1).refgr;
        refgr3 = (RefGroupDummy) line.dosers.get(2).refgr;
        refgr4 = (RefGroupDummy) line.dosers.get(3).refgr;
        refgr5 = (RefGroupDummy) line.dosers.get(4).refgr;

        ctx.lines.add(line);


        conf.put("num", 2);
        conf.put("name", "line2");
        conf.put("descr", "line two");
        line2 = new LineCombo(ctx);
        line2.load(conf);
        ctx.lines.add(line2);

        conf.put("num", 3);
        conf.put("name", "line3");
        conf.put("descr", "line three");
        line3 = new LineCombo(ctx);
        line3.load(conf);
        ctx.lines.add(line3);

    }


    @Test
    void initTest() throws SQLException {
        assertTrue( line.prepare() );
        assertTrue( line.init() );
    }

    @Test
    void holdDosersTest() throws SQLException {

        // 1
        line.tagState.setReadValInt(STATE_HOLD_DOSERS);
        line2.tagState.setReadValInt(STATE_NONE);
        line3.tagState.setReadValInt(STATE_NONE);
        line.execute();
        line2.execute();
        line3.execute();
        assertTrue(line.tagHoldDosers.getBool());
        assertFalse(line2.tagHoldDosers.getBool());
        assertFalse(line3.tagHoldDosers.getBool());

        // 2
        line2.tagState.setReadValInt(STATE_HOLD_DOSERS);
        line.execute();
        line2.execute();
        line3.execute();
        assertTrue(line.tagHoldDosers.getBool());
        assertFalse(line2.tagHoldDosers.getBool());
        assertFalse(line3.tagHoldDosers.getBool());

        // 3
        line.tagState.setReadValInt(STATE_HOLD_DOSERS);
        line2.tagState.setReadValInt(STATE_HOLD_DOSERS);
        line3.tagState.setReadValInt(STATE_HOLD_DOSERS);
        line.timeHoldCatching = 3;
        line2.timeHoldCatching = 2;
        line3.timeHoldCatching = 1;
        line.execute();
        line2.execute();
        line3.execute();
        assertFalse(line.tagHoldDosers.getBool());
        assertFalse(line2.tagHoldDosers.getBool());
        assertTrue(line3.tagHoldDosers.getBool());

        // 4
        line.execute();
        line2.execute();
        line3.execute();
        assertFalse(line.tagHoldDosers.getBool());
        assertFalse(line2.tagHoldDosers.getBool());
        assertTrue(line3.tagHoldDosers.getBool());
        assertEquals(STATE_HOLD_DOSERS, line.tagState.getInt());
        assertEquals(STATE_HOLD_DOSERS, line2.tagState.getInt());
        assertEquals(STATE_PREPARING_C, line3.tagState.getInt());

        // 5
        line.tagState.setReadValInt(STATE_PREPARING_C);
        line.execute();
        line2.execute();
        line3.execute();
        assertFalse(line.tagHoldDosers.getBool());
        assertFalse(line2.tagHoldDosers.getBool());
        assertTrue(line3.tagHoldDosers.getBool());
        assertEquals(STATE_ERROR, line.tagState.getInt());
        assertEquals(STATE_HOLD_DOSERS, line2.tagState.getInt());
        assertEquals(STATE_PREPARING_C, line3.tagState.getInt());
    }

    @Test
    void holdTransportTest() throws SQLException {

        line.tagTimeTransportP.setInt(10);

        // 1
        line.tagState.setReadValInt(STATE_HOLD_TRANSPORT);
        line2.tagState.setReadValInt(STATE_NONE);
        line3.tagState.setReadValInt(STATE_NONE);
        line.execute();
        line2.execute();
        line3.execute();
        assertFalse(line.tagHoldDosers.getBool());
        assertFalse(line2.tagHoldDosers.getBool());
        assertFalse(line3.tagHoldDosers.getBool());

        assertTrue(line.tagHoldTransport.getBool());
        assertFalse(line2.tagHoldTransport.getBool());
        assertFalse(line3.tagHoldTransport.getBool());

        assertEquals(STATE_DELAY_TRANSPORT_P, line.tagState.getInt());
        assertEquals(STATE_NONE, line2.tagState.getInt());
        assertEquals(STATE_NONE, line3.tagState.getInt());


        // 2
//        line.tagState.setReadValInt(STATE_HOLD_TRANSPORT);
        line2.tagState.setReadValInt(STATE_HOLD_TRANSPORT);
        line3.tagState.setReadValInt(STATE_NONE);
        line.execute();
        line2.execute();
        line3.execute();
        assertFalse(line.tagHoldDosers.getBool());
        assertTrue(line2.tagHoldDosers.getBool()); //?
        assertFalse(line3.tagHoldDosers.getBool());

        assertTrue(line.tagHoldTransport.getBool());
        assertFalse(line2.tagHoldTransport.getBool());
        assertFalse(line3.tagHoldTransport.getBool());

        assertEquals(STATE_DELAY_TRANSPORT_P, line.tagState.getInt());
        assertEquals(STATE_HOLD_TRANSPORT, line2.tagState.getInt());
        assertEquals(STATE_NONE, line3.tagState.getInt());

    }


    @Test
    void executeTest() throws SQLException {
        setDoserSumWeights(110000, 120000, 130000, 210000, 310000, 410000, 420000, 510000);

        line.tagEnableLoad.setOff();
        line.tagEnableUnload.setOff();
        line.tagEnableLoadP.setOff();
        line.tagEnableUnloadP.setOff();
        line.tagEnableLoadM.setOff();
        line.tagEnableUnloadM.setOff();



        // 0
        execute();
        check1(STATE_NONE, 0,    0, 0,     0, 0,     0);
        check2(0, 0, 0,0,     0, 0, 0);
        check3(0, 0, 0, 0, 0, 0);
        checkDoser(dos1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        checkDoser(dos2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        checkDoser(dos3, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        checkDoser(dos4, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        checkDoser(dos5, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        checkCtlDoser(1, 0, 0, 0, 0, 0, 0, 0, 0);
        checkFeeder(dos1, 0, 0, 0, 0, 0, 110000, -1, 0);
        checkFeeder(dos1, 1, 0, 0, 0, 0, 120000, -1, 0);
        checkFeeder(dos2, 0, 0, 0, 0, 0, 210000, -1, 0);
        checkFeeder(dos3, 0, 0, 0, 0, 0, 310000, -1, 0);
        checkFeeder(dos4, 0, 0, 0, 0, 0, 410000, -1, 0);
        checkFeeder(dos4, 1, 0, 0, 0, 0, 420000, -1, 0);
        checkFeeder(dos5, 0, 0, 0, 0, 0, 510000, -1, 0);


        // 1 - install task
        svc.task = getTask();
        line.tagTaskInstall.setInt(1);
        execute();
        check1(STATE_IDLE, 0, 750, 0, 1000, 0, 0);
        check2(600, 600, 0,    0,     150, 150, 0);
        check3(0, 0, 0, 0, 0, 0);
        checkDoser(dos1, 1, 300, 300, 0, 1, 0, 0, 0, 0, 0, 0);
        checkDoser(dos2, 1, 300, 300, 0, 1, 0, 0, 0, 0, 0, 0);
        checkDoser(dos3, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        checkDoser(dos4, 1, 90, 90, 0, 1, 0, 0, 0, 0, 0, 0);
        checkDoser(dos5, 1, 60, 60, 0, 1, 0, 0, 0, 0, 0, 0);
        checkCtlDoser(1, 0, 0, 0, 0, 0, 0, 0, 0);
        checkFeeder(dos1, 0, 100, 100, 100, 0, 110000, -1, 0);
        checkFeeder(dos1, 1, 200, 200, 200, 0, 120000, -1, 0);
        checkFeeder(dos2, 0, 300, 300, 300, 0, 210000, -1, 0);
        checkFeeder(dos3, 0, 0, 0, 0, 0, 310000, -1, 0);
        checkFeeder(dos4, 0, 40, 40, 40, 0, 410000, -1, 0);
        checkFeeder(dos4, 1, 50, 50, 50, 0, 420000, -1, 0);
        checkFeeder(dos5, 0, 60, 60, 60, 0, 510000, -1, 0);

        // 2
        line.tagStart.setOn();
        execute();
        check1(STATE_PREPARING_C, 1, 750, 0, 1000, 0, 0);
        check2(600, 600, 0,    0,     150, 150, 0);
        check3(1, 0, 0, 0, 0, 0);
        checkDoser(dos1, 1, 300, 300, 0, 1, 0, 0, 0, 0, 0, 0);
        checkDoser(dos2, 1, 300, 300, 0, 1, 0, 0, 0, 0, 0, 0);
        checkDoser(dos3, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        checkDoser(dos4, 1, 90, 90, 0, 1, 0, 0, 0, 0, 0, 0);
        checkDoser(dos5, 1, 60, 60, 0, 1, 0, 0, 0, 0, 0, 0);
        checkCtlDoser(1, 0, 0, 0, 1, 0, 0, 0, 0);
        checkFeeder(dos1, 0, 100, 100, 100, 0, 110000, 110000, 0);
        checkFeeder(dos1, 1, 200, 200, 200, 0, 120000, 120000, 0);
        checkFeeder(dos2, 0, 300, 300, 300, 0, 210000, 210000, 0);
        checkFeeder(dos3, 0, 0, 0, 0, 0, 310000, 310000, 0);
        checkFeeder(dos4, 0, 40, 40, 40, 0, 410000, 410000, 40);
        checkFeeder(dos4, 1, 50, 50, 50, 0, 420000, 420000, 50);
        checkFeeder(dos5, 0, 60, 60, 60, 0, 510000, 510000, 60);

        // 2.1
        dosC.refStatus.getTag().setInt(PaGeliosDozkkmcModule.STATUS_TASK_READY);
        line.tagStart.setOn();
        execute();
        check1(STATE_START_LOAD_C, 1, 750, 0, 1000, 0, 0);
        check2(600, 600, 0,    0,     150, 150, 0);
        check3(1, 0, 0, 0, 0, 0);
        checkDoser(dos1, 1, 300, 300, 0, 1, 0, 0, 0, 0, 0, 0);
        checkDoser(dos2, 1, 300, 300, 0, 1, 0, 0, 0, 0, 0, 0);
        checkDoser(dos3, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        checkDoser(dos4, 1, 90, 90, 0, 1, 0, 0, 0, 0, 0, 0);
        checkDoser(dos5, 1, 60, 60, 0, 1, 0, 0, 0, 0, 0, 0);
        checkCtlDoser(1, 0, 0, 1, 1, 1, 1, 0, 0);
        checkFeeder(dos1, 0, 100, 100, 100, 0, 110000, 110000, 100);
        checkFeeder(dos1, 1, 200, 200, 200, 0, 120000, 120000, 200);
        checkFeeder(dos2, 0, 300, 300, 300, 0, 210000, 210000, 300);
        checkFeeder(dos3, 0, 0, 0, 0, 0, 310000, 310000, 0);
        checkFeeder(dos4, 0, 40, 40, 40, 0, 410000, 410000, 40);
        checkFeeder(dos4, 1, 50, 50, 50, 0, 420000, 420000, 50);
        checkFeeder(dos5, 0, 60, 60, 60, 0, 510000, 510000, 60);

        // 3
        dosC.refStatus.getTag().setInt(PaGeliosDozkkmcModule.STATUS_LOAD);
        execute();
        check1(STATE_PREPARING_P, 1, 750, 0, 1000, 0, 0);
        check2(600, 600, 0,    0,     150, 150, 0);
        check3(1, 0, 0, 1, 0, 0);
        checkDoser(dos1, 1, 300, 300, 0, 1, 0, 0, 1, 0, 0, 0);
        checkDoser(dos2, 1, 300, 300, 0, 1, 0, 0, 1, 0, 0, 0);
        checkDoser(dos3, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        checkDoser(dos4, 1, 90, 90, 0, 1, 0, 0, 0, 0, 0, 0);
        checkDoser(dos5, 1, 60, 60, 0, 1, 0, 0, 0, 0, 0, 0);
        checkCtlDoser(1, 0, 0, 0, 0, 0, 0, 0, 0);
        checkFeeder(dos1, 0, 100, 100, 100, 0, 110000, 110000, 100);
        checkFeeder(dos1, 1, 200, 200, 200, 0, 120000, 120000, 200);
        checkFeeder(dos2, 0, 300, 300, 300, 0, 210000, 210000, 300);
        checkFeeder(dos3, 0, 0, 0, 0, 0, 310000, 310000, 0);
        checkFeeder(dos4, 0, 40, 40, 40, 0, 410000, 410000, 40);
        checkFeeder(dos4, 1, 50, 50, 50, 0, 420000, 420000, 50);
        checkFeeder(dos5, 0, 60, 60, 60, 0, 510000, 510000, 60);

        // 5
        setDoserStatus(dos1, PaGeliosDozkkmcModule.STATUS_TASK_READY);
        execute();
        check1(STATE_PREPARING_P, 1, 750, 0, 1000, 0, 0);
        check2(600, 600, 0,    0,     150, 150, 0);
        check3(1, 0, 0, 1, 0, 0);
        checkDoser(dos1, 1, 300, 300, 0, 1, 0, 0, 1, 0, 0, 0);
        checkDoser(dos2, 1, 300, 300, 0, 1, 0, 0, 1, 0, 0, 0);
        checkDoser(dos3, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        checkDoser(dos4, 1, 90, 90, 0, 1, 0, 0, 0, 0, 0, 0);
        checkDoser(dos5, 1, 60, 60, 0, 1, 0, 0, 0, 0, 0, 0);
        checkCtlDoser(1, 0, 0, 0, 0, 0, 0, 0, 0);
        checkFeeder(dos1, 0, 100, 100, 100, 0, 110000, 110000, 100);
        checkFeeder(dos1, 1, 200, 200, 200, 0, 120000, 120000, 200);
        checkFeeder(dos2, 0, 300, 300, 300, 0, 210000, 210000, 300);
        checkFeeder(dos3, 0, 0, 0, 0, 0, 310000, 310000, 0);
        checkFeeder(dos4, 0, 40, 40, 40, 0, 410000, 410000, 40);
        checkFeeder(dos4, 1, 50, 50, 50, 0, 420000, 420000, 50);
        checkFeeder(dos5, 0, 60, 60, 60, 0, 510000, 510000, 60);

        // 6
        setDoserStatus(dos2, PaGeliosDozkkmcModule.STATUS_TASK_READY);
        execute();
        check1(STATE_CAN_LOAD_P, 1, 750, 0, 1000, 0, 0);
        check2(600, 600, 0,    0,     150, 150, 0);
        check3(1, 0, 0, 1, 0, 0);
        checkDoser(dos1, 1, 300, 300, 0, 1, 0, 1, 1, 0, 0, 0);
        checkDoser(dos2, 1, 300, 300, 0, 1, 0, 1, 1, 0, 0, 0);
        checkDoser(dos3, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        checkDoser(dos4, 1, 90, 90, 0, 1, 0, 0, 0, 0, 0, 0);
        checkDoser(dos5, 1, 60, 60, 0, 1, 0, 0, 0, 0, 0, 0);
        checkCtlDoser(1, 0, 0, 0, 0, 0, 0, 0, 0);
        checkFeeder(dos1, 0, 100, 100, 100, 0, 110000, 110000, 100);
        checkFeeder(dos1, 1, 200, 200, 200, 0, 120000, 120000, 200);
        checkFeeder(dos2, 0, 300, 300, 300, 0, 210000, 210000, 300);
        checkFeeder(dos3, 0, 0, 0, 0, 0, 310000, 310000, 0);
        checkFeeder(dos4, 0, 40, 40, 40, 0, 410000, 410000, 40);
        checkFeeder(dos4, 1, 50, 50, 50, 0, 420000, 420000, 50);
        checkFeeder(dos5, 0, 60, 60, 60, 0, 510000, 510000, 60);

        // 7
        execute();
        check1(STATE_CAN_LOAD_P, 1, 750, 0, 1000, 0, 0);
        check2(600, 600, 0,    0,     150, 150, 0);
        check3(1, 0, 0, 1, 0, 0);
        checkDoser(dos1, 1, 300, 300, 0, 1, 0, 0, 0, 0, 0, 0);
        checkDoser(dos2, 1, 300, 300, 0, 1, 0, 0, 0, 0, 0, 0);
        checkDoser(dos3, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        checkDoser(dos4, 1, 90, 90, 0, 1, 0, 0, 0, 0, 0, 0);
        checkDoser(dos5, 1, 60, 60, 0, 1, 0, 0, 0, 0, 0, 0);
        checkCtlDoser(1, 0, 0, 0, 0, 0, 0, 0, 0);
        checkFeeder(dos1, 0, 100, 100, 100, 0, 110000, 110000, 100);
        checkFeeder(dos1, 1, 200, 200, 200, 0, 120000, 120000, 200);
        checkFeeder(dos2, 0, 300, 300, 300, 0, 210000, 210000, 300);
        checkFeeder(dos3, 0, 0, 0, 0, 0, 310000, 310000, 0);
        checkFeeder(dos4, 0, 40, 40, 40, 0, 410000, 410000, 40);
        checkFeeder(dos4, 1, 50, 50, 50, 0, 420000, 420000, 50);
        checkFeeder(dos5, 0, 60, 60, 60, 0, 510000, 510000, 60);

        // 8
        line.tagEnableLoad.setOn();
        line.tagEnableLoadP.setOn();
        execute();
        check1(STATE_LOADING_P, 1, 750, 0, 1000, 0, 0);
        check2(600, 600, 0,    0,     150, 150, 0);
        check3(1, 0, 0, 1, 0, 0);
        checkDoser(dos1, 1, 300, 300, 0, 1, 0, 0, 0, 1, 1, 0);
        checkDoser(dos2, 1, 300, 300, 0, 1, 0, 0, 0, 1, 1, 0);
        checkDoser(dos3, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        checkDoser(dos4, 1, 90, 90, 0, 1, 0, 0, 0, 0, 0, 0);
        checkDoser(dos5, 1, 60, 60, 0, 1, 0, 0, 0, 0, 0, 0);
        checkCtlDoser(1, 0, 0, 0, 0, 0, 0, 0, 0);
        checkFeeder(dos1, 0, 100, 100, 100, 0, 110000, 110000, 100);
        checkFeeder(dos1, 1, 200, 200, 200, 0, 120000, 120000, 200);
        checkFeeder(dos2, 0, 300, 300, 300, 0, 210000, 210000, 300);
        checkFeeder(dos3, 0, 0, 0, 0, 0, 310000, 310000, 0);
        checkFeeder(dos4, 0, 40, 40, 40, 0, 410000, 410000, 40);
        checkFeeder(dos4, 1, 50, 50, 50, 0, 420000, 420000, 50);
        checkFeeder(dos5, 0, 60, 60, 60, 0, 510000, 510000, 60);

        // 9
        setDoserStatus(dos1, PaGeliosDozkkmcModule.STATUS_UNLOAD_READY);
        execute();
        check1(STATE_LOADING_P, 1, 750, 0, 1000, 0, 0);
        check2(600, 600, 0,    0,     150, 150, 0);
        check3(1, 0, 0, 1, 0, 0);
        checkDoser(dos1, 1, 300, 300, 0, 1, 0, 0, 0, 1, 1, 0);
        checkDoser(dos2, 1, 300, 300, 0, 1, 0, 0, 0, 1, 1, 0);
        checkDoser(dos3, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        checkDoser(dos4, 1, 90, 90, 0, 1, 0, 0, 0, 0, 0, 0);
        checkDoser(dos5, 1, 60, 60, 0, 1, 0, 0, 0, 0, 0, 0);
        checkCtlDoser(1, 0, 0, 0, 0, 0, 0, 0, 0);
        checkFeeder(dos1, 0, 100, 100, 100, 0, 110000, 110000, 100);
        checkFeeder(dos1, 1, 200, 200, 200, 0, 120000, 120000, 200);
        checkFeeder(dos2, 0, 300, 300, 300, 0, 210000, 210000, 300);
        checkFeeder(dos3, 0, 0, 0, 0, 0, 310000, 310000, 0);
        checkFeeder(dos4, 0, 40, 40, 40, 0, 410000, 410000, 40);
        checkFeeder(dos4, 1, 50, 50, 50, 0, 420000, 420000, 50);
        checkFeeder(dos5, 0, 60, 60, 60, 0, 510000, 510000, 60);

        // 10
        setDoserStatus(dos2, PaGeliosDozkkmcModule.STATUS_UNLOAD_READY);
        execute();
        check1(STATE_UNLOADING_P, 1, 750, 0, 1000, 0, 0);
        check2(600, 600, 0,    0,     150, 150, 0);
        check3(1, 1, 0, 1, 0, 0);
        checkDoser(dos1, 1, 300, 300, 0, 1, 0, 0, 0, 0, 0, 0);
        checkDoser(dos2, 1, 300, 300, 0, 1, 0, 0, 0, 0, 0, 0);
        checkDoser(dos3, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        checkDoser(dos4, 1, 90, 90, 0, 1, 0, 0, 0, 0, 0, 0);
        checkDoser(dos5, 1, 60, 60, 0, 1, 0, 0, 0, 0, 0, 0);
        checkCtlDoser(1, 0, 0, 0, 0, 0, 0, 0, 0);
        checkFeeder(dos1, 0, 100, 100, 100, 0, 110000, 110000, 100);
        checkFeeder(dos1, 1, 200, 200, 200, 0, 120000, 120000, 200);
        checkFeeder(dos2, 0, 300, 300, 300, 0, 210000, 210000, 300);
        checkFeeder(dos3, 0, 0, 0, 0, 0, 310000, 310000, 0);
        checkFeeder(dos4, 0, 40, 40, 40, 0, 410000, 410000, 40);
        checkFeeder(dos4, 1, 50, 50, 50, 0, 420000, 420000, 50);
        checkFeeder(dos5, 0, 60, 60, 60, 0, 510000, 510000, 60);

        // 11
        execute();
        check1(STATE_UNLOADING_P, 1, 750, 0, 1000, 0, 0);
        check2(600, 600, 0,    0,     150, 150, 0);
        check3(1, 1, 0, 1, 0, 0);
        checkDoser(dos1, 1, 300, 300, 0, 1, 0, 0, 0, 0, 0, 0);
        checkDoser(dos2, 1, 300, 300, 0, 1, 0, 0, 0, 0, 0, 0);
        checkDoser(dos3, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        checkDoser(dos4, 1, 90, 90, 0, 1, 0, 0, 0, 0, 0, 0);
        checkDoser(dos5, 1, 60, 60, 0, 1, 0, 0, 0, 0, 0, 0);
        checkCtlDoser(1, 0, 0, 0, 0, 0, 0, 0, 0);
        checkFeeder(dos1, 0, 100, 100, 100, 0, 110000, 110000, 100);
        checkFeeder(dos1, 1, 200, 200, 200, 0, 120000, 120000, 200);
        checkFeeder(dos2, 0, 300, 300, 300, 0, 210000, 210000, 300);
        checkFeeder(dos3, 0, 0, 0, 0, 0, 310000, 310000, 0);
        checkFeeder(dos4, 0, 40, 40, 40, 0, 410000, 410000, 40);
        checkFeeder(dos4, 1, 50, 50, 50, 0, 420000, 420000, 50);
        checkFeeder(dos5, 0, 60, 60, 60, 0, 510000, 510000, 60);

        // 12
        line.tagEnableUnload.setOn();
        line.tagEnableUnloadP.setOn();
        execute();
        check1(STATE_UNLOADING_P, 1, 750, 0, 1000, 0, 0);
        check2(600, 600, 0,    0,     150, 150, 0);
        check3(1, 1, 0, 1, 0, 0);
        checkDoser(dos1, 1, 300, 300, 0, 1, 0, 0, 0, 0, 0, 1);
        checkDoser(dos2, 1, 300, 300, 0, 1, 0, 0, 0, 0, 0, 1);
        checkDoser(dos3, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        checkDoser(dos4, 1, 90, 90, 0, 1, 0, 0, 0, 0, 0, 0);
        checkDoser(dos5, 1, 60, 60, 0, 1, 0, 0, 0, 0, 0, 0);
        checkCtlDoser(1, 0, 0, 0, 0, 0, 0, 0, 0);
        checkFeeder(dos1, 0, 100, 100, 100, 0, 110000, 110000, 100);
        checkFeeder(dos1, 1, 200, 200, 200, 0, 120000, 120000, 200);
        checkFeeder(dos2, 0, 300, 300, 300, 0, 210000, 210000, 300);
        checkFeeder(dos3, 0, 0, 0, 0, 0, 310000, 310000, 0);
        checkFeeder(dos4, 0, 40, 40, 40, 0, 410000, 410000, 40);
        checkFeeder(dos4, 1, 50, 50, 50, 0, 420000, 420000, 50);
        checkFeeder(dos5, 0, 60, 60, 60, 0, 510000, 510000, 60);

        // 13
        setDoserSumWeights(110600, 120000, 130000, 210000, 310000, 410000, 420000, 510000);
        setDoserStatus(dos1, PaGeliosDozkkmcModule.STATUS_IDLE);
        execute();
        check1(STATE_UNLOADING_P, 1, 750, 600, 1000, 600, 60);
        check2(600, 600, 600,    0,     150, 150, 0);
        check3(1, 1, 0, 1, 0, 0);
        checkDoser(dos1, 1, 300, 300, 600, 1, 0, 0, 0, 0, 0, 1);
        checkDoser(dos2, 1, 300, 300, 0, 1, 0, 0, 0, 0, 0, 1);
        checkDoser(dos3, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        checkDoser(dos4, 1, 90, 90, 0, 1, 0, 0, 0, 0, 0, 0);
        checkDoser(dos5, 1, 60, 60, 0, 1, 0, 0, 0, 0, 0, 0);
        checkCtlDoser(1, 0, 0, 0, 0, 0, 0, 0, 0);
        checkFeeder(dos1, 0, 100, 100, 100, 600, 110600, 110000, 100);
        checkFeeder(dos1, 1, 200, 200, 200, 0, 120000, 120000, 200);
        checkFeeder(dos2, 0, 300, 300, 300, 0, 210000, 210000, 300);
        checkFeeder(dos3, 0, 0, 0, 0, 0, 310000, 310000, 0);
        checkFeeder(dos4, 0, 40, 40, 40, 0, 410000, 410000, 40);
        checkFeeder(dos4, 1, 50, 50, 50, 0, 420000, 420000, 50);
        checkFeeder(dos5, 0, 60, 60, 60, 0, 510000, 510000, 60);

        // 14
        line.tagTimeTransportP.setInt(2);
        setDoserStatus(dos2, PaGeliosDozkkmcModule.STATUS_IDLE);
        execute();
        check1(STATE_DELAY_TRANSPORT_P, 1, 750, 600, 1000, 600, 60);
        check2(600, 600, 600,    0,     150, 150, 0);
        check3(0, 1, 1, 1, 0, 0);
        checkDoser(dos1, 1, 300, 300, 600, 1, 1, 0, 0, 0, 0, 0);
        checkDoser(dos2, 1, 300, 300, 0, 1, 1, 0, 0, 0, 0, 0);
        checkDoser(dos3, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        checkDoser(dos4, 1, 90, 90, 0, 1, 0, 0, 0, 0, 0, 0);
        checkDoser(dos5, 1, 60, 60, 0, 1, 0, 0, 0, 0, 0, 0);
        checkCtlDoser(1, 0, 0, 0, 0, 0, 0, 0, 0);
        checkFeeder(dos1, 0, 100, 100, 100, 600, 110600, 110000, 100);
        checkFeeder(dos1, 1, 200, 200, 200, 0, 120000, 120000, 200);
        checkFeeder(dos2, 0, 300, 300, 300, 0, 210000, 210000, 300);
        checkFeeder(dos3, 0, 0, 0, 0, 0, 310000, 310000, 0);
        checkFeeder(dos4, 0, 40, 40, 40, 0, 410000, 410000, 40);
        checkFeeder(dos4, 1, 50, 50, 50, 0, 420000, 420000, 50);
        checkFeeder(dos5, 0, 60, 60, 60, 0, 510000, 510000, 60);

        // 15
        line.dosersP.forEach(d -> d.feeders.forEach(f -> f.refSetWeight.getValue().setInt(0)));
        execute();
        check1(STATE_DELAY_TRANSPORT_P, 1, 750, 600, 1000, 600, 60);
        check2(600, 600, 600,    0,     150, 150, 0);
        check3(0, 1, 0, 1, 0, 0);
        checkDoser(dos1, 1, 300, 300, 600, 1, 1, 0, 0, 0, 0, 0);
        checkDoser(dos2, 1, 300, 300, 0, 1, 1, 0, 0, 0, 0, 0);
        checkDoser(dos3, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        checkDoser(dos4, 1, 90, 90, 0, 1, 0, 0, 0, 0, 0, 0);
        checkDoser(dos5, 1, 60, 60, 0, 1, 0, 0, 0, 0, 0, 0);
        checkCtlDoser(1, 0, 0, 0, 0, 0, 0, 0, 0);
        checkFeeder(dos1, 0, 100, 100, 0, 600, 110600, 110000, 0);
        checkFeeder(dos1, 1, 200, 200, 0, 0, 120000, 120000, 0);
        checkFeeder(dos2, 0, 300, 300, 0, 0, 210000, 210000, 0);
        checkFeeder(dos3, 0, 0, 0, 0, 0, 310000, 310000, 0);
        checkFeeder(dos4, 0, 40, 40, 40, 0, 410000, 410000, 40);
        checkFeeder(dos4, 1, 50, 50, 50, 0, 420000, 420000, 50);
        checkFeeder(dos5, 0, 60, 60, 60, 0, 510000, 510000, 60);

        // 16
        execute();
        check1(STATE_WAIT_CRUSHER_EMPTY, 1, 750, 600, 1000, 600, 60);
        check2(600, 600, 600,    0,     150, 150, 0);
        check3(0, 0, 0, 1, 0, 0);
        checkDoser(dos1, 1, 300, 300, 600, 1, 1, 0, 0, 0, 0, 0);
        checkDoser(dos2, 1, 300, 300, 0, 1, 1, 0, 0, 0, 0, 0);
        checkDoser(dos3, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        checkDoser(dos4, 1, 90, 90, 0, 1, 0, 0, 0, 0, 0, 0);
        checkDoser(dos5, 1, 60, 60, 0, 1, 0, 0, 0, 0, 0, 0);
        checkCtlDoser(1, 0, 0, 0, 0, 0, 0, 0, 0);
        checkFeeder(dos1, 0, 100, 100, 0, 600, 110600, 110000, 0);
        checkFeeder(dos1, 1, 200, 200, 0, 0, 120000, 120000, 0);
        checkFeeder(dos2, 0, 300, 300, 0, 0, 210000, 210000, 0);
        checkFeeder(dos3, 0, 0, 0, 0, 0, 310000, 310000, 0);
        checkFeeder(dos4, 0, 40, 40, 40, 0, 410000, 410000, 40);
        checkFeeder(dos4, 1, 50, 50, 50, 0, 420000, 420000, 50);
        checkFeeder(dos5, 0, 60, 60, 60, 0, 510000, 510000, 60);

        // 17
        line.tagInpCrusherEmpty.setOn();
        execute();
        check1(STATE_LOADING_C, 1, 750, 600, 1000, 600, 60);
        check2(600, 600, 600,    0,     150, 150, 0);
        check3(0, 0, 0, 0, 0, 0);
        checkDoser(dos1, 1, 300, 300, 600, 1, 1, 0, 0, 0, 0, 0);
        checkDoser(dos2, 1, 300, 300, 0, 1, 1, 0, 0, 0, 0, 0);
        checkDoser(dos3, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        checkDoser(dos4, 1, 90, 90, 0, 1, 0, 0, 0, 0, 0, 0);
        checkDoser(dos5, 1, 60, 60, 0, 1, 0, 0, 0, 0, 0, 0);
        checkCtlDoser(1, 0, 0, 0, 0, 0, 0, 0, 0);
        checkFeeder(dos1, 0, 100, 100, 0, 600, 110600, 110000, 0);
        checkFeeder(dos1, 1, 200, 200, 0, 0, 120000, 120000, 0);
        checkFeeder(dos2, 0, 300, 300, 0, 0, 210000, 210000, 0);
        checkFeeder(dos3, 0, 0, 0, 0, 0, 310000, 310000, 0);
        checkFeeder(dos4, 0, 40, 40, 40, 0, 410000, 410000, 40);
        checkFeeder(dos4, 1, 50, 50, 50, 0, 420000, 420000, 50);
        checkFeeder(dos5, 0, 60, 60, 60, 0, 510000, 510000, 60);

        // 18
        line.tagUnderweightLimitC.setLong(20);
        execute();
        check1(STATE_LOADING_C, 1, 750, 600, 1000, 600, 60);
        check2(600, 600, 600,    0,     150, 150, 0);
        check3(0, 0, 0, 0, 0, 0);
        checkDoser(dos1, 1, 300, 300, 600, 1, 1, 0, 0, 0, 0, 0);
        checkDoser(dos2, 1, 300, 300, 0, 1, 1, 0, 0, 0, 0, 0);
        checkDoser(dos3, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        checkDoser(dos4, 1, 90, 90, 0, 1, 0, 0, 0, 0, 0, 0);
        checkDoser(dos5, 1, 60, 60, 0, 1, 0, 0, 0, 0, 0, 0);
        checkCtlDoser(1, 0, 0, 0, 0, 0, 0, 0, 0);
        checkFeeder(dos1, 0, 100, 100, 0, 600, 110600, 110000, 0);
        checkFeeder(dos1, 1, 200, 200, 0, 0, 120000, 120000, 0);
        checkFeeder(dos2, 0, 300, 300, 0, 0, 210000, 210000, 0);
        checkFeeder(dos3, 0, 0, 0, 0, 0, 310000, 310000, 0);
        checkFeeder(dos4, 0, 40, 40, 40, 0, 410000, 410000, 40);
        checkFeeder(dos4, 1, 50, 50, 50, 0, 420000, 420000, 50);
        checkFeeder(dos5, 0, 60, 60, 60, 0, 510000, 510000, 60);

        // 19
//        setDoserSumWeights(110600, 120000, 130000, 210000, 310000, 410000, 420000, 510000);
        execute();
        check1(STATE_LOADING_C, 1, 750, 600, 1000, 600, 60);
        check2(600, 600, 600,    0,     150, 150, 0);
        check3(0, 0, 0, 0, 0, 0);
        checkDoser(dos1, 1, 300, 300, 600, 1, 1, 0, 0, 0, 0, 0);
        checkDoser(dos2, 1, 300, 300, 0, 1, 1, 0, 0, 0, 0, 0);
        checkDoser(dos3, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        checkDoser(dos4, 1, 90, 90, 0, 1, 0, 0, 0, 0, 0, 0);
        checkDoser(dos5, 1, 60, 60, 0, 1, 0, 0, 0, 0, 0, 0);
        checkCtlDoser(1, 0, 0, 0, 0, 0, 0, 0, 0);
        checkFeeder(dos1, 0, 100, 100, 0, 600, 110600, 110000, 0);
        checkFeeder(dos1, 1, 200, 200, 0, 0, 120000, 120000, 0);
        checkFeeder(dos2, 0, 300, 300, 0, 0, 210000, 210000, 0);
        checkFeeder(dos3, 0, 0, 0, 0, 0, 310000, 310000, 0);
        checkFeeder(dos4, 0, 40, 40, 40, 0, 410000, 410000, 40);
        checkFeeder(dos4, 1, 50, 50, 50, 0, 420000, 420000, 50);
        checkFeeder(dos5, 0, 60, 60, 60, 0, 510000, 510000, 60);

        // 20
        dosC.refStable.getTag().setOn();
        execute();
        check1(STATE_LOADING_C, 1, 750, 600, 1000, 600, 60);
        check2(600, 600, 600,    0,     150, 150, 0);
        check3(0, 0, 0, 0, 0, 0);
        checkDoser(dos1, 1, 300, 300, 600, 1, 1, 0, 0, 0, 0, 0);
        checkDoser(dos2, 1, 300, 300, 0, 1, 1, 0, 0, 0, 0, 0);
        checkDoser(dos3, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        checkDoser(dos4, 1, 90, 90, 0, 1, 0, 0, 0, 0, 0, 0);
        checkDoser(dos5, 1, 60, 60, 0, 1, 0, 0, 0, 0, 0, 0);
        checkCtlDoser(1, 1, 0, 0, 0, 0, 0, 0, 0);
        checkFeeder(dos1, 0, 100, 100, 0, 600, 110600, 110000, 0);
        checkFeeder(dos1, 1, 200, 200, 0, 0, 120000, 120000, 0);
        checkFeeder(dos2, 0, 300, 300, 0, 0, 210000, 210000, 0);
        checkFeeder(dos3, 0, 0, 0, 0, 0, 310000, 310000, 0);
        checkFeeder(dos4, 0, 40, 40, 40, 0, 410000, 410000, 40);
        checkFeeder(dos4, 1, 50, 50, 50, 0, 420000, 420000, 50);
        checkFeeder(dos5, 0, 60, 60, 60, 0, 510000, 510000, 60);

        // 21
        dosC.refCurWeight.getTag().setLong(579);
        execute();
        check1(STATE_LOADING_C, 1, 750, 600, 1000, 600, 60);
        check2(600, 600, 600,    0,     150, 150, 0);
        check3(0, 0, 0, 0, 0, 0);
        checkDoser(dos1, 1, 300, 300, 600, 1, 1, 0, 0, 0, 0, 0);
        checkDoser(dos2, 1, 300, 300, 0, 1, 1, 0, 0, 0, 0, 0);
        checkDoser(dos3, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        checkDoser(dos4, 1, 90, 90, 0, 1, 0, 0, 0, 0, 0, 0);
        checkDoser(dos5, 1, 60, 60, 0, 1, 0, 0, 0, 0, 0, 0);
        checkCtlDoser(1, 1, 579, 0, 0, 0, 0, 0, 0);
        checkFeeder(dos1, 0, 100, 100, 0, 600, 110600, 110000, 0);
        checkFeeder(dos1, 1, 200, 200, 0, 0, 120000, 120000, 0);
        checkFeeder(dos2, 0, 300, 300, 0, 0, 210000, 210000, 0);
        checkFeeder(dos3, 0, 0, 0, 0, 0, 310000, 310000, 0);
        checkFeeder(dos4, 0, 40, 40, 40, 0, 410000, 410000, 40);
        checkFeeder(dos4, 1, 50, 50, 50, 0, 420000, 420000, 50);
        checkFeeder(dos5, 0, 60, 60, 60, 0, 510000, 510000, 60);

        // 22
        dosC.refCurWeight.getTag().setLong(580);
        execute();
        check1(STATE_STOP_LOAD_C, 1, 750, 600, 1000, 600, 60);
        check2(600, 600, 600,    0,     150, 150, 0);
        check3(0, 0, 0, 0, 0, 0);
        checkDoser(dos1, 1, 300, 300, 600, 1, 1, 0, 0, 0, 0, 0);
        checkDoser(dos2, 1, 300, 300, 0, 1, 1, 0, 0, 0, 0, 0);
        checkDoser(dos3, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        checkDoser(dos4, 1, 90, 90, 0, 1, 0, 0, 0, 0, 0, 0);
        checkDoser(dos5, 1, 60, 60, 0, 1, 0, 0, 0, 0, 0, 0);
        checkCtlDoser(1, 1, 580, 0, 0, 0, 0, 0, 1);
        checkFeeder(dos1, 0, 100, 100, 0, 600, 110600, 110000, 0);
        checkFeeder(dos1, 1, 200, 200, 0, 0, 120000, 120000, 0);
        checkFeeder(dos2, 0, 300, 300, 0, 0, 210000, 210000, 0);
        checkFeeder(dos3, 0, 0, 0, 0, 0, 310000, 310000, 0);
        checkFeeder(dos4, 0, 40, 40, 40, 0, 410000, 410000, 40);
        checkFeeder(dos4, 1, 50, 50, 50, 0, 420000, 420000, 50);
        checkFeeder(dos5, 0, 60, 60, 60, 0, 510000, 510000, 60);

        // 23
        dosC.refStable.getTag().setOff();
        dosC.refStatus.getTag().setInt(PaGeliosDozkkmcModule.STATUS_UNLOAD_READY);
        execute();
        check1(STATE_STOP_LOAD_C, 1, 750, 600, 1000, 600, 60);
        check2(600, 600, 600,    0,     150, 150, 0);
        check3(0, 0, 0, 0, 0, 0);
        checkDoser(dos1, 1, 300, 300, 600, 1, 1, 0, 0, 0, 0, 0);
        checkDoser(dos2, 1, 300, 300, 0, 1, 1, 0, 0, 0, 0, 0);
        checkDoser(dos3, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        checkDoser(dos4, 1, 90, 90, 0, 1, 0, 0, 0, 0, 0, 0);
        checkDoser(dos5, 1, 60, 60, 0, 1, 0, 0, 0, 0, 0, 0);
        checkCtlDoser(1, 0, 580, 0, 0, 0, 0, 0, 1);
        checkFeeder(dos1, 0, 100, 100, 0, 600, 110600, 110000, 0);
        checkFeeder(dos1, 1, 200, 200, 0, 0, 120000, 120000, 0);
        checkFeeder(dos2, 0, 300, 300, 0, 0, 210000, 210000, 0);
        checkFeeder(dos3, 0, 0, 0, 0, 0, 310000, 310000, 0);
        checkFeeder(dos4, 0, 40, 40, 40, 0, 410000, 410000, 40);
        checkFeeder(dos4, 1, 50, 50, 50, 0, 420000, 420000, 50);
        checkFeeder(dos5, 0, 60, 60, 60, 0, 510000, 510000, 60);

        // 24
        dosC.refStable.getTag().setOn();
        execute();
        check1(STATE_PREPARING_M, 1, 750, 600, 1000, 600, 60);
        check2(600, 600, 600,    580,     150, 145, 0);
        check3(0, 0, 0, 0, 0, 0);
        checkDoser(dos1, 1, 300, 300, 600, 1, 1, 0, 0, 0, 0, 0);
        checkDoser(dos2, 1, 300, 300, 0, 1, 1, 0, 0, 0, 0, 0);
        checkDoser(dos3, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        checkDoser(dos4, 1, 90, 87, 0, 1, 0, 0, 1, 0, 0, 0);
        checkDoser(dos5, 1, 60, 58, 0, 1, 0, 0, 1, 0, 0, 0);
        checkCtlDoser(1, 1, 580, 0, 0, 0, 0, 0, 0);
        checkFeeder(dos1, 0, 100, 100, 0, 600, 110600, 110000, 0);
        checkFeeder(dos1, 1, 200, 200, 0, 0, 120000, 120000, 0);
        checkFeeder(dos2, 0, 300, 300, 0, 0, 210000, 210000, 0);
        checkFeeder(dos3, 0, 0, 0, 0, 0, 310000, 310000, 0);
        checkFeeder(dos4, 0, 40, 39, 39, 0, 410000, 410000, 40);
        checkFeeder(dos4, 1, 50, 48, 48, 0, 420000, 420000, 50);
        checkFeeder(dos5, 0, 60, 58, 58, 0, 510000, 510000, 60);

        // 25
        setDoserStatus(dos4, PaGeliosDozkkmcModule.STATUS_TASK_READY);
        execute();
        check1(STATE_PREPARING_M, 1, 750, 600, 1000, 600, 60);
        check2(600, 600, 600,    580,     150, 145, 0);
        check3(0, 0, 0, 0, 0, 0);
        checkDoser(dos1, 1, 300, 300, 600, 1, 1, 0, 0, 0, 0, 0);
        checkDoser(dos2, 1, 300, 300, 0, 1, 1, 0, 0, 0, 0, 0);
        checkDoser(dos3, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        checkDoser(dos4, 1, 90, 87, 0, 1, 0, 0, 1, 0, 0, 0);
        checkDoser(dos5, 1, 60, 58, 0, 1, 0, 0, 1, 0, 0, 0);
        checkCtlDoser(1, 1, 580, 0, 0, 0, 0, 0, 0);
        checkFeeder(dos1, 0, 100, 100, 0, 600, 110600, 110000, 0);
        checkFeeder(dos1, 1, 200, 200, 0, 0, 120000, 120000, 0);
        checkFeeder(dos2, 0, 300, 300, 0, 0, 210000, 210000, 0);
        checkFeeder(dos3, 0, 0, 0, 0, 0, 310000, 310000, 0);
        checkFeeder(dos4, 0, 40, 39, 39, 0, 410000, 410000, 39);
        checkFeeder(dos4, 1, 50, 48, 48, 0, 420000, 420000, 48);
        checkFeeder(dos5, 0, 60, 58, 58, 0, 510000, 510000, 58);

        // 26
        setDoserStatus(dos5, PaGeliosDozkkmcModule.STATUS_TASK_READY);
        execute();
        check1(STATE_CAN_LOAD_M, 1, 750, 600, 1000, 600, 60);
        check2(600, 600, 600,    580,     150, 145, 0);
        check3(0, 0, 0, 0, 0, 0);
        checkDoser(dos1, 1, 300, 300, 600, 1, 1, 0, 0, 0, 0, 0);
        checkDoser(dos2, 1, 300, 300, 0, 1, 1, 0, 0, 0, 0, 0);
        checkDoser(dos3, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        checkDoser(dos4, 1, 90, 87, 0, 1, 0, 1, 1, 0, 0, 0);
        checkDoser(dos5, 1, 60, 58, 0, 1, 0, 1, 1, 0, 0, 0);
        checkCtlDoser(1, 1, 580, 0, 0, 0, 0, 0, 0);
        checkFeeder(dos1, 0, 100, 100, 0, 600, 110600, 110000, 0);
        checkFeeder(dos1, 1, 200, 200, 0, 0, 120000, 120000, 0);
        checkFeeder(dos2, 0, 300, 300, 0, 0, 210000, 210000, 0);
        checkFeeder(dos3, 0, 0, 0, 0, 0, 310000, 310000, 0);
        checkFeeder(dos4, 0, 40, 39, 39, 0, 410000, 410000, 39);
        checkFeeder(dos4, 1, 50, 48, 48, 0, 420000, 420000, 48);
        checkFeeder(dos5, 0, 60, 58, 58, 0, 510000, 510000, 58);

        // 27
        line.tagEnableLoad.setOn();
        line.tagEnableLoadM.setOff();
        execute();
        check1(STATE_CAN_LOAD_M, 1, 750, 600, 1000, 600, 60);
        check2(600, 600, 600,    580,     150, 145, 0);
        check3(0, 0, 0, 0, 0, 0);
        checkDoser(dos1, 1, 300, 300, 600, 1, 1, 0, 0, 0, 0, 0);
        checkDoser(dos2, 1, 300, 300, 0, 1, 1, 0, 0, 0, 0, 0);
        checkDoser(dos3, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        checkDoser(dos4, 1, 90, 87, 0, 1, 0, 0, 0, 0, 0, 0);
        checkDoser(dos5, 1, 60, 58, 0, 1, 0, 0, 0, 0, 0, 0);
        checkCtlDoser(1, 1, 580, 0, 0, 0, 0, 0, 0);
        checkFeeder(dos1, 0, 100, 100, 0, 600, 110600, 110000, 0);
        checkFeeder(dos1, 1, 200, 200, 0, 0, 120000, 120000, 0);
        checkFeeder(dos2, 0, 300, 300, 0, 0, 210000, 210000, 0);
        checkFeeder(dos3, 0, 0, 0, 0, 0, 310000, 310000, 0);
        checkFeeder(dos4, 0, 40, 39, 39, 0, 410000, 410000, 39);
        checkFeeder(dos4, 1, 50, 48, 48, 0, 420000, 420000, 48);
        checkFeeder(dos5, 0, 60, 58, 58, 0, 510000, 510000, 58);

        // 28
        line.tagEnableLoadM.setOn();
        execute();
        check1(STATE_LOADING_M, 1, 750, 600, 1000, 600, 60);
        check2(600, 600, 600,    580,     150, 145, 0);
        check3(0, 0, 0, 0, 0, 0);
        checkDoser(dos1, 1, 300, 300, 600, 1, 1, 0, 0, 0, 0, 0);
        checkDoser(dos2, 1, 300, 300, 0, 1, 1, 0, 0, 0, 0, 0);
        checkDoser(dos3, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        checkDoser(dos4, 1, 90, 87, 0, 1, 0, 0, 0, 1, 1, 0);
        checkDoser(dos5, 1, 60, 58, 0, 1, 0, 0, 0, 1, 1, 0);
        checkCtlDoser(1, 1, 580, 0, 0, 0, 0, 0, 0);
        checkFeeder(dos1, 0, 100, 100, 0, 600, 110600, 110000, 0);
        checkFeeder(dos1, 1, 200, 200, 0, 0, 120000, 120000, 0);
        checkFeeder(dos2, 0, 300, 300, 0, 0, 210000, 210000, 0);
        checkFeeder(dos3, 0, 0, 0, 0, 0, 310000, 310000, 0);
        checkFeeder(dos4, 0, 40, 39, 39, 0, 410000, 410000, 39);
        checkFeeder(dos4, 1, 50, 48, 48, 0, 420000, 420000, 48);
        checkFeeder(dos5, 0, 60, 58, 58, 0, 510000, 510000, 58);

        // 29
        setDoserStatus(dos4, PaGeliosDozkkmcModule.STATUS_UNLOAD_READY);
        line.tagInpMixerBusy.setOn();
        execute();
        check1(STATE_LOADING_M, 1, 750, 600, 1000, 600, 60);
        check2(600, 600, 600,    580,     150, 145, 0);
        check3(0, 0, 0, 0, 0, 0);
        checkDoser(dos1, 1, 300, 300, 600, 1, 1, 0, 0, 0, 0, 0);
        checkDoser(dos2, 1, 300, 300, 0, 1, 1, 0, 0, 0, 0, 0);
        checkDoser(dos3, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        checkDoser(dos4, 1, 90, 87, 0, 1, 0, 0, 0, 1, 1, 0);
        checkDoser(dos5, 1, 60, 58, 0, 1, 0, 0, 0, 1, 1, 0);
        checkCtlDoser(1, 1, 580, 0, 0, 0, 0, 0, 0);
        checkFeeder(dos1, 0, 100, 100, 0, 600, 110600, 110000, 0);
        checkFeeder(dos1, 1, 200, 200, 0, 0, 120000, 120000, 0);
        checkFeeder(dos2, 0, 300, 300, 0, 0, 210000, 210000, 0);
        checkFeeder(dos3, 0, 0, 0, 0, 0, 310000, 310000, 0);
        checkFeeder(dos4, 0, 40, 39, 39, 0, 410000, 410000, 39);
        checkFeeder(dos4, 1, 50, 48, 48, 0, 420000, 420000, 48);
        checkFeeder(dos5, 0, 60, 58, 58, 0, 510000, 510000, 58);

        // 30
        setDoserStatus(dos5, PaGeliosDozkkmcModule.STATUS_UNLOAD_READY);
        execute();
        check1(STATE_WAIT_MIXER_READY, 1, 750, 600, 1000, 600, 60);
        check2(600, 600, 600,    580,     150, 145, 0);
        check3(0, 0, 0, 0, 0, 0);
        checkDoser(dos1, 1, 300, 300, 600, 1, 1, 0, 0, 0, 0, 0);
        checkDoser(dos2, 1, 300, 300, 0, 1, 1, 0, 0, 0, 0, 0);
        checkDoser(dos3, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        checkDoser(dos4, 1, 90, 87, 0, 1, 0, 0, 0, 0, 0, 0);
        checkDoser(dos5, 1, 60, 58, 0, 1, 0, 0, 0, 0, 0, 0);
        checkCtlDoser(1, 1, 580, 0, 0, 0, 0, 0, 0);
        checkFeeder(dos1, 0, 100, 100, 0, 600, 110600, 110000, 0);
        checkFeeder(dos1, 1, 200, 200, 0, 0, 120000, 120000, 0);
        checkFeeder(dos2, 0, 300, 300, 0, 0, 210000, 210000, 0);
        checkFeeder(dos3, 0, 0, 0, 0, 0, 310000, 310000, 0);
        checkFeeder(dos4, 0, 40, 39, 39, 0, 410000, 410000, 39);
        checkFeeder(dos4, 1, 50, 48, 48, 0, 420000, 420000, 48);
        checkFeeder(dos5, 0, 60, 58, 58, 0, 510000, 510000, 58);


        // 31
        line.tagInpMixerBusy.setOff();
        execute();
        check1(STATE_MIXER_START, 1, 750, 600, 1000, 600, 60);
        check2(600, 600, 600,    580,     150, 145, 0);
        check3(0, 0, 0, 0, 1, 0);
        checkDoser(dos1, 1, 300, 300, 600, 1, 1, 0, 0, 0, 0, 0);
        checkDoser(dos2, 1, 300, 300, 0, 1, 1, 0, 0, 0, 0, 0);
        checkDoser(dos3, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        checkDoser(dos4, 1, 90, 87, 0, 1, 0, 0, 0, 0, 0, 0);
        checkDoser(dos5, 1, 60, 58, 0, 1, 0, 0, 0, 0, 0, 0);
        checkCtlDoser(1, 1, 580, 0, 0, 0, 0, 0, 0);
        checkFeeder(dos1, 0, 100, 100, 0, 600, 110600, 110000, 0);
        checkFeeder(dos1, 1, 200, 200, 0, 0, 120000, 120000, 0);
        checkFeeder(dos2, 0, 300, 300, 0, 0, 210000, 210000, 0);
        checkFeeder(dos3, 0, 0, 0, 0, 0, 310000, 310000, 0);
        checkFeeder(dos4, 0, 40, 39, 39, 0, 410000, 410000, 39);
        checkFeeder(dos4, 1, 50, 48, 48, 0, 420000, 420000, 48);
        checkFeeder(dos5, 0, 60, 58, 58, 0, 510000, 510000, 58);


        // 32
        line.tagInpMixerLoadReady.setOn();
        execute();
        check1(STATE_UNLOADING_M, 1, 750, 600, 1000, 600, 60);
        check2(600, 600, 600,    580,     150, 145, 0);
        check3(0, 0, 0, 0, 1, 0);
        checkDoser(dos1, 1, 300, 300, 600, 1, 1, 0, 0, 0, 0, 0);
        checkDoser(dos2, 1, 300, 300, 0, 1, 1, 0, 0, 0, 0, 0);
        checkDoser(dos3, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        checkDoser(dos4, 1, 90, 87, 0, 1, 0, 0, 0, 0, 0, 0);
        checkDoser(dos5, 1, 60, 58, 0, 1, 0, 0, 0, 0, 0, 0);
        checkCtlDoser(1, 1, 580, 0, 0, 0, 0, 0, 0);
        checkFeeder(dos1, 0, 100, 100, 0, 600, 110600, 110000, 0);
        checkFeeder(dos1, 1, 200, 200, 0, 0, 120000, 120000, 0);
        checkFeeder(dos2, 0, 300, 300, 0, 0, 210000, 210000, 0);
        checkFeeder(dos3, 0, 0, 0, 0, 0, 310000, 310000, 0);
        checkFeeder(dos4, 0, 40, 39, 39, 0, 410000, 410000, 39);
        checkFeeder(dos4, 1, 50, 48, 48, 0, 420000, 420000, 48);
        checkFeeder(dos5, 0, 60, 58, 58, 0, 510000, 510000, 58);


        // 33
        line.tagEnableUnloadM.setOn();
        execute();
        check1(STATE_UNLOADING_M, 1, 750, 600, 1000, 600, 60);
        check2(600, 600, 600,    580,     150, 145, 0);
        check3(0, 0, 0, 0, 1, 0);
        checkDoser(dos1, 1, 300, 300, 600, 1, 1, 0, 0, 0, 0, 0);
        checkDoser(dos2, 1, 300, 300, 0, 1, 1, 0, 0, 0, 0, 0);
        checkDoser(dos3, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        checkDoser(dos4, 1, 90, 87, 0, 1, 0, 0, 0, 0, 0, 1);
        checkDoser(dos5, 1, 60, 58, 0, 1, 0, 0, 0, 0, 0, 1);
        checkCtlDoser(1, 1, 580, 0, 0, 0, 0, 1, 0);
        checkFeeder(dos1, 0, 100, 100, 0, 600, 110600, 110000, 0);
        checkFeeder(dos1, 1, 200, 200, 0, 0, 120000, 120000, 0);
        checkFeeder(dos2, 0, 300, 300, 0, 0, 210000, 210000, 0);
        checkFeeder(dos3, 0, 0, 0, 0, 0, 310000, 310000, 0);
        checkFeeder(dos4, 0, 40, 39, 39, 0, 410000, 410000, 39);
        checkFeeder(dos4, 1, 50, 48, 48, 0, 420000, 420000, 48);
        checkFeeder(dos5, 0, 60, 58, 58, 0, 510000, 510000, 58);

        // 34
        setDoserStatus(dos4, PaGeliosDozkkmcModule.STATUS_IDLE);
        execute();

        // 35
        setDoserStatus(dos4, PaGeliosDozkkmcModule.STATUS_UNLOAD);
        setDoserStatus(dos5, PaGeliosDozkkmcModule.STATUS_IDLE);
        execute();

        // 36
        setDoserStatus(dos5, PaGeliosDozkkmcModule.STATUS_UNLOAD);
        dosC.refStatus.getTag().setInt(PaGeliosDozkkmcModule.STATUS_IDLE);
        execute();

        // 37
        setDoserStatus(dos4, PaGeliosDozkkmcModule.STATUS_IDLE);
        setDoserStatus(dos5, PaGeliosDozkkmcModule.STATUS_IDLE);
        line.tagTimeAfterUnloadCM.setInt(2);
        execute();
        check1(STATE_DELAY_AFTER_UNLOAD_C_M, 1, 750, 600, 1000, 600, 60);
        check2(600, 600, 600,    580,     150, 145, 0);
        check3(0, 0, 1, 0, 1, 0);
        checkDoser(dos1, 1, 300, 300, 600, 1, 1, 0, 0, 0, 0, 0);
        checkDoser(dos2, 1, 300, 300, 0, 1, 1, 0, 0, 0, 0, 0);
        checkDoser(dos3, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        checkDoser(dos4, 1, 90, 87, 0, 1, 1, 0, 0, 0, 0, 0);
        checkDoser(dos5, 1, 60, 58, 0, 1, 1, 0, 0, 0, 0, 0);
        checkCtlDoser(1, 1, 580, 0, 0, 0, 0, 0, 0);
        checkFeeder(dos1, 0, 100, 100, 0, 600, 110600, 110000, 0);
        checkFeeder(dos1, 1, 200, 200, 0, 0, 120000, 120000, 0);
        checkFeeder(dos2, 0, 300, 300, 0, 0, 210000, 210000, 0);
        checkFeeder(dos3, 0, 0, 0, 0, 0, 310000, 310000, 0);
        checkFeeder(dos4, 0, 40, 39, 39, 0, 410000, 410000, 39);
        checkFeeder(dos4, 1, 50, 48, 48, 0, 420000, 420000, 48);
        checkFeeder(dos5, 0, 60, 58, 58, 0, 510000, 510000, 58);

        // 38
        execute();
        check1(STATE_DELAY_AFTER_UNLOAD_C_M, 1, 750, 600, 1000, 600, 60);
        check2(600, 600, 600,    580,     150, 145, 0);
        check3(0, 0, 0, 0, 1, 0);
        checkDoser(dos1, 1, 300, 300, 600, 1, 1, 0, 0, 0, 0, 0);
        checkDoser(dos2, 1, 300, 300, 0, 1, 1, 0, 0, 0, 0, 0);
        checkDoser(dos3, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        checkDoser(dos4, 1, 90, 87, 0, 1, 1, 0, 0, 0, 0, 0);
        checkDoser(dos5, 1, 60, 58, 0, 1, 1, 0, 0, 0, 0, 0);
        checkCtlDoser(1, 1, 580, 0, 0, 0, 0, 0, 0);
        checkFeeder(dos1, 0, 100, 100, 0, 600, 110600, 110000, 0);
        checkFeeder(dos1, 1, 200, 200, 0, 0, 120000, 120000, 0);
        checkFeeder(dos2, 0, 300, 300, 0, 0, 210000, 210000, 0);
        checkFeeder(dos3, 0, 0, 0, 0, 0, 310000, 310000, 0);
        checkFeeder(dos4, 0, 40, 39, 0, 0, 410000, 410000, 0);
        checkFeeder(dos4, 1, 50, 48, 0, 0, 420000, 420000, 0);
        checkFeeder(dos5, 0, 60, 58, 0, 0, 510000, 510000, 0);

        // 39
        execute();
        check1(STATE_MIXER_LOADED, 1, 750, 600, 1000, 600, 60);
        check2(600, 600, 600,    580,     150, 145, 0);
        check3(0, 0, 0, 0, 1, 1);
        checkDoser(dos1, 1, 300, 300, 600, 1, 1, 0, 0, 0, 0, 0);
        checkDoser(dos2, 1, 300, 300, 0, 1, 1, 0, 0, 0, 0, 0);
        checkDoser(dos3, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        checkDoser(dos4, 1, 90, 87, 0, 1, 1, 0, 0, 0, 0, 0);
        checkDoser(dos5, 1, 60, 58, 0, 1, 1, 0, 0, 0, 0, 0);
        checkCtlDoser(1, 1, 580, 0, 0, 0, 0, 0, 0);
        checkFeeder(dos1, 0, 100, 100, 0, 600, 110600, 110000, 0);
        checkFeeder(dos1, 1, 200, 200, 0, 0, 120000, 120000, 0);
        checkFeeder(dos2, 0, 300, 300, 0, 0, 210000, 210000, 0);
        checkFeeder(dos3, 0, 0, 0, 0, 0, 310000, 310000, 0);
        checkFeeder(dos4, 0, 40, 39, 0, 0, 410000, 410000, 0);
        checkFeeder(dos4, 1, 50, 48, 0, 0, 420000, 420000, 0);
        checkFeeder(dos5, 0, 60, 58, 0, 0, 510000, 510000, 0);


        // 40
        line.tagInpMixerBusy.setOn();
        execute();
        check1(STATE_CYCLE_FINISHED, 1, 750, 600, 1000, 600, 60);
        check2(600, 600, 600,    580,     150, 145, 0);
        check3(0, 0, 0, 0, 0, 0);
        checkDoser(dos1, 1, 300, 300, 600, 1, 1, 0, 0, 0, 0, 0);
        checkDoser(dos2, 1, 300, 300, 0, 1, 1, 0, 0, 0, 0, 0);
        checkDoser(dos3, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        checkDoser(dos4, 1, 90, 87, 0, 1, 1, 0, 0, 0, 0, 0);
        checkDoser(dos5, 1, 60, 58, 0, 1, 1, 0, 0, 0, 0, 0);
        checkCtlDoser(1, 1, 580, 0, 0, 0, 0, 0, 0);
        checkFeeder(dos1, 0, 100, 100, 0, 600, 110600, 110000, 0);
        checkFeeder(dos1, 1, 200, 200, 0, 0, 120000, 120000, 0);
        checkFeeder(dos2, 0, 300, 300, 0, 0, 210000, 210000, 0);
        checkFeeder(dos3, 0, 0, 0, 0, 0, 310000, 310000, 0);
        checkFeeder(dos4, 0, 40, 39, 0, 0, 410000, 410000, 0);
        checkFeeder(dos4, 1, 50, 48, 0, 0, 420000, 420000, 0);
        checkFeeder(dos5, 0, 60, 58, 0, 0, 510000, 510000, 0);


        // 41
        line.tagStart.setOff();
        execute();
        check1(STATE_IDLE, 1, 750, 0, 1000, 600, 60);
        check2(600, 600, 0,    0,     150, 145, 0);
        check3(0, 0, 0, 0, 0, 0);
        checkDoser(dos1, 1, 300, 300, 0, 1, 1, 0, 0, 0, 0, 0);
        checkDoser(dos2, 1, 300, 300, 0, 1, 1, 0, 0, 0, 0, 0);
        checkDoser(dos3, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        checkDoser(dos4, 1, 90, 87, 0, 1, 1, 0, 0, 0, 0, 0);
        checkDoser(dos5, 1, 60, 58, 0, 1, 1, 0, 0, 0, 0, 0);
        checkCtlDoser(1, 1, 580, 0, 0, 0, 0, 0, 0);
        checkFeeder(dos1, 0, 100, 100, 0, 0, 110600, -1, 0);
        checkFeeder(dos1, 1, 200, 200, 0, 0, 120000, -1, 0);
        checkFeeder(dos2, 0, 300, 300, 0, 0, 210000, -1, 0);
        checkFeeder(dos3, 0, 0, 0, 0, 0, 310000, -1, 0);
        checkFeeder(dos4, 0, 40, 39, 0, 0, 410000, -1, 0);
        checkFeeder(dos4, 1, 50, 48, 0, 0, 420000, -1, 0);
        checkFeeder(dos5, 0, 60, 58, 0, 0, 510000, -1, 0);


        // 42
        execute();
        check1(STATE_IDLE, 1, 750, 0, 1000, 600, 60);
        check2(600, 600, 0,    0,     150, 150, 0);
        check3(0, 0, 0, 0, 0, 0);
        checkDoser(dos1, 1, 300, 300, 0, 1, 1, 0, 0, 0, 0, 0);
        checkDoser(dos2, 1, 300, 300, 0, 1, 1, 0, 0, 0, 0, 0);
        checkDoser(dos3, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        checkDoser(dos4, 1, 90, 90, 0, 1, 1, 0, 0, 0, 0, 0);
        checkDoser(dos5, 1, 60, 60, 0, 1, 1, 0, 0, 0, 0, 0);
        checkCtlDoser(1, 1, 580, 0, 0, 0, 0, 0, 0);
        checkFeeder(dos1, 0, 100, 100, 0, 0, 110600, -1, 0);
        checkFeeder(dos1, 1, 200, 200, 0, 0, 120000, -1, 0);
        checkFeeder(dos2, 0, 300, 300, 0, 0, 210000, -1, 0);
        checkFeeder(dos3, 0, 0, 0, 0, 0, 310000, -1, 0);
        checkFeeder(dos4, 0, 40, 40, 0, 0, 410000, -1, 0);
        checkFeeder(dos4, 1, 50, 50, 0, 0, 420000, -1, 0);
        checkFeeder(dos5, 0, 60, 60, 0, 0, 510000, -1, 0);

    }


    @Test
    void twoPassPTest() throws SQLException {
        dos1.refSetWeightMax.getValue().setLong(200);

        line.tagTimeTransportP.setInt(2);
        line.tagEnableLoad.setOn();
        line.tagEnableUnload.setOn();
        line.tagEnableLoadP.setOn();
        line.tagEnableUnloadP.setOn();
        line.tagEnableLoadM.setOn();
        line.tagEnableUnloadM.setOn();

        svc.task = getTask();
        line.tagTaskInstall.setInt(1);
        execute();
        setDoserSumWeights(110000, 120000, 130000, 210000, 310000, 410000, 420000, 510000);
        dosC.refStatus.getTag().setInt(PaGeliosDozkkmcModule.STATUS_TASK_READY);
        line.tagStart.setOn();
        execute();
        line.updateShift(0, 0);
        dosC.refStatus.getTag().setInt(PaGeliosDozkkmcModule.STATUS_LOAD);
        setDoserStatus(dos1, PaGeliosDozkkmcModule.STATUS_TASK_READY);
        setDoserStatus(dos2, PaGeliosDozkkmcModule.STATUS_TASK_READY);
        execute();
        setDoserStatus(dos1, PaGeliosDozkkmcModule.STATUS_UNLOAD_READY);
        setDoserStatus(dos2, PaGeliosDozkkmcModule.STATUS_UNLOAD_READY);
        execute();
        setDoserSumWeights(110600, 120000, 130000, 210000, 310000, 410000, 420000, 510000);
        setDoserStatus(dos1, PaGeliosDozkkmcModule.STATUS_IDLE);
        setDoserStatus(dos2, PaGeliosDozkkmcModule.STATUS_IDLE);

        // 1
        execute();
        check1(STATE_PREPARING_LOOP_P, 1, 750, 600, 1000, 600, 60);
        check3(1, 1, 0, 1, 0, 0);
        checkDoser(dos1, 1, 300, 300, 600, 2, 1, 0, 1, 0, 0, 0);
        checkDoser(dos2, 1, 300, 300, 0, 1, 1, 0, 0, 0, 0, 0);
        checkFeeder(dos1, 0, 100, 100, 50, 600, 110600, 110000, 50);
        checkFeeder(dos1, 1, 200, 200, 100, 0, 120000, 120000, 100);
        checkFeeder(dos2, 0, 300, 300, 300, 0, 210000, 210000, 300);

        // 2
        execute();
        check1(STATE_PREPARING_LOOP_P, 1, 750, 600, 1000, 600, 60);
        check3(1, 1, 0, 1, 0, 0);
        checkDoser(dos1, 1, 300, 300, 600, 2, 1, 0, 1, 0, 0, 0);
        checkDoser(dos2, 1, 300, 300, 0, 1, 1, 0, 0, 0, 0, 0);
        checkFeeder(dos1, 0, 100, 100, 50, 600, 110600, 110000, 50);
        checkFeeder(dos1, 1, 200, 200, 100, 0, 120000, 120000, 100);
        checkFeeder(dos2, 0, 300, 300, 0, 0, 210000, 210000, 0);

        // 3
        setDoserStatus(dos1, PaGeliosDozkkmcModule.STATUS_TASK_READY);
        execute();
        check1(STATE_LOADING_LOOP_P, 1, 750, 600, 1000, 600, 60);
        check3(1, 1, 0, 1, 0, 0);
        checkDoser(dos1, 1, 300, 300, 600, 2, 1, 1, 1, 1, 1, 0);
        checkDoser(dos2, 1, 300, 300, 0, 1, 1, 0, 0, 0, 0, 0);
        checkFeeder(dos1, 0, 100, 100, 50, 600, 110600, 110000, 50);
        checkFeeder(dos1, 1, 200, 200, 100, 0, 120000, 120000, 100);
        checkFeeder(dos2, 0, 300, 300, 0, 0, 210000, 210000, 0);

        // 4
        execute();
        check1(STATE_LOADING_LOOP_P, 1, 750, 600, 1000, 600, 60);
        check3(1, 1, 0, 1, 0, 0);
        checkDoser(dos1, 1, 300, 300, 600, 2, 1, 0, 0, 1, 1, 0);
        checkDoser(dos2, 1, 300, 300, 0, 1, 1, 0, 0, 0, 0, 0);
        checkFeeder(dos1, 0, 100, 100, 50, 600, 110600, 110000, 50);
        checkFeeder(dos1, 1, 200, 200, 100, 0, 120000, 120000, 100);
        checkFeeder(dos2, 0, 300, 300, 0, 0, 210000, 210000, 0);

        // 5
        execute();
        check1(STATE_LOADING_LOOP_P, 1, 750, 600, 1000, 600, 60);
        check3(1, 1, 0, 1, 0, 0);
        checkDoser(dos1, 1, 300, 300, 600, 2, 1, 0, 0, 1, 1, 0);
        checkDoser(dos2, 1, 300, 300, 0, 1, 1, 0, 0, 0, 0, 0);
        checkFeeder(dos1, 0, 100, 100, 50, 600, 110600, 110000, 50);
        checkFeeder(dos1, 1, 200, 200, 100, 0, 120000, 120000, 100);
        checkFeeder(dos2, 0, 300, 300, 0, 0, 210000, 210000, 0);

        // 6
        setDoserStatus(dos1, PaGeliosDozkkmcModule.STATUS_UNLOAD_READY);
        execute();
        check1(STATE_UNLOADING_LOOP_P, 1, 750, 600, 1000, 600, 60);
        check3(1, 1, 0, 1, 0, 0);
        checkDoser(dos1, 1, 300, 300, 600, 2, 1, 0, 0, 0, 0, 1);
        checkDoser(dos2, 1, 300, 300, 0, 1, 1, 0, 0, 0, 0, 0);
        checkFeeder(dos1, 0, 100, 100, 50, 600, 110600, 110000, 50);
        checkFeeder(dos1, 1, 200, 200, 100, 0, 120000, 120000, 100);
        checkFeeder(dos2, 0, 300, 300, 0, 0, 210000, 210000, 0);

        // 7
        setDoserStatus(dos1, PaGeliosDozkkmcModule.STATUS_IDLE);
        execute();
        check1(STATE_DELAY_TRANSPORT_P, 1, 750, 600, 1000, 600, 60);
        check3(0, 1, 1, 1, 0, 0);
        checkDoser(dos1, 1, 300, 300, 600, 2, 2, 0, 0, 0, 0, 0);
        checkDoser(dos2, 1, 300, 300, 0, 1, 1, 0, 0, 0, 0, 0);
        checkFeeder(dos1, 0, 100, 100, 50, 600, 110600, 110000, 50);
        checkFeeder(dos1, 1, 200, 200, 100, 0, 120000, 120000, 100);
        checkFeeder(dos2, 0, 300, 300, 0, 0, 210000, 210000, 0);

    }


    @Test
    void twoPassMTest() throws SQLException {
        dos4.refSetWeightMax.getValue().setLong(80);

        line.tagTimeTransportP.setInt(2);
        line.tagEnableLoad.setOn();
        line.tagEnableUnload.setOn();
        line.tagEnableLoadP.setOn();
        line.tagEnableUnloadP.setOn();
        line.tagEnableLoadM.setOn();
        line.tagEnableUnloadM.setOn();

        svc.task = getTask();
        line.tagTaskInstall.setInt(1);
        execute();
        setDoserSumWeights(110000, 120000, 130000, 210000, 310000, 410000, 420000, 510000);
        dosC.refStatus.getTag().setInt(PaGeliosDozkkmcModule.STATUS_TASK_READY);
        line.tagStart.setOn();
        execute();

        line.updateShift(0, 0);

        dosC.refStatus.getTag().setInt(PaGeliosDozkkmcModule.STATUS_LOAD);
        setDoserStatus(dos1, PaGeliosDozkkmcModule.STATUS_TASK_READY);
        setDoserStatus(dos2, PaGeliosDozkkmcModule.STATUS_TASK_READY);
        execute();
        setDoserStatus(dos1, PaGeliosDozkkmcModule.STATUS_UNLOAD_READY);
        setDoserStatus(dos2, PaGeliosDozkkmcModule.STATUS_UNLOAD_READY);
        execute();
        setDoserSumWeights(110600, 120000, 130000, 210000, 310000, 410000, 420000, 510000);
        setDoserStatus(dos1, PaGeliosDozkkmcModule.STATUS_IDLE);
        setDoserStatus(dos2, PaGeliosDozkkmcModule.STATUS_IDLE);
        setDoserStatus(dos2, PaGeliosDozkkmcModule.STATUS_IDLE);
        line.tagTimeTransportP.setInt(2);
        execute();
        line.dosersP.forEach(d -> d.feeders.forEach(f -> f.refSetWeight.getValue().setInt(0)));
        line.tagInpCrusherEmpty.setOn();
        line.tagInpMixerLoadReady.setOn();
        line.tagUnderweightLimitC.setLong(20);
        dosC.refCurWeight.getTag().setLong(580);
        dosC.refStatus.getTag().setInt(PaGeliosDozkkmcModule.STATUS_UNLOAD_READY);
        dosC.refStable.getTag().setOn();
        execute();
        setDoserStatus(dos4, PaGeliosDozkkmcModule.STATUS_TASK_READY);
        setDoserStatus(dos5, PaGeliosDozkkmcModule.STATUS_TASK_READY);
        execute();
        setDoserStatus(dos4, PaGeliosDozkkmcModule.STATUS_UNLOAD_READY);
        setDoserStatus(dos5, PaGeliosDozkkmcModule.STATUS_UNLOAD_READY);
        execute();
        setDoserStatus(dos4, PaGeliosDozkkmcModule.STATUS_IDLE);
        setDoserStatus(dos5, PaGeliosDozkkmcModule.STATUS_IDLE);
        line.tagTimeAfterUnloadCM.setInt(2);

        execute();
        check1(STATE_PREPARING_LOOP_M, 1, 750, 600, 1000, 600, 60);
        checkDoser(dos4, 1, 90, 87, 0, 2, 1, 0, 1, 0, 0, 0);
        checkDoser(dos5, 1, 60, 58, 0, 1, 1, 0, 0, 0, 0, 0);
        checkCtlDoser(1, 1, 580, 0, 0, 0, 0, 0, 0);
        checkFeeder(dos4, 0, 40, 39, 20, 0, 410000, 410000, 20);
        checkFeeder(dos4, 1, 50, 48, 24, 0, 420000, 420000, 24);
        checkFeeder(dos5, 0, 60, 58, 58, 0, 510000, 510000, 58);

        execute();
        check1(STATE_PREPARING_LOOP_M, 1, 750, 600, 1000, 600, 60);
        checkDoser(dos4, 1, 90, 87, 0, 2, 1, 0, 1, 0, 0, 0);
        checkDoser(dos5, 1, 60, 58, 0, 1, 1, 0, 0, 0, 0, 0);
        checkCtlDoser(1, 1, 580, 0, 0, 0, 0, 0, 0);
        checkFeeder(dos4, 0, 40, 39, 19, 0, 410000, 410000, 19);
        checkFeeder(dos4, 1, 50, 48, 24, 0, 420000, 420000, 24);
        checkFeeder(dos5, 0, 60, 58, 0, 0, 510000, 510000, 0);

        setDoserStatus(dos4, PaGeliosDozkkmcModule.STATUS_TASK_READY);
        execute();
        check1(STATE_LOADING_LOOP_M, 1, 750, 600, 1000, 600, 60);
        checkDoser(dos4, 1, 90, 87, 0, 2, 1, 1, 1, 1, 1, 0);
        checkDoser(dos5, 1, 60, 58, 0, 1, 1, 0, 0, 0, 0, 0);
        checkCtlDoser(1, 1, 580, 0, 0, 0, 0, 0, 0);
        checkFeeder(dos4, 0, 40, 39, 19, 0, 410000, 410000, 19);
        checkFeeder(dos4, 1, 50, 48, 24, 0, 420000, 420000, 24);
        checkFeeder(dos5, 0, 60, 58, 0, 0, 510000, 510000, 0);

        execute();
        check1(STATE_LOADING_LOOP_M, 1, 750, 600, 1000, 600, 60);
        checkDoser(dos4, 1, 90, 87, 0, 2, 1, 0, 0, 1, 1, 0);
        checkDoser(dos5, 1, 60, 58, 0, 1, 1, 0, 0, 0, 0, 0);
        checkCtlDoser(1, 1, 580, 0, 0, 0, 0, 0, 0);
        checkFeeder(dos4, 0, 40, 39, 19, 0, 410000, 410000, 19);
        checkFeeder(dos4, 1, 50, 48, 24, 0, 420000, 420000, 24);
        checkFeeder(dos5, 0, 60, 58, 0, 0, 510000, 510000, 0);

        setDoserStatus(dos4, PaGeliosDozkkmcModule.STATUS_UNLOAD_READY);
        execute();
        check1(STATE_UNLOADING_LOOP_M, 1, 750, 600, 1000, 600, 60);
        checkDoser(dos4, 1, 90, 87, 0, 2, 1, 0, 0, 0, 0, 1);
        checkDoser(dos5, 1, 60, 58, 0, 1, 1, 0, 0, 0, 0, 0);
        checkCtlDoser(1, 1, 580, 0, 0, 0, 0, 1, 0);
        checkFeeder(dos4, 0, 40, 39, 19, 0, 410000, 410000, 19);
        checkFeeder(dos4, 1, 50, 48, 24, 0, 420000, 420000, 24);
        checkFeeder(dos5, 0, 60, 58, 0, 0, 510000, 510000, 0);

        setDoserStatus(dos4, PaGeliosDozkkmcModule.STATUS_IDLE);
        execute();
        check1(STATE_UNLOADING_C, 1, 750, 600, 1000, 600, 60);
        checkDoser(dos4, 1, 90, 87, 0, 2, 2, 0, 0, 0, 0, 0);
        checkDoser(dos5, 1, 60, 58, 0, 1, 1, 0, 0, 0, 0, 0);
        checkCtlDoser(1, 1, 580, 0, 0, 0, 0, 1, 0);
        checkFeeder(dos4, 0, 40, 39, 19, 0, 410000, 410000, 19);
        checkFeeder(dos4, 1, 50, 48, 24, 0, 420000, 420000, 24);
        checkFeeder(dos5, 0, 60, 58, 0, 0, 510000, 510000, 0);

        execute();
        check1(STATE_UNLOADING_C, 1, 750, 600, 1000, 600, 60);
        checkDoser(dos4, 1, 90, 87, 0, 2, 2, 0, 0, 0, 0, 0);
        checkDoser(dos5, 1, 60, 58, 0, 1, 1, 0, 0, 0, 0, 0);
        checkCtlDoser(1, 1, 580, 0, 0, 0, 0, 1, 0);
        checkFeeder(dos4, 0, 40, 39, 0, 0, 410000, 410000, 0);
        checkFeeder(dos4, 1, 50, 48, 0, 0, 420000, 420000, 0);
        checkFeeder(dos5, 0, 60, 58, 0, 0, 510000, 510000, 0);

        dosC.refStatus.getTag().setInt(PaGeliosDozkkmcModule.STATUS_IDLE);
        execute();
        check1(STATE_DELAY_AFTER_UNLOAD_C_M, 1, 750, 600, 1000, 600, 60);
        checkDoser(dos4, 1, 90, 87, 0, 2, 2, 0, 0, 0, 0, 0);
        checkDoser(dos5, 1, 60, 58, 0, 1, 1, 0, 0, 0, 0, 0);
        checkCtlDoser(1, 1, 580, 0, 0, 0, 0, 0, 0);
        checkFeeder(dos4, 0, 40, 39, 0, 0, 410000, 410000, 0);
        checkFeeder(dos4, 1, 50, 48, 0, 0, 420000, 420000, 0);
        checkFeeder(dos5, 0, 60, 58, 0, 0, 510000, 510000, 0);
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    private Task getTask() {
        Task task = new Task(1, 1, "line1", 1, "recipe1", 0, Task.STATUS_READY);
        task.products.add(new Task.Product(1, "prod1", "dos1", 1, 100));
        task.products.add(new Task.Product(2, "prod2", "dos1", 2, 200));
        task.products.add(new Task.Product(3, "prod3", "dos2", 1, 300));
        task.products.add(new Task.Product(4, "prod4", "dos4", 1, 40));
        task.products.add(new Task.Product(5, "prod5", "dos4", 2, 50));
        task.products.add(new Task.Product(5, "prod6", "dos5", 1, 60));
        return task;
    }

    private void setDoserStatus(
            Doser doser,
            int status
    ) {
        doser.refStatus.getTag().setInt(status);
    }

    private void setDoserSumWeights(
            long bunk11,
            long bunk12,
            long bunk13,
            long bunk21,
            long bunk31,
            long bunk41,
            long bunk42,
            long bunk51
            ) {
        refgr1.items.get("SumWeight1").getTag().setLong(bunk11);
        refgr1.items.get("SumWeight2").getTag().setLong(bunk12);
        refgr1.items.get("SumWeight3").getTag().setLong(bunk13);
        refgr2.items.get("SumWeight1").getTag().setLong(bunk21);
        refgr3.items.get("SumWeight1").getTag().setLong(bunk31);
        refgr4.items.get("SumWeight1").getTag().setLong(bunk41);
        refgr4.items.get("SumWeight2").getTag().setLong(bunk42);
        refgr5.items.get("SumWeight1").getTag().setLong(bunk51);
    }

    private void check1(
            int state,
            int cycleId,
            long rcpWeight,
            long exeWeight,
            long setWeightTot,
            long exeWeightShift,
            int progressTot
    ) {
        assertEquals(state          , line.tagState         .getInt(), "state");
        assertEquals(cycleId        , line.tagCycleId       .getInt(), "cycleId");
        assertEquals(rcpWeight      , line.tagRcpWeight     .getLong(),"rcpWeight");
        assertEquals(exeWeight      , line.tagExeWeight     .getLong(),"exeWeight");
        assertEquals(exeWeightShift , line.tagExeWeightShf.getLong(),"exeWeightShift");
    }



    private void check3(
            int tagHoldDosers,
            int tagHoldTransport,
            int tagTimeCnt,
            int tagCmdSeqBeforeLoadC,
            int tagCmdSeqBeforeLoadMixer,
            int tagCmdSeqStartMixing
    ) {
        assertEquals(tagHoldDosers           , line.tagHoldDosers           .getInt(), "tagHoldDosers           ");
        assertEquals(tagHoldTransport        , line.tagHoldTransport        .getInt(), "tagHoldTransport        ");
        assertEquals(tagTimeCnt              , line.tagTimeCnt              .getLong(),"tagTimeCnt              ");
        assertEquals(tagCmdSeqBeforeLoadMixer, line.tagCmdMixerStart.getLong(),"tagCmdSeqBeforeLoadMixer");
        assertEquals(tagCmdSeqStartMixing    , line.tagCmdMixerLoaded.getLong(),"tagCmdSeqStartMixing    ");
    }

    private void check2(
            long tagRcpWeightP,
            long tagSetWeightP,
            long tagExeWeightP,
            long tagExeWeightC,
            long tagRcpWeightM,
            long tagSetWeightM,
            long tagExeWeightM
    ) {
        assertEquals(tagRcpWeightP, line.tagRcpWeightP.getLong(), "tagRcpWeightP");
        assertEquals(tagSetWeightP, line.tagSetWeightP.getLong(), "tagSetWeightP");
        assertEquals(tagExeWeightP, line.tagExeWeightP.getLong(),"tagExeWeightP");
        assertEquals(tagExeWeightC, line.tagExeWeightC.getLong(),"tagExeWeightC");
        assertEquals(tagRcpWeightM, line.tagRcpWeightM.getLong(),"tagRcpWeightM");
        assertEquals(tagSetWeightM, line.tagSetWeightM.getLong(),"tagSetWeightM");
        assertEquals(tagExeWeightM, line.tagExeWeightM.getLong(), "tagExeWeightM");
    }


    private void checkDoser(
            Doser doser,
            int tagConnected,
            long tagRcpWeight,
            long tagSetWeight,
            long tagExeWeight,
            int tagPassQty,
            int tagPassCnt,
            int refSendTask,
            int refCmdStartCycle,
            int refCmdStartTask,
            int refCmdStartLoad,
            int refCmdStartUnload
    ) {
        assertEquals(tagRcpWeight,      doser.tagRcpWeight  .getLong(), "tagRcpWeight");
        assertEquals(tagSetWeight,      doser.tagSetWeight  .getLong(), "tagSetWeight");
        assertEquals(tagExeWeight,      doser.tagExeWeight  .getLong(), "tagExeWeight");
        assertEquals(tagConnected,      doser.tagConnected  .getInt(), "tagConnected");
        assertEquals(tagPassQty,        doser.tagPassQty    .getInt(), "tagPassQty");
        assertEquals(tagPassCnt,        doser.tagPassCnt    .getInt(), "tagPassCnt");
        assertEquals(refSendTask,       doser.refSendTask       .getValue().getInt(), "refSendTask");
        assertEquals(refCmdStartCycle,  doser.refCmdStartCycle  .getValue().getInt(), "refCmdStartCycle");
        assertEquals(refCmdStartTask,   doser.refCmdStartTask   .getValue().getInt(), "refCmdStartTask");
        assertEquals(refCmdStartLoad,   doser.refCmdStartLoad   .getValue().getInt(), "refCmdStartLoad");
        assertEquals(refCmdStartUnload, doser.refCmdStartUnload .getValue().getInt(), "refCmdStartUnload");
    }

    private void checkCtlDoser(
            int tagConnected,
            int tagStable,
            long tagCurWeight,
            int refSendTask,
            int refCmdStartCycle,
            int refCmdStartTask,
            int refCmdStartLoad,
            int refCmdStartUnload,
            int refCmdStopLoad

    ) {
        assertEquals(tagConnected,      dosC.tagConnected  .getInt(), "tagConnected");
        assertEquals(tagStable,         dosC.tagStable     .getInt(), "tagStable");
        assertEquals(tagCurWeight,      dosC.tagCurWeight  .getInt(), "tagCurWeight");
        assertEquals(refSendTask,       dosC.refSendTask       .getValue().getInt(), "refSendTask");
        assertEquals(refCmdStartCycle,  dosC.refCmdStartCycle  .getValue().getInt(), "refCmdStartCycle");
        assertEquals(refCmdStartTask,   dosC.refCmdStartTask   .getValue().getInt(), "refCmdStartTask");
        assertEquals(refCmdStartLoad,   dosC.refCmdStartLoad   .getValue().getInt(), "refCmdStartLoad");
        assertEquals(refCmdStartUnload, dosC.refCmdStartUnload .getValue().getInt(), "refCmdStartUnload");
        assertEquals(refCmdStopLoad,    dosC.refCmdStopLoad    .getValue().getInt(), "refCmdStopLoad");
    }

    private void checkFeeder(
            Doser doser,
            int num,
            long tagRcpWeight,
            long tagSetWeight,
            long tagSetWeightPass,
            long tagExeWeight,
            long tagSumWeight,
            long tagSumWeightBeg,
            long refSetWeight
    ) {
        Feeder feeder = doser.feeders.get(num);
        assertEquals(tagRcpWeight,      feeder.tagRcpWeight.getLong(), "tagRcpWeight");
        assertEquals(tagSetWeight,      feeder.tagSetWeight.getLong(), "tagSetWeight");
        assertEquals(tagSetWeightPass,  feeder.tagSetWeightPass.getLong(), "tagSetWeightPass");
        assertEquals(tagExeWeight,      feeder.tagExeWeight.getLong(), "tagExeWeight");
        assertEquals(tagSumWeight,      feeder.tagSumWeight.getLong(), "tagSumWeight");
        assertEquals(tagSumWeightBeg,   feeder.tagSumWeightBeg.getLong(), "tagSumWeightBeg");
        assertEquals(refSetWeight,      feeder.refSetWeight.getValue().getLong(), "refSetWeight");
    }


    private void clearCmds() {
        Arrays.asList(
                line.tagCmdMixerStart,
                line.tagCmdMixerLoaded
        ).forEach(t -> t.setReadValInt(0));

        Arrays.asList( dos1, dos2, dos3, dos4, dos5 ).forEach(d ->
                Arrays.asList(
                        d.refSendTask,
                        d.refCmdStartCycle,
                        d.refCmdStartTask,
                        d.refCmdStartLoad,
                        d.refCmdStartUnload
                ).forEach(r -> r.getValue().setInt(0)));

        Arrays.asList(
                dosC.refSendTask,
                dosC.refCmdStartCycle,
                dosC.refCmdStartTask,
                dosC.refCmdStartLoad,
                dosC.refCmdStartUnload,
                dosC.refCmdStopLoad
        ).forEach(r -> r.getValue().setInt(0));

    }

    private void execute() throws SQLException {
        clearCmds();
        line.execute();
    }

}