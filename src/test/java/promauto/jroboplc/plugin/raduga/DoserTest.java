package promauto.jroboplc.plugin.raduga;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import promauto.jroboplc.core.ConfigurationYaml;
import promauto.jroboplc.core.TagTable;
import promauto.jroboplc.core.api.EnvironmentInst;
import promauto.jroboplc.core.api.SampleEnvironment;
import promauto.jroboplc.core.api.Tag;
import promauto.jroboplc.core.tags.*;
import promauto.jroboplc.plugin.peripherial.PaGeliosDozkkmcModule;

import java.sql.SQLException;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static promauto.jroboplc.plugin.raduga.Doser.*;

class DoserTest {
    Doser doser;
    LineDummy line;
    TaskRepo tr;
    Context ctx;
    DataServiceDummy svc;
    SampleEnvironment env;

    Feeder feeder0, feeder1, feeder2;

    RefGroupDummy refgr;

    @BeforeEach
    void setUp() {
        env = new SampleEnvironment();
        EnvironmentInst.set(env);
        env.setConfiguration(new ConfigurationYaml(""));

        svc = new DataServiceDummy();

        ctx = new Context();
        ctx.setEnvironment(env);
        ctx.setDataService(svc);
        ctx.tagtable = new TagTable();
//        ctx.setTaskrepo(new TaskRepo(ctx));

        line = new LineDummy();

        Map<Object,Object> conf = new HashMap<>();
        conf.put("name", "dos1");
        conf.put("module", "mod1");
        conf.put("srcBunkers", new ArrayList<>(Arrays.asList("bunk1", "bunk2", "bunk3")));

        doser = new Doser(ctx, line, 1);
        doser.load(conf);
        feeder0 = doser.feeders.get(0);
        feeder1 = doser.feeders.get(1);
        feeder2 = doser.feeders.get(2);

        refgr = (RefGroupDummy) doser.refgr;
        DoserDummy.changeTagTypes(doser);

//        refgr.items.values().stream()
//                .filter(item -> item.tagname.contains("Weight"))
//                .forEach( RefItemDummy::changeTagTypeLong );
//
//        refgr.items.get("SYSTEM.ErrorFlag").changeTagType(Tag.Type.BOOL);


//        refgr.items.values().stream()
//                .forEach( item -> System.out.println(item.tagname + "  -  " + item.tag.getType()) );

    }


    @Test
    void initTest() throws SQLException {
        doser.init();
        assertEquals(0, doser.getId());
        assertEquals(3, doser.feeders.size());
        assertEquals(1, doser.feeders.get(0).getId());
        assertEquals(2, doser.feeders.get(1).getId());
        assertEquals(3, doser.feeders.get(2).getId());
    }

    @Test
    void executeTest() throws SQLException {
        // 1
        set(1, 123, 0, PaGeliosDozkkmcModule.STATUS_IDLE );
        setFeeder(0, 111, 0);
        setFeeder(1, 222, 0);
        setFeeder(2, 333, 0);
        doser.execute();
        check(1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        checkFeeder(0, 0, 0, 0, 0, 111, -1, 0);
        checkFeeder(1, 0, 0, 0, 0, 222, -1, 0);
        checkFeeder(2, 0, 0, 0, 0, 333, -1, 0);

        // 2 - no connection
        doser.refErrorFlag.getValue().setBool(true);
        feeder0.refSumWeight.getValue().setLong(112);
        doser.execute();
        assertFalse(doser.tagConnected.getBool());
        assertEquals(111, feeder0.tagSumWeight.getLong());
        assertEquals(ERROR_NOT_CONNECTED, doser.tagError.getString());

        // 3 - bad crc
        refgr.crcOk = false;
        doser.refErrorFlag.getValue().setBool(false);
        feeder0.refSumWeight.getValue().setLong(113);
        doser.execute();
        assertFalse(doser.tagConnected.getBool());
        assertEquals(111, feeder0.tagSumWeight.getLong());
        assertEquals(ERROR_CRC, doser.tagError.getString());

        // 4 - no link
        refgr.linkOk = false;
        refgr.crcOk = true;
        feeder0.refSumWeight.getValue().setLong(114);
        doser.execute();
        assertFalse(doser.tagConnected.getBool());
        assertEquals(111, feeder0.tagSumWeight.getLong());
        assertEquals(ERROR_NOT_LINKED, doser.tagError.getString());

        // 5 - task differ
        refgr.linkOk = true;
        doser.refStatus.getValue().setInt(PaGeliosDozkkmcModule.STATUS_LOAD);
        feeder0.refSumWeight.getValue().setLong(115);
        feeder0.refReqWeight.getValue().setLong(444);
        doser.execute();
        assertTrue(doser.tagConnected.getBool());
        assertEquals(115, feeder0.tagSumWeight.getLong());
        assertEquals(ERROR_TASK_DIFFER, doser.tagError.getString());

        // 6 - ok
        feeder0.refReqWeight.getValue().setLong(0);
        doser.execute();
        assertEquals("", doser.tagError.getString());
    }


    @Test
    void installTaskTest() throws SQLException {
        Task task = getTask();

        doser.installTask(task);
        doser.execute();
        check(1, 300, 300, 0, 1, 0, 0, 0, 0, 0, 0);
        checkFeeder(0, 100, 100, 100, 0, 0, -1, 100);
        checkFeeder(1, 200, 200, 200, 0, 0, -1, 200);
        checkFeeder(2, 0, 0, 0, 0, 0, -1, 0);

        doser.refSetWeightMax.getValue().setLong(250);
        doser.installTask(task);
        doser.execute();
        check(1, 300, 300, 0, 2, 0, 0, 0, 0, 0, 0);
        checkFeeder(0, 100, 100, 50, 0, 0, -1, 50);
        checkFeeder(1, 200, 200, 100, 0, 0, -1, 100);

        doser.refSetWeightMax.getValue().setLong(49);
        doser.installTask(task);
        doser.execute();
        check(1, 300, 300, 0, 7, 0, 0, 0, 0, 0, 0);
        checkFeeder(0, 100, 100, 14, 0, 0, -1, 14);
        checkFeeder(1, 200, 200, 29, 0, 0, -1, 29);

        doser.incrementPassCnt(); // 1
        doser.execute();
        check(1, 300, 300, 0, 7, 1, 0, 0, 0, 0, 0);
        checkFeeder(0, 100, 100, 14, 0, 0, -1, 14);
        checkFeeder(1, 200, 200, 29, 0, 0, -1, 29);

        doser.incrementPassCnt(); // 2
        doser.incrementPassCnt(); // 3
        doser.incrementPassCnt(); // 4
        doser.incrementPassCnt(); // 5
        doser.incrementPassCnt(); // 6
        doser.execute();
        check(1, 300, 300, 0, 7, 6, 0, 0, 0, 0, 0);
        checkFeeder(0, 100, 100, 16, 0, 0, -1, 16);
        checkFeeder(1, 200, 200, 26, 0, 0, -1, 26);

    }


    @Test
    void setSetWeightTest() throws SQLException {
        Task task = getTask();

        doser.installTask(task);
        doser.execute();
        check(1, 300, 300, 0, 1, 0, 0, 0, 0, 0, 0);
        checkFeeder(0, 100, 100, 100, 0, 0, -1, 100);
        checkFeeder(1, 200, 200, 200, 0, 0, -1, 200);
        checkFeeder(2, 0, 0, 0, 0, 0, -1, 0);

        doser.setSetWeight(350);
        doser.execute();
        check(1, 300, 350, 0, 1, 0, 0, 0, 0, 0, 0);
        checkFeeder(0, 100, 117, 117, 0, 0, -1, 117);
        checkFeeder(1, 200, 233, 233, 0, 0, -1, 233);
        checkFeeder(2, 0, 0, 0, 0, 0, -1, 0);

        doser.refSetWeightMax.getValue().setLong(250);
        doser.setSetWeight(350);
        doser.execute();
        check(1, 300, 350, 0, 2, 0, 0, 0, 0, 0, 0);
        checkFeeder(0, 100, 117, 59, 0, 0, -1, 59);
        checkFeeder(1, 200, 233, 117, 0, 0, -1, 117);
        checkFeeder(2, 0, 0, 0, 0, 0, -1, 0);

        doser.incrementPassCnt();
        doser.execute();
        check(1, 300, 350, 0, 2, 1, 0, 0, 0, 0, 0);
        checkFeeder(0, 100, 117, 58, 0, 0, -1, 58);
        checkFeeder(1, 200, 233, 116, 0, 0, -1, 116);
        checkFeeder(2, 0, 0, 0, 0, 0, -1, 0);
    }


    /////////////////////////////////////////////// utils //////////////////////////////////////////////
    private Task getTask() {
        Task task = new Task(1, 1, "line1", 1, "recipe1", 0, Task.STATUS_READY);
        task.products.add(new Task.Product(1, "prod1", "dos1", 1, 100));
        task.products.add(new Task.Product(2, "prod2", "dos1", 2, 200));
        task.products.add(new Task.Product(3, "prod3", "dos2", 1, 300));
        return task;
    }

    private void set(
            int refCurStor,
            int refCurWeight,
            int refErrorFlag,
            int refStatus

    ) {
        doser.refCurStor    .getTag().setInt( refCurStor     );
        doser.refCurWeight  .getTag().setInt( refCurWeight   );
        doser.refStatus     .getTag().setInt( refStatus       );
        doser.refErrorFlag  .getTag().setInt( refErrorFlag   );
    }

    private void check(
            int tagConnected,
            long tagRcpWeight,
            long tagSetWeight,
            long tagExeWeight,
            int tagPassQty,
            int tagPassCnt,
            int refSendTask,
            int refCmdStartCycle,
            int refCmdStartTask,
            int refCmdStartLoad,
            int refCmdStartUnload
    ) {
        assertEquals(tagRcpWeight,      doser.tagRcpWeight  .getLong());
        assertEquals(tagSetWeight,      doser.tagSetWeight  .getLong());
        assertEquals(tagExeWeight,      doser.tagExeWeight  .getLong());
        assertEquals(tagConnected,      doser.tagConnected  .getInt());
        assertEquals(tagPassQty,        doser.tagPassQty    .getInt());
        assertEquals(tagPassCnt,        doser.tagPassCnt    .getInt());
        assertEquals(refSendTask,       doser.refSendTask       .getValue().getInt());
        assertEquals(refCmdStartCycle,  doser.refCmdStartCycle  .getValue().getInt());
        assertEquals(refCmdStartTask,   doser.refCmdStartTask   .getValue().getInt());
        assertEquals(refCmdStartLoad,   doser.refCmdStartLoad   .getValue().getInt());
        assertEquals(refCmdStartUnload, doser.refCmdStartUnload .getValue().getInt());
    }



    private void setFeeder(
            int num,
            long refSumWeight,
            long refReqWeight

    ) {
        Feeder feeder = doser.feeders.get(num);
        feeder.refSumWeight.getValue().setLong(refSumWeight);
        feeder.refReqWeight.getValue().setLong(refReqWeight);
    }


    private void checkFeeder(
            int num,
            long tagRcpWeight,
            long tagSetWeight,
            long tagSetWeightPass,
            long tagExeWeight,
            long tagSumWeight,
            long tagSumWeightBeg,
            long refSetWeight
    ) {
        Feeder feeder = doser.feeders.get(num);
        assertEquals(tagRcpWeight,      feeder.tagRcpWeight.getLong());
        assertEquals(tagSetWeight,      feeder.tagSetWeight.getLong());
        assertEquals(tagSetWeightPass,  feeder.tagSetWeightPass.getLong());
        assertEquals(tagExeWeight,      feeder.tagExeWeight.getLong());
        assertEquals(tagSumWeight,      feeder.tagSumWeight.getLong());
        assertEquals(tagSumWeightBeg,   feeder.tagSumWeightBeg.getLong());
        assertEquals(refSetWeight,      feeder.refSetWeight.getValue().getLong());
    }

/*

//            protected TagRW tagCurWeight;
//            protected TagRW tagCurProductName;
//    private RefItem refSumWeightMax;
//    private RefItem refSetWeightMax;

 */

}