package promauto.jroboplc.plugin.raduga;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import promauto.jroboplc.core.ConfigurationYaml;
import promauto.jroboplc.core.api.EnvironmentInst;
import promauto.jroboplc.core.api.SampleEnvironment;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class PeriodsTest {

    Periods p;

    @BeforeEach
    void setUp() {
        SampleEnvironment env = new SampleEnvironment();
        EnvironmentInst.set(env);
        env.setConfiguration(new ConfigurationYaml(""));

        Map<Object,Object> conf = new HashMap<>();
        conf.put("shifts", new ArrayList<>(Arrays.asList(800, 2000)));

        p = new Periods();
        p.load(conf);
    }

    @Test
    void calcPeriodShiftTest() {
        LocalDateTime dt = LocalDateTime.of(2024, 01, 16, 0, 0);
        assertEquals( 202401152, p.calcPeriodShift(dt));

        dt = dt.withHour(7).withMinute(59);
        assertEquals( 202401152, p.calcPeriodShift(dt));

        dt = dt.withHour(8).withMinute(0);
        assertEquals( 202401161, p.calcPeriodShift(dt));

        dt = dt.withHour(19).withMinute(59);
        assertEquals( 202401161, p.calcPeriodShift(dt));

        dt = dt.withHour(20).withMinute(0);
        assertEquals( 202401162, p.calcPeriodShift(dt));

        dt = dt.withHour(23).withMinute(59);
        assertEquals( 202401162, p.calcPeriodShift(dt));

    }

    @Test
    void calcPeriodHourTest() {
        LocalDateTime dt = LocalDateTime.of(2024, 01, 16, 0, 0);
        assertEquals(2024011600, Periods.calcPeriodHour(dt));

        dt = dt.withHour(7).withMinute(59);
        assertEquals(2024011607, Periods.calcPeriodHour(dt));

        dt = dt.withHour(23).withMinute(30);
        assertEquals(2024011623, Periods.calcPeriodHour(dt));
    }

}