package promauto.jroboplc.plugin.raduga;

import java.sql.SQLException;
import java.util.List;

public class LineDummy implements Line {
    public int taskId;
    public int recipeId;
    public int cycleId;

    @Override
    public boolean load(Object conf) {
        return false;
    }

    @Override
    public boolean prepare() {
        return false;
    }

    @Override
    public boolean init() throws SQLException {
        return false;
    }

    @Override
    public void execute() throws SQLException {

    }

    @Override
    public String check() {
        return null;
    }

    @Override
    public String getInfo() {
        return null;
    }

    @Override
    public String makeTagName(String tagname) {
        return "L1_" + tagname;
    }

    @Override
    public String getName() {
        return null;
    }

    @Override
    public int getId() {
        return 0;
    }

    @Override
    public int getTaskId() {
        return taskId;
    }

    @Override
    public int getRecipeId() {
        return recipeId;
    }

    @Override
    public int getCycleId() {
        return cycleId;
    }

    @Override
    public List<Doser> getDosers() {
        return null;
    }

    @Override
    public int getDstBunkerId() {
        return 0;
    }

    @Override
    public int getDstProductId() {
        return 0;
    }

    @Override
    public void updateShift(int period, long plusWeight) {

    }

    @Override
    public List<Line> getLineGroup() {
        return null;
    }

    @Override
    public long getExeWeightShf() {
        return 0;
    }

    @Override
    public void setExeWeightGrp(long weight) {

    }

//    @Override
//    public int getShiftId() {
//        return 0;
//    }
}
