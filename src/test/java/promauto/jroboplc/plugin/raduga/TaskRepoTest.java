package promauto.jroboplc.plugin.raduga;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.*;

class TaskRepoTest {
/*
//    TaskRepo tr;
    Context ctx;
    DataServiceDummy svc;

    Task task1, task2, taskEmpty;

    @BeforeEach
    void setUp() {
        svc = new DataServiceDummy();
        ctx = new Context();
        ctx.setDataService(svc);
//        tr = new TaskRepo(ctx);

        task1 = new Task();
        task1.id = 1;
        task1.presented = true;

        task2 = new Task();
        task2.id = 2;
        task2.presented = true;

        taskEmpty = new Task();
        taskEmpty.presented = false;
    }


    @Test
    void getTest() throws SQLException {
        svc.task = task1;
        Task task = task1; //tr.get(1);
        assertTrue(task.presented);
        assertEquals(1, task.id);

        svc.task = task2;
        task = tr.get(2);
        assertTrue(task.presented);
        assertEquals(2, task.id);

        svc.task = taskEmpty;
        task = tr.get(3);
        assertFalse(task.presented);

        svc.task = taskEmpty;
        task = tr.get(1);
        assertTrue(task.presented);
        assertEquals(1, task.id);
    }

    @Test
    void removeUnusedTest() throws SQLException {
        tr.beforeUsing();
        svc.task = task1;
        tr.get(1);
        svc.task = task2;
        tr.get(2);
        svc.task = taskEmpty;
        tr.get(3);
        tr.removeUnused();
        assertEquals(3, tr.size());

        tr.beforeUsing();
        svc.task = null;
        tr.get(1);
        tr.removeUnused();
        assertEquals(1, tr.size());

        tr.beforeUsing();
        svc.task = taskEmpty;
        tr.get(3);
        tr.removeUnused();
        assertEquals(1, tr.size());
    }
*/
}