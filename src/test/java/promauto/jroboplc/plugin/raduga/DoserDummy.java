package promauto.jroboplc.plugin.raduga;

import promauto.jroboplc.core.api.Tag;
import promauto.jroboplc.core.tags.RefGroupDummy;
import promauto.jroboplc.core.tags.RefItemDummy;

public class DoserDummy extends Doser {
    public DoserDummy(Context ctx, Line line, int doserNum) {
        super(ctx, line, doserNum);
    }

    @Override
    public String makeTagName(String tagname) {
        return "L1_D1_" + tagname;
    }

    @Override
    public long getSumWeightMax() {
        return 3_999_999_999L;
    }

    public static void changeTagTypes(Doser doser) {
        RefGroupDummy refgr = (RefGroupDummy)doser.refgr;
        refgr.items.values()
                .forEach( item -> {
                    if( item.tagname.contains("Weight") )
                        item.changeTagTypeLong();

                    if( item.tagname.equals("SYSTEM.ErrorFlag") )
                        item.changeTagTypeBool();
                } );

//        refgr.items.values().stream()
//                .forEach( item -> System.out.println(item.tagname + "  -  " + item.tag.getType()) );

    }
}
