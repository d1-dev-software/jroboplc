package promauto.jroboplc.plugin.rpsvrtcp;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.ArgumentMatchers.any;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import org.mockito.MockitoAnnotations;
import promauto.jroboplc.core.api.InitUtils;
import promauto.jroboplc.core.api.Environment;
import promauto.jroboplc.core.api.EnvironmentInst;
import promauto.jroboplc.core.api.Module;
import promauto.jroboplc.core.api.ModuleManager;
import promauto.jroboplc.core.api.Tag;
import promauto.jroboplc.core.TagTable;
import promauto.jroboplc.core.api.TcpServerChannel;
import promauto.jroboplc.plugin.rpsvrtcp.RpsvrtcpClient.ClientTag;
import promauto.utils.Strings;

@SuppressWarnings("unchecked")
public class RpsvrtcpClientTest {
	
	@Mock private LinkedList<String> ml;
	@Mock private HashMap<String,LinkedList<String>> mmm;
	
	@Mock private Environment env;
	@Mock private ModuleManager mm; 
	@Mock private Module mod1; 
	@Mock private Module mod2;
	@Mock private TagTable tbl1; 
	@Mock private TagTable tbl2; 
	@Mock private Tag tag1; 
	@Mock private Tag tag2; 
	@Mock private Tag tag3; 
	@Mock private Tag tag4; 
	@Mock private Tag tag5; 
	@Mock private TcpServerChannel channel;

	private RpsvrtcpClient client;
	RpsvrtcpClient clientspy;


	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.openMocks(this);

		InitUtils.setupLogger();
		EnvironmentInst.set(env);

		HashSet<Module> mods = new HashSet<>();
		mods.add(mod1);
		mods.add(mod2);
		when(env.getModuleManager()).thenReturn(mm);
		when(mm.getModules()).thenReturn(mods);
		
		when(mod1.getTagTable()).thenReturn(tbl1);
		when(mod2.getTagTable()).thenReturn(tbl2);
		
		when(mod1.getName()).thenReturn("mod1");
		when(mod2.getName()).thenReturn("mod2");
		when(mod1.getFlag(any(String.class))).thenReturn("");
		when(mod2.getFlag(any(String.class))).thenReturn("");
		
		List<Tag> modtags1 = new LinkedList<>();
		modtags1.add(tag1);
		modtags1.add(tag2);
		modtags1.add(tag3);
		
		List<Tag> modtags2 = new LinkedList<>();
		modtags2.add(tag4);
		modtags2.add(tag5);
		
		when(tbl1.values()).thenReturn(modtags1);
		when(tbl2.values()).thenReturn(modtags2);

		// tag1
		when(tag1.getType()).thenReturn(Tag.Type.STRING);
		when(tag1.getName()).thenReturn("status");
		
		// tag2
		when(tag2.getType()).thenReturn(Tag.Type.INT);
		when(tag2.getName()).thenReturn("input");
		
		// tag3
		when(tag3.getType()).thenReturn(Tag.Type.INT);
		when(tag3.getName()).thenReturn("output");
		
		// tag4
		when(tag4.getType()).thenReturn(Tag.Type.DOUBLE);
		when(tag4.getName()).thenReturn("input");
		
		// tag5
		when(tag5.getType()).thenReturn(Tag.Type.DOUBLE);
		when(tag5.getName()).thenReturn("output");
		
		RpsvrtcpModule m = mock(RpsvrtcpModule.class);
		m.transparentModules = new HashSet<>();
		client = new RpsvrtcpClient(m, "");
		clientspy = spy(client);

	}
	
	
	@Test
	public void testCmdCREATETAGLIST() {
		
		String res = client.cmdCREATETAGLIST();
		List<ClientTag> cltags = client.getCltags();//  (List<ClientTag>) Whitebox.getInternalState(client, "cltags");

		assertEquals(4, cltags.size());
		assertEquals("103 4\r\n", res);
		
		List<Tag> tags = extractTags(cltags);
		assertTrue( tags.contains(tag2));
		assertTrue( tags.contains(tag3));
		assertTrue( tags.contains(tag4));
		assertTrue( tags.contains(tag5));
	}


	@Test
	public void testCmdCREATETAGLIST_TagFilter() {

		client.tagFilter = "mod1.inp*";

		String res = client.cmdCREATETAGLIST();
		List<ClientTag> cltags = client.getCltags();

		assertEquals(1, cltags.size());
		assertEquals("103 1\r\n", res);
		
		List<Tag> tags = extractTags(cltags);
		assertTrue( tags.contains(tag2));
	}

	
	@Test
	public void testCmdCREATETAGLIST_Incl() {
		
		client.module.incl = Strings.getFilterPattern("mod2.inp*"); 
		String res = client.cmdCREATETAGLIST();
		List<ClientTag> cltags = client.getCltags();

		assertEquals(1, cltags.size());
		assertEquals("103 1\r\n", res);
		
		List<Tag> tags = extractTags(cltags);
		assertTrue( tags.contains(tag4));
	}

	
	@Test
	public void testCmdCREATETAGLIST_Excl() {
		
		client.module.excl = Strings.getFilterPattern("mod*.out*"); 
		
		String res = client.cmdCREATETAGLIST();
		List<ClientTag> cltags = client.getCltags();

		assertEquals(2, cltags.size());
		assertEquals("103 2\r\n", res);
		
		List<Tag> tags = extractTags(cltags);
		assertTrue( tags.contains(tag2));
		assertTrue( tags.contains(tag4));
	}

	
	@Test
	public void testCmdCREATETAGLIST_InclExcl() {
		
		client.module.incl = Strings.getFilterPattern("mod1.*"); 
		client.module.excl = Strings.getFilterPattern("*.out*"); 

		String res = client.cmdCREATETAGLIST();
		List<ClientTag> cltags = client.getCltags();

		assertEquals(1, cltags.size());
		assertEquals("103 1\r\n", res);
		
		List<Tag> tags = extractTags(cltags);
		assertTrue( tags.contains(tag2));
	}


	@Test
	public void testCmdSETFILTER() {
		String tagFilter = client.tagFilter;
		assertEquals("*", tagFilter);
		
		client.cmdSETFILTER("test");
		tagFilter = client.tagFilter;
		assertEquals("test", tagFilter);

		String res = client.cmdSETFILTER("'test'");
		tagFilter = client.tagFilter;
		assertEquals("test", tagFilter);

		assertEquals("101 OK\r\n", res);
	}

	
	@Test
	public void testCmdGETFILTER() {
		String res = client.cmdGETFILTER();
		assertEquals("102 '*'\r\n", res);
	}


	@Test
	public void testCmdGETTAGLIST() {
		List<ClientTag> cltags = client.getCltags();
		cltags.add(new ClientTag(mod1, false, tag2));
		cltags.add(new ClientTag(mod1, false, tag3));
		cltags.add(new ClientTag(mod2, false, tag4));
		cltags.add(new ClientTag(mod2, false, tag5));
		
		String res = client.cmdGETTAGLIST("0");
		assertEquals("104 4#0!mod1.input;mod1.output;mod2.input;mod2.output=6E28\r\n", res);
		
		res = client.cmdGETTAGLIST("2");
		assertEquals("104 2#2!mod2.input;mod2.output=EED1\r\n", res);
		
		res = client.cmdGETTAGLIST("4");
		assertEquals("104 0#4!=3628\r\n", res);
	}

	
	@Test
	public void testCmdFIXALL() {
		List<ClientTag> cltags = client.getCltags();
		cltags.add(new ClientTag(mod1, false, tag2));
		cltags.add(new ClientTag(mod1, false, tag3));

		// initially all cltags have state=1 (fixed)
		String res = client.cmdFIXALL();
		assertEquals("105 2\r\n", res);
		
		// reset state tag2
		cltags.get(0).state = 0;
		res = client.cmdFIXALL();
		assertEquals("105 1\r\n", res);
		
		// reset state tag3
		cltags.get(1).state = 0;
		res = client.cmdFIXALL();
		assertEquals("105 0\r\n", res);

		// change value tag2
		when( tag2.getInt() ).thenReturn(1);
		res = client.cmdFIXALL();
		assertEquals("105 1\r\n", res);
	}
	

	@Test
	public void testCmdGETALL() {
		List<ClientTag> cltags = client.getCltags();
		cltags.add(new ClientTag(mod1, false, tag2));
		cltags.add(new ClientTag(mod1, false, tag3));

		String res = client.cmdGETALL("0");
		assertEquals("106 2#0!;=CC4F\r\n", res);
		
		cltags.get(0).fixval = 1;
		res = client.cmdGETALL("0");
		assertEquals("106 2#0!1;=87CA\r\n", res);

		cltags.get(1).fixval = 255;
		res = client.cmdGETALL("0");
		assertEquals("106 2#0!1;FF=50A5\r\n", res);

		res = client.cmdGETALL("1");
		assertEquals("106 1#1!FF=992C\r\n", res);
	}

	
	@Test
	public void testCmdGETCHG() {
		List<ClientTag> cltags = client.getCltags();
		cltags.add(new ClientTag(mod1, false, tag2));
		cltags.add(new ClientTag(mod1, false, tag3));

		String res = client.cmdGETCHG("0");
		assertEquals("107 2#0!;=CC4F\r\n", res);
		assertEquals(2, cltags.get(0).state);
		assertEquals(2, cltags.get(1).state);

		cltags.get(0).state = 0;
		cltags.get(1).state = 0;
		res = client.cmdGETCHG("0");
		assertEquals("107 0=54BF\r\n", res);
		assertEquals(0, cltags.get(0).state);
		assertEquals(0, cltags.get(1).state);
	}

	
	@Test
	public void testCmdWNM() {
		client.addCltag(new ClientTag(mod1, false, tag2));
		client.addCltag(new ClientTag(mod1, false, tag3));

		String res = client.cmdWNM("0 a e7a9");
		assertEquals("109 !\r\n", res);
		verify(tag2).setInt(10);
		
		// bad crc
		res = client.cmdWNM("0 1 bad");
		assertEquals("109 ?\r\n", res);
		verify(tag2, never()).setInt(1);
	}

	
	@Test
	public void testOnRequest_FIXALL() {
		clientspy.onRequest(channel, "FIXALL");
		verify(clientspy).cmdFIXALL();
	}

	@Test
	public void testOnRequest_GETCHG() {
		clientspy.onRequest(channel, "GETCHG 0");
		verify(clientspy).cmdGETCHG("0");
	}

	@Test
	public void testOnRequest_WNM() {
		clientspy.onRequest(channel, "WNM 1 2 3");
		verify(clientspy).cmdWNM("1 2 3");
	}

	@Test
	public void testOnRequest_GETALL() {
		clientspy.onRequest(channel, "GETALL 0");
		verify(clientspy).cmdGETALL("0");
	}

	@Test
	public void testOnRequest_CREATETAGLIST() {
		clientspy.onRequest(channel, "CREATETAGLIST");
		verify(clientspy).cmdCREATETAGLIST();
	}

	@Test
	public void testOnRequest_GETTAGLIST() {
		clientspy.onRequest(channel, "GETTAGLIST 0");
		verify(clientspy).cmdGETTAGLIST("0");
	}

	@Test
	public void testOnRequest_SETFILTER() {
		clientspy.onRequest(channel, "SETFILTER abc");
		verify(clientspy).cmdSETFILTER("abc");
	}

	@Test
	public void testOnRequest_GETFILTER() {
		clientspy.onRequest(channel, "GETFILTER");
		verify(clientspy).cmdGETFILTER();
	}

	@Test
	public void testOnRequest_UNKNOWN() {
		clientspy.onRequest(channel, "BLABLABLA");
		verify(channel).write("400 NOT SUPPORTED\r\n");
	}

	
	private List<Tag> extractTags(List<ClientTag> cltags) {
		List<Tag> tags = new LinkedList<>();
		for (ClientTag cltag: cltags)
			tags.add(cltag.tag);
		return tags;
	}

}
