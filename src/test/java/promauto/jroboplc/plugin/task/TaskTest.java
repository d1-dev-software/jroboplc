package promauto.jroboplc.plugin.task;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import promauto.jroboplc.core.api.InitUtils;
import promauto.jroboplc.core.api.SampleEnvironment;
import promauto.jroboplc.core.api.SampleModule;
import promauto.jroboplc.core.api.SampleModuleManager;
import promauto.jroboplc.core.api.SamplePlugin;
import promauto.jroboplc.core.ConfigurationYaml;
import promauto.jroboplc.core.api.EnvironmentInst;

public class TaskTest {
	private TaskModule task;
	private int period;
	private boolean enable;
	SampleEnvironment env;
	SampleModuleManager mm;
	private SampleModule mod1;
	private SampleModule mod2;
	

	@Before
	public void setUp() throws Exception {
		InitUtils.setupLogger();

		env = new SampleEnvironment();
		EnvironmentInst.set(env);
		
		env.setConfiguration( new ConfigurationYaml("") );
		
		mm = new SampleModuleManager();
		env.mm = mm;
		
		mod1 = new SampleModule("exbot1");
		mod2 = new SampleModule("exbot2");
		mm.modules.put(mod1.getName(), mod1);
		mm.modules.put(mod2.getName(), mod2);

		SamplePlugin plug = new SamplePlugin();

		task = new TaskModule(plug, "modulename");
		task.load(new HashMap<String,String>());
		getPrivateFields();
	}
	
	private void getPrivateFields() throws Exception {
		Field f;
		f = task.getClass().getDeclaredField("items");
		f.setAccessible(true);
		
		f = task.getClass().getDeclaredField("period");
		f.setAccessible(true);
		period = (int) f.get(task); 
		
		f = task.getClass().getSuperclass().getDeclaredField("enable");
		f.setAccessible(true);
		enable = (boolean) f.get(task); 
	}


	@Test
	public void testLoadBadTimePeriod() {
		Map<String,Object> conf = new HashMap<>();
		conf.put("enable", false);
		conf.put("period", "1:00");
		conf.put("modules", "anymod");
		
		task.load(conf);
	}



	@Test
	public void testRun() throws Exception {
		
		Map<String,Object> conf = new HashMap<>();
		conf.put("enable", true);
		conf.put("period", 100);
		conf.put("modules", "exbot1 exbot2");
		
		assertTrue( task.load(conf));
		assertTrue( task.prepare());
		getPrivateFields();
		assertEquals( 100, period);
		assertTrue( enable);
		
		
		
		env.setRunning(true);
		task.execute();
		
		Thread.sleep(50);
		assertEquals( 0L, task.timecounter.get());
		assertTrue(task.running);
		Thread.sleep(100);

		env.setRunning(false);
		Thread.sleep(200);

		assertEquals( 2L, task.timecounter.get());
		assertFalse( task.running);
		
	}

}
