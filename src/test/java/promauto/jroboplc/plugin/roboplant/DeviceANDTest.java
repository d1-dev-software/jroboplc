package promauto.jroboplc.plugin.roboplant;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static promauto.jroboplc.plugin.roboplant.AuxProcs.newInputAndTag;

import java.nio.file.Paths;

import org.junit.Before;
import org.junit.Test;

import promauto.jroboplc.core.api.InitUtils;
import promauto.jroboplc.core.tags.TagInt;

public class DeviceANDTest {
	private DeviceAND d;
	
	@Before
	public void setUp() throws Exception {
		InitUtils.setupLogger();
		
		d = new DeviceAND();

		d.tagOutput = new TagInt("", 0);
		d.inpInput.add( newInputAndTag() );
		d.inpInput.add( newInputAndTag() );
		d.inpInput.add( newInputAndTag() );
	}

	
	@Test
	public void testLoad() throws Exception {
		
		RoboplantModule robo = AuxProcs.createRoboplantModule();
		assertTrue( robo.loadProject( Paths.get( 
				InitUtils.getResourcesDir("roboplant", RoboplantModuleTest.class) + "and.xml" )));

		assertEquals(1, robo.devicesByAddr.length);
		DeviceAND d = (DeviceAND)robo.devicesByAddr[0];
		
		assertEquals( "AND", d.devtype );
		assertEquals( null, d.tagname );
		
		assertTrue( d.init(robo) );
		assertTrue( d.prepare() );
		
		assertEquals( "S01_01", d.name );
		assertEquals( "T0000", d.tagname );
		assertEquals(2, d.inpInput.size());
	}

	@Test
	public void testExecute() {
		assertTrue( d.execute() );
		assertEquals( 0, d.tagOutput.getInt() );
		
		d.inpInput.get(0).tag.setInt(1);
		assertTrue( d.execute() );
		assertEquals( 0, d.tagOutput.getInt() );
		
		d.inpInput.get(1).tag.setInt(1);
		assertTrue( d.execute() );
		assertEquals( 0, d.tagOutput.getInt() );
		
		d.inpInput.get(2).tag.setInt(1);
		assertTrue( d.execute() );
		assertEquals( 1, d.tagOutput.getInt() );
	}

	@Test
	public void testNoInputs() {
		d.inpInput.clear();
		assertTrue( d.execute() );
		assertEquals( 1, d.tagOutput.getInt() );
	}
}
