package promauto.jroboplc.plugin.roboplant;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;

import promauto.jroboplc.core.api.InitUtils;
import promauto.jroboplc.core.api.Tag;
import promauto.jroboplc.core.tags.TagInt;

public class ChannelManagerTest {

	ChannelManager cm;
	
	Device d0;
	Device d1;
	Device d2;
	
	Tag tagAddr0;
	Tag tagAddr1;
	Tag tagAddr2;
	
	Output out0;
	Output out1;
	Output out2;
	Output out3;
	Output out4;
	
	@Before
	public void setUp() throws Exception {
		InitUtils.setupLogger();
		
		cm = new ChannelManager(null);
		
		out0 = new Output();
		out0.tag = new TagInt("tag0", 0);
		
		out1 = new Output();
		out1.tag = new TagInt("tag1", 0);
		
		out2 = new Output();
		out2.tag = new TagInt("tag2", 0);
		
		out3 = new Output();
		out3.tag = new TagInt("tag3", 0);
		
		out4 = new Output();
		out4.tag = new TagInt("tag4", 0);
		
		d0 = mock(Device.class);
		when(d0.getOutput(0)).thenReturn(out0);
		
		d1 = mock(Device.class);
		when(d1.getOutput(0)).thenReturn(out1);
		when(d1.getOutput(1)).thenReturn(out2);
		
		d2 = mock(Device.class);
		when(d2.getOutput(0)).thenReturn(out3);
		when(d2.getOutput(1)).thenReturn(out4);

		tagAddr0 = new TagInt("DipNetAddress", 1);
		tagAddr1 = new TagInt("DipNetAddress", 1);
		tagAddr2 = new TagInt("DipNetAddress", 2);
	}

	
	@Test
	public void test() {
		
		cm.registerChannelProvider(d0, tagAddr0, Channel.Type.Out);
		cm.registerChannelProvider(d1, tagAddr1, Channel.Type.In);
		cm.registerChannelProvider(d2, tagAddr2, Channel.Type.InOut);
		
		Tag tagAddrNum0 = new TagInt("ch0", 0x0100);
		Channel ch0 = cm.createChannel(tagAddrNum0, Channel.Type.Out);

		Tag tagAddrNum1 = new TagInt("ch1", 0x0100);
		Channel ch1 = cm.createChannel(tagAddrNum1, Channel.Type.In);

		Tag tagAddrNum2 = new TagInt("ch2", 0x0101);
		Channel ch2 = cm.createChannel(tagAddrNum2, Channel.Type.In);

		Tag tagAddrNum3 = new TagInt("ch3", 0x0200);
		Channel ch3 = cm.createChannel(tagAddrNum3, Channel.Type.In);
		
		Tag tagAddrNum4 = new TagInt("ch4", 0x0201);
		Channel ch4 = cm.createChannel(tagAddrNum4, Channel.Type.Out);


		// 1
		cm.execute();
		
		assertEquals( Channel.State.Ok, ch0.state);
		assertEquals( Channel.State.Ok, ch1.state);
		assertEquals( Channel.State.Ok, ch2.state);
		assertEquals( Channel.State.Ok, ch3.state);
		assertEquals( Channel.State.Ok, ch4.state);

		assertSame(out0.tag, ch0.tagValue);
		assertSame(out1.tag, ch1.tagValue);
		assertSame(out2.tag, ch2.tagValue);
		assertSame(out3.tag, ch3.tagValue);
		assertSame(out4.tag, ch4.tagValue);
		

		
		// 2
		cm.execute();
		
		assertEquals( Channel.State.Ok, ch0.state);
		assertEquals( Channel.State.Ok, ch1.state);
		assertEquals( Channel.State.Ok, ch2.state);
		assertEquals( Channel.State.Ok, ch3.state);
		assertEquals( Channel.State.Ok, ch4.state);


		// 3
		tagAddrNum1.setInt(0x0300);
		cm.execute();
		assertEquals( Channel.State.Ok, 	ch0.state);
		assertEquals( Channel.State.Error, 	ch1.state);
		assertEquals( Channel.State.Ok, 	ch2.state);
		assertEquals( Channel.State.Ok, 	ch3.state);
		assertEquals( Channel.State.Ok, 	ch4.state);
		
		// 4
		tagAddr1.setInt(3);
		cm.execute();
		assertEquals( Channel.State.Ok, 	ch0.state);
		assertEquals( Channel.State.Ok, 	ch1.state);
		assertEquals( Channel.State.Error, 	ch2.state);
		assertEquals( Channel.State.Ok, 	ch3.state);
		assertEquals( Channel.State.Ok, 	ch4.state);

		// 5
		tagAddrNum3.setInt(0x0207);
		cm.execute();
		assertEquals( Channel.State.Ok, 	ch0.state);
		assertEquals( Channel.State.Ok, 	ch1.state);
		assertEquals( Channel.State.Error, 	ch2.state);
		assertEquals( Channel.State.Error, 	ch3.state);
		assertEquals( Channel.State.Ok, 	ch4.state);

		// 6
		tagAddrNum0.setInt(0x0201);
		cm.execute();
		assertEquals( Channel.State.Ok, 	ch0.state);
		assertEquals( Channel.State.Ok, 	ch1.state);
		assertEquals( Channel.State.Error, 	ch2.state);
		assertEquals( Channel.State.Error, 	ch3.state);
		assertEquals( Channel.State.Ok, 	ch4.state);
		assertSame(out4.tag, ch0.tagValue);
		assertSame(out4.tag, ch4.tagValue);
		assertSame(ch4.tagValue, ch0.tagValue);

		
		// provider's address conflict
		// 7
		tagAddr1.setInt(1);
		tagAddrNum0.setInt(0x0100);
		tagAddrNum1.setInt(0x0100);
		tagAddrNum2.setInt(0x0101);
		tagAddrNum3.setInt(0x0200);
		tagAddrNum4.setInt(0x0201);
		cm.execute();
		assertEquals( Channel.State.Ok, 	ch0.state);
		assertEquals( Channel.State.Ok, 	ch1.state);
		assertEquals( Channel.State.Ok, 	ch2.state);
		assertEquals( Channel.State.Ok, 	ch3.state);
		assertEquals( Channel.State.Ok, 	ch4.state);
		assertSame(out0.tag, ch0.tagValue);
		assertSame(out1.tag, ch1.tagValue);
		assertSame(out2.tag, ch2.tagValue);
		assertSame(out3.tag, ch3.tagValue);
		assertSame(out4.tag, ch4.tagValue);
		
		// 8 - all have one
		tagAddr0.setInt(1);
		tagAddr1.setInt(1);
		tagAddr2.setInt(1); 
		cm.execute();
		assertEquals( Channel.State.Ok, 	ch0.state);
		assertEquals( Channel.State.Ok, 	ch1.state);
		assertEquals( Channel.State.Ok, 	ch2.state);
		assertEquals( Channel.State.Error, 	ch3.state);
		assertEquals( Channel.State.Error, 	ch4.state);
		assertSame(out3.tag, ch0.tagValue);
		assertSame(out3.tag, ch1.tagValue);
		assertSame(out4.tag, ch2.tagValue);

		// 9 
		tagAddr0.setInt(1);
		tagAddr1.setInt(10);
		tagAddr2.setInt(1); 
		cm.execute();
		assertEquals( Channel.State.Ok, 	ch0.state);
		assertEquals( Channel.State.Ok, 	ch1.state);
		assertEquals( Channel.State.Ok, 	ch2.state);
		assertEquals( Channel.State.Error, 	ch3.state);
		assertEquals( Channel.State.Error, 	ch4.state);
		assertSame(out3.tag, ch0.tagValue);
		assertSame(out3.tag, ch1.tagValue);
		assertSame(out4.tag, ch2.tagValue);

		// 11
		tagAddr0.setInt(1);
		tagAddr1.setInt(1);
		tagAddr2.setInt(1); 
		cm.execute();
		assertEquals( Channel.State.Ok, 	ch0.state);
		assertEquals( Channel.State.Ok, 	ch1.state);
		assertEquals( Channel.State.Ok, 	ch2.state);
		assertEquals( Channel.State.Error, 	ch3.state);
		assertEquals( Channel.State.Error, 	ch4.state);
		assertSame(out3.tag, ch0.tagValue);
		assertSame(out1.tag, ch1.tagValue);
		assertSame(out2.tag, ch2.tagValue);

		// 12
		tagAddr2.setInt(2);
		cm.execute();
		cm.execute();
		assertEquals( Channel.State.Ok, 	ch0.state);
		assertEquals( Channel.State.Ok, 	ch1.state);
		assertEquals( Channel.State.Ok, 	ch2.state);
		assertEquals( Channel.State.Ok, 	ch3.state);
		assertEquals( Channel.State.Ok, 	ch4.state);
		assertSame(out0.tag, ch0.tagValue);
		assertSame(out1.tag, ch1.tagValue);
		assertSame(out2.tag, ch2.tagValue);
		assertSame(out3.tag, ch3.tagValue);
		assertSame(out4.tag, ch4.tagValue);

		// 12
		tagAddr0.setInt(0x0A01);
		tagAddr1.setInt(0x0B01);
		tagAddr2.setInt(0x0C02);
		cm.execute();
		cm.execute();
		assertEquals( Channel.State.Ok, 	ch0.state);
		assertEquals( Channel.State.Ok, 	ch1.state);
		assertEquals( Channel.State.Ok, 	ch2.state);
		assertEquals( Channel.State.Ok, 	ch3.state);
		assertEquals( Channel.State.Ok, 	ch4.state);
		assertSame(out0.tag, ch0.tagValue);
		assertSame(out1.tag, ch1.tagValue);
		assertSame(out2.tag, ch2.tagValue);
		assertSame(out3.tag, ch3.tagValue);
		assertSame(out4.tag, ch4.tagValue);
	}

}
