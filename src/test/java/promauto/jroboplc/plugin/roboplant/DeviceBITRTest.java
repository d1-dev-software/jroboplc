package promauto.jroboplc.plugin.roboplant;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import promauto.jroboplc.core.api.Tag;
import promauto.jroboplc.plugin.roboplant.Device.RefBool;

class DeviceBITRTest {

    private DeviceBITR d;

    RoboplantModule module;

    Tag tag0;
    Tag tag1;
    Tag tag2;


    @BeforeEach
    void setUp() throws IllegalAccessException {

        d = new DeviceBITR();
        d.module = new RoboplantModule(new RoboplantPlugin(), "");
        d.outputs.add( new Output(""));
        d.outputs.add( new Output(""));
        d.outputs.add( new Output(""));
        d.createTagForEachOutputs();

        RefBool res = new RefBool(true);
        d.prepareTags(res);

        tag0 = d.outputs.get(0).tag;
        tag1 = d.outputs.get(1).tag;
        tag2 = d.outputs.get(2).tag;

    }

    private void exec(
            int output0,
            int output1,
            int output2
    ) {
        d.execute();
        Assertions.assertEquals(output0, d.outputs.get(0).tag.getInt(), "output 0");
        Assertions.assertEquals(output1, d.outputs.get(1).tag.getInt(), "output 1");
        Assertions.assertEquals(output2, d.outputs.get(2).tag.getInt(), "output 2");
    }

    @Test
    void executeTest() {
        exec(0, 0, 0);

        tag0.setOn();
        exec(1, 0, 0);

        tag1.setOn();
        exec(0, 1, 0);

        tag2.setOn();
        exec(0, 0, 1);

        tag2.setOff();
        exec(0, 0, 0);

        tag0.setOn();
        tag1.setOn();
        tag2.setOn();
        exec(1, 0, 0);
    }
}