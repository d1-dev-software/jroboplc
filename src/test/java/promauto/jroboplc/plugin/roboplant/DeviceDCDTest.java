package promauto.jroboplc.plugin.roboplant;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static promauto.jroboplc.plugin.roboplant.AuxProcs.newInputAndTag;

import java.nio.file.Paths;

import org.junit.Before;
import org.junit.Test;

import promauto.jroboplc.core.api.InitUtils;
import promauto.jroboplc.core.tags.TagInt;

public class DeviceDCDTest {
	private DeviceDCD d;
	
	@Before
	public void setUp() throws Exception {
		InitUtils.setupLogger();
		
		d = new DeviceDCD();
		d.inpInput 	= newInputAndTag();
		
		for (int i=0; i<256; i++)
			d.tagOutput.add(new TagInt("", 0));
	
//		d.resetState();
	}

	@Test
	public void testLoad() throws Exception {
		
		RoboplantModule robo = AuxProcs.createRoboplantModule();
		assertTrue( robo.loadProject( Paths.get( 
				InitUtils.getResourcesDir("roboplant", RoboplantModuleTest.class) + "dcd.xml" )));

		assertEquals(1, robo.devicesByAddr.length);
		DeviceDCD d = (DeviceDCD)robo.devicesByAddr[0];
		
		assertEquals( "DCD", d.devtype );
		assertEquals( "S01_off9", d.name );
		
		assertTrue( d.init(robo) );
		assertTrue( d.prepare() );

		assertEquals( "T0000", d.tagname );
	}
	
	private void set(
			int input
			) {
		d.inpInput.tag.setInt(input);
	}

	
	private void exec(
			int... values
			) {
		d.execute();
		
		for (int i=0; i<values.length; i++)
			assertEquals("output_" + i, values[i], d.tagOutput.get(i).getInt());     
	}


	@Test
	public void testExecute() {
		set(0);  exec(1,0,0,0);
		set(1);  exec(1,1,0,0);
		set(0);  exec(1,0,0,0);
		set(1);  exec(1,1,0,0);
		set(2);  exec(1,1,1,0);
		set(3);  exec(1,1,1,1);
		set(4);  exec(1,1,1,1);
	}


}
