package promauto.jroboplc.plugin.roboplant;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import promauto.jroboplc.core.api.InitUtils;
import promauto.jroboplc.core.tags.TagInt;

import static org.junit.Assert.assertEquals;

public class DeviceCRC16Test {

	private DeviceCRC16 d;

	@Mock RoboplantModule module;


	@Before
	public void setUp() throws Exception {
		InitUtils.setupLogger();
		
		d = new DeviceCRC16();
		d.module = module;
		
		d.tagOutput = new TagInt("", 0);

        for (int i = 0; i < 5; ++i) {
            Input inp = new Input();
            inp.tag = new TagInt("", 0);
            d.inpInput.add( inp );
        }
	}



    private void exec(
            int Output
    ) {
        d.execute();
        assertEquals("Output"	, Output   , d.tagOutput   .getInt());
    }


    @Test
	public void test() {
        exec(1904);

        d.inpInput.get(0).tag.setInt(1);
        d.inpInput.get(1).tag.setInt(2);
        d.inpInput.get(2).tag.setInt(3);
        d.inpInput.get(3).tag.setInt(4);
        d.inpInput.get(4).tag.setInt(0);
        exec(38491);
    }




}



















