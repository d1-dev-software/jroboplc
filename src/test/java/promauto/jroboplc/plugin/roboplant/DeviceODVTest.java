package promauto.jroboplc.plugin.roboplant;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static promauto.jroboplc.plugin.roboplant.AuxProcs.newInputAndTag;

import java.nio.file.Paths;

import org.junit.Before;
import org.junit.Test;

import promauto.jroboplc.core.api.InitUtils;
import promauto.jroboplc.core.tags.TagInt;

public class DeviceODVTest {
	private DeviceODV d;
	
	@Before
	public void setUp() throws Exception {
		InitUtils.setupLogger();

		d = new DeviceODV();
		d.inpInput 	= newInputAndTag();
		
		d.tagOutput		= new TagInt("", 0);
		d.tagTime		= new TagInt("", 3);
		d.tagCnt		= new TagInt("", 0);
	}

	@Test
	public void testLoad() throws Exception {
		RoboplantModule robo = AuxProcs.createRoboplantModule();
		assertTrue( robo.loadProject( Paths.get( 
				InitUtils.getResourcesDir("roboplant", RoboplantModuleTest.class) + "odv.xml" )));

		assertEquals(1, robo.devicesByAddr.length);
		d = (DeviceODV)robo.devicesByAddr[0];
		assertTrue( d.init(robo) );
		assertTrue( d.prepare() );

		assertEquals( "ODV", d.devtype );
		assertEquals( "Beep1 Start", d.name );
		assertEquals( "T0000", d.tagname );
	}
	
	private void set(
			int input
			) {
		d.inpInput.tag.setInt(input);
	}

	
	private void exec(
			int output 
			) {
		d.execute();
		assertEquals("output"	, output	 , d.tagOutput.getInt());     
	}


	@Test
	public void testExecute() {
		set(0); 	exec(0);
		set(0); 	exec(0);
		set(1); 	exec(1);
		set(1); 	exec(1);
		set(1); 	exec(1);
		set(1); 	exec(0);
		set(1); 	exec(0);
		set(1); 	exec(0);
		set(0); 	exec(0);
		set(0); 	exec(0);
		set(0); 	exec(0);
		set(1); 	exec(1);
		set(1); 	exec(1);
		set(1); 	exec(1);
		set(1); 	exec(0);
		set(1); 	exec(0);
		set(1); 	exec(0);
	}

	@Test
	public void testExecute_time_0() {
		d.tagTime.setInt(0);
		set(0); 	exec(0);
		set(0); 	exec(0);
		set(1); 	exec(1);
		set(1); 	exec(0);
		set(1); 	exec(0);
		set(0); 	exec(0);
		set(0); 	exec(0);
		set(0); 	exec(0);
	}
}
