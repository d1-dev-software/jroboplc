package promauto.jroboplc.plugin.roboplant;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.nio.file.Paths;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import promauto.jroboplc.core.api.InitUtils;
import promauto.jroboplc.core.tags.TagInt;

public class DeviceZDVTTest {
	private DeviceZDVT d;

	@Mock RoboplantModule module;
	

	@Before
	public void setUp() throws Exception {
		InitUtils.setupLogger();
		
		d = new DeviceZDVT();
		d.module = module;

		d.inpEnable = new Input();
		d.inpEnable.tag = new TagInt("", 0);
		
    	d.dirAmount = 3;
		for (int i=0; i<d.dirAmount; i++) {
			DeviceZDVT.Direction dir = new DeviceZDVT.Direction();
			
			d.dirs.add(dir);
			dir.inp = new Input();
			dir.inp.tag = new TagInt("", 0);
			
			dir.channelIn = new Channel();
			dir.channelIn.tagAddrNum = new TagInt("", 0); 
			dir.channelIn.tagValue = new TagInt("", 0); 
			dir.channelIn.state = Channel.State.Ok;
			
			dir.channelOut = new Channel();
			dir.channelOut.tagAddrNum = new TagInt("", 0); 
			dir.channelOut.tagValue = new TagInt("", 0); 
			dir.channelOut.state = Channel.State.Ok;
		}


		d.tagOutput   			= new TagInt("", 0);
		d.tagSost     			= new TagInt("", 0);
		d.tagControl  			= new TagInt("", 0);
		d.tagBlok     			= new TagInt("", 0);
		d.tagFlags    			= new TagInt("", 0);
		d.tagDelay   			= new TagInt("", 0);
		d.tagParam   			= new TagInt("", 0);
	    
//		d.resetState();
	}
	
	private void exec(
			int output	,       
			int sost	,         
			int chnl1out,
			int chnl2out,
			int chnl3out
			) {
		
		d.execute();
		assertEquals("output"	, output	, d.tagOutput.getInt());     
		assertEquals("sost"		, sost		, d.tagSost.getInt());     
		assertEquals("chnl1out"	, chnl1out	, d.dirs.get(0).channelOut.tagValue.getInt());     
		assertEquals("chnl2out"	, chnl2out	, d.dirs.get(1).channelOut.tagValue.getInt());     
		assertEquals("chnl3out"	, chnl3out	, d.dirs.get(2).channelOut.tagValue.getInt());     
		
	}

	private void set(int inp1, int inp2, int inp3, int ch1in, int ch2in, int ch3in) {
		d.dirs.get(0).inp.tag.setInt(inp1);
		d.dirs.get(1).inp.tag.setInt(inp2);
		d.dirs.get(2).inp.tag.setInt(inp3);
		
		d.dirs.get(0).channelIn.tagValue.setInt(ch1in);
		d.dirs.get(1).channelIn.tagValue.setInt(ch2in);
		d.dirs.get(2).channelIn.tagValue.setInt(ch3in);
	}

	
	@Test
	public void testLoad() throws Exception {
		
		RoboplantModule robo = AuxProcs.createRoboplantModule();
		assertTrue( robo.loadProject( Paths.get( 
				InitUtils.getResourcesDir("roboplant", RoboplantModuleTest.class) + "zdvt.xml" )));

		assertEquals(1, robo.devicesByAddr.length);
		DeviceZDVT d = (DeviceZDVT)robo.devicesByAddr[0];
		
		assertEquals( "ZDVT", d.devtype );
		assertEquals( "610", d.tagname );
		
		assertEquals(4, d.inputs.size());
		assertEquals(13, d.outputs.size());
		
		assertTrue( d.init(robo) );
		assertTrue( d.prepare() );

		assertEquals(3, d.dirs.size());
	}
	
	
	@Test
	public void testLinkChannel_False() {
		d.dirs.get(1).channelOut.state = Channel.State.Error;
		assertFalse(d.execute());
		assertEquals(0xFE, d.tagSost.getInt());
	}

	@Test
	public void testFlags_0() {
		d.tagFlags.setInt(0);
		
		set( 0,0,0,  0,0,0 );
		exec( 0, 0x81,  0,0,0 );
	}

	@Test
	public void testFlag_80_sequential_opening() {
		d.tagFlags.setInt(0x80);
		d.inpEnable.tag.setInt(1);
		
		set(    1,0,0,  0,0,0 ); // dir 1
		exec( 1, 0x81,  1,0,0 );
		exec( 1, 0x81,  0,0,0 );
		
		set(    1,0,0,  1,0,0 );
		exec( 1, 0x01,  0,0,0 );
		
		set(    0,1,0,  0,0,0 ); // dir 2
		exec( 1, 0x82,  0,1,0 );
		exec( 1, 0x82,  0,0,0 );
		
		set(    0,1,0,  0,1,0 );
		exec( 1, 0x02,  0,0,0 );
		
		set(    0,0,1,  0,0,0 ); // dir 3
		exec( 1, 0x83,  0,0,1 );
		exec( 1, 0x83,  0,0,0 );
		
		set(    0,0,1,  0,0,1 );
		exec( 1, 0x03,  0,0,0 );
	}

	@Test
	public void testFlag_01_sequential_opening() {
		d.tagFlags.setInt(0x01);
		d.inpEnable.tag.setInt(1);
		
		set(    1,0,0,  1,1,1 ); // dir 1
		exec( 1, 0x81,  1,0,0 );
		exec( 1, 0x81,  1,0,0 );
		
		set(    1,0,0,  0,1,1 );
		exec( 1, 0x01,  0,0,0 );
		
		set(    0,1,0,  1,1,1 ); // dir 2
		exec( 1, 0x82,  0,1,0 );
		exec( 1, 0x82,  0,1,0 );
		
		set(    0,1,0,  1,0,1 );
		exec( 1, 0x02,  0,0,0 );
		
		set(    0,0,1,  1,1,1 ); // dir 3
		exec( 1, 0x83,  0,0,1 );
		exec( 1, 0x83,  0,0,1 );
		
		set(    0,0,1,  1,1,0 );
		exec( 1, 0x03,  0,0,0 );
	}

	@Test
	public void testFlag_82_sequential_opening() {
		d.tagFlags.setInt(0x82);
		d.inpEnable.tag.setInt(1);
		
		set(    1,0,0,  0,0,0 ); // dir 1
		exec( 1, 0x81,  1,0,0 );
		exec( 1, 0x81,  1,0,0 );
		
		set(    1,0,0,  1,0,0 );
		exec( 1, 0x01,  1,0,0 );
		
		set(    0,1,0,  0,0,0 ); // dir 2
		exec( 1, 0x82,  0,1,0 );
		exec( 1, 0x82,  0,1,0 );
		
		set(    0,1,0,  0,1,0 );
		exec( 1, 0x02,  0,1,0 );
		
		set(    0,0,1,  0,0,0 ); // dir 3
		exec( 1, 0x83,  0,0,1 );
		exec( 1, 0x83,  0,0,1 );
		
		set(    0,0,1,  0,0,1 );
		exec( 1, 0x03,  0,0,1 );
	}

	@Test
	public void testFlag_85_lost_signal() {
		d.tagFlags.setInt(0x85);
		d.inpEnable.tag.setInt(1);
		
		set(    1,0,0,  1,0,0 ); // dir1 ok
		exec( 1, 0x01,  0,0,0 );
		
		set(    1,0,0,  0,0,0 ); // dir 1 - lost signal 
		exec( 0, 0x81,  0,0,0 ); 
		
		set(    1,0,0,  1,0,0 ); // dir1 ok
		exec( 1, 0x01,  0,0,0 );

		set(    1,0,0,  0,0,0 ); // dir 1 - lost signal 
		exec( 0, 0x81,  0,0,0 ); 
		
		set(    0,0,0,  0,0,0 ); // no dir1
		exec( 0, 0x00,  0,0,0 );
		
		set(    1,0,0,  0,0,0 ); // dir 1 - set again  
		exec( 0, 0x81,  1,0,0 ); 
	}


	@Test
	public void testFlag_80_lost_signal() {
		d.tagFlags.setInt(0x80);
		d.inpEnable.tag.setInt(1);
		
		set(    1,0,0,  1,0,0 ); // dir1 ok
		exec( 1, 0x01,  0,0,0 );
		
		set(    1,0,0,  0,0,0 ); // dir 1 - lost signal 
		exec( 1, 0x81,  0,0,0 ); 
		
		set(    1,0,0,  1,0,0 ); // dir1 ok
		exec( 1, 0x01,  0,0,0 );

		set(    1,0,0,  0,0,0 ); // dir 1 - lost signal 
		exec( 1, 0x81,  0,0,0 ); 
		
		set(    0,0,0,  0,0,0 ); // no dir1
		exec( 0, 0x00,  0,0,0 );
		
		set(    1,0,0,  0,0,0 ); // dir 1 - set again  
		exec( 1, 0x81,  1,0,0 ); 
	}

	
	@Test
	public void testFlag_81_lost_signal() {
		d.tagFlags.setInt(0x81);
		d.inpEnable.tag.setInt(1);
		
		set(    1,0,0,  1,0,0 ); // dir1 ok
		exec( 1, 0x01,  0,0,0 );
		
		set(    1,0,0,  0,0,0 ); // dir 1 - lost signal 
		exec( 1, 0x81,  0,0,0 ); 
		
		set(    1,0,0,  1,0,0 ); // dir1 ok
		exec( 1, 0x01,  0,0,0 );

		set(    1,0,0,  0,0,0 ); // dir 1 - lost signal 
		exec( 1, 0x81,  0,0,0 ); 
		
		set(    0,0,0,  0,0,0 ); // no dir1
		exec( 0, 0x00,  0,0,0 );
		
		set(    1,0,0,  0,0,0 ); // dir 1 - set again  
		exec( 1, 0x81,  1,0,0 ); 
	}

	@Test
	public void testFlag_82_lost_signal() {
		d.tagFlags.setInt(0x82);
		d.inpEnable.tag.setInt(1);
		
		set(    1,0,0,  1,0,0 ); // dir1 ok
		exec( 1, 0x01,  1,0,0 );
		
		set(    1,0,0,  0,0,0 ); // dir 1 - lost signal 
		exec( 1, 0x81,  1,0,0 ); 
		
		set(    1,0,0,  1,0,0 ); // dir1 ok
		exec( 1, 0x01,  1,0,0 );

		set(    1,0,0,  0,0,0 ); // dir 1 - lost signal 
		exec( 1, 0x81,  1,0,0 ); 
		
		set(    0,0,0,  0,0,0 ); // no dir1
		exec( 0, 0x00,  0,0,0 );
		
		set(    1,0,0,  0,0,0 ); // dir 1 - set again  
		exec( 1, 0x81,  1,0,0 ); 
	}

	
	@Test
	public void testFlag_90_lost_signal() {
		d.tagFlags.setInt(0x90);
//		d.tagDelay.setInt(1);
		d.inpEnable.tag.setInt(1);
		
		set(    1,0,0,  0,0,0 ); // dir 1
		exec( 1, 0x81,  1,0,0 );
		exec( 1, 0x81,  0,0,0 );
		exec( 1, 0x81,  1,0,0 );
		exec( 1, 0x81,  0,0,0 );
		exec( 1, 0x81,  1,0,0 );
		exec( 1, 0x81,  0,0,0 );

		set(    1,0,0,  1,0,0 ); // dir1 ok
		exec( 1, 0x81,  1,0,0 );
		exec( 1, 0x01,  0,0,0 );
		exec( 1, 0x01,  0,0,0 );
		exec( 1, 0x01,  0,0,0 );
		
		set(    1,0,0,  0,0,0 ); // dir 1 - lost signal 
		exec( 1, 0x81,  0,0,0 ); 
		exec( 1, 0x81,  0,0,0 ); 
		exec( 1, 0x81,  1,0,0 ); 
		exec( 1, 0x81,  0,0,0 ); 
		exec( 1, 0x81,  1,0,0 ); 
		exec( 1, 0x81,  0,0,0 ); 
	}


	@Test
	public void testFlag_80_reset() {
		d.tagFlags.setInt(0x80);
		d.inpEnable.tag.setInt(1);
		
		set(    1,0,0,  1,0,0 ); // dir1 ok
		exec( 1, 0x01,  0,0,0 );
		
		set(    1,0,0,  0,0,0 ); // dir 1 - lost signal 
		exec( 1, 0x81,  0,0,0 ); 
		exec( 1, 0x81,  0,0,0 ); 
		exec( 1, 0x81,  0,0,0 ); 
		
		d.tagControl.setInt(0x80); // reset
		exec( 1, 0x81,  1,0,0 );
		assertEquals( 0, d.tagControl.getInt());
		exec( 1, 0x81,  0,0,0 ); 
		exec( 1, 0x81,  0,0,0 ); 
	}

	@Test
	public void testFlag_82_control() {
		d.tagFlags.setInt(0x82);
		d.inpEnable.tag.setInt(1);
		
		d.tagControl.setInt(0x301);		// dir 1
		set(    0,1,0,  0,0,0 ); 
		exec( 1, 0x81,  1,0,0 );
		exec( 1, 0x81,  1,0,0 );
		
		set(    1,0,0,  1,0,0 );
		exec( 1, 0x01,  1,0,0 );
		
		d.tagControl.setInt(2);		// dir 2
		set(    0,0,1,  0,0,0 ); 
		exec( 1, 0x82,  0,1,0 );
		exec( 1, 0x82,  0,1,0 );
		
		set(    0,1,0,  0,1,0 );
		exec( 1, 0x02,  0,1,0 );
		
		d.tagControl.setInt(3);		// dir 3
		set(    0,0,0,  0,0,0 ); 
		exec( 0, 0x83,  0,0,1 );
		exec( 0, 0x83,  0,0,1 );
		
		set(    0,0,0,  0,0,1 );
		exec( 0, 0x03,  0,0,1 );
	}
	
	@Test
	public void testFlag_82_external() {
		d.tagFlags.setInt(0x82);
		d.inpEnable.tag.setInt(1);
		
		d.tagControl.setInt(0x100);		// dir 1
		set(    0,1,0,  0,0,0 ); 
		exec( 1, 0x81,  1,0,0 );
		exec( 1, 0x81,  1,0,0 );
		
		set(    1,0,0,  1,0,0 );
		exec( 1, 0x01,  1,0,0 );
		
		d.tagControl.setInt(0x200);		// dir 2
		set(    0,0,1,  0,0,0 ); 
		exec( 1, 0x82,  0,1,0 );
		exec( 1, 0x82,  0,1,0 );
		
		set(    0,1,0,  0,1,0 );
		exec( 1, 0x02,  0,1,0 );
		
		d.tagControl.setInt(0x300);		// dir 3
		set(    0,0,0,  0,0,0 ); 
		exec( 1, 0x83,  0,0,1 );
		exec( 1, 0x83,  0,0,1 );
		
		set(    0,0,0,  0,0,1 );
		exec( 1, 0x03,  0,0,1 );
	}
	
}
