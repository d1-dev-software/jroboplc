package promauto.jroboplc.plugin.roboplant;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import promauto.jroboplc.core.api.InitUtils;


public class DeviceVALATest {

	private DeviceVALA d;

	@Mock RoboplantModule module;


	@Before
	public void setUp() throws Exception {
		InitUtils.setupLogger();
		
		d = new DeviceVALA();
		d.module = module;
		
		AuxProcs.createInputsOutputs(d);
	}

	
	private void exec(
			int ValOut,
			int ValSet
			) {
		
		d.execute();
		assertEquals("ValOut  "	, ValOut  , d.tagValOut  .getInt());     
		assertEquals("ValSet  "	, ValSet  , d.tagValSet  .getInt());     
	}



	@Test
	public void test_no_delay() {
		d.tagDlyEnOn.setInt(0);
		d.tagDlyEnOff.setInt(0);
		d.tagFlags.setInt(0);
		
		d.inpValIn.tag.setInt(0);
		d.inpEnable.tag.setInt(0);
		exec(0, 0);

		d.inpValIn.tag.setInt(111);
		exec(0, 111);

		d.inpEnable.tag.setInt(1);
		exec(111, 111);
		exec(111, 111);
		
		d.inpEnable.tag.setInt(0);
		exec(0, 111);
		exec(0, 111);
	}

	
	@Test
	public void test_with_delay() {
		d.tagDlyEnOn.setInt(2);
		d.tagDlyEnOff.setInt(3);
		d.tagFlags.setInt(0);
		
		d.inpValIn.tag.setInt(222);
		d.inpEnable.tag.setInt(0);
		exec(0, 222);
		exec(0, 222);
		exec(0, 222);

		d.inpEnable.tag.setInt(1);
		exec(0, 222);
		exec(0, 222);
		exec(222, 222);
		exec(222, 222);
		
		d.inpEnable.tag.setInt(0);
		exec(222, 222);
		exec(222, 222);
		exec(222, 222);
		exec(0, 222);
		exec(0, 222);
	}

	
	@Test
	public void test_valin_empty() {
		d.tagDlyEnOn.setInt(0);
		d.tagDlyEnOff.setInt(1);
		d.tagFlags.setInt(0);
		d.inpValIn.refaddr = 0x7000;
		d.inpValIn.refnum = 0;
		
		d.inpValIn.tag.setInt(333);
		d.inpEnable.tag.setInt(0);
		d.tagValSet.setInt(444);
		exec(0, 444);
		exec(0, 444);
		
		d.inpEnable.tag.setInt(1);
		exec(444, 444);
		exec(444, 444);

		d.inpEnable.tag.setInt(0);
		exec(444, 444);
		exec(0, 444);
		exec(0, 444);
	}
	
	
	@Test
	public void test_flags_1() {
		d.tagDlyEnOn.setInt(2);
		d.tagDlyEnOff.setInt(1);
		d.tagFlags.setInt(1);
		
		d.inpValIn.tag.setInt(555);
		d.inpEnable.tag.setInt(0);
		exec(555, 555);
		exec(555, 555);

		d.inpEnable.tag.setInt(1);
		exec(555, 555);
		exec(555, 555);
	}
	
	
	@Test
	public void test_flags_2() {
		d.tagDlyEnOn.setInt(0);
		d.tagDlyEnOff.setInt(1);
		d.tagFlags.setInt(2);
		
		d.inpValIn.tag.setInt(777);
		d.inpEnable.tag.setInt(0);
		d.tagValSet.setInt(888);
		exec(0, 888);
		exec(0, 888);
		
		d.inpEnable.tag.setInt(1);
		exec(888, 888);
		exec(888, 888);

		d.inpEnable.tag.setInt(0);
		exec(888, 888);
		exec(0, 888);
		exec(0, 888);
	}
	
	@Test
	public void test_flags_3() {
		d.tagDlyEnOn.setInt(3);
		d.tagDlyEnOff.setInt(2);
		d.tagFlags.setInt(3);
		
		d.inpValIn.tag.setInt(999);
		d.inpEnable.tag.setInt(0);
		d.tagValSet.setInt(888);
		exec(888, 888);
		exec(888, 888);
		
		d.inpEnable.tag.setInt(1);
		exec(888, 888);
		exec(888, 888);
	}
	
	
}



















