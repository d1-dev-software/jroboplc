package promauto.jroboplc.plugin.roboplant;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static promauto.jroboplc.plugin.roboplant.AuxProcs.newInputAndTag;

import java.nio.file.Paths;

import org.junit.Before;
import org.junit.Test;

import promauto.jroboplc.core.api.InitUtils;
import promauto.jroboplc.core.tags.TagInt;

public class DeviceSCNTest {
	private DeviceSCN d;
	
	@Before
	public void setUp() throws Exception {
		InitUtils.setupLogger();

		d = new DeviceSCN();
		
		d.inpInput.add( newInputAndTag() );
		d.inpInput.add( newInputAndTag() );
		d.inpInput.add( newInputAndTag() );
		
		d.tagOutput		= new TagInt("", 0);
		d.tagOper 		= new TagInt("", 0);
		
		d.tagValue.add( new TagInt("", 0) );
		d.tagValue.add( new TagInt("", 0) );
		d.tagValue.add( new TagInt("", 0) );
		
		d.inpAmount = 3;

//		d.resetState();
	}

	@Test
	public void testLoad() throws Exception {
		RoboplantModule robo = AuxProcs.createRoboplantModule();
		assertTrue( robo.loadProject( Paths.get( 
				InitUtils.getResourcesDir("roboplant", RoboplantModuleTest.class) + "scn.xml" )));

		assertEquals(1, robo.devicesByAddr.length);
		d = (DeviceSCN)robo.devicesByAddr[0];
		assertTrue( d.init(robo) );
		assertTrue( d.prepare() );

		assertEquals( "SCN", d.devtype );
		assertEquals( "S01", d.name );
		assertEquals( "S01", d.tagname );
		assertEquals(12, d.inpAmount);
		
		assertEquals(0x1122, d.inpInput.get(7).refaddr);
		assertEquals(0x33,   d.inpInput.get(7).refnum);
		assertEquals(0x44,   d.tagValue.get(7).getInt());

		assertEquals(0x1234, d.inpInput.get(11).refaddr);
		assertEquals(0x12,   d.inpInput.get(11).refnum);
		assertEquals(0x56,   d.tagValue.get(11).getInt());
	}
	

	
	private void exec(
			int oper, 
			int inp0, 
			int inp1, 
			int inp2, 
			int val0, 
			int val1, 
			int val2,
			int out
			) {
		d.tagOper.setInt(oper);

		d.inpInput.get(0).tag.setInt(inp0);
		d.inpInput.get(1).tag.setInt(inp1);
		d.inpInput.get(2).tag.setInt(inp2);

		d.tagValue.get(0).setInt(val0);
		d.tagValue.get(1).setInt(val1);
		d.tagValue.get(2).setInt(val2);
		
		d.execute();
		
		assertEquals("output", out, d.tagOutput.getInt());     
	}

	private void exec3(
			int oper, 
			int inp0, 
			int inp1, 
			int inp2, 
			int val0, 
			int val1, 
			int val2,
			int out
			) {
		d.tagOper.setInt(oper);

		d.inpInput.get(0).tag.setInt(inp0);
		d.inpInput.get(1).tag.setInt(inp1);
		d.inpInput.get(2).tag.setInt(inp2);

		
		d.execute();
		
		assertEquals("value_0", val0, d.tagValue.get(0).getInt());     
		assertEquals("value_1", val1, d.tagValue.get(1).getInt());     
		assertEquals("value_2", val2, d.tagValue.get(2).getInt());     
		assertEquals("output", out, d.tagOutput.getInt()); 
		
	}

	@Test
	public void testOper_0() {
		exec(0,  0,0,0,  0,0,0,  1);
		exec(0,  1,0,0,  0,0,0,  0);
		exec(0,  1,0,0,  1,0,0,  1);
		exec(0,  1,1,0,  0,0,0,  0);
		exec(0,  1,1,0,  1,1,0,  1);
		exec(0,  1,1,1,  0,0,0,  0);
		exec(0,  1,1,1,  1,1,1,  1);
	}

	@Test
	public void testOper_1() {
		exec(1,  0,0,0,  0,0,0,  1);
		exec(1,  1,0,0,  0,0,0,  1);
		exec(1,  1,0,0,  1,0,0,  1);
		exec(1,  1,1,0,  0,0,0,  1);
		exec(1,  1,1,0,  1,1,0,  1);
		exec(1,  1,1,1,  1,1,1,  1);
		exec(1,  1,1,1,  0,0,0,  0);
		exec(1,  0,0,0,  1,1,1,  0);
		exec(1,  1,0,1,  0,1,0,  0);
	}

	@Test
	public void testOper_2() {
		exec(2,  0,0,0,  0,0,0,  3);
		exec(2,  1,0,0,  0,0,0,  2);
		exec(2,  1,0,0,  1,0,0,  3);
		exec(2,  1,1,0,  0,0,0,  1);
		exec(2,  1,1,0,  1,1,0,  3);
		exec(2,  1,1,1,  0,0,0,  0);
		exec(2,  1,1,1,  1,1,1,  3);
	}

	@Test
	public void testOper_3() {
		exec3(3,  0,0,0,  1,0,0,  0);
		exec3(3,  1,0,0,  0,1,0,  1);
		exec3(3,  0,1,0,  0,0,1,  2);
		exec3(3,  1,1,0,  0,0,0,  3);
		exec3(3,  1,1,1,  0,0,0,  7);
	}

	@Test
	public void testOper_8() {
		exec3(8,  0,0,0,  0,0,0,  0);
		exec3(8,  1,0,0,  0,0,0,  0);
		exec3(8,  1,1,0,  1,1,0,  1);
		exec3(8,  1,1,1,  1,1,1,  1);
		exec3(8,  2,1,1,  0,1,1,  1);
		exec3(8,  2,3,1,  0,0,0,  0);
		exec3(8,  0,0,0,  0,0,0,  0);
	}
}
