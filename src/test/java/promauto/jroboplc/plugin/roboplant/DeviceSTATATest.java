package promauto.jroboplc.plugin.roboplant;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import promauto.jroboplc.core.api.InitUtils;
import promauto.jroboplc.core.tags.TagInt;

import static org.junit.Assert.assertEquals;


public class DeviceSTATATest {

	private DeviceSTATA d;

	@Mock RoboplantModule module;


	@Before
	public void setUp() throws Exception {
		InitUtils.setupLogger();

		d = new DeviceSTATA();
		d.module = module;

		AuxProcs.createInputsOutputs(d);

		d.tagMin.setInt(2);
		d.tagMax.setInt(5);

		for (int i = 0; i < DeviceSTATA.TIMES_SIZE; ++i) {
			d.times[i] = new DeviceSTATA.Time();
			d.times[i].tag = new TagInt("", 0);
		}
	}

	
	private void exec(
			int Count    ,
			int Total    ,
			int Time_0   ,
			int Time_1   ,
			int Time_2   ,
			int Time_3   ,
			int Time_4   ,
			int Time_5   ,
			int Time_6
			) {
		
		d.execute();
		assertEquals("Count "	, Count , d.tagCount .getInt());
		assertEquals("Total "	, Total , d.tagTimeTotal.getInt());
		assertEquals("Time_0"	, Time_0, d.times[0].tag.getInt());
		assertEquals("Time_1"	, Time_1, d.times[1].tag.getInt());
		assertEquals("Time_2"	, Time_2, d.times[2].tag.getInt());
		assertEquals("Time_3"	, Time_3, d.times[3].tag.getInt());
		assertEquals("Time_4"	, Time_4, d.times[4].tag.getInt());
		assertEquals("Time_5"	, Time_5, d.times[5].tag.getInt());
		assertEquals("Time_6"	, Time_6, d.times[6].tag.getInt());
	}



	@Test
	public void test() throws InterruptedException {
		exec(0,0, 0,0,0,0,0,0,0);
		exec(0,0, 0,0,0,0,0,0,0);

		d.inpInput.tag.setInt(1);
		exec(0,0, 0,0,0,0,0,0,0);

		d.inpInput.tag.setInt(2);
		exec(0,0, 0,0,0,0,0,0,0);
		Thread.sleep(1000);

		d.inpInput.tag.setInt(3);
		exec(0,0, 0,0,0,0,0,0,0);
		Thread.sleep(2000);

		d.inpInput.tag.setInt(0);
		exec(1,3, 0,0,1,2,0,0,0);

	}

	
}



















