package promauto.jroboplc.plugin.roboplant;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static promauto.jroboplc.plugin.roboplant.AuxProcs.newInputAndTag;

import java.nio.file.Paths;

import org.junit.Before;
import org.junit.Test;

import promauto.jroboplc.core.api.InitUtils;
import promauto.jroboplc.core.tags.TagInt;

public class DeviceCNTTest {
	private DeviceCNT d;
	
	@Before
	public void setUp() throws Exception {
		InitUtils.setupLogger();
		
		d = new DeviceCNT();
		d.inpInput 		= newInputAndTag();    
		d.inpReset 		= newInputAndTag();    
		d.tagCnt     	= new TagInt("", 0);
		d.tagLimit     	= new TagInt("", 0);
		d.tagCntLimit   = new TagInt("", 0);
		d.tagTime     	= new TagInt("", 0);
	}

	@Test
	public void testLoad() throws Exception {
		
		RoboplantModule robo = AuxProcs.createRoboplantModule();
		assertTrue( robo.loadProject( Paths.get( 
				InitUtils.getResourcesDir("roboplant", RoboplantModuleTest.class) + "cnt.xml" )));

		assertEquals(1, robo.devicesByAddr.length);
		DeviceCNT d = (DeviceCNT)robo.devicesByAddr[0];
		
		assertEquals( "CNT", d.devtype );
		assertEquals( "580_Dly", d.tagname );
		
		assertTrue( d.init(robo) );
		assertTrue( d.prepare() );
	}
	
	private void set(
			int input,
			int reset
			) {
		d.inpInput.tag.setInt(input);
		d.inpReset.tag.setInt(reset);
	}

	
	private void exec(
			int cnt		,
			int limit	
			) {
		d.execute();
		assertEquals("cnt	"	,	cnt		, d.tagCnt		.getInt());     
		assertEquals("limit	"	,	limit	, d.tagLimit	.getInt());     
	}


	@Test
	public void testExecute() {
		set(0,0);  exec(0,1);
		set(1,0);  exec(0,1);
		set(1,1);  exec(0,1);
	}

	@Test
	public void testExecute_cntlimit_3() {
		d.tagCntLimit.setInt(3);
		set(0,0);  exec(0,0);
		set(1,0);  exec(1,0);
		set(0,0);  exec(1,0);
		set(0,1);  exec(0,0);

		set(0,0);  exec(0,0);
		set(1,0);  exec(1,0);
		set(0,0);  exec(1,0);
		set(0,0);  exec(1,0);

		set(1,0);  exec(2,0);
		set(1,0);  exec(3,1);
		set(1,0);  exec(3,1);

		set(0,0);  exec(3,1);
		set(0,0);  exec(3,1);
		
		set(0,1);  exec(0,0);
		set(0,0);  exec(0,0);
		
		set(1,0);  exec(1,0);
		set(1,0);  exec(2,0);
		set(1,0);  exec(3,1);
		set(1,1);  exec(0,0);
		set(1,0);  exec(1,0);
		set(1,0);  exec(2,0);
		set(1,0);  exec(3,1);
	}

	
	@Test
	public void testExecute_time_2() {
		d.tagCntLimit.setInt(3);
		d.tagTime.setInt(2);
		set(0,0);  exec(0,0);
		set(1,0);  exec(0,0);
		set(1,0);  exec(1,0);
		set(1,0);  exec(1,0);
		set(1,0);  exec(2,0);
		set(1,0);  exec(2,0);
		set(1,0);  exec(3,1);
	}

}
