package promauto.jroboplc.plugin.roboplant;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import promauto.jroboplc.core.api.InitUtils;


public class DeviceSTMCTest {

	private DeviceSTMC d;

	@Mock RoboplantModule module;


	@Before
	public void setUp() throws Exception {
		InitUtils.setupLogger();
		
		d = new DeviceSTMC();
		d.module = module;
		
		AuxProcs.createInputsOutputs(d);
		
		d.tagCycleQnt      .setInt(2);
		d.tagPAlarm        .setInt(80);
		d.tagPWork         .setInt(70);
		d.tagPWorkMax      .setInt(70);
		d.tagPDelta        .setInt(5);
		d.tagPDeltaMax     .setInt(5);
		d.tagPOpenK6       .setInt(65);
		d.tagPOpenK5       .setInt(60);
		d.tagPLoad         .setInt(10);
		d.tagPUnload       .setInt(20);
		d.tagTimeBlowInfl  .setInt(4);
		d.tagTimeStartDly  .setInt(3);
		d.tagTimeLoad      .setInt(3);
		d.tagTimeInflMax   .setInt(3);
		d.tagTimeExp       .setInt(3);
		d.tagTimeExpMax    .setInt(9);
		d.tagTimeUnload    .setInt(3);
		d.tagTimeUnloadMin .setInt(3);
		d.tagTimeBlow      .setInt(3);
		d.tagTimeCycDly    .setInt(3);
		d.tagBlowQnt       .setInt(2);
		d.tagBlowQntMax    .setInt(3);
		    
		    
	}

	
	private void exec(
			int State    ,
			int Alarm    ,
			int Timer    ,
			
			int CycleNum ,
			int BlowNum  ,
			
			int OpenK1   ,
			int OpenK2   ,
			int OpenK3   ,
			int OpenK4   ,
			int OpenK5   ,
			int OpenK6   ,
			int OpenK7   ,
			int OpenK8   ,
			int OpenK9   ,

			int FullK6   ,
			
			int WaitCl   ,
			int WaitOp   ,
			int WaitEx   
			) {
		
		d.execute();
		assertEquals("State   "	, State   , d.tagState   .getInt());     
		assertEquals("Alarm   "	, Alarm   , d.tagAlarm   .getInt());     
		assertEquals("Timer   "	, Timer   , d.tagTimer   .getInt());     
		assertEquals("CycleNum"	, CycleNum, d.tagCycleNum.getInt());     
		assertEquals("BlowNum "	, BlowNum , d.tagBlowNum .getInt());     
		assertEquals("OpenK1  "	, OpenK1  , d.tagOpenK1  .getInt());     
		assertEquals("OpenK2  "	, OpenK2  , d.tagOpenK2  .getInt());     
		assertEquals("OpenK3  "	, OpenK3  , d.tagOpenK3  .getInt());     
		assertEquals("OpenK4  "	, OpenK4  , d.tagOpenK4  .getInt());     
		assertEquals("OpenK5  "	, OpenK5  , d.tagOpenK5  .getInt());     
		assertEquals("OpenK6  "	, OpenK6  , d.tagOpenK6  .getInt());     
		assertEquals("OpenK7  "	, OpenK7  , d.tagOpenK7  .getInt());     
		assertEquals("OpenK8  "	, OpenK8  , d.tagOpenK8  .getInt());     

		assertEquals("FullK6  "	, FullK6  , d.tagFullK6  .getInt());     

		assertEquals("WaitCl  "	, WaitCl  , d.tagWaitCl  .getInt());     
		assertEquals("WaitOp  "	, WaitOp  , d.tagWaitOp  .getInt());     
		assertEquals("WaitEx  "	, WaitEx  , d.tagWaitEx  .getInt());     
	}

	private void nextState(int state) {
		d.tagNextState.setInt(state);
		d.execute();
		assertEquals(state, d.tagState.getInt());
	}

	private void nextState(int stateSet, int stateGet) {
		d.tagNextState.setInt(stateSet);
		d.execute();
		assertEquals(stateGet, d.tagState.getInt());
	}

	

	@Test
	public void test() {
/*
		exec(0,0,-1, 0,0, 0,0,0,0,0,0,0,1,0, 0, 0,0,0);

		// STATE_OFF
		d.tagBtnStart.setInt(1);
		exec(1,0, 3, 0,0, 0,0,0,0,0,0,0,1,0, 0, 0,0,WAIT_TIMER);
		exec(1,0, 2, 0,0, 0,0,0,0,0,0,0,1,0, 0, 0,0,WAIT_TIMER);
		exec(1,0, 1, 0,0, 0,0,0,0,0,0,0,1,0, 0, 0,0,WAIT_TIMER);
		
		// STATE_BEFORE_LOAD
		exec(2,0, 0, 0,0, 0,0,0,0,1,0,0,1,0, 0, 0b01101111_00000000, 0, WAIT_PLINE + WAIT_DVU1);
		exec(2,0,-1, 0,0, 0,0,0,0,1,0,0,1,0, 0, 0b01101111_00000000, 0, WAIT_PLINE + WAIT_DVU1);
		
		d.inpClosedK1.tag.setInt(1);
		exec(2,0,-1, 0,0, 0,0,0,0,1,0,0,1,0, 0, 0b01101110_00000000, 0, WAIT_PLINE + WAIT_DVU1);

		d.inpClosedK2.tag.setInt(1);
		exec(2,0,-1, 0,0, 0,0,0,0,1,0,0,1,0, 0, 0b01101100_00000000, 0, WAIT_PLINE + WAIT_DVU1);

		d.inpClosedK3.tag.setInt(1);
		exec(2,0,-1, 0,0, 0,0,0,0,1,0,0,1,0, 0, 0b01101000_00000000, 0, WAIT_PLINE + WAIT_DVU1);

		d.inpClosedK4.tag.setInt(1);
		exec(2,0,-1, 0,0, 0,0,0,0,1,0,0,1,0, 0, 0b01100000_00000000, 0, WAIT_PLINE + WAIT_DVU1);

		d.inpClosedK6.tag.setInt(1);
		d.inpClosedK7.tag.setInt(1);
		exec(2,0,-1, 0,0, 0,0,0,0,1,0,0,1,0, 0, 0b00000000_00000000, 0, WAIT_PLINE + WAIT_DVU1);

		d.inpDVU1.tag.setInt(1);
		exec(2,0,-1, 0,0, 0,0,0,0,1,0,0,1,0, 0, 0b00000000_00000000, 0, WAIT_PLINE);

		// STATE_LOAD_OPEN
		d.inpPLineHigh.tag.setInt(1);
		exec(3,0,-1, 0,0, 1,0,0,0,1,0,0,1,0, 0, 0b00000000_00000001, 0, 0);
		
		// STATE_LOAD_DLY
		d.inpClosedK1.tag.setInt(0);
		exec(4,0, 3, 0,0, 1,0,0,0,1,0,0,1,0, 0, 0b00000000_00000000, 0, WAIT_TIMER);
		exec(4,0, 2, 0,0, 1,0,0,0,1,0,0,1,0, 0, 0b00000000_00000000, 0, WAIT_TIMER);
		exec(4,0, 1, 0,0, 1,0,0,0,1,0,0,1,0, 0, 0b00000000_00000000, 0, WAIT_TIMER);
		
		// STATE_LOAD_FINISH
		exec(5,0, 0, 0,0, 0,0,0,0,0,0,0,1,0, 0, 0b00010001_00000000, 0, 0);

		// STATE_INFLATE
		d.inpClosedK1.tag.setInt(1);
		d.inpClosedK5.tag.setInt(1);
		d.inpClosedK6.tag.setInt(1);
		exec(6,0, 3, 0,0, 0,0,1,0,0,0,0,1,0, 0, 0b00000000_00000000, 0, WAIT_PWORK);
		
		d.inpClosedK3.tag.setInt(0);
		d.inpP.tag.setInt(20);
		exec(6,0, 2, 0,0, 0,0,1,0,0,0,0,1,0, 0, 0b00000000_00000000, 0, WAIT_PWORK);
		
		d.inpP.tag.setInt(30);
		exec(6,0, 1, 0,0, 0,0,1,0,0,0,0,1,0, 0, 0b00000000_00000000, 0, WAIT_PWORK);
		
		d.inpP.tag.setInt(40);
		exec(6,1, 0, 0,0, 0,0,1,0,0,0,0,1,0, 0, 0b00000000_00000000, 0, WAIT_PWORK);

		d.inpP.tag.setInt(50);
		exec(6,1,-1, 0,0, 0,0,1,0,0,0,0,1,0, 0, 0b00000000_00000000, 0, WAIT_PWORK);

		d.inpP.tag.setInt(60);
		exec(6,1,-1, 0,0, 0,0,1,0,0,0,0,1,0, 0, 0b00000000_00000000, 0, WAIT_PWORK);

		// STATE_INFLATE_FINISH
		d.inpP.tag.setInt(70);
		exec(7,0,-1, 0,0, 0,0,0,0,0,0,0,1,0, 0, 0b00000100_00000000, 0, 0);

		// STATE_EXPOSITION
		d.inpClosedK3.tag.setInt(1);
		exec(8,0, 3, 0,0, 0,0,0,0,0,0,0,1,0, 0, 0b00000000_00000000, 0, WAIT_TIMER);
		exec(8,0, 2, 0,0, 0,0,0,0,0,0,0,1,0, 0, 0b00000000_00000000, 0, WAIT_TIMER);
		exec(8,0, 1, 0,0, 0,0,0,0,0,0,0,1,0, 0, 0b00000000_00000000, 0, WAIT_TIMER);

		// STATE_DEFLATE
		exec(9,0, 0, 0,0, 0,0,0,0,0,1,0,1,0, 0, 0b00000000_00000000, 0, WAIT_PUNLOAD);

		d.inpClosedK6.tag.setInt(0);
		d.inpP.tag.setInt(65);
		exec(9,0,-1, 0,0, 0,0,0,0,0,1,0,1,0, 1, 0b00000000_00000000, 0, WAIT_PUNLOAD);

		d.inpP.tag.setInt(60);
		exec(9,0,-1, 0,0, 0,0,0,0,1,1,0,1,0, 1, 0b00000000_00000000, 0, WAIT_PUNLOAD);

		d.inpClosedK5.tag.setInt(0);
		d.inpP.tag.setInt(30);
		exec(9,0,-1, 0,0, 0,0,0,0,1,1,0,1,0, 1, 0b00000000_00000000, 0, WAIT_PUNLOAD);

		// STATE_UNLOAD_OPEN
		d.inpP.tag.setInt(20);
		exec(10,0,-1, 0,0, 0,1,0,0,1,1,0,1,0, 1, 0b00000000_00000010, 0, 0);

		// STATE_UNLOAD_DLY
		d.inpClosedK2.tag.setInt(0);
		exec(11,0, 3, 0,0, 0,1,0,0,1,0,0,1,0, 0, 0b00000000_00000000, 0, WAIT_TIMER);
		exec(11,0, 2, 0,0, 0,1,0,0,1,0,0,1,0, 0, 0b00000000_00000000, 0, WAIT_TIMER);
		exec(11,0, 1, 0,0, 0,1,0,0,1,0,0,1,0, 0, 0b00000000_00000000, 0, WAIT_TIMER);

		// STATE_BLOW_PREPARE
		exec(12,0, 0, 0,0, 0,0,0,0,0,0,0,0,0, 0, 0b10110010_00000000, 0, 0);

		// STATE_BLOW_INFLATE
		d.inpClosedK2.tag.setInt(1);
		d.inpClosedK5.tag.setInt(1);
		d.inpClosedK6.tag.setInt(1);
		d.inpClosedK8.tag.setInt(1);
		exec(13,0,-1, 0,0, 0,0,0,0,0,0,1,0,0, 0, 0b00000000_00000000, 0, WAIT_PBLOW);

		d.inpClosedK7.tag.setInt(0);
		d.inpP.tag.setInt(25);
		exec(13,0,-1, 0,0, 0,0,0,0,0,0,1,0,0, 0, 0b00000000_00000000, 0, WAIT_PBLOW);

		// STATE_BLOW_K2_PREPARE
		d.inpP.tag.setInt(30);
		exec(14,0,-1, 0,0, 0,0,0,0,0,0,0,0,0, 0, 0b01000000_00000000, 0, 0);

		// STATE_BLOW_K2_OPEN
		d.inpClosedK7.tag.setInt(1);
		exec(15,0,-1, 0,0, 0,1,0,0,0,0,0,0,0, 0, 0, 0b00000010_00000000, 0);

		d.inpClosedK2.tag.setInt(0);
		exec(15,0,-1, 0,0, 0,1,0,0,0,0,0,0,0, 0, 0, 0b00000010_00000000, 0);

		// STATE_BLOW_K2_DLY
		d.inpOpenedK2.tag.setInt(1);
		d.inpP.tag.setInt(0);
		exec(16,0,3, 0,0, 0,1,0,0,0,0,0,0,0, 0, 0b00000000_00000000, 0, WAIT_TIMER);
		exec(16,0,2, 0,0, 0,1,0,0,0,0,0,0,0, 0, 0b00000000_00000000, 0, WAIT_TIMER);
		exec(16,0,1, 0,0, 0,1,0,0,0,0,0,0,0, 0, 0b00000000_00000000, 0, WAIT_TIMER);

		// STATE_BLOW_K2_CLOSE
		exec(17,0,0, 0,0, 0,0,0,0,0,0,0,0,0, 0, 0b00000010_00000000, 0, 0);

		d.inpOpenedK2.tag.setInt(0);
		exec(17,0,-1, 0,0, 0,0,0,0,0,0,0,0,0, 0, 0b00000010_00000000, 0, 0);

		d.inpClosedK2.tag.setInt(1);
		d.execute();
		assertEquals(13, d.tagState.getInt());
		assertEquals(1, d.tagBlowNum.getInt());
		
		nextState(STATE_BLOW_K2_PREPARE, STATE_BLOW_K2_OPEN);
		nextState(STATE_BLOW_K2_DLY);
		nextState(STATE_BLOW_K2_CLOSE, STATE_BLOW_INFLATE);

		// STATE_CYCLE_PAUSE
		exec(18,0,3, 0,2, 0,0,0,0,0,0,0,1,0, 0, 0b00000000_00000000, 0, WAIT_TIMER);
		exec(18,0,2, 0,2, 0,0,0,0,0,0,0,1,0, 0, 0b00000000_00000000, 0, WAIT_TIMER);
		exec(18,0,1, 0,2, 0,0,0,0,0,0,0,1,0, 0, 0b00000000_00000000, 0, WAIT_TIMER);
		
		// STATE_BEFORE_LOAD
		exec(2,0,0, 1,0, 0,0,0,0,0,0,0,1,0, 0, 0b00000000_00000000, 0, 0);

		d.tagBlowQnt.setInt(0);
		nextState(STATE_LOAD_OPEN);
		nextState(STATE_LOAD_DLY);
		nextState(STATE_LOAD_FINISH);
		nextState(STATE_INFLATE);
		nextState(STATE_INFLATE_FINISH);
		nextState(STATE_EXPOSITION);
		nextState(STATE_DEFLATE, STATE_UNLOAD_OPEN);
		nextState(STATE_UNLOAD_DLY);
		nextState(STATE_BLOW_PREPARE, STATE_CYCLE_PAUSE);

		// STATE_CYCLE_PAUSE
		exec(18,0,2, 1,0, 0,0,0,0,0,0,0,1,0, 0, 0b00000000_00000000, 0, WAIT_TIMER);
		exec(18,0,1, 1,0, 0,0,0,0,0,0,0,1,0, 0, 0b00000000_00000000, 0, WAIT_TIMER);

		// STATE_OFF
		exec(0,0,0, 2,0, 0,0,0,0,0,0,0,1,0, 0, 0, 0, 0);
		exec(0,0,-1, 2,0, 0,0,0,0,0,0,0,1,0, 0, 0, 0, 0);
*/
	}


	@Test
	public void test_palarm() {

		exec(0,0,-1, 0,0, 0,0,0,0,0,0,0,0,0, 0, 0,0,0);

		d.inpP.tag.setInt(80);
		exec(0,2,-1, 0,0, 0,0,0,0,0,0,0,0,0, 0, 0,0,0);
	}

	
}



















