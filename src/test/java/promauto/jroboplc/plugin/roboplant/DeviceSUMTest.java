package promauto.jroboplc.plugin.roboplant;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static promauto.jroboplc.plugin.roboplant.AuxProcs.newInputAndTag;

import java.nio.file.Paths;

import org.junit.Before;
import org.junit.Test;

import promauto.jroboplc.core.api.InitUtils;
import promauto.jroboplc.core.tags.TagInt;

public class DeviceSUMTest {
	private DeviceSUM d;
	
	@Before
	public void setUp() throws Exception {
		InitUtils.setupLogger();
		
		d = new DeviceSUM();

		d.tagOutput = new TagInt("", 0);
		d.inpInput.add( newInputAndTag() );
		d.inpInput.add( newInputAndTag() );
		d.inpInput.add( newInputAndTag() );
	}

	
	@Test
	public void testLoad() throws Exception {
		
		RoboplantModule robo = AuxProcs.createRoboplantModule();
		assertTrue( robo.loadProject( Paths.get( 
				InitUtils.getResourcesDir("roboplant", RoboplantModuleTest.class) + "sum.xml" )));

		assertEquals(1, robo.devicesByAddr.length);
		DeviceSUM d = (DeviceSUM)robo.devicesByAddr[0];
		
		assertEquals( "SUM", d.devtype );
		
		assertTrue( d.init(robo) );
		assertTrue( d.prepare() );
		
		assertEquals( "S05_DvuSum", d.name );
		assertEquals( "S05_DvuSum", d.tagname );
		assertEquals(4, d.inpInput.size());
	}

	private void set(
			int input0,
			int input1,
			int input2
			) {
		d.inpInput.get(0).tag.setInt(input0);
		d.inpInput.get(1).tag.setInt(input1);
		d.inpInput.get(2).tag.setInt(input2);
	}

	
	private void exec(
			int output 
			) {
		d.execute();
		assertEquals("output"	, output	 , d.tagOutput.getInt());     
	}


	@Test
	public void testExecute() {
		set(0,0,0); 	exec(0);
		set(1,0,0); 	exec(1);
		set(1,2,0); 	exec(3);
		set(1,2,3); 	exec(6);
	}
}
