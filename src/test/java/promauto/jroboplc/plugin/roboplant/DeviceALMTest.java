package promauto.jroboplc.plugin.roboplant;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static promauto.jroboplc.plugin.roboplant.AuxProcs.newInputAndTag;

import java.nio.file.Paths;

import org.junit.Before;
import org.junit.Test;

import promauto.jroboplc.core.api.InitUtils;
import promauto.jroboplc.core.tags.TagInt;

public class DeviceALMTest {
	private DeviceALM d;
	
	@Before
	public void setUp() throws Exception {
		InitUtils.setupLogger();
		
		d = new DeviceALM();
		d.inpInput.add( newInputAndTag() );    
		d.inpInput.add( newInputAndTag() );    
		d.inpInput.add( newInputAndTag() );    
		d.inpReset 		= newInputAndTag();    
		d.tagOutput     = new TagInt("", 0);
		d.inpAmount = d.inpInput.size(); 
		d.values = new int[ d.inpAmount ];
	}

	@Test
	public void testLoad() throws Exception {
		
		RoboplantModule robo = AuxProcs.createRoboplantModule();
		assertTrue( robo.loadProject( Paths.get( 
				InitUtils.getResourcesDir("roboplant", RoboplantModuleTest.class) + "alm.xml" )));

		assertEquals(1, robo.devicesByAddr.length);
		DeviceALM d = (DeviceALM)robo.devicesByAddr[0];
		
		assertEquals( "ALM", d.devtype );
		
		assertTrue( d.init(robo) );
		assertTrue( d.prepare() );

		assertEquals( "Beep1 MCHB", d.name );
		assertEquals( "T0000", d.tagname );
		
		assertEquals( 0x5d, d.inpInput.size() );
		assertEquals( 0x5d, d.inpAmount );

	}
	
	private void set(
			int reset,
			int input0,
			int input1,
			int input2) {
		d.inpReset.tag.setInt(reset);
		d.inpInput.get(0).tag.setInt(input0);
		d.inpInput.get(1).tag.setInt(input1);
		d.inpInput.get(2).tag.setInt(input2);
	}

	
	private void exec(
			int output       
			) {
		d.execute();
		assertEquals("output"	, output	, d.tagOutput.getInt());     
	}


	@Test
	public void testExecute() {
		set(0, 0,0,0);  exec(0);
		set(0, 1,0,0);  exec(1);
		set(1, 1,0,0);  exec(0);
		set(0, 1,0,0);  exec(0);
		set(0, 1,1,0);  exec(1);
		set(0, 1,0,0);  exec(0);
		set(0, 1,1,0);  exec(1);
		set(1, 1,1,0);  exec(0);
		set(0, 1,1,0);  exec(0);
		set(0, 0,1,0);  exec(0);
		set(0, 1,1,0);  exec(1);
		set(1, 1,1,1);  exec(0);
		set(0, 1,1,1);  exec(0);
		set(0, 1,0,0);  exec(0);
	}

}
