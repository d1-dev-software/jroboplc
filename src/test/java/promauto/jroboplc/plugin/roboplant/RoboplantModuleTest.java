package promauto.jroboplc.plugin.roboplant;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.xml.stream.XMLStreamException;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import promauto.jroboplc.core.api.InitUtils;
import promauto.jroboplc.core.api.Plugin;
import promauto.jroboplc.core.TagTable;

public class RoboplantModuleTest {
	private static String CFG_DIR = InitUtils.getResourcesDir("roboplant", RoboplantModuleTest.class);
	
	private RoboplantModule robo;
	@Mock private TagTable tbl;
	@Mock private Plugin plugin; 

	@Before
	public void setUp() throws Exception {
		InitUtils.setupLogger();
		
		robo = AuxProcs.createRoboplantModule();
	}

	
	@Test
	public void testLoadProject() throws XMLStreamException, IOException {
		Path prjfile = Paths.get(CFG_DIR + "1.xml");
		
		assertTrue( robo.loadProject(prjfile) );
		
		assertEquals(29, robo.devicesByAddr.length);
		assertEquals(2, robo.devicesByOrder.length);
		
		assertSame( robo.devicesByAddr[27], robo.devicesByOrder[1] );
		assertSame( robo.devicesByAddr[28], robo.devicesByOrder[0] );
		
		Device d = robo.devicesByAddr[27];
		
		assertEquals( "MDTA", d.devtype );
		assertEquals( "618_Tok", d.tagname );
		assertEquals( "618 Нория Ток", d.name );
		assertEquals( 0x1b, d.addr );
		assertEquals( 0x3d, d.order );
		
		assertEquals(2, d.inputs.size());
		assertEquals(19, d.outputs.size());
		
		Input inp = d.inputs.get(1);
		assertEquals( "MchaSost", inp.name );
		assertEquals( 0x1c, inp.refaddr );
		assertEquals( 1, inp.refnum );

		Output out = d.outputs.get(0);
		assertEquals( "Output", out.name );
		assertEquals( 0, out.num );
		assertEquals( false, out.initiated );
		assertEquals( 0, out.inival );

		out = d.outputs.get(5);
		assertEquals( "High", out.name );
		assertEquals( 5, out.num );
		assertEquals( true, out.initiated );
		assertEquals( 10000, out.inival );
	}

	
}
