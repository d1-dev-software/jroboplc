package promauto.jroboplc.plugin.roboplant;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static promauto.jroboplc.plugin.roboplant.AuxProcs.addTagToList;
import static promauto.jroboplc.plugin.roboplant.AuxProcs.newInputAndTag;

import java.nio.file.Paths;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import promauto.jroboplc.core.api.InitUtils;
import promauto.jroboplc.core.api.Tag;
import promauto.jroboplc.core.tags.TagInt;

public class DeviceSEQTest {
	private DeviceSEQ d;
	
	@Mock RoboplantModule module;

	
	@Before
	public void setUp() throws Exception {
		InitUtils.setupLogger();
		
		d = new DeviceSEQ();
		d.module = module;
		
		d.inpInput 		= newInputAndTag();    
		d.inpStop1   	= newInputAndTag();    
		d.inpStop2   	= newInputAndTag();    
		d.inpReset   	= newInputAndTag();    
		d.inpSuspend 	= newInputAndTag();    
		
		
		d.tagOutput     = new TagInt("", 0);
		d.tagStop1      = new TagInt("", 0);
		d.tagStop2      = new TagInt("", 0);
		d.tagMode       = new TagInt("", 0);
		d.tagCurOut     = new TagInt("", 0);
		d.tagCnt        = new TagInt("", 0);
		

		d.outAmount = 5;
		for (int i=0; i<d.outAmount; i++) {
			addTagToList( d.tagOut			, 0 );	
			addTagToList( d.tagTimeStart	, 0 );	
			addTagToList( d.tagTimeStopA	, 0 );	
			addTagToList( d.tagTimeStopB	, 0 );	
		}
		
//		d.resetState();
		
	}


	

	
	
	@Test
	public void testLoad() throws Exception {
		
		RoboplantModule robo = AuxProcs.createRoboplantModule();
		assertTrue( robo.loadProject( Paths.get( 
				InitUtils.getResourcesDir("roboplant", RoboplantModuleTest.class) + "seq.xml" )));

		assertEquals(1, robo.devicesByAddr.length);
		DeviceSEQ d = (DeviceSEQ)robo.devicesByAddr[0];
		
		assertEquals( "SEQ", d.devtype );
		assertEquals( "S01", d.tagname );
		
		assertTrue( d.init(robo) );
		assertTrue( d.prepare() );
		
		assertEquals(7, d.tagOut.size());
		assertEquals(7, d.tagTimeStart.size());
		assertEquals(7, d.tagTimeStopA.size());
		assertEquals(7, d.tagTimeStopB.size());
		assertEquals(7, d.outAmount);

	}

	
	
	private void exec(
			int output ,
			int stop1  ,
			int stop2  ,
			int mode   ,
			int curOut ,
			int cnt    
			) {
		
		d.execute();	
		assertEquals(  "output",   output,   d.tagOutput .getInt());       
		assertEquals(  "stop1 ",   stop1 ,   d.tagStop1  .getInt());       
		assertEquals(  "stop2 ",   stop2 ,   d.tagStop2  .getInt());       
		assertEquals(  "mode  ",   mode  ,   d.tagMode   .getInt());       
		assertEquals(  "curOut",   curOut,   d.tagCurOut .getInt());       
		assertEquals(  "cnt   ",   cnt   ,   d.tagCnt    .getInt());       
	}
	
	private void out(int...values) {
		for (int i=0; i<values.length; i++)
			assertEquals( "out["+i+"]", values[i], d.tagOut.get(i).getInt() );
	}

	private void set(List<Tag> tags, int...values) {
		for (int i=0; i<values.length; i++)
			tags.get(i).setInt(values[i]);
	}

	@Test
	public void testStartStop1() {
		exec(0, 0,0, 0,-1, 0);    out(0,0,0,0,0);

		d.inpInput.tag.setInt(1);
		exec(1, 0,0, 1, 5, 0);    out(1,1,1,1,1);

		d.inpStop1.tag.setInt(1);
		exec(0, 1,0, 5,-1, 0);    out(0,0,0,0,0);

		d.inpInput.tag.setInt(0);
		d.inpStop1.tag.setInt(0);
		exec(0, 1,0, 5,-1, 0);    out(0,0,0,0,0);

		d.inpReset.tag.setInt(1);
		exec(0, 0,0, 0,-1, 0);    out(0,0,0,0,0);
		d.inpReset.tag.setInt(0);
		exec(0, 0,0, 0,-1, 0);    out(0,0,0,0,0);
	}

	@Test
	public void testStartStop2() {
		exec(0, 0,0, 0,-1, 0);    out(0,0,0,0,0);

		d.inpInput.tag.setInt(1);
		exec(1, 0,0, 1, 5, 0);    out(1,1,1,1,1);

		d.inpStop2.tag.setInt(1);
		exec(0, 0,1, 6,-1, 0);    out(0,0,0,0,0);

		d.inpInput.tag.setInt(0);
		d.inpStop2.tag.setInt(0);
		exec(0, 0,1, 6,-1, 0);    out(0,0,0,0,0);

		d.inpReset.tag.setInt(1);
		exec(0, 0,0, 0,-1, 0);    out(0,0,0,0,0);
		d.inpReset.tag.setInt(0);
		exec(0, 0,0, 0,-1, 0);    out(0,0,0,0,0);
	}
	
	@Test
	public void testStartTime_continuous() {
		set(d.tagTimeStart, 1,2,3,0,1);

		d.inpInput.tag.setInt(1);
		exec(0, 0,0, 2, 0, 1);    out(0,0,0,0,0);
		exec(0, 0,0, 2, 1, 1);    out(1,0,0,0,0);
		exec(0, 0,0, 2, 1, 2);    out(1,0,0,0,0);
		exec(0, 0,0, 2, 2, 1);    out(1,1,0,0,0);
		exec(0, 0,0, 2, 2, 2);    out(1,1,0,0,0);
		exec(0, 0,0, 2, 2, 3);    out(1,1,0,0,0);
		exec(0, 0,0, 2, 4, 1);    out(1,1,1,1,0);
		exec(1, 0,0, 1, 5, 0);    out(1,1,1,1,1);
		exec(1, 0,0, 1, 5, 0);    out(1,1,1,1,1);
		exec(1, 0,0, 1, 5, 0);    out(1,1,1,1,1);
	}

	@Test
	public void testStartTime_interrupted() {
		set(d.tagTimeStart, 1,2,3,0,1);

		d.inpInput.tag.setInt(1);
		exec(0, 0,0, 2, 0, 1);    out(0,0,0,0,0);
		exec(0, 0,0, 2, 1, 1);    out(1,0,0,0,0);
		exec(0, 0,0, 2, 1, 2);    out(1,0,0,0,0);
		exec(0, 0,0, 2, 2, 1);    out(1,1,0,0,0);

		d.inpInput.tag.setInt(0);
		exec(0, 0,0, 2, 2, 1);    out(1,1,0,0,0);
		exec(0, 0,0, 2, 2, 1);    out(1,1,0,0,0);
		exec(0, 0,0, 2, 2, 1);    out(1,1,0,0,0);

		d.inpInput.tag.setInt(1);
		exec(0, 0,0, 2, 2, 2);    out(1,1,0,0,0);
		exec(0, 0,0, 2, 2, 3);    out(1,1,0,0,0);
		exec(0, 0,0, 2, 4, 1);    out(1,1,1,1,0);
		exec(1, 0,0, 1, 5, 0);    out(1,1,1,1,1);
		exec(1, 0,0, 1, 5, 0);    out(1,1,1,1,1);
		exec(1, 0,0, 1, 5, 0);    out(1,1,1,1,1);
	}


	@Test
	public void testStopTimeA_continuous() {
		set(d.tagTimeStopA, 1,2,3,0,1);

		d.inpInput.tag.setInt(1);
		exec(1, 0,0, 1, 5, 0);    out(1,1,1,1,1);

		d.inpStop1.tag.setInt(1);
		exec(0, 0,0, 3, 4, 1);    out(1,1,1,1,1);
		exec(0, 0,0, 3, 2, 1);    out(1,1,1,0,0);
		exec(0, 0,0, 3, 2, 2);    out(1,1,1,0,0);
		exec(0, 0,0, 3, 2, 3);    out(1,1,1,0,0);
		exec(0, 0,0, 3, 1, 1);    out(1,1,0,0,0);
		exec(0, 0,0, 3, 1, 2);    out(1,1,0,0,0);
		exec(0, 0,0, 3, 0, 1);    out(1,0,0,0,0);
		exec(0, 1,0, 5,-1, 0);    out(0,0,0,0,0);

	}

	@Test
	public void testStopTimeA_interrupted() {
		set(d.tagTimeStopA, 1,2,3,0,1);

		d.inpInput.tag.setInt(1);
		exec(1, 0,0, 1, 5, 0);    out(1,1,1,1,1);
		d.inpInput.tag.setInt(0);

		d.inpStop1.tag.setInt(1);
		exec(0, 0,0, 3, 4, 1);    out(1,1,1,1,1);
		exec(0, 0,0, 3, 2, 1);    out(1,1,1,0,0);
		
		// stop process can't be interrupted
		d.inpStop1.tag.setInt(0);

		exec(0, 0,0, 3, 2, 2);    out(1,1,1,0,0);
		exec(0, 0,0, 3, 2, 3);    out(1,1,1,0,0);
		exec(0, 0,0, 3, 1, 1);    out(1,1,0,0,0);
		exec(0, 0,0, 3, 1, 2);    out(1,1,0,0,0);
		exec(0, 0,0, 3, 0, 1);    out(1,0,0,0,0);
		exec(0, 1,0, 5,-1, 0);    out(0,0,0,0,0);

	}

	
	@Test
	public void testStop1_Stop2_prioritet() {
		set(d.tagTimeStopA, 11,12,13,10,11);
		set(d.tagTimeStopB, 1,2,3,0,1);

		d.inpInput.tag.setInt(1);
		exec(1, 0,0, 1, 5, 0);    out(1,1,1,1,1);
		d.inpInput.tag.setInt(0);

		d.inpStop1.tag.setInt(1);
		d.inpStop2.tag.setInt(1);
		exec(0, 0,0, 4, 4, 1);    out(1,1,1,1,1);
		exec(0, 0,0, 4, 2, 1);    out(1,1,1,0,0);
		exec(0, 0,0, 4, 2, 2);    out(1,1,1,0,0);
		exec(0, 0,0, 4, 2, 3);    out(1,1,1,0,0);
		exec(0, 0,0, 4, 1, 1);    out(1,1,0,0,0);
		exec(0, 0,0, 4, 1, 2);    out(1,1,0,0,0);
		exec(0, 0,0, 4, 0, 1);    out(1,0,0,0,0);
		exec(0, 0,1, 6,-1, 0);    out(0,0,0,0,0);

	}

	
	@Test
	public void testSuspend() {
		set(d.tagTimeStart, 1,2,3,0,1);

		d.inpInput.tag.setInt(1);
		exec(0, 0,0, 2, 0, 1);    out(0,0,0,0,0);
		exec(0, 0,0, 2, 1, 1);    out(1,0,0,0,0);
		exec(0, 0,0, 2, 1, 2);    out(1,0,0,0,0);
		exec(0, 0,0, 2, 2, 1);    out(1,1,0,0,0);

		d.inpSuspend.tag.setInt(1);
		exec(0, 0,0, 7, 2, 1);    out(1,1,0,0,0);
		exec(0, 0,0, 7, 2, 1);    out(1,1,0,0,0);
		exec(0, 0,0, 7, 2, 1);    out(1,1,0,0,0);

		d.inpSuspend.tag.setInt(0);
		exec(0, 0,0, 2, 2, 1);    out(1,1,0,0,0);

		exec(0, 0,0, 2, 2, 2);    out(1,1,0,0,0);
		exec(0, 0,0, 2, 2, 3);    out(1,1,0,0,0);
		exec(0, 0,0, 2, 4, 1);    out(1,1,1,1,0);
		exec(1, 0,0, 1, 5, 0);    out(1,1,1,1,1);
		exec(1, 0,0, 1, 5, 0);    out(1,1,1,1,1);
		exec(1, 0,0, 1, 5, 0);    out(1,1,1,1,1);
	}

	@Test
	public void testSuspend_interrupted() {
		set(d.tagTimeStart, 1,2,3,0,1);

		d.inpInput.tag.setInt(1);
		exec(0, 0,0, 2, 0, 1);    out(0,0,0,0,0);
		exec(0, 0,0, 2, 1, 1);    out(1,0,0,0,0);
		exec(0, 0,0, 2, 1, 2);    out(1,0,0,0,0);
		exec(0, 0,0, 2, 2, 1);    out(1,1,0,0,0);

		d.inpSuspend.tag.setInt(1);
		exec(0, 0,0, 7, 2, 1);    out(1,1,0,0,0);
		d.inpInput.tag.setInt(0);
		exec(0, 0,0, 7, 2, 1);    out(1,1,0,0,0);
		exec(0, 0,0, 7, 2, 1);    out(1,1,0,0,0);
		exec(0, 0,0, 7, 2, 1);    out(1,1,0,0,0);

		d.inpSuspend.tag.setInt(0);
		exec(0, 0,0, 7, 2, 1);    out(1,1,0,0,0);
		exec(0, 0,0, 7, 2, 1);    out(1,1,0,0,0);
		exec(0, 0,0, 7, 2, 1);    out(1,1,0,0,0);

		d.inpInput.tag.setInt(1);
		exec(0, 0,0, 2, 2, 1);    out(1,1,0,0,0);

		exec(0, 0,0, 2, 2, 2);    out(1,1,0,0,0);
		exec(0, 0,0, 2, 2, 3);    out(1,1,0,0,0);
		exec(0, 0,0, 2, 4, 1);    out(1,1,1,1,0);
		exec(1, 0,0, 1, 5, 0);    out(1,1,1,1,1);
		exec(1, 0,0, 1, 5, 0);    out(1,1,1,1,1);
		exec(1, 0,0, 1, 5, 0);    out(1,1,1,1,1);
	}

}
