package promauto.jroboplc.plugin.roboplant;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import promauto.jroboplc.core.api.InitUtils;

import static org.junit.Assert.assertEquals;


public class DeviceMEMATest {

	private DeviceMEMA d;

	@Mock RoboplantModule module;


	@Before
	public void setUp() throws Exception {
		InitUtils.setupLogger();
		
		d = new DeviceMEMA();
		d.module = module;
		
		AuxProcs.createInputsOutputs(d);
		
		d.tagOper	     .setInt(0);
	}



    private void exec(
            int Output
    ) {
        d.execute();
        assertEquals("Output"	, Output   , d.tagOutput   .getInt());
    }


    @Test
	public void test() {
        exec(0);

        d.inpInput1.tag.setInt(1);
        d.inpInput2.tag.setInt(2);
        d.inpValue.tag.setInt(3);
        exec(0);

        d.inpInput1.tag.setInt(2);
        exec(3);

        d.inpInput1.tag.setInt(4);
        d.inpValue.tag.setInt(5);
        exec(3);

        d.inpInput1.tag.setInt(0);
        d.inpValue.tag.setInt(6);
        exec(3);

        d.inpInput1.tag.setInt(3);
        d.inpValue.tag.setInt(7);
        exec(7);
    }




}



















