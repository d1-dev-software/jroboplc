package promauto.jroboplc.plugin.roboplant;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static promauto.jroboplc.plugin.roboplant.AuxProcs.newInputAndTag;

import java.nio.file.Paths;

import org.junit.Before;
import org.junit.Test;

import promauto.jroboplc.core.api.InitUtils;
import promauto.jroboplc.core.tags.TagInt;

public class DeviceKARTUTest {
	private DeviceKARTU d;	
	
	@Before
	public void setUp() throws Exception {
		InitUtils.setupLogger();

		d = new DeviceKARTU();		
		d.inpWork     = newInputAndTag();
		d.inpClean    = newInputAndTag();
		d.inpDVU      = newInputAndTag();
		d.inpCls1     = newInputAndTag();
		d.inpMdl1     = newInputAndTag();
		d.inpOpn1     = newInputAndTag();
		d.inpCls2     = newInputAndTag();
		d.inpMdl2     = newInputAndTag();
		d.inpOpn2     = newInputAndTag();
		d.inpCls3     = newInputAndTag();
		d.inpMdl3     = newInputAndTag();
		d.inpOpn3     = newInputAndTag();
		d.inpEmrgStop = newInputAndTag();
		
		d.tagState      = new TagInt("", 0);
		d.tag_Wrk       = new TagInt("", 0);
		d.tag_Cln       = new TagInt("", 0);
		d.tag_Cls1      = new TagInt("", 0);
		d.tag_Mdl1      = new TagInt("", 0);
		d.tag_Opn1      = new TagInt("", 0);
		d.tag_Cls2      = new TagInt("", 0);
		d.tag_Mdl2      = new TagInt("", 0);
		d.tag_Opn2      = new TagInt("", 0);
		d.tag_Cls3      = new TagInt("", 0);
		d.tag_Mdl3      = new TagInt("", 0);
		d.tag_Opn3      = new TagInt("", 0);
		d.tag_Blow1     = new TagInt("", 0);
		d.tag_Blow2     = new TagInt("", 0);
		d.tag_Blow3     = new TagInt("", 0);
		d.tag_Blow4     = new TagInt("", 0);
		d.tagCntClean   = new TagInt("", 0);

		d.tagTimeOpCls  = new TagInt("", 0);
		d.tagTimeMdlOpn = new TagInt("", 0);
		d.tagTimeClean  = new TagInt("", 0);
		d.tagTimeBlow1  = new TagInt("", 0);
		d.tagTimeBlow2  = new TagInt("", 0);
		d.tagTimeBlow3  = new TagInt("", 0);
		d.tagTimeBlow4  = new TagInt("", 0);
		
		
				
	}

	@Test
	public void testLoad() throws Exception {
		RoboplantModule robo = AuxProcs.createRoboplantModule();
		assertTrue( robo.loadProject( Paths.get( 
				InitUtils.getResourcesDir("roboplant", RoboplantModuleTest.class) + "kartu.xml" )));

		assertEquals(1, robo.devicesByAddr.length);
		d = (DeviceKARTU)robo.devicesByAddr[0];
		assertTrue( d.init(robo) );
		assertTrue( d.prepare() );

		assertEquals( "KARTU", d.devtype );
		assertEquals( "3.3", d.name );
		assertEquals( "3_3", d.tagname );
	}
	
	private void setInps(
			int Work     ,
			int Clean    ,
			int DVU      ,
			int Cls1     ,
			int Mdl1     ,
			int Opn1     ,
			int Cls2     ,
			int Mdl2     ,
			int Opn2     ,
			int Cls3     ,
			int Mdl3     ,
			int Opn3     ,
			int EmrgStop
			) {
		d.inpWork    .tag.setInt(Work    );
		d.inpClean   .tag.setInt(Clean   );
		d.inpDVU     .tag.setInt(DVU     );
		d.inpCls1    .tag.setInt(Cls1    );
		d.inpMdl1    .tag.setInt(Mdl1    );
		d.inpOpn1    .tag.setInt(Opn1    );
		d.inpCls2    .tag.setInt(Cls2    );
		d.inpMdl2    .tag.setInt(Mdl2    );
		d.inpOpn2    .tag.setInt(Opn2    );
		d.inpCls3    .tag.setInt(Cls3    );
		d.inpMdl3    .tag.setInt(Mdl3    );
		d.inpOpn3    .tag.setInt(Opn3    );
		d.inpEmrgStop.tag.setInt(EmrgStop);
	}

	private void setTimes(
			int TimeOpCls ,
			int TimeMdlOpn,
			int TimeClean ,
			int TimeBlow1 ,
			int TimeBlow2 ,
			int TimeBlow3 ,
			int TimeBlow4
			) {
		d.tagTimeOpCls     .setInt(TimeOpCls );
		d.tagTimeMdlOpn    .setInt(TimeMdlOpn);
		d.tagTimeClean     .setInt(TimeClean );
		d.tagTimeBlow1     .setInt(TimeBlow1 );
		d.tagTimeBlow2     .setInt(TimeBlow2 );
		d.tagTimeBlow3     .setInt(TimeBlow3 );
		d.tagTimeBlow4     .setInt(TimeBlow4 );
	}
	
	
	private void exec(
			int State   ,
			int _Wrk    ,
			int _Cln    ,
			int _Cls1   ,
			int _Mdl1   ,
			int _Opn1   ,
			int _Cls2   ,
			int _Mdl2   ,
			int _Opn2   ,
			int _Cls3   ,
			int _Mdl3   ,
			int _Opn3   ,
			int _Blow1  ,
			int _Blow2  ,
			int _Blow3  ,
			int _Blow4  ,
			int CntClean
			) {
		d.execute();
		assertEquals("State   "	, 	State    	, d.tagState   	.getInt());     
		assertEquals("_Wrk    "	, 	_Wrk     	, d.tag_Wrk    	.getInt());     
		assertEquals("_Cln    "	, 	_Cln     	, d.tag_Cln    	.getInt());     
		assertEquals("_Cls1   "	, 	_Cls1    	, d.tag_Cls1   	.getInt());     
		assertEquals("_Mdl1   "	, 	_Mdl1    	, d.tag_Mdl1   	.getInt());     
		assertEquals("_Opn1   "	, 	_Opn1    	, d.tag_Opn1   	.getInt());     
		assertEquals("_Cls2   "	, 	_Cls2    	, d.tag_Cls2   	.getInt());     
		assertEquals("_Mdl2   "	, 	_Mdl2    	, d.tag_Mdl2   	.getInt());     
		assertEquals("_Opn2   "	, 	_Opn2    	, d.tag_Opn2   	.getInt());     
		assertEquals("_Cls3   "	, 	_Cls3    	, d.tag_Cls3   	.getInt());     
		assertEquals("_Mdl3   "	, 	_Mdl3    	, d.tag_Mdl3   	.getInt());     
		assertEquals("_Opn3   "	, 	_Opn3    	, d.tag_Opn3   	.getInt());     
		assertEquals("_Blow1  "	, 	_Blow1   	, d.tag_Blow1  	.getInt());     
		assertEquals("_Blow2  "	, 	_Blow2   	, d.tag_Blow2  	.getInt());     
		assertEquals("_Blow3  "	, 	_Blow3   	, d.tag_Blow3  	.getInt());     
		assertEquals("_Blow4  "	, 	_Blow4   	, d.tag_Blow4  	.getInt());     
		assertEquals("CntClean"	, 	CntClean 	, d.tagCntClean	.getInt());     
	}

// from state 0
	@Test
	public void testToState1_Opened() {
		setTimes(0, 0, 0, 0, 0, 0, 0);			 
		setInps(1,0, 0, 0,0,1, 0,0,1, 0,0,1, 0);  exec(1, 0,0, 0,0,0, 0,0,0, 0,0,0, 0,0,0,0, 0);	// start from all open pos
		setInps(1,0, 0, 0,0,1, 0,0,1, 0,0,1, 0);  exec(1, 0,0, 1,0,0, 1,0,0, 1,0,0, 0,0,0,0, 0);	
	}
	
	@Test
	public void testToState1_OpenedTime() {
		setTimes(5, 0, 0, 0, 0, 0, 0);
		setInps(1,0, 0, 0,0,1, 0,0,1, 0,0,1, 0);  exec(1, 0,0, 0,0,0, 0,0,0, 0,0,0, 0,0,0,0, 0);	// start from all open pos
		setInps(1,0, 0, 0,0,1, 0,0,1, 0,0,1, 0);  exec(1, 0,0, 1,0,0, 1,0,0, 1,0,0, 0,0,0,0, 0);
	}
	
	@Test
	public void testToState1_Mdl() {
		setTimes(0, 0, 0, 0, 0, 0, 0);		
		setInps(1,0, 0, 0,1,0, 0,1,0, 0,1,0, 0);  exec(1, 0,0, 0,0,0, 0,0,0, 0,0,0, 0,0,0,0, 0);
	}
	
	@Test
	public void testToState1_Clsd() {
		setTimes(0, 0, 0, 0, 0, 0, 0);		
		setInps(1,0, 0, 1,0,0, 1,0,0, 0,0,0, 0);  exec(1, 0,0, 0,0,0, 0,0,0, 0,0,0, 0,0,0,0, 0);
	}	

	@Test
	public void testWork() {
		setTimes(1, 0, 0, 0, 0, 0, 0);
		setInps(1,0, 0, 1,0,0, 1,0,0, 0,0,0, 0);  exec(1, 0,0, 0,0,0, 0,0,0, 0,0,0, 0,0,0,0, 0);
		setInps(1,0, 0, 1,0,0, 1,0,0, 1,0,0, 0);  exec(1, 0,0, 0,0,0, 0,0,0, 0,0,0, 0,0,0,0, 0);	// time opcls --
		setInps(1,0, 0, 1,0,0, 1,0,0, 1,0,0, 0);  exec(2, 0,0, 0,0,0, 0,0,0, 0,0,0, 0,0,0,0, 0);
		setInps(1,0, 0, 1,0,0, 1,0,0, 1,0,0, 0);  exec(2, 0,0, 0,1,0, 0,1,0, 0,1,0, 0,0,0,0, 0);	// time opcls --
		setInps(1,0, 0, 0,1,0, 0,1,0, 0,1,0, 0);  exec(3, 1,0, 0,1,0, 0,1,0, 0,1,0, 0,0,0,0, 0);		
		setInps(0,0, 1, 0,1,0, 0,1,0, 0,1,0, 0);  exec(3, 1,0, 0,1,0, 0,1,0, 0,1,0, 0,0,0,0, 0);
		setInps(0,0, 0, 0,1,0, 0,1,0, 0,1,0, 0);  exec(4, 1,0, 0,0,0, 0,0,0, 0,0,0, 0,0,0,0, 0);
		setInps(0,0, 0, 0,1,0, 0,1,0, 0,1,0, 0);  exec(5, 0,0, 0,0,0, 0,0,0, 0,0,0, 0,0,0,0, 0);
		setInps(0,0, 0, 0,0,0, 0,0,0, 0,1,0, 0);  exec(5, 0,0, 1,0,0, 1,0,0, 1,0,0, 0,0,0,0, 0);		
	}	
	
	@Test
	public void testClean() {
		setTimes(1, 0, 10, 1, 1, 1, 1);
		setInps(0,1, 0, 1,0,0, 1,0,0, 1,0,0, 0);  exec(6, 0,0, 0,0,0, 0,0,0, 0,0,0, 0,0,0,0, 0); //1
		setInps(0,1, 0, 1,0,0, 1,0,0, 1,0,0, 0);  exec(7, 0,0, 0,0,1, 0,0,1, 0,0,1, 0,0,0,0, 0); //2
		setInps(0,1, 0, 0,0,1, 0,0,1, 0,0,1, 0);  exec(7, 0,0, 0,0,1, 0,0,1, 0,0,1, 0,0,0,0, 0); //3 	// just opened		
		
		setInps(0,1, 0, 0,0,1, 0,0,1, 0,0,1, 0);  exec(8, 0,0, 0,0,1, 0,0,1, 0,0,1, 0,0,0,0, 0); //4
		setInps(0,1, 0, 0,0,1, 0,0,1, 0,0,1, 0);  exec(9, 0,0, 0,0,1, 0,0,1, 0,0,1, 1,0,0,0, 0); //5		
		setInps(0,1, 0, 0,0,1, 0,0,1, 0,0,1, 0);  exec(10,0,1, 0,0,1, 0,0,1, 0,0,1, 1,0,0,0, 0); //6 
		setInps(0,1, 0, 0,0,1, 0,0,1, 0,0,1, 0);  exec(10,0,1, 0,0,1, 0,0,1, 0,0,1, 0,0,0,0, 1); //7
		
		setInps(0,1, 0, 0,0,1, 0,0,1, 0,0,1, 0);  exec(10,0,1, 0,0,1, 0,0,1, 0,0,1, 0,1,0,0, 2); //8
		setInps(0,1, 0, 0,0,1, 0,0,1, 0,0,1, 0);  exec(10,0,1, 0,0,1, 0,0,1, 0,0,1, 0,1,0,0, 3); //9 
		setInps(0,1, 0, 0,0,1, 0,0,1, 0,0,1, 0);  exec(10,0,1, 0,0,1, 0,0,1, 0,0,1, 0,0,0,0, 4); //10
		setInps(0,1, 0, 0,0,1, 0,0,1, 0,0,1, 0);  exec(10,0,1, 0,0,1, 0,0,1, 0,0,1, 0,0,1,0, 5); //11
		setInps(0,1, 0, 0,0,1, 0,0,1, 0,0,1, 0);  exec(10,0,1, 0,0,1, 0,0,1, 0,0,1, 0,0,1,0, 6); //12
		setInps(0,1, 0, 0,0,1, 0,0,1, 0,0,1, 0);  exec(10,0,1, 0,0,1, 0,0,1, 0,0,1, 0,0,0,0, 7); //13
		setInps(0,1, 0, 0,0,1, 0,0,1, 0,0,1, 0);  exec(10,0,1, 0,0,1, 0,0,1, 0,0,1, 0,0,0,1, 8); //14
		setInps(0,1, 0, 0,0,1, 0,0,1, 0,0,1, 0);  exec(10,0,1, 0,0,1, 0,0,1, 0,0,1, 0,0,0,1, 9); //15
		setInps(0,1, 0, 0,0,1, 0,0,1, 0,0,1, 0);  exec(10,0,1, 0,0,1, 0,0,1, 0,0,1, 0,0,0,0, 10);//16
		setInps(0,1, 0, 0,0,1, 0,0,1, 0,0,1, 0);  exec(10,0,1, 0,0,1, 0,0,1, 0,0,1, 1,0,0,0, 11);//17

		setInps(0,1, 0, 0,0,1, 0,0,1, 0,0,1, 0);  exec(11,0,1, 0,0,0, 0,0,0, 0,0,0, 0,0,0,0, 11);//18
		setInps(0,1, 0, 1,0,0, 1,0,0, 1,0,0, 0);  exec(12,0,0, 0,0,0, 0,0,0, 0,0,0, 0,0,0,0, 11);//19
		setInps(0,1, 0, 1,0,0, 1,0,0, 1,0,0, 0);  exec(0, 0,0, 0,0,0, 0,0,0, 0,0,0, 0,0,0,0, 11);//20
		setInps(0,1, 0, 1,0,0, 1,0,0, 1,0,0, 0);  exec(0, 0,0, 0,0,0, 0,0,0, 0,0,0, 0,0,0,0, 11);//21
	}
	
}
