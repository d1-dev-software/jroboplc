package promauto.jroboplc.plugin.roboplant;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static promauto.jroboplc.plugin.roboplant.AuxProcs.newInputAndTag;

import java.nio.file.Paths;

import org.junit.Before;
import org.junit.Test;

import promauto.jroboplc.core.api.InitUtils;
import promauto.jroboplc.core.tags.TagInt;

public class DeviceANDORTest {
	private DeviceANDOR d;
	
	@Before
	public void setUp() throws Exception {
		InitUtils.setupLogger();
		
		d = new DeviceANDOR();
		d.inpInpNum		  = newInputAndTag();    
		d.inpInput0 	  = newInputAndTag();     
		d.inpInput1  	  = newInputAndTag(); 
		d.tagOutput       = new TagInt("", 0);
	}

	@Test
	public void testLoad() throws Exception {
		
		RoboplantModule robo = AuxProcs.createRoboplantModule();
		assertTrue( robo.loadProject( Paths.get( 
				InitUtils.getResourcesDir("roboplant", RoboplantModuleTest.class) + "andor.xml" )));

		assertEquals(1, robo.devicesByAddr.length);
		DeviceANDOR d = (DeviceANDOR)robo.devicesByAddr[0];
		
		assertEquals( "ANDOR", d.devtype );
		
		assertTrue( d.init(robo) );
		assertTrue( d.prepare() );

		assertEquals( "S05_05", d.name );
		assertEquals( "T0000", d.tagname );

	}
	
	private void set(
			int inpnum,
			int input0,
			int input1) {
		d.inpInpNum.tag.setInt(inpnum);
		d.inpInput0.tag.setInt(input0);
		d.inpInput1.tag.setInt(input1);
	}

	@Test
	public void testExecute() {
		set(0,0,0);
		assertTrue( d.execute() );
		assertEquals( 0, d.tagOutput.getInt());
		
		set(0,0,3);
		assertTrue( d.execute() );
		assertEquals( 0, d.tagOutput.getInt());

		set(0,2,0);
		assertTrue( d.execute() );
		assertEquals( 2, d.tagOutput.getInt());

		set(0,2,3);
		assertTrue( d.execute() );
		assertEquals( 2, d.tagOutput.getInt());

		set(1,0,0);
		assertTrue( d.execute() );
		assertEquals( 0, d.tagOutput.getInt());

		set(1,0,3);
		assertTrue( d.execute() );
		assertEquals( 3, d.tagOutput.getInt());

		set(1,2,0);
		assertTrue( d.execute() );
		assertEquals( 0, d.tagOutput.getInt());

		set(1,2,3);
		assertTrue( d.execute() );
		assertEquals( 3, d.tagOutput.getInt());
	}

}
