package promauto.jroboplc.plugin.roboplant;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static promauto.jroboplc.plugin.roboplant.AuxProcs.addTagToList;

import java.nio.file.Paths;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import promauto.jroboplc.core.api.InitUtils;
import promauto.jroboplc.core.api.Tag;
import promauto.jroboplc.core.tags.TagInt;

public class DeviceSEQDTest {

	private DeviceSEQD d;
	
	Tag tagInpStartBan; 	
	Tag tagInpStop;		
	Tag tagInpStopStart;	
	Tag tagInpStopRun; 	
	Tag tagInpStopFlow;
	Tag tagInpStopDly; 	
	Tag tagInpSusp; 	
	
	Tag tagStopDlyTime; 	
	Tag tagMesStartBan; 	
	Tag tagMesStop; 		
	Tag tagMesStopStart;	
	Tag tagMesStopRun; 	
	Tag tagMesStopFlow 	;
	Tag tagMesStopDly; 	
	Tag tagMesSusp; 		
	Tag tagTimeStart0; 		
	Tag tagTimeStart1; 		
	Tag tagTimeStart2; 		
	Tag tagTimeStop0; 		
	Tag tagTimeStop1; 		
	Tag tagTimeStop2; 		
	Tag tagOut0;	 		
	Tag tagOut1;	 		
	Tag tagOut2;	 		
	
	

	@Mock private RoboplantModule module;
	

	@Before
	public void setUp() throws Exception {
		InitUtils.setupLogger();
		
		d = new DeviceSEQD();
		d.module = module;
		
		d.inpInput = new Input();
		d.inpInput.tag = new TagInt("", 0);
		
		tagInpStartBan  = addInputToList( d.inpStartBan );
		tagInpStop		= addInputToList( d.inpStop		); 	
		tagInpStopStart = addInputToList( d.inpStopStart);
		tagInpStopRun 	= addInputToList( d.inpStopRun 	);
		tagInpStopFlow  = addInputToList( d.inpStopFlow );
		tagInpStopDly 	= addInputToList( d.inpStopDly 	);
		tagInpSusp 	 	= addInputToList( d.inpSusp 	);


		d.tagBtnStart    	= new TagInt("BtnStart", 0);
		d.tagBtnStop     	= new TagInt("BtnStop", 0);
		d.tagBtnSusp     	= new TagInt("BtnSusp", 0);
		d.tagBtnEmrg     	= new TagInt("BtnEmrg", 0);
		d.tagBtnCancel      = new TagInt("BtnCancel", 0);
		d.tagTimeBtnStart	= new TagInt("TimeBtnStart", 3);
		d.tagFlags      	= new TagInt("Flags", 0);
		d.tagExclStartBan	= new TagInt("ExclStartBan", 0);
		d.tagExclStop    	= new TagInt("ExclStop", 0);
		d.tagExclStopStart  = new TagInt("ExclStopStart", 0);
		d.tagExclStopRun	= new TagInt("ExclStopRun", 0);
		d.tagExclStopFlow	= new TagInt("ExclStopFlow", 0);
		d.tagExclStopDly 	= new TagInt("ExclStopDly", 0);
		d.tagExclSusp   	= new TagInt("ExclSusp", 0);
		d.tagOutputOn    	= new TagInt("OutputOn", 0);
		d.tagOutputRun  	= new TagInt("OutputRun", 0);
		d.tagSost       	= new TagInt("Sost", 0);
		d.tagCounter    	= new TagInt("Counter", 0);
		d.tagMesReady   	= new TagInt("MesReady", 0);
		d.tagMesAutoStop	= new TagInt("MesAutoStop", 0);
		d.tagStopFlow   	= new TagInt("StopFlow", 0);
		d.tagStopDlyCnt 	= new TagInt("StopDlyCnt", 0);

		
		tagStopDlyTime  = addTagToList( d.tagStopDlyTime , 3 );	
		
		tagMesStartBan 	= addTagToList( d.tagMesStartBan , 0 );
		tagMesStop 		= addTagToList( d.tagMesStop 	 , 0 );
		tagMesStopStart	= addTagToList( d.tagMesStopStart, 0 );
		tagMesStopRun 	= addTagToList( d.tagMesStopRun  , 0 );
		tagMesStopFlow 	= addTagToList( d.tagMesStopFlow , 0 );
		tagMesStopDly 	= addTagToList( d.tagMesStopDly  , 0 );
		tagMesSusp 		= addTagToList( d.tagMesSusp 	 , 0 );
		
		tagTimeStart0 	= addTagToList( d.tagTimeStart 	 , 1 );	
		tagTimeStart1 	= addTagToList( d.tagTimeStart 	 , 2 );	
		tagTimeStart2 	= addTagToList( d.tagTimeStart 	 , 3 );
		
		tagTimeStop0 	= addTagToList( d.tagTimeStop 	 , 1 );	
		tagTimeStop1 	= addTagToList( d.tagTimeStop 	 , 2 );	
		tagTimeStop2 	= addTagToList( d.tagTimeStop 	 , 3 );
		
		tagOut0	 		= addTagToList( d.tagOut	 	 , 0 );
		tagOut1	 		= addTagToList( d.tagOut	 	 , 0 );
		tagOut2	 		= addTagToList( d.tagOut	 	 , 0 );

		d.SEQD_INP_AMOUNT = 1;
		d.outAmount = 3;
		
//		d.resetState();
	}
	
	private Tag addInputToList(List<DeviceSEQD.SeqdInput> list) {
		Input inp = new Input();
		inp.tag = new TagInt("", 0);
		list.add( new DeviceSEQD.SeqdInput(inp));
		return inp.tag;
	}


	private void exec(
			int outputOn    		,       
			int outputRun   		,       
			int sost        		,       
			int counter     		,       
			int mesReady    		,       
			int mesAutoStop 		,       
			int stopFlow    		,       
			int stopDlyCnt  		,       
			int out0				,
			int out1				,
			int out2				
			) {
		
		d.execute();
		assertEquals("outputOn"		, 	outputOn    , d.tagOutputOn.getInt());     
		assertEquals("outputRun"	, 	outputRun   , d.tagOutputRun.getInt());     
		assertEquals("sost"			, 	sost        , d.tagSost.getInt());     
		assertEquals("counter"		, 	counter     , d.tagCounter.getInt());     
		assertEquals("mesReady"		, 	mesReady    , d.tagMesReady.getInt());     
		assertEquals("mesAutoStop"	, 	mesAutoStop , d.tagMesAutoStop.getInt());     
		assertEquals("stopFlow"		, 	stopFlow    , d.tagStopFlow.getInt());     
		assertEquals("stopDlyCnt"	, 	stopDlyCnt  , d.tagStopDlyCnt.getInt());     
		assertEquals("out0"			, 	out0		, tagOut0.getInt());     
		assertEquals("out1"			, 	out1		, tagOut1.getInt());     
		assertEquals("out2"			, 	out2		, tagOut2.getInt());     
	}
	
	private void mes(
			int mesStartBan 		,       
			int mesStop 			,       
			int mesStopStart		,       
			int mesStopRun 			,       
			int mesStopFlow 		,       
			int mesStopDly 			,       
			int mesSusp 			
			) {
		
		assertEquals("mesStartBan"	, 	mesStartBan , tagMesStartBan.getInt());     
		assertEquals("mesStop"		, 	mesStop 	, tagMesStop.getInt());     
		assertEquals("mesStopStart"	, 	mesStopStart, tagMesStopStart.getInt());     
		assertEquals("mesStopRun"	, 	mesStopRun 	, tagMesStopRun.getInt());     
		assertEquals("mesStopFlow"	, 	mesStopFlow , tagMesStopFlow.getInt());     
		assertEquals("mesStopDly"	,	mesStopDly 	, tagMesStopDly.getInt());     
		assertEquals("mesSusp"		, 	mesSusp 	, tagMesSusp.getInt());     
	}
	
		
	
		

	@Test
	public void testLoad() throws Exception {
		
		RoboplantModule robo = AuxProcs.createRoboplantModule();
		assertTrue( robo.loadProject( Paths.get( 
				InitUtils.getResourcesDir("roboplant", RoboplantModuleTest.class) + "seqd.xml" )));

		assertEquals(1, robo.devicesByAddr.length);
		DeviceSEQD d = (DeviceSEQD)robo.devicesByAddr[0];
		
		assertEquals( "SEQD", d.devtype );
		assertEquals( "S03", d.tagname );
		
		assertTrue( d.init(robo) );
		assertTrue( d.prepare() );

		assertEquals(5, d.inpStartBan.size());
		assertEquals(5, d.inpStop.size());
		assertEquals(5, d.inpStopDly.size());
		assertEquals(5, d.inpStopFlow.size());
		assertEquals(5, d.inpStopRun.size());
		assertEquals(5, d.inpSusp.size());
		
		assertEquals(5, d.tagStopDlyTime.size());
		assertEquals(5, d.tagMesStartBan.size());
		assertEquals(5, d.tagMesStop.size());
		assertEquals(5, d.tagMesStopDly.size());
		assertEquals(5, d.tagMesStopFlow.size());
		assertEquals(5, d.tagMesStopRun.size());
		assertEquals(5, d.tagMesStopStart.size());
		assertEquals(5, d.tagMesSusp.size());

		assertEquals(25, d.tagTimeStart.size());
		assertEquals(25, d.tagTimeStop.size());
		assertEquals(25, d.tagOut.size());
	}
	
	
	@Test
	public void test0() throws Exception {
		exec( 0,0,0, 0,0,0,0,65535, 0,0,0);
		mes( 0,0,0,0,0,0,0 );
	}
	
	@Test
	public void testStartStop() throws Exception {
		d.tagBtnStart.setInt(1);
		exec( 0,0,0, 0,0,0,0,65535, 0,0,0);
		assertEquals(1, d.cntBtnStart);
		assertEquals(1, d.tagBtnStart.getInt());
		
		exec( 0,0,0, 0,0,0,0,65535, 0,0,0);
		assertEquals(2, d.cntBtnStart);
		assertEquals(1, d.tagBtnStart.getInt());

		exec( 0,0,0, 0,0,0,0,65535, 0,0,0);
		assertEquals(3, d.cntBtnStart);
		assertEquals(1, d.tagBtnStart.getInt());

		exec( 1,0,3, 1,0,0,0,65535, 1,0,0);
		assertEquals(0, d.tagBtnStart.getInt());
		exec( 1,0,3, 1,0,0,0,65535, 1,1,0);
		exec( 1,0,3, 2,0,0,0,65535, 1,1,0);
		exec( 1,0,3, 1,0,0,0,65535, 1,1,1);
		exec( 1,0,3, 2,0,0,0,65535, 1,1,1);
		exec( 1,0,3, 3,0,0,0,65535, 1,1,1);
		exec( 1,0,3, 0,0,0,0,65535, 1,1,1);
		exec( 1,1,4, 0,0,0,0,65535, 1,1,1);
		exec( 1,1,4, 0,0,0,0,65535, 1,1,1);
		mes( 0,0,0,0,0,0,0 );
		
		d.tagBtnStop.setInt(1);
		exec( 1,0,5, 1,0,0,0,65535, 1,1,1);
		assertEquals(0, d.tagBtnStop.getInt());
		exec( 1,0,5, 2,0,0,0,65535, 1,1,1);
		exec( 1,0,5, 3,0,0,0,65535, 1,1,1);
		exec( 1,0,5, 1,0,0,0,65535, 1,1,0);
		exec( 1,0,5, 2,0,0,0,65535, 1,1,0);
		exec( 1,0,5, 1,0,0,0,65535, 1,0,0);
		exec( 1,0,5, 0,0,0,0,65535, 0,0,0);
		exec( 0,0,0, 0,0,0,0,65535, 0,0,0);

	}

	@Test
	public void testInput() throws Exception {
		d.inpInput.tag.setInt(1);
		exec( 1,0,3, 1,0,0,0,65535, 1,0,0);
		exec( 1,0,3, 1,0,0,0,65535, 1,1,0);
		exec( 1,0,3, 2,0,0,0,65535, 1,1,0);
		exec( 1,0,3, 1,0,0,0,65535, 1,1,1);
		exec( 1,0,3, 2,0,0,0,65535, 1,1,1);
		exec( 1,0,3, 3,0,0,0,65535, 1,1,1);
		exec( 1,0,3, 0,0,0,0,65535, 1,1,1);
		exec( 1,1,4, 0,0,0,0,65535, 1,1,1);
		exec( 1,1,4, 0,0,0,0,65535, 1,1,1);

		d.inpInput.tag.setInt(0);
		exec( 1,1,4, 0,0,0,0,65535, 1,1,1);
		exec( 1,1,4, 0,0,0,0,65535, 1,1,1);
		exec( 1,1,4, 0,0,0,0,65535, 1,1,1);
	}

	
	@Test
	public void testInpStartBan_sost_0() throws Exception {
		tagInpStartBan.setInt(1);
		exec( 0,0,0, 0,0,0,0,65535, 0,0,0);		mes( 0,0,0,0,0,0,0 );
	}
	
	@Test
	public void testInpStartBan() throws Exception {
		d.inpInput.tag.setInt(1);
		
		// #1
		tagInpStartBan.setInt(1);
		exec( 1,0,1, 0,0,0,0,65535, 0,0,0);		mes( 1,0,0,0,0,0,0 );
		exec( 1,0,1, 0,0,0,0,65535, 0,0,0);		mes( 1,0,0,0,0,0,0 );
		exec( 1,0,1, 0,0,0,0,65535, 0,0,0);		mes( 1,0,0,0,0,0,0 );
		
		tagInpStartBan.setInt(0);
		exec( 1,0,2, 0,1,0,0,65535, 0,0,0);		mes( 0,0,0,0,0,0,0 );
		exec( 1,0,2, 0,1,0,0,65535, 0,0,0);		mes( 0,0,0,0,0,0,0 );
		exec( 1,0,2, 0,1,0,0,65535, 0,0,0);		mes( 0,0,0,0,0,0,0 );

		// #2
		tagInpStartBan.setInt(1);
		exec( 1,0,1, 0,0,0,0,65535, 0,0,0);		mes( 1,0,0,0,0,0,0 );

		tagInpStartBan.setInt(0);
		exec( 1,0,2, 0,1,0,0,65535, 0,0,0);		mes( 0,0,0,0,0,0,0 );

		d.tagBtnStart.setInt(1);
		exec( 1,0,2, 0,1,0,0,65535, 0,0,0);		mes( 0,0,0,0,0,0,0 );

		// #3
		tagInpStartBan.setInt(1);
		exec( 1,0,1, 0,0,0,0,65535, 0,0,0);		mes( 1,0,0,0,0,0,0 );
		exec( 1,0,1, 0,0,0,0,65535, 0,0,0);		mes( 1,0,0,0,0,0,0 );
		exec( 1,0,1, 0,0,0,0,65535, 0,0,0);		mes( 1,0,0,0,0,0,0 );

		tagInpStartBan.setInt(0);
		exec( 1,0,2, 0,1,0,0,65535, 0,0,0);		mes( 0,0,0,0,0,0,0 );

		d.tagBtnStart.setInt(1);
		exec( 1,0,2, 0,1,0,0,65535, 0,0,0);		mes( 0,0,0,0,0,0,0 );
		exec( 1,0,2, 0,1,0,0,65535, 0,0,0);		mes( 0,0,0,0,0,0,0 );
		exec( 1,0,2, 0,1,0,0,65535, 0,0,0);		mes( 0,0,0,0,0,0,0 );

		// starting
		exec( 1,0,3, 1,0,0,0,65535, 1,0,0);		mes( 0,0,0,0,0,0,0 );
		tagInpStartBan.setInt(1);
		exec( 1,0,3, 1,0,0,0,65535, 1,1,0);
		tagInpStartBan.setInt(0);
		exec( 1,0,3, 2,0,0,0,65535, 1,1,0);
		tagInpStartBan.setInt(1);
		exec( 1,0,3, 1,0,0,0,65535, 1,1,1);
		exec( 1,0,3, 2,0,0,0,65535, 1,1,1);
		exec( 1,0,3, 3,0,0,0,65535, 1,1,1);
		exec( 1,0,3, 0,0,0,0,65535, 1,1,1);
		exec( 1,1,4, 0,0,0,0,65535, 1,1,1);
		exec( 1,1,4, 0,0,0,0,65535, 1,1,1);

	}

	@Test
	public void testInpStartBan1() throws Exception {
		
		d.inpInput.tag.setInt(0);
		tagInpStartBan.setInt(1);
		d.tagBtnStart.setInt(0);
		d.tagTimeBtnStart.setInt(0);

		d.tagBtnStart.setInt(1);
		exec( 1,0,1, 0,0,0,0,65535, 0,0,0);		mes( 1,0,0,0,0,0,0 );

	}

	
	@Test
	public void testInpStartBan_sost_2_excl_1() throws Exception {
		d.tagExclStartBan.setInt(1);
		d.sost = 2;
		tagInpStartBan.setInt(1);
		exec( 1,0,2, 0,1,0,0,65535, 0,0,0);		mes( 0,0,0,0,0,0,0 );
	}

	@Test
	public void testInpStartBan_sost_2_excl_100() throws Exception {
		d.tagExclStartBan.setInt(0x100);
		d.sost = 2;
		tagInpStartBan.setInt(1);
		exec( 1,0,2, 0,1,0,0,65535, 0,0,0);		mes( 1,0,0,0,0,0,0 );
	}


	
	@Test
	public void testInpStop_sost_0() throws Exception {
		tagInpStop.setInt(1);
		exec( 0,0,0, 0,0,0,0,65535, 0,0,0);		mes( 0,0,0,0,0,0,0 );
	}

	@Test
	public void testInpStop_sost_1() throws Exception {
		d.sost = 1;
		tagInpStartBan.setInt(1);
		tagInpStop.setInt(1);
		exec( 1,0,1, 0,0,0,0,65535, 0,0,0);		mes( 1,0,0,0,0,0,0 );
	}

	@Test
	public void testInpStop_sost_2() throws Exception {
		d.sost = 2;
		tagInpStop.setInt(1);
		exec( 1,0,2, 0,1,0,0,65535, 0,0,0);		mes( 0,0,0,0,0,0,0 );
	}

	@Test
	public void testInpStop_sost_3() throws Exception {
		d.sost = 3;
		exec( 1,0,3, 1,0,0,0,65535, 1,0,0);		mes( 0,0,0,0,0,0,0 );

		tagInpStop.setInt(1);
		exec( 1,0,5, 0,0,1,0,65535, 0,0,0);		mes( 0,1,0,0,0,0,0 );
		exec( 0,0,0, 0,0,0,0,65535, 0,0,0);		mes( 0,0,0,0,0,0,0 );
		exec( 0,0,0, 0,0,0,0,65535, 0,0,0);		mes( 0,0,0,0,0,0,0 );
	}

	@Test
	public void testInpStop_sost_3_excl_1() throws Exception {
		d.tagExclStop.setInt(1);
		d.sost = 3;
		exec( 1,0,3, 1,0,0,0,65535, 1,0,0);		mes( 0,0,0,0,0,0,0 );

		tagInpStop.setInt(1);
		exec( 1,0,3, 1,0,0,0,65535, 1,1,0);		mes( 0,0,0,0,0,0,0 );
	}

	@Test
	public void testInpStop_sost_3_excl_100() throws Exception {
		d.tagExclStop.setInt(0x100);
		d.sost = 3;
		exec( 1,0,3, 1,0,0,0,65535, 1,0,0);		mes( 0,0,0,0,0,0,0 );

		tagInpStop.setInt(1);
		exec( 1,0,3, 1,0,0,0,65535, 1,1,0);		mes( 0,1,0,0,0,0,0 );
	}

	@Test
	public void testInpStop_sost_4() throws Exception {
		d.sost = 4;
		d.curOut = 2;
		exec( 1,1,4, 0,0,0,0,65535, 1,1,1);		mes( 0,0,0,0,0,0,0 );

		tagInpStop.setInt(1);
		exec( 1,0,5, 1,0,1,0,65535, 1,1,1);		mes( 0,1,0,0,0,0,0 );
		exec( 1,0,5, 2,0,1,0,65535, 1,1,1);		mes( 0,0,0,0,0,0,0 );
		exec( 1,0,5, 3,0,1,0,65535, 1,1,1);		mes( 0,0,0,0,0,0,0 );
	}

	@Test
	public void testInpStop_sost_6() throws Exception {
		d.sost = 6;
		d.curOut = 1;
		exec( 1,0,6, 0,0,0,0,65535, 1,1,0);		mes( 0,0,0,0,0,0,0 );

		tagInpStop.setInt(1);
		exec( 1,0,5, 0,0,0,0,65535, 1,1,0);		mes( 0,1,0,0,0,0,0 );
		exec( 1,0,5, 1,0,0,0,65535, 1,1,0);		mes( 0,0,0,0,0,0,0 );
		exec( 1,0,5, 2,0,0,0,65535, 1,1,0);		mes( 0,0,0,0,0,0,0 );
	}

	
	// inpStopStart
	@Test
	public void testInpStopStart_sost_0() throws Exception {
		tagInpStopStart.setInt(1);
		exec( 0,0,0, 0,0,0,0,65535, 0,0,0);		mes( 0,0,0,0,0,0,0 );
	}

	@Test
	public void testInpStopStart_sost_1() throws Exception {
		d.sost = 1;
		tagInpStartBan.setInt(1);
		tagInpStopStart.setInt(1);
		exec( 1,0,1, 0,0,0,0,65535, 0,0,0);		mes( 1,0,0,0,0,0,0 );
	}

	@Test
	public void testInpStopStart_sost_2() throws Exception {
		d.sost = 2;
		tagInpStopStart.setInt(1);
		exec( 1,0,2, 0,1,0,0,65535, 0,0,0);		mes( 0,0,0,0,0,0,0 );
	}

	@Test
	public void testInpStopStart_sost_3() throws Exception {
		d.sost = 3;
		exec( 1,0,3, 1,0,0,0,65535, 1,0,0);		mes( 0,0,0,0,0,0,0 );

		tagInpStopStart.setInt(1);
		exec( 1,0,5, 0,0,1,0,65535, 0,0,0);		mes( 0,0,1,0,0,0,0 );
		exec( 0,0,0, 0,0,0,0,65535, 0,0,0);		mes( 0,0,0,0,0,0,0 );
		exec( 0,0,0, 0,0,0,0,65535, 0,0,0);		mes( 0,0,0,0,0,0,0 );
	}

	@Test
	public void testInpStopStart_sost_3_excl_1() throws Exception {
		d.tagExclStopStart.setInt(1);
		d.sost = 3;
		exec( 1,0,3, 1,0,0,0,65535, 1,0,0);		mes( 0,0,0,0,0,0,0 );

		tagInpStopStart.setInt(1);
		exec( 1,0,3, 1,0,0,0,65535, 1,1,0);		mes( 0,0,0,0,0,0,0 );
	}

	@Test
	public void testInpStopStart_sost_3_excl_100() throws Exception {
		d.tagExclStopStart.setInt(0x100);
		d.sost = 3;
		exec( 1,0,3, 1,0,0,0,65535, 1,0,0);		mes( 0,0,0,0,0,0,0 );

		tagInpStopStart.setInt(1);
		exec( 1,0,3, 1,0,0,0,65535, 1,1,0);		mes( 0,0,1,0,0,0,0 );
	}


	@Test
	public void testInpStopStart_sost_4() throws Exception {
		d.sost = 4;
		d.curOut = 2;
		exec( 1,1,4, 0,0,0,0,65535, 1,1,1);		mes( 0,0,0,0,0,0,0 );

		tagInpStopStart.setInt(1);
		exec( 1,1,4, 0,0,0,0,65535, 1,1,1);		mes( 0,0,0,0,0,0,0 );
	}

	@Test
	public void testInpStopStart_sost_6() throws Exception {
		d.sost = 6;
		d.curOut = 1;
		exec( 1,0,6, 0,0,0,0,65535, 1,1,0);		mes( 0,0,0,0,0,0,0 );

		tagInpStopStart.setInt(1);
		exec( 1,0,5, 0,0,0,0,65535, 1,1,0);		mes( 0,0,1,0,0,0,0 );
		exec( 1,0,5, 1,0,0,0,65535, 1,1,0);		mes( 0,0,0,0,0,0,0 );
		exec( 1,0,5, 2,0,0,0,65535, 1,1,0);		mes( 0,0,0,0,0,0,0 );
	}


	
	// inpStopRun
	@Test
	public void testInpStopRun_sost_0() throws Exception {
		tagInpStopRun.setInt(1);
		exec( 0,0,0, 0,0,0,0,65535, 0,0,0);		mes( 0,0,0,0,0,0,0 );
	}

	@Test
	public void testInpStopRun_sost_1() throws Exception {
		d.sost = 1;
		tagInpStartBan.setInt(1);
		tagInpStopRun.setInt(1);
		exec( 1,0,1, 0,0,0,0,65535, 0,0,0);		mes( 1,0,0,0,0,0,0 );
	}

	@Test
	public void testInpStopRun_sost_2() throws Exception {
		d.sost = 2;
		tagInpStopRun.setInt(1);
		exec( 1,0,2, 0,1,0,0,65535, 0,0,0);		mes( 0,0,0,0,0,0,0 );
	}

	@Test
	public void testInpStopRun_sost_3() throws Exception {
		d.sost = 3;
		exec( 1,0,3, 1,0,0,0,65535, 1,0,0);		mes( 0,0,0,0,0,0,0 );

		tagInpStopRun.setInt(1);
		exec( 1,0,3, 1,0,0,0,65535, 1,1,0);		mes( 0,0,0,0,0,0,0 );
	}

	@Test
	public void testInpStopRun_sost_4() throws Exception {
		d.sost = 4;
		d.curOut = 2;
		exec( 1,1,4, 0,0,0,0,65535, 1,1,1);		mes( 0,0,0,0,0,0,0 );

		tagInpStopRun.setInt(1);
		exec( 1,0,5, 1,0,1,0,65535, 1,1,1);		mes( 0,0,0,1,0,0,0 );
	}

	@Test
	public void testInpStopRun_sost_4_excl_1() throws Exception {
		d.tagExclStopRun.setInt(1);
		d.sost = 4;
		d.curOut = 2;
		exec( 1,1,4, 0,0,0,0,65535, 1,1,1);		mes( 0,0,0,0,0,0,0 );

		tagInpStopRun.setInt(1);
		exec( 1,1,4, 0,0,0,0,65535, 1,1,1);		mes( 0,0,0,0,0,0,0 );
	}

	@Test
	public void testInpStopRun_sost_4_excl_100() throws Exception {
		d.tagExclStopRun.setInt(0x100);
		d.sost = 4;
		d.curOut = 2;
		exec( 1,1,4, 0,0,0,0,65535, 1,1,1);		mes( 0,0,0,0,0,0,0 );

		tagInpStopRun.setInt(1);
		exec( 1,1,4, 0,0,0,0,65535, 1,1,1);		mes( 0,0,0,1,0,0,0 );
	}

	@Test
	public void testInpStopRun_sost_6() throws Exception {
		d.sost = 6;
		d.curOut = 1;
		exec( 1,0,6, 0,0,0,0,65535, 1,1,0);		mes( 0,0,0,0,0,0,0 );

		tagInpStopRun.setInt(1);
		exec( 1,0,6, 0,0,0,0,65535, 1,1,0);		mes( 0,0,0,0,0,0,0 );
	}

	// inpStopDly
	@Test
	public void testInpStopDly_sost_0() throws Exception {
		tagInpStopDly.setInt(1);
		exec( 0,0,0, 0,0,0,0,65535, 0,0,0);		mes( 0,0,0,0,0,0,0 );
	}

	@Test
	public void testInpStopDly_sost_1() throws Exception {
		d.sost = 1;
		tagInpStartBan.setInt(1);
		tagInpStopDly.setInt(1);
		exec( 1,0,1, 0,0,0,0,65535, 0,0,0);		mes( 1,0,0,0,0,0,0 );
	}

	@Test
	public void testInpStopDly_sost_2() throws Exception {
		d.sost = 2;
		tagInpStopDly.setInt(1);
		exec( 1,0,2, 0,1,0,0,65535, 0,0,0);		mes( 0,0,0,0,0,0,0 );
	}

	@Test
	public void testInpStopDly_sost_3() throws Exception {
		d.sost = 3;
		exec( 1,0,3, 1,0,0,0,65535, 1,0,0);		mes( 0,0,0,0,0,0,0 );

		tagInpStopDly.setInt(1);
		exec( 1,0,3, 1,0,0,0,65535, 1,1,0);		mes( 0,0,0,0,0,0,0 );
	}

	@Test
	public void testInpStopDly_sost_4() throws Exception {
		d.sost = 4;
		d.curOut = 2;
		exec( 1,1,4, 0,0,0,0,65535, 1,1,1);		mes( 0,0,0,0,0,0,0 );

		tagInpStopDly.setInt(1);
		exec( 1,1,4, 0,0,0,0,    2, 1,1,1);		mes( 0,0,0,0,0,1,0 );
		exec( 1,1,4, 0,0,0,0,    1, 1,1,1);		mes( 0,0,0,0,0,1,0 );
		exec( 1,1,4, 0,0,0,0,    0, 1,1,1);		mes( 0,0,0,0,0,1,0 );
		exec( 1,0,5, 1,0,1,0,    0, 1,1,1);		mes( 0,0,0,0,0,1,0 );
		exec( 1,0,5, 2,0,1,0,65535, 1,1,1);		mes( 0,0,0,0,0,0,0 );
		exec( 1,0,5, 3,0,1,0,65535, 1,1,1);		mes( 0,0,0,0,0,0,0 );
		exec( 1,0,5, 1,0,1,0,65535, 1,1,0);		mes( 0,0,0,0,0,0,0 );
	}
	
	@Test
	public void testInpStopDly_sost_4_excl_1() throws Exception {
		d.tagExclStopDly.setInt(1);
		d.sost = 4;
		d.curOut = 2;
		exec( 1,1,4, 0,0,0,0,65535, 1,1,1);		mes( 0,0,0,0,0,0,0 );

		tagInpStopDly.setInt(1);
		exec( 1,1,4, 0,0,0,0,65535, 1,1,1);		mes( 0,0,0,0,0,0,0 );
	}

	@Test
	public void testInpStopDly_sost_4_excl_100() throws Exception {
		d.tagExclStopDly.setInt(0x100);
		d.sost = 4;
		d.curOut = 2;
		exec( 1,1,4, 0,0,0,0,65535, 1,1,1);		mes( 0,0,0,0,0,0,0 );

		tagInpStopDly.setInt(1);
		exec( 1,1,4, 0,0,0,0,65535, 1,1,1);		mes( 0,0,0,0,0,1,0 );
	}

	@Test
	public void testInpStopDly_sost_4_short() throws Exception {
		d.sost = 4;
		d.curOut = 2;
		exec( 1,1,4, 0,0,0,0,65535, 1,1,1);		mes( 0,0,0,0,0,0,0 );

		tagInpStopDly.setInt(1);
		exec( 1,1,4, 0,0,0,0,    2, 1,1,1);		mes( 0,0,0,0,0,1,0 );
		exec( 1,1,4, 0,0,0,0,    1, 1,1,1);		mes( 0,0,0,0,0,1,0 );
		tagInpStopDly.setInt(0);
		exec( 1,1,4, 0,0,0,0,65535, 1,1,1);		mes( 0,0,0,0,0,0,0 );
		exec( 1,1,4, 0,0,0,0,65535, 1,1,1);		mes( 0,0,0,0,0,0,0 );
	}

	@Test
	public void testInpStopDly_sost_6() throws Exception {
		d.sost = 6;
		d.curOut = 1;
		exec( 1,0,6, 0,0,0,0,65535, 1,1,0);		mes( 0,0,0,0,0,0,0 );

		tagInpStopDly.setInt(1);
		exec( 1,0,6, 0,0,0,0,65535, 1,1,0);		mes( 0,0,0,0,0,0,0 );
	}


	// inpStopFlow
	@Test
	public void testInpStopFlow_sost_0() throws Exception {
		tagInpStopFlow.setInt(1);
		exec( 0,0,0, 0,0,0,1,65535, 0,0,0);		mes( 0,0,0,0,1,0,0 );
	}

	@Test
	public void testInpStopFlow_sost_1() throws Exception {
		d.sost = 1;
		tagInpStartBan.setInt(1);
		tagInpStopFlow.setInt(1);
		exec( 1,0,1, 0,0,0,1,65535, 0,0,0);		mes( 1,0,0,0,1,0,0 );
	}

	@Test
	public void testInpStopFlow_sost_2() throws Exception {
		d.sost = 2;
		tagInpStopFlow.setInt(1);
		exec( 1,0,2, 0,1,0,1,65535, 0,0,0);		mes( 0,0,0,0,1,0,0 );
	}

	@Test
	public void testInpStopFlow_sost_3() throws Exception {
		d.sost = 3;
		exec( 1,0,3, 1,0,0,0,65535, 1,0,0);		mes( 0,0,0,0,0,0,0 );

		tagInpStopFlow.setInt(1);
		exec( 1,0,3, 1,0,0,1,65535, 1,1,0);		mes( 0,0,0,0,1,0,0 );
	}

	@Test
	public void testInpStopFlow_sost_4() throws Exception {
		d.sost = 4;
		d.curOut = 2;
		exec( 1,1,4, 0,0,0,0,65535, 1,1,1);		mes( 0,0,0,0,0,0,0 );

		tagInpStopFlow.setInt(1);
		exec( 1,1,4, 0,0,0,1,65535, 1,1,1);		mes( 0,0,0,0,1,0,0 );
		exec( 1,1,4, 0,0,0,1,65535, 1,1,1);		mes( 0,0,0,0,1,0,0 );
		exec( 1,1,4, 0,0,0,1,65535, 1,1,1);		mes( 0,0,0,0,1,0,0 );

		tagInpStopFlow.setInt(0);
		exec( 1,1,4, 0,0,0,0,65535, 1,1,1);		mes( 0,0,0,0,0,0,0 );
	}
	
	public void testInpStopFlow_sost_4_excl_1() throws Exception {
		d.tagExclStopFlow.setInt(1);
		d.sost = 4;
		d.curOut = 2;
		exec( 1,1,4, 0,0,0,0,65535, 1,1,1);		mes( 0,0,0,0,0,0,0 );

		tagInpStopFlow.setInt(1);
		exec( 1,1,4, 0,0,0,0,65535, 1,1,1);		mes( 0,0,0,0,0,0,0 );
	}

	public void testInpStopFlow_sost_4_excl_100() throws Exception {
		d.tagExclStopFlow.setInt(0x100);
		d.sost = 4;
		d.curOut = 2;
		exec( 1,1,4, 0,0,0,0,65535, 1,1,1);		mes( 0,0,0,0,0,0,0 );

		tagInpStopFlow.setInt(1);
		exec( 1,1,4, 0,0,0,0,65535, 1,1,1);		mes( 0,0,0,0,1,0,0 );
	}

	@Test
	public void testInpStopFlow_sost_5() throws Exception {
		d.sost = 5;
		d.curOut = 1;
		exec( 1,0,5, 1,0,0,0,65535, 1,1,0);		mes( 0,0,0,0,0,0,0 );

		tagInpStopFlow.setInt(1);
		exec( 1,0,5, 2,0,0,1,65535, 1,1,0);		mes( 0,0,0,0,1,0,0 );
	}

	@Test
	public void testInpStopFlow_sost_6() throws Exception {
		d.sost = 6;
		d.curOut = 1;
		exec( 1,0,6, 0,0,0,0,65535, 1,1,0);		mes( 0,0,0,0,0,0,0 );

		tagInpStopFlow.setInt(1);
		exec( 1,0,6, 0,0,0,1,65535, 1,1,0);		mes( 0,0,0,0,1,0,0 );
	}


	
	// inpSusp
	@Test
	public void testInpSusp_sost_0() throws Exception {
		tagInpSusp.setInt(1);
		exec( 0,0,0, 0,0,0,0,65535, 0,0,0);		mes( 0,0,0,0,0,0,0 );
	}

	@Test
	public void testInpSusp_sost_1() throws Exception {
		d.sost = 1;
		tagInpStartBan.setInt(1);
		tagInpSusp.setInt(1);
		exec( 1,0,1, 0,0,0,0,65535, 0,0,0);		mes( 1,0,0,0,0,0,0 );
	}

	@Test
	public void testInpSusp_sost_2() throws Exception {
		d.sost = 2;
		tagInpSusp.setInt(1);
		exec( 1,0,2, 0,1,0,0,65535, 0,0,0);		mes( 0,0,0,0,0,0,0 );
	}

	@Test
	public void testInpSusp_sost_3() throws Exception {
		d.sost = 3;
		exec( 1,0,3, 1,0,0,0,65535, 1,0,0);		mes( 0,0,0,0,0,0,0 );

		tagInpSusp.setInt(1);
		exec( 1,0,6, 1,0,0,0,65535, 1,0,0);		mes( 0,0,0,0,0,0,1 );
		exec( 1,0,6, 1,0,0,0,65535, 1,0,0);		mes( 0,0,0,0,0,0,1 );

		tagInpSusp.setInt(0);
		exec( 1,0,6, 1,0,0,0,65535, 1,0,0);		mes( 0,0,0,0,0,0,0 );
		exec( 1,0,6, 1,0,0,0,65535, 1,0,0);		mes( 0,0,0,0,0,0,0 );
	}

	@Test
	public void testInpSusp_sost_3_flag_1() throws Exception {
		d.sost = 3;
		d.tagFlags.setInt(1);
		exec( 1,0,3, 1,0,0,0,65535, 1,0,0);		mes( 0,0,0,0,0,0,0 );

		tagInpSusp.setInt(1);
		exec( 1,0,6, 1,0,0,0,65535, 1,0,0);		mes( 0,0,0,0,0,0,1 );
		exec( 1,0,6, 1,0,0,0,65535, 1,0,0);		mes( 0,0,0,0,0,0,1 );

		tagInpSusp.setInt(0);
		exec( 1,0,3, 1,0,0,0,65535, 1,0,0);		mes( 0,0,0,0,0,0,0 );
	}

	@Test
	public void testInpSusp_sost_3_excl_1() throws Exception {
		d.tagExclSusp.setInt(1);
		d.sost = 3;
		exec( 1,0,3, 1,0,0,0,65535, 1,0,0);		mes( 0,0,0,0,0,0,0 );

		tagInpSusp.setInt(1);
		exec( 1,0,3, 1,0,0,0,65535, 1,1,0);		mes( 0,0,0,0,0,0,1 );
	}

	@Test
	public void testInpSusp_sost_3_excl_100() throws Exception {
		d.tagExclSusp.setInt(0x100);
		d.sost = 3;
		exec( 1,0,3, 1,0,0,0,65535, 1,0,0);		mes( 0,0,0,0,0,0,0 );

		tagInpSusp.setInt(1);
		exec( 1,0,3, 1,0,0,0,65535, 1,1,0);		mes( 0,0,0,0,0,0,1 );
	}

	@Test
	public void testInpSusp_sost_4() throws Exception {
		d.sost = 4;
		d.curOut = 2;
		exec( 1,1,4, 0,0,0,0,65535, 1,1,1);		mes( 0,0,0,0,0,0,0 );

		tagInpSusp.setInt(1);
		exec( 1,1,4, 0,0,0,0,65535, 1,1,1);		mes( 0,0,0,0,0,0,0 );

		tagInpSusp.setInt(0);
		exec( 1,1,4, 0,0,0,0,65535, 1,1,1);		mes( 0,0,0,0,0,0,0 );
	}

	@Test
	public void testInpSusp_sost_5() throws Exception {
		d.sost = 5;
		d.curOut = 1;
		exec( 1,0,5, 1,0,0,0,65535, 1,1,0);		mes( 0,0,0,0,0,0,0 );

		tagInpSusp.setInt(1);
		exec( 1,0,5, 2,0,0,0,65535, 1,1,0);		mes( 0,0,0,0,0,0,0 );
	}

	@Test
	public void testInpSusp_sost_6() throws Exception {
		d.sost = 6;
		d.curOut = 1;
		exec( 1,0,6, 0,0,0,0,65535, 1,1,0);		mes( 0,0,0,0,0,0,0 );

		tagInpSusp.setInt(1);
		exec( 1,0,6, 0,0,0,0,65535, 1,1,0);		mes( 0,0,0,0,0,0,1 );
	}

	@Test
	public void testBtnSusp_sost_3() throws Exception {
		d.sost = 3;
		exec( 1,0,3, 1,0,0,0,65535, 1,0,0);		mes( 0,0,0,0,0,0,0 );

		d.tagBtnSusp.setInt(1);
		exec( 1,0,6, 1,0,0,0,65535, 1,0,0);		mes( 0,0,0,0,0,0,0 );
		exec( 1,0,6, 1,0,0,0,65535, 1,0,0);		mes( 0,0,0,0,0,0,0 );

		d.tagBtnSusp.setInt(0);
		exec( 1,0,6, 1,0,0,0,65535, 1,0,0);		mes( 0,0,0,0,0,0,0 );
		exec( 1,0,6, 1,0,0,0,65535, 1,0,0);		mes( 0,0,0,0,0,0,0 );
	}



	// BtnEmrg
	@Test
	public void testBtnEmrg_sost_0() throws Exception {
		d.tagBtnEmrg.setInt(1);
		exec( 0,0,0, 0,0,0,0,65535, 0,0,0);		mes( 0,0,0,0,0,0,0 );
	}

	@Test
	public void testBtnEmrg_sost_1() throws Exception {
		d.sost = 1;
		tagInpStartBan.setInt(1);
		exec( 1,0,1, 0,0,0,0,65535, 0,0,0);		mes( 1,0,0,0,0,0,0 );

		d.tagBtnEmrg.setInt(1);
		exec( 0,0,0, 0,0,0,0,65535, 0,0,0);		mes( 1,0,0,0,0,0,0 );
		exec( 0,0,0, 0,0,0,0,65535, 0,0,0);		mes( 0,0,0,0,0,0,0 );
	}

	@Test
	public void testBtnEmrg_sost_2() throws Exception {
		d.sost = 2;
		exec( 1,0,2, 0,1,0,0,65535, 0,0,0);		mes( 0,0,0,0,0,0,0 );

		d.tagBtnEmrg.setInt(1);
		exec( 0,0,0, 0,0,0,0,65535, 0,0,0);		mes( 0,0,0,0,0,0,0 );
	}

	@Test
	public void testBtnEmrg_sost_3() throws Exception {
		d.sost = 3;
		exec( 1,0,3, 1,0,0,0,65535, 1,0,0);		mes( 0,0,0,0,0,0,0 );
		exec( 1,0,3, 1,0,0,0,65535, 1,1,0);		mes( 0,0,0,0,0,0,0 );

		d.tagBtnEmrg.setInt(1);
		exec( 0,0,0, 0,0,0,0,65535, 0,0,0);		mes( 0,0,0,0,0,0,0 );
		exec( 0,0,0, 0,0,0,0,65535, 0,0,0);		mes( 0,0,0,0,0,0,0 );
	}

	@Test
	public void testBtnEmrg_sost_4() throws Exception {
		d.sost = 4;
		d.curOut = 2;
		exec( 1,1,4, 0,0,0,0,65535, 1,1,1);		mes( 0,0,0,0,0,0,0 );

		d.tagBtnEmrg.setInt(1);
		exec( 0,0,0, 0,0,0,0,65535, 0,0,0);		mes( 0,0,0,0,0,0,0 );
	}

	@Test
	public void testBtnEmrg_sost_5() throws Exception {
		d.sost = 5;
		d.curOut = 1;
		exec( 1,0,5, 1,0,0,0,65535, 1,1,0);		mes( 0,0,0,0,0,0,0 );

		d.tagBtnEmrg.setInt(1);
		exec( 0,0,0, 0,0,0,0,65535, 0,0,0);		mes( 0,0,0,0,0,0,0 );
	}

	@Test
	public void testBtnEmrg_sost_6() throws Exception {
		d.sost = 6;
		d.curOut = 1;
		exec( 1,0,6, 0,0,0,0,65535, 1,1,0);		mes( 0,0,0,0,0,0,0 );

		d.tagBtnEmrg.setInt(1);
		exec( 0,0,0, 0,0,0,0,65535, 0,0,0);		mes( 0,0,0,0,0,0,0 );
	}

	@Test
	public void testBtnCancel_sost_4() throws Exception {
		d.sost = 4;
		d.curOut = 2;
		exec( 1,1,4, 0,0,0,0,65535, 1,1,1);		mes( 0,0,0,0,0,0,0 );

		tagInpStopDly.setInt(1);
		exec( 1,1,4, 0,0,0,0,    2, 1,1,1);		mes( 0,0,0,0,0,1,0 );
		d.tagBtnCancel.setInt(1);
		exec( 1,1,4, 0,0,0,0,65535, 1,1,1);		mes( 0,0,0,0,0,1,0 );
		assertEquals( 0, d.tagBtnCancel.getInt() );
		exec( 1,1,4, 0,0,0,0,65535, 1,1,1);		mes( 0,0,0,0,0,1,0 );
		exec( 1,1,4, 0,0,0,0,65535, 1,1,1);		mes( 0,0,0,0,0,1,0 );
		exec( 1,1,4, 0,0,0,0,65535, 1,1,1);		mes( 0,0,0,0,0,1,0 );
	}

	@Test
	public void testBtnCancel_sost_3() throws Exception {
		d.sost = 3;
		exec( 1,0,3, 1,0,0,0,65535, 1,0,0);		mes( 0,0,0,0,0,0,0 );

		d.tagBtnCancel.setInt(1);
		exec( 1,0,3, 1,0,0,0,65535, 1,1,0);		mes( 0,0,0,0,0,0,0 );
	}

}
