package promauto.jroboplc.plugin.roboplant;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import promauto.jroboplc.core.api.InitUtils;
import promauto.jroboplc.core.tags.TagInt;

import static org.junit.Assert.assertEquals;


public class DeviceSTGATest {


	private DeviceSTGA d;

	@Mock RoboplantModule module;


	@Before
	public void setUp() throws Exception {
		InitUtils.setupLogger();
		
		d = new DeviceSTGA();
		d.module = module;
		AuxProcs.createInputsOutputs(d);

		for(int i=0; i<3; ++i) {
            DeviceSTGA.Group gr = new DeviceSTGA.Group();
            gr.inpNext = new Input();
            gr.inpNext.tag = new TagInt("Next" + (i+1), 0);
            gr.tagOutput = new TagInt("Output" + (i+1), 0);
            d.groups.add(gr);
        }


	}



    private void exec(
            int input,
            int cycleCnt,
            int stage,
            int next1,
            int next2,
            int next3,
            int output1,
            int output2,
            int output3
    ) {
        d.inpInput.tag.setInt(input);
        d.groups.get(0).inpNext.tag.setInt(next1);
        d.groups.get(1).inpNext.tag.setInt(next2);
        d.groups.get(2).inpNext.tag.setInt(next3);

        d.execute();
        assertEquals("cycleCnt",    cycleCnt,   d.tagCycleCnt.getInt());
        assertEquals("stage",     stage,    d.tagStage.getInt());

        assertEquals("output1",    output1,   d.groups.get(0).tagOutput.getInt());
        assertEquals("output2",    output2,   d.groups.get(1).tagOutput.getInt());
        assertEquals("output3",    output3,   d.groups.get(2).tagOutput.getInt());
    }


    @Test
	public void test_forward() {
        d.tagCycleMax.setInt(0);
        d.tagStageStart.setInt(0);
        d.tagStageLoop.setInt(0);

        exec(0,0, 0, 0, 0, 0, 0, 0, 0);

        // only one Next is on
        exec(1,0, 1, 0, 0, 0, 1, 0, 0);
        exec(1,0, 2, 1, 0, 0, 0, 1, 0);
        exec(1,0, 3, 0, 1, 0, 0, 0, 1);

        exec(1,1, 1, 0, 0, 1, 1, 0, 0);
        exec(1,1, 2, 1, 0, 0, 0, 1, 0);
        exec(1,1, 3, 0, 1, 0, 0, 0, 1);

        // all Nexts are on
        exec(1,2, 1, 1, 1, 1, 1, 0, 0);
        exec(1,2, 2, 1, 1, 1, 0, 1, 0);
        exec(1,2, 3, 1, 1, 1, 0, 0, 1);

        exec(1,3, 1, 1, 1, 1, 1, 0, 0);
        exec(1,3, 2, 1, 1, 1, 0, 1, 0);
        exec(1,3, 3, 1, 1, 1, 0, 0, 1);

        // Input turns off
        exec(1,4, 1, 1, 1, 1, 1, 0, 0);
        exec(0,4, 2, 1, 1, 1, 0, 1, 0);
        exec(0,4, 3, 1, 1, 1, 0, 0, 1);
        exec(0,5, 0, 1, 1, 1, 0, 0, 0);

        // Reset
        exec(1,5, 1, 1, 1, 1, 1, 0, 0);
        d.inpReset.tag.setInt(1);
        exec(1,0, 0, 1, 1, 1, 0, 0, 0);
        d.inpReset.tag.setInt(0);
        exec(0,0, 0, 1, 1, 1, 0, 0, 0);

        // big Next
        exec(1,0, 1, 0, 0, 0, 1, 0, 0);
        exec(1,1, 1, 5, 0, 0, 1, 0, 0);
        exec(1,2, 1, 5, 0, 0, 1, 0, 0);

    }


    @Test
    public void test_output_trigger() {
        d.tagFlags.setInt(3);
        d.tagCycleMax.setInt(1);
//        d.tagStageStart.setInt(0);
//        d.tagStageLoop.setInt(0);

        exec(0,0, 0, 0, 0, 0, 0, 0, 0);

        // only one Next is on
        exec(1,0, 1, 0, 0, 0, 1, 0, 0);
        exec(1,0, 2, 1, 0, 0, 1, 1, 0);
        exec(1,0, 3, 0, 1, 0, 1, 1, 1);
        exec(1,0, 3, 0, 1, 0, 1, 1, 1);
        exec(1,0, 3, 0, 0, 0, 1, 1, 1);
        exec(1,1, 3, 0, 0, 1, 1, 1, 1);
        exec(1,1, 3, 0, 0, 1, 1, 1, 1);
        d.inpReset.tag.setInt(1);
        exec(1,0, 0, 0, 0, 0, 0, 0, 0);
        exec(1,0, 0, 0, 0, 0, 0, 0, 0);
        d.inpReset.tag.setInt(0);
        exec(1,0, 1, 0, 0, 0, 1, 0, 0);


    }

    @Test
    public void test_backward() {
        d.tagCycleMax.setInt(0);
        d.tagStageStart.setInt(0);
        d.tagStageLoop.setInt(0);

        exec(0, 0, 0, 0, 0, 0, 0, 0, 0);
        exec(1, 0, 1, 0, 0, 0, 1, 0, 0);
        exec(1, 0, 2, 1, 0, 0, 0, 1, 0);
        exec(1, 0, 2, 0, 0, 0, 0, 1, 0);
        exec(1, 0, 1, 0, -1, 0, 1, 0, 0);
        exec(1, 0, 1, 0, 0, 0, 1, 0, 0);
        exec(1, 0, 0, -1, 0, 0, 0, 0, 0);
        exec(1, 0, 1, 0, 0, 0, 1, 0, 0);
        exec(1, 0, 0, -1, 0, 0, 0, 0, 0);
        exec(1, 0, 1, -1, 0, 0, 1, 0, 0);
        exec(1, 0, 0, -5, 0, 0, 0, 0, 0);
    }


    @Test
    public void test_cyclemax() {
        d.tagCycleMax.setInt(2);
        d.tagStageStart.setInt(0);
        d.tagStageLoop.setInt(0);

        exec(1,0, 1, 1, 1, 1, 1, 0, 0);
        exec(1,0, 2, 1, 1, 1, 0, 1, 0);
        exec(1,0, 3, 1, 1, 1, 0, 0, 1);

        exec(1,1, 1, 1, 1, 1, 1, 0, 0);
        exec(1,1, 2, 1, 1, 1, 0, 1, 0);
        exec(1,1, 3, 1, 1, 1, 0, 0, 1);

        exec(1,2, 0, 1, 1, 1, 0, 0, 0);
        exec(1,2, 0, 1, 1, 1, 0, 0, 0);
        exec(1,2, 0, 1, 1, 1, 0, 0, 0);
    }

    @Test
    public void test_stagestart() {
        d.tagCycleMax.setInt(0);
        d.tagStageStart.setInt(2);
        d.tagStageLoop.setInt(0);

        exec(1,0, 2, 1, 1, 1, 0, 1, 0);
        exec(1,0, 3, 1, 1, 1, 0, 0, 1);

        exec(1,1, 1, 1, 1, 1, 1, 0, 0);
        exec(1,1, 2, 1, 1, 1, 0, 1, 0);
        exec(1,1, 3, 1, 1, 1, 0, 0, 1);

    }


    @Test
    public void test_stageloop() {
        d.tagCycleMax.setInt(0);
        d.tagStageStart.setInt(0);
        d.tagStageLoop.setInt(2);

        exec(1,0, 1, 1, 1, 1, 1, 0, 0);
        exec(1,0, 2, 1, 1, 1, 0, 1, 0);
        exec(1,0, 3, 1, 1, 1, 0, 0, 1);

        exec(1,1, 2, 1, 1, 1, 0, 1, 0);
        exec(1,1, 3, 1, 1, 1, 0, 0, 1);

        exec(1,2, 2, 1, 1, 1, 0, 1, 0);
        exec(1,2, 3, 1, 1, 1, 0, 0, 1);

    }


    @Test
    public void test_write_bad_stage() {
        d.tagCycleMax.setInt(0);
        d.tagStageStart.setInt(0);
        d.tagStageLoop.setInt(0);

        exec(1, 0, 1, 1, 1, 1, 1, 0, 0);
        d.tagStage.setInt(-10);
        exec(1, 0, 0, 1, 1, 1, 0, 0, 0);

        exec(1, 0, 1, 1, 1, 1, 1, 0, 0);
        d.tagStage.setInt(10);
        exec(1, 0, 0, 1, 1, 1, 0, 0, 0);

        exec(1, 0, 1, 1, 1, 1, 1, 0, 0);
    }


    }

