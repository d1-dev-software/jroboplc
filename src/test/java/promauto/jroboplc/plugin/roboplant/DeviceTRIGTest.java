package promauto.jroboplc.plugin.roboplant;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static promauto.jroboplc.plugin.roboplant.AuxProcs.newInputAndTag;

import java.nio.file.Paths;

import org.junit.Before;
import org.junit.Test;

import promauto.jroboplc.core.api.InitUtils;
import promauto.jroboplc.core.tags.TagInt;

public class DeviceTRIGTest {
	private DeviceTRIG d;
	
	@Before
	public void setUp() throws Exception {
		InitUtils.setupLogger();

		d = new DeviceTRIG();
		d.inpSet 	= newInputAndTag();
		d.inpReset 	= newInputAndTag();
		d.tagOutput		= new TagInt("", 0);
	}

	@Test
	public void testLoad() throws Exception {
		RoboplantModule robo = AuxProcs.createRoboplantModule();
		assertTrue( robo.loadProject( Paths.get( 
				InitUtils.getResourcesDir("roboplant", RoboplantModuleTest.class) + "trig.xml" )));

		assertEquals(1, robo.devicesByAddr.length);
		d = (DeviceTRIG)robo.devicesByAddr[0];
		assertTrue( d.init(robo) );
		assertTrue( d.prepare() );

		assertEquals( "TRIG", d.devtype );
		assertEquals( "S01_07", d.name );
		assertEquals( "T0000", d.tagname );
	}
	
	private void set(
			int set,
			int reset
			) {
		d.inpSet.tag.setInt(  set);
		d.inpReset.tag.setInt(reset);
	}

	
	private void exec(
			int output 
			) {
		d.execute();
		assertEquals("output"	, output	 , d.tagOutput.getInt());     
	}


	@Test
	public void testExecute() {
		set(0,0); 	exec(0);
		set(1,0); 	exec(1);
		set(1,1); 	exec(0);
		set(0,1); 	exec(0);
		set(1,1); 	exec(0);
		set(1,0); 	exec(1);
		set(0,0); 	exec(1);
		set(0,1); 	exec(0);
	}
}
