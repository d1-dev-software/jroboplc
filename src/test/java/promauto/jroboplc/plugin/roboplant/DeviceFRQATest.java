package promauto.jroboplc.plugin.roboplant;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import promauto.jroboplc.core.State;
import promauto.jroboplc.core.api.InitUtils;

import static org.junit.Assert.assertEquals;


public class DeviceFRQATest {

    private static class DeviceFRQAHelper extends DeviceFRQA {
        public long time = 0;

        @Override
        protected long getCurrentTime() {
            return time;
        }

        protected void setInput(int value) {
            inpInput.tag.setInt(value);
        }

        protected void setEnable(int value) {
            inpEnable.tag.setInt(value);
        }
    }

	private DeviceFRQAHelper d;

	@Mock RoboplantModule module;


	@Before
	public void setUp() throws Exception {
		InitUtils.setupLogger();
		
		d = new DeviceFRQAHelper();
		d.module = module;

		AuxProcs.createInputsOutputs(d, d.getClass().getSuperclass() );
	}



    private void exec(
            int enable,
            int input,
            int freq,
            int low,
            int normal,
            int high
    ) {
	    d.time++;
        d.inpInput.tag.setInt(input);
        d.inpEnable.tag.setInt(enable);

        d.execute();
        assertEquals("freq",    freq,   d.tagFreq.getInt());
        assertEquals("low",     low,    d.tagLow.getInt());
        assertEquals("normal",  normal, d.tagNormal.getInt());
        assertEquals("high",    high,   d.tagHigh.getInt());
    }


    @Test
	public void test_period_6() {
        d.tagPeriod.setInt(6);
        d.tagFreqLow.setInt(0);
        d.tagFreqHigh.setInt(2);

        exec(0,0, 0, 0, 0, 0);
        exec(0, 1, 0, 0, 0, 0);
        exec(1, 0, 0, 1, 0, 0);

        // freq = 1
        exec(1, 1, 1, 0, 1, 0);
        exec(1, 0, 1, 0, 1, 0);
        exec(1, 0, 1, 0, 1, 0);
        exec(1, 0, 1, 0, 1, 0);
        exec(1, 0, 1, 0, 1, 0);
        exec(1, 0, 1, 0, 1, 0);
        exec(1, 0, 0, 1, 0, 0);

        // freq = 2
        exec(1, 1, 1, 0, 1, 0);
        exec(1, 0, 1, 0, 1, 0);
        exec(1, 1, 2, 0, 0, 1);
        exec(1, 0, 2, 0, 0, 1);
        exec(1, 0, 2, 0, 0, 1);
        exec(1, 0, 2, 0, 0, 1);
        exec(1, 0, 1, 0, 1, 0);
        exec(1, 0, 1, 0, 1, 0);
        exec(1, 0, 0, 1, 0, 0);

        // freq = 3
        exec(1, 1, 1, 0, 1, 0);
        exec(1, 0, 1, 0, 1, 0);
        exec(1, 1, 2, 0, 0, 1);
        exec(1, 0, 2, 0, 0, 1);
        exec(1, 1, 3, 0, 0, 1);
        exec(1, 0, 3, 0, 0, 1);
        exec(1, 0, 2, 0, 0, 1);
        exec(1, 0, 2, 0, 0, 1);
        exec(1, 0, 1, 0, 1, 0);
        exec(1, 0, 1, 0, 1, 0);
        exec(1, 0, 0, 1, 0, 0);

    }


    @Test
    public void test_period_1() {
        d.tagPeriod.setInt(1);

        // freq = 1
        exec(1, 0, 0, 1, 0, 1);
        exec(1, 1, 1, 0, 0, 1);
        exec(1, 0, 0, 1, 0, 1);
    }

    @Test
    public void test_mul_div() {
        d.tagPeriod.setInt(1);
        d.tagMul.setInt(20);
        d.tagDiv.setInt(3);

        // freq = 1
        exec(1, 0, 0, 1, 0, 1);
        exec(1, 1, 7, 0, 0, 1);
        exec(1, 0, 0, 1, 0, 1);
    }


    @Test
    public void test_state() {
        State state = new State();
        d.tagPeriod.setInt(6);
        d.tagFreqLow.setInt(0);
        d.tagFreqHigh.setInt(2);

        // freq = 2
        exec(1, 1, 1, 0, 1, 0);
        exec(1, 0, 1, 0, 1, 0);
        exec(1, 1, 2, 0, 0, 1);
        exec(1, 0, 2, 0, 0, 1);
        d.saveStateExtra(state);
        long time = d.time;
        exec(1, 0, 2, 0, 0, 1);
        exec(1, 0, 2, 0, 0, 1);
        exec(1, 0, 1, 0, 1, 0);
        exec(1, 0, 1, 0, 1, 0);
        exec(1, 0, 0, 1, 0, 0);

        d.loadStateExtra(state);
        d.time = time;
        d.tagFreq.setInt(2);
        exec(1, 0, 2, 0, 0, 1);
        exec(1, 0, 2, 0, 0, 1);
        exec(1, 0, 1, 0, 1, 0);
        exec(1, 0, 1, 0, 1, 0);
        exec(1, 0, 0, 1, 0, 0);
    }


    }



















