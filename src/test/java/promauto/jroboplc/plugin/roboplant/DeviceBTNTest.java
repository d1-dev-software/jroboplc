package promauto.jroboplc.plugin.roboplant;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.nio.file.Paths;

import org.junit.Before;
import org.junit.Test;

import promauto.jroboplc.core.api.InitUtils;
import promauto.jroboplc.core.tags.TagInt;

public class DeviceBTNTest {
	private DeviceBTN d;
	
	@Before
	public void setUp() throws Exception {
		InitUtils.setupLogger();
		
		d = new DeviceBTN();
		d.tagOutput  = new TagInt("", 0);
		d.tagInput   = new TagInt("", 0);
		d.tagTimeDel = new TagInt("", 0);
		d.tagTimeImp = new TagInt("", 0);
	}

	@Test
	public void testLoad() throws Exception {
		
		RoboplantModule robo = AuxProcs.createRoboplantModule();
		assertTrue( robo.loadProject( Paths.get( 
				InitUtils.getResourcesDir("roboplant", RoboplantModuleTest.class) + "btn.xml" )));

		assertEquals(1, robo.devicesByAddr.length);
		DeviceBTN d = (DeviceBTN)robo.devicesByAddr[0];
		
		assertEquals( "BTN", d.devtype );
		
		assertTrue( d.init(robo) );
		assertTrue( d.prepare() );

		assertEquals( "Beep1", d.name );
		assertEquals( "Beep1", d.tagname );
	}
	
	
	private void exec(
			int output	,       
			int input	
			) {
		
		d.execute();
		assertEquals("output"	, output	, d.tagOutput.getInt());     
		assertEquals("input"	, input		, d.tagInput.getInt());     
	}


	@Test
	public void testExecute_0() {
		d.tagTimeDel.setInt(0);
		d.tagTimeImp.setInt(0);

		exec(0,0);
		d.tagInput.setInt(1);
		exec(1,1);
		exec(0,0);
		exec(0,0);
	}

	@Test
	public void testExecute_timeimp_1() {
		d.tagTimeDel.setInt(0);
		d.tagTimeImp.setInt(1);

		exec(0,0);
		d.tagInput.setInt(1);
		exec(1,1);
		exec(1,1);
		exec(0,0);
	}

	@Test
	public void testExecute_timeimp_2() {
		d.tagTimeDel.setInt(0);
		d.tagTimeImp.setInt(2);

		exec(0,0);
		d.tagInput.setInt(1);
		exec(1,1);
		exec(1,1);
		exec(1,1);
		exec(0,0);
	}

	@Test
	public void testExecute_timedel_1() {
		d.tagTimeDel.setInt(1);
		d.tagTimeImp.setInt(0);

		exec(0,0);
		d.tagInput.setInt(1);
		exec(0,1);
		exec(1,1);
		exec(0,0);
	}

	@Test
	public void testExecute_timedel_2() {
		d.tagTimeDel.setInt(2);
		d.tagTimeImp.setInt(0);

		exec(0,0);
		d.tagInput.setInt(1);
		exec(0,1);
		exec(0,1);
		exec(1,1);
		exec(0,0);
	}

	@Test
	public void testExecute_timedel_3_timeimp_1() {
		d.tagTimeDel.setInt(3);
		d.tagTimeImp.setInt(1);

		exec(0,0);
		d.tagInput.setInt(1);
		exec(0,1);
		exec(0,1);
		exec(0,1);
		exec(1,1);
		exec(1,1);
		exec(0,0);
	}


}
