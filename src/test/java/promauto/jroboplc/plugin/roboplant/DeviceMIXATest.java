package promauto.jroboplc.plugin.roboplant;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import promauto.jroboplc.core.api.InitUtils;
import promauto.jroboplc.core.tags.TagInt;

public class DeviceMIXATest {

	private DeviceZDVH d;

	@Mock RoboplantModule module;


	@Before
	public void setUp() throws Exception {
		InitUtils.setupLogger();
		
		d = new DeviceZDVH();
		d.module = module;

		d.tagOpened   			= new TagInt("", 0);
		d.tagClosed   			= new TagInt("", 0);
		d.tagBlok     			= new TagInt("", 0);
		d.tagFlags    			= new TagInt("", 0);
		d.chnlIn 				= new Channel();
		d.chnlIn.tagAddrNum  	= new TagInt("", 0);
		d.chnlIn.tagValue	  	= new TagInt("", -1);
		d.chnlIn.state 			= Channel.State.Ok;

//		d.resetState();
	}
	
	private void exec(
			int closed	,        
			int opened	,          
			int chnlIn
			) {
		
		d.execute();
		assertEquals("closed"	, closed	, d.tagClosed.getInt());     
		assertEquals("opened"	, opened	, d.tagOpened.getInt());     
		assertEquals("chnlIn"	, chnlIn	, d.chnlIn.tagValue.getInt());     
	}

	private void setIn(int op) {
		d.chnlIn.tagValue.setInt(op);
	}

	
	
	@Test
	public void testLinkChannel_False() {
		d.chnlIn.state = Channel.State.Error;
		assertFalse(d.execute());
	}


	@Test
	public void testFlags_0() {
		d.tagFlags.setInt(0);
		setIn(0);
		exec(1,0,0);
		exec(1,0,0);

		setIn(1);
		exec(0,1,1);
		exec(0,1,1);
		
		setIn(0);
		exec(1,0,0);
		exec(1,0,0);

		setIn(1);
		exec(0,1,1);
		exec(0,1,1);
	}
	
	@Test
	public void testFlags_0x80() {
		d.tagFlags.setInt(0x80);
		setIn(0);
		exec(0,1,0);

		setIn(1);
		exec(1,0,1);
		
		setIn(0);
		exec(0,1,0);

		setIn(1);
		exec(1,0,1);
	}
	
	
	
}