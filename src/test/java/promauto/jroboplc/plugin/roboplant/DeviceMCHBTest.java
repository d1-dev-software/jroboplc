package promauto.jroboplc.plugin.roboplant;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import promauto.jroboplc.core.api.InitUtils;
import promauto.jroboplc.core.api.Tag;
import promauto.jroboplc.core.tags.TagInt;


public class DeviceMCHBTest {

	private DeviceMCHB d;

	@Mock RoboplantModule module;
	
	Channel channelOut = new Channel();
	Channel channelIn  = new Channel();
	
	Tag tagInpInput = new TagInt("", 0);
	Tag tagInpTrigres = new TagInt("", 0);
	Tag tagInpSuspend = new TagInt("", 0);
	Tag tagInpInputZ = new TagInt("", 0);
	

	@Before
	public void setUp() throws Exception {
		InitUtils.setupLogger();
		
		d = new DeviceMCHB();
		d.module = module;
		
		d.channelOut 	= channelOut;
		d.channelIn 	= channelIn;
		
		channelOut.tagAddrNum 	= new TagInt("", 0); 
		channelOut.tagValue = new TagInt("", -1);
		channelIn.tagAddrNum 	= new TagInt("", 0); 
		channelIn.tagValue 	= new TagInt("", -1);
		
		d.channelOut.state = Channel.State.Ok;
		d.channelIn.state = Channel.State.Ok;

		d.tagMasterOut = new TagInt("", 0);
		d.tagSost      = new TagInt("", 0);
	    d.tagAlarm     = new TagInt("", 0);
	    d.tagPlata     = new TagInt("", 0);
	    d.tagCnt       = new TagInt("", 0);
	    d.tagTrigRes   = new TagInt("", 0);
	    d.tagControl   = new TagInt("", 0);
	    d.tagBlok      = new TagInt("", 0);
	    d.tagTimeStart = new TagInt("", 0);
	    d.tagTimeStop  = new TagInt("", 0);
	    d.tagFlags     = new TagInt("", 0);
	    d.tagInput     = new TagInt("", 0);
		d.tagInputExt  = new TagInt("", 0);
		d.tagCtrlBeep  = new TagInt("", 0);

	    Input inp = new Input();
	    inp.tag = tagInpInput;
	    LinkedList<Input> inplist = new LinkedList<>();
	    inplist.add(inp);
		d.inpInput.add( inplist );

		Input trig = new Input();
	    trig.tag = tagInpTrigres;
	    d.inpTrigres.add( trig );

		Input susp = new Input();
	    susp.tag = tagInpSuspend;
	    d.inpSuspend.add( susp );

	}

	private void attachInputZ() {
		d.inputZ = new Input();
		d.inputZ.tag = tagInpInputZ;
	}

	private Input makeInput(String name, int refaddr) {
		Input input = new Input();
		input.name = name; 
		input.refaddr = refaddr;
		d.inputs.add(input);
		return input;
	}

	
	private void exec(
			int valMasterOut, 
			int valSost, 
			int valAlarm, 
			int valPlata, 
			int valCnt, 
			int valTrigRes, 
			int valInput, 
			int valInputExt,
			int valChannelOut,
			int valChannelIn 
			) {
		
		d.execute();
		assertEquals("MasterOut", 	valMasterOut, 	d.tagMasterOut.getInt()	);
		assertEquals("Sost", 		valSost, 		d.tagSost.getInt() 		);
		assertEquals("Alarm", 		valAlarm,		d.tagAlarm.getInt() 		);
		assertEquals("Plata", 		valPlata, 		d.tagPlata.getInt() 		);
		assertEquals("Cnt", 		valCnt,			d.tagCnt.getInt() 			);
		assertEquals("TrigRes", 	valTrigRes,		d.tagTrigRes.getInt() 		);
		assertEquals("Input", 		valInput,		d.tagInput.getInt() 		);
		assertEquals("InputExt", 	valInputExt,	d.tagInputExt.getInt() 	);
		assertEquals("ChannelOut", 	valChannelOut,	channelOut.tagValue.getInt() 	);
		assertEquals("ChannelIn", 	valChannelIn,	channelIn.tagValue.getInt() 	);
	}

	
	
	@Test
	public void testPrepareInputs() {
		d.inpInput.clear();
		d.inpSuspend.clear();
		d.inpTrigres.clear();
		
		makeInput("InputA",  1); 
		makeInput("InputA",  2); 
		makeInput("InputB",  3); 
		makeInput("InputC",  4); 
		makeInput("InputC",  5); 
		makeInput("InputZ",  6); 
		makeInput("TrigRes", 7); 
		makeInput("TrigRes", 8); 
		makeInput("Suspend", 9);
		
		assertEquals( 0, d.inpInput.size());
		assertEquals( 0, d.inpTrigres.size());
		assertEquals( 0, d.inpSuspend.size());
		
		d.prepareInputArrays();

		assertEquals( 3, d.inpInput.size());
		assertEquals( 2, d.inpTrigres.size());
		assertEquals( 1, d.inpSuspend.size());
		
		assertEquals( 6, d.inputZ.refaddr );
		
		assertEquals( 1, d.inpInput.get(0).get(0).refaddr );
		assertEquals( 2, d.inpInput.get(0).get(1).refaddr );
		assertEquals( 3, d.inpInput.get(1).get(0).refaddr );
		assertEquals( 4, d.inpInput.get(2).get(0).refaddr );
		assertEquals( 5, d.inpInput.get(2).get(1).refaddr );
		assertEquals( 7, d.inpTrigres.get(0).refaddr );
		assertEquals( 8, d.inpTrigres.get(1).refaddr );
		assertEquals( 9, d.inpSuspend.get(0).refaddr );

		
		d.prepareInputArrays();

		assertEquals( 3, d.inpInput.size());
		assertEquals( 2, d.inpTrigres.size());
		assertEquals( 1, d.inpSuspend.size());

	}

	
	@Test
	public void testPrepareInputs_Empty() {
		d.inpInput.clear();
		d.inpSuspend.clear();
		d.inpTrigres.clear();

		assertEquals( 0, d.inpInput.size());
		assertEquals( 0, d.inpTrigres.size());
		assertEquals( 0, d.inpSuspend.size());
		
		d.prepareInputArrays();

		assertEquals( 0, d.inpInput.size());
		assertEquals( 0, d.inpTrigres.size());
		assertEquals( 0, d.inpSuspend.size());
	}
	
	
	@Test
	public void testGetValueAnd() {
		List<Input> list = new LinkedList<>();
		
		
		Input inp1 = mock(Input.class);
		list.add(inp1);
		
		when(inp1.getInt()).thenReturn(0);
		assertFalse( d.getValueAnd(list));
		assertFalse( d.getValueOr(list));

		when(inp1.getInt()).thenReturn(1);
		assertTrue( d.getValueAnd(list));
		assertTrue( d.getValueOr(list));
		
		
		Input inp2 = mock(Input.class);
		list.add(inp2);
		
		when(inp2.getInt()).thenReturn(0);
		assertFalse( d.getValueAnd(list));
		assertTrue( d.getValueOr(list));

		when(inp2.getInt()).thenReturn(1);
		assertTrue( d.getValueAnd(list));
		assertTrue( d.getValueOr(list));

	}

	
	@Test
	public void testLinkChannels_mode_0() {
		channelOut.tagAddrNum.setInt(0);
		channelIn.tagAddrNum.setInt(1);

		// good
		d.channelOut.state = Channel.State.Ok;
		d.channelIn.state = Channel.State.Ok;
		assertTrue( d.linkChannels(0) );
		assertEquals(0, d.tagSost.getInt() );

		// bad channelOut
		d.channelOut.state = Channel.State.Error;
		d.channelIn.state = Channel.State.Ok;
		assertFalse( d.linkChannels(0) );

		// bad channelIn
		d.channelOut.state = Channel.State.Ok;
		d.channelIn.state = Channel.State.Error;
		assertFalse( d.linkChannels(0) );

		assertEquals( 1, channelIn.tagAddrNum.getInt() );
		assertEquals(8, d.tagSost.getInt() );
	}

	@Test
	public void testLinkChannels_mode_1() {
		channelOut.tagAddrNum.setInt(0);
		channelIn.tagAddrNum.setInt(1);

		// good
		d.channelOut.state = Channel.State.Ok;
		d.channelIn.state = Channel.State.Ok;
		assertTrue( d.linkChannels(1) );
		assertEquals( 0, channelIn.tagAddrNum.getInt() );
		assertEquals( 0, d.tagSost.getInt() );

		// bad channelOut
		d.channelOut.state = Channel.State.Error;
		d.channelIn.state = Channel.State.Ok;
		assertFalse( d.linkChannels(1) );

		// bad channelIn
		d.channelOut.state = Channel.State.Ok;
		d.channelIn.state = Channel.State.Error;
		assertFalse( d.linkChannels(1) );

		assertEquals(8, d.tagSost.getInt() );
	}

	@Test
	public void testLinkChannels_mode_1_execute() {
		channelOut.tagAddrNum.setInt(0);
		channelIn.tagAddrNum.setInt(1);

		// bad channelOut
		d.channelOut.state = Channel.State.Error;
		d.channelIn.state = Channel.State.Ok;
		assertFalse( d.linkChannels(1) );

		// bad channelIn
		d.channelOut.state = Channel.State.Ok;
		d.channelIn.state = Channel.State.Error;
		assertFalse( d.linkChannels(1) );
		
		d.execute();

		assertEquals(8, d.tagSost.getInt() );
	}

	@Test
	public void testLinkChannels_mode_2() {
		channelOut.tagAddrNum.setInt(0);
		channelIn.tagAddrNum.setInt(1);

		// good
		d.channelOut.state = Channel.State.Ok;
		d.channelIn.state = Channel.State.Ok;
		assertTrue( d.linkChannels(2) );
		assertEquals( 0, channelIn.tagAddrNum.getInt() );
		assertEquals( 0, d.tagSost.getInt() );

		// bad channelOut
		d.channelOut.state = Channel.State.Error;
		d.channelIn.state = Channel.State.Ok;
		assertTrue( d.linkChannels(2) );

		// bad channelIn
		d.channelOut.state = Channel.State.Ok;
		d.channelIn.state = Channel.State.Error;
		assertFalse( d.linkChannels(2) );

		assertEquals(8, d.tagSost.getInt() );
	}

	@Test
	public void testLinkChannels_mode_3() {
		channelOut.tagAddrNum.setInt(0);
		channelIn.tagAddrNum.setInt(1);

		// good
		d.channelOut.state = Channel.State.Ok;
		d.channelIn.state = Channel.State.Ok;
		assertTrue( d.linkChannels(3) );

		// bad channelOut
		d.channelOut.state = Channel.State.Error;
		d.channelIn.state = Channel.State.Ok;
		assertTrue( d.linkChannels(3) );

		// bad channelIn
		d.channelOut.state = Channel.State.Ok;
		d.channelIn.state = Channel.State.Error;
		assertTrue( d.linkChannels(3) );

		assertEquals( 1, channelIn.tagAddrNum.getInt() );
		assertEquals( 0, d.tagSost.getInt() );
	}

	
	

	
	
	@Test
	public void testExecute_Mode3_Control_3() {
		d.tagFlags.setInt(3);
		d.tagControl.setInt(3);
		
		exec(0,2,0, 1,1,1, 0,0, -1,-1); // start

		exec(0,1,0, 1,0,0, 0,0, -1,-1); // working

		d.tagControl.setInt(0);
		exec(0,3,0, 0,1,0, 0,0, -1,-1); // stop

		exec(0,0,0, 0,0,0, 0,0, -1,-1); // off
	}


	@Test
	public void testExecute_Mode3_Control_3_with_delay() {
		d.tagFlags.setInt(3);

		exec(0,0,0, 0,0,0, 0,0, -1,-1); // off
		assertEquals(0, d.tagCtrlBeep.getInt());

		d.tagControl.setInt(0x0203);

		exec(0,0,0, 0,0,0, 0,0, -1,-1); // off
		assertEquals(0x0203, d.tagControl.getInt());
		assertEquals(1, d.tagCtrlBeep.getInt());

		exec(0,0,0, 0,0,0, 0,0, -1,-1); // off
		assertEquals(0x0203, d.tagControl.getInt());
		assertEquals(1, d.tagCtrlBeep.getInt());

		exec(0,2,0, 1,1,1, 0,0, -1,-1); // start
		assertEquals(0x0003, d.tagControl.getInt());
		assertEquals(0, d.tagCtrlBeep.getInt());

	}


	@Test
	public void testExecute_Mode3_ExtInp() {
		d.tagFlags.setInt(3);
		d.tagControl.setInt(0x10);
		
		exec(0,2,0, 1,1,0, 1,1, -1,-1); // start

		exec(1,1,0, 1,0,0, 1,1, -1,-1); // working

		d.tagControl.setInt(0); 
		exec(0,3,0, 0,1,0, 0,0, -1,-1); // stop

		exec(0,0,0, 0,0,0, 0,0, -1,-1); // off
	}

	@Test
	public void testExecute_Mode3_Inp() {
		d.tagFlags.setInt(3);
		tagInpInput.setInt(1);
		
		exec(0,2,0, 1,1,0, 1,0, -1,-1);

		exec(1,1,0, 1,0,0, 1,0, -1,-1);
		
		tagInpInput.setInt(0);
		exec(0,3,0, 0,1,0, 0,0, -1,-1); // stop

		exec(0,0,0, 0,0,0, 0,0, -1,-1); // off
	}

	
	@Test
	public void testExecute_Mode3_Inp_Control() {
		d.tagFlags.setInt(3);
		tagInpInput.setInt(1);
		
		exec(0,2,0, 1,1,0, 1,0, -1,-1);

		exec(1,1,0, 1,0,0, 1,0, -1,-1); // working

		d.tagControl.setInt(1);
		exec(1,3,0, 0,1,0, 1,0, -1,-1); // control = 1

		exec(1,0,0, 0,0,0, 1,0, -1,-1);

		d.tagControl.setInt(0);
		exec(0,2,0, 1,1,0, 1,0, -1,-1); // control = 0
		
		exec(1,1,0, 1,0,0, 1,0, -1,-1); // working
	
		d.tagControl.setInt(1);
		exec(1,3,0, 0,1,0, 1,0, -1,-1); // control = 1

		exec(1,0,0, 0,0,0, 1,0, -1,-1);

		d.tagControl.setInt(3);
		exec(1,2,0, 1,1,1, 1,0, -1,-1); // control = 3

		exec(1,1,0, 1,0,0, 1,0, -1,-1); // working
	}

	@Test
	public void testExecute_Mode3_Inp_Timestart_0() {
		d.tagFlags.setInt(3);
		d.tagTimeStart.setInt(0);
		tagInpInput.setInt(1);
		
		exec(0,2,0, 1,1,0, 1,0, -1,-1);
		exec(1,1,0, 1,0,0, 1,0, -1,-1);
	}

	@Test
	public void testExecute_Mode3_Inp_Timestart_1() {
		d.tagFlags.setInt(3);
		d.tagTimeStart.setInt(1);
		tagInpInput.setInt(1);
		
		exec(0,2,0, 1,1,0, 1,0, -1,-1);
		exec(1,1,0, 1,0,0, 1,0, -1,-1);
	}

	@Test
	public void testExecute_Mode3_Inp_Timestart_2() {
		d.tagFlags.setInt(3);
		d.tagTimeStart.setInt(2);
		tagInpInput.setInt(1);
		
		exec(0,2,0, 1,1,0, 1,0, -1,-1);
		exec(0,2,0, 1,2,0, 1,0, -1,-1);
		exec(1,1,0, 1,0,0, 1,0, -1,-1);
	}


	@Test
	public void testExecute_Mode0_Flags_b8_0() {
		d.tagFlags.setInt(0);
		
		channelIn.tagValue.setInt(0);
		exec(0,7,0, 1,0,0, 0,0, 0,0);
		
		channelIn.tagValue.setInt(1);
		exec(0,0,0, 0,0,0, 0,0, 0,1);
	}

	@Test
	public void testExecute_Mode0_Flags_b8_1() {
		d.tagFlags.setInt(0x80);
		
		channelIn.tagValue.setInt(0);
		exec(0,0,0, 0,0,0, 0,0, 0,0);
		
		channelIn.tagValue.setInt(1);
		exec(0,7,0, 1,0,0, 0,0, 0,1);
	}

	@Test
	public void testExecute_Mode0_Inp() {
		d.tagFlags.setInt(0x80);
		d.tagTimeStart.setInt(2);
		
		channelIn.tagValue.setInt(0); 
		tagInpInput.setInt(1);
		exec(0,2,0, 0,1,0, 1,0, 1,0); // start
		
		channelIn.tagValue.setInt(1);
		exec(0,2,0, 1,2,0, 1,0, 1,1);
		
		exec(1,1,0, 1,0,0, 1,0, 1,1); // working
		
		tagInpInput.setInt(0);
		exec(0,3,0, 1,1,0, 0,0, 0,1); // stop

		exec(0,7,0, 1,0,0, 0,0, 0,1);
		
		channelIn.tagValue.setInt(0);
		exec(0,0,0, 0,0,0, 0,0, 0,0);
	}

	
	@Test
	public void testExecute_Mode0_Inp_Blok() {
		d.tagFlags.setInt(0x80);
		d.tagBlok.setInt(1);
		channelIn.tagValue.setInt(0);
		
		tagInpInput.setInt(1);
		exec(0,2,0, 0,1,0, 1,0, 1,0); // start

		exec(1,1,0, 0,0,0, 1,0, 1,0); // working
		
		tagInpInput.setInt(0);
		exec(0,3,0, 0,1,0, 0,0, 0,0); // stop

		exec(0,0,0, 0,0,0, 0,0, 0,0); // off
	}

	@Test
	public void testExecute_Mode0_Inp_Blok_Alarm() {
		d.tagFlags.setInt(0x80);
		d.tagBlok.setInt(1);
		channelIn.tagValue.setInt(0);
		
		tagInpInput.setInt(1);
		exec(0,2,0, 0,1,0, 1,0, 1,0); // start

		exec(1,1,0, 0,0,0, 1,0, 1,0); // working
		
		d.tagBlok.setInt(0);
		exec(0,4,1, 0,0,0, 1,0, 0,0);
	}

	@Test
	public void testExecute_Mode0_Inp_Alarm() {
		d.tagFlags.setInt(0x80);
		d.tagTimeStart.setInt(2);
		
		channelIn.tagValue.setInt(0);
		tagInpInput.setInt(1);
		exec(0,2,0, 0,1,0, 1,0, 1,0); // start
		exec(0,2,0, 0,2,0, 1,0, 1,0);
		
		exec(0,4,1, 0,0,0, 1,0, 0,0); // alarm
		tagInpInput.setInt(0);

		exec(0,4,1, 0,0,0, 0,0, 0,0); // alarm
	}
	
	@Test
	public void testExecute_Mode0_Inp_Control_Alarm() {
		d.tagFlags.setInt(0x80);
		d.tagTimeStart.setInt(2);
		
		channelIn.tagValue.setInt(0);
		d.tagControl.setInt(3);
		exec(0,2,0, 0,1,1, 0,0, 1,0); // start
		exec(0,2,0, 0,2,0, 0,0, 1,0);
		
		exec(0,4,1, 0,0,0, 0,0, 0,0); // alarm

		d.tagControl.setInt(0);
		exec(0,4,1, 0,0,0, 0,0, 0,0); // still alarm
		exec(0,4,1, 0,0,0, 0,0, 0,0); // 
		exec(0,4,1, 0,0,0, 0,0, 0,0); // 

		d.tagControl.setInt(3);
		exec(0,0,0, 0,0,1, 0,0, 0,0); // start once more time
		exec(0,2,0, 0,1,0, 0,0, 1,0); // 
	}



	@Test
	public void testExecute_Mode0_Alarm_TrigRes() {
		d.tagFlags.setInt(0x80);
		d.sost = 4;
		channelIn.tagValue.setInt(0);
		tagInpInput.setInt(1);
		
		exec(0,4,1, 0,0,0, 1,0, 0,0); // alarm

		tagInpTrigres.setInt(1);
		exec(0,0,0, 0,0,1, 1,0, 0,0); // reset
		
		tagInpTrigres.setInt(0);
		exec(0,2,0, 0,1,0, 1,0, 1,0); // start
	}

	@Test
	public void testExecute_Mode0_Alarm_Control_4() {
		d.tagFlags.setInt(0x80);
		d.sost = 4;
		channelIn.tagValue.setInt(0);
		tagInpInput.setInt(1);
		
		exec(0,4,1, 0,0,0, 1,0, 0,0); // alarm

		d.tagControl.setInt(0x04);
		exec(0,0,0, 0,0,1, 1,0, 0,0); // reset
		assertEquals( 0, d.tagControl.getInt() );
		
		tagInpTrigres.setInt(0);
		exec(0,2,0, 0,1,0, 1,0, 1,0); // start
	}

	@Test
	public void testExecute_Mode0_Control_8() {
		d.tagFlags.setInt(0x80);
		d.sost = 1;
		channelIn.tagValue.setInt(1);
		tagInpInput.setInt(1);
		
		exec(1,1,0, 1,0,0, 1,0, 1,1); // working

		d.tagControl.setInt(0x08);
		exec(0,4,1, 1,0,0, 1,0, 0,1); // alarm
		assertEquals( 0, d.tagControl.getInt() );
	}

	@Test
	public void testExecute_Mode0_Timestop_0() {
		d.tagFlags.setInt(0x80);
		d.tagTimeStop.setInt(0);
		channelIn.tagValue.setInt(1);
		tagInpInput.setInt(1);
		d.sost = 1;
		
		exec(1,1,0, 1,0,0, 1,0, 1,1); // working
		
		tagInpInput.setInt(0);
		exec(0,3,0, 1,1,0, 0,0, 0,1); // stop

		channelIn.tagValue.setInt(0);
		exec(0,0,0, 0,0,0, 0,0, 0,0); // off
	}

	@Test
	public void testExecute_Mode0_Timestop_1() {
		d.tagFlags.setInt(0x80);
		d.tagTimeStop.setInt(1);
		channelIn.tagValue.setInt(1);
		tagInpInput.setInt(1);
		d.sost = 1;
		
		exec(1,1,0, 1,0,0, 1,0, 1,1); // working
		
		tagInpInput.setInt(0);
		exec(0,3,0, 1,1,0, 0,0, 1,1); // stop

		channelIn.tagValue.setInt(0);
		exec(0,0,0, 0,0,0, 0,0, 0,0); // off
	}

	@Test
	public void testExecute_Mode0_Timestop_2() {
		d.tagFlags.setInt(0x80);
		d.tagTimeStop.setInt(5);
		channelIn.tagValue.setInt(1);
		tagInpInput.setInt(1);
		d.sost = 1;
		
		exec(1,1,0, 1,0,0, 1,0, 1,1); // working
		
		tagInpInput.setInt(0);
		exec(0,3,0, 1,1,0, 0,0, 1,1); // stop
		exec(0,3,0, 1,2,0, 0,0, 1,1); // stop
		exec(0,3,0, 1,3,0, 0,0, 1,1); // stop
		exec(0,3,0, 1,4,0, 0,0, 1,1); // stop
		exec(0,3,0, 1,5,0, 0,0, 1,1); // stop

		channelIn.tagValue.setInt(0);
		exec(0,0,0, 0,0,0, 0,0, 0,0); // off
	}

	@Test
	public void testExecute_Mode0_Suspend_Start() {
		d.tagFlags.setInt(0x80);
		channelIn.tagValue.setInt(0);
		tagInpSuspend.setInt(1);
		
		exec(0,0,0, 0,0,0, 0,0, 0,0); // off

		tagInpInput.setInt(1);
		exec(0,5,0, 0,0,0, 1,0, 0,0); // susp start
		
		tagInpSuspend.setInt(0);
		exec(0,2,0, 0,1,0, 1,0, 1,0); // start
	}

	@Test
	public void testExecute_Mode0_Suspend_Running() {
		d.tagFlags.setInt(0x80);
		channelIn.tagValue.setInt(1);
		tagInpInput.setInt(1);
		d.sost = 1;
		
		exec(1,1,0, 1,0,0, 1,0, 1,1); // working
		
		tagInpSuspend.setInt(1);
		exec(0,5,0, 1,0,0, 1,0, 0,1); // susp 
	}

	@Test
	public void testExecute_Mode0_InputZ() {
		attachInputZ();
		d.tagFlags.setInt(0x80);
		d.tagTimeStart.setInt(2);
		
		tagInpInputZ.setInt(0);
		channelIn.tagValue.setInt(1);
		tagInpInput.setInt(1);
		exec(0,2,0, 0,1,0, 1,0, 1,1); // start
		
		tagInpInputZ.setInt(1);
		channelIn.tagValue.setInt(0);
		exec(0,2,0, 1,2,0, 1,0, 1,0); // start
		
		exec(1,1,0, 1,0,0, 1,0, 1,0); // working
	}

	
	@Test
	public void testExecute_Mode2_Inp() {
		d.tagFlags.setInt(0x82);
		d.tagTimeStart.setInt(2);
		
		channelIn.tagValue.setInt(0);
		tagInpInput.setInt(1);
		exec(0,0,0, 0,0,0, 1,0, -1,0); // start
		
		channelIn.tagValue.setInt(1);
		exec(1,1,0, 1,0,0, 1,0, -1,1); // feedback on
		
		exec(1,1,0, 1,0,0, 1,0, -1,1); // working
		
		tagInpInput.setInt(0);
		exec(0,1,0, 1,0,0, 0,0, -1,1); // stop
		exec(0,1,0, 1,0,0, 0,0, -1,1); 

		channelIn.tagValue.setInt(0);
		exec(0,0,0, 0,0,0, 0,0, -1,0); // feedback off
	}

	
	@Test
	public void testExecute_Mode1_Flags_b2_0() {
		d.tagFlags.setInt(0x81);
		d.tagTimeStart.setInt(3);
		
		channelIn.tagValue.setInt(1);
		tagInpInput.setInt(1);
		exec(0,2,0, 1,1,0, 1,0, 1,1);
		exec(0,2,0, 1,2,0, 1,0, 1,1);
		exec(0,2,0, 1,3,0, 1,0, 1,1);
		exec(1,1,0, 1,0,0, 1,0, 1,1);
	}

	
	@Test
	public void testExecute_Mode1_Flags_b2_1() {
		d.tagFlags.setInt(0x85);
		d.tagTimeStart.setInt(3);
		
		channelIn.tagValue.setInt(1);
		tagInpInput.setInt(1);
		exec(0,2,0, 1,1,0, 1,0, 1,1);
		exec(1,1,0, 1,0,0, 1,0, 1,1);
	}

	@Test
	public void testExecute_Mode1_Flags_b3_1() {
		d.tagFlags.setInt(0x89);
		
		d.tagControl.setInt(0);
		exec(0,0,0, 0,0,0, 0,0, 0,-1);

		d.tagControl.setInt(0x10);
		exec(0,0,0, 0,0,0, 0,1, 0,-1);
	}

	@Test
	public void testExecute_Mode1_Flags_b4_1() {
		d.tagFlags.setInt(0x91);
		d.tagTimeStart.setInt(3);
		channelIn.tagValue.setInt(1);
		tagInpInput.setInt(1);
		d.sost = 1;
		
		exec(1,1,0, 1,0,0, 1,0, 1,1); // working

		channelIn.tagValue.setInt(0);
		exec(1,1,0, 0,1,0, 1,0, 1,0); // no feedback
		exec(1,1,0, 0,2,0, 1,0, 1,0); 
		exec(1,1,0, 0,3,0, 1,0, 1,0); 

		exec(0,4,1, 0,0,0, 1,0, 0,0); // alarm
	}


	
	@Test
	public void testExecute_Alarm_on_Suspend() {
		d.tagFlags.setInt(0x180);
		channelIn.tagValue.setInt(0);
		tagInpSuspend.setInt(1);
		
		exec(0,0,0, 0,0,0, 0,0, 0,0); // off

		tagInpInput.setInt(1);
		exec(0,5,1, 0,0,0, 1,0, 0,0); // susp start
		
		tagInpSuspend.setInt(0);
		exec(0,2,0, 0,1,0, 1,0, 1,0); // start
	}
	

	
	@Test
	public void testExecute_Ignore_Suspend_on_start() {
		d.tagFlags.setInt(0x280);
		d.tagTimeStart.setInt(2);
		channelIn.tagValue.setInt(0);
		tagInpSuspend.setInt(1);
		
		exec(0,0,0, 0,0,0, 0,0, 0,0); // off

		tagInpInput.setInt(1);
		exec(0,2,0, 0,1,0, 1,0, 1,0); // susp start

		channelIn.tagValue.setInt(1);
		exec(0,2,0, 1,2,0, 1,0, 1,1); // susp start
		exec(0,5,0, 1,0,0, 1,0, 0,1); // susp start
		
		
		channelIn.tagValue.setInt(0);
		tagInpSuspend.setInt(0);
		exec(0,2,0, 0,1,0, 1,0, 1,0); // start
	}
	
	
	@Test
	public void testLoad() throws Exception {
		
		RoboplantModule robo = AuxProcs.createRoboplantModule();
		assertTrue( robo.loadProject( Paths.get( 
				InitUtils.getResourcesDir("roboplant", RoboplantModuleTest.class) + "mchb.xml" )));

		assertEquals(1, robo.devicesByAddr.length);
		DeviceMCHB d = (DeviceMCHB)robo.devicesByAddr[0];
		
		assertEquals( "MCHB", d.devtype );
		assertEquals( "618", d.tagname );
		
		assertEquals(3, d.inputs.size());
		assertEquals(15, d.outputs.size());
		
		assertTrue( d.init(robo) );
		assertTrue( d.prepare() );

		assertEquals(1, d.inpInput.size());
		assertEquals(1, d.inpInput.get(0).size());
		assertEquals(1, d.inpSuspend.size());
		assertEquals(1, d.inpTrigres.size());
	}
	
	
}
