package promauto.jroboplc.plugin.roboplant;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static promauto.jroboplc.plugin.roboplant.AuxProcs.newInputAndTag;

import java.nio.file.Paths;

import org.junit.Before;
import org.junit.Test;

import promauto.jroboplc.core.api.InitUtils;
import promauto.jroboplc.core.tags.TagInt;

public class DeviceSQBATest {
	private DeviceSQBA d;
	
	@Before
	public void setUp() throws Exception {
		InitUtils.setupLogger();

		d = new DeviceSQBA();
		d.inpCntOn 		= newInputAndTag();
		d.inpCntOff 	= newInputAndTag();
		
		d.tagOutput		= new TagInt("", 0);
		d.tagValOn		= new TagInt("", 2);
		d.tagValOff		= new TagInt("", 3);
	}

	@Test
	public void testLoad() throws Exception {
		RoboplantModule robo = AuxProcs.createRoboplantModule();
		assertTrue( robo.loadProject( Paths.get( 
				InitUtils.getResourcesDir("roboplant", RoboplantModuleTest.class) + "sqba.xml" )));

		assertEquals(1, robo.devicesByAddr.length);
		d = (DeviceSQBA)robo.devicesByAddr[0];
		assertTrue( d.init(robo) );
		assertTrue( d.prepare() );

		assertEquals( "SQBA", d.devtype );
		assertEquals( "S01__2", d.name );
		assertEquals( "T0000", d.tagname );
	}
	
	private void set(
			int cnton	,
			int cntoff
			) {
		d.inpCntOn .tag.setInt(cnton );
		d.inpCntOff.tag.setInt(cntoff);
	}

	
	private void exec(
			int output 
			) {
		d.execute();
		assertEquals("output"	, output	 , d.tagOutput.getInt());     
	}


	@Test
	public void testExecute() {
		set(0,0); 	exec(0);
		set(1,0); 	exec(0);
		set(2,0); 	exec(1);
		set(3,0); 	exec(1);
		set(4,0); 	exec(1);
		set(4,1); 	exec(1);
		set(4,2); 	exec(1);
		set(4,3); 	exec(0);
		set(4,4); 	exec(0);
		set(4,5); 	exec(0);
	}

}
