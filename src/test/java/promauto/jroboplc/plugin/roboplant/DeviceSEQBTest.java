package promauto.jroboplc.plugin.roboplant;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static promauto.jroboplc.plugin.roboplant.AuxProcs.createTagWithOutput;
import static promauto.jroboplc.plugin.roboplant.AuxProcs.newInputAndTag;

import java.nio.file.Paths;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import promauto.jroboplc.core.State;
import promauto.jroboplc.core.api.InitUtils;


public class DeviceSEQBTest {
	private DeviceSEQB d;
	
	@Mock RoboplantModule module;

	
	@Before
	public void setUp() throws Exception {
		InitUtils.setupLogger();
		
		d = new DeviceSEQB();
		d.module = module;
		d.devtype = "SEQB";
		d.tagname = "S01";

		d.inpStart 		= newInputAndTag();    
		d.inpStop 		= newInputAndTag();     
		d.inpStopEmrg 	= newInputAndTag(); 
		d.inpSusp 		= newInputAndTag();     
		d.inpSuspStart 	= newInputAndTag();
		d.inpZdvOk 		= newInputAndTag(1);    
		d.inpDvuOk 		= newInputAndTag(1);    
		d.inpAuxOk 		= newInputAndTag(1);    
		d.inpAspOk 		= newInputAndTag(1);    
		
		
		d.tagSost         = createTagWithOutput(d, "Sost"        , 0, 0);
		d.tagSetZdv       = createTagWithOutput(d, "SetZdv"      , 0, 0);
		d.tagCntOn        = createTagWithOutput(d, "CntOn"       , 0, 0);
		d.tagCntOff       = createTagWithOutput(d, "CntOff"      , 0, 0);
		d.tagCnt          = createTagWithOutput(d, "Cnt"         , 0, 0);
		d.tagCntStop      = createTagWithOutput(d, "CntStop"     , 0, 0);
		d.tagZdvBad       = createTagWithOutput(d, "ZdvBad"      , 0, 0);
		d.tagDvuBad       = createTagWithOutput(d, "DvuBad"      , 0, 0);
		d.tagAuxBad       = createTagWithOutput(d, "AuxBad"      , 0, 0);
		d.tagAspBad       = createTagWithOutput(d, "AspBad"      , 0, 0);
		d.tagReadyToStart = createTagWithOutput(d, "ReadyToStart", 0, 0);
		d.tagStoping      = createTagWithOutput(d, "Stoping"     , 0, 0);
		d.tagWaitZdvOk    = createTagWithOutput(d, "WaitZdvOk"   , 0, 0);
		d.tagQntOn        = createTagWithOutput(d, "QntOn"       , 0, 5);
		d.tagQntOff       = createTagWithOutput(d, "QntOff"      , 0, 3);
		d.tagDlyStart     = createTagWithOutput(d, "DlyStart"    , 0, 0);
		d.tagDlyStopZdv   = createTagWithOutput(d, "DlyStopZdv"  , 0, 0);
		d.tagDlyStopDvu   = createTagWithOutput(d, "DlyStopDvu"  , 0, 0);
		d.tagDlyStopAux   = createTagWithOutput(d, "DlyStopAux"  , 0, 0);
		d.tagDlyStopAsp   = createTagWithOutput(d, "DlyStopAsp"  , 0, 0);
		d.tagFlags        = createTagWithOutput(d, "Flags"       , 0, 0);
		d.tagStartSpeed   = createTagWithOutput(d, "StartSpeed"  , 0, 1);
		d.tagStopSpeed    = createTagWithOutput(d, "StopSpeed"   , 0, 1);
		d.tagDlyMesZdv    = createTagWithOutput(d, "DlyMesZdv"   , 0, 0);
		d.tagDlyMesDvu    = createTagWithOutput(d, "DlyMesDvu"   , 0, 0);
		d.tagDlyMesAux    = createTagWithOutput(d, "DlyMesAux"   , 0, 0);
		d.tagDlyMesAsp    = createTagWithOutput(d, "DlyMesAsp"   , 0, 0);

//		d.resetState();
	}


	

	
	
	@Test
	public void testLoad() throws Exception {
		
		RoboplantModule robo = AuxProcs.createRoboplantModule();
		assertTrue( robo.loadProject( Paths.get( 
				InitUtils.getResourcesDir("roboplant", RoboplantModuleTest.class) + "seqb.xml" )));

		assertEquals(1, robo.devicesByAddr.length);
		DeviceSEQB d = (DeviceSEQB)robo.devicesByAddr[0];
		
		assertEquals( "SEQB", d.devtype );
		assertEquals( "S05", d.tagname );
		
		assertTrue( d.init(robo) );
		assertTrue( d.prepare() );

	}

	
	
	private void exec(
			int sost         ,
			int setZdv       ,
			int cntOn        ,
			int cntOff       ,
			int cnt          ,
			int cntStop      ,
			int zdvBad       ,
			int dvuBad       ,
			int auxBad       ,
			int aspBad       ,
			int readyToStart ,
			int stoping      ,
			int waitZdvOk          
			) {
		
		d.execute();	
		assertEquals("sost        ", sost        , d.tagSost        .getInt());       
		assertEquals("setZdv      ", setZdv      , d.tagSetZdv      .getInt());       
		assertEquals("cntOn       ", cntOn       , d.tagCntOn       .getInt());       
		assertEquals("cntOff      ", cntOff      , d.tagCntOff      .getInt());       
		assertEquals("cnt         ", cnt         , d.tagCnt         .getInt());       
		assertEquals("cntStop     ", cntStop     , d.tagCntStop     .getInt());       
		assertEquals("zdvBad      ", zdvBad      , d.tagZdvBad      .getInt());       
		assertEquals("dvuBad      ", dvuBad      , d.tagDvuBad      .getInt());       
		assertEquals("auxBad      ", auxBad      , d.tagAuxBad      .getInt());       
		assertEquals("aspBad      ", aspBad      , d.tagAspBad      .getInt());       
		assertEquals("readyToStart", readyToStart, d.tagReadyToStart.getInt());       
		assertEquals("stoping     ", stoping     , d.tagStoping     .getInt());       
		assertEquals("waitZdvOk   ", waitZdvOk   , d.tagWaitZdvOk   .getInt());       
	}
	

	
	@Test
	public void testStartStop_speed_0() {
		d.tagStartSpeed.setInt(0);
		d.tagStopSpeed.setInt(0);
		exec(0,0, 0,0,  0,    0,       0,0,0,0, 0,0,0);
		
		
		d.inpStart.tag.setInt(1);
		exec(5,1, 5,0,  0,65535,       0,0,0,0, 0,0,0);
		d.inpStart.tag.setInt(0);
		
		d.inpStop.tag.setInt(1);
		exec(0,0, 5,3,  0,    0,       0,0,0,0, 0,0,0);
		d.inpStop.tag.setInt(0);
		exec(0,0, 0,0,  0,    0,       0,0,0,0, 0,0,0);
	}

	@Test
	public void testStartStop_speed_1() {
		d.tagStartSpeed.setInt(1);
		d.tagStopSpeed.setInt(1);
		exec(0,0, 0,0,  0,    0,       0,0,0,0, 0,0,0);
		
		d.inpStart.tag.setInt(1);
		exec(3,1, 0,0,  1,65535,       0,0,0,0, 0,0,0);
		d.inpStart.tag.setInt(0);
		exec(3,1, 1,0,  1,65535,       0,0,0,0, 0,0,0);
		exec(3,1, 2,0,  1,65535,       0,0,0,0, 0,0,0);
		exec(3,1, 3,0,  1,65535,       0,0,0,0, 0,0,0);
		exec(3,1, 4,0,  1,65535,       0,0,0,0, 0,0,0);

		exec(5,1, 5,0,  0,65535,       0,0,0,0, 0,0,0);
		exec(5,1, 5,0,  0,65535,       0,0,0,0, 0,0,0);
		exec(5,1, 5,0,  0,65535,       0,0,0,0, 0,0,0);
		
		d.inpStop.tag.setInt(1);
		exec(6,1, 5,0,  1,    0,       0,0,0,0, 0,1,0);
		d.inpStop.tag.setInt(0);
		exec(6,1, 5,1,  1,    0,       0,0,0,0, 0,1,0);
		exec(6,1, 5,2,  1,    0,       0,0,0,0, 0,1,0);
		exec(0,0, 5,3,  0,    0,       0,0,0,0, 0,0,0);
		exec(0,0, 0,0,  0,    0,       0,0,0,0, 0,0,0);
	}

	@Test
	public void testStartStop_speed_2() {
		d.tagStartSpeed.setInt(2);
		d.tagStopSpeed.setInt(2);
		exec(0,0, 0,0,  0,    0,       0,0,0,0, 0,0,0);
		
		d.inpStart.tag.setInt(1);
		exec(3,1, 0,0,  1,65535,       0,0,0,0, 0,0,0);
		d.inpStart.tag.setInt(0);
		exec(3,1, 0,0,  2,65535,       0,0,0,0, 0,0,0);
		exec(3,1, 1,0,  1,65535,       0,0,0,0, 0,0,0);
		exec(3,1, 1,0,  2,65535,       0,0,0,0, 0,0,0);
		exec(3,1, 2,0,  1,65535,       0,0,0,0, 0,0,0);
		exec(3,1, 2,0,  2,65535,       0,0,0,0, 0,0,0);
		exec(3,1, 3,0,  1,65535,       0,0,0,0, 0,0,0);
		exec(3,1, 3,0,  2,65535,       0,0,0,0, 0,0,0);
		exec(3,1, 4,0,  1,65535,       0,0,0,0, 0,0,0);
		exec(3,1, 4,0,  2,65535,       0,0,0,0, 0,0,0);

		exec(5,1, 5,0,  0,65535,       0,0,0,0, 0,0,0);
		exec(5,1, 5,0,  0,65535,       0,0,0,0, 0,0,0);
		exec(5,1, 5,0,  0,65535,       0,0,0,0, 0,0,0);
		
		d.inpStop.tag.setInt(1);
		exec(6,1, 5,0,  1,    0,       0,0,0,0, 0,1,0);
		d.inpStop.tag.setInt(0);
		exec(6,1, 5,0,  2,    0,       0,0,0,0, 0,1,0);
		exec(6,1, 5,1,  1,    0,       0,0,0,0, 0,1,0);
		exec(6,1, 5,1,  2,    0,       0,0,0,0, 0,1,0);
		exec(6,1, 5,2,  1,    0,       0,0,0,0, 0,1,0);
		exec(6,1, 5,2,  2,    0,       0,0,0,0, 0,1,0);
		
		exec(0,0, 5,3,  0,    0,       0,0,0,0, 0,0,0);
		exec(0,0, 0,0,  0,    0,       0,0,0,0, 0,0,0);
		exec(0,0, 0,0,  0,    0,       0,0,0,0, 0,0,0);
	}

	
	
	
	public void quickstart() {
		d.tagStartSpeed.setInt(0);
		d.inpStart.tag.setInt(1);
		exec(5,1, 5,0,  0,65535,       0,0,0,0, 0,0,0);
		d.inpStart.tag.setInt(0);
		d.tagStartSpeed.setInt(1);
	}	
	
	
	
	@Test
	public void testStopEmrg() {
		quickstart();
		
		d.inpStopEmrg.tag.setInt(1);
		exec(0,0, 0,0,  0,    0,       0,0,0,0, 0,0,0);
		d.inpStopEmrg.tag.setInt(0);
		exec(0,0, 0,0,  0,    0,       0,0,0,0, 0,0,0);
	}
	
	
	
	@Test
	public void testSusp() {
		
		// Susp does suspend while starting only
		d.inpStart.tag.setInt(1);
		exec(3,1, 0,0,  1,65535,       0,0,0,0, 0,0,0);
		d.inpStart.tag.setInt(0);
		exec(3,1, 1,0,  1,65535,       0,0,0,0, 0,0,0);
		
		d.inpSusp.tag.setInt(1);
		exec(4,1, 1,0,  1,65535,       0,0,0,0, 0,0,0);
		d.inpSusp.tag.setInt(0);
		exec(4,1, 1,0,  1,65535,       0,0,0,0, 0,0,0);
		exec(4,1, 1,0,  1,65535,       0,0,0,0, 0,0,0);
		
		d.inpStart.tag.setInt(1);
		exec(3,1, 1,0,  1,65535,       0,0,0,0, 0,0,0);
		d.inpStart.tag.setInt(0);
		exec(3,1, 2,0,  1,65535,       0,0,0,0, 0,0,0);
		exec(3,1, 3,0,  1,65535,       0,0,0,0, 0,0,0);
		exec(3,1, 4,0,  1,65535,       0,0,0,0, 0,0,0);

		exec(5,1, 5,0,  0,65535,       0,0,0,0, 0,0,0);
		exec(5,1, 5,0,  0,65535,       0,0,0,0, 0,0,0);
		exec(5,1, 5,0,  0,65535,       0,0,0,0, 0,0,0);
		
		// Susp doesn't suspend while stopping
		d.inpStop.tag.setInt(1);
		exec(6,1, 5,0,  1,    0,       0,0,0,0, 0,1,0);
		d.inpStop.tag.setInt(0);
		exec(6,1, 5,1,  1,    0,       0,0,0,0, 0,1,0);
		d.inpSusp.tag.setInt(1);
		exec(6,1, 5,2,  1,    0,       0,0,0,0, 0,1,0);
		d.inpSusp.tag.setInt(0);
		exec(0,0, 5,3,  0,    0,       0,0,0,0, 0,0,0);
		exec(0,0, 0,0,  0,    0,       0,0,0,0, 0,0,0);
	}
	
	
	@Test
	public void testZdvOk_start() {
		d.inpZdvOk.tag.setInt(0);
		
		d.inpStart.tag.setInt(1);
		exec(1,1, 0,0,  0,    0,       0,0,0,0, 0,0,1);
		d.inpStart.tag.setInt(0);
		exec(1,1, 0,0,  0,    0,       0,0,0,0, 0,0,1);
		exec(1,1, 0,0,  0,    0,       0,0,0,0, 0,0,1);
		exec(1,1, 0,0,  0,    0,       0,0,0,0, 0,0,1);

		d.inpZdvOk.tag.setInt(1);
		exec(2,1, 0,0,  0,    0,       0,0,0,0, 1,0,0);
		exec(2,1, 0,0,  0,    0,       0,0,0,0, 1,0,0);
		exec(2,1, 0,0,  0,    0,       0,0,0,0, 1,0,0);

		d.inpStart.tag.setInt(1);
		exec(3,1, 0,0,  1,65535,       0,0,0,0, 0,0,0);
		d.inpStart.tag.setInt(0);
		exec(3,1, 1,0,  1,65535,       0,0,0,0, 0,0,0);
		exec(3,1, 2,0,  1,65535,       0,0,0,0, 0,0,0);
		exec(3,1, 3,0,  1,65535,       0,0,0,0, 0,0,0);
		exec(3,1, 4,0,  1,65535,       0,0,0,0, 0,0,0);
		exec(5,1, 5,0,  0,65535,       0,0,0,0, 0,0,0);
	}	
	
	@Test
	public void testZdvOk_run() {
		quickstart();
		d.inpZdvOk.tag.setInt(0);
		exec(5,1, 5,0,  0,65535,       1,0,0,0, 0,0,0);
		exec(5,1, 5,0,  0,65535,       1,0,0,0, 0,0,0);
		exec(5,1, 5,0,  0,65535,       1,0,0,0, 0,0,0);
		
		d.tagFlags.setInt(0b0_0001);
		exec(6,1, 5,0,  1,    0,       0,0,0,0, 0,1,0);
	}	
	
	
	@Test
	public void testDvuOk_start() {
		d.inpDvuOk.tag.setInt(0);
		
		d.inpStart.tag.setInt(1);
		exec(1,1, 0,0,  0,    0,       0,0,0,0, 0,0,1);
		d.inpStart.tag.setInt(0);
		exec(1,1, 0,0,  0,    0,       0,0,0,0, 0,0,1);
		exec(1,1, 0,0,  0,    0,       0,0,0,0, 0,0,1);
		exec(1,1, 0,0,  0,    0,       0,0,0,0, 0,0,1);

		d.inpDvuOk.tag.setInt(1);
		exec(2,1, 0,0,  0,    0,       0,0,0,0, 1,0,0);
		exec(2,1, 0,0,  0,    0,       0,0,0,0, 1,0,0);
		exec(2,1, 0,0,  0,    0,       0,0,0,0, 1,0,0);

		d.inpStart.tag.setInt(1);
		exec(3,1, 0,0,  1,65535,       0,0,0,0, 0,0,0);
		d.inpStart.tag.setInt(0);
		exec(3,1, 1,0,  1,65535,       0,0,0,0, 0,0,0);
		exec(3,1, 2,0,  1,65535,       0,0,0,0, 0,0,0);
		exec(3,1, 3,0,  1,65535,       0,0,0,0, 0,0,0);
		exec(3,1, 4,0,  1,65535,       0,0,0,0, 0,0,0);
		exec(5,1, 5,0,  0,65535,       0,0,0,0, 0,0,0);
	}	
	
	@Test
	public void testDvuOk_run() {
		quickstart();
		d.inpDvuOk.tag.setInt(0);
		exec(5,1, 5,0,  0,65535,       0,1,0,0, 0,0,0);
		exec(5,1, 5,0,  0,65535,       0,1,0,0, 0,0,0);
		exec(5,1, 5,0,  0,65535,       0,1,0,0, 0,0,0);
		
		d.tagFlags.setInt(0b0_0010);
		exec(6,1, 5,0,  1,    0,       0,0,0,0, 0,1,0);
	}	
	
	
	@Test
	public void testAuxOk_start() {
		d.inpAuxOk.tag.setInt(0);
		
		d.inpStart.tag.setInt(1);
		exec(1,1, 0,0,  0,    0,       0,0,0,0, 0,0,1);
		d.inpStart.tag.setInt(0);
		exec(1,1, 0,0,  0,    0,       0,0,0,0, 0,0,1);
		exec(1,1, 0,0,  0,    0,       0,0,0,0, 0,0,1);
		exec(1,1, 0,0,  0,    0,       0,0,0,0, 0,0,1);

		d.inpAuxOk.tag.setInt(1);
		exec(2,1, 0,0,  0,    0,       0,0,0,0, 1,0,0);
		exec(2,1, 0,0,  0,    0,       0,0,0,0, 1,0,0);
		exec(2,1, 0,0,  0,    0,       0,0,0,0, 1,0,0);

		d.inpStart.tag.setInt(1);
		exec(3,1, 0,0,  1,65535,       0,0,0,0, 0,0,0);
		d.inpStart.tag.setInt(0);
		exec(3,1, 1,0,  1,65535,       0,0,0,0, 0,0,0);
		exec(3,1, 2,0,  1,65535,       0,0,0,0, 0,0,0);
		exec(3,1, 3,0,  1,65535,       0,0,0,0, 0,0,0);
		exec(3,1, 4,0,  1,65535,       0,0,0,0, 0,0,0);
		exec(5,1, 5,0,  0,65535,       0,0,0,0, 0,0,0);
	}	
	
	@Test
	public void testAuxOk_run() {
		quickstart();
		d.inpAuxOk.tag.setInt(0);
		exec(5,1, 5,0,  0,65535,       0,0,1,0, 0,0,0);
		exec(5,1, 5,0,  0,65535,       0,0,1,0, 0,0,0);
		exec(5,1, 5,0,  0,65535,       0,0,1,0, 0,0,0);
		
		d.tagFlags.setInt(0b0_0100);
		exec(6,1, 5,0,  1,    0,       0,0,0,0, 0,1,0);
	}	
	
	
	@Test
	public void testAspOk_start() {
		d.inpAspOk.tag.setInt(0);
		
		d.inpStart.tag.setInt(1);
		exec(3,1, 0,0,  1,65535,       0,0,0,0, 0,0,0);
		exec(3,1, 1,0,  1,65535,       0,0,0,0, 0,0,0);
		exec(3,1, 2,0,  1,65535,       0,0,0,0, 0,0,0);
		exec(3,1, 3,0,  1,65535,       0,0,0,0, 0,0,0);
		exec(3,1, 4,0,  1,65535,       0,0,0,0, 0,0,0);
		exec(5,1, 5,0,  0,65535,       0,0,0,1, 0,0,0);
		d.inpAspOk.tag.setInt(1);
		exec(5,1, 5,0,  0,65535,       0,0,0,0, 0,0,0);
	}	
	
	@Test
	public void testAspOk_run() {
		quickstart();
		d.inpAspOk.tag.setInt(0);
		exec(5,1, 5,0,  0,65535,       0,0,0,1, 0,0,0);
		exec(5,1, 5,0,  0,65535,       0,0,0,1, 0,0,0);
		exec(5,1, 5,0,  0,65535,       0,0,0,1, 0,0,0);
		
		d.tagFlags.setInt(0b0_1000);
		exec(6,1, 5,0,  1,    0,       0,0,0,0, 0,1,0);
	}	
	
		
	@Test
	public void testDlySetZdv_3() {
		d.tagDlyStart.setInt(3);
		d.inpZdvOk.tag.setInt(0);
		exec(0,0, 0,0,  0,    0,       0,0,0,0, 0,0,0);
		exec(0,0, 0,0,  0,    0,       0,0,0,0, 0,0,0);
		exec(0,0, 0,0,  0,    0,       0,0,0,0, 0,0,0);
		
		d.inpStart.tag.setInt(1);
		exec(1,1, 0,0,  1,    0,       0,0,0,0, 0,0,1);
		d.inpStart.tag.setInt(0);
		exec(1,1, 0,0,  2,    0,       0,0,0,0, 0,0,1);
		d.inpZdvOk.tag.setInt(1);
		exec(3,1, 0,0,  1,65535,       0,0,0,0, 0,0,0);
		d.inpZdvOk.tag.setInt(0);
		exec(3,1, 1,0,  1,65535,       1,0,0,0, 0,0,0);
		exec(3,1, 2,0,  1,65535,       1,0,0,0, 0,0,0);
		exec(3,1, 3,0,  1,65535,       1,0,0,0, 0,0,0);
		exec(3,1, 4,0,  1,65535,       1,0,0,0, 0,0,0);
		exec(5,1, 5,0,  0,65535,       1,0,0,0, 0,0,0);
		exec(5,1, 5,0,  0,65535,       1,0,0,0, 0,0,0);
		exec(5,1, 5,0,  0,65535,       1,0,0,0, 0,0,0);

	}	
	
	@Test
	public void testReadyToStart() {
		d.tagDlyStart.setInt(3);
		d.inpDvuOk.tag.setInt(0);
		exec(0,0, 0,0,  0,    0,       0,0,0,0, 0,0,0);
		exec(0,0, 0,0,  0,    0,       0,0,0,0, 0,0,0);
		exec(0,0, 0,0,  0,    0,       0,0,0,0, 0,0,0);
		
		d.inpStart.tag.setInt(1);
		exec(1,1, 0,0,  1,    0,       0,0,0,0, 0,0,1);
		d.inpStart.tag.setInt(0);
		exec(1,1, 0,0,  2,    0,       0,0,0,0, 0,0,1);
		exec(1,1, 0,0,  3,    0,       0,0,0,0, 0,0,1);
		exec(1,1, 0,0,  3,    0,       0,0,0,0, 0,0,1);
		exec(1,1, 0,0,  3,    0,       0,0,0,0, 0,0,1);

		d.inpDvuOk.tag.setInt(1);
		exec(2,1, 0,0,  3,    0,       0,0,0,0, 1,0,0);
		exec(2,1, 0,0,  3,    0,       0,0,0,0, 1,0,0);
		exec(2,1, 0,0,  3,    0,       0,0,0,0, 1,0,0);

		d.inpStart.tag.setInt(1);
		exec(3,1, 0,0,  1,65535,       0,0,0,0, 0,0,0);
		d.inpStart.tag.setInt(0);
		d.inpDvuOk.tag.setInt(0);
		exec(3,1, 1,0,  1,65535,       0,1,0,0, 0,0,0);
		exec(3,1, 2,0,  1,65535,       0,1,0,0, 0,0,0);
		exec(3,1, 3,0,  1,65535,       0,1,0,0, 0,0,0);
		exec(3,1, 4,0,  1,65535,       0,1,0,0, 0,0,0);
		exec(5,1, 5,0,  0,65535,       0,1,0,0, 0,0,0);
		exec(5,1, 5,0,  0,65535,       0,1,0,0, 0,0,0);
		exec(5,1, 5,0,  0,65535,       0,1,0,0, 0,0,0);

	}	

	
	@Test
	public void testDlyStopAux_start() {
		d.tagDlyStopAux.setInt(3);
		
		d.inpAuxOk.tag.setInt(0);
		d.tagFlags.setInt(0b0_0100);
		
		d.inpStart.tag.setInt(1);
		exec(1,1, 0,0,  0,    0,       0,0,0,0, 0,0,1);
		d.inpStart.tag.setInt(0);
		exec(1,1, 0,0,  0,    0,       0,0,0,0, 0,0,1);
		exec(1,1, 0,0,  0,    0,       0,0,0,0, 0,0,1);
		exec(1,1, 0,0,  0,    0,       0,0,0,0, 0,0,1);

		d.inpAuxOk.tag.setInt(1);
		exec(2,1, 0,0,  0,    0,       0,0,0,0, 1,0,0);
		exec(2,1, 0,0,  0,    0,       0,0,0,0, 1,0,0);
		exec(2,1, 0,0,  0,    0,       0,0,0,0, 1,0,0);

		d.inpStart.tag.setInt(1);
		exec(3,1, 0,0,  1,65535,       0,0,0,0, 0,0,0);
		d.inpStart.tag.setInt(0);
		d.inpAuxOk.tag.setInt(0);
		exec(3,1, 1,0,  1,    2,       0,0,1,0, 0,0,0);
		exec(3,1, 2,0,  1,    1,       0,0,1,0, 0,0,0);
		exec(3,1, 3,0,  1,    0,       0,0,1,0, 0,0,0);
		exec(6,1, 3,1,  1,    0,       0,0,0,0, 0,1,0);
		exec(6,1, 3,2,  1,    0,       0,0,0,0, 0,1,0);
	}	
	
	@Test
	public void testDlyStopAux_run() {
		quickstart();
		d.tagDlyStopAux.setInt(3);
		d.inpAuxOk.tag.setInt(0);
		exec(5,1, 5,0,  0,65535,       0,0,1,0, 0,0,0);
		exec(5,1, 5,0,  0,65535,       0,0,1,0, 0,0,0);
		exec(5,1, 5,0,  0,65535,       0,0,1,0, 0,0,0);
		exec(5,1, 5,0,  0,65535,       0,0,1,0, 0,0,0);
		exec(5,1, 5,0,  0,65535,       0,0,1,0, 0,0,0);
		
		d.tagFlags.setInt(0b0_0100);
		exec(5,1, 5,0,  0,    2,       0,0,1,0, 0,0,0);
		exec(5,1, 5,0,  0,    1,       0,0,1,0, 0,0,0);
		exec(5,1, 5,0,  0,    0,       0,0,1,0, 0,0,0);
		exec(6,1, 5,0,  1,    0,       0,0,0,0, 0,1,0);
	}	
	

	
	@Test
	public void testSaveLoad() throws Exception {
		State state = new State();
		
		d.tagStartSpeed.setInt(1);
		d.tagStopSpeed.setInt(1);
		exec(0,0, 0,0,  0,    0,       0,0,0,0, 0,0,0);
		
		d.inpStart.tag.setInt(1);
		exec(3,1, 0,0,  1,65535,       0,0,0,0, 0,0,0);
		d.inpStart.tag.setInt(0);
		exec(3,1, 1,0,  1,65535,       0,0,0,0, 0,0,0);
		exec(3,1, 2,0,  1,65535,       0,0,0,0, 0,0,0);
		exec(3,1, 3,0,  1,65535,       0,0,0,0, 0,0,0);
		
		d.saveState(state);
		d = null;
		
		setUp();
		
		exec(0,0, 0,0,  0,    0,       0,0,0,0, 0,0,0);
		d.loadState(state);

		exec(3,1, 4,0,  1,65535,       0,0,0,0, 0,0,0);

		exec(5,1, 5,0,  0,65535,       0,0,0,0, 0,0,0);
		exec(5,1, 5,0,  0,65535,       0,0,0,0, 0,0,0);
		exec(5,1, 5,0,  0,65535,       0,0,0,0, 0,0,0);
		
		d.inpStop.tag.setInt(1);
		exec(6,1, 5,0,  1,    0,       0,0,0,0, 0,1,0);
		d.inpStop.tag.setInt(0);
		exec(6,1, 5,1,  1,    0,       0,0,0,0, 0,1,0);
		exec(6,1, 5,2,  1,    0,       0,0,0,0, 0,1,0);
		exec(0,0, 5,3,  0,    0,       0,0,0,0, 0,0,0);
		exec(0,0, 0,0,  0,    0,       0,0,0,0, 0,0,0);
		
	}

}
