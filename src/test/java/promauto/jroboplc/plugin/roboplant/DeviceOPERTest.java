package promauto.jroboplc.plugin.roboplant;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static promauto.jroboplc.plugin.roboplant.AuxProcs.newInputAndTag;
import static promauto.jroboplc.plugin.roboplant.DeviceOPER.*;

import java.nio.file.Paths;

import org.junit.Before;
import org.junit.Test;

import promauto.jroboplc.core.api.InitUtils;
import promauto.jroboplc.core.tags.TagDouble;
import promauto.jroboplc.core.tags.TagInt;
import promauto.jroboplc.core.tags.TagLong;

public class DeviceOPERTest {
	private DeviceOPER d;
	
	@Before
	public void setUp() throws Exception {
		InitUtils.setupLogger();

		d = new DeviceOPER();
		d.inpInput1 	= newInputAndTag();
		d.inpInput2 	= newInputAndTag();
		
		d.tagOutput		= new TagInt("", 0);
		d.tagOper		= new TagInt("", 0);

		d.resetState();
	}

	@Test
	public void testLoad() throws Exception {
		RoboplantModule robo = AuxProcs.createRoboplantModule();
		assertTrue( robo.loadProject( Paths.get( 
				InitUtils.getResourcesDir("roboplant", RoboplantModuleTest.class) + "oper.xml" )));

		assertEquals(1, robo.devicesByAddr.length);
		d = (DeviceOPER)robo.devicesByAddr[0];
		assertTrue( d.init(robo) );
		assertTrue( d.prepare() );

		assertEquals( "OPER", d.devtype );
		assertEquals( "S05_501A", d.name );
		assertEquals( "S05_501A", d.tagname );
	}

	private void set(
			int oper,
			int input1,
			int input2
	) {
		d.tagOper.setInt(oper);
		d.inpInput1.tag.setInt(input1);
		d.inpInput2.tag.setInt(input2);
	}

	private void set(
			int oper,
			long input1,
			long input2
	) {
		d.tagOper.setInt(oper);
		d.inpInput1.tag.setLong(input1);
		d.inpInput2.tag.setLong(input2);
	}

	private void set(
			int oper,
			double input1,
			double input2
	) {
		d.tagOper.setInt(oper);
		d.inpInput1.tag.setDouble(input1);
		d.inpInput2.tag.setDouble(input2);
	}


	private void exec(
			int output
	) {
		d.execute();
		assertEquals("output"	, output	 , d.tagOutput.getInt());
	}

	private void exec(
			long output
	) {
		d.execute();
		assertEquals("output"	, output	 , d.tagOutput.getLong());
	}

	private void exec(
			double output
	) {
		d.execute();
		assertEquals("output"	, output	 , d.tagOutput.getDouble(), 0.001);
	}


	@Test
	public void testExecuteInt() {
		set(OPER_EQUAL, 0,0); 	exec(1); // =
		set(OPER_EQUAL, 1,0); 	exec(0); // =
		set(OPER_EQUAL, 0,2); 	exec(0); // =

		set(OPER_GREATER_OR_EQUAL, 2,2); 	exec(1); // >=
		set(OPER_GREATER_OR_EQUAL, 2,3); 	exec(0); // >=
		set(OPER_GREATER_OR_EQUAL, 3,2); 	exec(1); // >=
		
		set(OPER_LOWER_OR_EQUAL, 1,1); 	exec(1); // <=
		set(OPER_LOWER_OR_EQUAL, 2,3); 	exec(1); // <=
		set(OPER_LOWER_OR_EQUAL, 3,2); 	exec(0); // <=

		set(OPER_GREATER, 1,1); 	exec(0); // >
		set(OPER_GREATER, 2,3); 	exec(0); // >
		set(OPER_GREATER, 3,2); 	exec(1); // >

		set(OPER_LOWER, 1,1); 	exec(0); // <
		set(OPER_LOWER, 2,3); 	exec(1); // <
		set(OPER_LOWER, 3,2); 	exec(0); // <

		set(OPER_SUM, 1,1); 	exec(2); // sum
		set(OPER_SUM, 2,3); 	exec(5); // sum
		set(OPER_SUM, 3,2); 	exec(5); // sum

		set(OPER_SUB, 1,1); 	exec(0); // sub
//		set(6, 2,3); 	exec(0);// sub
		set(OPER_SUB, 2,3); 	exec(-1);// sub
		set(OPER_SUB, 3,2); 	exec(1); // sub

		set(OPER_XOR, 1,1); 	exec(0); // xor
		set(OPER_XOR, 2,3); 	exec(1); // xor
		set(OPER_XOR, 3,2); 	exec(1); // xor

		set(OPER_MUL, 1,1); 	exec(1); // mul
		set(OPER_MUL, 2,3); 	exec(6); // mul
		set(OPER_MUL, 4,2); 	exec(8); // mul

		set(OPER_DIV, 1,1); 	exec(1); // div
		set(OPER_DIV, 6,3); 	exec(2); // div
		set(OPER_DIV, 3,2); 	exec(1); // div
		set(OPER_DIV, 4,3); 	exec(1); // div

		set(OPER_MOD, 1,1); 	exec(0); // mod
		set(OPER_MOD, 5,3); 	exec(2); // mod
		set(OPER_MOD, 3,2); 	exec(1); // mod

		set(OPER_AND, 1,0); 	exec(0); // and
		set(OPER_AND, 2,3); 	exec(2); // and
		set(OPER_AND, 3,3); 	exec(3); // and

		set(OPER_OR, 1,0); 	exec(1); // or
		set(OPER_OR, 2,3); 	exec(3); // or
		set(OPER_OR, 4,2); 	exec(6); // or

		set(OPER_PERCENT, 1,1); 	exec(0); // %
		set(OPER_PERCENT, 2,3); 	exec(0); // %
		set(OPER_PERCENT, 3,2); 	exec(0); // %

		set(OPER_TEST_BITS, 1,1); 	exec(1); // (inp1&inp2)==inp2
		set(OPER_TEST_BITS, 2,3); 	exec(0); // (inp1&inp2)==inp2
		set(OPER_TEST_BITS, 7,3); 	exec(1); // (inp1&inp2)==inp2
	}

	@Test
	public void testExecuteLong() {
		d.tagOutput		= new TagLong("", 0);
		d.inpInput1 	= newInputAndTag(0L);
		d.inpInput2 	= newInputAndTag(0L);
		d.resetState();

		set(OPER_EQUAL, 0x7fff_ffff_ffffL,0x7fff_ffff_ffffL); 	exec(1); // =
		set(OPER_EQUAL, 0x7fff_ffff_ffffL,0); 	exec(0); // =

		set(OPER_GREATER_OR_EQUAL, 0x7fff_ffff_ffffL,0x7fff_ffff_fffeL); 	exec(1); // >=
		set(OPER_GREATER_OR_EQUAL, 0x7fff_ffff_fffeL,0x7fff_ffff_ffffL); 	exec(0); // >=

		set(OPER_LOWER_OR_EQUAL, 0x7fff_ffff_fffeL,0x7fff_ffff_ffffL); 	exec(1); // <=
		set(OPER_LOWER_OR_EQUAL, 0x7fff_ffff_ffffL,0x7fff_ffff_fffeL); 	exec(0); // <=

		set(OPER_GREATER, 0x7fff_ffff_ffffL,0x7fff_ffff_ffffL); 	exec(0); // >
		set(OPER_GREATER, -0x7fff_ffff_ffffL,-0x7fff_ffff_fffeL); 	exec(0); // >
		set(OPER_GREATER, 0x7fff_ffff_ffffL,0x7fff_ffff_fffeL); 	exec(1); // >

		set(OPER_LOWER, 0x7fff_ffff_fffeL,0x7fff_ffff_ffffL); 	exec(1); // <
		set(OPER_LOWER, 0x7fff_ffff_ffffL,0x7fff_ffff_fffeL); 	exec(0); // <
		set(OPER_LOWER, -0x7fff_ffff_fffeL,-0x7fff_ffff_ffffL); 	exec(0); // <

		set(OPER_SUM, 0x0fff_ffff_ffffL,0x7000_0000_0000L); 	exec(0x7fff_ffff_ffffL); // sum
		set(OPER_SUM, 0x7fff_ffff_ffffL,-0x0fff_ffff_ffffL); 	exec(0x7000_0000_0000L); // sum
		set(OPER_SUM, -0x0fff_ffff_ffffL,-0x7000_0000_0000L); 	exec(-0x7fff_ffff_ffffL); // sum

		set(OPER_SUB, 0x7fff_ffff_ffffL,0x7000_0000_0000L); 	exec(0x0fff_ffff_ffffL); // sub
		set(OPER_SUB, -0x0fff_ffff_ffffL,0x7000_0000_0000L); 	exec(-0x7fff_ffff_ffffL);// sub

		set(OPER_XOR, 0x7fff_ffff_ffffL,0x0fff_ffff_ffffL); 	exec(0x7000_0000_0000L); // xor

		set(OPER_MUL, 0x7ff_ffff_ffffL,16); 	exec(0x7fff_ffff_fff0L); // mul

		set(OPER_DIV, 0x7fff_ffff_fff0L,16); 	exec(0x7ff_ffff_ffffL); // div

		set(OPER_MOD, 0x7fff_ffff_ffffL,0x7000_0000_0000L); 	exec(0x0fff_ffff_ffffL); // mod

		set(OPER_AND, 0x1fff_ffff_ffffL,0x7000_0000_0000L); 	exec(0x1000_0000_0000L); // and

		set(OPER_OR, 0x1fff_ffff_ffffL,0x7000_0000_0000L); 	exec(0x7fff_ffff_ffffL); // or


		set(OPER_TEST_BITS, 0x1fff_ffff_ffffL,0x7000_0000_0000L); 	exec(0); // (inp1&inp2)==inp2
		set(OPER_TEST_BITS, 0x7fff_ffff_ffffL,0x7000_0000_0000L); 	exec(1); // (inp1&inp2)==inp2
	}


	@Test
	public void testExecuteDouble() {
		d.tagOutput		= new TagDouble("", 0);
		d.inpInput1 	= newInputAndTag(0.0);
		d.inpInput2 	= newInputAndTag(0.0);
		d.resetState();

		set(OPER_EQUAL, 0.0,0.0); 	exec(1); // =
		set(OPER_EQUAL, 0.0,0.2); 	exec(0); // =

		set(OPER_GREATER_OR_EQUAL, 2.2,2.1); 	exec(1); // >=

		set(OPER_LOWER_OR_EQUAL, 1.1,1.2); 	exec(1); // <=

		set(OPER_GREATER, 2.2,2.1); 	exec(1); // >

		set(OPER_LOWER, 2.2,2.3); 	exec(1); // <

		set(OPER_SUM, 1.1,1.2); 	exec(2.3); // sum

		set(OPER_SUB, 1.3,1.2); 	exec(0.1); // sub

		set(OPER_MUL, 1.1,0.1); 	exec(0.11); // mul

		set(OPER_DIV, 0.11,1.1); 	exec(0.1); // div
	}

}
