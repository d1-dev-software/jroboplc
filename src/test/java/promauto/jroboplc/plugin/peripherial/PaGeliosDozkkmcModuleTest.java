package promauto.jroboplc.plugin.peripherial;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import promauto.jroboplc.core.ConfigurationYaml;
import promauto.jroboplc.core.api.EnvironmentInst;
import promauto.jroboplc.core.api.SampleEnvironment;
import promauto.jroboplc.core.api.SamplePlugin;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static promauto.jroboplc.plugin.peripherial.PaGeliosDozkkmcModule.*;

class PaGeliosDozkkmcModuleTest {

    PaGeliosDozkkmcModuleDummy d;

    @BeforeEach
    void setUp() {
        SampleEnvironment env;
        env = new SampleEnvironment();
        EnvironmentInst.set(env);
        env.setConfiguration(new ConfigurationYaml(""));

        d = new PaGeliosDozkkmcModuleDummy(new SamplePlugin(), "");

        Map<Object,Object> conf = new HashMap<>();
        conf.put("type", "promauto.gelios.dozkkmc");
        conf.put("storqnt", 2);
        conf.put("version", 2);

        d.load(conf);
    }

    @Test
    void processCmdDly0Test() {
        d.tagDelayUnload.setInt(0);

        // no delay
        d.tagStatus.setInt(STATUS_UNLOAD_READY);
        d.tagCmdStartUnload.setInt(1);
        d.tagCmd.setInt(0);
        d.execute();
        assertEquals(1, d.tagCmdStartUnload.getInt());
        assertEquals(4, d.tagCmd.getWriteValInt());
    }

    @Test
    void processCmdDly1Test() throws InterruptedException {
        d.tagDelayUnload.setInt(1);
        d.tagStatus.setInt(STATUS_IDLE);

        d.tagCmd.setInt(0);
        d.execute();
        assertEquals(0, d.tagCmdStartUnload.getInt());
        assertEquals(0, d.tagCmd.getWriteValInt());

        // no effect because of status
        d.tagCmdStartUnload.setInt(1);
        d.tagCmd.setInt(0);
        d.execute();
        assertEquals(0, d.tagCmdStartUnload.getInt());
        assertEquals(0, d.tagCmd.getWriteValInt());

        // start of delay
        d.tagStatus.setInt(STATUS_UNLOAD_READY);
        d.tagCmdStartUnload.setInt(1);
        d.tagCmd.setInt(0);
        d.execute();
        assertEquals(1, d.tagCmdStartUnload.getInt());
        assertEquals(0, d.tagCmd.getWriteValInt());

        d.tagCmd.setInt(0);
        d.execute();
        assertEquals(1, d.tagCmdStartUnload.getInt());
        assertEquals(0, d.tagCmd.getWriteValInt());

        // delay completed
        Thread.sleep(1000);
        d.tagCmd.setInt(0);
        d.execute();
        assertEquals(1, d.tagCmdStartUnload.getInt());
        assertEquals(4, d.tagCmd.getWriteValInt());

        d.tagCmd.setInt(0);
        d.execute();
        assertEquals(1, d.tagCmdStartUnload.getInt());
        assertEquals(4, d.tagCmd.getWriteValInt());

        // reset because of status
        d.tagStatus.setInt(STATUS_UNLOAD);
        d.tagCmd.setInt(0);
        d.execute();
        assertEquals(0, d.tagCmdStartUnload.getInt());
        assertEquals(0, d.tagCmd.getWriteValInt());
    }


    @Test
    void processCmdDly2Test() {
        d.tagDelayUnload.setInt(2);

        d.tagStatus.setInt(STATUS_UNLOAD_READY);
        d.tagCmdStartUnload.setInt(1);
        d.tagCmd.setInt(0);
        d.execute();
        assertEquals(1, d.tagCmdStartUnload.getInt());
        assertEquals(0, d.tagCmd.getWriteValInt());

        d.tagCmd.setInt(0);
        d.execute();
        assertEquals(1, d.tagCmdStartUnload.getInt());
        assertEquals(0, d.tagCmd.getWriteValInt());

        // reset because of CmdStartUnload becomes zero
        d.tagCmdStartUnload.setInt(0);
        d.tagCmd.setInt(0);
        d.execute();
        assertEquals(0, d.tagCmdStartUnload.getInt());
        assertEquals(0, d.tagCmd.getWriteValInt());

    }

    @Test
    void processCmdDly3Test() {
        d.tagDelayUnload.setInt(2);

        d.tagStatus.setInt(STATUS_UNLOAD_READY);
        d.tagCmdStartUnload.setInt(1);
        d.tagCmd.setInt(0);
        d.execute();
        assertEquals(1, d.tagCmdStartUnload.getInt());
        assertEquals(0, d.tagCmd.getWriteValInt());

        d.tagCmd.setInt(0);
        d.execute();
        assertEquals(1, d.tagCmdStartUnload.getInt());
        assertEquals(0, d.tagCmd.getWriteValInt());

        // reset because of status
        d.tagStatus.setInt(STATUS_UNLOAD);
        d.tagCmd.setInt(0);
        d.execute();
        assertEquals(0, d.tagCmdStartUnload.getInt());
        assertEquals(0, d.tagCmd.getWriteValInt());
    }


    @Test
    void processCmdDly4Test() throws InterruptedException {
        d.tagDelayUnload.setInt(1);
        d.tagStatus.setInt(STATUS_UNLOAD_READY);

        // pass 1
        d.tagCmdStartUnload.setInt(1);
        d.tagCmd.setInt(0);
        d.execute();
        assertEquals(1, d.tagCmdStartUnload.getInt());
        assertEquals(0, d.tagCmd.getWriteValInt());

        d.tagCmd.setInt(0);
        d.execute();
        assertEquals(1, d.tagCmdStartUnload.getInt());
        assertEquals(0, d.tagCmd.getWriteValInt());

        Thread.sleep(1000);
        d.tagCmd.setInt(0);
        d.execute();
        assertEquals(1, d.tagCmdStartUnload.getInt());
        assertEquals(4, d.tagCmd.getWriteValInt());

        // reset
        d.tagCmdStartUnload.setInt(0);
        d.tagCmd.setInt(0);
        d.execute();
        assertEquals(0, d.tagCmdStartUnload.getInt());
        assertEquals(0, d.tagCmd.getWriteValInt());

        // pass 2
        d.tagCmdStartUnload.setInt(1);
        d.tagCmd.setInt(0);
        d.execute();
        assertEquals(1, d.tagCmdStartUnload.getInt());
        assertEquals(0, d.tagCmd.getWriteValInt());

        d.tagCmd.setInt(0);
        d.execute();
        assertEquals(1, d.tagCmdStartUnload.getInt());
        assertEquals(0, d.tagCmd.getWriteValInt());

        Thread.sleep(1000);
        d.tagCmd.setInt(0);
        d.execute();
        assertEquals(1, d.tagCmdStartUnload.getInt());
        assertEquals(4, d.tagCmd.getWriteValInt());

    }


}