package promauto.jroboplc.plugin.peripherial;

import promauto.jroboplc.core.api.Plugin;

public class PaGeliosDozkkmcModuleDummy extends PaGeliosDozkkmcModule {

    public PaGeliosDozkkmcModuleDummy(Plugin plugin, String name) {
        super(plugin, name);
    }

    @Override
    protected boolean readStatusAA55() throws Exception {
        return true;
    }

    @Override
    protected boolean readSumCountersAA55() throws Exception {
        return true;
    }

    @Override
    protected boolean readTaskWeightAA55() throws Exception {
        return true;
    }

    @Override
    protected boolean writeTaskWeightAA55() throws Exception {
        return true;
    }

    @Override
    protected boolean writeCmdAA55() throws Exception {
        return true;
    }

    @Override
    protected boolean requestFirmwareAA55() throws Exception {
        return true;
    }

    @Override
    protected boolean readStatusModbus() throws Exception {
        return true;
    }

    @Override
    protected boolean readSumCountersModbus() throws Exception {
        return true;
    }

    @Override
    protected boolean readTaskWeightModbus() throws Exception {
        return true;
    }

    @Override
    protected boolean writeTaskWeightModbus() throws Exception {
        return true;
    }

    @Override
    protected boolean writeCmdModbus() throws Exception {
        return true;
    }

    @Override
    protected boolean requestFirmwareModbus() throws Exception {
        return true;
    }
}
