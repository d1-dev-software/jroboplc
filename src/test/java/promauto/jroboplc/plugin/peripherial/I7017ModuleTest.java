package promauto.jroboplc.plugin.peripherial;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.fail;

import java.util.HashMap;

import org.junit.Before;
import org.junit.Test;

import promauto.jroboplc.core.api.InitUtils;
import promauto.jroboplc.core.api.SampleEnvironment;
import promauto.jroboplc.core.api.SamplePlugin;
import promauto.jroboplc.core.ConfigurationYaml;
import promauto.jroboplc.core.api.Configuration;
import promauto.jroboplc.core.api.EnvironmentInst;

public class I7017ModuleTest {
	private static String CFG_DIR = InitUtils.getResourcesDir("peripherial", PeripherialModule.class);

	I7017Module m;
	SampleProtocolIcpcon prot;


	@Before
	public void setUp() throws Exception {
		InitUtils.setupLogger();

		SampleEnvironment env = new SampleEnvironment();
		EnvironmentInst.set(env);
		
		Configuration conf = new ConfigurationYaml( CFG_DIR );
		if( !conf.load() ) 
			fail();
		env.setConfiguration(conf);

		SamplePlugin plug = new SamplePlugin();

		m = new I7017Module(plug, "di");
		m.load(new HashMap<String,String>());
		m.retrial = 3;
		m.netaddr = 1;

		prot = new SampleProtocolIcpcon(m);
		m.protocol = prot; 
	}

	
	
	@Test
	public void testExecute() {
		prot.pushIn( ">FF257FFF002500250025002400250023??\r" );
		m.execute();
		
		assertFalse( m.tagError.getBool());
		assertEquals( 0, m.tagErrorCnt.getInt());
		
		String expout = "#01";
		String actout = prot.pollOut();

		assertEquals(0xFF25-0x10000, m.inputs[0].value.getInt() );
		assertEquals(0x7FFF, m.inputs[1].value.getInt() );
		assertEquals(0x25, m.inputs[2].value.getInt() );
		assertEquals(0x25, m.inputs[3].value.getInt() );
		assertEquals(0x25, m.inputs[4].value.getInt() );
		assertEquals(0x24, m.inputs[5].value.getInt() );
		assertEquals(0x25, m.inputs[6].value.getInt() );
		assertEquals(0x23, m.inputs[7].value.getInt() );
	}
	
	@Test
	public void testExecuteMinus() {
		m.nominus = true;
		m.initTags();
		
		prot.pushIn( ">FF257FFF002500250025002400250023??\r" );
		m.execute();
		
		assertEquals(0x00, m.inputs[0].value.getInt() );
		assertEquals(0x7FFF, m.inputs[1].value.getInt() );
		assertEquals(0x25, m.inputs[2].value.getInt() );
		assertEquals(0x25, m.inputs[3].value.getInt() );
		assertEquals(0x25, m.inputs[4].value.getInt() );
		assertEquals(0x24, m.inputs[5].value.getInt() );
		assertEquals(0x25, m.inputs[6].value.getInt() );
		assertEquals(0x23, m.inputs[7].value.getInt() );
	}
	
	@Test
	public void testExecuteScale() {
		m.noscale = false;
		m.initTags();
		
		for( int i=0; i<8; ++i )
		{
			m.inputs[i].v1.setInt( 10 );
			m.inputs[i].v2.setInt( 20 );
			m.inputs[i].s1.setDouble( 0.5 );
			m.inputs[i].s2.setDouble( 1.5 );
		}
		
		prot.pushIn( ">000A0014000F00000005002400250023??\r" );
		                  //___.___.___.___.___.___.___.___.
		m.execute();
		
		assertEquals(0.5,  m.inputs[0].scaled.getDouble(), 0.001 );
		assertEquals(1.5,  m.inputs[1].scaled.getDouble(), 0.001 );
		assertEquals(1.0,  m.inputs[2].scaled.getDouble(), 0.001 );
		assertEquals(-0.5, m.inputs[3].scaled.getDouble(), 0.001 );
		assertEquals(0.0,  m.inputs[4].scaled.getDouble(), 0.001 );
	}
	
	
}

