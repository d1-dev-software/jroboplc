package promauto.jroboplc.plugin.peripherial;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static promauto.jroboplc.plugin.peripherial.ModbusTag.Access.RO;
import static promauto.jroboplc.plugin.peripherial.ModbusTag.Access.RW;
import static promauto.jroboplc.plugin.peripherial.ModbusTag.Access.WO;
import static promauto.jroboplc.plugin.peripherial.ModbusTag.Region.COIL;
import static promauto.jroboplc.plugin.peripherial.ModbusTag.Region.HLDREG;
import static promauto.jroboplc.plugin.peripherial.ModbusTag.Region.INPREG;
import static promauto.jroboplc.plugin.peripherial.ModbusTag.Type.BOOL;
import static promauto.jroboplc.plugin.peripherial.ModbusTag.Type.FLOAT16;
import static promauto.jroboplc.plugin.peripherial.ModbusTag.Type.FLOAT32;
import static promauto.jroboplc.plugin.peripherial.ModbusTag.Type.INT16;
import static promauto.jroboplc.plugin.peripherial.ModbusTag.Type.INT32;
import static promauto.jroboplc.plugin.peripherial.ModbusTag.Type.STRING;
import static promauto.jroboplc.plugin.peripherial.ModbusTag.Type.UINT16;

import java.util.Arrays;
import java.util.HashMap;

import org.junit.Before;
import org.junit.Test;

import promauto.jroboplc.core.api.InitUtils;
import promauto.jroboplc.core.api.SampleEnvironment;
import promauto.jroboplc.core.api.SamplePlugin;
import promauto.jroboplc.core.ConfigurationYaml;
import promauto.jroboplc.core.api.Configuration;
import promauto.jroboplc.core.api.EnvironmentInst;
import promauto.jroboplc.core.tags.TagRWInt;

public class ModbusModuleTest {
	private static String CFG_DIR = InitUtils.getResourcesDir("peripherial", PeripherialModule.class);

	ModbusModule m;
	SampleProtocolModbus prot;

	ModbusTag mtag;
	ModbusTag mtag2;
	ModbusTag mtag3;
	ModbusTag mtag4;
	ModbusTag mtag5;
	ModbusTag mtag6;

	@Before
	public void setUp() throws Exception {
		InitUtils.setupLogger();

		SampleEnvironment env = new SampleEnvironment();
		EnvironmentInst.set(env);
		
		Configuration conf = new ConfigurationYaml( CFG_DIR );
		if( !conf.load() ) 
			fail();
		env.setConfiguration(conf);

		SamplePlugin plug = new SamplePlugin();

		m = new ModbusModule(plug, "modbus");
		m.load(new HashMap<String,String>());
		m.retrial = 3;
		m.netaddr = 1;
		
		prot = new SampleProtocolModbus(m); 
		m.protocol = prot; 
		
	}



	private ModbusTag makeModbusTag(int addr, ModbusTag.Type type, ModbusTag.Region region,
									ModbusTag.Access access, boolean writeSingle, boolean writeMultiple) {
		ModbusTag mtag;
		mtag = new ModbusTag();
		mtag.address = addr;
		mtag.type = type;
		mtag.region = region;
		mtag.access = access;
		mtag.writeSingle = writeSingle;
		mtag.writeMultiple = writeMultiple;
		mtag.enable = true;
		mtag.tag = new TagRWInt("", 0, 0);
		mtag.init();
		
		m.modbustags.add(mtag);
		return mtag;
	}

	
	
	@Test
	public void test_write_hldreg_int16() {
		
		mtag = makeModbusTag(11, INT16, HLDREG, WO, true, true);
		mtag2 = makeModbusTag(12, INT16, HLDREG, WO, true, true);
		mtag3 = makeModbusTag(13, INT16, HLDREG, WO, true, true);
		mtag4 = makeModbusTag(14, INT16, HLDREG, WO, true, true);
		mtag5 = makeModbusTag(15, INT16, HLDREG, WO, true, true);
		
		// send two
		putEmptyInputAnswer(1);
		mtag.tag.setInt(111);
		mtag2.tag.setInt(222);
		m.execute();
		assertFalse( m.tagError.getBool());
		assertOut(16, 11, 111, 222);
		assertOutEmpty();
		m.execute();
		assertOutEmpty();
		
		// send another two
		putEmptyInputAnswer(1);
		mtag4.tag.setInt(444);
		mtag5.tag.setInt(555);
		m.execute();
		assertFalse( m.tagError.getBool());
		assertOut(16, 14, 444, 555);
		assertOutEmpty();
		m.execute();
		assertOutEmpty();
		
		// send four
		putEmptyInputAnswer(1);
		mtag.tag.setInt(111);
		mtag2.tag.setInt(222);
		mtag3.tag.setInt(333);
		mtag4.tag.setInt(444);
		m.execute();
		assertFalse( m.tagError.getBool());
		assertOut(16, 11, 111, 222, 333, 444);
		assertOutEmpty();
		m.execute();
		assertOutEmpty();

		// send four with break 
		putEmptyInputAnswer(2);
		mtag.tag.setInt(111);
		mtag2.tag.setInt(222);
		mtag4.tag.setInt(444);
		mtag5.tag.setInt(555);
		m.execute();
		assertFalse( m.tagError.getBool());
		assertOut(16, 11, 111, 222);
		assertOut(16, 14, 444, 555);
		assertOutEmpty();
		m.execute();
		assertOutEmpty();

		// send two multiple and one single 
		putEmptyInputAnswer(2);
		mtag2.tag.setInt(222);
		mtag4.tag.setInt(444);
		mtag5.tag.setInt(555);
		m.execute();
		assertFalse( m.tagError.getBool());
		assertOut(16, 14, 444, 555);
		assertOut(6, 12, 222);
		assertOutEmpty();
		m.execute();
		assertOutEmpty();

		// send one multiple and two multiple 
		putEmptyInputAnswer(2);
		mtag2.writeSingle = false;
		mtag2.tag.setInt(222);
		mtag4.tag.setInt(444);
		mtag5.tag.setInt(555);
		m.execute();
		assertFalse( m.tagError.getBool());
		assertOut(16, 12, 222);
		assertOut(16, 14, 444, 555);
		assertOutEmpty();
		m.execute();
		assertOutEmpty();

		// send two multiple, ignoring one 
		putEmptyInputAnswer(1);
		mtag2.writeSingle = false;
		mtag2.writeMultiple = false;
		mtag2.tag.setInt(222);
		mtag4.tag.setInt(444);
		mtag5.tag.setInt(555);
		m.execute();
		assertFalse( m.tagError.getBool());
		assertOut(16, 14, 444, 555);
		assertOutEmpty();
		m.execute();
		assertOutEmpty();

		// send negative value 
		putEmptyInputAnswer(1);
		mtag.tag.setInt(-1);
		m.execute();
		assertFalse( m.tagError.getBool());
		assertOut(6, 11, 0xffff);
		assertOutEmpty();
		m.execute();
		assertOutEmpty();

		// send two negative values 
		putEmptyInputAnswer(1);
		mtag3.tag.setInt(-1);
		mtag4.tag.setInt(-2);
		m.execute();
		assertFalse( m.tagError.getBool());
		assertOut(16, 13, 0xffff, 0xfffe);
		assertOutEmpty();
		m.execute();
		assertOutEmpty();

	}


	
	@Test
	public void test_maxWriteSizeReg() {
		
		m.maxWriteSizeReg = 2;
		mtag = makeModbusTag(11, INT16, HLDREG, WO, true, true);
		mtag2 = makeModbusTag(12, INT16, HLDREG, WO, true, true);
		mtag3 = makeModbusTag(13, INT16, HLDREG, WO, true, true);
		mtag4 = makeModbusTag(14, INT16, HLDREG, WO, true, true);
		mtag5 = makeModbusTag(15, INT16, HLDREG, WO, true, true);
		
		putEmptyInputAnswer(3);
		mtag.tag.setInt(111);
		mtag2.tag.setInt(222);
		mtag3.tag.setInt(333);
		mtag4.tag.setInt(444);
		mtag5.tag.setInt(555);
		m.execute();
		assertFalse( m.tagError.getBool());
		assertOut(16, 11, 111, 222);
		assertOut(16, 13, 333, 444);
		assertOut(6, 15, 555);
		assertOutEmpty();
		m.execute();
		assertOutEmpty();
		
		
		m.maxWriteSizeReg = 0;
		mtag.tag.setInt(111);
		mtag2.tag.setInt(222);
		mtag3.tag.setInt(333);
		mtag4.tag.setInt(444);
		mtag5.tag.setInt(555);
		putEmptyInputAnswer(5);
		m.execute();
		assertFalse( m.tagError.getBool());
		assertOut(6, 11, 111);
		assertOut(6, 12, 222);
		assertOut(6, 13, 333);
		assertOut(6, 14, 444);
		assertOut(6, 15, 555);
		assertOutEmpty();
		m.execute();
		assertOutEmpty();

	}
	

	
	@Test
	public void test_write_hldreg_uint16() {
		
		mtag = makeModbusTag(11, UINT16, HLDREG, WO, true, true);
		mtag2 = makeModbusTag(12, UINT16, HLDREG, WO, true, true);
		
		// send two
		putEmptyInputAnswer(1);
		mtag.tag.setInt(111);
		mtag2.tag.setInt(0xffff);
		m.execute();
		assertFalse( m.tagError.getBool());
		assertOut(16, 11, 111, 0xffff);
		assertOutEmpty();
		m.execute();
		assertOutEmpty();
		
		// send negative
		putEmptyInputAnswer(1);
		mtag.tag.setInt(0xffff);
		mtag2.tag.setInt(-1);
		m.execute();
		assertOut(16, 11, 0xffff, 0xffff);
		assertOutEmpty();
		m.execute();
		assertOutEmpty();
		
	}
	
	@Test
	public void test_write_hldreg_int32() {
		
		mtag = makeModbusTag(11, INT32, HLDREG, WO, true, true);
		mtag2 = makeModbusTag(13, INT32, HLDREG, WO, true, true);
		
		// send two
		putEmptyInputAnswer(1);
		mtag.tag.setInt(0x1_0005);
		mtag2.tag.setInt(0x8_ffff);
		m.execute();
		assertFalse( m.tagError.getBool());
		assertOut(16, 11, 1,5, 8,0xffff);
		assertOutEmpty();
		m.execute();
		assertOutEmpty();

		// send one
		putEmptyInputAnswer(1);
		mtag.tag.setInt(0x1_0005);
		m.execute();
		assertFalse( m.tagError.getBool());
		assertOut(16, 11, 1,5);
		assertOutEmpty();
		m.execute();
		assertOutEmpty();
		
		// send one negative
		putEmptyInputAnswer(1);
		mtag.tag.setInt(-1);
		m.execute();
		assertFalse( m.tagError.getBool());
		assertOut(16, 11, 0xffff, 0xffff);
		assertOutEmpty();
		m.execute();
		assertOutEmpty();
		
		// send one by single
		putEmptyInputAnswer(2);
		mtag.tag.setInt(0x1_0005);
		mtag.writeMultiple = false;
		m.execute();
		assertFalse( m.tagError.getBool());
		assertOut(6, 11, 1);
		assertOut(6, 12, 5);
		assertOutEmpty();
		m.execute();
		assertOutEmpty();
		

	}
	

	@Test
	public void test_write_hldreg_error() {
		
		mtag = makeModbusTag(11, INT16, HLDREG, RW, true, true);
		mtag2 = makeModbusTag(12, INT16, HLDREG, RW, true, true);
		
		// send two
//		putEmptyInputAnswer();   - no answer
		mtag.tag.setInt(111);
		mtag2.tag.setInt(222);
		m.execute();
		assertTrue( m.tagError.getBool());
		assertOut(16, 11, 111, 222);
		assertOutEmpty();
		m.execute();
		assertOut(16, 11, 111, 222);
//		assertOut(6, 11, 111);
		assertOutEmpty();
		m.execute();
		assertOut(16, 11, 111, 222);
//		assertOut(6, 11, 111);
		assertOutEmpty();
		
	}

	
	
	@Test
	public void test_read_hldreg() {
		m.hasReadHldregs = true;
		
		mtag = makeModbusTag(11, INT16,  HLDREG, RO, true, true);
		mtag2 = makeModbusTag(12, INT16,  HLDREG, RO, true, true);
		mtag3 = makeModbusTag(13, UINT16, HLDREG, RO, true, true);
		mtag4 = makeModbusTag(14, INT32,  HLDREG, RO, true, true);
		mtag5 = makeModbusTag(16, BOOL,   HLDREG, RO, true, true);
		mtag6 = makeModbusTag(17, INT32,  HLDREG, RO, true, true);
		
		putInputAnswer(1,3,0,  0,1,  255,255,  255,255,  127,255,255,255,  0,1,  255,255,255,255);
		
		mtag.tag.setInt(111);
		mtag2.tag.setInt(222);
		m.execute();
		assertFalse( m.tagError.getBool());
		assertOut(3, 11, 8);
		assertOutEmpty();
		assertEquals(1, mtag.tag.getInt());
		assertEquals(-1, mtag2.tag.getInt());
		assertEquals(0xffff, mtag3.tag.getInt());
		assertEquals(0x7fff_ffff, mtag4.tag.getInt());
		assertEquals(true, mtag5.tag.getBool());
		assertEquals(-1, mtag6.tag.getInt());

		
		mtag4.readEnd = true;
		putInputAnswer(1,3,0,  0,1,  255,255,  255,255,  0,1,0,0 );
		putInputAnswer(1,3,0,  0,0,  255,255,255,255);
		m.execute();
		assertFalse( m.tagError.getBool());
		assertOut(3, 11, 5);
		assertOut(3, 16, 3);
		assertOutEmpty();
		assertEquals(1, mtag.tag.getInt());
		assertEquals(-1, mtag2.tag.getInt());
		assertEquals(0xffff, mtag3.tag.getInt());
		assertEquals(0x10000, mtag4.tag.getInt());
		assertEquals(false, mtag5.tag.getBool());
		assertEquals(-1, mtag6.tag.getInt());
		
	}

	
	
	@Test
	public void test_read_inpreg() {
		m.hasReadInpregs = true;
		
		mtag = makeModbusTag(11, INT16,  INPREG, RW, true, true);
		mtag2 = makeModbusTag(12, INT16,  INPREG, RW, true, true);
		mtag3 = makeModbusTag(13, UINT16, INPREG, RW, true, true);
		mtag4 = makeModbusTag(14, INT32,  INPREG, RW, true, true);
		mtag5 = makeModbusTag(16, BOOL,   INPREG, RW, true, true);
		mtag6 = makeModbusTag(17, INT32,  INPREG, RW, true, true);
		
		putInputAnswer(1,4,0,  0,1,  255,255,  255,255,  0x7f,0xff,0xfe,0xfd,  0,1,  255,255,255,255);
		
		mtag.tag.setInt(111);
		mtag2.tag.setInt(222);
		m.execute();
		assertFalse( m.tagError.getBool());
		assertOut(4, 11, 8);
		assertOutEmpty();
		assertEquals(1, mtag.tag.getInt());
		assertEquals(-1, mtag2.tag.getInt());
		assertEquals(0xffff, mtag3.tag.getInt());
		assertEquals(0x7fff_fefd, mtag4.tag.getInt());
		assertEquals(true, mtag5.tag.getBool());
		assertEquals(-1, mtag6.tag.getInt());

	}

	
	@Test
	public void test_write_mult_coils() {
		m.maxWriteSizeCoil = 3;
		
		mtag = makeModbusTag(11, INT16,  COIL, WO, true, true);
		mtag2 = makeModbusTag(12, UINT16, COIL, WO, true, true);
		mtag3 = makeModbusTag(13, INT32,  COIL, WO, true, true);
		mtag4 = makeModbusTag(14, BOOL,   COIL, WO, true, true);
		mtag5 = makeModbusTag(15, BOOL,   COIL, WO, true, true);
		mtag6 = makeModbusTag(17, BOOL,   COIL, WO, false, true);
		
		putEmptyInputAnswer(3);
		mtag.tag.setInt(111);
		mtag2.tag.setInt(222);
		mtag3.tag.setInt(333);
		mtag4.tag.setInt(444);
		mtag5.tag.setInt(555);
		mtag6.tag.setBool(false);
		m.execute();
		assertFalse( m.tagError.getBool());
		assertOut(15, 11, 1, 1, 1);
		assertOut(15, 14, 1, 1);
		assertOut(15, 17, 0);
		assertOutEmpty();
		m.execute();
		assertOutEmpty();
	}

	
	@Test
	public void test_write_single_coils() {
		
		m.maxWriteSizeCoil = 1;
		mtag = makeModbusTag(11, BOOL, COIL, WO, true, true);
		mtag2 = makeModbusTag(12, BOOL, COIL, WO, true, true);
		mtag3 = makeModbusTag(13, BOOL, COIL, WO, true, true);
		
		putEmptyInputAnswer(3);
		mtag.tag.setInt(111);
		mtag2.tag.setInt(0);
		mtag3.tag.setInt(333);
		m.execute();
		assertFalse( m.tagError.getBool());
		assertOut(5, 11, 1);
		assertOut(5, 12, 0);
		assertOut(5, 13, 1);
		assertOutEmpty();
		m.execute();
		assertOutEmpty();
		
	}
	
	
	@Test
	public void test_read_coil() {
		m.hasReadCoils = true;
		
		mtag = makeModbusTag(11, BOOL,   COIL, RO, true, true);
		mtag2 = makeModbusTag(12, INT16,  COIL, RO, true, true);
		mtag3 = makeModbusTag(13, UINT16, COIL, RO, true, true);
		mtag4 = makeModbusTag(14, INT32,  COIL, RO, true, true);
		mtag4.readEnd = true;
		mtag5 = makeModbusTag(15, BOOL,   COIL, RO, true, true);
		mtag6 = makeModbusTag(16, INT32,  COIL, RO, true, true);
		
		putInputAnswer(1,1,0,  0x0F);
		putInputAnswer(1,1,0,  0x03);
		
		mtag.tag.setInt(111);
		m.execute();
		assertFalse( m.tagError.getBool());
		assertOut(1, 11, 4);
		assertOut(1, 15, 2);
		assertOutEmpty();
		assertEquals(1, mtag.tag.getInt());
		assertEquals(1, mtag2.tag.getInt());
		assertEquals(1, mtag3.tag.getInt());
		assertEquals(1, mtag4.tag.getInt());
		assertEquals(1, mtag5.tag.getInt());
		assertEquals(1, mtag6.tag.getInt());

		mtag4.readEnd = false;
		putInputAnswer(1,1,0,  0x15);
		
		mtag.tag.setInt(111);
		m.execute();
		assertFalse( m.tagError.getBool());
		assertOut(1, 11, 6);
		assertOutEmpty();
		assertEquals(1, mtag.tag.getInt());
		assertEquals(0, mtag2.tag.getInt());
		assertEquals(1, mtag3.tag.getInt());
		assertEquals(0, mtag4.tag.getInt());
		assertEquals(1, mtag5.tag.getInt());
		assertEquals(0, mtag6.tag.getInt());

	}


	@Test
	public void test_read_dscinp() {
		m.hasReadCoils = true;
		
		mtag = makeModbusTag(11, BOOL,   COIL, RO, true, true);
		mtag2 = makeModbusTag(12, INT16,  COIL, RO, true, true);
		mtag3 = makeModbusTag(13, UINT16, COIL, RO, true, true);
		mtag4 = makeModbusTag(15, INT32,  COIL, RO, true, true);
		mtag5 = makeModbusTag(16, BOOL,   COIL, RO, true, true);
		mtag6 = makeModbusTag(17, INT32,  COIL, RO, true, true);
		
		putInputAnswer(1,1,0,  0b0010_0101);
		
		mtag.tag.setInt(111);
		m.execute();
		assertFalse( m.tagError.getBool());
		assertOut(1, 11, 7);
		assertOutEmpty();
		assertEquals(1, mtag.tag.getInt());
		assertEquals(0, mtag2.tag.getInt());
		assertEquals(1, mtag3.tag.getInt());
		assertEquals(0, mtag4.tag.getInt());
		assertEquals(1, mtag5.tag.getInt());
		assertEquals(0, mtag6.tag.getInt());

	}


	@Test
	public void test_read_float() {
		m.hasReadInpregs = true;
		
		mtag = makeModbusTag(11, FLOAT16, INPREG, RO, true, true);
		mtag2 = makeModbusTag(12, FLOAT32, INPREG, RO, true, true);
		
		putInputAnswer(1,4,0,  0xc0,00,  0x3f,0x80,00,00);
		
		m.execute();
		assertFalse( m.tagError.getBool());
		assertOut(4, 11, 3);
		assertOutEmpty();
		assertEquals(-2.0, mtag.tag.getDouble(), 0.001);
		assertEquals(1.0, mtag2.tag.getDouble(), 0.001);

	}

	@Test
	public void test_write_float() {
		
		mtag = makeModbusTag(11, FLOAT16, HLDREG, WO, true, true);
		mtag2 = makeModbusTag(12, FLOAT32, HLDREG, WO, true, true);
		
		mtag.tag.setDouble(-2.0);
		mtag2.tag.setDouble(1.0);
		mtag2.littleEndian = true;
		
		putEmptyInputAnswer(1);
		
		m.execute();
		assertFalse( m.tagError.getBool());
		assertOut(16, 11, 0xc000, 0, 0x3f80);
		assertOutEmpty();

	}

	
	@Test
	public void test_read_string() {
		m.hasReadInpregs = true;
		
		mtag = makeModbusTag(11, STRING, INPREG, RO, true, true);
		mtag.size = 3;
		
		putInputAnswer(1,4,0,  0x30,0x31,0x32,0x33,0x34,0x35);
		
		m.execute();
		assertFalse( m.tagError.getBool());
		assertOut(4, 11, 3);
		assertOutEmpty();
		assertEquals("012345", mtag.tag.getString());

	}
	
	
	@Test
	public void test_enable() {
		
		mtag = makeModbusTag(11, FLOAT16, HLDREG, RW, true, true);
		mtag2 = makeModbusTag(12, FLOAT32, HLDREG, RW, true, true);
		
		mtag.tag.setDouble(-2.0);
		mtag2.tag.setDouble(1.0);
		mtag.enable = false;
		mtag2.enable = false;
		
		m.execute();
		assertFalse( m.tagError.getBool());
		assertOutEmpty();

	}



	@Test
	public void test_read_bittags() {
		m.hasReadInpregs = true;

		mtag = makeModbusTag(11, INT16,  INPREG, RW, true, true);

		ModbusBitTag btag1 = new ModbusBitTag("", 1, new TagRWInt("", 0, 0));
		ModbusBitTag btag2 = new ModbusBitTag("", 2, new TagRWInt("", 0, 0));
		ModbusBitTag btag3 = new ModbusBitTag("", 8, new TagRWInt("", 0, 0));
		btag1.reftag = mtag.tag;
		btag2.reftag = mtag.tag;
		btag3.reftag = mtag.tag;
		m.bittags.add(btag1);
		m.bittags.add(btag2);
		m.bittags.add(btag3);

		putInputAnswer(1,4,0,  0,3);

		mtag.tag.setInt(0);
		m.execute();
		assertFalse( m.tagError.getBool());
		assertOut(4, 11, 1);
		assertOutEmpty();
		assertEquals(3, mtag.tag.getInt());
		assertEquals(1, btag1.tag.getInt());
		assertEquals(1, btag2.tag.getInt());
		assertEquals(0, btag3.tag.getInt());
	}

	@Test
	public void test_write_bittags() {
		mtag = makeModbusTag(11, INT16, HLDREG, WO, true, true);
		ModbusBitTag btag1 = new ModbusBitTag("", 1, new TagRWInt("", 0, 0));
		ModbusBitTag btag2 = new ModbusBitTag("", 2, new TagRWInt("", 0, 0));
		ModbusBitTag btag3 = new ModbusBitTag("", 8, new TagRWInt("", 0, 0));
		btag1.reftag = mtag.tag;
		btag2.reftag = mtag.tag;
		btag3.reftag = mtag.tag;
		m.bittags.add(btag1);
		m.bittags.add(btag2);
		m.bittags.add(btag3);

		mtag.tag.setInt(0);
		btag1.tag.setInt(1);
		btag2.tag.setInt(1);
		btag3.tag.setInt(1);

		// send two
		putEmptyInputAnswer(1);
		m.execute();
		assertFalse( m.tagError.getBool());
		assertOut(6, 11, 0b1011);
		assertOutEmpty();
		m.execute();
		assertOutEmpty();
	}


	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	private void putEmptyInputAnswer(int amount) {
		for(int i=0; i<amount; ++i)
			prot.in.push( new int[0] );
	}

	private void putInputAnswer(int...answer) {
		prot.in.push( Arrays.copyOf(answer, answer.length) );
	}
	
	private void assertOut(int...expected) {
		assertArrayEquals(expected, prot.out.pollLast());
	}
	
	private void assertOutEmpty() {
		assertNull(prot.out.pollLast());
	}
	

}

