package promauto.jroboplc.plugin.peripherial;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.HashMap;

import org.junit.Before;
import org.junit.Test;

import promauto.jroboplc.core.api.InitUtils;
import promauto.jroboplc.core.api.SampleEnvironment;
import promauto.jroboplc.core.api.SamplePlugin;
import promauto.jroboplc.core.api.SampleSerialManager;
import promauto.jroboplc.core.api.SampleSerialPort;
import promauto.jroboplc.core.ConfigurationYaml;
import promauto.jroboplc.core.api.Configuration;
import promauto.jroboplc.core.api.EnvironmentInst;

public class ProtocolAA55Test {
	private static String CFG_DIR = InitUtils.getResourcesDir("peripherial", PeripherialModule.class);

	ProtocolAA55 prot;
	PeripherialModule mod;
	SampleSerialPort ser;
	SampleSerialManager sm;

	@Before
	public void setUp() throws Exception {
		InitUtils.setupLogger();
		
		SampleEnvironment env = new SampleEnvironment();
		EnvironmentInst.set(env);
		
		Configuration conf = new ConfigurationYaml( CFG_DIR );
		if( !conf.load() ) 
			fail();
		env.setConfiguration(conf);

		sm = new SampleSerialManager();
    	ser = sm.port;
		env.setSerialManager(sm);

		SamplePlugin plug = new SamplePlugin();
		mod = new PeripherialModule(plug, "");
		mod.load(new HashMap<String,String>());
		mod.netaddr = 1;
		mod.port = ser;
		
		prot = new ProtocolAA55(mod);

	}

	
	
	@Test
	public void testWriteBytesAA55() throws Exception {
		int[] setout = new int[]{0x55, 0x21, 0xAA, 0x55, 0x00};
		prot.writeBytes(setout, 5);

		int[] actout = ser.getBuffWrite();
		int[] expout = new int[]{0x55, 0x21, 0xAA, 0x01, 0xAA, 0x00, 0x00};
		
		assertArrayEquals(expout, actout);
	}

	
	
	@Test
	public void testReadBytesAA55() throws Exception {
		int[] setin = new int[]{0x55, 0x21, 0xAA, 0x01, 0xAA, 0x00, 0x00};
		ser.setBuffRead( setin );
		
		int[] actin = new int[5]; 
		
		assertTrue( prot.readBytes(actin, 5) );
		
		int[] expin = new int[]{0x55, 0x21, 0xAA, 0x55, 0x00};

		assertArrayEquals(expin, actin);
	}

	
	
	@Test
	public void testCheckCrcAA55() {
		int[] data = new int[]{0xFF,0x00, 0x00,0xFF, 0x55,0xAA, 0x11,0xEE, 0xFD};
		assertTrue( prot.checkCrcAA55(data, 9) );
	}

	
	
	@Test
	public void testSetCrcAA55() {
		int[] data = new int[]{0x55, 0x21, 0xFF,0x00, 0x00,0xFF, 0x55,0xAA, 0x11,0xEE, 0};
		prot.setCrcAA55(data, 11);
		assertEquals( 0xFD, data[data.length-1] );
	}

}
