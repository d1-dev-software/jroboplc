package promauto.jroboplc.plugin.peripherial;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.fail;

import java.lang.reflect.Field;
import java.util.HashMap;

import org.junit.Before;
import org.junit.Test;

import promauto.jroboplc.core.api.InitUtils;
import promauto.jroboplc.core.api.SampleEnvironment;
import promauto.jroboplc.core.api.SamplePlugin;
import promauto.jroboplc.core.ConfigurationYaml;
import promauto.jroboplc.core.api.Configuration;
import promauto.jroboplc.core.api.EnvironmentInst;

public class PdioOutModuleTest {
	private static String CFG_DIR = InitUtils.getResourcesDir("peripherial", PeripherialModule.class);

	PaPdioModule m;
	SampleProtocolAA55 prot;

	@Before
	public void setUp() throws Exception {
		InitUtils.setupLogger();

		SampleEnvironment env = new SampleEnvironment();
		EnvironmentInst.set(env);
		
		Configuration cm = new ConfigurationYaml( CFG_DIR );
		if( !cm.load() ) 
			fail();
		env.setConfiguration(cm);

		SamplePlugin plug = new SamplePlugin();

		m = new PaPdioModule(plug, "di");
		HashMap<String,String> conf = new HashMap<String,String>();
		conf.put("mode", "writeonly");
		m.load(conf);
		m.retrial = 3;
		m.netaddr = 1;

		prot = new SampleProtocolAA55(m);
		m.protocol = prot; 
	}

	@Test
	public void testExecuteGood() {
		int[] setout = new int[]{0xFF,0x00, 0x00,0xFF, 0x55,0xAA, 0x11,0xEE};
		int[] expout = new int[]{0x55, 0x21, 0xFF,0x00, 0x00,0xFF, 0x55,0xAA, 0x11,0xEE, 0xFD};
		
		int mask = 1;
		for (int i = 0; i < 64; i++) {
			m.outputs[i].setBool((setout[i / 8] & mask) > 0);
			if ((mask = mask << 1) > 0x80) mask = 1;
		}
		
		m.execute();
		
		int[] actout = prot.buffout.pollLast();
		
		assertArrayEquals("Check data to send", expout, actout);
	}
	
	@Test
	public void testEnable_0() throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		Field f = m.getClass().getSuperclass().getSuperclass().getDeclaredField("enable");
		f.setAccessible(true);
		f.setBoolean(m, Boolean.FALSE);
		
		m.execute();
		
		int[] actout = prot.buffout.pollLast();
		
		assertArrayEquals(null, actout);
	}
	

	
}


