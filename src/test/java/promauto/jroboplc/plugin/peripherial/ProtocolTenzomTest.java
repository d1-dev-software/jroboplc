package promauto.jroboplc.plugin.peripherial;

import org.junit.Before;
import org.junit.Test;
import promauto.jroboplc.core.ConfigurationYaml;
import promauto.jroboplc.core.api.*;

import java.util.HashMap;

import static org.junit.Assert.*;

public class ProtocolTenzomTest {
	private static String CFG_DIR = InitUtils.getResourcesDir("peripherial", PeripherialModule.class);

	ProtocolTenzom prot;
	PeripherialModule mod;
	SampleSerialPort ser;
	SampleSerialManager sm;
	

	@Before
	public void setUp() throws Exception {
		InitUtils.setupLogger();
		
		SampleEnvironment env = new SampleEnvironment();
		EnvironmentInst.set(env);
		
		Configuration conf = new ConfigurationYaml( CFG_DIR );
		if( !conf.load() ) 
			fail();
		env.setConfiguration(conf);

		sm = new SampleSerialManager();
    	ser = sm.port;
		env.setSerialManager(sm);

		SamplePlugin plug = new SamplePlugin();
		mod = new PeripherialModule(plug, "");
		mod.load(new HashMap<String,String>());
		mod.netaddr = 1;
		mod.port = ser;
		mod.retrial = 1;
		
		prot = new ProtocolTenzom(mod);

	}

	
	
	@Test
	public void testCmdC8() throws Exception {
		int[] request = new int[]{0xff, 1, 0xc8, 0x86, 0x20, 0xff, 0xff};
		int[] answer = new int[]{
				0xFF, 0x01, 0xC8, 0x86,
				0x62, 0x00, 0x00, 0x00, 0x00,
				0x00, 0x20, 0x00, 0x00, 0x00,
				0x00, 0x00, 0x00, 0x00, 0x00,
				0x02, 0x00, 0x00, 0x00, 0x00,
				0x00, 0x20, 0x00, 0x00, 0x00,
				0x02, 0x00, 0x00, 0x00, 0x00,
				0x00, 0x00, 0x00, 0x00, 0x00,
				0x19, 0xFF, 0xFF
		};

		ser.setBuffRead(answer);
		assertTrue( prot.requestCounters(5) );
		assertArrayEquals(request, ser.getBuffWrite());
	}


	@Test
	public void testCmdC3() throws Exception {
		int[] request = new int[]{0xff, 1, 0xc3, 0xe3, 0xff, 0xff};
		int[] answer = new int[]{
				0xFF, 0x01, 0xC3, 0x00, 0x00, 0x00, 0x12, 0x89, 0xFF, 0xFF
		};

		ser.setBuffRead(answer);
		assertTrue( prot.requestValue(0xc3, 4) );
		assertArrayEquals(request, ser.getBuffWrite());
	}


	@Test
	public void testCmdC2() throws Exception {
		int[] request = new int[]{0xff, 1, 0xc2, 0x8a, 0xff, 0xff};
		int[] answer = new int[]{
				0xFF, 0x01, 0xC3, 0x00, 0x00, 0x00, 0x12, 0x89, 0xFF, 0xFF
		};

		ser.setBuffRead(answer);
		assertTrue( prot.requestValue(0xc2, 4) );
		assertArrayEquals(request, ser.getBuffWrite());
	}


}
