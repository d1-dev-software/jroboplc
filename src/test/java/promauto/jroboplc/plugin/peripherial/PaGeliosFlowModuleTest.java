package promauto.jroboplc.plugin.peripherial;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.fail;

import java.util.HashMap;

import org.junit.Before;
import org.junit.Test;

import promauto.jroboplc.core.api.InitUtils;
import promauto.jroboplc.core.api.SampleEnvironment;
import promauto.jroboplc.core.api.SamplePlugin;
import promauto.jroboplc.core.ConfigurationYaml;
import promauto.jroboplc.core.api.Configuration;
import promauto.jroboplc.core.api.EnvironmentInst;

public class PaGeliosFlowModuleTest {
	private static String CFG_DIR = InitUtils.getResourcesDir("peripherial", PeripherialModule.class);

	PaGeliosFlowModule m;
	SampleProtocolModbus prot;


	@Before
	public void setUp() throws Exception {
		InitUtils.setupLogger();

		SampleEnvironment env = new SampleEnvironment();
		EnvironmentInst.set(env);
		
		Configuration conf = new ConfigurationYaml( CFG_DIR );
		if( !conf.load() ) 
			fail();
		env.setConfiguration(conf);

		SamplePlugin plug = new SamplePlugin();

		m = new PaGeliosFlowModule(plug, "gelios");
		m.load(new HashMap<String,String>());
		m.retrial = 3;
		m.netaddr = 1;

		prot = new SampleProtocolModbus(m);
		m.protocol = prot; 
	}

	
	
	@Test
	public void testExecuteRead() {
		prot.in.push( new int[]{
				0,0,34,
				0,1, 		// State
				0,0,0,2,	// PrzCur
				0,0,0,3, 	// Neto
				0,209, 		// WeightMainH
				121,15, 	// WeightMainL
				0,0,0,6,	// PrzReq
				0,7, 		// ErrorCode
				0,8, 		// In
				0,9, 		// Out
				0,10, 		// ADCInit
				0,11, 		// ADCSetGr
				0,12, 		// ADCCtrl
				0,13, 		// ADCMain
				0,14, 		// ADCRequest
				0,0} );
		
		m.execute();
		
		assertFalse( m.tagError.getBool());
		assertEquals( 0, m.tagErrorCnt.getInt());
		
		assertArrayEquals(new int[]{3, 0, 17}, prot.out.pollLast());
		
		assertEquals( 247, m.tagCrc.getInt() );
		assertEquals( 1, m.tagState.getInt() );
		assertEquals( 2, m.tagCurOutput.getInt() );
		assertEquals( 3, m.tagWeight.getInt() );
		assertEquals( 209, m.tagSumWeightHigh.getInt() );
		assertEquals( 30991, m.tagSumWeightLow.getInt() );
		assertEquals( 6, m.tagReqOutput.getInt() );
		assertEquals( 7, m.tagErrorCode.getInt() );
//		assertEquals( 8, m.tagIn.getInt() );
//		assertEquals( 9, m.tagOut.getInt() );
//		assertEquals( 10, m.tagADCInit.getInt() );
//		assertEquals( 11, m.tagADCSetGr.getInt() );
//		assertEquals( 12, m.tagADCCtrl.getInt() );
//		assertEquals( 13, m.tagADCMain.getInt() );
//		assertEquals( 14, m.tagADCRequest.getInt() );
	}
	
	
	@Test
	public void testExecutePrzReq() {
		prot.in.push( new int[]{
				0,0,0,0,0,0,0,0,2,0,0,0,3,0,4,0,5,	0,0,0,10000,  0,7,0,8,0,9,0,10,0,11,0,12,0,13,0,14,0,0} );
		m.execute();
		assertArrayEquals(new int[]{3, 0, 17}, prot.out.pollLast());
		assertEquals( 10000, m.tagReqOutput.getInt() );
		
		
		m.tagReqOutput.setInt(20000);

		prot.in.push( new int[]{0,0,0,0,0,0,0,0} );
		prot.in.push( new int[]{0,0,0,0,0,0,0,0} );
		prot.in.push( new int[]{
				0,0,0,0,0,0,0,0,2,0,0,0,3,0,4,0,5,	0,0,0,10000,  0,7,0,8,0,9,0,10,0,11,0,12,0,13,0,14,0,0} );
		m.execute();
		assertArrayEquals(new int[]{6, 7, 0}, prot.out.pollLast());
		assertArrayEquals(new int[]{6, 8, 20000}, prot.out.pollLast());
		assertArrayEquals(new int[]{3, 0, 17}, prot.out.pollLast());
		assertEquals( 10000, m.tagReqOutput.getInt() );

		
		prot.in.push( new int[]{
				0,0,0,0,0,0,0,0,2,0,0,0,3,0,4,0,5,	0,0,0,10000,  0,7,0,8,0,9,0,10,0,11,0,12,0,13,0,14,0,0} );
		m.execute();
		assertArrayEquals(new int[]{3, 0, 17}, prot.out.pollLast());
		assertEquals( 10000, m.tagReqOutput.getInt() );
	}
	
	@Test
	public void testExecuteSetState() {
		prot.in.push( new int[]{
				0,0,0,  0,0,  0,0,0,2,0,0,0,3,0,4,0,5,0,0,0,6,0,7,0,8,0,9,0,10,0,11,0,12,0,13,0,14,0,0} );
		m.execute();
		assertArrayEquals(new int[]{3, 0, 17}, prot.out.pollLast());
		assertEquals( 0, m.tagState.getInt() );
		
		
		m.tagCmd.setInt(1);

		prot.in.push( new int[]{0,0,0,0,0,0,0,0} );
		prot.in.push( new int[]{
				0,0,0,  0,0,  0,0,0,2,0,0,0,3,0,4,0,5,0,0,0,6,0,7,0,8,0,9,0,10,0,11,0,12,0,13,0,14,0,0} );
		m.execute();
		assertArrayEquals(new int[]{6, 0, 1}, prot.out.pollLast());
		assertArrayEquals(new int[]{3, 0, 17}, prot.out.pollLast());
		assertEquals( 0, m.tagState.getInt() );

		
		prot.in.push( new int[]{0,0,0,0,0,0,0,0} );
		prot.in.push( new int[]{
				0,0,0,  0,1,  0,0,0,2,0,0,0,3,0,4,0,5,0,0,0,6,0,7,0,8,0,9,0,10,0,11,0,12,0,13,0,14,0,0} );
		m.execute();
		assertArrayEquals(new int[]{6, 0, 1}, prot.out.pollLast());
		assertArrayEquals(new int[]{3, 0, 17}, prot.out.pollLast());
		assertEquals( 1, m.tagState.getInt() );
		
		
		prot.in.push( new int[]{
				0,0,0,  0,1,  0,0,0,2,0,0,0,3,0,4,0,5,0,0,0,6,0,7,0,8,0,9,0,10,0,11,0,12,0,13,0,14,0,0} );
		m.execute();
		assertArrayEquals(new int[]{3, 0, 17}, prot.out.pollLast());
		assertEquals( 1, m.tagState.getInt() );

		
		prot.in.push( new int[]{
				0,0,0,  0,0,  0,0,0,2,0,0,0,3,0,4,0,5,0,0,0,6,0,7,0,8,0,9,0,10,0,11,0,12,0,13,0,14,0,0} );
		m.execute();
		assertArrayEquals(new int[]{3, 0, 17}, prot.out.pollLast());
		assertEquals( 0, m.tagState.getInt() );

		
		prot.in.push( new int[]{0,0,0,0,0,0,0,0} );
		prot.in.push( new int[]{
				0,0,0,  0,1,  0,0,0,2,0,0,0,3,0,4,0,5,0,0,0,6,0,7,0,8,0,9,0,10,0,11,0,12,0,13,0,14,0,0} );
		m.execute();
		assertArrayEquals(new int[]{6, 0, 1}, prot.out.pollLast());
		assertArrayEquals(new int[]{3, 0, 17}, prot.out.pollLast());
		assertEquals( 1, m.tagState.getInt() );

		
		prot.in.push( new int[]{
				0,0,0,  0,1,  0,0,0,2,0,0,0,3,0,4,0,5,0,0,0,6,0,7,0,8,0,9,0,10,0,11,0,12,0,13,0,14,0,0} );
		m.execute();
		assertArrayEquals(new int[]{3, 0, 17}, prot.out.pollLast());
		assertEquals( 1, m.tagState.getInt() );


		prot.in.push( new int[]{
				0,0,0,  0,2,  0,0,0,2,0,0,0,3,0,4,0,5,0,0,0,6,0,7,0,8,0,9,0,10,0,11,0,12,0,13,0,14,0,0} );
		m.execute();
		assertArrayEquals(new int[]{3, 0, 17}, prot.out.pollLast());
		assertEquals( 2, m.tagState.getInt() );


		prot.in.push( new int[]{
				0,0,0,  0,2,  0,0,0,2,0,0,0,3,0,4,0,5,0,0,0,6,0,7,0,8,0,9,0,10,0,11,0,12,0,13,0,14,0,0} );
		m.execute();
		assertArrayEquals(new int[]{3, 0, 17}, prot.out.pollLast());
		assertEquals( 2, m.tagState.getInt() );

		
		m.tagCmd.setInt(0);
		prot.in.push( new int[]{
				0,0,0,  0,2,  0,0,0,2,0,0,0,3,0,4,0,5,0,0,0,6,0,7,0,8,0,9,0,10,0,11,0,12,0,13,0,14,0,0} );
		m.execute();
		assertArrayEquals(new int[]{3, 0, 17}, prot.out.pollLast());
		assertEquals( 2, m.tagState.getInt() );

		
		m.tagCmd.setInt(1);
		prot.in.push( new int[]{
				0,0,0,  0,2,  0,0,0,2,0,0,0,3,0,4,0,5,0,0,0,6,0,7,0,8,0,9,0,10,0,11,0,12,0,13,0,14,0,0} );
		m.execute();
		assertArrayEquals(new int[]{3, 0, 17}, prot.out.pollLast());
		assertEquals( 2, m.tagState.getInt() );


		m.tagReset.setInt(1);
		prot.in.push( new int[]{0,0,0,0,0,0,0,0} );
		prot.in.push( new int[]{
				0,0,0,  0,1,  0,0,0,2,0,0,0,3,0,4,0,5,0,0,0,6,0,7,0,8,0,9,0,10,0,11,0,12,0,13,0,14,0,0} );
		m.execute();
		assertArrayEquals(new int[]{6, 0, 1}, prot.out.pollLast());
		assertArrayEquals(new int[]{3, 0, 17}, prot.out.pollLast());
		assertEquals( 0, m.tagReset.getInt() );
		assertEquals( 1, m.tagState.getInt() );

	}
	


}

