package promauto.jroboplc.plugin.peripherial;

import java.util.LinkedList;

public class SampleProtocolModbus extends ProtocolModbus {
	public LinkedList<int[]> in = new LinkedList<>();
	public LinkedList<int[]> out = new LinkedList<>();
	
	public SampleProtocolModbus(PeripherialModule module) { //, int[] buffout, int[] buffin) {
		super(module); //, buffout, buffin);
	}

	private boolean updateBuffin() {
		int[] buff = this.in.pollLast();
		if (buff==null)
			return false;
		
		buffin = new int[buff.length];
		for(int i=0; i<buff.length; i++)
			buffin[i] = buff[i];
		return true;
	}

	@Override
    public boolean requestCmd1(int addr, int size) throws Exception {
    	int[] buffout = new int[]{1, addr, size};
		this.out.push( buffout );		
		return updateBuffin();
    }

	@Override
    public boolean requestCmd2(int addr, int size) throws Exception {
    	int[] buffout = new int[]{2, addr, size};
		this.out.push( buffout );		
		return updateBuffin();
    }

	@Override
    public boolean requestCmd3(int addr, int size) throws Exception {
    	int[] buffout = new int[]{3, addr, size};
		this.out.push( buffout );		
		return updateBuffin();
    }

	@Override
    public boolean requestCmd4(int addr, int size) throws Exception {
    	int[] buffout = new int[]{4, addr, size};
		this.out.push( buffout );		
		return updateBuffin();
    }

    @Override
    public boolean requestCmd5(int addr, int value) throws Exception {
    	int[] buffout = new int[]{5, addr, value};
		this.out.push( buffout );		
		return updateBuffin();
    }

    @Override
    public boolean requestCmd6(int addr, int value) throws Exception {
    	int[] buffout = new int[]{6, addr, value};
		this.out.push( buffout );		
		return updateBuffin();
    }

    @Override
    public boolean requestCmd0F(int addr, int size, int... values) throws Exception {
		size = Math.min(values.length, size);
    	int[] buffout = new int[size + 2];
    	buffout[0] = 15;
    	buffout[1] = addr;
    	System.arraycopy( values, 0, buffout, 2, size);
		this.out.push( buffout );		
		return updateBuffin();
    }

    @Override
    public boolean requestCmd10(int addr, int size, int... values) throws Exception {
		size = Math.min(values.length, size);
    	int[] buffout = new int[size + 2];
    	buffout[0] = 16;
    	buffout[1] = addr;
    	System.arraycopy( values, 0, buffout, 2, size);
		this.out.push( buffout );		
		return updateBuffin();
    }

/*
    @Override
    public boolean requestModbus(int cmd, int addr, int size, int[] buffout, int[] buffin) throws Exception {
    	buffout = new int[]{cmd, addr, size};
		this.buffout.push( buffout );		

    	
		int[] buff = this.buffin.pollLast();
		if (buff==null)
			return false;
		
		int size1 = buffin.length;
		int size2 = buff.length;
		for(int i=0; (i<size1) && (i<size2); i++)
			buffin[i] = buff[i];
		return true;
    }
*/
}
