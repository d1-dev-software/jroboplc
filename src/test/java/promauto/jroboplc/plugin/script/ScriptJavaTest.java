package promauto.jroboplc.plugin.script;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import promauto.jroboplc.core.State;
import promauto.jroboplc.core.api.InitUtils;
import promauto.jroboplc.core.api.SampleModuleManager;
import promauto.jroboplc.core.RealEnvironment;
import promauto.jroboplc.core.api.EnvironmentInst;
import promauto.jroboplc.core.api.Tag;

public class ScriptJavaTest {
	private static String CFGDIR = InitUtils.getResourcesDir("script", ScriptJavaTest.class);

	@Before
	public void setUp() {
		InitUtils.setupLogger();

	}

	
	private ScriptModule load(String modname) throws Exception {
		assertTrue( RealEnvironment.create(CFGDIR) );
		SampleModuleManager mm = new SampleModuleManager();
		EnvironmentInst.get().setModuleManager(mm);
		
		Object conf = EnvironmentInst.get().getConfiguration().getModuleConf("script", modname);
		
		ScriptPlugin plg = new ScriptPlugin();
		plg.initialize();
		ScriptModule m = (ScriptModule)plg.createModule(modname, conf);
		mm.modules.put(m.getName(), m);
		return m;
	}
	
	@Test
	public void testExecute() throws Exception {
		ScriptModule m = load("scr1");
		assertNotNull(m);
		assertTrue( m.prepare() );

		// tags
		Tag myvar3 = m.getTagTable().get("MyVar3");
		assertNotNull( myvar3 );
		assertEquals( 22, myvar3.getInt() );

		Tag cnt = m.getTagTable().get("proc1.cnt");
		assertNull( cnt );

		Tag counter = m.getTagTable().get("proc1.counter");
		assertNotNull( counter );
		assertEquals( 0, counter.getInt() );

		Tag thetag = m.getTagTable().get("proc1.thetag");
		assertNotNull( thetag );
		assertEquals( Tag.Type.INT, thetag.getType() );
		assertEquals( 123, thetag.getInt() );
		
		// execute
		assertTrue( m.execute() );
		assertEquals("", m.getErrors() );
		assertEquals( 23, myvar3.getInt() );
		assertEquals( 122, thetag.getInt() );
		assertEquals( 45, counter.getInt() );
		
		assertTrue( m.execute() );
		assertEquals( 24, myvar3.getInt() );
		assertEquals( 121, thetag.getInt() );
		assertEquals( 47, counter.getInt() );
		
		
		// state
		State state = new State();
		m.saveState(state);
		assertEquals(1, state.size());
		assertEquals( "37", state.get("proc1.cnt") );
		state.put("proc1.cnt", "222");
		m.loadState(state);
		
		assertTrue( m.execute() );
		assertEquals( 25, myvar3.getInt() );
		assertEquals( 120, thetag.getInt() );
		assertEquals( 234, counter.getInt() );

	}
	
	
}
