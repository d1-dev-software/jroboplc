package promauto.jroboplc.plugin.script;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import promauto.jroboplc.core.TagTable;
import promauto.jroboplc.core.api.InitUtils;
import promauto.jroboplc.core.ConfigurationYaml;
import promauto.jroboplc.core.api.Environment;
import promauto.jroboplc.core.api.EnvironmentInst;
import promauto.jroboplc.core.api.Module;
import promauto.jroboplc.core.api.ModuleManager;
import promauto.jroboplc.core.api.Tag;
import promauto.jroboplc.core.tags.RefFactoryImpl;
import promauto.jroboplc.core.tags.TagInt;
import promauto.jroboplc.core.tags.Ref;

public class TagRefTest {
	private static String CFG_DIR = InitUtils.getResourcesDir("script", ScriptModuleTest.class);

	Ref ref;
	Module mod1;
	Module mod2;
	Tag tag1;
	Tag tag2;

	@Before
	public void setUp() {
		RefFactoryImpl reffact = new RefFactoryImpl();
		ref = reffact.createRef(); //new Ref();
		
		tag1 = new TagInt("tag1", 1);
		tag2 = new TagInt("tag2", 2);
		
		mod1 = mock(Module.class);
		when(mod1.getName()).thenReturn("mod1");

		
		TagTable tt = mock(TagTable.class);
		when(tt.get("tag1")).thenReturn(tag1);
		when(tt.get("tag2")).thenReturn(tag2);
		when(tt.get("tag3")).thenReturn(null);

		mod2 = mock(Module.class);
		when(mod2.getName()).thenReturn("mod2");
		when(mod2.getTagTable()).thenReturn(tt);


		ModuleManager mm = mock(ModuleManager.class);
		when(mm.getModule("mod1")).thenReturn(mod1);
		when(mm.getModule("mod2")).thenReturn(mod2);
		when(mm.getModule("mod3")).thenReturn(null);

		Environment env = mock(Environment.class);
		EnvironmentInst.set(env);
		when(env.getModuleManager()).thenReturn(mm);
		
		when(env.getConfiguration()).thenReturn( new ConfigurationYaml(CFG_DIR) );
		env.getConfiguration().load();

	}

	@Test
	public void testInit() {

		Map<String,Object> conf = new HashMap<>();
		conf.put("src", "tag1");
		assertTrue( ref.init(conf, "src", mod1) );
		assertEquals( "mod1", ref.getRefModuleName());
		assertEquals( "tag1", ref.getRefTagName());

		conf.put("src", "mod2:tag1");
		assertTrue( ref.init(conf, "src", mod1) );
		assertEquals( "mod2", ref.getRefModuleName());
		assertEquals( "tag1", ref.getRefTagName());
		
		conf.put("src", ":tag1");
		assertFalse( ref.init(conf, "src", mod1) );

	}

	
	@Test
	public void testLink() {
		Map<String,Object> conf = new HashMap<>();
		conf.put("src", "mod2:tag1");
		assertTrue( ref.init(conf, "src", mod1) );
		assertTrue( ref.prepare() );
		assertTrue( ref.link() );
		assertSame( tag1, ref.getTag() );
		
		conf.put("src", "mod2:tag2");
		assertTrue( ref.init(conf, "src", mod1) );
		assertTrue( ref.link() );
		assertSame( tag2, ref.getTag() );

		assertTrue( ref.isValid() );
		assertEquals( "mod2:tag2", ref.getName() );

		conf.put("src", "mod2:tag3");
		assertTrue( ref.init(conf, "src", mod1) );
		assertFalse( ref.link() );
		
		conf.put("src", "mod3:tag1");
		assertTrue( ref.init(conf, "src", mod1) );
		assertTrue( ref.prepare() );
		assertFalse( ref.link() );
		
		assertFalse( ref.isValid() );
	}
}
