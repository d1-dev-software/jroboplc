package promauto.jroboplc.plugin.script.samples;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import promauto.jroboplc.core.ConfigurationYaml;
import promauto.jroboplc.core.api.EnvironmentInst;
import promauto.jroboplc.core.api.SampleEnvironment;
import promauto.jroboplc.core.api.Tag;
import promauto.jroboplc.core.tags.Ref;
import promauto.jroboplc.core.tags.RefDummy;
import promauto.jroboplc.plugin.script.ActionScriptJava;
import promauto.jroboplc.plugin.script.ScriptModule;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static promauto.jroboplc.plugin.script.samples.UvSteamerAScript.*;


class UvSteamerAScriptTest {
    UvSteamerAScript m;

    @BeforeEach
    void setUp() {
        SampleEnvironment env = new SampleEnvironment();
        EnvironmentInst.set(env);
        env.setConfiguration(new ConfigurationYaml(""));

        m = new UvSteamerAScript();

        ScriptModule sm = new ScriptModule(null, "");
        ActionScriptJava asj = new ActionScriptJava(sm);
        Map<Object,Object> conf = new HashMap<>();

        m.load(sm, asj, conf);
        m.useTimerSimple();


        for(Ref ref: asj.getRefs()) {
            Tag tag = sm.getTagTable().get(ref.getRefTagName());
            if( tag != null )
                ((RefDummy)ref).tag = tag;
        }

    }

    @Test
    void executeTest() {
        m.tagEnable         .setInt(0);
        m.tagReset          .setInt(0);
        m.tagWarmSkip       .setInt(0);
        m.tagWarmRepeat     .setInt(0);
        m.tagWarmPassQnt    .setInt(2);
        m.tagWarmPInfl      .setInt(8);
        m.tagWarmTimeInfl   .setInt(2);
        m.tagWarmTimeExp    .setInt(2);
        m.tagWarmPDefl      .setInt(3);
        m.tagWarmPDefl2     .setInt(6);
        m.tagWarmTimeDefl   .setInt(2);
        m.tagCycleCnt       .setInt(0);
        m.tagTimeLoad       .setInt(2);
        m.tagTimeAfterLoad  .setInt(2);
        m.tagPInfl          .setInt(10);
        m.tagPInfl1         .setInt(5);
        m.tagTimeInfl       .setInt(2);
        m.tagTimeExp        .setInt(4);
        m.tagPDefl          .setInt(4);
        m.tagPDefl2         .setInt(7);
        m.tagTimeDefl       .setInt(2);
        m.tagTimeCanReinfl  .setInt(2);
        m.tagTimeUnload     .setInt(2);
        m.tagPLineMin       .setInt(12);


        m.inpStart          .setInt(0);
        m.inpDvu            .setInt(0);
        m.inpDnu            .setInt(0);
        m.inpVlvInflClosed  .setInt(0);
        m.inpVlvInflOpened  .setInt(0);
        m.inpVlvDefl1Closed .setInt(0);
        m.inpVlvDefl1Opened .setInt(0);
        m.inpVlvDefl2Closed .setInt(0);
        m.inpVlvDefl2Opened .setInt(0);
        m.inpVlvDrnClosed   .setInt(0);
        m.inpVlvDrnOpened   .setInt(0);
        m.inpZdvBunkClosed  .setInt(0);
        m.inpZdvBunkOpened  .setInt(0);
        m.inpZdvLoadClosed  .setInt(0);
        m.inpZdvLoadOpened  .setInt(0);
        m.inpZdvUnloadClosed.setInt(0);
        m.inpZdvUnloadOpened.setInt(0);
        m.inpPLine          .setInt(15);
        m.inpPIn            .setInt(0);
        m.inpPOut           .setInt(0);
        m.inpBunkFlowStopped.setInt(0);
        m.inpCanUnload      .setInt(0);

        // 1
        m.execute();
        check( STATE_IDLE,0,0,0,
                0,0,0,0,1,0,0,
                0,0,0,0,0,0,
                0,0);

        // 2
        m.inpStart.setInt(1);
        m.execute();
        check(STATE_WARM_DRAIN_OPEN,0,1,0,
                0,0,0,1,1,0,0,
                0,0,0,0,0,0,
                0,0);

        // 3
        m.inpVlvDrnOpened.setInt(1);
        m.execute();
        check(STATE_WARM_PREPARE,0,1,0,
                0,0,0,1,1,0,0,
                0,0,0,0,0,0,
                0,0);

        // 4
        m.inpVlvDefl1Closed.setInt(1);
        m.inpVlvDefl2Closed.setInt(1);
        m.inpZdvLoadClosed.setInt(1);
        m.inpZdvUnloadClosed.setInt(1);
        m.execute();
        check(STATE_WARM_INFL,2,1,0,
                1,0,0,1,1,0,0,
                0,0,0,0,0,0,
                0,0);

        // 5
        m.execute();
        check(STATE_WARM_INFL,1,1,0,
                1,0,0,1,1,0,0,
                0,0,0,0,0,0,
                0,0);

        // 6
        m.execute();
        check(STATE_WARM_INFL,0,1,0,
                1,0,0,1,1,0,0,
                1,0,0,0,0,0,
                0,0);

        // 7
        m.inpPOut.setInt(8);
        m.execute();
        check(STATE_WARM_EXP,2,1,0,
                0,0,0,1,1,0,0,
                1,0,0,0,0,0,
                0,0);

        // 8
        m.execute();
        check(STATE_WARM_EXP,1,1,0,
                0,0,0,1,1,0,0,
                0,0,0,0,0,0,
                0,0);

        // 9
        m.execute();
        check(STATE_WARM_DEFL,2,1,0,
                0,1,0,1,1,0,0,
                0,0,0,0,0,0,
                0,0);

        // 10
        m.inpPOut.setInt(6);
        m.inpVlvDefl1Closed.setInt(0);
        m.inpVlvDefl1Opened.setInt(1);
        m.execute();
        check(STATE_WARM_DEFL,1,1,0,
                0,1,1,1,1,0,0,
                0,0,0,0,0,0,
                0,0);

        // 11
        m.inpPOut.setInt(5);
        m.inpVlvDefl2Closed.setInt(0);
        m.inpVlvDefl2Opened.setInt(1);
        m.execute();
        check(STATE_WARM_DEFL,0,1,0,
                0,1,1,1,1,0,0,
                0,1,0,0,0,0,
                0,0);

        // 12
        m.inpPOut.setInt(3);
        m.execute();
        check(STATE_WARM_PREPARE,0,1,1,
                0,0,0,1,1,0,0,
                0,1,0,0,0,0,
                0,0);

        // 13
        m.execute();
        check(STATE_WARM_PREPARE,0,1,1,
                0,0,0,1,1,0,0,
                0,0,0,0,0,0,
                0,0);


        // 14
        m.inpVlvDefl1Closed.setInt(1);
        m.inpVlvDefl2Closed.setInt(1);
        m.execute();
        check(STATE_WARM_INFL,2,1,1,
                1,0,0,1,1,0,0,
                0,0,0,0,0,0,
                0,0);

        // 15
        m.inpPOut.setInt(8);
        m.execute();
        check(STATE_WARM_EXP,2,1,1,
                0,0,0,1,1,0,0,
                0,0,0,0,0,0,
                0,0);

        // 16
        m.execute();
        check(STATE_WARM_EXP,1,1,1,
                0,0,0,1,1,0,0,
                0,0,0,0,0,0,
                0,0);

        // 17
        m.execute();
        check(STATE_WARM_DEFL,2,1,1,
                0,1,0,1,1,0,0,
                0,0,0,0,0,0,
                0,0);

        // 18
        m.inpPOut.setInt(0);
        m.execute();
        check(STATE_WARM_DRAIN_CLOSE,0,1,2,
                0,1,1,0,1,0,0,
                0,0,0,0,0,0,
                0,0);

        // 19
        m.inpVlvDrnClosed.setInt(1);
        m.inpVlvDrnOpened.setInt(0);
        m.execute();
        check(STATE_LOAD_BUNK_FULL,0,0,2,
                0,1,1,0,1,0,0,
                0,0,0,0,0,0,
                0,0);

        // 20
        m.inpDvu.setInt(1);
        m.inpDnu.setInt(1);
        m.execute();
        check(STATE_BUNK_FLOW_STOP,0,0,2,
                0,1,1,0,1,0,0,
                0,0,0,0,0,0,
                1,0);

        // 20
        m.inpBunkFlowStopped.setInt(1);
        m.execute();
        check(STATE_LOAD_ENABLE,0,0,2,
                0,1,1,0,0,0,0,
                0,0,0,0,0,0,
                1,0);

        // 21
        m.tagEnable.setInt(1);
        m.execute();
        check(STATE_LOAD_PREPARE,0,0,2,
                0,1,1,0,0,0,0,
                0,0,0,0,0,0,
                1,0);

        // 22
        m.inpVlvInflClosed.setInt(1);
        m.inpZdvBunkClosed.setInt(1);
        m.inpZdvUnloadClosed.setInt(1);
        m.execute();
        check(STATE_LOAD,2,0,2,
                0,1,1,0,0,1,0,
                0,0,0,0,0,0,
                1,0);

        // 22
        m.execute();
        check(STATE_LOAD,1,0,2,
                0,1,1,0,0,1,0,
                0,0,0,0,0,0,
                1,0);

        // 23
        m.execute();
        check(STATE_LOAD,0,0,2,
                0,1,1,0,0,1,0,
                0,0,1,0,0,0,
                1,0);

        // 24
        m.inpDvu.setInt(0);
        m.inpDnu.setInt(0);
        m.execute();
        check(STATE_LOAD_AFTER_DLY,2,0,2,
                0,1,1,0,0,1,0,
                0,0,1,0,0,0,
                1,0);

        // 25
        m.execute();
        check(STATE_LOAD_AFTER_DLY,1,0,2,
                0,1,1,0,0,1,0,
                0,0,0,0,0,0,
                1,0);

        // 26
        m.inpZdvLoadClosed.setInt(0);
        m.inpZdvUnloadClosed.setInt(0);
        m.inpVlvDefl2Closed.setInt(0);
        m.execute();
        check(STATE_INFL_PREPARE,0,0,2,
                0,1,0,0,0,0,0,
                0,0,0,0,0,0,
                1,0);

        // 26.1
        m.inpZdvUnloadClosed.setInt(1);
        m.execute();
        check(STATE_INFL_PREPARE,0,0,2,
                0,1,0,0,0,0,0,
                0,0,0,0,0,0,
                1,0);

        // 26.2
        m.inpVlvDefl2Closed.setInt(1);
        m.execute();
        check(STATE_INFL_PREPARE,0,0,2,
                0,1,0,0,0,0,0,
                0,0,0,0,0,0,
                1,0);


        // 27
        m.inpZdvLoadClosed.setInt(1);
        m.execute();
        check(STATE_INFL,2,0,2,
                1,1,0,0,1,0,0,
                0,0,0,0,0,0,
                0,0);

        // 28
        m.inpPOut.setInt(1);
        m.execute();
        check(STATE_INFL,1,0,2,
                1,1,0,0,1,0,0,
                0,0,0,0,0,0,
                0,0);

        // 28
        assertEquals(1,  m.tagEnable.getInt());
        m.inpPOut.setInt(2);
        m.execute();
        check(STATE_INFL,0,0,2,
                1,1,0,0,1,0,0,
                0,0,0,1,0,0,
                0,0);
        assertEquals(0,  m.tagEnable.getInt());

        // 29
        m.inpPOut.setInt(5);
        m.execute();
        check(STATE_INFL,0,0,2,
                1,0,0,0,1,0,0,
                0,0,0,1,0,0,
                0,0);

        // 30
        m.inpPOut.setInt(10);
        m.execute();
        check(STATE_EXPOSE,4,0,2,
                0,0,0,0,1,0,0,
                0,0,0,1,0,0,
                0,0);

        // 31
        m.inpPOut.setInt(9);
        m.execute();
        check(STATE_EXPOSE,3,0,2,
                1,0,0,0,1,0,0,
                0,0,0,0,0,0,
                0,0);

        // 32
        m.inpPOut.setInt(10);
        m.execute();
        check(STATE_EXPOSE,2,0,2,
                0,0,0,0,1,0,0,
                0,0,0,0,0,0,
                0,0);

        // 32
        m.inpPOut.setInt(9);
        m.execute();
        check(STATE_EXPOSE,1,0,2,
                0,0,0,0,1,0,0,
                0,0,0,0,0,0,
                0,0);

        // 33
        m.inpPOut.setInt(10);
        m.execute();
        check(STATE_DEFL,2,0,2,
                0,1,0,0,1,0,0,
                0,0,0,0,0,0,
                0,0);

        // 34
        m.inpPOut.setInt(7);
        m.execute();
        check(STATE_DEFL,1,0,2,
                0,1,1,0,1,0,0,
                0,0,0,0,0,0,
                0,0);

        // 35
        m.inpPOut.setInt(5);
        m.execute();
        check(STATE_DEFL,0,0,2,
                0,1,1,0,1,0,0,
                0,0,0,0,1,0,
                0,0);

        // 35
        m.inpPOut.setInt(4);
        m.execute();
        check(STATE_CAN_UNLOAD,0,0,2,
                0,1,1,0,1,0,0,
                0,0,0,0,1,0,
                0,1);

        // 36
        m.inpCanUnload.setInt(1);
        m.execute();
        check(STATE_UNLOAD,2,0,2,
                0,1,1,0,1,0,1,
                0,0,0,0,0,0,
                0,1);

        // 37
        m.execute();
        check(STATE_UNLOAD,1,0,2,
                0,1,1,0,1,0,1,
                0,0,0,0,0,0,
                0,1);

        // 38
        assertEquals(0,  m.tagCycleCnt.getInt());
        m.execute();
        check(STATE_FINISH,0,0,2,
                0,1,1,0,1,0,1,
                0,0,0,0,0,0,
                0,0);
        assertEquals(1,  m.tagCycleCnt.getInt());

        // 39
        m.execute();
        check(STATE_LOAD_BUNK_FULL,0,0,2,
                0,1,1,0,1,0,0,
                0,0,0,0,0,0,
                0,0);

        // 40
        m.tagWarmRepeat.setInt(1);
        m.execute();
        check(STATE_WARM_DRAIN_OPEN,0,1,2,
                0,1,1,0,1,0,0,
                0,0,0,0,0,0,
                0,0);

        // 41
        m.execute();
        check(STATE_WARM_DRAIN_OPEN,0,1,0,
                0,0,0,1,1,0,0,
                0,0,0,0,0,0,
                0,0);

        // 42
        m.tagWarmSkip.setInt(1);
        m.execute();
        check(STATE_LOAD_BUNK_FULL,0,0,0,
                0,1,1,0,1,0,0,
                0,0,0,0,0,0,
                0,0);

        // 42
        m.inpStart.setInt(0);
        m.tagReset.setInt(1);
        m.execute();
        check(STATE_IDLE,0,0,0,
                0,0,0,0,1,0,0,
                0,0,0,0,0,0,
                0,0);


        assertEquals(0,  m.tagReset.getInt());
        assertEquals(0,  m.tagWarmSkip.getInt());



    }

    private void check(
        int state,
        int timer,
        int warming,
        int warmPassCnt,
        int vlvInfl,
        int vlvDefl1,
        int vlvDefl2,
        int vlvDrn,
        int zdvBunk,
        int zdvLoad,
        int zdvUnload,
        int alarmWarmTimeInfl,
        int alarmWarmTimeDefl,
        int alarmTimeLoad,
        int alarmTimeInfl,
        int alarmTimeDefl,
        int alarmPLine,
        int cmdBunkFlowStop,
        int cmdUnloadSeq
    ) {
        assertEquals(state            ,     m.tagState                .getInt(), "tagState            ");
        assertEquals(timer            ,     m.tagTimer                .getInt(), "tagTimer            ");
        assertEquals(warmPassCnt      ,     m.tagWarmPassCnt          .getInt(), "tagWarmPassCnt      ");
        assertEquals(vlvInfl          ,     m.tagVlvInflOpen          .getInt(), "tagVlvInflOpen      ");
        assertEquals(vlvDefl1         ,     m.tagVlvDefl1Open         .getInt(), "tagVlvDefl1Open     ");
        assertEquals(vlvDefl2         ,     m.tagVlvDefl2Open         .getInt(), "tagVlvDefl2Open     ");
        assertEquals(vlvDrn           ,     m.tagVlvDrnOpen           .getInt(), "tagVlvDrnOpen       ");
        assertEquals(zdvBunk          ,     m.tagZdvBunkOpen          .getInt(), "tagZdvBunkOpen      ");
        assertEquals(zdvLoad          ,     m.tagZdvLoadOpen          .getInt(), "tagZdvLoadOpen      ");
        assertEquals(zdvUnload        ,     m.tagZdvUnloadOpen        .getInt(), "tagZdvUnloadOpen    ");
        assertEquals(alarmWarmTimeInfl,     m.tagAlarmWarmTimeInfl    .getInt(), "tagAlarmWarmTimeInfl");
        assertEquals(alarmWarmTimeDefl,     m.tagAlarmWarmTimeDefl    .getInt(), "tagAlarmWarmTimeDefl");
        assertEquals(alarmTimeLoad    ,     m.tagAlarmTimeLoad        .getInt(), "tagAlarmTimeLoad    ");
        assertEquals(alarmTimeInfl    ,     m.tagAlarmTimeInfl        .getInt(), "tagAlarmTimeInfl    ");
        assertEquals(alarmTimeDefl    ,     m.tagAlarmTimeDefl        .getInt(), "tagAlarmTimeDefl    ");
        assertEquals(alarmPLine       ,     m.tagAlarmPLine           .getInt(), "tagAlarmPLine       ");
        assertEquals(cmdBunkFlowStop  ,     m.tagCmdBunkFlowStop      .getInt(), "tagCmdBunkFlowStop  ");
        assertEquals(cmdUnloadSeq     ,     m.tagCmdUnloadSeq         .getInt(), "tagCmdUnloadSeq     ");
        assertEquals(warming          ,     m.tagWarming              .getInt(), "tagWarming          ");
    }



}