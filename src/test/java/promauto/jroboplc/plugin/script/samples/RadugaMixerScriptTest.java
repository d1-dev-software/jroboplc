package promauto.jroboplc.plugin.script.samples;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import promauto.jroboplc.core.ConfigurationYaml;
import promauto.jroboplc.core.api.EnvironmentInst;
import promauto.jroboplc.core.api.SampleEnvironment;
import promauto.jroboplc.plugin.script.ActionScriptJava;
import promauto.jroboplc.plugin.script.ScriptModule;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static promauto.jroboplc.plugin.script.samples.RadugaMixerScript.*;

class RadugaMixerScriptTest {
    RadugaMixerScript m;

    @BeforeEach
    void setUp() {
        SampleEnvironment env = new SampleEnvironment();
        EnvironmentInst.set(env);
        env.setConfiguration(new ConfigurationYaml(""));

        m = new RadugaMixerScript();

        ScriptModule sm = new ScriptModule(null, "");
        ActionScriptJava asj = new ActionScriptJava(sm);
        Map<Object,Object> conf = new HashMap<>();

        m.load(sm, asj, conf);
    }

    @Test
    void executeTest() {
/*
        m.tagTimeExp.setInt(5);
        m.tagTimeOverrun.setInt(4);
        m.tagTimeUnload.setInt(3);
        m.tagTimeClean.setInt(2);

        // 1
        m.execute();
        check(0, STATE_IDLE,  0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        assertFalse(m.tagBusy.getBool());

        // 2
        m.inpStart.setInt(1);
        m.execute();
        check(0, STATE_START_OPEN,  0, 0, 0, 0, 0, 0, 0, 0, 1, 1);

        // 3
        m.refZdvClosedC.setInt(0);
        m.refZdvOpenedC.setInt(1);
        m.execute();
        check(0, STATE_START_OPEN,  0, 0, 0, 0, 0, 0, 1, 1, 1, 1);

        // 4
        m.refZdvClosedB.setInt(0);
        m.refZdvOpenedB.setInt(1);
        m.execute();
        check(0, STATE_START_OPEN,  0, 0, 0, 0, 1, 1, 1, 1, 1, 1);

        // 5
        m.refZdvClosedA.setInt(0);
        m.refZdvOpenedA.setInt(1);
        m.execute();
        check(0, STATE_START_OPEN,  0, 0, 0, 1, 1, 1, 1, 1, 1, 1);

        // 6
        m.execute();
        check(0, STATE_START_OPEN_DELAY,  2, 0, 0, 1, 1, 1, 1, 1, 1, 1);

        // 7
        m.execute();
        check(0, STATE_START_OPEN_DELAY,  1, 0, 0, 1, 1, 1, 1, 1, 1, 1);

        // 8
        m.execute();
        check(0, STATE_START_OPEN_DELAY,  0, 0, 0, 1, 1, 1, 1, 1, 1, 1);

        // 9
        m.execute();
        check(1, STATE_START_MIXER,  0, 0, 0, 1, 1, 1, 1, 1, 1, 1);

        // 10
        m.refMixerMasterOut.setInt(1);
        m.execute();
        check(1, STATE_START_CLOSE,  0, 0, 0, 1, 1, 0, 0, 0, 0, 0);

        // 11
        m.refZdvClosedA.setInt(1);
        m.refZdvOpenedA.setInt(0);
        m.execute();
        check(1, STATE_START_CLOSE,  0, 0, 0, 0, 1, 0, 1, 0, 0, 0);

        // 12
        m.refZdvClosedB.setInt(1);
        m.refZdvOpenedB.setInt(0);
        m.execute();
        check(1, STATE_START_CLOSE,  0, 0, 0, 0, 1, 0, 1, 0, 1, 0);

        // 13
        m.refZdvClosedC.setInt(1);
        m.refZdvOpenedC.setInt(0);
        m.execute();
        check(1, STATE_START_CLOSE,  0, 0, 1, 0, 1, 0, 1, 0, 1, 0);

        // 14
        m.inpLoaded.setInt(0);
        m.execute();
        check(1, STATE_LOAD,  0, 0, 1, 0, 1, 0, 1, 0, 1, 0);
        assertFalse(m.tagBusy.getBool());

        // 15
        m.inpLoaded.setInt(1);
        m.execute();
        assertTrue(m.tagBusy.getBool());
        check(1, STATE_EXPOSITION,  4, 0, 1, 0, 1, 0, 1, 0, 1, 0);
        m.execute();
        check(1, STATE_EXPOSITION,  3, 0, 1, 0, 1, 0, 1, 0, 1, 0);
        m.execute();
        check(1, STATE_EXPOSITION,  2, 0, 1, 0, 1, 0, 1, 0, 1, 0);
        m.execute();
        check(1, STATE_EXPOSITION,  1, 0, 1, 0, 1, 0, 1, 0, 1, 0);
        m.execute();
        check(1, STATE_EXPOSITION,  0, 0, 1, 0, 1, 0, 1, 0, 1, 0);

        // 16
        m.execute();
        check(1, STATE_UNLOAD_READY,  0, 0, 1, 0, 1, 0, 1, 0, 1, 0);

        // 17
        m.inpCanUnload.setInt(1);
        m.execute();
        check(1, STATE_UNLOAD_OPEN,  0, 0, 1, 0, 0, 0, 0, 0, 1, 1);

        // 18
        m.refZdvClosedC.setInt(0);
        m.refZdvOpenedC.setInt(1);
        m.execute();
        check(1, STATE_UNLOAD_OPEN,  0, 0, 0, 0, 0, 0, 1, 1, 1, 1);

        // 19
        m.refZdvClosedB.setInt(0);
        m.refZdvOpenedB.setInt(1);
        m.execute();
        check(1, STATE_UNLOAD_OPEN,  0, 0, 0, 0, 1, 1, 1, 1, 1, 1);

        // 20
        m.refMixerMasterOut.setInt(0);
        m.refZdvClosedA.setInt(0);
        m.refZdvOpenedA.setInt(1);
        m.execute();
        check(1, STATE_UNLOAD_OPEN,  0, 1, 0, 1, 1, 1, 1, 1, 1, 1);

        // 21
        m.execute();
        check(1, STATE_UNLOAD_DELAY,  2, 1, 0, 1, 1, 1, 1, 1, 1, 1);
        m.execute();
        check(1, STATE_UNLOAD_DELAY,  1, 1, 0, 1, 1, 1, 1, 1, 1, 1);
        m.execute();
        check(1, STATE_UNLOAD_DELAY,  0, 1, 0, 1, 1, 1, 1, 1, 1, 1);

        // 22
        m.refDvuOutput.setInt(1);
        m.execute();
        check(1, STATE_UNLOAD_DVU,  0, 1, 0, 1, 1, 1, 1, 1, 1, 1);

        // 23
        m.refDvuOutput.setInt(0);
        m.execute();
        check(1, STATE_CLEAN_START_MIXER,  0, 0, 0, 1, 1, 1, 1, 1, 1, 1);

        // 24
        m.refMixerMasterOut.setInt(1);
        m.execute();
        check(1, STATE_CLEAN_DELAY,  1, 0, 0, 1, 1, 1, 1, 1, 1, 1);
        m.execute();
        check(1, STATE_CLEAN_DELAY,  0, 0, 0, 1, 1, 1, 1, 1, 1, 1);
        assertTrue(m.tagBusy.getBool());

        // 25
        m.execute();
        check(0, STATE_IDLE,  0, 0, 0, 1, 0, 0, 0, 0, 0, 0);
        assertFalse(m.tagBusy.getBool());

// second cycle

        // 26
        m.inpStart.setInt(1);
        m.refMixerMasterOut.setInt(0);
        m.inpLoaded.setInt(0);
        m.execute();
        check(1, STATE_START_MIXER,  0, 0, 0, 1, 1, 1, 1, 1, 1, 1);

*/
    }


    private void check(
            int tagMixer,
            int tagState,
            int tagTimer,
            int tagOverrun,
            int tagClosed,
            int tagOpened,
            int tagZdvEnA,
            int tagZdvOpA,
            int tagZdvEnB,
            int tagZdvOpB,
            int tagZdvEnC,
            int tagZdvOpC
    ) {
        assertEquals(tagMixer,     m.tagMixer    .getInt(), "tagMixer    ");
        assertEquals(tagState,     m.tagState    .getInt(), "tagState    ");
        assertEquals(tagTimer,     m.tagTimer    .getInt(), "tagTimer    ");
        assertEquals(tagOverrun,   m.tagOverrun  .getInt(), "tagOverrun  ");
        assertEquals(tagClosed,    m.tagClosed   .getInt(), "tagClosed   ");
        assertEquals(tagOpened,    m.tagOpened   .getInt(), "tagOpened   ");
        assertEquals(tagZdvEnA,    m.tagZdvEnA   .getInt(), "tagZdvEnA   ");
        assertEquals(tagZdvOpA,    m.tagZdvOpA   .getInt(), "tagZdvOpA   ");
        assertEquals(tagZdvEnB,    m.tagZdvEnB   .getInt(), "tagZdvEnB   ");
        assertEquals(tagZdvOpB,    m.tagZdvOpB   .getInt(), "tagZdvOpB   ");
        assertEquals(tagZdvEnC,    m.tagZdvEnC   .getInt(), "tagZdvEnC   ");
        assertEquals(tagZdvOpC,    m.tagZdvOpC   .getInt(), "tagZdvOpC   ");
    }
}

