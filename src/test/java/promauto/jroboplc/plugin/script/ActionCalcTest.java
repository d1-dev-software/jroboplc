package promauto.jroboplc.plugin.script;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;

import parsii.eval.Parser;
import parsii.tokenizer.ParseException;
import promauto.jroboplc.core.api.EnvironmentInst;
import promauto.jroboplc.core.api.SampleEnvironment;
import promauto.jroboplc.core.tags.RefFactoryImpl;
import promauto.jroboplc.core.tags.TagDouble;
import promauto.jroboplc.core.tags.TagInt;
import promauto.jroboplc.core.api.Module;
import promauto.jroboplc.core.api.Tag;
import promauto.jroboplc.core.tags.Ref;

public class ActionCalcTest {

	ActionCalc ac;

	ScriptModule mod;
	Module mod1;
	Module mod2;
	Tag tagR;
	Tag tagX;
	Tag tagY;

	
	@Before
	public void setUp() throws ParseException {
		SampleEnvironment env = new SampleEnvironment();
		EnvironmentInst.set(env);
		env.refFactory =  new RefFactoryImpl();
		
		mod = mock(ScriptModule.class);
		ac = new ActionCalc(mod);

		tagR = new TagDouble("tag1", 1);
		tagX = new TagDouble("tag2", 2);
		tagY = new TagInt("tag3", 3);

		ac.rslt = mock(Ref.class);
		
		ac.expr = new ActionCalc.Expr("10*x+y");
		ActionCalc.Argument arg1 = new ActionCalc.Argument("x", ac.expr.scope); 
		ActionCalc.Argument arg2 = new ActionCalc.Argument("y", ac.expr.scope);
		arg1.ref = mock(Ref.class);
		arg2.ref = mock(Ref.class);
		ac.args.add(arg1);
		ac.args.add(arg2);
		
		
		when(ac.rslt.getTag()).thenReturn(tagR);
		when(arg1.ref.getTag()).thenReturn(tagX);
		when(arg2.ref.getTag()).thenReturn(tagY);
		
		when(ac.rslt.linkIfNotValid()).thenReturn(true);
		when(arg1.ref.linkIfNotValid()).thenReturn(true);
		when(arg2.ref.linkIfNotValid()).thenReturn(true);
		
	}


	@Test
	public void testExpr1() throws InterruptedException, ParseException {
		ac.expr.expr = Parser.parse("x^y", ac.expr.scope);
		tagR.setDouble(1);
		tagX.setDouble(2);
		tagY.setInt(3);
		ac.execute();
		assertEquals(8, tagR.getDouble(), 0.001 );
	}

	
	@Test
	public void testExpr2() throws InterruptedException, ParseException {
		ac.expr.expr = Parser.parse("min(max(x,y),10)", ac.expr.scope);
		tagR.setDouble(1);
		tagX.setDouble(2);
		tagY.setInt(3);
		ac.execute();
		assertEquals(3, tagR.getDouble(), 0.001 );
	}

	@Test
	public void testExpr3() throws InterruptedException, ParseException {
		ac.expr.expr = Parser.parse( "AND( OR(8, AND(x,y)), 14)", ac.expr.scope);
		tagR.setDouble(0);
		tagX.setDouble(7);
		tagY.setInt(3);
		ac.execute();
		assertEquals(10, tagR.getDouble(), 0.001 );
	}

	@Test
	public void testExpr4() throws InterruptedException, ParseException {
		ac.expr.expr = Parser.parse( "(x>y) + (x>10) + (y<10)", ac.expr.scope);
		tagR.setDouble(0);
		tagX.setDouble(7);
		tagY.setInt(3);
		ac.execute();
		assertEquals(2, tagR.getDouble(), 0.001 );
	}

	
	@Test
	public void testExecute() throws InterruptedException {
		// 1
		assertEquals(1, tagR.getInt() );
		assertEquals(2, tagX.getInt() );
		assertEquals(3, tagY.getInt() );
		ac.execute();
		assertEquals(23, tagR.getInt() );
		assertEquals(2, tagX.getInt() );
		assertEquals(3, tagY.getInt() );
		
		// 2
		ac.execute();
		assertEquals(23, tagR.getInt() );

		// 3
		tagR.setDouble(11.1);
		ac.execute();
		assertEquals(23, tagR.getInt() );

		// 4
		tagX.setDouble(1.1);
		ac.execute();
		assertEquals(14, tagR.getInt() );

		// 5
		ac.delay_ms = 20;
		tagR.setDouble(11.1);
		ac.execute();
		assertEquals(11.1, tagR.getDouble(), 0.001 );
		Thread.sleep(10);
		ac.execute();
		assertEquals(11.1, tagR.getDouble(), 0.001 );
		Thread.sleep(15);
		ac.execute();
		assertEquals(14, tagR.getDouble(), 0.001 );

		// 6
		ac.delay_ms = 20;
		tagX.setDouble(2.2);
		ac.execute();
		assertEquals(14, tagR.getDouble(), 0.001 );
		Thread.sleep(10);
		ac.execute();
		assertEquals(14, tagR.getDouble(), 0.001 );
		Thread.sleep(15);
		ac.execute();
		assertEquals(25, tagR.getDouble(), 0.001 );
		
	}
	
}
