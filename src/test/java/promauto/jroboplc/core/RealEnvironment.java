package promauto.jroboplc.core;

import promauto.jroboplc.core.api.Configuration;
import promauto.jroboplc.core.api.Environment;
import promauto.jroboplc.core.api.EnvironmentInst;
import promauto.jroboplc.core.api.Plugin;
import promauto.jroboplc.core.cmd.CmdDispatcherImpl;

public class RealEnvironment {

	public static boolean create(String cfgdir, Plugin... plugins) throws Exception {
		Configuration conf = new ConfigurationYaml( cfgdir );
		if( !conf.load() )
			return false;

		Environment env = new EnvironmentImpl();
		EnvironmentInst.set(env);
		env.setConfiguration(conf);
		
		env.setConsoleManager( new ConsoleManagerImpl() );
		env.setConsole( new ConsoleInteract(env) );

		env.setModuleManager( new ModuleManagerImpl() );
		env.setCmdDispatcher( new CmdDispatcherImpl() );

		for(Plugin plugin: plugins)
			plugin.initialize();
		
		ModuleManagerImpl mm = (ModuleManagerImpl)EnvironmentInst.get().getModuleManager();
		mm.addPlugins(plugins);
		mm.loadModules();

		return true;
	}
	

}
