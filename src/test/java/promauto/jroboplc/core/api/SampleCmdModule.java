package promauto.jroboplc.core.api;

import promauto.jroboplc.core.api.Command;
import promauto.jroboplc.core.api.Console;
import promauto.jroboplc.core.api.Module;
import promauto.jroboplc.core.api.Plugin;

public class SampleCmdModule implements Command {

	@Override
	public String getName() {
		return "cmdmod";
	}

	@Override
	public String getUsage() {
		return "";
	}

	@Override
	public String getDescription() {
		return "cmdmod descr";
	}

	@Override
	public String execute(Console console, String args) {
		return "";
	}

	@Override
	public String execute(Console console, Plugin plugin, String args) {
		return "";
	}

	@Override
	public String execute(Console console, Module module, String args) {
		return "module command result " + getName() + " " + args;
	}

	@Override
	public void executePosted(Console console, Module module, String args) {
	}
	
}

