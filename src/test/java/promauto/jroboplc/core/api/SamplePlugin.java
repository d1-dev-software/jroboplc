package promauto.jroboplc.core.api;

import java.nio.file.Path;
import java.util.List;

import org.slf4j.Logger;

import promauto.jroboplc.core.api.Environment;
import promauto.jroboplc.core.api.EnvironmentInst;
import promauto.jroboplc.core.api.Module;
import promauto.jroboplc.core.api.Plugin;

public class SamplePlugin implements Plugin {
	public Environment env = null;
	public String name = "simplug";
	public String descr = "the simplug descr";

	@Override
	public void initialize() {
		env = EnvironmentInst.get();
}

	@Override
	public String getPluginName() {
		return name;
	}

	@Override
	public String getPluginDescription() {
		return descr;
	}

//	@Override
//	public Class<?> getModuleClass() {
//		return SampleModule.class;
//	}


	@Override
	public List<Module> getModules() {
		return null;
	}




	@Override
	public Module createModule(String name, Object conf) {
		SampleModule m = new SampleModule(name);
		return m;
	}
	
}