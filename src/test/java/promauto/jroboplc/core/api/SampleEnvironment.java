package promauto.jroboplc.core.api;

import javax.xml.stream.XMLInputFactory;

import org.slf4j.Logger;

import promauto.jroboplc.core.cmd.CmdDispatcherImpl;
import promauto.jroboplc.core.LoggerMode;
import promauto.jroboplc.core.tags.*;

public class SampleEnvironment implements Environment {
	public ConsoleManager cm = null;
	public Console csl = null;
	public ModuleManager mm = null;
	public CmdDispatcher cd = new CmdDispatcherImpl();
	public TaskManager tm = null;
	public SerialManager sm = new SampleSerialManager();
	public boolean terminated;
	public boolean running;
	private Configuration configuration = null;
	private KeyManager keyManager = null;
	public RefFactory refFactory = null;


	@Override
	public CmdDispatcher getCmdDispatcher() {
		return cd;
	}

	@Override
	public ModuleManager getModuleManager() {
		return mm;
	}

	@Override
	public Console getConsole() {
		return csl;
	}

	@Override
	public void setConsole(Console console) {
		csl = console;
	}

	@Override
	public TcpServer getTcpServer() {
		return null;
	}

	@Override
	public void setTcpServer(TcpServer tcpserver) {
	}

	@Override
	public boolean isTerminated() {
		return terminated;
	}

	@Override
	public void setTerminated() {
		terminated = true;
	}

	@Override
	public void setCmdDispatcher(CmdDispatcher commandDispatcher) {
		cd = commandDispatcher;
	}

	@Override
	public void setModuleManager(ModuleManager moduleManager) {
		mm = moduleManager;
	}

	@Override
	public TaskManager getTaskManager() {
		return tm;
	}

	@Override
	public void setTaskManager(TaskManager taskManager) {
		this.tm = taskManager;
	}

	@Override
	public boolean isRunning() {
		return running;
	}

	@Override
	public void setRunning(boolean status) {
		running = status;
	}


	@Override
	public XMLInputFactory getXMLInputFactory() {
		return null;
	}

	@Override
	public RefFactory getRefFactory() {
		if( refFactory == null )
			refFactory = new RefFactoryDummyImpl();
		return refFactory;
	}


	@Override
	public SerialManager getSerialManager() {
		return sm;
	}

	@Override
	public void setSerialManager(SerialManager serialManager) {
		this.sm = serialManager;
	}

	@Override
	public void printError(Logger logger, String... text) {
	}

	@Override
	public void printError(Logger logger, Throwable e, String... text) {
	}

    @Override
    public void logError(Logger logger, String... text) {

    }

    @Override
    public void logError(Logger logger, Throwable e, String... text) {

    }

    @Override
	public void printInfo(Logger logger, String... text) {
	}

    @Override
    public void logInfo(Logger logger, String... text) {

    }

    @Override
    public void logStatus(Logger logger, String... text) {

    }

    @Override
    public void print(String text) {

    }

    @Override
	public ConsoleManager getConsoleManager() {
		return cm;
	}

	@Override
	public void setConsoleManager(ConsoleManager consoleManager) {
		cm = consoleManager;
	}

	@Override
	public LoggerMode getLoggerMode() {
		return null;
	}

	@Override
	public Configuration getConfiguration() {
		return configuration;
	}

	@Override
	public void setConfiguration(Configuration conf) {
		configuration  = conf;
	}

	@Override
	public KeyManager getKeyManager() {
		return keyManager;
	}

	public void setKeyManager(KeyManager keyManager) {
		this.keyManager = keyManager;
	}
}
