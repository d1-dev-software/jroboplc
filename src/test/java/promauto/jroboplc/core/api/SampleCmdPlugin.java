package promauto.jroboplc.core.api;

import promauto.jroboplc.core.api.Command;
import promauto.jroboplc.core.api.Console;
import promauto.jroboplc.core.api.Module;
import promauto.jroboplc.core.api.Plugin;

public class SampleCmdPlugin implements Command {

	@Override
	public String getName() {
		return "cmdplug";
	}

	@Override
	public String getUsage() {
		return "";
	}

	@Override
	public String getDescription() {
		return "cmdplug descr";
	}

	@Override
	public String execute(Console console, String args) {
		return "";
	}

	@Override
	public String execute(Console console, Plugin plugin, String args) {
		return "plugin command result " + getName() + " " + args;
	}

	@Override
	public String execute(Console console, Module module, String args) {
		return "";
	}

	@Override
	public void executePosted(Console console, Module module, String args) {
	}
	
}

