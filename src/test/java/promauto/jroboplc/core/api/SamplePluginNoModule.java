package promauto.jroboplc.core.api;

import promauto.jroboplc.core.api.Module;

public class SamplePluginNoModule extends SamplePlugin {
	
	public SamplePluginNoModule() {
		name = "plug_no_modules";
		descr = "A plugin which is not able to produce any modules";
	}

	@Override
	public Module createModule(String name, Object conf) {
		return null;
	}


}
