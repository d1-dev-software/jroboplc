package promauto.jroboplc.core.api;

import promauto.jroboplc.core.api.SerialManager;
import promauto.jroboplc.core.api.SerialPort;

public class SampleSerialManager implements SerialManager {
	public SampleSerialPort port = new SampleSerialPort();

	@Override
	public SerialPort getPort(int portid) {
		return port;
	}

}
