package promauto.jroboplc.core.api;

import promauto.jroboplc.core.api.Command;
import promauto.jroboplc.core.api.Console;
import promauto.jroboplc.core.api.Module;
import promauto.jroboplc.core.api.Plugin;

public class SampleCmdCommon implements Command {

	@Override
	public String getName() {
		return "cmdcom";
	}

	@Override
	public String getUsage() {
		return "";
	}

	@Override
	public String getDescription() {
		return "cmdcom descr";
	}

	@Override
	public String execute(Console console, String args) {
		return "common command result " + getName() + " " + args;
	}

	@Override
	public String execute(Console console, Plugin plugin, String args) {
		return "";
	}

	@Override
	public String execute(Console console, Module module, String args) {
		return "";
	}

	@Override
	public void executePosted(Console console, Module module, String args) {
	}
	
}



