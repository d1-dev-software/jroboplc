package promauto.jroboplc.core.api;

import java.nio.charset.Charset;
import java.util.LinkedList;

import promauto.jroboplc.core.api.SerialPort;

public class SampleSerialPort implements SerialPort {
	private final static Charset charset = Charset.forName("UTF-8");
	
	public LinkedList<Integer> buffRead = new LinkedList<>(); 
	public LinkedList<Integer> buffWrite = new LinkedList<>();
	
	
	@Override
	public int getId() {
		return 0;
	}
	@Override
	public boolean setParams(int baud, int databits, int parity, int stopbits, int timeout) {
		return true;
	}
	@Override
	public boolean open() {
		return true;
	}
	@Override
	public void close() {
	}
	
	@Override
	public boolean isOpened() {
		return true;
	}
	
	@Override
	public boolean isValid() {
		return true;
	}
	
	@Override
	public int getAvailable() throws Exception {
		return 0;
	}
	
	@Override
	public int readByte() throws Exception {
		return buffRead.pollLast();
	}
	
	@Override
	public int readBytes(int[] buff, int size) throws Exception {
		int i;
//		for( i=0; i<buff.length; i++) {
		for( i=0; i<size; i++) {
			if (buffRead.size()==0) break;
			buff[i] = buffRead.pollLast();
		}
		return i;
	}
	
	@Override
	public int readBytesDelim(int[] buff, int delim) throws Exception {
		Integer b;
		int size = 0;
		while( (b = buffRead.pollLast()) != null) {
			buff[size++] = b;
			
			if( b == delim )
				break;
			
			if( size>=buff.length )
				return -1;
		}
		return size;
	}

	@Override
	public String readString(int size) throws Exception {
		return null;
	}
		
	@Override
	public String readStringDelim(int delim) throws Exception {
		StringBuilder sb = new StringBuilder();
		Integer i;
		while( (i = buffRead.pollLast()) != null) {
			sb.append( (char)i.byteValue() );
			if( i == delim )
				break;
		}

		return sb.toString();
	}
	
	@Override
	public boolean writeByte(int data) throws Exception {
		buffWrite.push(data);
		return true;
	}
	
	@Override
	public boolean writeBytes(int[] data, int size) throws Exception {
		for(int i=0; i<size; ++i)
			buffWrite.push(data[i]);
		return true;
	}
	
	@Override
	public boolean writeString(String data) throws Exception {
		for( int v: data.getBytes(charset)) 
			buffWrite.push(v);
		return true;
	}
	
	@Override
	public boolean discard() throws Exception {
		return false;
	}
	
	@Override
	public String getInfo() {
		return null;
	} 

	
	
	public void setBuffRead(int[] buff) {
		for( int v: buff) 
			buffRead.push(v);
	}

	public void setBuffRead(String buff) {
		for( int v: buff.getBytes(charset)) 
			buffRead.push(v);
	}

	public int[] getBuffWrite() {
		int[] buff = new int[buffWrite.size()];
		for(int i=0; i<buff.length; i++)
			buff[i] = buffWrite.pollLast();
		return buff;
	}

	public String getBuffWriteString() {
		int[] buff = getBuffWrite();
		String buffstr = new String(buff, 0, buff.length);
		return buffstr;
	}
	
	@Override
	public void setInvalid() {
	}
	
}
