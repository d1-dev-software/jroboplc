package promauto.jroboplc.core.api;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import promauto.jroboplc.core.api.Tag;
import promauto.jroboplc.core.TagTable;
import promauto.jroboplc.core.tags.TagRW;


public class SampleTagTable extends TagTable {
	
	public SampleTagTable() {
		
	}

	@Override
	public Tag get(String name) {
		return null;
	}

	@Override
	public Map<String, Tag> getMap() {
		return null;
	}


	@Override
	public Collection<Tag> values() {
		return null;
	}

	@Override
	public List<Tag> getTags(String filter, boolean sorted) {
		return null;
	}


	@Override
	public boolean remove(Tag tag) {
		return false;
	}

	@Override
	public void clear() {
	}

	@Override
	public Tag createTag(Tag.Type tagtype, String name, int flags) {
		return null;
	}

	@Override
	public Tag createTag(Tag.Type tagtype, String name) {
		return null;
	}

	@Override
	public Tag createBool(String name, boolean value, int flags) {
		Tag t = new SampleTag(name);
		t.setBool(value);
		return t;
	}

	@Override
	public Tag createInt(String name, int value, int flags) {
		Tag t = new SampleTag(name);
		t.setInt(value);
		return t;
	}

	@Override
	public Tag createLong(String name, long value, int flags) {
		return null;
	}


	@Override
	public Tag createDouble(String name, double value, int flags) {
		Tag t = new SampleTag(name);
		t.setDouble(value);
		return t;
	}

	@Override
	public Tag createString(String name, String value, int flags) {
		Tag t = new SampleTag(name);
		t.setString(value);
		return t;
	}

	@Override
	public Tag createBool(String name, boolean value) {
		return null;
	}

	@Override
	public Tag createInt(String name, int value) {
		return null;
	}

	@Override
	public Tag createLong(String name, long value) {
		return null;
	}

	@Override
	public Tag createDouble(String name, double value) {
		return null;
	}

	@Override
	public Tag createString(String name, String value) {
		return null;
	}

	@Override
	public TagRW createTagRW(Tag.Type tagtype, String name, int flags) {
		return null;
	}

	@Override
	public TagRW createTagRW(Tag.Type tagtype, String name) {
		return null;
	}

	@Override
	public TagRW createRWBool(String name, boolean value, int flags) {
		return null;
	}

	@Override
	public TagRW createRWInt(String name, int value, int flags) {
		return null;
	}

	@Override
	public TagRW createRWLong(String name, long value, int flags) {
		return null;
	}

	@Override
	public TagRW createRWDouble(String name, double value, int flags) {
		return null;
	}

	@Override
	public TagRW createRWString(String name, String value, int flags) {
		return null;
	}


	@Override
	public TagRW createRWBool(String name, boolean value) {
		return null;
	}

	@Override
	public TagRW createRWInt(String name, int value) {
		return null;
	}

	@Override
	public TagRW createRWLong(String name, long value) {
		return null;
	}

	@Override
	public TagRW createRWDouble(String name, double value) {
		return null;
	}

	@Override
	public TagRW createRWString(String name, String value) {
		return null;
	}



	@Override
	public int getSize() {
		return 0;
	}

	@Override
	public Tag createTag(String tagtype, String name, String value, int flags) {
		return createInt(name, Integer.parseInt(value), flags);
	}

	@Override
	public Tag createTag(String strTagtype, String name, String value) {
		return null;
	}

	@Override
	public <T extends Tag> T add(T tag) {
		return tag;
	}


}
