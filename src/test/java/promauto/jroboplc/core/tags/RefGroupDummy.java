package promauto.jroboplc.core.tags;

import promauto.jroboplc.core.api.Tag;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RefGroupDummy extends RefGroup {

    public LocalDateTime dt;
    public boolean linkOk = true;
    public boolean readOk = true;
    public boolean crcOk = true;

    public Map<String,RefItemDummy> items = new HashMap<>();

//    public static class FactoryDummyImpl implements RefGroup.Factory {
//        @Override
//        public RefGroup createInstance() {
//            return new RefGroupDummy();
//        }
//    }


    @Override
    public void clear() {
    }

    @Override
    public LocalDateTime getDtRead() {
        return dt;
    }

    @Override
    public RefItem createItem(String modulename, String tagname) {
        RefItemDummy item = new RefItemDummy(modulename, tagname);
        items.put(tagname, item);
        return item;
    }

    @Override
    public RefItem createItemCrc(String modulename, String tagname) {
        return createItem(modulename, tagname);
    }

    @Override
    public RefItem createItemCrcSum(String modulename, String tagname) {
        return createItem(modulename, tagname);
    }

    @Override
    public RefItem addItemCrc(RefItem item) {
        return item;
    }

//    @Override
//    public void removeItem(RefItem item) {
//    }

    @Override
    public void prepare() {
    }

    @Override
    public boolean link() {
        return linkOk;
    }

    @Override
    public boolean read() {
        return readOk;
    }

    @Override
    public boolean linkAndRead() {
        return linkOk;
    }

//    @Override
//    public List<RefItem> getItemsNotLinked() {
//        return new ArrayList<RefItem>();
//    }

    @Override
    public boolean checkCrc8() {
        return crcOk;
    }

    @Override
    public boolean checkCrc16() {
        return crcOk;
    }

    @Override
    public boolean checkCrc32() {
        return crcOk;
    }

    @Override
    public void disableCrc() {
    }

    @Override
    public String check() {
        return "";
    }
}
