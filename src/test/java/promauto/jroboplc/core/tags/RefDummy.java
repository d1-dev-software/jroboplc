package promauto.jroboplc.core.tags;

import promauto.jroboplc.core.api.Module;
import promauto.jroboplc.core.api.Tag;

import java.util.Map;

public class RefDummy extends Ref {
    public Tag tag;
    public String modulename;
    public String tagname;
    private boolean linkOk = true;
    private boolean validOk = true;


//    public static class FactoryDummy implements Ref.Factory {
//        @Override
//        public Ref createInstance() {
//            return new RefDummy();
//        }
//    }


    public RefDummy() {
        tag = new TagInt("", 0, 0);
    }

    @Override
    public boolean init(Object conf, String refname, Module module) {
        if( conf != null  &&  conf instanceof Map) {
            Map<String, String> confMap = (Map<String, String>) conf;
            String s = confMap.get(refname);
            String[] ss = s.split(":");
            if( ss.length > 1 ) {
                modulename = ss[0];
                tagname = ss[1];
            } else {
                modulename = module.getName();
                tagname = s;
            }

        }
        return true;
    }

//    @Override
//    public boolean init(String nameModuleTag) {
//        return true;
//    }

    @Override
    public boolean init(String nameLinkModule, String nameLinkTag) {
        this.modulename = nameLinkModule;
        this.tagname = nameLinkTag;
        return true;
    }

    @Override
    public boolean prepare() {
        return true;
    }

    @Override
    public boolean link() {
        return linkOk;
    }

    @Override
    public boolean isValid() {
        return validOk;
    }

    @Override
    public boolean linkIfNotValid() {
        return linkOk;
    }

    @Override
    public String getName() {
        return modulename + ':' + tagname;
    }

    @Override
    public String getRefModuleName() {
        return modulename;
    }

    @Override
    public String getRefTagName() {
        return tagname;
    }

    @Override
    public Tag getTag() {
        return tag;
    }

    @Override
    public boolean getBool() {
        return tag.getBool();
    }

    @Override
    public int getInt() {
        return tag.getInt();
    }

    @Override
    public long getLong() {
        return tag.getLong();
    }

    @Override
    public double getDouble() {
        return tag.getDouble();
    }

    @Override
    public String getString() {
        return tag.getString();
    }

    @Override
    public void setBool(boolean value) {
        tag.setBool(value);
    }

    @Override
    public void setInt(int value) {
        tag.setInt(value);
    }

    @Override
    public void setLong(long value) {
        tag.setLong(value);
    }

    @Override
    public void setDouble(double value) {
        tag.setDouble(value);
    }

    @Override
    public void setString(String value) throws NumberFormatException {
        tag.setString(value);
    }

    @Override
    public String check() {
        return "";
    }
}
