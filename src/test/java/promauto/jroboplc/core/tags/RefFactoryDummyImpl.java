package promauto.jroboplc.core.tags;

public class RefFactoryDummyImpl implements RefFactory {
    @Override
    public Ref createRef() {
        return new RefDummy();
    }

    @Override
    public RefGroup createRefGroup() {
        return new RefGroupDummy();
    }
}
