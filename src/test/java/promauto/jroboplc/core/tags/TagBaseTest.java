package promauto.jroboplc.core.tags;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import promauto.jroboplc.core.api.Flags;


public class TagBaseTest {
	TagInt tag1;
	TagInt tag2;
	TagInt tag3;
	TagInt tag4;

	
	@Before
	public void setUp() throws Exception {
		tag1 = new TagInt("name1", 0);
		tag2 = new TagInt("name2", 0);
		tag3 = new TagInt("name3", 0);
		tag4 = new TagInt("name4", 0);
	}


	@Test
	public void testGetName() {
		assertEquals("name1", tag1.getName());
		assertEquals("name2", tag2.getName());
		assertEquals("name3", tag3.getName());
		assertEquals("name4", tag4.getName());
	}




	@Test
	public void testIsUpdated() {
		tag1 = new TagInt("", 0, Flags.AUTOSAVE | Flags.EXTERNAL | Flags.HIDDEN);
		tag2 = new TagInt("", 0, Flags.HIDDEN | Flags.AUTOSAVE | Flags.EXTERNAL);
		tag3 = new TagInt("", 0, Flags.EXTERNAL | Flags.HIDDEN);

		assertTrue( tag1.hasFlags(Flags.AUTOSAVE));
		assertTrue( tag1.hasFlags(Flags.EXTERNAL));
		assertTrue( tag1.hasFlags(Flags.HIDDEN));

		assertTrue( tag2.hasFlags(Flags.AUTOSAVE));
		assertTrue( tag2.hasFlags(Flags.EXTERNAL));
		assertTrue( tag2.hasFlags(Flags.HIDDEN));

		assertFalse( tag3.hasFlags(Flags.AUTOSAVE));
		assertTrue(  tag3.hasFlags(Flags.EXTERNAL));
		assertTrue(  tag3.hasFlags(Flags.HIDDEN));

		
		assertFalse( tag4.hasFlags(Flags.HIDDEN));
		assertFalse( tag4.hasFlags(Flags.EXTERNAL));
		assertFalse( tag4.hasFlags(Flags.AUTOSAVE));

		tag4.addFlag(Flags.EXTERNAL);
		assertFalse( tag4.hasFlags(Flags.HIDDEN));
		assertTrue(  tag4.hasFlags(Flags.EXTERNAL));
		assertFalse( tag4.hasFlags(Flags.AUTOSAVE));

		tag4.addFlag(Flags.AUTOSAVE);
		assertFalse( tag4.hasFlags(Flags.HIDDEN));
		assertTrue(  tag4.hasFlags(Flags.EXTERNAL));
		assertTrue(  tag4.hasFlags(Flags.AUTOSAVE));

		tag4.addFlag(Flags.HIDDEN);
		assertTrue(  tag4.hasFlags(Flags.HIDDEN));
		assertTrue(  tag4.hasFlags(Flags.EXTERNAL));
		assertTrue(  tag4.hasFlags(Flags.AUTOSAVE));
	}
}
