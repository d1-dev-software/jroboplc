package promauto.jroboplc.core.tags;

import promauto.jroboplc.core.api.Tag;

import java.util.Map;

public class RefItemDummy extends RefItem {

    public Tag tag;
    public String modulename;
    public String tagname;

    public RefItemDummy(Tag.Type ttype) {
        super(null, null, null);
        tag = TagBase.create(ttype, "", 0);
    }

    public RefItemDummy(String modulename, String tagname) {
        super(null, null, null);
        tag = TagBase.create(Tag.Type.INT, tagname, 0);
        this.modulename = modulename;
        this.tagname = tagname;
    }

    @Override
    public Tag getTag() {
        return tag;
    }

    @Override
    public Tag getValue() {
        return tag;
    }

    public void changeTagType(Tag.Type ttype) {
        tag = TagBase.create(ttype, tag.getName(), 0);
    }
    public void changeTagTypeLong() {
        changeTagType(Tag.Type.LONG);
    }

    public void changeTagTypeBool() {
        changeTagType(Tag.Type.BOOL);
    }
}
