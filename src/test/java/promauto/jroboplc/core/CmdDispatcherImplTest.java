package promauto.jroboplc.core;

import static org.junit.Assert.assertEquals;

import java.nio.file.Path;

import org.junit.Before;
import org.junit.Test;

import promauto.jroboplc.core.api.InitUtils;
import promauto.jroboplc.core.api.SampleCmdCommon;
import promauto.jroboplc.core.api.SampleCmdModule;
import promauto.jroboplc.core.api.SampleCmdPlugin;
import promauto.jroboplc.core.api.SampleEnvironment;
import promauto.jroboplc.core.api.SamplePlugin;
import promauto.jroboplc.core.api.CmdDispatcher;
import promauto.jroboplc.core.api.Environment;
import promauto.jroboplc.core.api.EnvironmentInst;
import promauto.jroboplc.core.api.Module;
import promauto.jroboplc.core.api.Plugin;
import promauto.jroboplc.core.cmd.CmdDispatcherImpl;


public class CmdDispatcherImplTest {
	private static final String NO_DOUBLE_COMMANDS = "No double commands!";
	private static final String BAD_COMMAND_RESULT = "Bad command result";
	
	Environment env;
	CmdDispatcher dsp;
	Plugin plg;
	Module mdl;

	@Before
	public void setup() {
		InitUtils.setupLogger();
		env = new SampleEnvironment();
		EnvironmentInst.set(env);

		dsp = new CmdDispatcherImpl();
		plg = new SamplePlugin();
		Path p = null;
		mdl = plg.createModule("simmod", null);

		dsp.addCommand(SampleCmdCommon.class);
		dsp.addCommand(plg, SampleCmdPlugin.class);
		dsp.addCommand(mdl, SampleCmdModule.class);
	}
	
	
	
	@Test
	public void testExecute() {
		assertEquals(BAD_COMMAND_RESULT,
				"common command result cmdcom p1 p2", 
				dsp.execute(null, "cmdcom p1 p2"));
		
		assertEquals(BAD_COMMAND_RESULT,
				"plugin command result cmdplug p3 p4", 
				dsp.execute(null, "simplug:cmdplug p3 p4"));
		
		assertEquals(BAD_COMMAND_RESULT,
				"module command result cmdmod prm7 prm8", 
				dsp.execute(null, "simmod:cmdmod prm7 prm8"));
		
		assertEquals(BAD_COMMAND_RESULT,
				"Unknown command", 
				dsp.execute(null, "cmdcop p1 p2"));

		
	}
	@Test
	public void testAddCommand() {
		dsp.getCommands().clear();
		
		assertEquals("Command map must empty", 0, dsp.getCommands().size() );
		
		dsp.addCommand(SampleCmdCommon.class);
		assertEquals("Number of commads must be 1", 1, dsp.getCommands().size() );
		
		dsp.addCommand(plg, SampleCmdPlugin.class);
		dsp.addCommand(plg, SampleCmdPlugin.class);
		assertEquals(NO_DOUBLE_COMMANDS, 2, dsp.getCommands().size() );
		
		dsp.addCommand(SampleCmdCommon.class);
		dsp.addCommand(mdl, SampleCmdModule.class);
		dsp.addCommand(mdl, SampleCmdModule.class);
		assertEquals(NO_DOUBLE_COMMANDS, 3, dsp.getCommands().size() );
	}
	
}



