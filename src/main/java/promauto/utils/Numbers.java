package promauto.utils;

public class Numbers {

	public static final int[] hexDigit = 
		{'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};

	public static final char[] hexDigitChar =
		{'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};

	public static int bytesToInt(int[] data, int index) {
		return (data[index] << 24) | (data[index+1] << 16) | (data[index+2] << 8) | data[index+3];
	}

	public static int bytesToIntReverse(int[] data, int index) {
		return (data[index+3] << 24) | (data[index+2] << 16) | (data[index+1] << 8) | data[index];
	}



	/**
	 * @deprecated Replaced by {@linkplain #bytesToDWord(int[], int)}
	 */
	@Deprecated public static long bytesToUInt(int[] data, int index) {
		return
				(((long)data[index]) << 24) |
                (((long)data[index+1]) << 16) |
                (((long)data[index+2]) << 8) |
                ((long)data[index+3]);
	}


	public static int bytesToWord(int[] data, int index) {
		return (data[index] << 8) | data[index+1];
	}

	public static long bytesToDWord(int[] data, int index) {
		return
				((long)(data[index]) << 24) |
				(data[index+1] << 16) |
				(data[index+2] << 8) |
				data[index+3];
	}

	public static int bytesToWord(byte[] data, int index) {
		return ((data[index] & 0xff) << 8) | (data[index+1] & 0xff);
	}



	public static int asciiToInt(int[] src, int beg, int end){
		int result = 0;
		for(int i=beg; i<end; ++i)
			result = result * 10 + asciiDigitToInt( src[i] );
		return result;
	}


	public static long wordsToLong(int high, int low) {
		return (high << 16) | low;
	}

	
	public static int asciiDigitToInt(int digit) {
		if( digit > 48  &&  digit < 58 )
			return digit - 48;
		
		if( digit > 64  &&  digit < 71 )
			return digit - 55;
		
		if( digit > 96  &&  digit < 103 )
			return digit - 87;
		
		return 0;
	}

	public static int asciiByteToInt(int[] data, int idx) {
		return 
				(asciiDigitToInt(data[idx]) << 4) + 
				 asciiDigitToInt(data[idx+1]);
	}

	public static int asciiWordToInt(int[] data, int idx) {
		return
				(asciiDigitToInt(data[idx])   << 12) +
				(asciiDigitToInt(data[idx+1]) << 8) +
				(asciiDigitToInt(data[idx+2]) << 4) +
			 	 asciiDigitToInt(data[idx+3]);
	}

	public static long asciiDWordToLong(int[] data, int idx) {
		return
				(asciiDigitToInt(data[idx])   << 28) +
				(asciiDigitToInt(data[idx+1]) << 24) +
   				(asciiDigitToInt(data[idx+2]) << 20) +
				(asciiDigitToInt(data[idx+3]) << 16) +
				(asciiDigitToInt(data[idx+4]) << 12) +
				(asciiDigitToInt(data[idx+5]) << 8) +
				(asciiDigitToInt(data[idx+6]) << 4) +
				 asciiDigitToInt(data[idx+7]);
	}


	public static void intToAsciiByte(int value, int[] dst, int idx) {
		dst[idx] = hexDigitChar[(value >> 4) & 0xF];
		dst[idx+1] = hexDigitChar[value & 0xF];
	}

	public static void intToAsciiWord(int value, int[] dst, int idx) {
		intToAsciiByte((value >> 8) & 0xFF, dst, idx);
		intToAsciiByte(value & 0xFF, dst, idx + 2);
	}

	public static void longToAsciiDWord(long value, int[] dst, int idx) {
		intToAsciiWord((int)((value >> 16) & 0xFFFF), dst, idx);
		intToAsciiWord((int)(value & 0xFFFF), dst, idx + 4);
	}

	public static int hexToInt(int[] data, int idx, int len) {
		int v = 0;
		while( true ) {
			v += asciiDigitToInt(data[idx++]);
			if( --len > 0 )
				v <<= 4;
			else
				break;
		}
		return v;
	}
	
	public static int hexToInt(byte[] data, int idx, int len) {
		int v = 0;
		while( true ) {
			v += asciiDigitToInt(data[idx++] & 0xFF);
			if( --len > 0 )
				v <<= 4;
			else
				break;
		}
		return v;
	}
	
	public static long hexToLong(byte[] data, int idx, int len) {
		long v = 0;
		while( true ) {
			v += asciiDigitToInt(data[idx++] & 0xFF);
			if( --len > 0 )
				v <<= 4;
			else
				break;
		}
		return v;
	}


	public static void wordToBytes(int value, int[] data, int idx) {
		data[idx] = (value>>8) & 0xFF;
		data[idx+1] = value & 0xFF;
	}

	public static void intToBytes(int value, int[] data, int idx) {
		data[idx] = (value>>24) & 0xFF;
		data[idx+1] = (value>>16) & 0xFF;
		data[idx+2] = (value>>8) & 0xFF;
		data[idx+3] = value & 0xFF;
	}

	public static void intToBytesReverse(int value, int[] data, int idx) {
		data[idx+3] = (value>>24) & 0xFF;
		data[idx+2] = (value>>16) & 0xFF;
		data[idx+1] = (value>>8) & 0xFF;
		data[idx] = value & 0xFF;
	}

	public static void dwordToBytes(long value, int[] data, int idx) {
		data[idx] = (int)((value>>24) & 0xFF);
		data[idx+1] = (int)((value>>16) & 0xFF);
		data[idx+2] = (int)((value>>8) & 0xFF);
		data[idx+3] = (int)(value & 0xFF);
	}

	public static void intToHexByte(int value, int[] data, int idx) {
		data[idx] = Numbers.hexDigit[(value>>4) & 0xF];
		data[idx+1] = Numbers.hexDigit[value & 0xF];
	}
	
	public static void intToHexWord(int value, int[] data, int idx) {
		data[idx+3] = Numbers.hexDigit[value & 0xF];
		data[idx+2] = Numbers.hexDigit[(value>>=4) & 0xF];
		data[idx+1] = Numbers.hexDigit[(value>>=4) & 0xF];
		data[idx]   = Numbers.hexDigit[(value>>=4) & 0xF];
	}
	
	
    public static int intToHex(int val, int[] buff, int offset) {
        int mag = Integer.SIZE - Integer.numberOfLeadingZeros(val);
        int len = Math.max(((mag + 3) / 4), 1);

        int last = len + offset;
        int charPos = last;
        do {
            buff[--charPos] = hexDigit[val & 0xF];
            val >>>= 4;
        } while (val != 0 && charPos > 0);

        return last;
    }

	
    
    public static char[] toHexChars(int val) {
        int mag = Integer.SIZE - Integer.numberOfLeadingZeros(val);
        int len = Math.max(((mag + 3) / 4), 1);
        char[] buf = new char[len];

        int charPos = len;
        do {
            buf[--charPos] = hexDigitChar[val & 0xF];
            val >>>= 4;
        } while (val != 0 && charPos > 0);

        return buf;
    }

    public static String toHexString(int val) {
        return new String( toHexChars(val) );
    }

    
    public static char[] toHexChars(long val, int size) {
        char[] buf = new char[size];
        do {
            buf[--size] = hexDigitChar[(int)(val & 0xF)];
            val >>>= 4;
        } while (size > 0);
        return buf;
    }

    public static String toHexString(long val, int size) {
        return new String( toHexChars(val, size) );
    }

    
	// ignores the higher 16 bits
	public static float decodeFloat16(int code) {
		int mant = code & 0x03ff;
		int exp = code & 0x7c00;
		if (exp == 0x7c00)
			exp = 0x3fc00;
		else if (exp != 0) {
			exp += 0x1c000;
			if (mant == 0 && exp > 0x1c400)
				return Float.intBitsToFloat((code & 0x8000) << 16 | exp << 13 | 0x3ff);
		} else if (mant != 0) {
			exp = 0x1c400;
			do {
				mant <<= 1;
				exp -= 0x400;
			} while ((mant & 0x400) == 0);
			mant &= 0x3ff;
		}
		return Float.intBitsToFloat((code & 0x8000) << 16 | (exp | mant) << 13);
	}

	// returns all higher 16 bits as 0 for all results
	public static int encodeFloat16(float value) {
		int fbits = Float.floatToIntBits(value);
		int sign = fbits >>> 16 & 0x8000;
		int val = (fbits & 0x7fffffff) + 0x1000;

		if (val >= 0x47800000) {
			if ((fbits & 0x7fffffff) >= 0x47800000) {
				if (val < 0x7f800000)
					return sign | 0x7c00;
				return sign | 0x7c00 | (fbits & 0x007fffff) >>> 13;
			}
			return sign | 0x7bff;
		}
		if (val >= 0x38800000)
			return sign | val - 0x38000000 >>> 13;
		if (val < 0x33000000)
			return sign;
		val = (fbits & 0x7fffffff) >>> 23;
		return sign | ((fbits & 0x7fffff | 0x800000) + (0x800000 >>> val - 102) >>> 126 - val);
	}

	
	public static float decodeFloat32(int code) {
		return Float.intBitsToFloat(code);
	}

	public static int encodeFloat32(float value) {
		return Float.floatToIntBits(value);
	}

	public static double decodeDouble64(long code) {
		return Double.longBitsToDouble(code);
	}

	public static long encodeDouble64(double value) {
		return Double.doubleToLongBits(value);
	}


	public static int limit(int value, int valmin, int valmax) {
		return Math.min( Math.max(value, valmin), valmax);
	}

    public static boolean compareIntArrays(int[] a, int[] b, int size) {
		for(int i = 0; i < size; ++i)
			if( a[i] != b[i] )
				return false;
		return true;
    }
}
