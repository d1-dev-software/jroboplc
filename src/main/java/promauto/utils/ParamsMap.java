package promauto.utils;

import java.util.Arrays;
import java.util.HashMap;
import java.util.regex.Pattern;

public class ParamsMap extends HashMap<String,String> {
	
	private static final long serialVersionUID = 1L;
	private static final Pattern p = Pattern.compile("\\s*(\\w+)\\s*=\\s*(\\w*)\\s*");

	public ParamsMap(String... params) {
		Arrays.stream(params)
			.map(param -> p.matcher(param))
			.filter(matcher -> matcher.find() )
			.forEach(matcher -> put(matcher.group(1), matcher.group(2)) );
	}
	
			
	

}
