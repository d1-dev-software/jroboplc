package promauto.jroboplc.core;

import promauto.jroboplc.core.api.TcpServer;
import promauto.jroboplc.core.api.TcpServerChannel;
import promauto.jroboplc.core.api.TcpServerPort;
import promauto.jroboplc.core.api.TcpSubscriber;

import java.util.Collection;

public class TcpServerStub implements TcpServer {

    @Override
    public TcpServerPort getPortByNum(int portnum) {
        return null;
    }

    @Override
    public TcpServerPort getPortByTcp(int porttcp) {
        return null;
    }

    @Override
    public TcpServerPort getPortBySubscriber(TcpSubscriber subscriber) {
        return null;
    }

    @Override
    public boolean subscribe(int portnum, TcpSubscriber subscriber) {
        return false;
    }

    @Override
    public boolean disconnect(int portnum) {
        return false;
    }

}
