package promauto.jroboplc.core;

import promauto.utils.CRC;
import promauto.utils.Numbers;
import promauto.utils.Strings;

import java.io.File;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.SecureRandom;
import java.sql.*;
import java.time.LocalDateTime;
import java.util.*;

public class Probe {
    public static void main(String[] args) {
//        int mul = 10;
        double mul = 10;
        long sum = 0;
        long t = System.currentTimeMillis();
        for(int i=0; i<1000000000; ++i) {
            sum += Math.round( (double)i / mul );
//            sum += Math.round( i / mul );
        }
        t = System.currentTimeMillis() - t;
        System.out.println(sum);
        System.out.println(t);

    }

    private static String generateFakeSN() {
        byte[] nonce = new byte[8];
        new SecureRandom().nextBytes(nonce);
        String sn = Strings.bytesToHexString(nonce, "");
        int crc = CRC.getCrc16(sn.getBytes());
        return sn + Numbers.toHexString(crc);
    }

    private static boolean checkFakeSn(String sn) {
        if(sn.length() != 20)
            return false;

        try {
            int crc = CRC.getCrc16(sn.substring(0, 16).getBytes());
            System.out.println(crc);
            if( crc != Integer.parseInt(sn.substring(16), 16) )
                return false;
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }

/*
    private static String generateFakeSN() {
        byte[] nonce = new byte[8];
        new SecureRandom().nextBytes(nonce);
        String sn = Strings.bytesToHexString(nonce, "");
        CRC32 crc = new CRC32();
        crc.update(sn.getBytes());
        return sn + Numbers.toHexString(crc.getValue(), 8);
    }

    private static boolean checkFakeSn(String sn) {
        if(sn.length() != 24)
            return false;

        try {
            CRC32 crc = new CRC32();
            crc.update(sn.substring(0, 16).getBytes());
//            System.out.println(sn.substring(0, 16));
//            System.out.println(sn.substring(16));
//            System.out.println(crc.getValue());
//            System.out.println(Long.parseLong(sn.substring(16), 16));
            if( crc.getValue() != Long.parseLong(sn.substring(16), 16) )
                return false;
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }
*/

    public static void main1(String[] args) {
        try {
            Class.forName("org.firebirdsql.jdbc.FBDriver");

            Properties databaseProperties = new Properties();
            databaseProperties.put("soTimeout", "" + (10000) );
            databaseProperties.put("user", "SYSDBA");
            databaseProperties.put("password", "masterkey");
            databaseProperties.put("encoding", "WIN1251");

            String url = "jdbc:firebirdsql://localhost:3050/uvrdrcp";

            Connection connection = DriverManager.getConnection(url, databaseProperties);
            connection.setAutoCommit(false);


            for(int i=0; i<1000; ++i) {
                try (Statement st = connection.createStatement()) {
                    String sql = "update rd_feeder set bunker_id=2 where id=1";
                    st.executeUpdate(sql);
                    st.close();
                    connection.commit();
                }
            }

////                statement.executeUpdate(
////                        "insert into person(firstname, lastname, birthdate) "
////                                + "values ('Mark1', 'Rotteveel', date'1979-01-12')",
////                        Statement.RETURN_GENERATED_KEYS);
////
////                try (ResultSet keys = statement.getGeneratedKeys()) {
////                    if (keys.next()) {
////                        int generatedId = keys.getInt("id");
////                        int age = keys.getInt("age");
////                        String firstName = keys.getString("firstname");
////
////                        System.out.printf("Inserted: %s, Id: %d, Age: %d%n",
////                                firstName, generatedId, age);
////                    }
////                }
///
//                statement.executeUpdate(
//                        "update or insert into rd_user (name, psw) values ('user3', 'aaa') matching (name)",
////                        Statement.RETURN_GENERATED_KEYS);
//                        new String[]{"id"});
//
//                try (ResultSet keys = statement.getGeneratedKeys()) {
//                    if (keys.next()) {
//                        int generatedId = keys.getInt(1);
//                        String name = ""; //keys.getString("name");
//
//                        System.out.printf("Inserted: %s, Id: %d %n", name, generatedId);
//                    }
//                }
//
//                connection.commit();
//
//
//            }


            connection.close();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }

    }

}