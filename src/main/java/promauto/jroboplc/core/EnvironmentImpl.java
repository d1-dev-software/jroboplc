package promauto.jroboplc.core;

import static javax.xml.stream.XMLInputFactory.IS_COALESCING;
import static javax.xml.stream.XMLInputFactory.IS_REPLACING_ENTITY_REFERENCES;
import static javax.xml.stream.XMLInputFactory.IS_SUPPORTING_EXTERNAL_ENTITIES;

import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLInputFactory;

import org.slf4j.Logger;

import promauto.jroboplc.core.api.*;
import promauto.jroboplc.core.tags.Ref;
import promauto.jroboplc.core.tags.RefFactory;
import promauto.jroboplc.core.tags.RefFactoryImpl;
import promauto.jroboplc.core.tags.RefGroup;
import promauto.utils.Strings;

public class EnvironmentImpl implements Environment {

	private LoggerMode loggerMode = null;
	private Configuration configuration = null;
	private CmdDispatcher cmdDispatcher = null;
	private ModuleManager moduleManager = null;
	private TaskManager taskManager = null;
	private ConsoleManager consoleManager = null;
	private Console console = null;
	private SerialManager serialManager = null;
	private TcpServer tcpserver = null;
	private volatile boolean terminated = false;
	private volatile boolean running = false;

	private XMLInputFactory xmlif = null;

	private KeyManager keyManager = null;

	private RefFactory refFactory = null;


	public EnvironmentImpl() {}


	@Override
	public LoggerMode getLoggerMode() {
		return loggerMode;
	}

	public void setLoggerMode(LoggerMode loggerMode) {
		this.loggerMode = loggerMode;
	}


	@Override
	public Configuration getConfiguration() {
		return configuration;
	}

	@Override
	public void setConfiguration(Configuration configuration) {
		this.configuration = configuration;
	}

	@Override
	public KeyManager getKeyManager()  {
		if (keyManager == null) {
			synchronized (this) {
				if (keyManager == null)
					keyManager = new KeyManagerImpl();
			}
		}
		return keyManager;
	}


	@Override
	public CmdDispatcher getCmdDispatcher() {
		return cmdDispatcher;
	}

	@Override
	public void setCmdDispatcher(CmdDispatcher cmdDispatcher) {
		this.cmdDispatcher = cmdDispatcher;
	}


	@Override
	public ModuleManager getModuleManager() {
		return moduleManager;
	}

	@Override
	public void setModuleManager(ModuleManager moduleManager) {
		this.moduleManager = moduleManager;
	}


	@Override
	public ConsoleManager getConsoleManager() {
		return consoleManager;
	}


	@Override
	public void setConsoleManager(ConsoleManager consoleManager) {
		this.consoleManager = consoleManager;
	}

	@Override
	public Console getConsole() {
		return console;
	}

	@Override
	public void setConsole(Console console) {
		if( this.console != null  &&  consoleManager != null )
			consoleManager.removeConsole(this.console);
			
		if( consoleManager != null )
			consoleManager.addConsole(console);
			
		this.console = console;
	}

	@Override
	public TcpServer getTcpServer() {
		return tcpserver;
	}

	@Override
	public SerialManager getSerialManager() {
		return serialManager;
	}

	@Override
	public void setSerialManager(SerialManager serialManager) {
		this.serialManager = serialManager;
	}

	@Override
	public void setTcpServer(TcpServer tcpserver) {
		this.tcpserver = tcpserver;
	}

	@Override
	public boolean isTerminated() {
		return terminated;
	}

	@Override
	public void setTerminated() {
		terminated = true;
	}

	@Override
	public boolean isRunning() {
		return running;
	}

	@Override
	public void setRunning(boolean status) {
		running = status;
	}

	@Override
	public TaskManager getTaskManager() {
		return taskManager;
	}

	@Override
	public void setTaskManager(TaskManager taskManager) {
		this.taskManager = taskManager;
	}

	@Override
	public XMLInputFactory getXMLInputFactory() throws FactoryConfigurationError {
		if (xmlif == null) {
			xmlif = XMLInputFactory.newInstance();
			xmlif.setProperty(IS_REPLACING_ENTITY_REFERENCES, true);
			xmlif.setProperty(IS_SUPPORTING_EXTERNAL_ENTITIES, true);
			xmlif.setProperty(IS_COALESCING, false);
		}
		return xmlif;
	}

	@Override
	public RefFactory getRefFactory() {
		if( refFactory == null )
			refFactory = new RefFactoryImpl();
		return refFactory;
	}


	@Override
	public void printError(Logger logger, String... text) {
		printError(logger, null, text);
	}

	@Override
	public void printError(Logger logger, Throwable e, String... text) {
		String s = String.join(" ", text);
		print(ANSI.RED + "\r\n[ERROR] "
				+ (logger == null? "": logger.getName() + ": ")
				+ s + (e == null ? "" : "\r\n" + e.toString()) + "\r\n" + ANSI.RESET);
		logError(logger, e, s);
	}

    @Override
	public void logError(Logger logger, String... text) {
		logError(logger, null, text);
	}

    @Override
	public void logError(Logger logger, Throwable e, String... text) {
		if( logger != null) {
			String s = Strings.removeAnsiColors(String.join(" ", text));
			if (e == null)
				logger.error(s);
			else
				logger.error(s, e);

			logger.info("[ERROR] "
					+ (logger == null? "": logger.getName() + ": ")
					+ s + (e == null ? "" : "\r\n" + e.toString() + "\r\n"));
		}
	}



	@Override
	public void printInfo(Logger logger, String... text) {
		String s = String.join(" ", text);
		print(ANSI.BLUE + "\r\n[INFO] " + s + "\r\n" + ANSI.RESET);
		logInfo(logger, s);
	}

    @Override
	public void logInfo(Logger logger, String... text) {
		if( logger != null) {
			logger.info("[INFO] " + Strings.removeAnsiColors(String.join(" ", text)));
		}
	}


    @Override
	public void print(String text) {
		if (consoleManager != null)
			consoleManager.printToAll(text);
		else
            System.out.println(text);
    }

    @Override
    public void logStatus(Logger logger, String... text) {
        if( logger != null) {
            logger.info("[STATUS] " + Strings.removeAnsiColors(String.join(" ", text)));
        }
    }


}
