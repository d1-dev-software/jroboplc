package promauto.jroboplc.core;

import java.util.HashMap;
import java.util.Map;

import promauto.jroboplc.core.api.ANSI;
import promauto.jroboplc.core.api.Console;
import promauto.jroboplc.core.api.Environment;
import promauto.jroboplc.core.api.EnvironmentInst;
import promauto.utils.Strings;

public abstract class AbstractConsole implements Console {
	
	protected Environment env;
	protected Map<String,String> props = new HashMap<>();

	private String lastRequest = "";
	private volatile boolean repeatingLastRequest = false;
	private Thread threadRepeat = null;


	public AbstractConsole(Environment envinronment) {
		this.env = envinronment;
	}

	@Override
	public String getPrompt() {
		if (env.isRunning())
			return "run>";
		else
			return "idle>";
	}


	@Override
	public String getProperty(String property_id, String default_value) {
		return props.getOrDefault(property_id, default_value);
	}


	@Override
	public void setProperty(String property_id, String value) {
		props.put(property_id, value);
	}


	@Override
	public int getProperty(String property_id, int default_value) {
		try {
			if( props.containsKey(property_id) )
				return Integer.parseInt( props.get(property_id) );
		} catch (NumberFormatException e) {
		}
		
		return default_value;
	}

	@Override
	public String parseAnsi(String text) {
		if( getProperty("ansi",PROP_OFF).equals(PROP_ON) )
			return text;
		
		return Strings.removeAnsiColors(text);
	}



	protected void afterRequestExecuted(String request) {
		if( repeatingLastRequest  &&  request.isEmpty() ) {
			stopRepeatingLastRequest();
		}

		if( !repeatingLastRequest  &&  !request.isEmpty() ) {
			lastRequest = request;
		}
	}


	@Override
	public synchronized String startRepeatingLastRequest(long updatePeriod) {
		if( repeatingLastRequest )
			return "";

		if( lastRequest.isEmpty() )
			return "Nothing to repeat\r\n";

		repeatingLastRequest = true;

		final long period = updatePeriod <= 0? 1000: updatePeriod;

		// start thread
		Runnable task = () -> {
			int i = 0;
			while ( repeatingLastRequest  &&  EnvironmentInst.get().isRunning() ) {

				String s = ANSI.CLEAR + ANSI.HOME +
						env.getCmdDispatcher().execute( this, lastRequest) +
						"\r\nPress ENTER to return";
				print(s);

				try {
					Thread.sleep(period);
				} catch (InterruptedException e) {
				}
			}
		};
		threadRepeat = new Thread(task);
		threadRepeat.start();

		return "";
	}


	@Override
	public synchronized void stopRepeatingLastRequest() {
		if( !repeatingLastRequest )
			return;

		repeatingLastRequest = false;
		threadRepeat.interrupt();
	}
}
