package promauto.jroboplc.core;

import static java.nio.charset.Charset.defaultCharset;

import java.io.*;
import java.net.URLClassLoader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import promauto.jroboplc.core.api.*;


public class ModuleManagerImpl implements ModuleManager {
	final Logger logger = LoggerFactory.getLogger(ModuleManagerImpl.class);
	
	private final Environment env;
	private final Map<String,Plugin> plugins = new HashMap<>();
	private final Map<String,Module> modules = new HashMap<>();
	private List<Module> extmodules = null;
	private int lastStatusTextLen = 0;

	
	public ModuleManagerImpl() {
		this.env = EnvironmentInst.get();
	}


	public void addPlugins(Plugin... plugins) {
		Arrays.stream(plugins)
				.forEach(plugin -> this.plugins.put(plugin.getPluginName(), plugin));
	}
	
	
	private void printStatus(String text) {
		if( lastStatusTextLen > 0 )
			env.getConsole().print(String.format("%-"+ lastStatusTextLen +"s\r", " ") );
		lastStatusTextLen  = text.length();
		env.getConsole().print(text + "\r");
		
	}


	@Override
	public List<Plugin> getPlugins() {
		// todo remove method
		return null;
	}


	@Override
	public Set<Module> getModules() {
		Set<Module> mdlset = new TreeSet<>(Comparator.comparing(Module::getName));
		mdlset.addAll( modules.values() );
		return mdlset;
	}


	@Override
	public List<Module> getModulesWithExternalTags() {
		if( extmodules == null ) {
//			extmodules = EnvironmentInst.get().getModuleManager().getModules().stream()
			extmodules = modules.values().stream()
                .filter(Module::canHaveExternalTags)
                .collect(Collectors.toList());
		}
		return extmodules;
	}

	@Override
	public Map<String, Tag> getTagsAll() {
		Map<String, Tag> tags = new HashMap<>();
		for(Module m: modules.values()) {
			for(Tag tag: m.getTagTable().values()) {
				if( tag.getStatus() != Tag.Status.Deleted ) {
					if (tag.hasFlags(Flags.EXTERNAL))
						tags.put(tag.getName(), tag);
					else
						tags.put(m.getName() + '.' + tag.getName(), tag);
				}
			}
		}
		return tags;
	}

	@Override
	public Module getModule(String name) {
		return modules.get(name);
	}


	@Override
	public <T> T getModule(String name, Class<? extends T> cls) {
		Module module = modules.get(name);
		return cls.isInstance(module)? cls.cast(module): null;
	}


	@Override
	public Tag searchTag(String name) {
		Tag tag;
		for(Module module: modules.values())
			if( (tag = module.getTagTable().get(name)) != null )
				return tag;
		return null;
	}


	@Override
	public Tag searchExternalTag(String name) {
		Tag tag;
		for(Module module: getModulesWithExternalTags())
			if( (tag = module.getTagTable().get(name)) != null  &&  tag.hasFlags(Flags.EXTERNAL) )
				return tag;
		return null;
	}


	@Override
	public boolean loadModules() {
		AtomicBoolean result = new AtomicBoolean(true);
		Configuration cm = env.getConfiguration();
		cm.getRoot().forEach((pkey, pconf) -> {
			if(pkey.startsWith("plugin.")  &&  pconf != null  &&  cm.get(pconf, "enable", true)) {
				String pluginName = pkey.substring(7);
				cm.toMap(pconf).forEach((mkey, mconf) -> {
					if(mkey.startsWith("module.")  &&  mconf != null) {
						String moduleName = mkey.substring(7);
						result.compareAndSet(true, appendModule(pluginName, moduleName));
					}
				});
			}
		});
		printStatus("");
		return result.get();
	}
	

	@Override
	public boolean appendModule(String plgname, String modname) {

		Optional<Plugin> plugin = getOrLoadPlugin(plgname);
		if( !plugin.isPresent() ) {
			env.printError(logger,"Plugin not found: ", plgname);
			return false;
		}

		Module module = getModule(modname);
		if( module != null ) {
			env.printError(logger, "Module already exists: ", modname);
			return false;
		}

		Configuration cm = env.getConfiguration();
		Object conf;
		if( (conf = cm.getModuleConf(plgname, modname)) == null )
			return false;

		module = plugin.get().createModule(modname, conf);
		if( module == null )
			return false;
		
		if( env.isRunning() ) {
			if( !module.prepare() ) {
				module.closedown();
				return false;
			}
			
			if( module instanceof Task )
				module.execute();
		}
		
		modules.put(modname, module);
		extmodules = null;
		printStatus("  - module: " + modname);
		
		return true;
	}


	private Optional<Plugin> getOrLoadPlugin(String plgname) {
		Plugin plugin = plugins.get(plgname);
		if( plugin != null )
			return Optional.of(plugin);

		Configuration cm = env.getConfiguration();
		String pluginClassName = (String)(cm.toMap(cm.get(cm.getRoot(), "plugin")).get(plgname));
		if( pluginClassName == null )
			return Optional.empty();

		try {
			Class<?> cls = Class.forName(pluginClassName);
			plugin = (Plugin) cls.newInstance();
			plugin.initialize();
			plugins.put(plgname, plugin);
			printStatus("...plugin " + plugin.getPluginName());
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
			env.printError(logger, e, "Failed to load plugin: " + plgname);
			return Optional.empty();
		}

		return Optional.of(plugin);
	}


	@Override
	public boolean saveState(Set<Module> modules, Path path) {
		if (!path.isAbsolute())
			path = EnvironmentInst.get().getConfiguration().getConfDir().resolve(path);
		
		try (BufferedWriter writer = Files.newBufferedWriter(path, defaultCharset())) {
			
			for(Module module: EnvironmentInst.get().getModuleManager().getModules()) { 
				if( !modules.isEmpty()  &&  !modules.contains(module) )
					continue;

				State state = new State();
				module.saveState(state);
				if (state.size() == 0)
					continue;

				writer.write(module.getPlugin().getPluginName() + "." + module.getName() + ":\r\n");

				for(Map.Entry<String,String> ent: state.entrySet() ) {
					writer.write( " " + ent.getKey() + ": \""
							+ ent.getValue().replace("\"", "\\\"") + "\"\r\n");
				}
				writer.write("\r\n");
			}
			
		} catch (IOException e) {
			EnvironmentInst.get().printError(logger, e);
			return false;
		}
		return true;
	}


	@Override
	public boolean loadState(Set<Module> modules, Path path) {
		if (!path.isAbsolute())
			path = EnvironmentInst.get().getConfiguration().getConfDir().resolve(path);
		
		Map<Object,Object> save = ConfigurationYaml.loadYamlFileAsMap(path);
		if( save == null )
			return false;
		
		for(Object obj: save.keySet()) {
			String plgmod = obj.toString();
			int k = plgmod.indexOf('.');
			String plgname = plgmod.substring(0, k);
			String modname = plgmod.substring(k+1);
			
			Module m = getModule(modname);
			if( m == null ) 
				continue;
			
			if( !m.getPlugin().getPluginName().equals( plgname ) )
				continue;
				
			State state = new State();
			Map<String,String> savemap = (Map<String,String>)save.get(obj);
			state.putAll(savemap);

			if( state.size() == 0)
				continue;;

			m.loadState(state);
			env.printInfo(logger, modname, "Successfully imported state");
		}
		return true;
	}

}

