package promauto.jroboplc.core;

import promauto.jroboplc.core.api.Database;
import promauto.jroboplc.core.api.DatabaseProtoService;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

public class DatabaseProtoServiceImpl implements DatabaseProtoService {

    protected Database db;

    protected  Statement st;
    protected  String sql;

    protected final Map<Integer,HashSet<Integer>> sync = new HashMap<>();


    public void setDb(Database db) {
        this.db = db;
    }

    public Database getDb() {
        return db;
    }

    public Statement createStatement() throws SQLException {
        if( db == null )
            throw new RuntimeException("Database is not set!");

        st = db.getConnection().createStatement();
        return st;
    }

    public void commit() throws SQLException {
        if( st != null )
            st.close();
        st = null;
        if( db != null )
            db.commit();
    }

    public void rollback()  {
        try {
            if (st != null)
                st.close();
        } catch (SQLException e) {
        }
        st = null;
        if( db != null )
            db.rollback();
    }

    protected void delete(String table, String field, int id) throws SQLException {
        sql = String.format(
                "delete from %s where %s=%d",
                table,
                field,
                id
        );
        st.executeUpdate(sql);
    }

    public String getLastSql() {
        return sql;
    }

    protected int executeAndGetId(String sql) throws SQLException {
        this.sql = sql;
        st.executeUpdate(sql, new String[]{"id"});
        try (ResultSet keys = st.getGeneratedKeys()) {
            if (keys.next())
                return keys.getInt(1);
        }
        return 0;
    }


    protected void startSyncTable(Integer syncType, String table) throws SQLException {
        HashSet<Integer> hs = new HashSet<>();
        sync.put(syncType, hs);

        sql = String.format("select id from %s", table);

        if( db.hasColumn(st, "", table, "deleted") )
            sql += " where deleted=0";

        try(ResultSet rs = st.executeQuery(sql)) {
            while (rs.next()) {
                hs.add( rs.getInt(1) );
            }
        }
    }

    protected void removeSyncId(Integer syncType, int id) {
        assert sync.get(syncType) != null;
        sync.get(syncType).remove(id);
    }


    protected void finishSyncTable(Integer syncType, String table) throws SQLException {
        assert sync.get(syncType) != null;
        boolean hasDeletedField = db.hasColumn(st, "", table, "deleted");
        for (Integer id : sync.get(syncType)) {
            if( hasDeletedField ) {
                sql = String.format("update %s set deleted=1 where id=%d", table, id);
            } else {
                sql = String.format("delete from  %s where id=%d", table, id);
            }
            st.executeUpdate(sql);
        }
        sync.get(syncType).clear();
    }

}
