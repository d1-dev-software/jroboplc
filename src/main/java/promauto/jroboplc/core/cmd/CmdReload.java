package promauto.jroboplc.core.cmd;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Consumer;

import promauto.jroboplc.core.AbstractCommand;
import promauto.jroboplc.core.api.Console;
import promauto.jroboplc.core.api.EnvironmentInst;
import promauto.jroboplc.core.api.Module;

public class CmdReload extends AbstractCommand {

	@Override
	public String getName() {
		return "reload";
	}

	@Override
	public String getUsage() {
		return "[mod1 mod2 ...]";
	}

	@Override
	public String getDescription() {
		return "reloads modules. Can be used while running";
	}

	@Override
	public String execute(Console console, String args) {
		
		List<Module> modules = new LinkedList<>();
		String[] modnames = args.split("\\s* \\s*");
		for(String modname: modnames) {
			if( !modname.isEmpty() ) {
				Module module = EnvironmentInst.get().getModuleManager().getModule(modname);
				if( module == null )
					return "\r\nModule [" + modname + "] is not found!\r\n";
				else
					modules.add(module);
			}
		}
		
		if( !EnvironmentInst.get().getConfiguration().load() )
			return "\r\nConfiguration error!\r\n";
			
		
		Reloader reloader = new Reloader(modules, statusOk -> {
			if( statusOk )
				console.print("\r\nReloading is done!\r\n");
			else
				console.print("\r\nReloading has been aborted!\r\n");
		});
		reloader.start();
		
		return "";
			 
	}
	
	public static class Reloader implements Consumer<Boolean> {
		private final Consumer<Boolean> onFinished;
		private final Iterator<Module> iterator;

		public Reloader(List<Module> modules, Consumer<Boolean> onFinished) {
			this.onFinished = onFinished;
			iterator = modules.iterator();
		}
		
		public void start() {
			accept(true);
		}

		@Override
		public void accept(Boolean result) {
			if( result  &&  iterator.hasNext() )
				iterator.next().requestReload(this);
			else
				if( onFinished != null )
					onFinished.accept(result);
		}
		
	}
	

}
