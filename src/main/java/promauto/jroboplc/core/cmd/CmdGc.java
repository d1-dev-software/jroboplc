package promauto.jroboplc.core.cmd;

import promauto.jroboplc.core.AbstractCommand;
import promauto.jroboplc.core.api.ANSI;
import promauto.jroboplc.core.api.Console;

public class CmdGc extends AbstractCommand {

	@Override
	public String getName() {
		return "gc";
	}
	
	@Override
	public String getUsage() {
		return "";
	}

	@Override
	public String getDescription() {
		return "starts garbage collector";
	}


	@Override
	public String execute(Console console, String args) {
		Runtime rt = Runtime.getRuntime();

		long max = Math.round(rt.maxMemory()/1024);

		long total1 = Math.round(rt.totalMemory()/1024);
		long free1 = Math.round(rt.freeMemory()/1024);
		long used1 = total1 - free1;
		double per1 = (double)used1 * 100 / total1;

		rt.gc();

		long total2 = Math.round(rt.totalMemory()/1024);
		long free2 = Math.round(rt.freeMemory()/1024);
		long used2 = total2 - free2;
		double per2 = (double)used2 * 100 / total2;


		return String.format("GC: max=%d, before=%d/%d (%.1f%%), after=%s%d%s/%d (%s%.1f%%%s), freed=%d (Kbytes)",
				max,
				used1,
				total1,
				per1,
				ANSI.BOLD+ ANSI.GREEN, used2, ANSI.RESET,
				total2,
				ANSI.BOLD+ ANSI.YELLOW, per2, ANSI.RESET,
				free2-free1
				);

	}


}
