package promauto.jroboplc.core.cmd;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import promauto.jroboplc.core.AbstractCommand;
import promauto.jroboplc.core.api.Console;
import promauto.jroboplc.core.api.Environment;
import promauto.jroboplc.core.api.EnvironmentInst;
import promauto.jroboplc.core.api.Module;
import promauto.jroboplc.core.api.Tag;

public class CmdSettag extends AbstractCommand {
	private final Logger logger = LoggerFactory.getLogger(CmdSettag.class);

	@Override
	public String getName() {
		return "st";
	}

	@Override
	public String getUsage() {
		return "mod tag val";
	}

	@Override
	public String getDescription() {
		return "sets a new value to a tag";
	}

	@Override
	public String execute(Console console, String args) {
		Environment env =EnvironmentInst.get();
		String result = "";
		String[] ss = args.split("\\s* \\s*", 3);
		if (ss.length==3) {
			Module module = env.getModuleManager().getModule(ss[0]);
			if (module!=null) {
				Tag tag = module.getTagTable().get(ss[1]);
				if (tag!=null) {
					try {
						tag.setString(ss[2]);
					} catch (NumberFormatException e) {
						EnvironmentInst.get().printError(logger, e);
					}
					result = tag.getName() + " = " + tag.getString();
				}
			}
		}
		return result;
	}


}
