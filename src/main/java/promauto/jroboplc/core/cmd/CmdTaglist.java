package promauto.jroboplc.core.cmd;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import promauto.jroboplc.core.AbstractCommand;
import promauto.jroboplc.core.api.ANSI;
import promauto.jroboplc.core.api.Console;
import promauto.jroboplc.core.api.Environment;
import promauto.jroboplc.core.api.EnvironmentInst;
import promauto.jroboplc.core.api.Module;
import promauto.jroboplc.core.api.Tag;

public class CmdTaglist extends AbstractCommand {

	private static final int DEFAULT_LIMIT = 1000;

	@Override
	public String getName() {
		return "tl";
	}

	@Override
	public String getUsage() {
		return "[[mod] tagname]";
	}

	@Override
	public String getDescription() {
		return "list tags filtered with regex by module/tagname";
	}
	
	private static class ModuleTag {
		public Module module;
		public Tag tag;
		public ModuleTag(Module module, Tag tag) {
			this.module = module;
			this.tag = tag;
		}
	}

	@Override
	public String execute(Console console, String args) {
		Environment env = EnvironmentInst.get();
		
		Pattern modpattern = null;
		String tagFilter = "";

		int limitdflt = DEFAULT_LIMIT;
		int limit = console.getProperty("cmd_tl_limit", limitdflt );
		int cntlast = 0;
		int cntbeg = 0;
		int cnttot = 0;
		
		if( args.equals(".") ) {
			cntbeg = console.getProperty("cmd_tl_pos", 0);
		} else
		if( args.startsWith("limit ") ) {
			String[] ss = args.split("\\s* \\s*", 2);
			if( ss.length > 1 )
				console.setProperty("cmd_tl_limit", ss[1]);
			return "Tag list limit: " + console.getProperty("cmd_tl_limit", limitdflt );
		} else
		{
			console.setProperty("cmd_tl_filter", args);
		}

		String filterstr = console.getProperty("cmd_tl_filter", "");
		String[] ss = filterstr.split("\\s* \\s*", 2);
		String modFilter = ss[0];
		tagFilter = ss.length>1? ss[1]: "";
		try {
			if (!modFilter.isEmpty())
				modpattern = Pattern.compile(modFilter);
		} catch (PatternSyntaxException e) {}

		
		int maxTagNameLen = 0;



		int cntend = cntbeg + limit;
		
		Set<Module> mods = env.getModuleManager().getModules();
		String[] noflags = new String[]{};
		List<ModuleTag> tags = new LinkedList<>();
		for(Module mod: mods) {
			
			if (modpattern!=null)
				if (!modpattern.matcher(mod.getName()).matches())
					continue;
			
			List<Tag> modtags = mod.getTagTable().getTags(tagFilter, true);
			if (modtags.size()>0) {
				for(Tag tag: modtags) {
					if( cnttot >= cntbeg  &&  cnttot < cntend ) {
						tags.add(new ModuleTag(mod, tag));
						maxTagNameLen = Math.max(maxTagNameLen, mod.getName().length() + tag.getName().length());
						cntlast = cnttot;
					}
					cnttot++;
				}
			}
		}
		
		StringBuilder sb = new StringBuilder();
		for (ModuleTag modtag: tags) {
			
			sb.append( String.format(
					"%-"+(maxTagNameLen+1)+"s", 
					modtag.module.getName() + ":" + modtag.tag.getName() 
				));
			
			sb.append(" = ");
			sb.append(modtag.tag.getString());
			sb.append("\r\n");
		}
		
		
		console.setProperty("cmd_tl_pos", "" + ++cntlast);

		sb.append("\r\nListed from " + (++cntbeg) + " to " + cntlast + ", total is " + cnttot);
		sb.append(ANSI.redBold(
				"\nDEPRECATED COMMAND! Use \"l\" instead and try also \"ll\", \"lf\", \"lb\"! See help \"h\" for more." ));

		return sb.toString();
	}

	
	
}
