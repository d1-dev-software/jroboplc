package promauto.jroboplc.core.cmd;

import promauto.jroboplc.core.AbstractCommand;
import promauto.jroboplc.core.api.Console;
import promauto.jroboplc.core.api.Environment;
import promauto.jroboplc.core.api.EnvironmentInst;

public class CmdQuit extends AbstractCommand {

	@Override
	public String getName() {
		return "quit";
	}
	
	@Override
	public String getUsage() {
		return "";
	}

	@Override
	public String getDescription() {
		return "stop and exit";
	}


	@Override
	public String execute(Console console, String args) {
		Environment env = EnvironmentInst.get();
		if (env.isRunning()) 
			env.getTaskManager().stop();
		
		env.setTerminated();
			
		return "Good-bye!";
	}


}
