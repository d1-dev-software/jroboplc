package promauto.jroboplc.core.cmd;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import promauto.jroboplc.core.AbstractCommand;
import promauto.jroboplc.core.JRoboPLC;
import promauto.jroboplc.core.api.Configuration;
import promauto.jroboplc.core.api.Console;
import promauto.jroboplc.core.api.EnvironmentInst;
import promauto.jroboplc.core.api.Module;

public class CmdState extends AbstractCommand {
	private final Logger logger = LoggerFactory.getLogger(JRoboPLC.class);

	@Override
	public String getName() {
		return "state";
	}

	@Override
	public String getUsage() {
		return "load|save [mod1 ...]";
	}

	@Override
	public String getDescription() {
		return "loads or saves state of modules to file";
	}

	@Override
	public String execute(Console console, String args) {
		
		Set<Module> modules = new HashSet<>();
		String[] ss = args.split("\\s* \\s*");
		String cmd = ss[0];
		for(int i=1; i<ss.length; ++i) {
			String modname = ss[i];
			if( !modname.isEmpty() ) {
				Module module = EnvironmentInst.get().getModuleManager().getModule(modname);
				if( module == null )
					return "\r\nModule [" + modname + "] is not found!\r\n";
				else
					modules.add(module);
			}
		}
		
		Configuration cm = EnvironmentInst.get().getConfiguration();
		String pathstr = cm.get(cm.getRoot(), "statepath",	"state.save");
		Path path = Paths.get( pathstr );
		
		boolean res = false;
		if( cmd.equals("save") )
			res = EnvironmentInst.get().getModuleManager().saveState(modules, path);
		else if( cmd.equals("load") )
			res = EnvironmentInst.get().getModuleManager().loadState(modules, path);
		else
			EnvironmentInst.get().printError(logger,"Bad arguments");
		
		return res? "OK": "FAIL";
	}
	
	


}
