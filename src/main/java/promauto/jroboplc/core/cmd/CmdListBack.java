package promauto.jroboplc.core.cmd;

import promauto.jroboplc.core.api.Console;
import promauto.jroboplc.core.cmd.CmdList;

public class CmdListBack extends CmdList {


	@Override
	public String getName() {
		return "lb";
	}

	@Override
	public String getUsage() {
		return "";
	}

	@Override
	public String getDescription() {
		return "list the previous page";
	}



	@Override
	public String execute(Console console, String args) {
		return list(console,
				console.getProperty("cmd_l_filter", ""),
				console.getProperty("cmd_l_pos", 0) - CmdList.LIMIT );
	}



}
