package promauto.jroboplc.core.cmd;

import java.util.Map;
import java.util.TreeMap;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import promauto.jroboplc.core.AbstractCommand;
import promauto.jroboplc.core.api.ANSI;
import promauto.jroboplc.core.api.Console;
import promauto.jroboplc.core.api.Environment;
import promauto.jroboplc.core.api.EnvironmentInst;
import promauto.jroboplc.core.api.Module;
import promauto.jroboplc.core.api.Tag;

public class CmdStx extends AbstractCommand {
	private final Logger logger = LoggerFactory.getLogger(CmdStx.class);

	@Override
	public String getName() {
		return "stx";
	}

	@Override
	public String getUsage() {
		return "mod tag val";
	}

	@Override
	public String getDescription() {
		return "sets a new value to a group of tags, where " + 
					ANSI.GREEN + "mod" + ANSI.RESET + " and " +
					ANSI.GREEN + "tag" + ANSI.RESET + " are regex expressions";
	}

	@Override
	public String execute(Console console, String args) {
		Environment env =EnvironmentInst.get();
		String[] ss = args.split("\\s* \\s*", 3);
		if( ss.length != 3 ) 
			return "";

		String result = "";
		Pattern pmod = Pattern.compile(ss[0]);
		Pattern ptag = Pattern.compile(ss[1]);
		Map<String,Tag> map = new TreeMap<>();
		
		for(Module module: env.getModuleManager().getModules()) {
			if( !pmod.matcher(module.getName()).matches() )
				continue;
			
			for( Tag tag: module.getTagTable().getMap().values() )
				if( ptag.matcher(tag.getName()).matches() )
					map.put(module.getName() + ":" + tag.getName(), tag);
		}
		
		for(Map.Entry<String,Tag> ent: map.entrySet()) { 
			try {
				ent.getValue().setString(ss[2]);
			} catch (NumberFormatException e) {
				EnvironmentInst.get().printError(logger, e);
				return result;
			}
			result += ent.getKey() + " = " + ent.getValue().getString() + "\r\n";
		}
		
		return result;
	}


}
