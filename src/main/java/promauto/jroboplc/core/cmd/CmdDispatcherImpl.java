package promauto.jroboplc.core.cmd;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import promauto.jroboplc.core.api.CmdDispatcher;
import promauto.jroboplc.core.api.Command;
import promauto.jroboplc.core.api.Console;
import promauto.jroboplc.core.api.Environment;
import promauto.jroboplc.core.api.EnvironmentInst;
import promauto.jroboplc.core.api.Module;
import promauto.jroboplc.core.api.Plugin;

public class CmdDispatcherImpl implements CmdDispatcher {
	
	private static final String UNHANDLED_EXCEPTION_TEXT = "Command execution failed!";

	final Logger logger = LoggerFactory.getLogger(CmdDispatcherImpl.class);
	
	private final Environment env;
	private final Map<String,CmdDispatcher.Entry> commands = new ConcurrentHashMap<>();
	private final Map<Class<?>,Command> cmdByClass = new ConcurrentHashMap<>();


	public CmdDispatcherImpl() {
		this.env = EnvironmentInst.get();
	}

	
	private boolean addCommand(String cmdNamePrefix, Plugin plugin, Module module, 
			Class<? extends Command> cmdClass) {
		
		Command cmd = createCommand(cmdClass);
		if (cmd!=null) {
			commands.putIfAbsent(
					cmdNamePrefix + cmd.getName(),
					new CmdDispatcher.Entry(cmd, plugin, module));
		}
		return (cmd!=null);
	}


	private Command createCommand(Class<? extends Command> cmdClass) {
		Command cmd = cmdByClass.get(cmdClass);
		if (cmd==null)
			try {
				cmd = cmdClass.newInstance();
				cmdByClass.put(cmdClass, cmd);
			} catch (InstantiationException | IllegalAccessException e) {
				env.printError(logger, e, cmdClass.getName());
			}
		return cmd;
	}
	

	@Override
	public boolean addCommand(Class<? extends Command> cmdClass) {
		return addCommand(
				"",
				null,
				null,
				cmdClass);
	}


	@Override
	public boolean addCommand(Plugin plugin, Class<? extends Command> cmdClass) {
		return addCommand(
				plugin.getPluginName() + ":",
				plugin,
				null,
				cmdClass);
	}
	

	@Override
	public boolean addCommand(Module module, Class<? extends Command> cmdClass) {
		return addCommand(
				module.getName() + ":", 
				module.getPlugin(),
				module,
				cmdClass);
	}


	@Override
	public String execute(Console console, String cmdtext) {
		if (cmdtext.isEmpty()) return "";
		
		String cmd[] = cmdtext.split("\\s* \\s*", 2);
		String args = (cmd.length==2)? cmd[1]: "";
		
		try {
			CmdDispatcher.Entry entry = commands.get(cmd[0]);
			if (entry!=null) {
				if (entry.module!=null)
					return entry.command.execute(console, entry.module, args);
				else
					
				if (entry.plugin!=null)
					return entry.command.execute(console, entry.plugin, args);
				else
					
					return entry.command.execute(console, args);
			}	
			else
				return "Unknown command";
		} catch (Throwable e) {
			env.printError(logger, e, UNHANDLED_EXCEPTION_TEXT);
			return UNHANDLED_EXCEPTION_TEXT;
		}
	}


	@Override
	public Map<String,CmdDispatcher.Entry>  getCommands() {
		return commands;
	}

}
