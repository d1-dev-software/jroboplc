package promauto.jroboplc.core.cmd;

import java.util.Set;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import promauto.jroboplc.core.AbstractCommand;
import promauto.jroboplc.core.api.ANSI;
import promauto.jroboplc.core.api.Console;
import promauto.jroboplc.core.api.EnvironmentInst;
import promauto.jroboplc.core.api.Module;


public class CmdCheck extends AbstractCommand {
	final Logger logger = LoggerFactory.getLogger(CmdCheck.class);

	@Override
	public String getName() {
		return "check";
	}
	
	@Override
	public String getUsage() {
		return "[mod filter]";
	}

	@Override
	public String getDescription() {
		return "checks internal errors of the modules";
	}


	@Override
	public String execute(Console console, String args) {
		Set<Module> mods = EnvironmentInst.get().getModuleManager().getModules();

		Pattern pattern = null;
		try {
			if (!args.isEmpty())
				pattern = Pattern.compile(args);
		} catch (PatternSyntaxException e) {
			EnvironmentInst.get().printError(logger, e, "Pattern compilation error:", args);
		}
		
		String res = "";
		int cnterr = 0;
		int cnttot = 0;
		for(Module m: mods) {
			if( pattern!=null  &&  !pattern.matcher(m.getName()).matches() )
				continue;
			
			++cnttot;
			
			String checkres = m.check();
			if( checkres.isEmpty() )
				continue;
			
			++cnterr;
			res += ANSI.RED + ANSI.BOLD + m.getName() + " (" + m.getPlugin().getPluginName() + "):" + ANSI.RESET +
					"\r\n" + checkres + "\r\n\r\n";
		}
		
		if( cnterr > 0 )
			res += "-----\r\n";

		res += "Checked modules total: " + cnttot + "\r\n";
		res += "Modules with errors:   " + cnterr + "\r\n";
			
		return res;
	}


}
