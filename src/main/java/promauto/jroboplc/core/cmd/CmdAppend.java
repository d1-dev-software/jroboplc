package promauto.jroboplc.core.cmd;

import promauto.jroboplc.core.AbstractCommand;
import promauto.jroboplc.core.api.Console;
import promauto.jroboplc.core.api.EnvironmentInst;

public class CmdAppend extends AbstractCommand {

	@Override
	public String getName() {
		return "load";
	}

	@Override
	public String getUsage() {
		return "plg mod";
	}

	@Override
	public String getDescription() {
		return "loads new module";
	}

	@Override
	public String execute(Console console, String args) {
		String[] ss = args.split("\\s* \\s*");
		
		if( ss.length != 2 )
			return "Bad arguments";
		
		String plgname = ss[0];
		String modname = ss[1];

		if( EnvironmentInst.get().getModuleManager().appendModule(plgname, modname) )
			return "OK";
		else
			return "FAIL";
	}
	
	

}
