package promauto.jroboplc.core.cmd;

import promauto.jroboplc.core.AbstractCommand;
import promauto.jroboplc.core.api.ANSI;
import promauto.jroboplc.core.api.Console;
import promauto.jroboplc.core.api.EnvironmentInst;

public class CmdKeygen extends AbstractCommand {

	@Override
	public String getName() {
		return "keygen";
	}
	
	@Override
	public String getUsage() {
		return "[keyName]";
	}

	@Override
	public String getDescription() {
		return "generates and save a pair of keys";
	}


	@Override
	public String execute(Console console, String args) {
		return "Generated: " + EnvironmentInst.get().getKeyManager().generateAndSaveKeyPair( args.trim() );
	}


}
