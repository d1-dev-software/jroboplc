package promauto.jroboplc.core;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.yaml.snakeyaml.LoaderOptions;
import org.yaml.snakeyaml.Yaml;

import promauto.jroboplc.core.api.Configuration;
import promauto.jroboplc.core.api.EnvironmentInst;
import promauto.jroboplc.core.exceptions.ConfigurationNotValidException;

public class ConfigurationYaml implements Configuration {
	public static final String APPLICATION_YML = "application.yml";
	public static final int MAX_ALIASES_FOR_COLLECTIONS = 100;
	private final Logger logger = LoggerFactory.getLogger(Configuration.class);

	private final String confdir;
	private Map<String,Object> confmap;
	
	public ConfigurationYaml(String confdir) {
		this.confdir = confdir;
	}

	@Override
	public boolean load() {
		Map<String,Object> tmpmap = new HashMap<>();

		// load application.yml
		try (InputStream in = ModuleManagerImpl.class.getClassLoader().getResourceAsStream(APPLICATION_YML)) {
			Yaml yaml = createYaml();
//			Yaml yaml = new Yaml();
			tmpmap.putAll(yaml.load(in));
		} catch (Exception e) {
			EnvironmentInst.get().printError(null, e, "Failed to load application.yml");
		}

		// iterate over directory
		try (DirectoryStream<Path> paths = Files.newDirectoryStream(Paths.get(confdir), "[!.]*.yml")) {
			for (Path path: paths) {
				if( !loadFile(path, tmpmap) ) {
					System.out.println("Configuration error: " + path.toString() + "\r\n");
					return false;
				}
			}
			confmap = tmpmap;
		} catch (IOException|InvalidPathException e) {
			System.out.println("Configuration error: " + confdir + "\r\n");
			e.printStackTrace();
			return false;
		}

		return true;
	}

	
	@SuppressWarnings("unchecked")
	private boolean loadFile(Path path, Map<String,Object> map) {
		
		Map<Object,Object> root = loadYamlFileAsMap(path);
		if( root == null )
			return false;
		
		
		for(Map.Entry<Object,Object> ent: root.entrySet()) {
			String key = (ent.getKey() instanceof String)? 
					(String)ent.getKey() :
					ent.getKey().toString();
					
			if( key.startsWith("plugin.")  &&  map.containsKey(key) ) {
				
				Map<Object,Object> plgmap = (Map<Object,Object>)map.get(key);
				Map<Object,Object> addmap = (Map<Object,Object>)ent.getValue();
				plgmap.putAll(addmap);
				
			} else {
				map.put(key, ent.getValue());
			}
		}
				
		return true;
	}

	
	@SuppressWarnings("unchecked")
	public static Map<Object,Object> loadYamlFileAsMap(Path path) {
		Yaml yaml = createYaml();
//		Yaml yaml = new Yaml();
		try (InputStream in = Files.newInputStream(path)) {

			Object res = yaml.load(in);
			
			return res==null? new HashMap<>(): (Map<Object,Object>)res;
			
		} catch (Exception e) {
			EnvironmentInst.get().printError(null, e, "File:", path.toString());
		}
		return null;
	}

	private static Yaml createYaml() {
		LoaderOptions lo = new LoaderOptions();
		lo.setMaxAliasesForCollections(MAX_ALIASES_FOR_COLLECTIONS);
		lo.setAllowDuplicateKeys(false);
		lo.setAllowRecursiveKeys(false);
		return new Yaml(lo);
	}

	
	@Override
	public Map<String, Object> getRoot() {
		return confmap;
	}

	
	@Override
	@SuppressWarnings("unchecked")
	public Object getModuleConf(String plugin, String module) {
		Object plgconf = confmap.get("plugin." + plugin);
		if( plgconf != null )
			try {
				Object modconf = ((Map<String,Object>)plgconf).get("module." + module);
				if( modconf != null )
					return modconf;
			} catch(ClassCastException e) {
				EnvironmentInst.get().printError(logger, e, plugin, module);
			}
		
		EnvironmentInst.get().printError(logger,"Configuration not found:", plugin, module);
		return null;
	}
	
	
	@Override
	public Path getPath(Object conf, String key, String defval) {
		Path path = Paths.get( get(conf, key, defval) );
		if (!path.isAbsolute())
			path = getConfDir().resolve(path);
		return path;
	}


	
	@Override
	@SuppressWarnings("unchecked")
	public Object get(Object conf, String key) {
		try {
			return ((Map<String,Object>)conf).get(key);
		} catch(ClassCastException e) {
			EnvironmentInst.get().printError(logger, e, "key:", key);
		}
		return null;
	}

	
	@Override
	@SuppressWarnings("unchecked")
	public int get(Object conf, String key, int defval) {
		Object obj=null;
		try {
			obj = ((Map<String,Object>)conf).get(key);
			if( obj != null)
				return (int)obj;
		} catch(ClassCastException e) {
			EnvironmentInst.get().printError(logger, e,
					"key:", key,
					"val:", (obj==null? "": obj.toString()));
		}
		return defval;
	}


	@Override
	@SuppressWarnings("unchecked")
	public long get(Object conf, String key, long defval) {
		Object obj=null;
		try {
			obj = ((Map<String,Object>)conf).get(key);
			if( obj != null )
				return (obj instanceof Integer)? (int)obj: (long)obj;

		} catch(ClassCastException e) {
			EnvironmentInst.get().printError(logger, e,
					"key:", key,
					"val:", (obj==null? "": obj.toString()));
		}
		return defval;
	}


	@Override
	@SuppressWarnings("unchecked")
	public double get(Object conf, String key, double defval) {
		Object obj=null;
		try {
			obj = ((Map<String,Object>)conf).get(key);
			if( obj != null)
				return (double)obj;
		} catch(ClassCastException e) {
			EnvironmentInst.get().printError(logger, e,
					"key:", key,
					"val:", (obj==null? "": obj.toString()));
		}
		return defval;
	}

	
	@Override
	@SuppressWarnings("unchecked")
	public boolean get(Object conf, String key, boolean defval) {
		Object obj=null;
		try {
			obj = ((Map<String,Object>)conf).get(key);
			if( obj != null)
				return (boolean)obj;
		} catch(ClassCastException e) {
			EnvironmentInst.get().printError(logger, e,
					"key:", key,
					"val:", (obj==null? "": obj.toString()));
		}
		return defval;
	}
	

	@Override
	@SuppressWarnings("unchecked")
	public String get(Object conf, String key, String defval) {
		Object obj=null;
		try {
			obj = ((Map<String,Object>)conf).get(key);
			if( obj != null) {
				if( obj instanceof String )
					return (String)obj;
				else
					return obj.toString();
			}
		} catch(ClassCastException e) {
			EnvironmentInst.get().printError(logger, e,
					"key:", key,
					"val:", (obj==null? "": obj.toString()));
		}
		return defval;
	}


	@Override
	public List<String> getStringList(Object conf, String key) {
		return toList(get(conf, key)).stream()
				.map(Object::toString)
				.collect(Collectors.toList());
	}


	@Override
	@SuppressWarnings("unchecked")
	public Map<String,Object> toMap(Object conf) {
		if( conf != null )
			try {
				return (Map<String,Object>)conf;
			} catch(ClassCastException e) {
				EnvironmentInst.get().printError(logger, e);
			}
		
		return new HashMap<>();
	}

	@Override
	@SuppressWarnings("unchecked")
	public <K,V> Map<K,V> toGenericMap(Object conf) {
		if( conf != null )
			try {
				return (Map<K,V>)conf;
			} catch(ClassCastException e) {
				EnvironmentInst.get().printError(logger, e);
			}
		
		return new HashMap<>();
	}

	
	@Override
	@SuppressWarnings("unchecked")
	public List<Object> toList(Object conf) {
		if( conf != null ) {
			if( conf instanceof List )
				return (List<Object>)conf;
			
			if( conf instanceof Map )
				return new ArrayList<>( ((Map<Object,Object>)conf).keySet() );
		}
		
		return new ArrayList<>();
	}

	
	@Override
	public Path getConfDir() {
		try {
			return Paths.get(confdir);
		} catch(InvalidPathException e) {
			EnvironmentInst.get().printError(logger, e);
		}
		return Paths.get(".");
	}


	@Override
	public void throwConfigurationNotValidException(String text) throws ConfigurationNotValidException {
		throw new ConfigurationNotValidException(text);
	}

	@Override
	public void throwConfigurationNotValidException(String text, Object conf) throws ConfigurationNotValidException {
		if( conf != null ) {
			if( conf instanceof List ) {
				text += ":\r\n" + toList(conf).stream()
						.map(Object::toString)
						.collect(Collectors.joining(", "));
			}
			else if( conf instanceof Map ) {
				text += ":" + toMap(conf).entrySet().stream()
						.map(e -> "\r\n" + e.getKey() + ": " + e.getValue())
						.collect(Collectors.joining());
			}
			else {
				text += ": " + conf.toString();
			}
		}
		throw new ConfigurationNotValidException(text);
	}

}
