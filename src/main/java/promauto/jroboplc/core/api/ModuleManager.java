package promauto.jroboplc.core.api; 

import java.net.URLClassLoader;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;
import java.util.Set;

public interface ModuleManager { 
	
//	boolean loadPlugins(String pluginDir);
	
	List<Plugin> getPlugins();
		
	boolean loadModules();
	
	Set<Module> getModules();

	List<Module> getModulesWithExternalTags();

	Module getModule(String name);

	<T> T getModule(String name, Class<? extends T> cls);

	Map<String, Tag> getTagsAll();

	Tag searchTag(String name);

	Tag searchExternalTag(String name);


	boolean appendModule(String plgname, String modname);

//	boolean removeModule(String modname);


	boolean saveState(Set<Module> modules, Path path);

	boolean loadState(Set<Module> modules, Path path);


//	String getPluginDir();

//	URLClassLoader getClassLoader();

}
