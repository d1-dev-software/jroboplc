package promauto.jroboplc.core.api;

import promauto.jroboplc.core.api.Database;

import java.sql.SQLException;
import java.sql.Statement;

public interface DatabaseProtoService {

    void setDb(Database db);

    Database getDb();

    Statement createStatement() throws SQLException;

    void commit() throws SQLException;

    void rollback();

    String getLastSql();

}
