package promauto.jroboplc.core.api;

import java.nio.file.Path;

public interface KeyManager {

    String encryptPublic(String keyName, String decryptedText) throws Exception;

    String decryptPrivate(String keyName, String enryptedText) throws Exception;

    String generateAndSaveKeyPair(String keyName);

    String getDefaultPrivateKeyName();
}
