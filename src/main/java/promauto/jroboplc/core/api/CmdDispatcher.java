package promauto.jroboplc.core.api;

import java.util.Map;

/**
 * A processor of text-based commands (like console commands or others).
 * The approach used by this command processor consists of three steps:
 * <ul>
 * <li>parse input text command and find an appropriate {@link Entry}
 * <li>get <code>Command</code> from the entry an invoke execute method 
 * <li>return text result of the command's execution
 * </ul>
 * Method {@link CmdDispatcher#execute} carries out processing of one text command.
 * Commands need to be added before using this method by one of three add-methods 
 * supposed for three kind of commands (shown with examples):
 * <ul>
 * <li> {@link #addCommand(Class)} for common commands
 * <blockquote><pre>
 * cmdDispatcher().addCommand( CmdQuit.class );
 * cmdDispatcher().addCommand( CmdHelp.class );
 * </pre></blockquote>
 * <li> {@link #addCommand(Plugin, Class)} for plugin commands
 * <blockquote><pre>
 * cmdDispatcher().addCommand( plugin, CmdPlugInfo.class );
 * </pre></blockquote>
 * <li> {@link #addCommand(Module, Class)} for module commands
 * <blockquote><pre>
 * cmdDispatcher().addCommand( module, CmdModInfo.class );
 * </pre></blockquote>
 * </ul>
 *  
 *  After the command has been added it is stored in the command collection (an internal map) which
 *  can be reached by {@link #getCommands()}.
 */
public interface CmdDispatcher {
	
	/**
	 * An entry of {@link CmdDispatcher}'s collection of commands which links object of {@link Command}
	 * with {@link Plugin} and {@link Module} objects. There can be three cases of Entry usage:
	 * <blockquote><pre>
	 * new CmdDispatcher.Entry(command, null, null); // creates entry for <i>common command</i>
	 * new CmdDispatcher.Entry(command, plugin, null); // creates entry for <i>plugin command</i>
	 * new CmdDispatcher.Entry(command, plugin, module); // creates entry for <i>module command</i>
	 * </pre></blockquote>
	 */
	static class Entry {
		public Command command;
		public Plugin plugin;
		public Module module;
		
		/**
		 * Creates an entry object for {@link CmdDispatcher} commands collection. 
		 * @param command reference to {@link Command} object. Must be not null.
		 * @param plugin reference to {@link Plugin} object. Can be null.
		 * @param module reference to {@link Module} object. Can be null.
		 */
		public Entry(Command command, Plugin plugin, Module module) {
			this.command = command;
			this.plugin = plugin; 
			this.module = module;
		}
	}

	
	
	/**
	 * Creates an object of the <code>cmdClass</code>  and adds it to the commands collection. 
	 * <ol>
	 * <li> Search for existing <code>cmdClass</code> object and if nothing creates one 
	 * <li> Creates new <b>common command</b> {@link Entry} in commands collection
	 * </ol>
	 * @param cmdClass must be descendant of {@link Command}
	 * @return true if successfully added, false if failed to create instance of <code>cmdClass</code>
	 */
	boolean addCommand(Class<? extends Command> cmdClass);
	
	/**
	 * Creates an object of the <code>cmdClass</code>  and adds it to the commands collection. 
	 * <ol>
	 * <li> Search for existing <code>cmdClass</code> object and if nothing creates one 
	 * <li> Creates new <b>plugin command</b> {@link Entry} in commands collection
	 * </ol>
	 * @param plugin instance of {@link Plugin} which command relates to
	 * @param cmdClass must be descendant of {@link Command}
	 * @return true if successfully added, false if failed to create instance of <code>cmdClass</code>
	 */
	boolean addCommand(Plugin plugin, Class<? extends Command> cmdClass);
	
	/**
	 * Creates an object of the <code>cmdClass</code>  and adds it to the commands collection. 
	 * <ol>
	 * <li> Search for existing <code>cmdClass</code> object and if nothing creates one 
	 * <li> Creates new <b>module command</b> {@link Entry} in commands collection
	 * </ol>
	 * @param module instance of {@link Modules} which command relates to
	 * @param cmdClass must be descendant of {@link Command}
	 * @return true if successfully added, false if failed to create instance of <code>cmdClass</code>
	 */
	boolean addCommand(Module module, Class<? extends Command> cmdClass);
	
	
	/**
	 * Executes text string and returns text result of execution. 
	 * First token of <code>cmdtext</code> separated by space
	 * is a key and is used for searching an appropriate {@link Entry} in commands collection. 
	 * The rest of the <code>cmdtext</code> pass to the {@link Command} as arguments 
	 * @param cmdtext raw command text, e.g: <code>"myplugin:info -brief -system"</code>
	 * @return result of execution 
	 */
	String execute(Console console, String cmdtext);
	
	/**
	 * Returns command collection as a map where key is String and value is an {@link Entry}. 
	 * Format of key string (for common, plugin and module commands respectively): <ul><code>
	 *  <li>{@literal <command name>} 
	 *  <li>{@literal <plugin name>:<command name>} 
	 *  <li>{@literal <module name>:<command name>}
	 *  </code></ul> 
	 *  
	 * @return
	 */
	Map<String,CmdDispatcher.Entry>  getCommands();

}
