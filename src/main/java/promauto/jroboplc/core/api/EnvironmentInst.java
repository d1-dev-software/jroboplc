package promauto.jroboplc.core.api;

public class EnvironmentInst {
	private static Environment env;

	public static Environment get() {
		return env;
	}

	public static void set(Environment env) {
		EnvironmentInst.env = env;
	}
	
	
}
