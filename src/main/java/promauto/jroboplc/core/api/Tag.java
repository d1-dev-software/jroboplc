package promauto.jroboplc.core.api;

public interface Tag {

	int STRING_LENGTH_MAX = 4096;

	String BOOL_ON = "on";
	String BOOL_OFF = "off";



    enum Type {
        INT,
        LONG,
        DOUBLE,
        BOOL,
        STRING
    };
	
	enum Status {
        Uninitiated,
        Good,
        Bad,
        Deleted
    };
	

	String getName();
	Type getType();
	
	Status getStatus();
	void setStatus(Status status);

	int 	getFlags();
	void 	setFlags(int flags);
	void 	addFlag(int flag);
	boolean hasFlags(int flag);
	void 	copyFlagsFrom(Tag tag);


	boolean equalsValue(Tag tag);
	void copyValueTo(Tag tag);


	boolean getBool();
	int 	getInt();
	double 	getDouble();
	long 	getLong();
	String 	getString();

	void setBool(boolean value);
	void setInt(int value);
	void setDouble(double value);
	void setLong(long value);
	void setString(String value) throws NumberFormatException;

	void setOn();
	void setOff();


	<T> T getObject();
	void setObject(Object object);

}
