package promauto.jroboplc.core.api;

import java.util.Collection;

public interface TcpServerPort {
	
	int getPortnum();
	
	int getPorttcp();

	String getFormat();
	
	boolean isEnabled();

	
	Collection<? extends TcpServerChannel> getChannels();

	TcpSubscriber getSubscriber();
	
	void setSubscriber(TcpSubscriber subscriber);

	long getAliveTimeout();


}
