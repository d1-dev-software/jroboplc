package promauto.jroboplc.core.api;

public interface Console {
	public static final String PROP_ON = "on";
	public static final String PROP_OFF = "off";

	boolean initialize();

	void close();

	void process();

	void print(String text);

	String getPrompt();
	
	int getProperty(String property_id, int default_value);

	String getProperty(String property_id, String default_value);

	void setProperty(String property_id, String value);
	
	String parseAnsi(String text);

	String startRepeatingLastRequest(long period);

	void stopRepeatingLastRequest();


}
