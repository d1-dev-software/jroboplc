package promauto.jroboplc.core.api;

public class ANSI {
	public static final String RESET 	= "\u001B[0m";
	public static final String BLACK 	= "\u001B[30m";
	public static final String RED 		= "\u001B[31m";
	public static final String GREEN 	= "\u001B[32m";
	public static final String YELLOW 	= "\u001B[33m";
	public static final String BLUE 	= "\u001B[34m";
	public static final String MAGENTA 	= "\u001B[35m";
	public static final String CYAN 	= "\u001B[36m";
	public static final String WHITE 	= "\u001B[37m";
	public static final String BOLD 	= "\u001B[01m";
	public static final String CLEAR 	= "\u001B[2J";
	public static final String HOME 	= "\u001B[H";


	public static String redBold(String text) {
		return ANSI.RED + ANSI.BOLD + text + ANSI.RESET;
	}

	public static String greenBold(String text) {
		return ANSI.GREEN + ANSI.BOLD + text + ANSI.RESET;
	}

	public static String yellow(String text) {
		return ANSI.YELLOW + text + ANSI.RESET;
	}

}
