package promauto.jroboplc.core.api;

import java.net.InetSocketAddress;

public interface TcpServerChannel {
	
	String getId();
	
	TcpServerPort getPort();
	
	InetSocketAddress getRemoteAddress(); 
	
	boolean write(String data);


	
	Object getAttachedObject();
	
	void setAttachedObject(Object obj);
	
	
	boolean isLogEnable();
	
	void setLogEnable(boolean logging);
	
	
	
	boolean checkAlive();
	void markChannelAsAlive();



}
