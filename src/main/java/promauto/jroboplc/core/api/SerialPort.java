package promauto.jroboplc.core.api;

public interface SerialPort {
	
	int getId();

	boolean setParams(int baud, int databits, int parity, int stopbits, int timeout);
	
	
	boolean open();

	void close();
	
	boolean isOpened();
	
	boolean isValid();
	
	void setInvalid();
	
	
	
	int getAvailable() throws Exception;

	int readByte() throws Exception;

	int readBytes(int[] buff, int size) throws Exception;

	int readBytesDelim(int[] buff, int delim) throws Exception;

	String readString(int size) throws Exception;

	String readStringDelim(int delim) throws Exception;

	
	boolean writeByte(int data) throws Exception;

	boolean writeBytes(int[] data, int size) throws Exception;

	boolean writeString(String data) throws Exception;

	boolean discard() throws Exception;

	
	String getInfo();


}
