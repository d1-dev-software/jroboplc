package promauto.jroboplc.core.api;

public interface TaskManager {
	

	String start();
	
	String stop();
	
	Task getTask(Module module);

	Task getTask(String modname);

}
