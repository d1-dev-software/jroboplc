package promauto.jroboplc.core.api;

public interface Flags {

	static final int NONE 	    = 0;
	static final int AUTOSAVE 	= 1;
	static final int HIDDEN 	= 2;
	static final int EXTERNAL 	= 4;
	static final int STATUS  	= 8;

	static int parseFlags(String flagstr) {
		int flags = 0;
		String[] ss = flagstr.split("[\\W,]+");
		for(int i=0; i<ss.length; ++i) {
			switch (ss[i].toUpperCase().trim()) {
				case "NONE":
					return NONE;
				case "AUTOSAVE":
					flags += AUTOSAVE;
					break;
				case "HIDDEN":
					flags += HIDDEN;
					break;
				case "EXTERNAL":
					flags += EXTERNAL;
					break;
				case "STATUS":
					flags += STATUS;
					break;
			}
		}
		return flags;
	}


//	static String flagsToStr(int flags) {
//		return ""
//				+ ((flags & AUTOSAVE) > 0? "AUTOSAVE":"")
//				+ ((flags & HIDDEN) > 0? "HIDDEN":"")
//				+ ((flags & EXTERNAL) > 0? "EXTERNAL":"")
//				+ ((flags & STATUS) > 0? "STATUS":"")
//				;
//	}


}
