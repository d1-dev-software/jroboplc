package promauto.jroboplc.core.api;

public interface TcpServer {

	
	TcpServerPort getPortByNum(int portnum);
	
	TcpServerPort getPortByTcp(int porttcp);
	
	TcpServerPort getPortBySubscriber(TcpSubscriber subscriber);

	boolean subscribe(int portnum, TcpSubscriber subscriber);

	boolean disconnect(int portnum);

}
