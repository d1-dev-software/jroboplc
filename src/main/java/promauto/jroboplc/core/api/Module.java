package promauto.jroboplc.core.api;

import promauto.jroboplc.core.State;
import promauto.jroboplc.core.TagTable;

import java.util.Map;
import java.util.function.Consumer;

public interface Module {
	
	
	boolean prepare(); 
	
	boolean execute(); 
	
	boolean closedown();
	
	void requestReload(Consumer<Boolean> reloader);
	
	String check();
		
	
	boolean isEnabled();

	boolean isTaskable();

	boolean canHaveExternalTags();

	Plugin getPlugin();

	String getName();

	String getInfo();
	
	TagTable getTagTable();
	
	String getFlag(String flag);
	
	boolean isFlagCompatibleWith(String flag, Module module);
	

//	void release();


//	Map<String,String> getState();
//	boolean applyState(Map<String,String> state);

	void loadState(State state);

	void saveState(State state);


	boolean isSuspended();
	
	void setSuspended(boolean value);


	void postCommand(Command cmd, Console console, Module module, String args);



    void addSignalListener(Signal.Listener signalListener);

    void removeSignalListener(Signal.Listener signalListener);

	void postSignal(Signal.SignalType id);

	void postSignal(Signal.SignalType id, Object data);

}
