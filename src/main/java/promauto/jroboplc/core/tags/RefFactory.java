package promauto.jroboplc.core.tags;

public interface RefFactory {
    Ref createRef();
    RefGroup createRefGroup();
}
