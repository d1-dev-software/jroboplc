package promauto.jroboplc.core.tags;

public class RefFactoryImpl implements RefFactory {
    @Override
    public Ref createRef() {
        return new Ref();
    }

    @Override
    public RefGroup createRefGroup() {
        return new RefGroup();
    }
}
