package promauto.jroboplc.core.tags;

import promauto.jroboplc.core.api.Tag;

public class TagRWInt extends TagRW {

	// can be accessed from multiple threads
	private volatile int valueRd;

	// must be accessed from one thread by tag owner module only!
	private int valueWr;
	private int valueWrLast = 0;
	
	

	public TagRWInt(String name, int value, int flags) {
		super(name, flags);
		this.valueRd = value;
	}

	@Override
	public Type getType() {
		return Type.INT;
	}
	
	
///// general methods for multithreading operations /////
	@Override
	public boolean getBool() {
		return valueRd!=0;
	}

	@Override
	public int getInt() {
		return valueRd;
	}

	@Override
	public long getLong() {
		return valueRd;
	}

	@Override
	public double getDouble() {
		return (double)valueRd;
	}

	@Override
	public String getString() {
		return ""+valueRd;
	}
	
	

	
	@Override
	public synchronized void setBool(boolean value) {
		valueWr = value? 1: 0;
		valueWrChanged = true;
	}

	@Override
	public synchronized void setOn() {
		valueWr = 1;
		valueWrChanged = true;
	}

	@Override
	public synchronized void setOff() {
		valueWr = 0;
		valueWrChanged = true;
	}

	@Override
	public synchronized void setInt(int value) {
		valueWr = value;
		valueWrChanged = true;
	}

	@Override
	public synchronized void setLong(long value) {
		valueWr = (int)value;
		valueWrChanged = true;
	}

	@Override
	public synchronized void setDouble(double value) {
		valueWr = (int)value;
		valueWrChanged = true;
	}

	@Override
	public synchronized void setString(String value) {
		try {
			valueWr = Integer.parseInt(value.trim());
		} catch (NumberFormatException e) {
			valueWr = 0;
		}
		valueWrChanged = true;
	}






///// Methods below are suppossed to be used by tag owner module only!
///// Must be invoked from single thread!

	@Override
	public boolean getWriteValBool() {
		return valueWrLast != 0;
	}
	
	@Override
	public int getWriteValInt() {
		return valueWrLast;
	}

	@Override
	public long getWriteValLong() {
		return (long)valueWrLast;
	}

	@Override
	public double getWriteValDouble() {
		return (double)valueWrLast;
	}

	@Override
	public String getWriteValString() {
		return ""+valueWrLast;
	}
	
	
	
	
	
	
	@Override
	public void setReadValBool(boolean value) {
		valueRd = value? 1: 0;
	}

	@Override
	public void setReadValInt(int value) {
		valueRd = value;
	}

	@Override
	public void setReadValLong(long value) {
		valueRd = (int)value;
	}

	@Override
	public void setReadValDouble(double value) {
		valueRd = (int)value;
	}

	@Override
	public void setReadValString(String value) {
		try {
			valueRd = Integer.parseInt(value.trim());
		} catch (NumberFormatException e) {
			valueRd = 0;
		}
	}



	
	
	
	@Override
	public boolean equalsValue(Tag tag) {
		return tag.getInt() == valueRd;
	}

	@Override
	public void copyValueTo(Tag tag) {
		tag.setInt(valueRd);
	}



	
	@Override
	protected void copyWriteToLastWrite() {
		valueWrChanged = false;
		valueWrLast = valueWr;
	}
	
	@Override
	protected void copyWriteToRead() {
		valueRd = valueWr;
	}

	@Override
	public void copyLastWriteToRead() {
		valueRd = valueWrLast;
	}

	
	

}
