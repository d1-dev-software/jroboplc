package promauto.jroboplc.core.tags;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import promauto.jroboplc.core.api.Configuration;
import promauto.jroboplc.core.api.EnvironmentInst;
import promauto.jroboplc.core.api.Module;
import promauto.jroboplc.core.api.Tag;

public class Ref {
	private final Logger logger = LoggerFactory.getLogger(Ref.class);

//	public interface Factory {
//		Ref createInstance();
//	}
//
//	public static class FactoryImpl implements Ref.Factory {
//		@Override
//		public Ref createInstance() {
//			return new Ref();
//		}
//	}

	private String refTagName;
	private String refModuleName;
	private String refModuleTagName;
	private Tag tag = null;
	private Module refModule;
	private boolean prepared = false;


	Ref() {
	}

	public boolean init(Object conf, String refname, Module module) {

		assert module != null;

		Configuration cm = EnvironmentInst.get().getConfiguration();

		String nameModuleTag = cm.get(conf, refname, "");

		int p = nameModuleTag.indexOf(':');
		if(p >= 0) {
			refModuleName = nameModuleTag.substring(0, p);
			refTagName = nameModuleTag.substring(p+1);
		} else {
			refModuleName = module.getName();
			refTagName = nameModuleTag;
		}

		boolean res = !refModuleName.isEmpty()  &&  !refTagName.isEmpty();
		if( !res )
			EnvironmentInst.get().printError(logger, "Bad refname: ", nameModuleTag );

		prepared = false;
		return res;
	}


	public boolean init(String nameModuleTag) {
		int p = nameModuleTag.indexOf(':');
		if(p < 0) {
			EnvironmentInst.get().printError(logger, "Bad refname: ", nameModuleTag );
			return false;
		}

		init( nameModuleTag.substring(0, p), nameModuleTag.substring(p+1) );
		return true;
	}


	public boolean init(String nameLinkModule, String nameLinkTag) {
		assert !nameLinkModule.isEmpty();
		assert !nameLinkTag.isEmpty();
		this.refModuleName = nameLinkModule;
		this.refTagName = nameLinkTag;
		prepared = false;
		return true;
	}


	public boolean prepare() {
		refModule = EnvironmentInst.get().getModuleManager().getModule(refModuleName);

		if( refModule == null ) {
			refModuleTagName = refModuleName + "." + refTagName;
		}

		prepared = true;
		return true;
	}


	public boolean link() {
		if( !prepared )
			prepare();

		if( refModule == null ) {
			tag = EnvironmentInst.get().getModuleManager().searchExternalTag( refModuleTagName );
		} else {
			tag = refModule.getTagTable().get(refTagName);
		}

		return tag != null;
	}

//	public boolean prepareAndLink() {
//		return prepare()  &&  link();
//	}


	public boolean isValid() {
		return tag != null  &&  tag.getStatus() != Tag.Status.Deleted;
	}


	public boolean linkIfNotValid() {
		return isValid() || link();
	}



	public String getName() {
		return refModuleName + ":" + refTagName;
	}

	public String getRefModuleName() {
		return refModuleName;
	}

	public String getRefTagName() {
		return refTagName;
	}

	public Tag getTag() {
		return tag;
	}

	public boolean getBool() {
		return tag != null && tag.getBool();
	}

	public int getInt() {
		return tag==null? 0: tag.getInt();
	}

	public long getLong() {
		return tag==null? 0: tag.getLong();
	}

	public double getDouble() {
		return tag==null? 0d: tag.getDouble();
	}

	public String getString() {
		return tag==null? "": tag.getString();
	}

	public void setBool(boolean value) {
		if( tag != null )
			tag.setBool(value);
	}

	public void setInt(int value) {
		if( tag != null )
			tag.setInt(value);
	}

	public void setLong(long value) {
		if( tag != null )
			tag.setLong(value);
	}

	public void setDouble(double value) {
		if( tag != null )
			tag.setDouble(value);
	}

	public void setString(String value) throws NumberFormatException {
		if( tag != null )
			tag.setString(value);
	}


	public String check() {
		return isValid()? "": String.format("%s - %s not found", getName(), refModule == null? "module": "tagname");
	}

	public String checkLn() {
		String res = check();
		return res.isEmpty()? "": "\r\n" + res;
	}

}
