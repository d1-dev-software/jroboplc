package promauto.jroboplc.core.tags;

import promauto.jroboplc.core.api.Tag;

public class TagCallableInt extends TagBase {
	
	public interface Callback {
		int getInt();
		void setInt(int value);
	}
	
	private Callback callback = null;
	
	
	public TagCallableInt(Callback callback, String name, int value) {
		super(name, value);
		this.callback = callback;
	}


	@Override
	public Type getType() {
		return Type.INT;
	}
	
	


	@Override
	public boolean getBool() {
		return callback.getInt() > 0;
	}

	@Override
	public int getInt() {
		return callback.getInt();
	}

	@Override
	public long getLong() {
		return (long)callback.getInt();
	}

	@Override
	public double getDouble() {
		return (double)callback.getInt();
	}

	@Override
	public String getString() {
		return ""+callback.getInt();
	}
	
	@Override
	public String toString() {
		return getString();
	}


	
	
	@Override
	public void setBool(boolean value) {
		callback.setInt(value? 1: 0);
	}

	@Override
	public void setOn() {
		setInt(1);
	}

	@Override
	public void setOff() {
		setInt(0);
	}



	@Override
	public void setInt(int value) {
		callback.setInt(value);
	}

	@Override
	public void setLong(long value) {
		callback.setInt((int)value);
	}

	@Override
	public void setDouble(double value) {
		callback.setInt((int)value);
	}

	@Override
	public void setString(String value) {
		int parsed;
		try {
			parsed = Integer.parseInt(value.trim());
		} catch (NumberFormatException e) {
			parsed = 0;
		}
		callback.setInt(parsed);
	}

	
	
	
	@Override
	public boolean equalsValue(Tag tag) {
		return tag.getInt() == getInt();
	}

	@Override
	public void copyValueTo(Tag tag) {
		tag.setInt(getInt());
	}

	


}
