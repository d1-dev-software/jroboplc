package promauto.jroboplc.core.tags;

import promauto.utils.CRC;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class RefGroup {

//    public interface Factory {
//        RefGroup createInstance();
//    }
//
//    public static class FactoryImpl implements RefGroup.Factory {
//        @Override
//        public RefGroup createInstance() {
//            return new RefGroup();
//        }
//    }

    private List<RefItem> items = new LinkedList<>();
    private List<RefItem> itemsCrc = null;
    private RefItem itemCrcSum = null;

    private LocalDateTime dtRead = LocalDateTime.now();

    private boolean linked = false;

    RefGroup() {
    }

    public void clear() {
        items.clear();
        itemsCrc = null;
        itemCrcSum = null;
    }



    public LocalDateTime getDtRead() {
        return dtRead;
    }


    public RefItem createItem(String modulename, String tagname) {
        RefItem item = new RefItem(this, modulename, tagname);
        items.add(item);
        return item;
    }



    public RefItem createItemCrc(String modulename, String tagname) {
        RefItem item = createItem(modulename, tagname);
        addItemCrc(item);
        return item;
    }



    public RefItem createItemCrcSum(String modulename, String tagname) {
        RefItem item = createItem(modulename, tagname);
        itemCrcSum = item;
        return item;
    }



    public RefItem addItemCrc(RefItem item) {
        if (itemsCrc == null)
            itemsCrc = new LinkedList<>();
        itemsCrc.add(item);
        return item;
    }



//    public void removeItem(RefItem item) {
//        items.removeIf(it -> it == item);
//        if( itemsCrc != null )
//            itemsCrc.removeIf(it -> it == item);
//    }




    public void prepare() {
        for (RefItem item : items)
            item.prepare();
    }



    public boolean link() {
        for (RefItem item : items) {
            if( item.isOptional() ) {
                if( !linked  &&  !item.isLinked() )
                    item.link();
            } else
            if( !item.link() ) {
                linked = false;
                return false;
            }
        }
        linked = true;
        return true;
    }

    public boolean isLinked() {
        return linked;
    }


    public boolean read() {
        boolean result = false;
        for (RefItem item : items) {
            result |= item.read();
        }
        dtRead = LocalDateTime.now();
        return result;
    }

    public boolean linkAndRead() {
        if( !link() )
            return false;
        read();
        return true;
    }


//    public List<RefItem> getItemsNotLinked() {
//         return items.stream()
//                .filter(item -> !item.link())
//                .collect(Collectors.toList());
//    }



    public boolean checkCrc8() {
        if( itemCrcSum == null  ||  itemsCrc == null )
            return true;
        int crc = CRC.getCrc8FromWordStream( itemsCrc.stream().map(a -> a.getValue().getInt()));
        return itemCrcSum.getValue().getInt() == crc;
    }



    public boolean checkCrc16() {
        if( itemCrcSum == null  ||  itemsCrc == null )
            return true;
        int crc = CRC.getCrc16FromWordStream( itemsCrc.stream().map(a -> a.getValue().getInt()));
        return itemCrcSum.getValue().getInt() == crc;
    }


    public boolean checkCrc32() {
        if( itemCrcSum == null  ||  itemsCrc == null )
            return true;
        long crc = CRC.getOctoCrc32FromTagStream( itemsCrc.stream()
                .filter(RefItem::isLinked)
                .map(RefItem::getTag));
        return itemCrcSum.isLinked()  &&  itemCrcSum.getValue().getLong() == crc;
    }


    public void disableCrc() {
        if( itemsCrc != null )
            itemsCrc.clear();
        itemsCrc = null;
        itemCrcSum = null;
    }


    public String check() {
        return items.stream()
                .filter(item -> !item.link())
                .map(RefItem::getInfo)
                .collect(Collectors.joining("\r\n"));
    }

    public String checkln() {
        String res = check();
        return res.isEmpty()? "": "\r\n" + res;
    }

}