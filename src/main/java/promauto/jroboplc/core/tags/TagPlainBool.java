package promauto.jroboplc.core.tags;

import promauto.jroboplc.core.api.Tag;

public class TagPlainBool extends TagPlain {

	boolean value;


	public TagPlainBool(String name, boolean value) {
		super(name);
		this.value = value;
	}

	@Override
	public Type getType() {
		return Type.BOOL;
	}

	@Override
	public boolean equalsValue(Tag tag) {
		return tag.getBool() == value;
	}

	@Override
	public void copyValueTo(Tag tag) {
		tag.setBool(value);
	}


	
	
	@Override
	public boolean getBool() {
		return value;
	}

	@Override
	public int getInt() {
		return (value)? 1: 0;
	}

	@Override
	public long getLong() {
		return (value)? 1: 0;
	}

	@Override
	public double getDouble() {
		return (value)? 1.0: 0.0;
	}

	@Override
	public String getString() {
		return (value)? BOOL_ON : BOOL_OFF;
	}



	
	
	
	@Override
	public void setBool(boolean value) {
		this.value = value;
	}

	@Override
	public void setOn() {
		this.value = true;
	}

	@Override
	public void setOff() {
		this.value = false;
	}

	@Override
	public void setInt(int value) {
		this.value = value!=0;
	}

	@Override
	public void setLong(long value) {
		this.value = value!=0;
	}

	@Override
	public void setDouble(double value) {
		this.value = value!=0.0;
	}
	
	@Override
	public void setString(String value) {
		this.value = value.equals(BOOL_ON);
	}

}
