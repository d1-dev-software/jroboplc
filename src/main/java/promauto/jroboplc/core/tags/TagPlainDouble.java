package promauto.jroboplc.core.tags;

import promauto.jroboplc.core.api.Tag;

public class TagPlainDouble extends TagPlain {
	
	double value;


	public TagPlainDouble(String name, double value) {
		super(name);
		this.value = value;
	}


	@Override
	public Type getType() {
		return Type.DOUBLE;
	}

	@Override
	public boolean equalsValue(Tag tag) {
		return tag.getDouble() == value;
	}

	@Override
	public void copyValueTo(Tag tag) {
		tag.setDouble(value);
	}

	
	
	@Override
	public boolean getBool() {
		return value!=0.0;
	}

	@Override
	public int getInt() {
		return (int)value;
	}

	@Override
	public long getLong() {
		return (long)value;
	}

	@Override
	public double getDouble() {
		return value;
	}

	@Override
	public String getString() {
		return ""+value;
	}
	
	
	

	@Override
	public void setBool(boolean value) {
		this.value = value? 1.0: 0.0;
	}

	@Override
	public void setOn() {
		this.value = 1.0;
	}

	@Override
	public void setOff() {
		this.value = 0.0;
	}

	@Override
	public void setInt(int value) {
		this.value = value;
	}

	@Override
	public void setLong(long value) {
		this.value = value;
	}

	@Override
	public void setDouble(double value) {
		this.value = value;
	}

	@Override
	public void setString(String value) {
		try {
			this.value = Double.parseDouble(value.trim());
		} catch (NumberFormatException e) {
			this.value = 0.0;
		}
	}

}
