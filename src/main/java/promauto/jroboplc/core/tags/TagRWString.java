package promauto.jroboplc.core.tags;

import promauto.jroboplc.core.api.Tag;

public class TagRWString extends TagRW {

	// can be accessed from multiple threads
	private volatile String valueRd;

	// must be accessed from one thread by tag owner module only!
	private String valueWr;
	private String valueWrLast = "";
	
	

	public TagRWString(String name, String value, int flags) {
		super(name, flags);
		setReadValString(value);
	}

	@Override
	public Type getType() {
		return Type.STRING;
	}




///// general methods for multithreading operations /////

	@Override
	public boolean getBool() {
		return valueRd.equals(BOOL_ON);
	}

	@Override
	public int getInt() {
		try {
			return Integer.parseInt(valueRd);
		} catch (NumberFormatException e) {
			return 0;
		}
	}

	@Override
	public long getLong() {
		try {
			return Long.parseLong(valueRd);
		} catch (NumberFormatException e) {
			return 0L;
		}
	}

	@Override
	public double getDouble() {
		try {
			return Double.parseDouble(valueRd);
		} catch (NumberFormatException e) {
			return 0.0;
		}
	}

	@Override
	public String getString() {
		return valueRd;
	}




	@Override
	public synchronized void setBool(boolean value) {
		valueWr = value? BOOL_ON : BOOL_OFF;
		valueWrChanged = true;
	}

	@Override
	public synchronized void setOn() {
		valueWr = BOOL_ON;
		valueWrChanged = true;
	}

	@Override
	public synchronized void setOff() {
		valueWr = BOOL_OFF;
		valueWrChanged = true;
	}

	@Override
	public synchronized void setInt(int value) {
		valueWr = ""+value;
		valueWrChanged = true;
	}

	@Override
	public synchronized void setLong(long value) {
		valueWr = ""+value;
		valueWrChanged = true;
	}

	@Override
	public synchronized void setDouble(double value) {
		valueWr = ""+value;
		valueWrChanged = true;
	}

	@Override
	public synchronized void setString(String value) {
		if( value.length() <= STRING_LENGTH_MAX )
			valueWr = value;
		else
			valueWr = value.substring(0, STRING_LENGTH_MAX);
		valueWrChanged = true;
	}





///// Methods below are suppossed to be used by tag owner module only!
///// Must be invoked from single thread!

	@Override
	public boolean getWriteValBool() {
		return valueWrLast.equals(BOOL_ON);
	}

	@Override
	public int getWriteValInt() {
		try {
			return Integer.parseInt(valueWrLast);
		} catch (NumberFormatException e) {
			return 0;
		}
	}

	@Override
	public long getWriteValLong() {
		try {
			return Long.parseLong(valueWrLast);
		} catch (NumberFormatException e) {
			return 0L;
		}
	}

	@Override
	public double getWriteValDouble() {
		try {
			return Double.parseDouble(valueWrLast);
		} catch (NumberFormatException e) {
			return 0.0;
		}
	}
	
	@Override
	public String getWriteValString() {
		return valueWrLast;
	}
	
	
	
	
	@Override
	public void setReadValBool(boolean value) {
		valueRd = value? BOOL_ON : BOOL_OFF;
	}

	@Override
	public void setReadValInt(int value) {
		valueRd = ""+value;
	}

	@Override
	public void setReadValLong(long value) {
		valueRd = ""+value;
	}

	@Override
	public void setReadValDouble(double value) {
		valueRd = ""+value;
	}

	@Override
	public void setReadValString(String value) {
		if( value.length() <= STRING_LENGTH_MAX )
			valueRd = value;
		else
			valueRd = value.substring(0, STRING_LENGTH_MAX);
	}



	
	@Override
	public boolean equalsValue(Tag tag) {
		return tag.getString() == valueRd;
	}

	@Override
	public void copyValueTo(Tag tag) {
		tag.setString(valueRd);
	}



	
	@Override
	protected void copyWriteToLastWrite() {
		valueWrChanged = false;
		valueWrLast = valueWr;
	}
	
	@Override
	protected void copyWriteToRead() {
		valueRd = valueWr;
	}

	@Override
	public void copyLastWriteToRead() {
		valueRd = valueWrLast;
	}


}
