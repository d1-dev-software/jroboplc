package promauto.jroboplc.core.tags;

import promauto.jroboplc.core.api.EnvironmentInst;
import promauto.jroboplc.core.api.Module;
import promauto.jroboplc.core.api.Tag;

public class RefItem {

    private RefGroup parent;

    private Module module = null;
    private Tag tag = null;
    private Tag value = null;

    private String modulename;
    private String tagname;
    private String moduletagname = null;

    private boolean external = false;
    private boolean optional = false;

    public RefItem(RefGroup parent, String modulename, String tagname) {
        this.parent = parent;
        this.modulename = modulename;
        this.tagname = tagname;
    }


    public void prepare() {
        external = false;
        module = EnvironmentInst.get().getModuleManager().getModule(modulename);
        if (module == null) {
            moduletagname = (modulename.isEmpty()? "": modulename + '.') + tagname;
            external = true;
        }
        tag = null;
    }


    public boolean link() {
        if (tag != null) {
            if (tag.getStatus() == Tag.Status.Deleted)
                prepare();
            else
                return true;
        }

        if( external ) {
            for(Module extmod: EnvironmentInst.get().getModuleManager().getModulesWithExternalTags())
                if ((tag = extmod.getTagTable().get(moduletagname)) != null) {
                    module = extmod;
                    break;
                }

            if (tag == null)
                return false;

        } else {
            if (module == null  ||  (tag = module.getTagTable().get(tagname)) == null)
                return false;
        }

        value = TagPlain.create(tag);
        tag.copyValueTo(value);

        return true;
    }


    public boolean isLinked() {
        return tag != null;
    }


    public boolean read() {
        if( !isLinked() )
            return false;

        if(tag.equalsValue(value))
            return false;

        tag.copyValueTo(value);
        return true;
    }


    public Tag getTag() {
        return tag;
    }


    public Tag getValue() {
        return value;
    }

    public Module getModule() {
        return module;
    }

    public boolean isOptional() {
        return optional;
    }

    public void setOptional(boolean optional) {
        this.optional = optional;
    }

    public String getInfo() {
        if( isLinked() )
            return module.getName() + ":" + (external? moduletagname: tagname) + " - ok";
        else
            return ( external? "?:" + moduletagname: modulename + ":" + tagname) + " - nolink" + (optional? " (optional)": "");
    }

}
