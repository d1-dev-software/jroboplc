package promauto.jroboplc.core.tags;

import promauto.jroboplc.core.api.Tag;

public class TagInt extends TagBase {
	
	private volatile int value;  
	
	
	public TagInt(String name, int value) {
		super(name);
		setInt(value);
	}

	public TagInt(String name, int value, int flags) {
		super(name, flags);
		setInt(value);
	}


	@Override
	public Type getType() {
		return Type.INT;
	}
	
	


	@Override
	public boolean getBool() {
		return value!=0;
	}

	@Override
	public int getInt() {
		return value;
	}

	@Override
	public long getLong() {
		return (long)value;
	}

	@Override
	public double getDouble() {
		return (double)value;
	}

	@Override
	public String getString() {
		return ""+value;
	}
	


	
	
	@Override
	public void setBool(boolean value) {
		if( this.value != (value? 1: 0) )
			this.value = value? 1: 0;
	}

	@Override
	public void setOn() {
		if( this.value != 1 )
			this.value = 1;
	}

	@Override
	public void setOff() {
		if( this.value != 0 )
			this.value = 0;
	}


	@Override
	public void setInt(int value) {
		if( this.value != value )
			this.value = value;
	}

	@Override
	public void setLong(long value) {
		if( this.value != (int)value )
			this.value = (int)value;
	}

	@Override
	public void setDouble(double value) {
		if( this.value != (int)value )
			this.value = (int)value;
	}

	@Override
	public void setString(String value) {
		int parsed;
		try {
			parsed = Integer.parseInt(value.trim());
		} catch (NumberFormatException e) {
			parsed = 0;
		}
		if( this.value != parsed )
			this.value = parsed;
	}

	
	@Override
	public boolean equalsValue(Tag tag) {
		return tag.getInt() == value;
	}

	@Override
	public void copyValueTo(Tag tag) {
		tag.setInt(value);
	}

	


}
