package promauto.jroboplc.core.tags;

import promauto.jroboplc.core.api.Tag;

public class TagDouble extends TagBase {
	
	private volatile double value;  
	
	
	public TagDouble(String name, double value) {
		super(name);
		setDouble(value);
	}

	public TagDouble(String name, double value, int flags) {
		super(name, flags);
		setDouble(value);
	}


	@Override
	public Type getType() {
		return Type.DOUBLE;
	}
	
	


	@Override
	public boolean getBool() {
		return value!=0.0;
	}

	@Override
	public int getInt() {
		return (int)value;
	}

	@Override
	public long getLong() {
		return (long)value;
	}

	@Override
	public double getDouble() {
		return value;
	}

	@Override
	public String getString() {
		return ""+value;
	}
	

	
	

	@Override
	public void setBool(boolean value) {
		if( this.value != (value? 1.0: 0.0) )
			this.value = value? 1.0: 0.0;
	}

	@Override
	public void setOn() {
		if( this.value != 1.0 )
			this.value = 1.0;
	}

	@Override
	public void setOff() {
		if( this.value != 0.0 )
			this.value = 0.0;
	}



	@Override
	public void setInt(int value) {
		if( this.value != (double)value )
			this.value = value;
	}

	@Override
	public void setLong(long value) {
		if( this.value != (long)value )
			this.value = value;
	}

	@Override
	public void setDouble(double value) {
		if( this.value != value )
			this.value = value;
	}

	@Override
	public void setString(String value) {
		double parsed;
		try {
			parsed = Double.parseDouble(value.trim());
		} catch (NumberFormatException e) {
			parsed = 0.0;
		}

		if( this.value != parsed )
			this.value = parsed;
	}

	
	@Override
	public boolean equalsValue(Tag tag) {
		return tag.getDouble() == value;
	}

	@Override
	public void copyValueTo(Tag tag) {
		tag.setDouble(value);
	}

	


}
