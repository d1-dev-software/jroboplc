package promauto.jroboplc.core;

import java.util.Scanner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import promauto.jroboplc.core.api.Console;
import promauto.jroboplc.core.api.Environment;
import promauto.utils.Strings;

public class ConsoleInteract extends AbstractConsole {

	final Logger logger = LoggerFactory.getLogger(ConsoleInteract.class);

	private Scanner scan;
	
	
	public ConsoleInteract(Environment env) {
		super(env);
		scan = new Scanner(System.in, "UTF-8");
	}
	

	@Override
	public boolean initialize() {
		// add commands
		return true;
	}

	@Override
	public void close() {
	}

	@Override
	public void process() {
		print(getPrompt());
		
		String request = "";
		
		try {
			request = scan.nextLine();
		} catch (Exception e) {
			env.setTerminated();
			System.out.println("Input canceled!");
		}

        if( !request.trim().isEmpty() )
    		logger.info("[INPUT] " + request);

		print( env.getCmdDispatcher().execute( this, request ));
		print("\r\n");

		afterRequestExecuted(request);
	}

	@Override
	public synchronized void print(String text) {
		System.out.print( parseAnsi(text) );
	}


}
