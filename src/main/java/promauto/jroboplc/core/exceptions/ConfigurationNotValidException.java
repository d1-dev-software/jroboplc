package promauto.jroboplc.core.exceptions;

public class ConfigurationNotValidException extends Exception {
    public ConfigurationNotValidException(String message) {
        super(message);
    }
}
