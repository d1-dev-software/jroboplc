package promauto.jroboplc.core;

import promauto.jroboplc.core.api.EnvironmentInst;

import java.io.InputStream;
import java.net.URLClassLoader;
import java.util.Properties;

public class Version {


	private static final String RESOURCE_NAME = "version";
	private static String value = "";

	public static String getVersionStr() {

		if( value.isEmpty() ) {
			try (InputStream in = Version.class.getClassLoader().getResourceAsStream(RESOURCE_NAME)) {
				if (in != null) {
					Properties props = new Properties();
					props.load(in);
					value = props.getProperty("version", "");
				}
			} catch (Exception e) {
				EnvironmentInst.get().printError(null, e, "Resource:", RESOURCE_NAME);
			}
		}

		return value;
	}

}
