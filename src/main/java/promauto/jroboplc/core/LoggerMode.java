package promauto.jroboplc.core;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import promauto.jroboplc.core.api.Configuration;
import promauto.jroboplc.core.api.EnvironmentInst;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

public class LoggerMode {

    final Logger logger = LoggerFactory.getLogger(LoggerMode.class);

    LocalDate ld = null;
    boolean fl = false;
    int val = 0;


    // "log.mode" format after parsed from octal:
    //     ccyymmdd
    //
    // where cc - control sum of yy+mm+dd
    //
    public LoggerMode() {
        Configuration cm = EnvironmentInst.get().getConfiguration();
        String s = cm.get(cm.getRoot(), "log.mode", "" );

        s = s.trim();

        if( s.isEmpty() ) {
            fl = true;
            ld = null;
            val = 0;
        } else {
            int a = Integer.parseInt(s.replace(",", ""), 8);
            int[] b = new int[4];
            for (int i = 0; i < 4; ++i) {
                b[i] = a % 100;
                a = a / 100;
            }

            fl = b[0] + b[1] + b[2] == b[3];
            if (fl) {
                b[2] += 2000;
                ld = LocalDate.of(b[2], b[1], b[0]);
                val = b[2] * 10000 + b[1] * 100 + b[0];
            } else {
                EnvironmentInst.get().printError(logger, "Bad logger mode!");
            }
        }

    }


    public int get() {
        return val;
    }

    public boolean isOk() {
        if( fl ) {
            if( ld==null )
                return true;
            else {
                long d = LocalDate.now().until(ld, ChronoUnit.DAYS);
                if( d > 0 )
                    return true;
            }
        }
        return false;
    }
}
