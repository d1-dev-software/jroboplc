package promauto.jroboplc.core;

import promauto.jroboplc.core.api.Console;

public class ConsoleDaemon implements Console {

	@Override
	public boolean initialize() {
		return true;
	}

	@Override
	public void close() {
	}

	@Override
	public void process() {
	}

	@Override
	public synchronized void print(String text) {
		System.out.print( parseAnsi(text) );
	}

	@Override
	public String getPrompt() {
		return "";
	}

	@Override
	public String getProperty(String property_id, String default_value) {
		return default_value;
	}

	@Override
	public void setProperty(String property_id, String value) {
	}

	@Override
	public int getProperty(String property_id, int default_value) {
		return default_value;
	}

	@Override
	public String parseAnsi(String text) {
		return text;
	}

	@Override
	public String startRepeatingLastRequest(long period) {
		return "";
	}

	@Override
	public void stopRepeatingLastRequest() {
	}


}
