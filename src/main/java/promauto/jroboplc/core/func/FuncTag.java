package promauto.jroboplc.core.func;

import promauto.jroboplc.core.api.Configuration;
import promauto.jroboplc.core.api.EnvironmentInst;
import promauto.jroboplc.core.api.Module;
import promauto.jroboplc.core.api.Tag;

public abstract class FuncTag {
    private final boolean beforeExecute;

    public static FuncTag create(Module module, String tagname, Object conf) throws Exception {
        String func  = EnvironmentInst.get().getConfiguration().get(conf, "func",	"");
        FuncTag ft;
        if(func.equals("avg")) {
            ft = new FuncTagAvg(module, tagname, conf);
        } else {
            throw new Exception("Unknown func: " + func);
        }
        module.getTagTable().add(ft.getTag());
        return ft;
    }

    public FuncTag(Object conf) {
        beforeExecute  = EnvironmentInst.get().getConfiguration().get(conf, "beforeExecute",false);
    }

    public boolean isBeforeExecute() {
        return beforeExecute;
    }

    public boolean prepare() {
        return true;
    }

    public abstract void execute();

    public abstract Tag getTag();
}
