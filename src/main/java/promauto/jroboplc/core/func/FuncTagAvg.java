package promauto.jroboplc.core.func;

import promauto.jroboplc.core.api.Configuration;
import promauto.jroboplc.core.api.EnvironmentInst;
import promauto.jroboplc.core.api.Module;
import promauto.jroboplc.core.api.Tag;
import promauto.jroboplc.core.tags.Ref;
import promauto.jroboplc.core.tags.TagRW;

import java.util.LinkedList;
import java.util.Queue;

public class FuncTagAvg extends FuncTag {

    private static class Rec {
        long time;
        double value;

        public Rec(long time, double value) {
            this.time = time;
            this.value = value;
        }
    }

    private final TagRW tag;
    private final Ref refInput;
    private final long period;

    private final Queue<Rec> recs;


    public FuncTagAvg(Module module, String tagname, Object conf) {
        super(conf);
        recs = new LinkedList<>();

        Configuration cm = EnvironmentInst.get().getConfiguration();

        tag = TagRW.create(Tag.Type.INT, tagname, 0);

        refInput = EnvironmentInst.get().getRefFactory().createRef();
        refInput.init(conf, "input", module);

        period  = Math.max(0, cm.get(conf, "period",0));

    }

    @Override
    public boolean prepare() {
        return refInput.prepare();
    }

    @Override
    public void execute() {
        if( !refInput.linkIfNotValid() )
            return;

        long time = System.currentTimeMillis();
        recs.offer(new Rec(time, refInput.getDouble()));

        time -= period;
        while( recs.size() > 0  &&  recs.peek().time < time )
            recs.poll();

        double sum = recs.stream().mapToDouble(rec -> rec.value).sum();
        tag.setReadValDouble( sum / recs.size() );
    }

    @Override
    public Tag getTag() {
        return tag;
    }
}
