package promauto.jroboplc.core;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import promauto.jroboplc.core.api.EnvironmentInst;
import promauto.jroboplc.core.api.Tag;
import promauto.jroboplc.core.tags.*;

public class TagTable {

	final Logger logger = LoggerFactory.getLogger(TagTable.class);

	private final Map<String,Tag> tagmap;
	
	
	public TagTable() {
		this.tagmap = new ConcurrentHashMap<>();
	}

	public Tag get(String name) {
		return tagmap.get(name);
	}

	
	public Collection<Tag> values() {
		return tagmap.values();
	}


	public List<Tag> getTags(String filter, boolean sorted) {
		Pattern p1 = null;
		try {
			if (!filter.isEmpty())
				p1 = Pattern.compile(filter);
		} catch (PatternSyntaxException e) {
			EnvironmentInst.get().printError(logger, e, "Pattern error:", filter);
		}
		final Pattern p = p1;

		if( sorted ) {
			Set<String> tagnames = new TreeSet<>(tagmap.keySet());
			return tagnames.stream()
					.filter(tagname -> p == null || p.matcher(tagname).matches())
					.map(tagname -> tagmap.get(tagname))
					.collect(Collectors.toList());
		} else {
//			return tagmap.values().stream()
//					.filter(tag -> p == null || p.matcher(tag.getName()).matches())
//					.collect(Collectors.toList());
			List<Tag> res = tagmap.values().stream()
					.filter(tag -> p == null || p.matcher(tag.getName()).matches())
					.collect(Collectors.toList());
			return res;
		}
	}

	
	public Map<String,Tag> getMap() {
		return Collections.unmodifiableMap(tagmap);
	}


	public int getSize() {
		return tagmap.size();
	}


	public <T extends Tag> T add(T tag) {
		if( tagmap.put(tag.getName(), tag) != null )
			EnvironmentInst.get().printError(logger, "Duplicate: " + tag.getName() );
		return tag;
	}


	public boolean remove(String tagname) {
		Tag tag = tagmap.get(tagname);
		if( tag == null )
			return false;

		tag.setStatus( Tag.Status.Deleted );

		tagmap.remove(tagname);
		return true;
	}


	public boolean remove(Tag tag) {
		return remove(tag.getName());
	}


	public void add(List<Tag> taglist) {
		taglist.forEach(tag -> add(tag));
	}

	public void remove(List<Tag> taglist) {
		taglist.forEach(tag -> remove(tag));
	}


	public void clear() {
		tagmap.clear();
	}


	public void removeAll() {
		tagmap.entrySet().stream()
				.forEach(ent -> ent.getValue().setStatus(Tag.Status.Deleted));

		tagmap.clear();
	}


	// transfer tags routines
	public Tag findByNameOrAddToList(Tag tag, List<Tag> list) {
		Tag found = get( tag.getName() );
		if( found != null )
			return found;

		list.add(tag);
		return tag;
	}


	public void setTagState(Tag.Status status, int flags) {
		tagmap.values().stream()
				.filter(tag -> tag.hasFlags(flags))
				.forEach(tag -> tag.setStatus(status));
	}





	// Base tags
	public Tag createTag(String strTagtype, String name, String value, int flags) {
		try {
			Tag.Type tagtype = Tag.Type.valueOf( strTagtype.toUpperCase() );
			Tag tag = createTag(tagtype, name, flags);

			if( !value.isEmpty() )
				tag.setString(value);

			return tag;
		} catch(Exception e) {
			EnvironmentInst.get().printError(logger, e, strTagtype, name, value);
		}
		return null;
	}

	public Tag createTag(String strTagtype, String name, String value) {
		return createTag(strTagtype, name, value, 0);
	}

	public Tag createTag(Tag.Type tagtype, String name, int flags) {
		return add( TagBase.create(tagtype, name, flags));
	}

	public Tag createBool(String name, boolean value, int flags) {
		return add(new TagBool(name, value, flags));
	}

	public Tag createInt(String name, int value, int flags) {
		return add(new TagInt(name, value, flags));
	}

	public Tag createLong(String name, long value, int flags) {
		return add(new TagLong(name, value, flags));
	}

	public Tag createDouble(String name, double value, int flags) {
		return add(new TagDouble(name, value, flags));
	}

	public Tag createString(String name, String value, int flags) {
		return add(new TagString(name, value, flags));
	}


	public Tag createTag(Tag.Type tagtype, String name) {
		return add( TagBase.create(tagtype, name, 0));
	}

	public Tag createBool(String name, boolean value) {
		return add(new TagBool(name, value, 0));
	}

	public Tag createInt(String name, int value) {
		return add(new TagInt(name, value, 0));
	}

	public Tag createLong(String name, long value) {
		return add(new TagLong(name, value, 0));
	}

	public Tag createDouble(String name, double value) {
		return add(new TagDouble(name, value, 0));
	}

	public Tag createString(String name, String value) {
		return add(new TagString(name, value, 0));
	}



	// RW tags
	public TagRW createTagRW(Tag.Type tagtype, String name, int flags) {
		TagRW tag = TagRW.create(tagtype, name, flags);
		return add( tag );
	}

	public TagRW createRWBool(String name, boolean value, int flags) {
		return add(new TagRWBool(name, value, flags));
	}

	public TagRW createRWInt(String name, int value, int flags) {
		return add(new TagRWInt(name, value, flags));
	}

	public TagRW createRWLong(String name, long value, int flags) {
		return add(new TagRWLong(name, value, flags));
	}

	public TagRW createRWDouble(String name, double value, int flags) {
		return add(new TagRWDouble(name, value, flags));
	}

	public TagRW createRWString(String name, String value, int flags) {
		return add(new TagRWString(name, value, flags));
	}


	public TagRW createTagRW(Tag.Type tagtype, String name) {
		TagRW tag = TagRW.create(tagtype, name, 0);
		return add( tag );
	}

	public TagRW createRWBool(String name, boolean value) {
		return add(new TagRWBool(name, value, 0));
	}

	public TagRW createRWInt(String name, int value) {
		return add(new TagRWInt(name, value, 0));
	}

	public TagRW createRWLong(String name, long value) {
		return add(new TagRWLong(name, value, 0));
	}

	public TagRW createRWDouble(String name, double value) {
		return add(new TagRWDouble(name, value, 0));
	}

	public TagRW createRWString(String name, String value) {
		return add(new TagRWString(name, value, 0));
	}


	public Tag getOrCreateBool(String name, boolean value) {
		Tag tag = tagmap.get(name);
		return tag != null? tag: createBool(name, value);
	}

	public Tag getOrCreateInt(String name, int value) {
		Tag tag = tagmap.get(name);
		return tag != null? tag: createInt(name, value);
	}

	public Tag getOrCreateLong(String name, long value) {
		Tag tag = tagmap.get(name);
		return tag != null? tag: createLong(name, value);
	}

	public Tag getOrCreateDouble(String name, double value) {
		Tag tag = tagmap.get(name);
		return tag != null? tag: createDouble(name, value);
	}

	public Tag getOrCreateString(String name, String value) {
		Tag tag = tagmap.get(name);
		return tag != null? tag: createString(name, value);
	}

    public void acceptWriteValues() {
		tagmap.values().stream()
				.filter(tag -> tag instanceof TagRW)
				.forEach(tag -> ((TagRW) tag).acceptWriteValue());
    }

	public void copyValuesTo(TagTable dst) {
		dst.values().forEach(tag -> {
			Tag found = get(tag.getName());
			if( found != null  &&  found.getStatus() != Tag.Status.Deleted )
				found.copyValueTo(tag);
		});
	}


	public void moveTagsTo(TagTable dst) {
		values().forEach(tag -> dst.add(tag));
		clear();
	}
}
