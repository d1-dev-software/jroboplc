package promauto.jroboplc.plugin.tcpconsole;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import promauto.jroboplc.core.AbstractModule;
import promauto.jroboplc.core.api.Configuration;
import promauto.jroboplc.core.api.Console;
import promauto.jroboplc.core.api.Plugin;
import promauto.jroboplc.core.api.TcpServerChannel;
import promauto.jroboplc.core.api.TcpServerPort;
import promauto.jroboplc.core.api.TcpSubscriber;

public class TcpConsoleModule extends AbstractModule implements TcpSubscriber {
	public static final String ENTER_PASSWORD = "Enter password: ";

	private final Logger logger = LoggerFactory.getLogger(TcpConsoleModule.class);
	
	protected int portnum;
	protected String welcome;
	protected String password;
	protected boolean needPassword;
	protected List<String> listForbidden = null;
	protected Pattern regexForbidden = null;
	
	private Set<TcpConsoleClient> clients = new HashSet<>();
	

	
	public TcpConsoleModule(Plugin plugin, String name) {
		super(plugin, name);
		taskable = false;
	}

	
	
	public boolean loadModule(Object conf) {
		Configuration cm = env.getConfiguration();

		portnum = cm.get(conf, "portnum", 0);
		welcome = cm.get(conf, "welcome", "Welcome to JRoboPLC!\r\n(use 'h' for help)") + "\r\n";
		password = cm.get(conf, "password", "");
		
		listForbidden = null;
		regexForbidden = null;
		Object forbidden = cm.get(conf, "forbidden");
		
		if( forbidden != null ) {
			if( forbidden instanceof List) {
				listForbidden = new LinkedList<>();
				cm.toList( cm.get(conf, "forbidden") ).stream().forEach(s -> listForbidden.add(s.toString()));
			} 
			
			else if( !forbidden.toString().isEmpty() ) {
				try {
					regexForbidden = Pattern.compile( forbidden.toString() );
				} catch( PatternSyntaxException e) {
					env.printError(logger, e, name);
					return false;
				}
			}
		}
		
		
		return true;
	}

	
	
	@Override
	public boolean prepareModule() {
		if( env.getTcpServer().subscribe(portnum, this) )
			return true;

		env.printError(logger, name);
		return false;
	}

	

	
	@Override
	public String getInfo() {
		return enable? "p" + portnum: "disabled";
	}


	
	@Override
	public boolean onTcpServerClientConnected(TcpServerChannel channel) {
		env.printInfo(logger, name, "Connected:", channel.getRemoteAddress().getAddress().getHostAddress() );
		
		TcpConsoleClient client = new TcpConsoleClient(env, this, channel);
		client.setProperty("ansi", Console.PROP_ON);
		env.getConsoleManager().addConsole(client);
		channel.setAttachedObject(client);
		clients.add(client);
		
		needPassword = !password.isEmpty();
		channel.write( welcome  + (needPassword? ENTER_PASSWORD: client.getPrompt()) );
		
		return true;
	}







	@Override
	public boolean onTcpServerClientDisconnected(TcpServerChannel channel) {
		env.printInfo(logger, name, "Disconnected:", channel.getRemoteAddress().getAddress().getHostAddress() );

		Object obj = channel.getAttachedObject();
		if (obj!=null  &&  obj instanceof TcpConsoleClient) {
			TcpConsoleClient client = (TcpConsoleClient)obj;
			client.onDisconnect();
			env.getConsoleManager().removeConsole(client);
			return clients.remove(client);
		} else
			return false;
	}



	@Override
	public boolean onTcpServerRequest(TcpServerChannel channel, String request) {
		if (channel.getAttachedObject()!=null  &&  
				channel.getAttachedObject() instanceof TcpConsoleClient)
			((TcpConsoleClient)channel.getAttachedObject()).onRequest(channel, request);
		return true;
	}





	@Override
	protected boolean reload() {

		TcpConsoleModule tmp = new TcpConsoleModule(plugin, name);
		if( !tmp.load() )
			return false;

		TcpServerPort port_old = env.getTcpServer().getPortBySubscriber(this);
		TcpServerPort port_new = env.getTcpServer().getPortByNum(tmp.portnum); 
		if( tmp.enable  &&  port_new == null ) {
			env.printError(logger, name, "Port not found: " + tmp.portnum);
			return false;
		}

		copySettingsFrom(tmp);
		int portnum_old = portnum;
		portnum = tmp.portnum;
		welcome = tmp.welcome;
		password = tmp.password;
		listForbidden = tmp.listForbidden;
		regexForbidden = tmp.regexForbidden;

		if( enable  &&  env.isRunning()  &&  port_new != null  &&  port_old != port_new )
			port_new.setSubscriber(this);

		if( port_old != null  &&  port_old != port_new ) { 
			env.getTcpServer().disconnect(portnum_old);
			port_old.setSubscriber(null);
		}
		
		return true;
	}



	
	
	

}