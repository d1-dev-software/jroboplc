package promauto.jroboplc.plugin.jrbustcp;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandler;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.ipfilter.IpFilterRuleType;
import io.netty.handler.ipfilter.IpSubnetFilter;
import io.netty.handler.ipfilter.IpSubnetFilterRule;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import io.netty.handler.ssl.SslContext;
import io.netty.handler.ssl.SslContextBuilder;
import io.netty.handler.ssl.util.SelfSignedCertificate;
import io.netty.util.AttributeKey;
import io.netty.util.ResourceLeakDetector;
import io.netty.util.concurrent.GlobalEventExecutor;
import org.apache.log4j.BasicConfigurator;
import promauto.jroboplc.core.AbstractModule;
import promauto.jroboplc.core.api.ANSI;
import promauto.jroboplc.core.api.Configuration;
import promauto.jroboplc.core.api.Plugin;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class JrbustcpServerModule extends AbstractModule  {

    protected static final AttributeKey<ServerSession> CHDATA = AttributeKey.valueOf("CHDATA");

    private int port;
    private boolean ssl;
    private String compress;
    private int idleTimeout;
    private boolean logging;
    private boolean auth;
    private IpSubnetFilter ipSubnetFilter;

    private EventLoopGroup bossGroup = null;
    private EventLoopGroup workerGroup = null;

    private final ChannelGroup clientChannels =
            new DefaultChannelGroup(GlobalEventExecutor.INSTANCE);

    private boolean bound;


    protected JrbustcpServerModule(Plugin plugin, String name) {
        super(plugin, name);
        taskable = false;
    }

    public int getPort() {
        return port;
    }


    public String getCompress() {
        return compress;
    }

    public int getIdleTimeout() {
        return idleTimeout;
    }

    public boolean isLogging() {
        return logging;
    }

    public boolean isAuth() {
        return auth;
    }

    public void setAuth(boolean auth) {
        this.auth = auth;
    }

    public ChannelHandler getIpSubnetFilter() {
        return ipSubnetFilter;
    }


    public ChannelGroup getClientChannels() {
        return clientChannels;
    }


    @Override
    protected boolean loadModule(Object conf) {
        Configuration cm = env.getConfiguration();

        port = cm.get(conf, "port", 0);
        ssl = cm.get(conf, "ssl", false);
        compress = cm.get(conf, "compress", "");
        idleTimeout = cm.get(conf, "idleTimeout_s", 180);
        logging = cm.get(conf, "logging", false);
        auth = cm.get(conf, "auth", false);

        ipSubnetFilter = createIpFilter( cm.getStringList( conf, "ipfilter"));

        bound = false;
        return true;
    }


    private IpSubnetFilter createIpFilter(List<String> strRules) {
        // Format examples:
        //   192.168.1.0/24 accept
        //   192.168.1.101 deny
        //   deny
        Pattern p = Pattern.compile("^((?:[0-9]{1,3}\\.){3}[0-9]{1,3}){0,1}(?:\\/(\\d*)){0,1}\\s*(accept|deny)$");

        List<IpSubnetFilterRule> rules = new ArrayList<>();
        boolean acceptIfNotFound = true;
        for(String strRule: strRules) {
            Matcher m = p.matcher(strRule);
            if( m.matches() ) {
                String ip = m.group(1);
                int cidr = m.group(2) == null? 32: Integer.parseInt(m.group(2));
                IpFilterRuleType type = m.group(3).equals("accept")? IpFilterRuleType.ACCEPT: IpFilterRuleType.REJECT;
                if( ip == null ) {
                    acceptIfNotFound = type == IpFilterRuleType.ACCEPT;
                } else {
                    rules.add(new IpSubnetFilterRule(ip, cidr, type));
                }
            } else {
                env.printError(logger, name, "Bad rule in ipfilter: " + strRule);
            }
        }
        return new IpSubnetFilter(acceptIfNotFound, rules);
    }


    @Override
    public String getInfo() {
        if( enable ) {
            List<String[]> list = new ArrayList<>();

            String s = String.format("%sp%d%s%s%s",
                    bound?"": ANSI.RED + ANSI.BOLD + "NOT BOUND! " + ANSI.RESET,
                    port,
                    ssl? " SSL": "",
                    compress.isEmpty()? "": " "+compress,
                    auth? " AUTH": ""
            );
            list.add(new String[]{s, "", ""});

            long totalRead = 0;
            long totalWritten = 0;

            for(Channel ch: clientChannels) {
                String[] r = new String[3];
                list.add(r);
                ServerSession sess = ch.attr(CHDATA).get();
                r[0] = ch.remoteAddress() + " " + sess.getInfo();
                long rd = sess.getTrafficHandler().getCounterRead();
                long wr = sess.getTrafficHandler().getCounterWritten();
                r[1] = rd + "";
                r[2] = wr + "";
                totalRead += rd;
                totalWritten += wr;
            }

            String[] r = list.get(0);
            r[1] = totalRead + "";
            r[2] = totalWritten + "";

            int[] lens = new int[]{0,0,0};
            list.stream().forEach(ss -> {
                for(int i=0; i<3; ++i)
                    lens[i] = Math.max(lens[i], ss[i].length());
            });

            StringBuilder sb = new StringBuilder();
            list.stream().forEach(ss -> {
                String fmt = "%-"+lens[0]+"s   %s%s%"+lens[1]+"s / %"+lens[2]+"s B/s%s\r\n";
                String row = String.format(fmt,
                        ss[0],
                        sb.length()>0?"": ANSI.BOLD,
                        ANSI.GREEN,
                        ss[1],
                        ss[2],
                        ANSI.RESET);
                sb.append(row);
            });

            return sb.toString();
        } else
            return "disabled";
    }


    @Override
    protected boolean prepareModule() {
        if( logging ) {
            BasicConfigurator.configure();
            ResourceLeakDetector.setLevel(ResourceLeakDetector.Level.PARANOID);
        }

        try {
            SslContext sslCtx = null;
            if (ssl) {
                SelfSignedCertificate ssc = new SelfSignedCertificate();
                sslCtx = SslContextBuilder.forServer(ssc.certificate(), ssc.privateKey()).build();
            }

            if( bossGroup == null)
                bossGroup = new NioEventLoopGroup(1);

            if( workerGroup == null)
                workerGroup = new NioEventLoopGroup();

            ServerBootstrap b = new ServerBootstrap();
            b.group(bossGroup, workerGroup)
                    .channel(NioServerSocketChannel.class)
                    .handler(new LoggingHandler(LogLevel.INFO))
                    .childHandler(new ServerInitializer(this, sslCtx));

            b.bind(port).sync();
            bound = true;
        } catch (Exception e) {
            bound = false;
            env.printError(logger, e, name);
        }

        return true;
    }


    @Override
    protected boolean closedownModule() {
        if(bossGroup != null  &&  workerGroup != null ) {
            bossGroup.shutdownGracefully();
            workerGroup.shutdownGracefully();

            try {
                bossGroup.terminationFuture().sync();
                workerGroup.terminationFuture().sync();
            } catch (InterruptedException e) {
                env.printError(logger, e, name);
            }

            bossGroup = null;
            workerGroup = null;
        }
        bound = false;

        return true;
    }


    @Override
    protected boolean reload() {
        JrbustcpServerModule tmp = new JrbustcpServerModule(plugin, name);
        if( !tmp.load() )
            return false;

        closedownModule();

        copySettingsFrom(tmp);

        port          = tmp.port;
        ssl           = tmp.ssl;
        compress      = tmp.compress;
        idleTimeout   = tmp.idleTimeout;
        auth          = tmp.auth;
        ipSubnetFilter = tmp.ipSubnetFilter;

        if( enable  &&  env.isRunning() )
            prepareModule();

        return true;
    }


}
