package promauto.jroboplc.plugin.jrbustcp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import promauto.jroboplc.core.AbstractModule;
import promauto.jroboplc.core.AbstractPlugin;
import promauto.jroboplc.core.api.Module;

public class JrbustcpPlugin extends AbstractPlugin {
	private final Logger logger = LoggerFactory.getLogger(JrbustcpPlugin.class);

	private static final String PLUGIN_NAME = "jrbustcp";
	
	@Override
	public void initialize() {
		super.initialize();
    }
	
	@Override
    public String getPluginName() {
    	return PLUGIN_NAME;
    }

	@Override
    public String getPluginDescription(){
    	return "JrBusTcp client/server support";
    }
	

	@Override
	public Module createModule(String name, Object conf) {
		String modtype = env.getConfiguration().get(conf, "type", "").toLowerCase();

		AbstractModule m = null;
		switch (modtype) {
			case "server":
				m = new JrbustcpServerModule(this, name);
				break;

			case "client":
				m = new JrbustcpClientModule(this, name);
				break;

			default:
				env.printError(logger, name, "Unknown module type:", modtype);
				return null;
		}

		if( !m.load(conf) )
			return null;

		modules.add(m);

		return m;
	}

}
