package promauto.jroboplc.plugin.jrbustcp;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import promauto.jroboplc.core.api.Environment;
import promauto.jroboplc.core.api.EnvironmentInst;

public class ClientHandler extends ChannelInboundHandlerAdapter {
    private final Logger logger = LoggerFactory.getLogger(ServerHandler.class);
    private Environment env;
    private JrbustcpClientModule module;

    public ClientHandler(JrbustcpClientModule module) {
        this.module = module;
        this.env = EnvironmentInst.get();
    }

    private String getAddress(ChannelHandlerContext ctx) {
        return ctx.channel().localAddress().toString();
    }



    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        ByteBuf buf = (ByteBuf) msg;
        module.answer.offer(buf);
    }


    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        env.printError(logger, cause, module.getName(), getAddress(ctx));
        ctx.close();
    }



}
