package promauto.jroboplc.plugin.jrbustcp;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufUtil;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.timeout.IdleStateEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import promauto.jroboplc.core.api.Environment;
import promauto.jroboplc.core.api.EnvironmentInst;

import static promauto.jroboplc.plugin.jrbustcp.JrbustcpServerModule.CHDATA;


public class ServerHandler extends ChannelInboundHandlerAdapter {
    private final Logger logger = LoggerFactory.getLogger(ServerHandler.class);
    private Environment env;
    private final JrbustcpServerModule module;


    public ServerHandler(JrbustcpServerModule module) {
        this.module = module;
        this.env = EnvironmentInst.get();
    }


    private String getAddress(ChannelHandlerContext ctx) {
        return ctx.channel().remoteAddress().toString();
    }


    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        if( env.isRunning() ) {
            env.printInfo(logger, module.getName(), "Connected: ", getAddress(ctx));

            module.getClientChannels().add(ctx.channel());

            ServerSession session = new ServerSession( module );
            ctx.channel().attr(CHDATA).set(session);
            session.onConnected( ctx.channel() );
        } else {
            env.printInfo(logger, module.getName(), "Rejected: ", getAddress(ctx));
            ctx.channel().close();
        }
    }


    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        ServerSession sess = ctx.channel().attr(CHDATA).get();
        if( sess != null ) {
            ctx.channel().attr(CHDATA).get().onDisconnected(ctx.channel());
            env.printInfo(logger, module.getName(), "Disconnected: ", getAddress(ctx));
        }
    }


    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        ByteBuf buf = (ByteBuf) msg;
        try {
            ctx.channel().attr(CHDATA).get().onMessageReceived(ctx.channel(), buf);
        } finally {
            buf.release();
        }

    }


    @Override
    public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception {
        if (evt instanceof IdleStateEvent) {
            env.printInfo(logger, module.getName(), "Close inactive channel: ", getAddress(ctx));
            ctx.close();
        }
    }


    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        env.printError(logger, cause, module.getName(), getAddress(ctx));
        ctx.close();
    }
}
