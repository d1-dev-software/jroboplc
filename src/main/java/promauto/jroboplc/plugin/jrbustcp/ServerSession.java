package promauto.jroboplc.plugin.jrbustcp;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufUtil;
import io.netty.channel.Channel;
import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import promauto.jroboplc.core.api.*;
import promauto.jroboplc.core.tags.TagPlain;
import promauto.utils.CRC;

import java.nio.charset.Charset;
import java.util.*;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import static promauto.jroboplc.plugin.jrbustcp.JrbustcpProtocol.*;
import static promauto.jroboplc.plugin.jrbustcp.ServerSession.ClientTagState.*;

public class ServerSession implements Signal.Listener {

    private final Logger logger = LoggerFactory.getLogger(ServerSession.class);
    private final static Charset charset = Charset.forName("UTF-8");


    public enum ListState {
        ACTUAL,
        HAS_CHANGES
    }

    public enum ClientTagState {
        OK,
        NEED_SEND,
        SENT
    }

    public static class ClientTag {
        Module module;
        Tag tag;
        Tag fixval;
//        boolean skipModuleName;
        ClientTagState state;
        String tagname;

        ClientTag(Module module, Tag tag, String tagname) {
            this.module = module;
            this.tag = tag;
            this.fixval = TagPlain.create(tag);
            this.fixval.setStatus( tag.getStatus() );
            this.state = NEED_SEND;
//            this.skipModuleName = skipModuleName;
            this.tagname = tagname;
//                    makeTagname(module, tag, skipModuleName);
        }

        public String getTagname() {
            return tagname; //makeTagname(module, tag, skipModuleName);
        }
    }


    private Environment env;
    private JrbustcpServerModule module;
    private TrafficHandler trafficHandler;
    protected JrbustcpProtocol prot = new JrbustcpProtocol();
    private ByteBuf outbuf = null;
    protected String clientFilter;
    protected String clientDescr;
    private volatile boolean hasReloadSignal = false;
    private ListState cltagsListState;
    protected List<ClientTag> cltags = new ArrayList<>();

    boolean useTagDescr = false;
    private boolean excludeExternal;
    private boolean includeHidden;


    private String authNonce;
    private boolean authAccepted = false;


    public ServerSession(JrbustcpServerModule module) {
        this.env = EnvironmentInst.get();
        this.module = module;
    }

    protected String getAuthNonce() {
        return authNonce;
    }

//    private void subscribeSignals() {
//        for (Module m : env.getModuleManager().getModules())
//            m.addSignalListener(this);
//    }


//    private void unsubscribeSignals() {
//        for (Module m : env.getModuleManager().getModules())
//            m.removeSignalListener(this);
//    }


    @Override
    public void onSignal(Module sender, Signal signal) {
        if (signal.type == Signal.SignalType.RELOADED)
            hasReloadSignal = true;
    }


    public String getInfo() {
        return String.format("%s filter=%s tags=%d"
                , clientDescr
                , clientFilter
                , cltags.size()
        );
    }


    public TrafficHandler getTrafficHandler() {
        return trafficHandler;
    }


    public void onConnected(Channel channel) {
        trafficHandler = (TrafficHandler) channel.pipeline().get("traffic");
        addAsSignalListerToAllOthers();
//        subscribeSignals();
    }


    public void onDisconnected(Channel channel) {
//        unsubscribeSignals();
        removeAsSignalListerFromAllOthers();
        if( outbuf != null  &&  outbuf.refCnt() > 0 ) {
            outbuf.release();
            outbuf = null;
        }
    }


    public void onMessageReceived(Channel channel, ByteBuf inbuf) {
        try {
            if (module.isLogging())
                env.logInfo(logger, "Server got request:\r\n" + ByteBufUtil.prettyHexDump(inbuf) + "\r\n");

            if (hasReloadSignal)
                checkListStatus();

            Message msg = prot.getMessage(inbuf);
            if (msg == null) {
                env.logError(logger, module.getName(), clientDescr,
                        "Bad message:\r\n" + ByteBufUtil.hexDump(inbuf) + "\r\n");
                return;
            }

            outbuf = channel.alloc().buffer();

            prot.writeHeader(outbuf, msg.reqId, msg.cmd | 0x80);

            if( module.isAuth()  &&  !authAccepted  &&  msg.cmd != CMD_AUTH_INIT  &&  msg.cmd != CMD_AUTH_SUBMIT) {
                replyError(ERROR_UNUATHENTICATED);
            } else {
                switch (msg.cmd) {
                    case CMD_INIT:
                        onCommandInit(msg.body);
                        break;
                    case CMD_LIST:
                        onCommandList(msg.body);
                        break;
                    case CMD_UPDATE:
                        onCommandUpdate(msg.body);
                        break;
                    case CMD_READ:
                        onCommandRead(msg.body);
                        break;
                    case CMD_WRITE:
                        onCommandWrite(msg.body);
                        break;
                    case CMD_CRC:
                        onCommandCrc(msg.body);
                        break;
                    case CMD_AUTH_INIT:
                        onCommandAuthInit(msg.body);
                        break;
                    case CMD_AUTH_SUBMIT:
                        onCommandAuthSubmit(msg.body);
                        break;
                    default:
                        replyError(ERROR_CMD_UNKNOWN);
                }
            }

            prot.writeFooter(outbuf);

            if (module.isLogging())
                env.logInfo(logger, "Server send answer:\r\n" + ByteBufUtil.prettyHexDump(outbuf) + "\r\n");

            channel.writeAndFlush(outbuf);
            outbuf = null;
        } catch (Exception e) {
            env.logError(logger, e, module.getName(), channel.localAddress().toString());
            channel.close();
        }
    }

    private void replyError(int errorCode) {
        outbuf.setByte(outbuf.writerIndex() - 1, errorCode);
    }


    // INIT
    private void onCommandInit(ByteBuf msgbody) {
        clientFilter = prot.readShortString(msgbody);
        clientDescr = prot.readShortString(msgbody);

        setInitPrms( msgbody.readUnsignedShort() );

        cltags = collectClientTags();
        cltagsListState = ListState.ACTUAL;

        outbuf.writeMedium( cltags.size() );
    }


    private void setInitPrms(int initPrms) {
        useTagDescr = (initPrms & INITPRM_TAGDESCR) > 0;
        prot.setUseTagStatus( (initPrms & INITPRM_TAGSTATUS) > 0 );
        excludeExternal = (initPrms & INITPRM_EXCLUDE_EXTERNAL) > 0;
        includeHidden = (initPrms & INITPRM_INCLUDE_HIDDEN) > 0;
    }


    // LIST
    private void onCommandList(ByteBuf msgbody) {
        int indexStart = msgbody.readUnsignedMedium();
        outbuf.writeMedium(indexStart);

        int qntPos = outbuf.writerIndex();
        outbuf.writeMedium(0);

        int nextPos = outbuf.writerIndex();
        outbuf.writeMedium(0);

        int n = 0;
        for( int i=indexStart; i < cltags.size(); ++i) {
            if( !prot.canWrite(outbuf) ) {
                outbuf.setMedium(nextPos, i);
                break;
            }

            ClientTag cltag = cltags.get(i);

            String tagname = cltag.getTagname();
            if( tagname.length() > 255 )
                tagname = tagname.substring(0, 256);

            String descr = "";
            // todo:  implementation for tag descriptions
//            String descr = useTagDescr? cltag.tag.getDescr(): "";
//            if( descr.length() > 255 )
//                descr = descr.substring(0, 256);

            outbuf.writeByte( prot.convertTagTypeToByte(cltag.tag) );
            prot.writeShortString(outbuf, tagname);
            prot.writeShortString(outbuf, descr);

            n++;
        }

        outbuf.setMedium(qntPos, n);
    }


    // UPDATE
    private void onCommandUpdate(ByteBuf msgbody) {
        int qnt = 0;
        int next = 0;
        boolean neednext = true;
        int n = cltags.size();
        for(int i=0; i<n; ++i) {
            ClientTag cltag = cltags.get(i);

            if( cltag.tag.getType() == Tag.Type.DOUBLE  &&  Double.isNaN(cltag.tag.getDouble())  &&  Double.isNaN(cltag.fixval.getDouble())) {
                // skip NaN
            } else

            if( !cltag.tag.equalsValue(cltag.fixval)
                    ||  (prot.isUseTagStatus()  &&  cltag.tag.getStatus() != cltag.fixval.getStatus()))
            {
                cltag.tag.copyValueTo(cltag.fixval);
                cltag.fixval.setStatus( cltag.tag.getStatus() );
                cltag.state = NEED_SEND;
            }

            if( cltag.state == NEED_SEND ) {
                qnt++;
                if(neednext) {
                    neednext = false;
                    next = i;
                }
            } else
                cltag.state = OK;
        }
        outbuf.writeMedium(qnt);
        outbuf.writeMedium(next);
        outbuf.writeByte( cltagsListState == ListState.HAS_CHANGES? 0xFF: 0 );
    }


    // READ
    private void onCommandRead(ByteBuf msgbody) {
        int indexStart = msgbody.readUnsignedMedium();
        int indexPos = outbuf.writerIndex();
        outbuf.writeMedium(0);

        int qntPos = outbuf.writerIndex();
        outbuf.writeMedium(0);

        int nextPos = outbuf.writerIndex();
        outbuf.writeMedium(0);

        int n = 0;
        boolean gap = false;
        for( int i=indexStart; i < cltags.size(); ++i) {
            if( !prot.canWrite(outbuf) ) {
                outbuf.setMedium(nextPos, i);
                break;
            }

            ClientTag cltag = cltags.get(i);
            if( cltag.state == OK ) {
                gap = true;
                continue;
            }

            if( n==0 ) {
                outbuf.setMedium(indexPos, i);
            } else
            if( gap )
                prot.writeIndex(outbuf, i);

            prot.writeValueAndStatus(outbuf, cltag.fixval);
            cltag.state = SENT;

            gap = false;
            n++;
        }
        outbuf.setMedium(qntPos, n);
    }


    // WRITE
    private void onCommandWrite(ByteBuf msgbody) {
        int index = msgbody.readUnsignedMedium();
        int qnt = msgbody.readUnsignedMedium();
        int cltagsSize = cltags.size();

        for (int i = 0; i < qnt; ++i, ++index) {
            index = prot.readIndex(msgbody, index);
            if (index >= 0 && index < cltagsSize)
                prot.readValue(msgbody, cltags.get(index).tag);
            else
                throw new IndexOutOfBoundsException();
        }
    }


    // CRC
    private void onCommandCrc(ByteBuf msgbody) {
        long crc = CRC.getCompactCrc32FromTagStream( cltags.stream().map(cltag -> cltag.fixval));

        if (module.isLogging())
            env.logInfo(logger, "Calc crc for: " + CRC.getHexStringFromTagStream(cltags.stream().map(cltag -> cltag.fixval)) + " = " + crc + "\r\n");

        outbuf.writeInt( (int)crc );
    }


    // AUTH_INIT
    private void onCommandAuthInit(ByteBuf msgbody) {
        String authKeyName = prot.readString(msgbody);
        if( !module.isAuth() ) {
            outbuf.writeByte(AUTH_INIT_DISABLED);
            return;
        }

        authNonce = RandomStringUtils.randomAlphanumeric(64);
        try {
            String authNonceEncrypted = env.getKeyManager().encryptPublic(authKeyName, authNonce);
            outbuf.writeByte(AUTH_INIT_OK);
            prot.writeString(outbuf, authNonceEncrypted);
        } catch (Exception e) {
            outbuf.writeByte(AUTH_INIT_FAILED);
            prot.writeString(outbuf, e.getLocalizedMessage());
        }
    }

    // AUTH_SUBMIT
    private void onCommandAuthSubmit(ByteBuf msgbody) {
        String authNonceReceived = prot.readString(msgbody);
        authAccepted = authNonce != null  &&  authNonceReceived.equals( authNonce );
        outbuf.writeByte( authAccepted ? AUTH_SUBMIT_ACCEPTED : AUTH_SUBMIT_DENIED);
    }



    private List<ClientTag> collectClientTags() {
        clientFilter = clientFilter.trim().isEmpty()? ".*": clientFilter;
        Pattern pfilter;
        try {
            pfilter = Pattern.compile(clientFilter);
        } catch (PatternSyntaxException e) {
            pfilter = Pattern.compile( clientFilter = ".*" );
        }

        boolean tooMany = false;

        Set<String> tagnames = new HashSet<>();
        List<ClientTag> list = new LinkedList<>();
        for (Module m : env.getModuleManager().getModules()) {

            for (Tag tag : m.getTagTable().values()) {
                if ( !includeHidden  &&  tag.hasFlags(Flags.HIDDEN) )
                    continue;

                if ( excludeExternal  &&  tag.hasFlags(Flags.EXTERNAL) )
                    continue;

                boolean skipModuleName = tag.hasFlags(Flags.EXTERNAL);

                String tagname = makeTagname(m, tag, skipModuleName);
                if( !pfilter.matcher(tagname).matches() )
                    continue;

                if( tagnames.contains(tagname) )
                    continue;

                list.add(new ClientTag(m, tag, tagname));
                tagnames.add(tagname);

                tooMany = list.size() == 0xFFFFFF;
                if( tooMany ) break;
            }
            if( tooMany ) break;
        }

        if( tooMany )
            env.printInfo(logger, module.getName(), clientDescr, "Client requested too many tags");

        List<ClientTag> result = new ArrayList<>(list);
        result.sort(Comparator.comparing(ClientTag::getTagname));
        return result;
    }


    private void checkListStatus() {
        hasReloadSignal = false;

        List<ClientTag> list = collectClientTags();
        if( cltags.size() != list.size() ) {
            cltagsListState = ListState.HAS_CHANGES;
        } else
            for(int i=0; i<list.size(); ++i)
                if( list.get(i).tag != cltags.get(i).tag ) {
                    cltagsListState = ListState.HAS_CHANGES;
                    break;
                }
    }


    private static String makeTagname(Module m, Tag tag, boolean skipModuleName) {
        if (skipModuleName) {
            return tag.getName();
        } else {
            return m.getName() + "." + tag.getName();
        }
//        return  (skipModuleName? "": m.getName() + ".") + tag.getName();
    }



}
