package promauto.jroboplc.plugin.database;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import promauto.jroboplc.core.EnvironmentImpl;
import promauto.jroboplc.core.api.Configuration;
import promauto.jroboplc.core.api.Environment;
import promauto.jroboplc.core.api.EnvironmentInst;
import promauto.jroboplc.core.api.Signal;

import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class Tabletags {
    private final Logger logger = LoggerFactory.getLogger(Tabletags.class);

    private final DatabaseModule module;

    private List<Tabletag> tabletags = new ArrayList<>();
    private Environment env = EnvironmentInst.get();

    public Tabletags(DatabaseModule module) {
        this.module = module;
    }


    public void load(Object conf) {
        Configuration cm = env.getConfiguration();

        List<Object> conf_tabletags = cm.toList( cm.get(conf, "tabletags") );
        for(Object conf_tabletag: conf_tabletags) {
            Tabletag tbtg = new Tabletag(module);
            if( tbtg.load(conf_tabletag) )
                tabletags.add(tbtg);
        }


    }


    public void connect() {

        boolean hasChanges = false;

        try(Statement st = module.getConnection().createStatement() ) {

            for(Tabletag tbtg: tabletags)
                hasChanges &= tbtg.connect(st);

        } catch (SQLException e) {
            env.logError(logger, e, module.getName());
        }


        if( env.isRunning()  &&  hasChanges )
            module.postSignal(Signal.SignalType.RELOADED);
    }


    public void execute() {
        boolean needReconnect = false;

        try(Statement st = module.getConnection().createStatement() ) {

            for(Tabletag tbtg: tabletags)
                needReconnect |= tbtg.execute(st);

            module.getConnection().commit();

        } catch (SQLException e) {
            env.logError(logger, e, module.getName());
        }

        if( needReconnect )
            connect();
    }

}
