package promauto.jroboplc.plugin.database;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import promauto.jroboplc.core.api.*;
import promauto.jroboplc.core.tags.TagRW;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SimpleTagRepository implements TagRepository {
    private final Logger logger = LoggerFactory.getLogger(SimpleTagRepository.class);

    private static class Rec {
        String value;
        boolean hasChanges;

        Rec(String value, boolean hasChanges) {
            this.value = value;
            this.hasChanges = hasChanges;
        }
    }

    private final Database db;
    public final String schema;
    public final String table;
    public final Map<String, Rec> data = new HashMap<>();
    private boolean hasChanges = false;


    public SimpleTagRepository(Database db, String schema, String table) {
        this.db = db;
        this.table = table;
        this.schema = schema;
    }

    private static class GroupKey {
        String group;
        String key;
    }
    private Map<Tag, GroupKey> groupKeyCache = new HashMap<>();
    private String makeKey(String group, Tag tag) {
        GroupKey gk = groupKeyCache.get(tag);
        if( gk != null ) {
            if( gk.group.equals(group) )
                return gk.key;
        } else {
            gk = new GroupKey();
            groupKeyCache.put(tag, gk);
        }
        gk.group = group;
        gk.key = group + ':' + tag.getName();
        return gk.key;
    }

    @Override
    public void load(String group, Tag tag) {
        String key = makeKey(group, tag);
        Rec rec = data.get(key);
        if( rec != null ) {
            tag.setString(rec.value);
            if( tag instanceof TagRW)
                ((TagRW) tag).acceptWriteValue();
        }
    }

    @Override
    public void load(String group, List<Tag> tags) {
        tags.forEach(tag -> load(group, tag));
    }

    @Override
    public void save(String group, Tag tag) {
        String key = makeKey(group, tag);
        Rec rec = data.get(key);
        if( rec == null ) {
            rec = new Rec( tag.getString(), true);
            data.put(key, rec);
            hasChanges = true;
        } else {
            if (!tag.getString().equals(rec.value)) {
                rec.value = tag.getString();
                rec.hasChanges = hasChanges = true;
            }
        }
    }

    @Override
    public void save(String group, List<Tag> tags) {
        tags.forEach(tag -> save(group, tag));
    }

    public boolean init() {
        data.clear();
        hasChanges = false;

        try(Statement st = db.getConnection().createStatement()) {
            if (!db.hasTable(st, "", table)) {
                String sql = String.format(
                        "create table %s (" +
                                "name varchar(128) not null primary key, " +
                                "val varchar(128) not null)",
                        table);
                st.executeUpdate(sql);
            }
            db.commit();
        } catch (SQLException e) {
            EnvironmentInst.get().printError(logger, e, schema, table);
            return false;
        }

        try(Statement st = db.getConnection().createStatement()) {
            String sql = String.format("select name, val from %s", table);
            try ( ResultSet rs = st.executeQuery(sql)) {
                while (rs.next()) {
                    String name = rs.getString(1);
                    String value = rs.getString(2);
                    data.put(name, new Rec(value, false));
                }
            }
            db.commit();
        } catch (SQLException e) {
            EnvironmentInst.get().printError(logger, e, schema, table);
            return false;
        }
        return true;
    }


    public void persist() throws SQLException {
        if( !hasChanges )
            return;

        try(Statement st = db.getConnection().createStatement()) {
            for (Map.Entry<String,Rec> ent : data.entrySet()) {
                Rec rec = ent.getValue();
                if (rec.hasChanges) {
                    String name = ent.getKey();

//                    String sql = String.format("delete from %s where name='%s'", table, name);
//                    st.executeUpdate(sql);
//                    sql = String.format("insert into %s (name, val) values ('%s', '%s')", table, name, rec.value);
                    String sql = String.format("update or insert into %s (name, val) values ('%s', '%s')", table, name, rec.value);
                    st.executeUpdate(sql);

                    rec.hasChanges = false;
                }
            }
        }
        hasChanges = false;
    }



}
