package promauto.jroboplc.plugin.database;


import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.*;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.yaml.snakeyaml.Yaml;

import promauto.jroboplc.core.AbstractModule;
import promauto.jroboplc.core.ModuleManagerImpl;
import promauto.jroboplc.core.api.*;
import promauto.utils.ParamsMap;

public abstract class DatabaseModule extends AbstractModule implements Database {
	private final Logger logger = LoggerFactory.getLogger(DatabaseModule.class);
	
	protected Properties databaseProperties = new Properties();

	protected String type;
	protected String host;
	protected int port;
	protected String dbname;
	protected String user;
	protected String password;
	protected String scripFileName;
	protected int reconnectTimeSec;
	protected int timeout_s;
	
	protected Tag tagConnected;
	protected Tag tagReconnectCounter;
	protected Tag tagDisconnectCounter;
	
    protected boolean connected = false;
    private long cooldownTime = 0;
    
    protected Connection connection = null;
	protected String connectionUrl = "";
	
	protected Set<String> notifications = new HashSet<>();

	protected String lastErrorMessage;

	private boolean reloading = false;
	
	private Map<String,Dbscr> dbscrs = new HashMap<>();
	private long serverTimeOffset;

	private static class Startup {
		public String dbscrname;
		public ParamsMap params;
	}
	private final List<Startup> startups = new ArrayList<>();

	private final Tabletags tabletags = new Tabletags(this);
	

	public DatabaseModule(Plugin plugin, String name) {
		super(plugin, name);
		env.getCmdDispatcher().addCommand(this, CmdExec.class);
		env.getCmdDispatcher().addCommand(this, CmdSql.class);

	}
	
	
	public final boolean loadModule(Object conf) {
		Configuration cm = env.getConfiguration();
		
		type 			 = cm.get(conf, "type", "");
		host			 = cm.get(conf, "host", "localhost");
		port			 = cm.get(conf, "port", getDefaultPort() );
		dbname			 = cm.get(conf, "dbname", "");
		user			 = cm.get(conf, "user", getDefaultUser() );
		password		 = cm.get(conf, "password", getDefaultPassword() );
		scripFileName	 = cm.get(conf, "script", "");
		reconnectTimeSec = cm.get(conf, "recon_s", 10);
		timeout_s 		 = cm.get(conf, "timeout_s", 10);
		
		databaseProperties.clear();
		cm.toMap(cm.get(conf, "properties")).
			forEach( (key,value) -> databaseProperties.put(key, value) );
		
		// startups
		for(Object obj: cm.toList(cm.get(conf, "startups"))) {
			String filename = "";
			ParamsMap params = new ParamsMap();
			if( obj instanceof Map) {
				Map.Entry<String,Object> ent = cm.toMap(obj).entrySet().iterator().next();
				filename = ent.getKey();
				cm.toMap(ent.getValue()).
					forEach( (key,value) -> params.put(key, value.toString()) );
			} else
				
			if( obj instanceof String) {
				filename = obj.toString();
			}
			
			// load from file
			List<String> loaded = loadScriptFromFile(filename);
			if( loaded == null )
				return false;
			
			// get script names and add to startups
			loaded.stream().forEach(dbscrname -> {
				Startup startup = new Startup();
				startup.dbscrname = dbscrname;
				startup.params = params;
				startups.add(startup);
			});
		}

		tabletags.load(conf);
				
		if( !reloading ) {
			tagConnected		 = tagtable.createBool("connected", false);
			tagReconnectCounter  = tagtable.createInt("reconnect.cnt", 0);
			tagDisconnectCounter = tagtable.createInt("disconnect.cnt", 0);
		}
		
		return loadDatabaseModule(conf);
	}
	

	@Override
	public final boolean prepareModule() {

		tagReconnectCounter.setInt(0);
		tagDisconnectCounter.setInt(0);
		
		connect();
		if( !connected ) {
			env.printInfo(logger, name, "No connection", lastErrorMessage);
			
			Pattern p = Pattern.compile(getDatabaseNotExistsRegex(), Pattern.MULTILINE);
			if( p.matcher( lastErrorMessage ).find() ) {
            	env.printInfo(logger, name, "Attempting to create database...");
            	if( createDatabase() )
            		connect();
			}
			
		}

		return true;
	}


	@Override
	public boolean closedownModule() {
		if (!enable) return true;
		
		disconnect();
		
		return true;
	}


	@Override
	public boolean executeModule() {
	    if( cooldownTime > 0 )
	    {
	        if( isCooldownOver() )
	        {
	            resetCooldown();
	            connect();
	        }
	    }
	    else {
			boolean ok = isConnected();
			if (ok)
				try {
					ok = updateServerTimeOffset();

					SQLWarning w = connection.getWarnings();
					if (w != null) {
						while (w != null) {
							env.printInfo(logger, name, "SQLWarning:", w.getMessage());
							w = w.getNextWarning();
						}
						connection.clearWarnings();
					}

				} catch (Exception e) {
					ok = false;
				}

			if (ok) {
				tabletags.execute();
			} else {
	            disconnect();
	            setupCooldown();

	            int cnt = tagDisconnectCounter.getInt() + 1;
	            tagDisconnectCounter.setInt( cnt );
				env.printInfo(logger, name, "Connection lost, cnt:", ""+cnt); 
	        }
	    }
	    
		return true;
	}
	
	



	private boolean isCooldownOver() {
	    return cooldownTime < System.currentTimeMillis();
	}

	
	private void setupCooldown() {
	    cooldownTime = System.currentTimeMillis() + reconnectTimeSec * 1000;
	}

	
	private void resetCooldown() {
		cooldownTime = 0;
	}

	
	private void connect() {
		if( connected )
			return;
		
    	int reconnectCnt = tagReconnectCounter.getInt();
        tagReconnectCounter.setInt( reconnectCnt + 1 );

		connection = null;
		String url = "";
		try {
			DriverManager.setLoginTimeout(timeout_s);
			url = getConnectionUrl();
			connection = DriverManager.getConnection( url, databaseProperties);
			connection.setAutoCommit(false);
			connected = !connection.isClosed()  &&  connection.isValid(0);
		} catch (SQLException e) {
			lastErrorMessage = e.getMessage();
		} 

		if( connected )
			connected &= doAfterConnected();

		if( connected ) {
	        tagConnected.setBool(true);
            if( reconnectCnt > 0 ) 
            	env.printInfo(logger, name, "Connection restored");
			resetCooldown();
			postSignal(Signal.SignalType.CONNECTED);
			
			executeStartups();

			tabletags.connect();
		} else {
			setupCooldown();
			disconnect();
		}
	}


	private void executeStartups() {
		startups.forEach( startup -> {
			ScriptResult sr = executeScript(startup.dbscrname, startup.params);
			if( sr.success ) {
				if( sr.executedDoCount > 0) {
	            	env.printInfo(logger, name, 
	            			"Startup script executed: " + startup.dbscrname + 
	            			(startup.params.size()>0? " " + startup.params.toString(): ""));
				}
			} else
            	env.printError(logger, name, "Database script failed: " + startup.dbscrname);
				
		});
	}


	public void disconnect() {
		if( connection == null )
			return;
		
		try {
			connection.close();
		} catch (SQLException e) {
		}

		doAfterDisconnected();

		connection = null;
		connected = false;

		postSignal(Signal.SignalType.DISCONNECTED);
	}

	
	public boolean updateServerTimeOffset() {
		boolean res = false;
		if( connection != null)
			try( Statement st = connection.createStatement() ) {
				if( !connection.isClosed() ) {
					try(ResultSet rs = st.executeQuery( getCurrentTimestampSql() )) {
						if (rs.next()) {
							serverTimeOffset = rs.getTimestamp(1).toInstant().toEpochMilli() - System.currentTimeMillis();
							res = true;
//							if (rs.getInt(1) == 1)
//								res = true;
						}
					}
					connection.commit();
				}
			} catch (SQLException e) {
				try {
					connection.rollback();
				} catch (SQLException e1) {
				}
			}
		return res;
	}

	

	@Override
	public List<String> loadScriptFromResource(Class<?> classOfLoader, String resourceName) {
		try (InputStream in = ModuleManagerImpl.class.getClassLoader().getResourceAsStream(resourceName)) {
			return loadScriptFromMap(new Yaml().load(in));
		} catch (Exception e) {
			env.printError(logger, e, name, "Failed to load script from resource:", resourceName);
		}
		return null;
	}
//		URLClassLoader cl = env.getModuleManager().getClassLoader();
//        URL resourceUrl = cl.getResource(resourceName);
//        if( resourceUrl == null ) {
//			env.printError(logger, "Resource not found:", resourceName);
//        	return null;
//        }
//
//        Yaml yaml = new Yaml();
//		try (InputStream in = cl.getResourceAsStream(resourceName)) {
//			return loadScriptFromMap( (Map<Object,Object>)yaml.load(in) );
//		} catch (Exception e) {
//			env.printError(logger, e, name, "Load script from resource:", classOfLoader.getName(), resourceName );
//		}
//		return null;
//	}

	@Override
	@SuppressWarnings("unchecked")
	public List<String> loadScriptFromFile(String filename) {
		Path path = Paths.get(filename);
		if (!path.isAbsolute())
			path = env.getConfiguration().getConfDir().resolve(path);

        Yaml yaml = new Yaml();
		try (InputStream in = Files.newInputStream(path)) {
			return loadScriptFromMap( (Map<Object,Object>)yaml.load(in) );
		} catch (Exception e) {
			env.printError(logger, e, name, "Load script from file:", filename );
		}
		return null;
	}

	public List<String> loadScriptFromMap(Map<Object,Object> map) {
		List<String> result = new ArrayList<>();
		for(Map.Entry<Object,Object> ent: map.entrySet()) {
			String key = ent.getKey().toString();
			if( key.startsWith("dbscr.") ) {
				String dbscrName = key.substring(6);
				Dbscr dbscr = new Dbscr(this, dbscrName);
				if( !dbscr.load(ent.getValue()) ) {
					return null;
				}
				dbscrs.put(dbscrName, dbscr);
				result.add(dbscrName);
			}
		}
		return result;
	}


	@Override
	public ScriptResult executeScript(String scriptName) {
		return executeScript(scriptName, null);
	}

		@Override
	public ScriptResult executeScript(String scriptName, Map<String,String> params) {	
		Dbscr dbscr = dbscrs.get(scriptName);
		if( dbscr == null ) {
			env.printError(logger, "Script not found: " + scriptName);
			return new ScriptResult();
		}
		
		return dbscr.execute(params);
	}


	@Override
	public boolean hasScript(String scriptName) {
		return dbscrs.containsKey(scriptName);
	}


	@Override
	public String makeSchemaObjectName(String schema, String objname) {
		if( schema.isEmpty() )
			return objname;
		else
			return schema + getSchemaDelimiter() + objname;
	}

	
	@Override
	public String getInfo() {
		return enable?
				(!connected? ANSI.RED + ANSI.BOLD + "ERROR! " + ANSI.RESET: "") +
				"errcnt=" + tagDisconnectCounter.getString() + ' ' +
				type + ':' + host + '/' + port + ':' + dbname 		
			  : "disabled";
	}

	
	@Override
	protected boolean reload() {
		String 	old_type 	 		 = type;
		String 	old_host			 = host;
		int 	old_port			 = port;
		String 	old_dbname		 	 = dbname;
		String 	old_user			 = user;
		String 	old_password		 = password;
		String 	old_scripFileName 	 = scripFileName;
		int 	old_reconnectTimeSec = reconnectTimeSec;
		int 	old_timeout_s 		 = timeout_s;
		
		Properties old_databaseProperties = new Properties();
		old_databaseProperties.putAll( databaseProperties );
		
		List<Startup> old_startups = new ArrayList<>();
		old_startups.addAll(startups);
		
		Map<String,Dbscr> old_dbscrs = new HashMap<>();
		old_dbscrs.putAll(dbscrs);

		reloading  = true;
		boolean closed = false;
		boolean result = false;
		if( load()  &&  type.equals(old_type) ) {
			closedown();
			closed = true;
		
			if( prepare() ) 
				result = true;
		}
		
		if( !result ) {
			type             = old_type; 
			host             = old_host; 
			port             = old_port; 
			dbname           = old_dbname; 
			user             = old_user; 
			password         = old_password; 
			scripFileName    = old_scripFileName; 
			reconnectTimeSec = old_reconnectTimeSec; 
			timeout_s        = old_timeout_s; 
			databaseProperties.clear();
			databaseProperties.putAll( old_databaseProperties );
			startups.clear();
			startups.addAll(old_startups);
			dbscrs.clear();
			dbscrs.putAll(old_dbscrs);
			
			if( closed )
				prepare();
		}
		
		return result;
	}

	
	// implementation methods for Database 
	@Override
	public boolean isConnected() {
		return connected;
	}

	@Override
	public Connection getConnection() {
		return connection;
	}


	@Override
	public void registerEventNotification(String notification) {
		if( !notifications.contains(notification) ) {
			listenEventNotification(notification);
			notifications.add(notification);
		}
	}


	@Override
	public void commit() throws SQLException {
		if( connection != null )
			connection.commit();
	}

	@Override
	public void rollback() {
		if( connection != null )
			try {
				connection.rollback();
			} catch (SQLException e) {
				env.printError(logger, e, name, "Rollback");
			}
	}

	@Override
	public TagRepository createTagRepository(String schema, String table) {
		return new SimpleTagRepository(this, schema, table);
	}


	@Override
	public long getServerTimerOffset() {
		return serverTimeOffset;
	}

	@Override
	public LocalDateTime getServerDatetime() {
		return LocalDateTime.now().plus(serverTimeOffset, ChronoUnit.MILLIS);
	}

	@Override
	public String formatDatetimeWithOffset(LocalDateTime ldt) {
		return ldt.plus(serverTimeOffset, ChronoUnit.MILLIS).format(getTimestampFormatter());
	}


	@Override
	public String formatDatetime(LocalDateTime ldt) {
		return ldt.format(getTimestampFormatter());
	}

	// methods to override
	protected abstract void listenEventNotification(String notification);
	
	protected abstract boolean loadDatabaseModule(Object conf);
	
	protected abstract int getDefaultPort();
	
	protected abstract String getDefaultUser();
	
	protected abstract String getDefaultPassword();
	
	protected abstract boolean createDatabase();
	
	protected abstract String getDatabaseNotExistsRegex();
	
	protected abstract String getConnectionUrl();
	
	protected abstract boolean doAfterConnected();
	
	protected abstract void doAfterDisconnected();
	
	protected abstract String getCurrentTimestampSql();
	


}