package promauto.jroboplc.plugin.database;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.format.DateTimeFormatter;

import org.firebirdsql.event.DatabaseEvent;
import org.firebirdsql.event.EventListener;
import org.firebirdsql.event.EventManager;
import org.firebirdsql.event.FBEventManager;
import org.firebirdsql.gds.impl.GDSType;
import org.firebirdsql.management.FBManager;
import org.firebirdsql.management.FBServiceManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import promauto.jroboplc.core.api.Plugin;
import promauto.jroboplc.core.api.Signal;

public class FirebirdDatabaseModule extends DatabaseModule implements EventListener  {
	private final Logger logger = LoggerFactory.getLogger(FirebirdDatabaseModule.class);
	static private DateTimeFormatter timestapFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

	public static final int EXPECTED_SERVER_MAJOR_VERSION = 3;

	private EventManager eventManager = null;
    private boolean eventManagerConnected = false;
	
	public FirebirdDatabaseModule(Plugin plugin, String name) {
		super(plugin, name);
		
		try {
			Class.forName("org.firebirdsql.jdbc.FBDriver");
		} catch (ClassNotFoundException e) {
        	env.printError(logger, e, name);
		}
	}
	
	@Override
	protected boolean loadDatabaseModule(Object conf) {

		databaseProperties.put("soTimeout", "" + (timeout_s*1000) );
		databaseProperties.put("user", user);
		databaseProperties.put("password", password);
		if( !databaseProperties.containsKey("encoding") )
			databaseProperties.put("encoding", "WIN1251");
		return true;
	}

	@Override
	public DatabaseType getType() {
		return DatabaseType.FIREBIRD;
	}

	@Override
	public DateTimeFormatter getTimestampFormatter() {
		return timestapFormatter;
	}

	@Override
	protected int getDefaultPort() {
		return 3050;
	}

	@Override
	protected String getDefaultUser() {
		return "SYSDBA";
	}

	@Override
	protected String getDefaultPassword() {
		return "masterkey";
	}

	@Override
	protected String getConnectionUrl() {
		return "jdbc:firebirdsql://" + host + ':' + port + '/' + dbname;
	}
	
	@Override
	protected String getDatabaseNotExistsRegex() {
		// GDS Exception. 335544344. I/O error during "open" operation for file "/home/denis/asutp/promauto/db/fb25/SHPMSC/SHPMSC.FDB1"
		// Error while trying to open file
		// null
		return "(.*GDS Exception. 335544344.*|.*ISC error code:335544344.*)";
	}

	@Override
	public String getCurrentTimestampSql() {
		return "select current_timestamp from rdb$database";
//		return "select 1 from rdb$database";
	}

	
	@Override
	protected boolean createDatabase() {
		try {
			FBManager manager = new FBManager(GDSType.getType("PURE_JAVA"));
			manager.setDialect(3);
			manager.setServer(host);
			manager.setPort(port);
			String encoding =  databaseProperties.getProperty("encoding");
			if( (encoding != null)  &&  (!encoding.isEmpty()) )
				manager.setDefaultCharacterSet(encoding);

			manager.start();
			manager.createDatabase(dbname, user, password);
			manager.stop();
			
			return true;
		} catch (Exception e) {
        	env.printError(logger, e, name, "Create database error:", name);
		}
		return false;
	}

	
	@Override
	protected boolean doAfterConnected() {
		FBServiceManager manager = new FBServiceManager();
		manager.setHost(host);
		manager.setPort(port);
		manager.setUser(user);
		manager.setPassword(password);
		try {
			int ver = manager.getServerVersion().getMajorVersion();
			if( ver < EXPECTED_SERVER_MAJOR_VERSION)
				env.printError(logger, name, "Incompatible server version!\r\n" +
						"Expected: >= " + EXPECTED_SERVER_MAJOR_VERSION + "\r\n" +
						"Current : " + manager.getServerVersion().getFullVersion());
		} catch (SQLException e) {
		}

		eventManagerConnected = false;
		if( notifications.size() > 0 ) {
			try {
				eventManager = FBEventManager.createFor(connection);
				eventManager.connect();
				eventManagerConnected = true;
			} catch (SQLException e) {
				env.printError(logger, e, name, "After connect error");
			}

			// subscribe to notifications
			for (String notification : notifications)
				listenEventNotification(notification);
		}
        
		return true;
	}
	
	
	@Override
	protected void doAfterDisconnected() {
		if( eventManagerConnected  &&  eventManager != null )
			try {
				eventManagerConnected = false;
				eventManager.close();
				eventManager = null;
			} catch (SQLException e) {
			}
	}
	

	@Override
	protected void listenEventNotification(String notification) {
		if( eventManagerConnected )
	        try {
	       		eventManager.addEventListener(notification, this);
			} catch (SQLException e) {
				env.printError(logger, e, name);
			}
	}

	@Override
	public void eventOccurred(DatabaseEvent event) {
		postSignal(Signal.SignalType.NOTIFICATION, event.getEventName());
	}


	
	@Override
	public String getSchemaDelimiter() {
		return "_";
	}

	@Override
	public boolean hasSchema(Statement statement, String schema) throws SQLException {
		return statement.executeQuery(
				"SELECT RDB$RELATION_ID FROM RDB$RELATIONS "+
				"WHERE RDB$RELATION_NAME STARTING WITH UPPER('" + makeSchemaObjectName(schema, "") + "')"
			).next();
	}

	@Override
	public boolean hasDomain(Statement statement, String schema, String domain) throws SQLException  {  
		return statement.executeQuery(
			    "SELECT RDB$FIELD_NAME FROM RDB$FIELDS "+
			    "WHERE RDB$FIELD_NAME = UPPER('" + makeSchemaObjectName(schema, domain) + "')"
			).next();
	}

	@Override
	public boolean hasTable(Statement statement, String schema, String table) throws SQLException {
		return statement.executeQuery(
				"SELECT RDB$RELATION_ID FROM RDB$RELATIONS "+
				"WHERE RDB$RELATION_NAME = UPPER('" + makeSchemaObjectName(schema, table) + "')"
			).next();
	}

	@Override
	public boolean hasColumn(Statement statement, String schema, String table, String column) throws SQLException  {
		return statement.executeQuery(
			     "SELECT RDB$FIELD_ID FROM RDB$RELATION_FIELDS "+ 
		         "WHERE RDB$RELATION_NAME = UPPER('"+ makeSchemaObjectName(schema, table) + "') "+ 
		         "AND RDB$FIELD_NAME = UPPER('" + column + "')"
			).next();
	}


	@Override
	public boolean hasConstraint(Statement statement, String schema, String table, String constraint) throws SQLException {
		return statement.executeQuery(
				"SELECT RDB$CONSTRAINT_NAME FROM RDB$RELATION_CONSTRAINTS "+
						"WHERE RDB$RELATION_NAME = UPPER('"+ makeSchemaObjectName(schema, table) + "') "+
						"AND RDB$CONSTRAINT_NAME = UPPER('" + constraint + "')"
		).next();
	}

	@Override
	public boolean hasIndex(Statement statement, String schema, String index) throws SQLException {
		return statement.executeQuery(
				"SELECT RDB$INDEX_NAME FROM RDB$INDICES "+
						"WHERE RDB$INDEX_NAME = UPPER('" + index + "')"
		).next();
	}

	@Override
	public boolean hasTrigger(Statement statement, String schema, String trigger) throws SQLException {
		return statement.executeQuery(
				"SELECT RDB$TRIGGER_NAME FROM RDB$TRIGGERS "+
						"WHERE RDB$TRIGGER_NAME = UPPER('" + trigger + "')"
		).next();
	}

	@Override
	public boolean hasProcedure(Statement statement, String schema, String procedure) throws SQLException {
		return statement.executeQuery(
				"SELECT RDB$PROCEDURE_NAME FROM RDB$PROCEDURES "+
						"WHERE RDB$PROCEDURE_NAME = UPPER('" + procedure + "')"
		).next();
	}

	@Override
	public boolean hasRecord(Statement statement, String sql) throws SQLException {
		return statement.executeQuery(sql).next();
	}

	@Override
	public Integer insertReturningId(Statement statement, String sql, String... optional) throws SQLException {
		String columnNameID = optional.length > 0? optional[0]: "id";
			
		sql += " returning " + columnNameID;
		try(ResultSet rs = statement.executeQuery(sql)) {
			if (rs.next())
				return rs.getInt(1);
		}
		
		return null;
	}




}
