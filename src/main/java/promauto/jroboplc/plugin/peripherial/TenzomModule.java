package promauto.jroboplc.plugin.peripherial;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import promauto.jroboplc.core.api.Configuration;
import promauto.jroboplc.core.api.Flags;
import promauto.jroboplc.core.api.Plugin;
import promauto.jroboplc.core.api.Tag;

import java.util.ArrayList;
import java.util.List;

public class TenzomModule extends PeripherialModule {
    private final Logger logger = LoggerFactory.getLogger(TenzomModule.class);

    protected ProtocolTenzom protocol = new ProtocolTenzom(this);

    private static class Counter {
        Tag tag;
        int offset;
    }

    List<Counter> counters = new ArrayList<>();
    protected int size;

    Tag tagStatus;
    Tag tagNetto;
    Tag tagBrutto;

    private boolean useCounters;
    private boolean useStatus;
    private boolean useNetto;
    private boolean useBrutto;

    private boolean stableNetto;
    private boolean stableBrutto;

    public TenzomModule(Plugin plugin, String name) {
        super(plugin, name);
    }

    @Override
    public boolean loadPeripherialModule(Object conf) {
        Configuration cm = env.getConfiguration();

        useCounters = cm.get(conf, "useCounters", true);
        useStatus = cm.get(conf, "useStatus", true);
        useNetto = cm.get(conf, "useNetto", false);
        useBrutto = cm.get(conf, "useBrutto", false);

        stableNetto = cm.get(conf, "stableNetto", false);
        stableBrutto = cm.get(conf, "stableBrutto", false);

        size = 0;
        addCounter("SumWeight",     cm.get(conf, "posSumWeight", 1));
        addCounter("SumNum",        cm.get(conf, "posSumNum",    2));
        addCounter("LastWeight",    cm.get(conf, "posLastWeight",3));
        addCounter("Output",        cm.get(conf, "posOutput",    4));
        addCounter("LastTime",      cm.get(conf, "posLastTime",  5));

        tagStatus = tagtable.createInt("Status", 0, Flags.STATUS);
        tagNetto = tagtable.createInt("Netto", 0, Flags.STATUS);
        tagBrutto = tagtable.createInt("Brutto", 0, Flags.STATUS);

        return true;
    }

    private void addCounter(String tagname, int pos) {
        Counter counter = new Counter();
        counter.tag = tagtable.createLong(tagname, 0, Flags.STATUS);
        if( pos > 0  &&  pos < 9) {
            counter.offset = pos * 5 + 3;
            size = Math.max(size, pos);
        } else {
            counter.offset = 0;
        }
        counters.add(counter);
    }


    @Override
    public boolean executePeripherialModule() {
        if( emulated ) {
            return true;
        }

        boolean result = true;
        try {
            if( useCounters  &&  result  &&  (result = protocol.requestCounters(size)) ) {
                for(Counter counter: counters) {
                    if( counter.offset > 0)
                        counter.tag.setLong(protocol.getBCDValue(counter.offset, 5));
                }
            }

            if( useStatus  &&  result  &&  (result = protocol.requestValue(0xBF, 1)) ) {
                tagStatus.setInt(protocol.buffin[2]);
            }

            if( useBrutto  &&  result  &&  (result = protocol.requestValue(0xC3, 4)) ) {
                setValue(tagBrutto, stableBrutto);
            }

            if( useNetto  &&  result  &&  (result = protocol.requestValue(0xC2, 4)) ) {
                setValue(tagNetto, stableNetto);
            }


        } catch (Exception e) {
            env.printError(logger, e, name);
            result = false;
        }

        return result;
    }


    private void setValue(Tag tag, boolean onlyStable) {
        int con = protocol.buffin[5];
        if( onlyStable  &&  (con & 0x10) == 0)
            return;

        int sign = ((con & 0x80) > 0? -1: 1);
        int mul = (int)Math.pow(10, 3 - (con & 3));
        tag.setInt( sign * mul * (int)protocol.getBCDValue(2, 3) );
    }


    @Override
    protected boolean reload() {
        TenzomModule tmp = new TenzomModule(plugin, name);
        if( !tmp.load() )
            return false;

        copySettingsFrom(tmp);
        size = tmp.size;
        for(int i=0; i<counters.size(); ++i)
            counters.get(i).offset = tmp.counters.get(i).offset;

        useCounters = tmp.useCounters;

        useStatus = tmp.useStatus;
        useNetto = tmp.useNetto;
        useBrutto = tmp.useBrutto;

        stableNetto = tmp.stableNetto;
        stableBrutto = tmp.stableBrutto;

        return true;
    }

}
