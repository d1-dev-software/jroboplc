package promauto.jroboplc.plugin.peripherial;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import promauto.jroboplc.core.api.Flags;
import promauto.jroboplc.core.api.Plugin;
import promauto.jroboplc.core.api.Tag;
import promauto.jroboplc.core.tags.TagRW;
import promauto.utils.Numbers;

import java.util.*;

public class PaGeliosMaslo2v2Module extends PeripherialModule {
	private final Logger logger = LoggerFactory.getLogger(PaGeliosMaslo2v2Module.class);

	protected ProtocolModbus protocol = new ProtocolModbus(this);


	protected TagRW tagWeightHigh1;
	protected TagRW tagWeightLow1;
	protected TagRW tagNumHigh1;
	protected TagRW tagNumLow1;
	protected TagRW tagWeightHigh2;
	protected TagRW tagWeightLow2;
	protected TagRW tagNumHigh2;
	protected TagRW tagNumLow2;
	protected TagRW tagCmd;
	protected TagRW tagNominal;
	protected Tag   tagNetto;
	protected Tag   tagState;
	protected Tag   tagDStep;
	protected Tag   tagIn;
	protected Tag   tagOut;
	protected Tag   tagErrorCode;
	protected Tag   tagStopCode;
	protected Tag   tagWeightKg1;
	protected Tag   tagWeightKg2;
	protected TagRW tagWesSvrState;

	private long wesSvrStateTimer;




	public PaGeliosMaslo2v2Module(Plugin plugin, String name) {
		super(plugin, name);
	}

	@Override
	public boolean loadPeripherialModule(Object conf) {

		tagWeightHigh1	= tagtable.createRWInt("WeightHigh1", 	0, Flags.STATUS);
		tagWeightLow1	= tagtable.createRWInt("WeightLow1", 	0, Flags.STATUS);
		tagNumHigh1	 	= tagtable.createRWInt("NumHigh1", 		0, Flags.STATUS);
		tagNumLow1	 	= tagtable.createRWInt("NumLow1", 		0, Flags.STATUS);
		tagWeightHigh2	= tagtable.createRWInt("WeightHigh2", 	0, Flags.STATUS);
		tagWeightLow2	= tagtable.createRWInt("WeightLow2", 	0, Flags.STATUS);
		tagNumHigh2		= tagtable.createRWInt("NumHigh2", 		0, Flags.STATUS);
		tagNumLow2	 	= tagtable.createRWInt("NumLow2", 		0, Flags.STATUS);
		tagCmd	 		= tagtable.createRWInt("Cmd", 			0, Flags.STATUS);
		tagNominal	 	= tagtable.createRWInt("Nominal", 		0, Flags.STATUS);

		tagNetto 		= tagtable.createInt("Netto", 		0, Flags.STATUS);
		tagState	 	= tagtable.createInt("State", 		0, Flags.STATUS);
		tagDStep	 	= tagtable.createInt("DStep", 		0, Flags.STATUS);
		tagIn	 		= tagtable.createInt("In", 		0, Flags.STATUS);
		tagOut	 		= tagtable.createInt("Out", 		0, Flags.STATUS);
		tagErrorCode	= tagtable.createInt("ErrorCode", 	0, Flags.STATUS);
		tagStopCode	 	= tagtable.createInt("StopCode", 	0, Flags.STATUS);
		tagWeightKg1	= tagtable.createInt("WeightKg1", 	0, Flags.STATUS);
		tagWeightKg2	= tagtable.createInt("WeightKg2", 	0, Flags.STATUS);

		tagWesSvrState = tagtable.createRWInt("WesSvrState", 0);

		initCrc16Tags();
		crc16Tags.addAll(Arrays.asList(
				tagWeightHigh1,
				tagWeightLow1,
				tagNumHigh1,
				tagNumLow1,
				tagWeightHigh2,
				tagWeightLow2,
				tagNumHigh2,
				tagNumLow2
		));
		crc16Tags.add(tagError);

		return true;
	}


	@Override
	public boolean executePeripherialModule() {

		boolean result = true;

		if( emulated ) {

			tagWeightHigh1.acceptWriteValue();
			tagWeightLow1 .acceptWriteValue();
			tagNumHigh1   .acceptWriteValue();
			tagNumLow1    .acceptWriteValue();
			tagWeightHigh2.acceptWriteValue();
			tagWeightLow2 .acceptWriteValue();
			tagNumHigh2   .acceptWriteValue();
			tagNumLow2    .acceptWriteValue();
			tagCmd        .acceptWriteValue();
			tagNominal    .acceptWriteValue();
		} else {

			try {
				if (result && tagNominal.hasWriteValue()) {
					int value = tagNominal.getWriteValInt();
					result &= protocol.requestCmd10(0x1010, 2, value >> 16, value & 0xFFFF);
//				result &= protocol.requestCmd10(0x1011, 2, value >> 16, value & 0xFFFF);
//				result &= protocol.requestCmd6(0x1007, tagCmd.getWriteValInt() );
				}

				if (result && tagCmd.hasWriteValue()) {
					result &= protocol.requestCmd6(0x1007, tagCmd.getWriteValInt());
				}

				if (result && (result = protocol.requestCmd3(0x1000, 18))) {
					tagNetto.setInt(Numbers.bytesToInt(protocol.buffin, 3));
					tagState.setInt(Numbers.bytesToWord(protocol.buffin, 7));
					tagIn.setInt(Numbers.bytesToWord(protocol.buffin, 9));
					tagOut.setInt(Numbers.bytesToWord(protocol.buffin, 11));
					tagErrorCode.setInt(Numbers.bytesToWord(protocol.buffin, 15));
					tagCmd.setReadValInt(Numbers.bytesToWord(protocol.buffin, 17));
					tagDStep.setInt(Numbers.bytesToWord(protocol.buffin, 19));
					tagStopCode.setInt(Numbers.bytesToWord(protocol.buffin, 21));
					tagNominal.setReadValInt(Numbers.bytesToInt(protocol.buffin, 35));
				}

				if (result && (result = protocol.requestCmd3(0x1020, 8))) {
					tagWeightHigh1.setReadValInt(Numbers.bytesToWord(protocol.buffin, 3));
					tagWeightLow1.setReadValInt(Numbers.bytesToWord(protocol.buffin, 5));
					tagNumHigh1.setReadValInt(Numbers.bytesToWord(protocol.buffin, 7));
					tagNumLow1.setReadValInt(Numbers.bytesToWord(protocol.buffin, 9));
					tagWeightHigh2.setReadValInt(Numbers.bytesToWord(protocol.buffin, 11));
					tagWeightLow2.setReadValInt(Numbers.bytesToWord(protocol.buffin, 13));
					tagNumHigh2.setReadValInt(Numbers.bytesToWord(protocol.buffin, 15));
					tagNumLow2.setReadValInt(Numbers.bytesToWord(protocol.buffin, 17));
				}

			} catch (Exception e) {
				env.printError(logger, e, name);
				result = false;
			}
		}

		tagWeightKg1.setInt( Math.round(
				((((long)tagWeightHigh1.getInt()) << 16) + tagWeightLow1.getInt()) / 1000  ));

		tagWeightKg2.setInt( Math.round(
				((((long)tagWeightHigh2.getInt()) << 16) + tagWeightLow2.getInt()) / 1000  ));

		wesSvrStateTimer = updateWesSvrState(tagWesSvrState, wesSvrStateTimer);
		return result;
	}

}

