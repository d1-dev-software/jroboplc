package promauto.jroboplc.plugin.peripherial;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import promauto.jroboplc.core.api.Flags;
import promauto.jroboplc.core.api.Plugin;
import promauto.jroboplc.core.api.Tag;
import promauto.utils.Numbers;

import java.util.ArrayList;
import java.util.List;

import static promauto.jroboplc.core.api.Tag.Type.INT;
import static promauto.jroboplc.core.api.Tag.Type.LONG;

public class KorennPassModule  extends PeripherialModule {
    private final Logger logger = LoggerFactory.getLogger(KorennPassModule.class);

    private static class TagAddr {
        Tag tag;
        int addr;
    }
    List<TagAddr> tas = new ArrayList<>();

    protected Tag tagSumWeight;
    protected Tag tagSumNum;
//    protected Tag tagLastWeight;
//    protected Tag tagCurWeight;
//    protected Tag tagStageNum;
//    protected Tag tagErrorCode;
    protected Tag tagLastTime;
    protected Tag tagReqOutput;
    protected Tag tagCurOutput;

    private int[] bufout = new int[9];
    private int[] bufin = new int[15];

    public KorennPassModule(Plugin plugin, String name) {
        super(plugin, name);
    }

/*
    SumWesHigh  = 0x2200 RS
    SumWesLow   = 0x2201 RS
    WNum        = 0x2401 RS
    LastWes     = 0x4001 RS
    CurWes      = 0x4101 R
    StageNum    = 0x4201 R
    ErrCode     = 0x4301 R
    WCycle      = 0x4401 R
    ReqOutput   = 0x4501 R
    ReqDose     = 0x4601 R
    CmdStop     = 0x4701 R
    CmdStart    = 0x4801 R
    CmdSetOutput= 0x4901 R
    CmdSetDose  = 0x4A01 R
*/

    @Override
    public boolean loadPeripherialModule(Object conf) {
        tagSumWeight 	    = createTagAddr(LONG, "SumWeight",    0x22);
        tagSumNum 	        = createTagAddr(INT,  "SumNum",       0x24);
//        tagLastWeight 	    = createTagAddr(INT,  "LastWeight",   0);
//        tagCurWeight 	    = createTagAddr(INT,  "CurWeight",    0);
//        tagStageNum 	    = createTagAddr(INT,  "StageNum",     0);
//        tagErrorCode 	    = createTagAddr(INT,  "ErrorCode",    0);
        tagLastTime 	    = createTagAddr(INT,  "LastTime",     0x31);
        tagReqOutput 	    = createTagAddr(INT,  "ReqOutput",    0x2F);
        tagCurOutput        = createTagAddr(INT,  "CurOutput",    0x2D);

        return true;
    }

    private Tag createTagAddr(Tag.Type tagtype, String tagname, int addr) {
        Tag tag = tagtable.createTag(tagtype, tagname, Flags.STATUS);
        TagAddr ta = new TagAddr();
        ta.tag = tag;
        ta.addr = addr;
        tas.add(ta);
        return tag;
    }

/*
request:
# addr 05 reg CRLF
0 12   34 56  7 8

answer:
 40 30 31 30 30 37 42 36 30 46 44 32 37 0D 0A
@ 01   007B60FD 27

@ 07   0000000A EF


@ addr 01234567 2F CRLF
0 12   34567891 11 1 1
              0 12 3 4
  -------------
        crc
*/

    boolean request(int addr) throws Exception {
        for (int trynum = 0; trynum < retrial; trynum++) {
            port.discard();
            delayBeforeWrite();

            bufout[0] = '#';
            Numbers.intToHexByte(netaddr, bufout, 1);
            bufout[3] = '0';
            bufout[4] = '5';
            Numbers.intToHexByte(addr, bufout, 5);
            bufout[7] = 13;
            bufout[8] = 10;

            port.writeBytes(bufout, bufout.length);

            int cntRead = 0;
            if( (cntRead = port.readBytes(bufin, bufin.length)) == bufin.length) {
                if( bufin[0]=='@'
                        && Numbers.asciiByteToInt(bufin, 1) == netaddr
                        && checkCrc() )
                    return true;
            }

            if( canLogError() )
                logError(trynum, checkCrc(), bufout, bufout.length, bufin, cntRead, "");

            delayAfterError();
        }
        tagErrorCnt.setInt( tagErrorCnt.getInt() + 1 );
        return false;
    }


    private boolean checkCrc() {
        int sum = 0;
        for( int i=1; i<11; i+=2) {
            sum += Numbers.asciiByteToInt(bufin, i);
        }
        sum = 0x100 - (sum & 0xff);

        return sum == Numbers.asciiByteToInt(bufin, 11);
    }


    @Override
    public boolean executePeripherialModule() {
        if( emulated )
            return true;

        boolean result = true;

        try {
            for(TagAddr ta: tas) {
                if(ta.addr == 0)
                    continue;

                result = request(ta.addr);
                if( !result )
                    break;

                if( ta.tag.getType() == LONG ) {
                    ta.tag.setLong( Numbers.asciiDWordToLong(bufin, 3));
                } else {
                    ta.tag.setInt( Numbers.asciiWordToInt(bufin, 7));
                }
            }
        } catch (Exception e) {
            env.printError(logger, e, name);
        }

        return result;
    }




}
