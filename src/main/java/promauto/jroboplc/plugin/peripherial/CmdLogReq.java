package promauto.jroboplc.plugin.peripherial;

import promauto.jroboplc.core.AbstractCommand;
import promauto.jroboplc.core.api.Console;
import promauto.jroboplc.core.api.Module;

public class CmdLogReq extends AbstractCommand {
	
	


	@Override
	public String getName() {
		return "logreq";
	}

	@Override
	public String getUsage() {
		return "cnt";
	}

	@Override
	public String getDescription() {
		return "sets logreq counter value";
	}

	
	
	
	@Override
	public String execute(Console console, Module module, String args) {
		
		PeripherialModule m = (PeripherialModule) module;

		if( args.trim().isEmpty() ) {
			return "logreq = " + m.logReqCnt;
		}

		m.logReqCnt = Integer.parseInt(args);
		
		return "OK";
	}


}
