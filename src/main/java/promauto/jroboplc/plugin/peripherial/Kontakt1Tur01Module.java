package promauto.jroboplc.plugin.peripherial;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import promauto.jroboplc.core.api.Configuration;
import promauto.jroboplc.core.api.Flags;
import promauto.jroboplc.core.api.Plugin;
import promauto.jroboplc.core.api.Tag;
import promauto.utils.Numbers;

import java.util.List;

public class Kontakt1Tur01Module extends PeripherialModule {

    private final Logger logger = LoggerFactory.getLogger(Kontakt1Tur01Module.class);

    protected ProtocolKontakt1 protocol = new ProtocolKontakt1(this);

    public Tag tagValue;
    public Tag tagPrimeval;
    public Tag tagPercent;
    public Tag tagMax;
    public Tag tagMin;
    public Tag tagPointV1;
    public Tag tagPointP1;
    public Tag tagPointV2;
    public Tag tagPointP2;
    public Tag tagSPmax;
    public Tag tagSPmin;
    public Tag tagBlok;
    public Tag tagBlokValue;
    public Tag tagTmprCnt;

    public Tag[] tagT;

    protected int size;

    public Kontakt1Tur01Module(Plugin plugin, String name) {
        super(plugin, name);
    }

    @Override
    public boolean loadPeripherialModule(Object conf) {
        Configuration cm = env.getConfiguration();
        size = cm.get(conf, "size", 0);

        tagValue     = tagtable.createInt("Value",     0, Flags.STATUS);
        tagPrimeval  = tagtable.createInt("Primeval",  0, Flags.STATUS);
        tagPercent   = tagtable.createInt("Percent",   0, Flags.STATUS);
        tagMax       = tagtable.createInt("Max",       0, Flags.STATUS);
        tagMin       = tagtable.createInt("Min",       0, Flags.STATUS);
        tagPointV1   = tagtable.createInt("PointV1",   0, Flags.AUTOSAVE);
        tagPointP1   = tagtable.createInt("PointP1",   0, Flags.AUTOSAVE);
        tagPointV2   = tagtable.createInt("PointV2",   0, Flags.AUTOSAVE);
        tagPointP2   = tagtable.createInt("PointP2",   0, Flags.AUTOSAVE);
        tagSPmax     = tagtable.createInt("SPmax",     0, Flags.AUTOSAVE);
        tagSPmin     = tagtable.createInt("SPmin",     0, Flags.AUTOSAVE);
        tagBlok      = tagtable.createInt("Blok",      0, Flags.AUTOSAVE);
        tagBlokValue = tagtable.createInt("BlokValue", 0, Flags.AUTOSAVE);
        tagTmprCnt   = tagtable.createInt("TmprCnt",   0, Flags.STATUS);

        tagT = new Tag[size];
        for(int i=0; i<size; ++i)
            tagT[i] = tagtable.createInt(String.format("T%02d", i), 0, Flags.STATUS);

        return true;
    }


    @Override
    protected void initChannelMap(List<String> chtags) {
        for( int i=0; i<size; ++i ) {
            addChannelMapTag(chtags, tagT[i], "" + i);
        }
    }


    @Override
    public boolean executePeripherialModule() {

        if( emulated )
            return true;

        boolean result = true;
        try {
            if( firstPass  ||  tagError.getBool() ) {
                protocol.buffout[1] = 180;
                protocol.buffout[2] = 2;
                protocol.buffout[3] = 1;
                result = protocol.request(6, 6);
                tagTmprCnt.setInt( protocol.buffin[3] );
                protocol.setupBuffIn( tagTmprCnt.getInt() );
            }

            protocol.buffout[1] = 1;
            protocol.buffout[2] = 2;
            protocol.buffout[3] = 1;
            if( result  &&
                    (result = protocol.request(6, 10)) ) {
                tagPrimeval.setInt( Numbers.bytesToWord(protocol.buffin, 3) );
                if( !tagBlok.getBool() ) {
                    if ((tagPointP1.getInt() < tagPointP2.getInt()) && (tagPointV1.getInt() < tagPointV2.getInt())) {
                        float p = tagPrimeval.getInt();
                        float p1 = tagPointP1.getInt();
                        float p2 = tagPointP2.getInt();
                        float v1 = tagPointV1.getInt();
                        float v2 = tagPointV2.getInt();
                        tagValue.setInt(Math.round((p - p1) * (v2 - v1) / (p2 - p1) + v1));
                    } else {
                        tagPrimeval.copyValueTo(tagValue);
                    }
                }
            }


            if( tagBlok.getBool() )
                tagValue.setInt( tagBlokValue.getInt() );

            if( tagSPmin.getInt() < tagSPmax.getInt() ) {
                tagMin.setBool(tagValue.getInt() <= tagSPmin.getInt());
                tagMax.setBool(tagValue.getInt() >= tagSPmax.getInt());

                if (tagMin.getBool())
                    tagPercent.setInt(0);
                else if (tagMax.getBool())
                    tagPercent.setInt(100);
                else {
                    tagPercent.setDouble( Math.round(
                            100.0 * ((float)(tagValue.getInt() - tagSPmin.getInt())) /
                                    ((float)(tagSPmax.getInt() - tagSPmin.getInt())) ));
                }
            } else {
                tagPercent.setInt(0);
            }



            protocol.buffout[1] = 1;
            protocol.buffout[2] = 2;
            protocol.buffout[3] = 2;
            if( result  &&
                    (result = protocol.request(6, protocol.calcTmprPackageLen(tagTmprCnt.getInt())))) {
                int n = Math.min(tagTmprCnt.getInt(), size);
                for(int i=0; i<n; ++i) {
                    short v = (short)Numbers.bytesToWord(protocol.buffin, i*2 + 3);
                    tagT[i].setDouble( ((double)v) * 10.0 / 16 );
                }
            }


        } catch (Exception e) {
            env.printError(logger, e, name);
        }

        return result;
    }


    @Override
    protected boolean reload() {
        Kontakt1Tur01Module tmp = new Kontakt1Tur01Module(plugin, name);
        if( !tmp.load() )
            return false;

        copySettingsFrom(tmp);

        if( size < tmp.size ) {
            System.arraycopy( tagT, 0, tmp.tagT, 0, size );
            tagT = tmp.tagT;
            for( int i=size; i<tmp.size; ++i)
                tagtable.add( tagT[i] );
        }

        if( size > tmp.size ) {
            System.arraycopy( tagT, 0, tmp.tagT, 0, tmp.size );
            for( int i=tmp.size; i<size; ++i)
                tagtable.remove( tagT[i] );
            tagT = tmp.tagT;
        }

        size = tmp.size;

        return true;
    }


}