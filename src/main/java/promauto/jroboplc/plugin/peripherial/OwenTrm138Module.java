package promauto.jroboplc.plugin.peripherial;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import promauto.jroboplc.core.api.Configuration;
import promauto.jroboplc.core.api.Flags;
import promauto.jroboplc.core.api.Plugin;
import promauto.jroboplc.core.api.Tag;

import java.util.List;


public class OwenTrm138Module extends PeripherialModule {

	private static final int SIZE = 8;
	private final Logger logger = LoggerFactory.getLogger(OwenTrm138Module.class);

	protected ProtocolModbus protocol = new ProtocolModbus(this);

	public static class Sensor {
		protected Tag tagValue;
		protected Tag tagError;
		protected int mul;
		protected boolean badIfError;
	}

	protected Sensor[] sensors = new Sensor[SIZE];


	public OwenTrm138Module(Plugin plugin, String name) {
		super(plugin, name);
	}


	@Override
	public boolean loadPeripherialModule(Object conf) {
		Configuration cm = env.getConfiguration();

		int mul = cm.get(conf, "mul", 100);
		boolean badIfError = cm.get(conf, "badIfError", false);

		for(int i=0; i<SIZE; ++i) {
			sensors[i] = new Sensor();
			sensors[i].mul = cm.get(conf, "mul." + i, mul);
			sensors[i].badIfError = cm.get(conf, "badIfError." + i, badIfError);
			sensors[i].tagValue = tagtable.createInt("d" + i + ".value", 0, Flags.STATUS);
			sensors[i].tagError = tagtable.createInt("d" + i + ".error", 0, Flags.STATUS);
		}

		return true;
	}


	@Override
	protected void initChannelMap(List<String> chtags) {
		for( int i=0; i<SIZE; ++i ) {
			addChannelMapTag(chtags, sensors[i].tagValue, ""+i);
		}
	}



	@Override
	public boolean executePeripherialModule() {
		
		if(emulated) 
			return true;
		
		boolean result = true;
		
		try {
			result = protocol.requestCmd4(0, SIZE * 5);
			if( result ) {
				for(int i=0; i<SIZE; ++i) {
					int error = protocol.getAnswerWord(i * 5 + 2);
					sensors[i].tagError.setInt(error);

					sensors[i].tagValue.setDouble(
							protocol.getAnswerFloat(i * 5 + 3) * (double) sensors[i].mul
					);

					if( sensors[i].badIfError ) {
						if( error == 0  &&  sensors[i].tagValue.getStatus() != Tag.Status.Good )
							sensors[i].tagValue.setStatus(Tag.Status.Good);

						if( error > 0  &&  sensors[i].tagValue.getStatus() != Tag.Status.Bad )
							sensors[i].tagValue.setStatus(Tag.Status.Bad);
					}
				}
			}
		} catch (Exception e) {
			env.printError(logger, e, name);
			result = false;
		}
	
		return result;
	}


	@Override
	protected boolean reload() {
		OwenTrm138Module tmp = new OwenTrm138Module(plugin, name);
		if( !tmp.load() )
			return false;

		copySettingsFrom(tmp);

		for(int i=0; i<SIZE; ++i) {
			sensors[i].mul = tmp.sensors[i].mul;
			sensors[i].badIfError = tmp.sensors[i].badIfError;
		}

		return true;
	}


}
