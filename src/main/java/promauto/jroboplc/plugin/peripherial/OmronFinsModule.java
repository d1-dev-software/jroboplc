package promauto.jroboplc.plugin.peripherial;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import promauto.jroboplc.core.api.Configuration;
import promauto.jroboplc.core.api.Flags;
import promauto.jroboplc.core.api.Plugin;
import promauto.jroboplc.core.api.Tag;
import promauto.jroboplc.core.tags.*;
import promauto.utils.Numbers;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class OmronFinsModule extends PeripherialModule {

    private final Logger logger = LoggerFactory.getLogger(OmronFinsModule.class);

    protected ProtocolOmronFins protocol = new ProtocolOmronFins(this);

    protected enum FinsTagType { BOOL, INT16, UINT16, INT32, UINT32, FLOAT16, FLOAT32 }

    protected enum FinsTagAccess { RO, WO, RW }

    protected enum FinsCommandRead { AREA, MULTI }


    protected static class FinsTag {
        public String name;
        public FinsTagType type;
        public boolean inverted;
        public int datasize;
        public int elementsize;
        public int region;
        public FinsTagAccess access;
        public FinsCommandRead cmdread;
        public int addressbit;
        public int address;
        public int duplex;
        public boolean readEnd;
        public boolean enable;
        public TagRW tag;

        public boolean needWrite;

        public String tracktagName;
        public FinsTag tracktag;


        public void init() {

            elementsize = ProtocolOmronFins.getElementSize(region);

            if( elementsize == 1) {
                datasize = 1;
                address = addressbit;
                duplex = 0;
            } else if( elementsize == 2) {
                datasize = (
                        type == FinsTagType.FLOAT32  ||
                        type == FinsTagType.INT32  ||
                        type == FinsTagType.UINT32
                    )? 4: 2;

                address = addressbit >> 4;
                duplex = datasize / elementsize - 1;
            }

            switch( type ) {
                case BOOL:
                    tag = new TagRWBool(name, false, Flags.STATUS); break;
                case INT16: case UINT16: case INT32:
                    tag = new TagRWInt(name, 0, Flags.STATUS); break;
                case UINT32:
                    tag = new TagRWLong(name, 0, Flags.STATUS); break;
                case FLOAT16: case FLOAT32:
                    tag = new TagRWDouble(name, 0.0, Flags.STATUS);	break;
            }


        }

        public void putValueIntoBuff(int[] buff, int pos) {
            if( elementsize == 1) {
                if( type == FinsTagType.BOOL)
                    buff[pos] = (tag.getWriteValBool() ^ inverted) ? 1 : 0;
                else
                    buff[pos] = tag.getWriteValInt() & 0xFF;

            } else if( elementsize == 2 ) {
                switch (type) {
                    case BOOL: {
                        buff[pos] = 0;
                        buff[pos + 1] = (tag.getWriteValBool() ^ inverted) ? 1 : 0;
                        break;
                    }
                    case INT16:
                    case UINT16: {
                        int i = tag.getWriteValInt() & 0xFFFF;
                        buff[pos] = i >> 8;
                        buff[pos+1] = i & 0xFF;
                        break;
                    }
                    case INT32: case UINT32: {
                        long l = tag.getWriteValInt() & 0xFFFF_FFFF;
                        buff[pos]   = (int)((l >> 24) & 0xFF);
                        buff[pos+1] = (int)((l >> 16) & 0xFF);
                        buff[pos+2] = (int)((l >> 8) & 0xFF);
                        buff[pos+3] = (int)(l & 0xFF);
                        break;
                    }
                    case FLOAT16: {
                        int i = Numbers.encodeFloat16((float) (tag.getWriteValDouble()));
                        buff[pos] = i >> 8;
                        buff[pos+1] = i & 0xFF;
                        break;
                    }
                    case FLOAT32: {
                        long l = Numbers.encodeFloat32((float) (tag.getWriteValDouble())) & 0xFFFF_FFFFL;
                        buff[pos]   = (int)((l >> 24) & 0xFF);
                        buff[pos+1] = (int)((l >> 16) & 0xFF);
                        buff[pos+2] = (int)((l >> 8) & 0xFF);
                        buff[pos+3] = (int)(l & 0xFF);
                        break;
                    }
                }
            } else {
                // not implemented for elementsize > 2
            }
        }

        public void fetchValueFromProtocolBuffin(ProtocolOmronFins protocol, int pos) {
            if( cmdread == FinsCommandRead.MULTI )
                pos++;

            if( elementsize == 1) {
                if( type == FinsTagType.BOOL)
                    tag.setReadValBool((protocol.getDataByte(pos) != 0) ^ inverted);
                else
                    tag.setReadValInt(protocol.getDataByte(pos));

            } else if( elementsize == 2 ) {
                switch( type ) {
                    case BOOL:
                        tag.setReadValBool( (protocol.getDataWord(pos) != 0) ^ inverted );
                        break;
                    case INT16:
                        tag.setReadValInt( (short)(protocol.getDataWord(pos)) );
                        break;
                    case UINT16:
                        tag.setReadValInt( protocol.getDataWord(pos) );
                        break;
                    case INT32:
                        if( cmdread == FinsCommandRead.MULTI ) {
                            // not implemented
                        } else {
                            tag.setReadValInt( protocol.getAnswerInt(pos) );
                        }
                        break;
                    case UINT32:
                        if( cmdread == FinsCommandRead.MULTI ) {
                            // not implemented
                        } else {
                            tag.setReadValLong( protocol.getAnswerInt(pos) & 0xFFFF_FFFF );
                        }
                        break;
                    case FLOAT16:
                        tag.setReadValDouble( Numbers.decodeFloat16(protocol.getDataWord(pos)));
                        break;
                    case FLOAT32:
                        if( cmdread == FinsCommandRead.MULTI ) {
                            // not implemented
                        } else {
                            tag.setReadValDouble(Numbers.decodeFloat32(protocol.getAnswerInt(pos)));
                        }
                        break;
                }
            } else {
                // not implemented for elementsize > 2
            }
        }

    }

    protected TagRW tagAnswerStatus;

    protected int maxDataSize;

    protected List<FinsTag> finstags = new ArrayList<>();




    public OmronFinsModule(Plugin plugin, String name) {
        super(plugin, name);
    }


    @Override
    public boolean loadPeripherialModule(Object conf) {
        finstags.clear();

        Configuration cm = env.getConfiguration();

        maxDataSize = Math.min( cm.get(conf, "maxDataSize", 250), 2000);

        try {
            for(Object conf_tag: cm.toList( cm.get(conf, "tags"))) {
                FinsTag ftag = new FinsTag();
                ftag.name = cm.get(conf_tag, "name", "");
                ftag.type = FinsTagType.valueOf( cm.get(conf_tag, "type", "uint16").toUpperCase() );
                ftag.cmdread = FinsCommandRead.valueOf( cm.get(conf_tag, "cmdread", "area").toUpperCase() );
                ftag.inverted = cm.get(conf_tag, "inverted", false);
                ftag.region = cm.get(conf_tag, "region", 176);
                ftag.access = FinsTagAccess.valueOf( cm.get(conf_tag, "access", "rw").toUpperCase() );
                ftag.addressbit = (cm.get(conf_tag, "address", 0) << 4) +
                        (cm.get(conf_tag, "bit", 0) & 0x0F);
                ftag.readEnd = cm.get(conf_tag, "readEnd", false);
                ftag.enable = cm.get(conf_tag, "enable", true);
                ftag.tracktagName = cm.get(conf_tag, "tracktag", "");

                ftag.init();

                if( ftag.elementsize == 0 ) {
                    env.printError(logger, name, ftag.name, "Bad region:", ftag.region+"");
                    return false;
                }

                if( ftag.cmdread == FinsCommandRead.MULTI  &&  ftag.duplex > 0 ) {
                    env.printError(logger, name, ftag.name, "Incompatible tagtype and cmdread");
                    return false;
                }

                tagtable.add(ftag.tag);
                finstags.add(ftag);
            }
        } catch (IllegalArgumentException e) {
            env.printError(logger, e, name);
            return false;
        }

        init();

        tagAnswerStatus = tagtable.createRWInt("AnswerStatus", 0);

        return true;
    }



    private void init() {
        finstags.sort((a, b) -> {
            int x = ((a.cmdread==FinsCommandRead.MULTI?1:0) << 28) +  (a.region << 20) + a.addressbit;
            int y = ((b.cmdread==FinsCommandRead.MULTI?1:0) << 28) +  (b.region << 20) + b.addressbit;
            return x - y;
        });

        Map<String, FinsTag> map = finstags.stream().collect(Collectors.toMap(c -> c.name, c -> c));
        for(FinsTag ftag: finstags)
            if( !ftag.tracktagName.isEmpty() ) {
                ftag.tracktag = map.get(ftag.tracktagName);
            }

    }



    @Override
    public boolean executePeripherialModule() {
        boolean res = true;

        if( tagAnswerStatus.hasWriteValue() )
            protocol.resetLastBadAnswerStatus();

        if(emulated) {
            for( FinsTag ftag: finstags)
                ftag.tag.acceptWriteValue();
            return res;
        }


        boolean needWrite = false;
        for( FinsTag ftag: finstags) {
            if( ftag.access == FinsTagAccess.RO )
                continue;

            ftag.needWrite = ftag.enable  &&
                    (ftag.tag.hasWriteValue()  ||
                            (ftag.tracktag != null  &&  !ftag.tag.equalsValue(ftag.tracktag.tag)));

            needWrite |= ftag.needWrite;
        }


        if( needWrite )
            res &= write();


        res &= read();

        tagAnswerStatus.setReadValInt( protocol.getLastBadAnswerStatus() );

        return res;
    }




    private boolean write() {
        int begidx = 0;
        int lastidx = 0;
        FinsTag begtag = null;
        FinsTag lasttag = null;
        for (int i = 0; i < finstags.size(); ++i) {
            FinsTag ftag = finstags.get(i);
            if (!ftag.needWrite)
                continue;

            if (lasttag != null) {
                boolean finish = ftag.region != lasttag.region;  // check 1: region sameness
                finish |= (ftag.address - lasttag.address + lasttag.duplex ) != 1; // check 2: address adjacency

                int readsize = (ftag.address + ftag.duplex - begtag.address + 1) * ftag.elementsize;
                finish |= readsize > maxDataSize; // check 3: max write size

                if (finish) {
                    if( !writeRequest(begidx, lastidx) )
                        return false;
                    lasttag = null;
                } else {
                    begtag = ftag;
                    lasttag = ftag;
                    lastidx = i;
                }
            }

            if (lasttag == null) {
                begidx = i;
                lastidx = i;
                begtag = ftag;
                lasttag = ftag;
            }
        }

        if (lasttag != null)
            if( !writeRequest(begidx, lastidx) )
                return false;

        return true;
    }


    private boolean writeRequest(int begidx, int lastidx) {
        FinsTag begtag = finstags.get(begidx);
        FinsTag lasttag = finstags.get(lastidx);

        int num = lasttag.address + lasttag.duplex - begtag.address +  1;

        ProtocolOmronFins.SetDataBuffoutFunction setdatafunc = (i, k) -> {
            FinsTag ftag = finstags.get(i);
            ftag.putValueIntoBuff(protocol.buffout, k);
            if( ftag.access == FinsTagAccess.WO )
                ftag.tag.copyLastWriteToRead();
            return ftag.datasize;
        };

        boolean result;
        try {
            result = protocol.request0102(begtag.region, begtag.elementsize, begtag.addressbit, num,
                    begidx, lastidx, setdatafunc);

        } catch (Exception e) {
            env.printError(logger, e, name);
            result = false;
        }

        if( !result )
            for (int i = begidx; i <= lastidx; ++i)
                finstags.get(i).tag.raiseWriteValue();

        return result;
    }





    private boolean read() {
        int begidx = 0;
        int lastidx = 0;
        FinsTag begtag = null;
        FinsTag lasttag = null;
        for (int i = 0; i < finstags.size(); ++i) {
            FinsTag ftag = finstags.get(i);
            if( ftag.access == FinsTagAccess.WO )
                continue;

            if (lasttag != null) {
                boolean finish = lasttag.readEnd; // check 1: read end
                finish |= ftag.cmdread != lasttag.cmdread; // check 2: command sameness

                if( ftag.cmdread == FinsCommandRead.AREA ) {
                    finish |= ftag.region != lasttag.region; // check 3: region sameness

                    int readsize = (ftag.address + ftag.duplex - begtag.address + 1) * ftag.elementsize;
                    finish |= readsize  > maxDataSize; // check 4: max read size
                } else {
                    int writesize = (i - begidx + 1) * 4;
                    finish |= writesize > maxDataSize; // check 5: max write size
                }

                if (finish) {
                    if( !readRequest(begidx, lastidx) )
                        return false;
                    lasttag = null;
                } else {
                    lastidx = i;
                    lasttag = ftag;
                }
            }

            if (lasttag == null) {
                begtag = ftag;
                lasttag = ftag;
                begidx = i;
                lastidx = i;
            }
        }

        if (lasttag != null)
            if( !readRequest(begidx, lastidx) )
                return false;

        return true;
    }


    private boolean readRequest(int begidx, int lastidx) {
        FinsCommandRead cmdread = finstags.get(begidx).cmdread;

        if( cmdread == FinsCommandRead.AREA )
            return readRequestArea(begidx, lastidx);
        else
            return readRequestMulti(begidx, lastidx);
    }


    private boolean readRequestArea(int begidx, int lastidx) {
        FinsTag begtag = finstags.get(begidx);
        FinsTag lasttag = finstags.get(lastidx);
        int num = lasttag.address + lasttag.duplex - begtag.address +  1;

        boolean result;
        try {
            result = protocol.request0101(begtag.region, begtag.elementsize, begtag.addressbit, num);

        } catch (Exception e) {
            env.printError(logger, e, name);
            result = false;
        }

        if( result  &&  protocol.isAnswerStatusOk() ) {
            int k;
            for (int i = begidx; i <= lastidx; ++i) {
                FinsTag ftag = finstags.get(i);
                k = (ftag.address - begtag.address) * ftag.elementsize;
                ftag.fetchValueFromProtocolBuffin(protocol, k);
            }
        }

        return result;
    }


    private boolean readRequestMulti(int begidx, int lastidx) {
        int num = lastidx - begidx +  1;
        int readsize = finstags.subList(begidx, lastidx+1).stream().mapToInt(x -> x.datasize + 1).sum();

        ProtocolOmronFins.SetDataBuffoutFunction setdatafunc = (i, k) -> {
            FinsTag ftag = finstags.get(i);
            protocol.buffout[k] = ftag.region;
            protocol.buffout[k+1] = (ftag.addressbit >> 12) & 0xFF;
            protocol.buffout[k+2] = (ftag.addressbit >> 4) & 0xFF;
            protocol.buffout[k+3] = ftag.addressbit & 0xF;
            return 4;
        };

        boolean result;
        try {
            result = protocol.request0104(num, readsize, begidx, lastidx, setdatafunc);

        } catch (Exception e) {
            env.printError(logger, e, name);
            result = false;
        }

        if( result  &&  protocol.isAnswerStatusOk() ) {
            int k = 0;
            for (int i = begidx; i <= lastidx; ++i) {
                FinsTag ftag = finstags.get(i);
                ftag.fetchValueFromProtocolBuffin(protocol, k);
                k += ftag.datasize + 1;
            }
        }

        return result;
    }


    @Override
    protected boolean reload() {
        OmronFinsModule tmp = new OmronFinsModule(plugin, name);
        if( !tmp.load() )
            return false;

        copySettingsFrom(tmp);

        maxDataSize = tmp.maxDataSize;

        for(FinsTag mtag: finstags) {
            Tag found = tmp.tagtable.get(mtag.tag.getName());
            if( found == null  ||  found.getType() != mtag.tag.getType()  ||  !(found instanceof TagRW) )
                tagtable.remove(mtag.tag);
        }

        for(FinsTag mtag: tmp.finstags) {
            TagRW tag = (TagRW)tagtable.get(mtag.tag.getName());
            if( tag == null )
                tagtable.add( mtag.tag );
            else {
                mtag.tag = tag;
            }
        }

        finstags = tmp.finstags;
        init();

        updateTagsStatus();

        return true;
    }

}
