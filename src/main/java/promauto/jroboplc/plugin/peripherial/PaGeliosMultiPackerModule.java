package promauto.jroboplc.plugin.peripherial;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import promauto.jroboplc.core.api.Configuration;
import promauto.jroboplc.core.api.Flags;
import promauto.jroboplc.core.api.Plugin;
import promauto.jroboplc.core.api.Tag;
import promauto.jroboplc.core.tags.TagRW;
import promauto.utils.CRC;
import promauto.utils.Numbers;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class PaGeliosMultiPackerModule extends PeripherialModule {
	private final Logger logger = LoggerFactory.getLogger(PaGeliosMultiPackerModule.class);

	protected ProtocolAA55 protocol = new ProtocolAA55(this);


	protected static class TagPair {
		Tag tagWeight;
		Tag tagNum;
		int addr;
	}


	protected List<TagPair> pairs = new LinkedList<>();

	private TagRW tagFirmware;


	private final int[] buffin = new int[9];
	private final int[] buffout = new int[5];
	protected boolean multireq = false;



	public PaGeliosMultiPackerModule(Plugin plugin, String name) {
		super(plugin, name);
	}

	
	@Override
	public boolean loadPeripherialModule(Object conf) {
		Configuration cm = env.getConfiguration();
		multireq = cm.get(conf, "multireq", false);

		cm.toMap(cm.get(conf, "tag.pairs")).forEach((regname, addr) -> {
			TagPair tp = new TagPair();
			tp.tagWeight = tagtable.createLong( regname + ".weight" , 0, Flags.STATUS);
			tp.tagNum = tagtable.createLong( regname + ".num" , 0, Flags.STATUS);
			tp.addr = Integer.parseInt(addr.toString());
			pairs.add(tp);
		});

		tagFirmware = tagtable.createRWString("firmware", "", Flags.STATUS);
		return true;
	}


	@Override
	public boolean executePeripherialModule() {
		boolean result = true;

		if( emulated ) {
			return true;
		}

		try {
			for( TagPair tp: pairs) {
				buffout[0] = 0x55;
				buffout[1] = 0xA0 + netaddr;
				buffout[2] = (tp.addr >> 8) & 0xFF;
				buffout[3] = tp.addr & 0xFF;
				buffout[4] = CRC.getCrc8(buffout, 1, 3);

				if (!multireq) {
					result = protocol.request(buffout, 5, buffin, 9, ProtocolAA55.Crc8);
				} else {
					result = protocol.multiRequest(buffout, 5, buffin, 9, 9, ProtocolAA55.Crc8);
				}

				if (result) {
					tp.tagNum.setLong(Numbers.bytesToDWord(buffin, 0));
					tp.tagWeight.setLong(Numbers.bytesToDWord(buffin, 4));
				}

				if( result  &&  (firstPass  || tagError.getBool())) {
					result = protocol.requestFirmware( tagFirmware );
				}
			}

		} catch (Exception e) {
			env.printError(logger, e, name);
		}

		return result;
	}

	



}

