package promauto.jroboplc.plugin.peripherial;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import promauto.jroboplc.core.api.Configuration;
import promauto.jroboplc.core.api.Flags;
import promauto.jroboplc.core.api.Plugin;
import promauto.jroboplc.core.api.Tag;
import promauto.jroboplc.core.tags.TagRW;
import promauto.utils.CRC;

// todo: Добавить Flags.STATUS в соответствующие теги

public class MercuryM230Module extends PeripherialModule{
    private final Logger logger = LoggerFactory.getLogger(SchuleFlowModule.class);
    private static int  MERCURY_RD_SERIAL_CNT = 120;

    protected Tag tagSerialNumberH;
    protected Tag tagSerialNumberL;
    protected Tag tagDayManuf;
    protected Tag tagMonthManuf;
    protected Tag tagYearManuf;
    protected Tag tagSessionOpened;
    protected Tag tagPSumAfterResetH;
    protected Tag tagPSumAfterResetL;
    protected Tag tagPSumForYearH;
    protected Tag tagPSumForYearL;
    protected Tag tagCrc;
    protected TagRW tagWesSvrState;
    protected Tag[] crcTags;

    protected int  askingCnt = 120;
   

    private static int BUFF_SIZE = 60;
    protected int[] buffin = new int[BUFF_SIZE];
    protected int[] buffout = new int[BUFF_SIZE];


    protected int accessLevel   =1;
    protected String password ="11111";
	
    /*
connect to counter with acces level 1 and password
111111 - ascii codes for password. // actualy not ascii just 0x01 0x01 0x01 0x01 0x01 0x01
send: 80 01 01 31 31 31 31 31 31 (CRC)
receive: 80 00 (CRC)
it`s ok
 */
/*
serial number and date of creation
    adr code function
    80 08 00 (CRC)
    Ответ: 80 04 2F 0D 3B 02 06 06 (CRC)
                          | date is 02.06.06
 */

/*
energy from some period
 adr code(0x5 or 0x15) (nArray NMonth = 1Byte) NTarif = 1 Byte (CRC) -> total 6 bytes
 NArray = 0 -from cnt reset
 NArray = 1 - for this year
 NArray = 2 - for last year and so on

 NTarif = 0 - sum all tarifs
 NTarif = 1 - tarif 1 and so on

Energy for current month
Запрос: 80 05 31 00 (CRC)
Ответ: 80 00 00 70 0A FF FF FF FF 00 00 E8 03 00 00 00 00 (CRC)
 */
    public MercuryM230Module(Plugin plugin, String name) {
        super(plugin, name);
    }

    @Override
    public boolean loadPeripherialModule(Object conf) {
        Configuration cm = env.getConfiguration();
        accessLevel      = cm.get(conf, "access", 1);
        password         = cm.get(conf, "password", "111111");

        tagCrc 			= tagtable.createInt("Crc", 				0);

        tagSerialNumberH		= tagtable.createInt("SerialNumberH", 		0, Flags.STATUS);
        tagSerialNumberL		= tagtable.createInt("SerialNumberL", 		0, Flags.STATUS);
        tagDayManuf			    = tagtable.createInt("DayManuf", 			    0, Flags.STATUS);
        tagMonthManuf			= tagtable.createInt("MonthManuf", 			0, Flags.STATUS);
        tagYearManuf			= tagtable.createInt("YearManuf", 			0, Flags.STATUS);
        tagSessionOpened    	= tagtable.createInt("SessionOpened", 		0, Flags.STATUS);
        tagPSumAfterResetH  	= tagtable.createInt("PSumAfterResetH", 		0, Flags.STATUS);
        tagPSumAfterResetL  	= tagtable.createInt("PSumAfterResetL", 		0, Flags.STATUS);
        tagPSumForYearH     	= tagtable.createInt("PSumForYearH", 			0, Flags.STATUS);
        tagPSumForYearL     	= tagtable.createInt("PSumForYearL", 			0, Flags.STATUS);
        tagWesSvrState		    = tagtable.createRWInt("WesSvrState",	0);

        crcTags = new Tag[] {
                tagSerialNumberH,
                tagSerialNumberL,
                tagPSumAfterResetH,
                tagPSumAfterResetL
        };
        initCrc16Tags();

        return true;
    }
    @Override
    public boolean executePeripherialModule() {

        boolean result = true;
        if(!emulated){
            try{
                // open session
                buffout[0] = netaddr;
                buffout[1] = 0x01; // operation code
                buffout[2] = 0x01; // access level user = 1
                // 6 bytes for password
                buffout[3] = 0x01;
                buffout[4] = 0x01;
                buffout[5] = 0x01;
                buffout[6] = 0x01;
                buffout[7] = 0x01;
                buffout[8] = 0x01;

                int crc = CRC.getCrc16(buffout, 9);
                buffout[9] = crc & 0xFF;
                buffout[10] = (crc >> 8) & 0xFF;
                if( result = request(11,4) ) {
                    tagSessionOpened.setInt(1);
//                    80 05 31 00
                    buffout[0] = netaddr;
                    buffout[1] = 0x05; // energy consumption
                    buffout[2] = 0x00;
                    buffout[3] = 0x00;
                    crc = CRC.getCrc16(buffout, 4);
                    buffout[4] = crc & 0xFF;
                    buffout[5] = (crc >> 8) & 0xFF;
                    if( result = request(6,19) ) {
//                        80 00 00 70 0A FF FF FF FF 00 00 E8 03 00 00 00 00
                        int pH = 0;
			pH = (buffin[2] << 8) + buffin[1];
                        int pL = 0;
                        pL = (buffin[4] << 8) + buffin[3];
                        tagPSumAfterResetH       .setInt(pH);
                        tagPSumAfterResetL       .setInt(pL);
			// ask data for  ths year
			
			if(askingCnt>=MERCURY_RD_SERIAL_CNT){
				
                    		buffout[0] = netaddr;
                    		buffout[1] = 0x08; // energy consumption
                    		buffout[2] = 0x00;

                    		crc = CRC.getCrc16(buffout, 3);
		                buffout[3] = crc & 0xFF;
                		buffout[4] = (crc >> 8) & 0xFF;
	                        if( result = request(5,10) ) {
					tagSerialNumberH	.setInt((buffin[2]<<8)+buffin[1]);
					tagSerialNumberL	.setInt((buffin[4]<<8)+buffin[3]);
					tagDayManuf	.setInt(buffin[5]);
					tagMonthManuf	.setInt(buffin[6]);
					tagYearManuf	.setInt(buffin[7]);
				}
	  		   	askingCnt = 0;
			}else askingCnt++;
			//
                    }


                }else tagSessionOpened.setInt(0);

            }catch (Exception e) {
                env.printError(logger, e, name);
                result = false;
            }


        }

        return result;
    }


    @Override
    protected void initCrc16Tags() {
        super.initCrc16Tags();
        for(Tag tag: crcTags)
            crc16Tags.add( tag );
        crc16Tags.add( tagError );
    }


    private boolean request(int iSizeOut,int iSizeIn) throws Exception {
        if( port == null )
            return false;

        for (int trynum = 0; trynum < retrial; trynum++) {

            port.discard();
            port.writeBytes(buffout, iSizeOut);
            int n = port.readBytes(buffin, iSizeIn);

            int crc = CRC.getCrc16(buffin, iSizeIn-2);
            if(((crc & 0xFF) == buffin[iSizeIn-2])&&(((crc>>8) & 0xFF) == buffin[iSizeIn-1])){
                if(buffin[0]==netaddr)
                    return true;
            }
            boolean badcrc = true;
            //

            if( canLogError() ) {
                int cntRead = Math.abs(n);
                logError(trynum, badcrc, buffout, BUFF_SIZE, buffin, cntRead, "");
            }

            try {
                Thread.sleep(20);
            } catch (InterruptedException e) {
            }

        }
        tagErrorCnt.setInt( tagErrorCnt.getInt() + 1 );

        return false;
    }
}
