package promauto.jroboplc.plugin.peripherial;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import promauto.jroboplc.core.api.Flags;
import promauto.jroboplc.core.api.Plugin;
import promauto.jroboplc.core.tags.TagRW;

import java.util.List;


public class OwenMu110_16rModule extends PeripherialModule {
    private final Logger logger = LoggerFactory.getLogger(OwenMu110_16rModule.class);

    protected ProtocolModbus protocol = new ProtocolModbus(this);

    protected TagRW tagTOut;
    protected TagRW[] 	tagOut = null;
    protected int           reqOut;
    protected int           curOut;

    protected final int size;
    protected final int regOut;
    protected final int regTOut;


    public OwenMu110_16rModule(Plugin plugin, String name) {
        super(plugin, name);

        size = 16;
        regOut = 50;
        regTOut = 48;
    }


    @Override
    public boolean loadPeripherialModule(Object conf) {

        tagTOut	= protocol.addWriteTag( 48, tagtable.createRWInt("TOut", 0, Flags.STATUS));

        if( tagOut == null ) {
            tagOut = new TagRW[size];
        }

        for(int i = 0; i< size; ++i) {
            tagOut[i] = tagtable.createRWInt(String.format("out%02d", i), 0, Flags.STATUS);
        }
        reqOut = 0;

        return true;
    }


    @Override
    protected void initChannelMap(List<String> chtags) {
        for( int i=0; i<size; ++i ) {
            addChannelMapTag(chtags, tagOut[i], "" + i);
        }
    }



    @Override
    public boolean executePeripherialModule() {

        if(emulated) {
            tagTOut.acceptWriteValue();
            for(int i = 0; i< size; ++i) {
                tagOut[i].acceptWriteValue();
            }
            return true;
        }

        boolean result = true;

        try {
            if( result )
                result = protocol.sendWriteTags(16);

            if( result ) {
                for (int i = 0; i < size; ++i) {
                    if (tagOut[i].hasWriteValue())
                        if (tagOut[i].getWriteValBool()) {
                            reqOut |= (1 << i);
                        } else {
                            reqOut &= ~(1 << i);
                        }
                }
            }

            result &= writeOutputs();

            result &= readOutputs();

            if( result  &&  (firstPass  ||  tagError.getBool()) ) {
                if( result = protocol.requestCmd3(regTOut, 1) )
                    tagTOut.setReadValInt( protocol.getAnswerWord(0) );
            }

        } catch (Exception e) {
            env.printError(logger, e, name);
            result = false;
        }

        return result;
    }


    protected boolean writeOutputs() throws Exception {
        if( curOut == reqOut )
            return true;

        return protocol.requestCmd10(regOut, 1, reqOut);
    }


    protected boolean readOutputs() throws Exception {
        if( !protocol.requestCmd3(regOut, 1) )
            return false;

        curOut = protocol.getAnswerWord(0);
        for (int i = 0; i < size; ++i)
            tagOut[i].setReadValInt((curOut >> i) & 1);

        return true;
    }


    @Override
    protected boolean reload() {
        OwenMu110_16rModule tmp = new OwenMu110_16rModule(plugin, name);
        if( !tmp.load() )
            return false;

        copySettingsFrom(tmp);

        reqOut = tmp.reqOut;
        curOut = tmp.curOut;

        return true;
    }

}
