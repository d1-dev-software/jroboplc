package promauto.jroboplc.plugin.peripherial;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import promauto.jroboplc.core.api.Flags;
import promauto.jroboplc.core.api.Plugin;
import promauto.jroboplc.core.api.Tag;

public class TermoMuxModule extends PeripherialModule {

    private final Logger logger = LoggerFactory.getLogger(PaTermo5Module.class);

    static final int VALUES_OFFSET = 3;
    static final int VALUE_SIZE = 5;
    static final int PDV_COUNT = 8;
    static final int T_COUNT = 12;

    private int[] bufout = new int[5];
    private int[] bufinp = new int[66];
    private int[] bufinp1 = new int[61];
    private boolean shortAnswer;


    public static class Pdv {
        Tag[] tagT = new Tag[T_COUNT];
        public Tag tagTime;
    }

    public Pdv[] pdvs = new Pdv[PDV_COUNT];


    public TermoMuxModule(Plugin plugin, String name) {
        super(plugin, name);
    }


    @Override
    public boolean loadPeripherialModule(Object conf) {
        for(int i=0; i<PDV_COUNT; ++i) {
            Pdv pdv = new Pdv();
            pdvs[i] = pdv;

            String s = "Pdv" + i + ".";
            for(int j=0; j<T_COUNT; ++j) {
                pdv.tagT[j] = tagtable.createInt(s + "T" + j, 0, Flags.STATUS);
            }
            pdv.tagTime = tagtable.createInt(s + "Time", 0, Flags.STATUS);
        }
        return true;
    }


    @Override
    protected void initChannelMap(List<String> chtags) {
        for( int i=0; i<PDV_COUNT; ++i )
            for(int j=0; j<T_COUNT; ++j) {
                addChannelMapTag(chtags, pdvs[i].tagT[j], i + "." + j);
            }
    }

    /*
      long answer size 66:
      3E-40-53-30-0D
      3C-01-40-  30-33-35-2E-35  -30-33-33-2E-36-30-33-32-2E-31-30-32-32-2E-36-30-32-30-2E-37-30-31-39-2E-31-2D-39-39-2E-39-2D-39-39-2E-39-2D-39-39-2E-39-2D-39-39-2E-39-2D-39-39-2E-39-2D-39-39-2E-39-30-72-0D
      <?I027.1027.2027.2027.4026.9026.6026.7-99.9-99.9-99.9-99.9-99.90X<crc>

      short answer size 5:
      3E-42-53-33-0D
      3C-01-42-15-0D

    */

    @Override
    public boolean executePeripherialModule() {
        if(emulated)
            return true;

        boolean result = true;
        try {

            for( int i=0; i<PDV_COUNT; ++i ) {
                result = request(i);
                if( !result)
                    break;

                Pdv pdv = pdvs[i];
                int tnum = 0;

                if( shortAnswer ) {
                    pdv.tagTime.setInt(2000);
                } else {
                    for (int j = PDV_COUNT - 1; j >= 0; --j) {
                        int value = getValue(j);
                        pdv.tagT[tnum].setInt(value);
                        if (value != -999 || tnum > 0) {
                            tnum++;
                        }
                    }
                    pdv.tagTime.setInt(10);
                }

                for( int j=tnum; j<T_COUNT; ++j) {
                    pdv.tagT[j].setInt(-999);
                }
            }

        } catch (Exception e) {
            env.printError(logger, e, name);
            result = false;
        }

        return result;
    }


    private int getValue(int num) {
        int k = getPos(num);
        int value = (bufinp[k+1] - 0x30) * 100;
        value += (bufinp[k+2] - 0x30) * 10;
        value += bufinp[k+4] - 0x30;

        if( bufinp[k]==0x2d )
            value *= -1;
        else
            value += (bufinp[k] - 0x30) * 1000;

        return value;
    }


    private int getPos(int num) {
        return VALUES_OFFSET + VALUE_SIZE * num;
    }


    public boolean request(int pdvnum) throws Exception {
        for (int trynum = 0; trynum < retrial; trynum++) {
            port.discard();
            delayBeforeWrite();
            int n = 0;

            bufout[0] = 0x3e;
            bufout[1] = 0x3f + netaddr;
            bufout[2] = 0x53;
            bufout[3] = 0x30 + pdvnum;
            bufout[4] = 0x0d;
            port.writeBytes(bufout, 5);

            if( port.readBytes(bufinp, 5) == 5 ) {
                if( bufinp[0]==0x3c  &&  bufinp[1]==1  &&  bufinp[2]==bufout[1] ) {
                    if( bufinp[3]==0x15  &&  bufinp[4]==0x0d) {
                        shortAnswer = true;
                        return true;
                    }

                    shortAnswer = false;
                    if( port.readBytes(bufinp1, 61) == 61 ) {
                        System.arraycopy(bufinp1, 0, bufinp, 5, 61);
                        if( bufinp[63]==0x30  &&  bufinp[65]==0x0d) {
                            if( checkLongAnswer()  &&  checkCrc() )
                                return true;
                        }
                    }
                }
            }

            if( canLogError() )
                logError(trynum, false, bufout, bufout.length, bufinp, n, "pdvnum=" + pdvnum);

            delayAfterError();
        }
        tagErrorCnt.setInt( tagErrorCnt.getInt() + 1 );

        return false;
    }

    private boolean checkCrc() {
        int len = VALUES_OFFSET + T_COUNT * VALUE_SIZE;
        int crc = 0;
        for(int i=0; i < len; ++i)
            crc += bufinp[i];
        crc &= 0xFF;

        return crc == bufinp[len+1];
    }


    private boolean checkLongAnswer() {
        for(int i=0; i<T_COUNT; ++i) {
            int k = getPos(i);
            boolean res = bufinp[k]==0x2D  ||  isDigit(bufinp[k]);
            k++;
            res &= isDigit(bufinp[k++]);
            res &= isDigit(bufinp[k++]);
            res &= bufinp[k++] == 0x2E;
            res &= isDigit(bufinp[k]);
            if( !res )
                return false;
        }
        return true;
    }


    private boolean isDigit(int b) {
        return b >= 0x30  &&  b<=0x39;
    }


/*
    @Override
    protected boolean reload() {

        env.getCmdDispatcher().enableAddCommand(false);
        TermoMuxModule tmp = new TermoMuxModule(plugin, name);
        env.getCmdDispatcher().enableAddCommand(true);
        if( !tmp.load() )
            return false;

        copySettingsFrom(tmp);

        for(int i=0; i<PDV_COUNT; ++i) {
            pdvs[i].Rzero   = tmp.pdvs[i].Rzero;
            pdvs[i].Rwire   = tmp.pdvs[i].Rwire;
            pdvs[i].pdvType = tmp.pdvs[i].pdvType;
        }

        return true;
    }
*/

}
