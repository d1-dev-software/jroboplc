package promauto.jroboplc.plugin.peripherial;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import promauto.jroboplc.core.api.Flags;
import promauto.jroboplc.core.api.Plugin;
import promauto.jroboplc.core.api.Tag;
import promauto.jroboplc.core.tags.TagRW;

public class LsisSvIg5aModule extends PeripherialModule {
	private final Logger logger = LoggerFactory.getLogger(LsisSvIg5aModule.class);
	
	protected ProtocolModbus protocol = new ProtocolModbus(this);

    protected TagRW tagReg0004;
    protected TagRW tagReg0005;
    protected TagRW tagReg0006;
    protected TagRW tagReg0007;
    protected TagRW tagReg0008;


    protected Tag tagReg0009;
    protected Tag tagReg000A;
    protected Tag tagReg000B;
    protected Tag tagReg000C;
    protected Tag tagReg000D;
    protected Tag tagReg000E; 
    protected Tag tagReg000F;
    protected Tag tagReg0010; 
    protected Tag tagReg0011;
    protected Tag tagReg0012;
    protected Tag tagReg0013;
    
   
	
	public LsisSvIg5aModule(Plugin plugin, String name) {
		super(plugin, name);
	}


	@Override
	public boolean loadPeripherialModule(Object conf) {

		
		tagReg0004 = protocol.addWriteTag( 0x0004, tagtable.createRWInt("Reg.R0004", 0, Flags.STATUS));
		tagReg0005 = protocol.addWriteTag( 0x0005, tagtable.createRWInt("Reg.R0005", 0, Flags.STATUS));
		tagReg0006 = protocol.addWriteTag( 0x0006, tagtable.createRWInt("Reg.R0006", 0, Flags.STATUS));
		tagReg0007 = protocol.addWriteTag( 0x0007, tagtable.createRWInt("Reg.R0007", 0, Flags.STATUS));
		tagReg0008 = protocol.addWriteTag( 0x0008, tagtable.createRWInt("Reg.R0008", 0, Flags.STATUS));
		
		tagReg0009 = tagtable.createInt("Reg.R0009", 0, Flags.STATUS);
		tagReg000A = tagtable.createInt("Reg.R000A", 0, Flags.STATUS);
		tagReg000B = tagtable.createInt("Reg.R000B", 0, Flags.STATUS);
		tagReg000C = tagtable.createInt("Reg.R000C", 0, Flags.STATUS);
		tagReg000D = tagtable.createInt("Reg.R000D", 0, Flags.STATUS);
		tagReg000E = tagtable.createInt("Reg.R000E", 0, Flags.STATUS);
		tagReg000F = tagtable.createInt("Reg.R000F", 0, Flags.STATUS);
		tagReg0010 = tagtable.createInt("Reg.R0010", 0, Flags.STATUS);
		tagReg0011 = tagtable.createInt("Reg.R0011", 0, Flags.STATUS);
		tagReg0012 = tagtable.createInt("Reg.R0012", 0, Flags.STATUS);
		tagReg0013 = tagtable.createInt("Reg.R0013", 0, Flags.STATUS);
		
		return true;
	}


	
	@Override
	public boolean executePeripherialModule() {
		
		if(emulated) {
			
			tagReg0004.acceptWriteValue();
			tagReg0005.acceptWriteValue();
			tagReg0006.acceptWriteValue();
			tagReg0007.acceptWriteValue();
			tagReg0008.acceptWriteValue();
			return true;
		}
		
		boolean result = true;
		
		try {
			for(int i=0;i<5;i++){
				
				result = protocol.sendWriteTags(0x06);				
//				result = protocol.sendWriteTags(0x10);
				if(result){					
					break;
				}
				Thread.sleep(100);
			}			
			
			
			if( result )
				if( result = protocol.requestCmd3(0x0004, 8) ) {
					
					tagReg0004.setReadValInt( protocol.getAnswerWord(0) );
					tagReg0005.setReadValInt( protocol.getAnswerWord(1) );
					tagReg0006.setReadValInt( protocol.getAnswerWord(2) );
					tagReg0007.setReadValInt( protocol.getAnswerWord(3) );
					tagReg0008.setReadValInt( protocol.getAnswerWord(4) );
					tagReg0009.setInt( protocol.getAnswerWord(5) );
					tagReg000A.setInt( protocol.getAnswerWord(6) );
					tagReg000B.setInt( protocol.getAnswerWord(7) );
				}
				            
			if( result )
				if( result = protocol.requestCmd3(0x000C, 8) ) {					
					
					tagReg000C.setInt( protocol.getAnswerWord(0) );
					tagReg000D.setInt( protocol.getAnswerWord(1) );
					tagReg000E.setInt( protocol.getAnswerWord(2) );
					tagReg000F.setInt( protocol.getAnswerWord(3) );
					tagReg0010.setInt( protocol.getAnswerWord(4) );
					tagReg0011.setInt( protocol.getAnswerWord(5) );
					tagReg0012.setInt( protocol.getAnswerWord(6) );
					tagReg0013.setInt( protocol.getAnswerWord(7) );					
				}
			
		} catch (Exception e) {
			env.printError(logger, e, name);
			result = false;
		}
	
		return result;
	}


}
