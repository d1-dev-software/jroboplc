package promauto.jroboplc.plugin.peripherial;

import promauto.jroboplc.core.api.Flags;
import promauto.jroboplc.core.tags.*;
import promauto.utils.Numbers;

public class ModbusTag {

    public enum Type { BOOL, INT16, UINT16, INT32, UINT32, FLOAT16, FLOAT32, STRING }

    public enum Access { RO, WO, RW }

    public enum Region { COIL, DSCINP, HLDREG, INPREG }

    public String name = "";
    public String tracktagName = "";
    public Type type = Type.UINT16;
    public Region region = Region.HLDREG;
    public Access access = Access.RW;
    public boolean inverted = false;
    public int address = 0;
    public int size = 1;
    public boolean littleEndian = false;
    public boolean readEnd = false;
    public boolean readOnce = false;
    public boolean writeSingle = true;
    public boolean writeMultiple = true;
    public boolean enable = true;
    public boolean bitNameOnly = false;
    public int mul = 1;

    public TagRW tag;
    public ModbusTag tracktag;

    public boolean needWrite;

    public static ModbusTag create(String name, int address) {
        return new ModbusTag().name(name).address(address);
    }

    public ModbusTag name(String name) {
        this.name = name;
        return this;
    }

    public ModbusTag type(Type type) {
        this.type = type;
        return this;
    }

    public ModbusTag inverted(boolean inverted) {
        this.inverted = inverted;
        return this;
    }

    public ModbusTag region(Region region) {
        this.region = region;
        return this;
    }

    public ModbusTag access(Access access) {
        this.access = access;
        return this;
    }

    public ModbusTag address(int address) {
        this.address = address;
        return this;
    }

    public ModbusTag littleEndian(boolean littleEndian) {
        this.littleEndian = littleEndian;
        return this;
    }

    public ModbusTag readEnd(boolean readEnd) {
        this.readEnd = readEnd;
        return this;
    }

    public ModbusTag writeSingle(boolean writeSingle) {
        this.writeSingle = writeSingle;
        return this;
    }

    public ModbusTag writeMultiple(boolean writeMultiple) {
        this.writeMultiple = writeMultiple;
        return this;
    }

    public ModbusTag enable(boolean enable) {
        this.enable = enable;
        return this;
    }

    public ModbusTag size(int size) {
        this.size = size;
        return this;
    }

    public ModbusTag trackName(String tracktagName) {
        this.tracktagName = tracktagName;
        return this;
    }

    public boolean isWriteOnly() {
        return access == Access.WO;
    }


    public void init() {

        if( region == Region.DSCINP  ||  region == Region.INPREG  ||  type == Type.STRING  ) {
            access = Access.RO;
        }

        if( region == Region.COIL  ||  region == Region.DSCINP ) {
            type = Type.BOOL;
            size = 1;
        } else
            switch( type ) {
                case BOOL: case INT16: case UINT16: case FLOAT16:
                    size = 1; break;
                case INT32: case UINT32: case FLOAT32:
                    size = 2; break;
                case STRING:
                    size = Math.max(size, 1);
            }

        switch( type ) {
            case BOOL:
                tag = new TagRWBool(name, false, Flags.STATUS); break;
            case INT16: case UINT16: case INT32:
                tag = new TagRWInt(name, 0, Flags.STATUS); break;
            case UINT32:
                tag = new TagRWLong(name, 0, Flags.STATUS); break;
            case FLOAT16: case FLOAT32:
                tag = new TagRWDouble(name, 0.0, Flags.STATUS);	break;
            case STRING:
                tag = new TagRWString(name, "", Flags.STATUS);
        }

        if( mul == 0 )
            mul = 1;

    }

    public void putValueIntoBuff(int[] buff, int pos) {
        switch( type ) {
            case BOOL:
                buff[pos] = (tag.getWriteValBool() ^ inverted)? 1: 0;
                break;
            case INT16: case UINT16:
                buff[pos] = transformWrite(tag.getWriteValInt()) & 0xFFFF;
                break;
            case INT32: case UINT32: {
                long l = transformWrite(tag.getWriteValInt()) & 0xFFFF_FFFFL;
                if( littleEndian) {
                    buff[pos + 1] = (int) (l >> 16);
                    buff[pos] = (int) (l & 0xFFFF);
                } else {
                    buff[pos] = (int) (l >> 16);
                    buff[pos + 1] = (int) (l & 0xFFFF);
                }
                break;
            }
            case FLOAT16:
                buff[pos] = Numbers.encodeFloat16( (float)(transformWrite(tag.getWriteValDouble())) );
                break;
            case FLOAT32: {
                long l = Numbers.encodeFloat32((float)(transformWrite(tag.getWriteValDouble()))) & 0xFFFF_FFFFL;
                if( littleEndian ) {
                    buff[pos + 1] = (int) (l >> 16);
                    buff[pos] = (int) (l & 0xFFFF);
                } else {
                    buff[pos] = (int) (l >> 16);
                    buff[pos + 1] = (int) (l & 0xFFFF);
                }
                break;
            }
            case STRING:
                // not implemented
        }
    }


    public void fetchValueFromProtocolBuffin(ProtocolModbus protocol, int pos) {
        switch( type ) {
            case BOOL:
                tag.setReadValBool( (protocol.getAnswerWord(pos) != 0) ^ inverted );
                break;
            case INT16:
                tag.setReadValInt( (short)(transformRead(protocol.getAnswerWord(pos))) );
                break;
            case UINT16:
                tag.setReadValInt( transformRead(protocol.getAnswerWord(pos)) );
                break;
            case INT32:
                tag.setReadValInt( transformRead(protocol.getAnswerInt32(pos, littleEndian)) );
                break;
            case UINT32:
                tag.setReadValLong( transformRead(protocol.getAnswerInt32(pos, littleEndian) & 0xFFFF_FFFFL ));
                break;
            case FLOAT16:
                tag.setReadValDouble( transformRead(Numbers.decodeFloat16(protocol.getAnswerWord(pos))));
                break;
            case FLOAT32:
                tag.setReadValDouble( transformRead(Numbers.decodeFloat32(protocol.getAnswerInt32(pos, littleEndian))));
                break;
            case STRING:
                tag.setReadValString( protocol.getAnswerString(pos, size) );
        }
    }



    private int transformRead(int value) {
        return value * mul;
    }

    private long transformRead(long value) {
        return value * mul;
    }

    private double transformRead(double value) {
        return value * mul;
    }

    private int transformWrite(int value) {
        if( mul == 1 )
            return value;
        else
            return (int)Math.round( (double)value / mul );
    }

    private double transformWrite(double value) {
        return value / mul;
    }

}
