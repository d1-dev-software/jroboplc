package promauto.jroboplc.plugin.peripherial;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import promauto.jroboplc.core.api.Configuration;
import promauto.jroboplc.core.api.Flags;
import promauto.jroboplc.core.api.Plugin;
import promauto.jroboplc.core.api.Tag;
import promauto.jroboplc.core.tags.TagRW;
import promauto.utils.Numbers;

public class AkkontPassModule extends PeripherialModule {
	private final Logger logger = LoggerFactory.getLogger(AkkontPassModule.class);

	protected ProtocolAA55 protocol = new ProtocolAA55(this);

	protected Tag tagCrc;
	protected Tag tagSumWesHigh;
	protected Tag tagSumWesLow;
	protected Tag tagWNum;
	protected Tag tagLastWes;
	protected Tag tagCurWes;
	protected Tag tagStageNum;
	protected Tag tagErrCode;
	protected Tag tagWCycle;
	protected Tag tagReqOutput;
	protected Tag tagReqDose;
	protected Tag tagRun;
	protected Tag tagCmdStop;
	protected Tag tagCmdStart;
	protected TagRW tagCmdSetOutput;
	protected TagRW tagCmdSetDose;

	protected Tag tagOutput;
	protected Tag tagOutputKg;
	protected TagRW tagWesSvrState;

	protected Tag[] crcTags;

	private int[] buffin = new int[64];
	private int[] buffout = new int[16];

	private long wesSvrStateTimer;

	private long outWeight;
	private long outTime;
	private boolean outFirstPass;
//	private int version;

	/*
	This is simplified edition for Akkont with version 1 only.
	 */


	public AkkontPassModule(Plugin plugin, String name) {
		super(plugin, name);
	}

	
	@Override
	public boolean loadPeripherialModule(Object conf) {
		Configuration cm = env.getConfiguration();

//		version = cm.get(conf, "version", 1);

		tagCrc 			= tagtable.createInt("Crc", 			0);
		tagSumWesHigh 	= tagtable.createInt("SumWesHigh", 	0, Flags.STATUS);
		tagSumWesLow 	= tagtable.createInt("SumWesLow", 		0, Flags.STATUS);
		tagWNum 		= tagtable.createInt("WNum", 			0, Flags.STATUS);
		tagLastWes 		= tagtable.createInt("LastWes", 		0, Flags.STATUS);
		tagCurWes 		= tagtable.createInt("CurWes", 		0, Flags.STATUS);
		tagStageNum 	= tagtable.createInt("StageNum", 		0, Flags.STATUS);
		tagErrCode 		= tagtable.createInt("ErrCode", 		0, Flags.STATUS);
		tagWCycle 		= tagtable.createInt("WCycle", 		0, Flags.STATUS);
		tagReqOutput 	= tagtable.createInt("ReqOutput", 		0, Flags.STATUS);
		tagReqDose 		= tagtable.createInt("ReqDose", 			0, Flags.STATUS);
		tagRun			= tagtable.createInt("Run", 				0, Flags.STATUS);
		tagCmdStop 		= tagtable.createInt("CmdStop", 		0, Flags.STATUS);
		tagCmdStart 	= tagtable.createInt("CmdStart", 		0, Flags.STATUS);
		tagCmdSetOutput = tagtable.createRWInt("CmdSetOutput", 	0, Flags.STATUS);
		tagCmdSetDose 	= tagtable.createRWInt("CmdSetDose", 	0, Flags.STATUS);

		crcTags = new Tag[] {
				tagSumWesHigh,
				tagSumWesLow,
				tagWNum,
				tagLastWes
		};

		initCrc16Tags();

		tagOutput			= tagtable.createInt("Output", 0, Flags.STATUS);
		tagOutputKg			= tagtable.createInt("OutputKg", 0, Flags.STATUS);
		tagWesSvrState		= tagtable.createRWInt("WesSvrState", 0);
		
		return true;
	}


	@Override
	protected void initCrc16Tags() {
		super.initCrc16Tags();
		for(Tag tag: crcTags)
			crc16Tags.add( tag );
		crc16Tags.add( tagError );
	}


	@Override
	public boolean preparePeripherialModule() {
	    outWeight = 0;
	    outTime = 0;
	    outFirstPass = true;
		return true;
	}

	public void processWrite(int cmd, TagRW tag) throws Exception {
		if( tag.hasWriteValue() ) {
			int value = tag.getWriteValInt();
			writeValue(cmd, value);
			tag.setReadValInt(value);
		}
	}

	public void writeValue(int cmd, int value) throws Exception {
		System.out.println(String.format("write %d %d", cmd, value));
		buffout[0] = 0x55;
		buffout[1] = 0x70 + netaddr;
		buffout[2] = cmd;
		buffout[3] = (value >> 8) & 0xff;
		buffout[4] = value & 0xff;
		buffout[5] = 0;
		protocol.setCrcAA55(buffout, 7);
		protocol.writeBytes(buffout, 7);
	}

	@Override
	public boolean executePeripherialModule() {
		
		boolean result = true;

		if( emulated ) {
			calcTagCrc8(crcTags, tagCrc, buffin);
		} else {
			try {
				buffout[0] = 0x55;

//				processWrite(0xA0, tagCmdStop);
				if( tagCmdStop.getInt() > 0  &&  tagRun.getInt() > 0 ) {
					writeValue(0xA0, 1);
				}

//				processWrite(0xA1, tagCmdStart);
				if( tagCmdStart.getInt() > 0  &&  tagCmdStop.getInt() == 0  &&  tagRun.getInt() == 0 ) {
					writeValue(0xA1, 1);
				}

				processWrite(0xA2, tagCmdSetOutput);
				processWrite(0xA3, tagCmdSetDose);

				if( result ) {
					buffout[1] = 0x10 + netaddr;
					result = protocol.request(buffout, 2, buffin, 10, ProtocolAA55.CrcAA55Word);
				}

				if( result ) {
					tagSumWesHigh.setInt(Numbers.bytesToWord(buffin, 0));
					tagSumWesLow.setInt(Numbers.bytesToWord(buffin, 2));
					tagWNum.setInt(Numbers.bytesToWord(buffin, 4));
					tagLastWes.setInt(Numbers.bytesToWord(buffin, 6));
					calcTagCrc8(crcTags, tagCrc, buffin);
				}


				if( result ) {
					buffout[1] = 0x30 + netaddr;
					result = protocol.request(buffout, 2, buffin, 6, ProtocolAA55.CrcAA55Word);
				}

				if( result ) {
					tagCurWes.setInt(Numbers.bytesToWord(buffin, 0));
					tagStageNum.setInt(buffin[3]);
					tagErrCode.setInt(buffin[2]);

					tagRun.setBool( (buffin[2] & 0x80) > 0 );
				}


				if( result ) {
					buffout[1] = 0x50 + netaddr;
					result = protocol.request(buffout, 2, buffin, 8, ProtocolAA55.CrcAA55Word);
				}

				if( result ) {
					tagWCycle.setInt(Numbers.bytesToWord(buffin, 0));
					tagReqOutput.setInt(Numbers.bytesToWord(buffin, 2));
					tagReqDose.setInt(Numbers.bytesToWord(buffin, 4));
				}

			} catch (Exception e) {
				env.printError(logger, e, name);
			}
		}

		calcWeightOutput();

		wesSvrStateTimer = updateWesSvrState(tagWesSvrState, wesSvrStateTimer);
	
		return result;
	}
	

	
	
	private void calcWeightOutput() {
		int result = 0;
		long W = Numbers.wordsToLong(tagSumWesHigh.getInt(), tagSumWesLow.getInt());
		long L = tagLastWes.getLong();
		long T = tagWCycle.getLong() * 100;

		long ms = System.currentTimeMillis();

		if (outFirstPass) {
			outFirstPass = false;
			outWeight = W;
			outTime = ms - T * 3;
			result = 0;
		} else

		if (T == 0 || L == 0) {
			outWeight = W;
			outTime = ms;
			result = 0;
		} else {

			long tm = ms - outTime;

			if (outWeight != W || tm < T * 3) {

				double d = ((double)L / (double)T) * 3600 * 1000;
				result = (int) Math.round(d);

				if (outWeight != W) {
					outWeight = W;
					outTime = ms;
				}
			} else {
				if (tm > T * 3) {
					outTime = ms - T * 3;
				}

				result = 0;
			}
		}
		
		tagOutput.setInt( result );
		tagOutputKg.setInt( result/1000 );
	}
	
	
	
//	@Override
//	protected boolean reload() {
//
//		env.getCmdDispatcher().enableAddCommand(false);
//		AkkontPassModule tmp = new AkkontPassModule(plugin, name);
//		env.getCmdDispatcher().enableAddCommand(true);
//		if( !tmp.load() )
//			return false;
//
//		copySettingsFrom(tmp);
//
//		return true;
//	}
	
	



}

