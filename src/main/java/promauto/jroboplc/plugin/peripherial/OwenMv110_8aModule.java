package promauto.jroboplc.plugin.peripherial;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import promauto.jroboplc.core.api.Configuration;
import promauto.jroboplc.core.api.Flags;
import promauto.jroboplc.core.api.Plugin;
import promauto.jroboplc.core.api.Tag;
import promauto.utils.Numbers;

import java.util.List;


public class OwenMv110_8aModule extends PeripherialModule {
    private final Logger logger = LoggerFactory.getLogger(OwenMv110_8aModule.class);

    protected ProtocolModbus protocol = new ProtocolModbus(this);

    protected final static class Input {
        Tag val_int;
        Tag val_double;
        Tag dp;
        Tag time;
        Tag status;

    }

    protected Input[] tagInp = null;

    protected final int sizeInp;
    protected final int regInp;


    public OwenMv110_8aModule(Plugin plugin, String name) {
        super(plugin, name);

        sizeInp = 8;
        regInp = 0;
    }


    @Override
    public boolean loadPeripherialModule(Object conf) {
        Configuration cm = env.getConfiguration();

        if( tagInp == null )
            tagInp = new Input[sizeInp];

        for(int i = 0; i< sizeInp; ++i) {
            Input inp = new Input();

            String tagname = String.format("inp%02d", i);

            inp.dp          = tagtable.createInt(    tagname + ".dp", 0, Flags.STATUS);
            inp.val_int     = tagtable.createInt(    tagname + ".val.int", 0, Flags.STATUS);
            inp.status      = tagtable.createInt(    tagname + ".status", 0, Flags.STATUS);
            inp.time        = tagtable.createInt(    tagname + ".time", 0, Flags.STATUS);
            inp.val_double  = tagtable.createDouble( tagname + ".val.double", 0, Flags.STATUS);

            tagInp[i] = inp;
        }
        return true;
    }

    @Override
    protected void initChannelMap(List<String> chtags) {
        for( int i=0; i<sizeInp; ++i ) {
            addChannelMapTag(chtags, tagInp[i].val_int, "" + i);
        }
    }


    @Override
    public boolean executePeripherialModule() {

        if(emulated) {
            return true;
        }

        boolean result = true;

        try {
            result &= readInputs();

        } catch (Exception e) {
            env.printError(logger, e, name);
            result = false;
        }

        return result;
    }


    protected boolean readInputs() throws Exception {
        if( !protocol.requestCmd3(regInp, sizeInp * 6) )
            return false;

        for (int i = 0; i < sizeInp; ++i) {
            tagInp[i].dp.setInt(        protocol.getAnswerWord(i * 6 + 0));
            tagInp[i].val_int.setInt(   protocol.getAnswerWord(i * 6 + 1));
            tagInp[i].status.setInt(    protocol.getAnswerWord(i * 6 + 2));
            tagInp[i].time.setInt(      protocol.getAnswerWord(i * 6 + 3));
            tagInp[i].val_double.setDouble(protocol.getAnswerFloat(i * 6 + 4));
        }

        return true;
    }


}
