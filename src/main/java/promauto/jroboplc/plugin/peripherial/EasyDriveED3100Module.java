package promauto.jroboplc.plugin.peripherial;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import promauto.jroboplc.core.api.Flags;
import promauto.jroboplc.core.api.Plugin;
import promauto.jroboplc.core.api.Tag;
import promauto.utils.Numbers;

public class EasyDriveED3100Module extends PeripherialModule {
	private final Logger logger = LoggerFactory.getLogger(EasyDriveED3100Module.class);

	protected ProtocolModbus protocol = new ProtocolModbus(this);

    protected Tag tagFreqSet;
	protected Tag tagFreqReq;
	protected Tag tagFreqOut;


	public EasyDriveED3100Module(Plugin plugin, String name) {
		super(plugin, name);
	}


	@Override
	public boolean loadPeripherialModule(Object conf) {
		tagFreqSet = tagtable.createInt("FreqSet", 0, Flags.STATUS);
		tagFreqReq = tagtable.createInt("FreqReq", 0, Flags.STATUS);
		tagFreqOut = tagtable.createInt("FreqOut", 0, Flags.STATUS);
		return true;
	}

	
	@Override
	public boolean executePeripherialModule() {
		
		if(emulated) {
			return true;
		}
		
		boolean result = true;
		try {

			if( tagFreqReq.getInt() != tagFreqSet.getInt() ) {
				protocol.requestEasyDriveCmd10(0x2001, tagFreqSet.getInt());
			}

			if( result ) {
				result = protocol.requestEasyDriveCmd3(0x0d01);
				if (result) {
					tagFreqReq.setInt(Numbers.bytesToWord( protocol.buffin, 4));
				}
			}
			if( result ) {
				result = protocol.requestEasyDriveCmd3(0x0d00);
				if (result) {
					tagFreqOut.setInt(Numbers.bytesToWord( protocol.buffin, 4));
				}
			}

		} catch (Exception e) {
			env.printError(logger, e, name);
			result = false;
		}

		return result;
	}
}
