package promauto.jroboplc.plugin.peripherial;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import promauto.jroboplc.core.api.Flags;
import promauto.jroboplc.core.api.Plugin;
import promauto.jroboplc.core.api.Tag;
import promauto.utils.Numbers;

import java.util.List;

public class I7024Module extends PeripherialModule {
	private final Logger logger = LoggerFactory.getLogger(I7024Module.class);

	protected ProtocolIcpcon protocol = new ProtocolIcpcon(this);

	private static final int OUT_SIZE = 4;

	protected static class Output {
		public Tag value;
		public Tag valsp;
		public boolean initiated;
	}

	protected Output[] outputs = new Output[OUT_SIZE];
	
	private int[] buffout = new int[16]; //11];
	private int[] buffin = new int[24]; //13];

	
	public I7024Module(Plugin plugin, String name) {
		super(plugin, name);
	}

	
	@Override
	public boolean loadPeripherialModule(Object conf) {

		initTags();

		return true;
	}


	protected void initTags() {
		for (int i = 0; i < OUT_SIZE; i++)
		{
			String out = "out" + i + ".";
			outputs[i] = new Output();
			outputs[i].value = tagtable.createInt( out + "value", 0, Flags.STATUS);
			outputs[i].valsp = tagtable.createInt( out + "valsp", 0, Flags.STATUS);
			outputs[i].initiated = false;
		}
	}


	@Override
	protected void initChannelMap(List<String> chtags) {
		for( int i=0; i<OUT_SIZE; ++i ) {
			addChannelMapTag(chtags, outputs[i].value, "" + i);
			addChannelMapTag(chtags, outputs[i].valsp, "sp" + i);
		}
	}


	@Override
	public boolean executePeripherialModule() {
		
		if(emulated) {
			for(int i=0; i<OUT_SIZE; ++i)
			{
				Output output = outputs[i];
				output.value.setInt( output.valsp.getInt() );
			}			
			return true;
		}
		
		boolean result = true;
		try {
			
			for(int i=0; i<OUT_SIZE; ++i)
			{
				Output output = outputs[i];
				if( output.initiated  &&  output.value.getInt() != output.valsp.getInt() ) 
				{
					// write
					// #020+05.00003
					// >3E
					
					buffout[0] = '#';
					Numbers.intToHexByte(netaddr, buffout, 1);
					buffout[3] = Numbers.hexDigit[i];
					buffout[4] = '+';
					
					int v = output.valsp.getInt();
					v %= 100000;
					buffout[5] = Numbers.hexDigit[ v/10000 ];
					
					v %= 10000;
					buffout[6] = Numbers.hexDigit[ v/1000 ];
					buffout[7] = '.';
					
					v %= 1000;
					buffout[8] = Numbers.hexDigit[ v/100 ];
					
					v %= 100;
					buffout[9] = Numbers.hexDigit[ v/10 ];
					
					v %= 10;
					buffout[10] = Numbers.hexDigit[ v ];
					
					result = protocol.request(buffout, 11, buffin, 4) == 4;
				}
				
				// read
				// $0280EE
				// !02+05.000D1
				buffout[0] = '$';
				Numbers.intToHexByte(netaddr, buffout, 1);
				buffout[3] = '8';
				buffout[4] = Numbers.hexDigit[i];

				
				if(result)
					result = protocol.request(buffout, 5, buffin, 13) == 13;
				
				if( result )
				{
					int value = 
							Numbers.asciiDigitToInt(buffin[4]) * 10000 +
							Numbers.asciiDigitToInt(buffin[5]) * 1000 +
							Numbers.asciiDigitToInt(buffin[7]) * 100 +
							Numbers.asciiDigitToInt(buffin[8]) * 10 +
							Numbers.asciiDigitToInt(buffin[9]);
					if( buffin[3] == '-' )
						value = -value;
					
					if( output.initiated  &&  Math.abs(output.valsp.getInt() - value)==1 )
						value = output.valsp.getInt();

					output.value.setInt(value);
					
					if( !output.initiated ) {
						output.valsp.setDouble(value);
						output.initiated = true;
					}
				}
				else
					break;
			}
			
		} catch (Exception e) {
			env.printError(logger, e, name);
		}
		
		return result;
	}

	


}
