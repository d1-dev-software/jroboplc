package promauto.jroboplc.plugin.peripherial;

import java.io.BufferedReader;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.function.BooleanSupplier;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import promauto.jroboplc.core.api.Configuration;
import promauto.jroboplc.core.api.Flags;
import promauto.jroboplc.core.api.Plugin;
import promauto.jroboplc.core.api.Tag;
import promauto.jroboplc.core.tags.TagInt;
import promauto.jroboplc.core.tags.TagRW;
import promauto.jroboplc.core.tags.TagRWInt;
import promauto.utils.CRC;
import promauto.utils.Numbers;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import static javax.xml.stream.XMLStreamConstants.START_ELEMENT;

public class PaRoboModule extends PeripherialModule {

	private final Logger logger = LoggerFactory.getLogger(PaRoboModule.class);
	

	private static final int PACK_SIZE = 400;
	
	enum ExtType {
		Periodic,
		Requested,
		Irregular
	}
	
	public static class ExtTag {
		public TagRW 	tag;
		public int 		index;
		public int 		addr;
		public int 		num;
		public ExtType 	exttype;
		public boolean  readonly;
	}
	
	protected ExtTag[] exttags;

	protected Tag tagTimeWrite;
	protected Tag tagTimeReadIrr;
	protected Tag tagTimeReadAll;
	protected Tag tagTimeReadPer;
	protected Tag tagTimeReadReq;
	protected Tag tagTimeReadTCycle;
	protected Tag tagTimeCycle;
	protected Tag tagErrorCode;
	
	protected int sizeTotal;
	protected int sizePer;
	protected int sizeReq;
	protected int sizeIrr;
	protected String progid;

	protected int indexIrrBegin;
	protected int packAmountTotal;
	protected int packAmountPer;

	private long t1;
	private long t2;
	private int readIndex;
    private boolean needToReadAll;

	private int[] buffin = new int[2048];
	private int[] buffout = new int[14];
	private int sizein;

	

	
	
	public PaRoboModule(Plugin plugin, String name) {
		super(plugin, name);
	}


	@Override
	public boolean loadPeripherialModule(Object conf) {
		tagTimeWrite 	= tagtable.createInt(systagpref + "TimeWrite", 0);
		tagTimeReadIrr  = tagtable.createInt(systagpref + "TimeReadIrr", 0);
		tagTimeReadAll  = tagtable.createInt(systagpref + "TimeReadAll", 0);
		tagTimeReadPer  = tagtable.createInt(systagpref + "TimeReadPer", 0);
		tagTimeReadReq  = tagtable.createInt(systagpref + "TimeReadReq", 0);
		tagTimeReadTCycle  = tagtable.createInt(systagpref + "TimeReadTCycle", 0);
		tagTimeCycle    = tagtable.createInt(systagpref + "TimeCycle", 0);
		tagErrorCode 	= tagtable.createInt(systagpref + "ErrorCode", 0);
		
		
		Configuration cm = env.getConfiguration();
		Path pathMtr = cm.getPath(conf, "mtr", "");
		String s = cm.get(conf, "mtr", "");
		String rpp = s.substring(0, s.length() - 3) + "rpp";
		Path pathRpp = cm.getPath(conf, "rpp", rpp);

		boolean useDescr;
		boolean hiddenDescr = false;
		s = cm.get(conf, "use.descr", "");
		if( s.equals("hidden") ) {
			useDescr = hiddenDescr = true;
		} else {
			useDescr = Boolean.parseBoolean(s);
		}


		String charsetname = cm.get(conf, "encoding", "windows-1251"); // or ISO-8859-1
		/*
		 *  more supported encodings are here:  
		 *  https://docs.oracle.com/javase/8/docs/technotes/guides/intl/encoding.doc.html
		 *  
		 */
		
		sizeTotal = sizePer = sizeReq = sizeIrr = indexIrrBegin = 0;
		boolean res = loadMtr(pathMtr, charsetname);

		if( useDescr)
			res &= loadDescr(pathRpp, hiddenDescr);

		
		packAmountTotal = ((sizeTotal - 1) / PACK_SIZE) + 1; 
		packAmountPer = ((sizePer - 1) / PACK_SIZE) + 1; 

		return res;
	}

	private boolean loadDescr(Path pathRpp, boolean hidden) {
		if( !Files.exists(pathRpp) )
			return true;

		XMLInputFactory xmlif = env.getXMLInputFactory();
		XMLStreamReader xmlr = null;

		Set<String> devTypesWithDescr = new HashSet<>(Arrays.asList(
				"MCHB","MDTA","ZDVA","ZDVB","ZDVH","ZDVS","ZDVT","SEQD"));

		int flags = hidden? Flags.HIDDEN: Flags.NONE;
		boolean res = false;
		try (InputStream prjstream = Files.newInputStream(pathRpp)) {
			xmlr = xmlif.createXMLStreamReader(prjstream);
			while (xmlr.hasNext()) {
				if( xmlr.next() == START_ELEMENT  &&  xmlr.getName().getLocalPart().equals("dev") ) {
					String devtype = xmlr.getAttributeValue("", "DevType");
					String tagname = xmlr.getAttributeValue("", "TagName");
					String name = xmlr.getAttributeValue("", "Name");
					if( devTypesWithDescr.contains(devtype) )
						tagtable.createRWString(String.format("%s_%s.descr", devtype, tagname), name, flags);
				}
			}
			res = true;
		} catch (Exception e) {
			env.printError(logger, e, name, pathRpp.toString(),
					xmlr==null? "": "line: " + xmlr.getLocation().getLineNumber() );
		} finally {
			try {
				xmlr.close();
			} catch (XMLStreamException e) {
				env.printError(logger, e, name);
			}
		}

		return res;
	}


	private boolean loadMtr(Path pathMtr, String charsetname) {
		try (BufferedReader reader = Files.newBufferedReader(pathMtr, Charset.forName(charsetname))) {
			exttags = null;
			int exttagcnt = 0;
			String line;
			Pattern p1 = Pattern.compile("%(\\w+)=(\\w+)");
			Pattern p2 = Pattern.compile("@(\\w+):(\\w+)\\.(\\w+) (..) - - 00\\.(.+)$");
			Matcher m;
			while( (line = reader.readLine()) != null ) {
				if( exttags == null  &&  line.startsWith("%")  &&  (m = p1.matcher(line)).find() ) {
					switch( m.group(1) ) {
						case "EXTERNAL":	sizeTotal 	= Integer.parseInt( m.group(2), 16); break;
						case "EXTMAIN": 	sizePer 	= Integer.parseInt( m.group(2), 16); break;
						case "EXTREQ": 		sizeReq 	= Integer.parseInt( m.group(2), 16); break;
						case "EXTIRR": 		sizeIrr 	= Integer.parseInt( m.group(2), 16); break;
						case "PROGID": 		progid 		= m.group(2); break;
					}
				} else

				if( line.startsWith("@")  &&  (m = p2.matcher(line)).find() ) {
					// do once before first tag
					if( exttags == null ) {
						exttags = new ExtTag[sizeTotal];
						indexIrrBegin = sizePer + sizeReq;
					}

					// create exttag
					ExtTag exttag = new ExtTag();
					int index = Integer.parseInt( m.group(1), 16);

					exttag.addr = Integer.parseInt( m.group(2), 16);
					exttag.num = Integer.parseInt( m.group(3), 16);
					exttag.readonly = m.group(4).equals("RO");

					exttag.tag = tagtable.createRWInt(m.group(5), 0, Flags.STATUS);

					if( index < sizePer )
						exttag.exttype = ExtType.Periodic;
					else if( index < indexIrrBegin )
						exttag.exttype = ExtType.Requested;
					else {
						exttag.exttype = ExtType.Irregular;
						exttag.tag.setObject(new TagInt("IRR", 0));
					}

					if( index < 0  ||  index >= sizeTotal ) {
						env.printError(logger, name, "Tag index out of bounds:", line);
						return false;
					}
					if( exttags[ index ] != null ) {
						env.printError(logger, name, "Tag index duplicate:", line );
						return false;
					}
					exttags[ index ] = exttag;
					exttagcnt++;
				}
			}

			if( exttagcnt != sizeTotal ) {
				env.printError(logger, name, "Bad tag amount:",  exttagcnt + " != " + sizeTotal);
				return false;
			}
		} catch (Exception e) {
			env.printError(logger, e, name);
			return false;
		}
		return true;
	}


	@Override
	public boolean preparePeripherialModule() {
		needToReadAll = true;
		return true;
	}

	


	@Override
	public boolean executePeripherialModule() {
		
		if( emulated ) {
			executeEmulate();
			return true;
		}
		
		int res = 0;
		try {
			t1 = System.currentTimeMillis();
			
			res |= writeTags()? 0: 1;
			fixTime( tagTimeWrite );
			
			res |= readIrregulars()? 0: 2;
			fixTime( tagTimeReadIrr );
			
			if( needToReadAll ) {
				res |= readAll()? 0: 4;
				fixTime( tagTimeReadAll );
				
				if( readIndex != sizeTotal )
					res |= 8;
			} else {
				res |= readPeriodic()? 0: 16;
				fixTime( tagTimeReadPer );
				
				res |= readRequested()? 0: 32;
				fixTime( tagTimeReadReq );
			}
			
			res |= readTimeCycle()? 0: 64;
			fixTime( tagTimeReadTCycle );
			
		} catch (Exception e) {
			env.printError(logger, e, name);
			res |= 128;
		}
		
		if( res > 0) {
			tagErrorCnt.setInt( tagErrorCnt.getInt() + 1 );
			if( canLogError() ) 
        		logError("executePeripherialModule, Err1: " + res);
		}

		tagErrorCode.setInt(res);
		return res == 0;
	}

	
	private void fixTime(Tag tagTime) {
		t2 = System.currentTimeMillis();
		tagTime.setInt((int)(t2-t1));
		t1 = t2;
	}


	private boolean writeTags() throws Exception {

		for(ExtTag exttag: exttags) {
			if( !exttag.readonly  &&  exttag.tag.hasWriteValue() ) {
				
//				logger.debug("[writeTags] " + name + " " + exttag.tag.getName() + ": " +
//							exttag.tag + " -> " + exttag.tag.getWriteValInt() );
				
				buffout[0] = '@';
				Numbers.intToHexWord(exttag.addr, buffout, 1);
				Numbers.intToHexByte(exttag.num, buffout, 5);
				Numbers.intToHexWord(exttag.tag.getWriteValInt(), buffout, 7);
				Numbers.intToHexByte( CRC.getCrc8(buffout, 0, 10), buffout, 11);
				buffout[13] = 13;

				if( !request(14, 
						() -> { 
							if( sizein != 15  ||  buffin[0] != '!' )
								return false;
							
							for(int i=0; i<13; ++i)
								if( buffin[i+1] != buffout[i] )
									return false;
							
							return true;
						}) )
				{
					exttag.tag.raiseWriteValue();
					return false;
				}

				if( exttag.exttype == ExtType.Irregular ) {
					Tag tag = exttag.tag.getObject();
					if( tag != null )
						tag.setInt(1);
				}
			}
		}
		return true;
	}


	private boolean readIrregulars() throws Exception {
		for(int i = indexIrrBegin; i < sizeTotal; ++i) {
			if( exttags[i].tag.getObject() == null  ||  ((Tag)exttags[i].tag.getObject()).getInt() == 0 )
				continue;
			
			ExtTag exttag = exttags[i];
				
			buffout[0] = '$';
			Numbers.intToHexWord(exttag.addr, buffout, 1);
			Numbers.intToHexByte(exttag.num, buffout, 5);
			Numbers.intToHexByte( CRC.getCrc8(buffout, 0, 6), buffout, 7);
			buffout[9] = 13;

			if( !request(10, () -> buffin[0] == '!'  &&  checkCrc8('=') ) )	
				return false;

			int k = getPos('=', 1, sizein);
			if( k == -1 )
				return false;
			
			int value = Numbers.hexToInt(buffin, 1, k-1);
			exttag.tag.setReadValInt(value);
			((Tag)exttags[i].tag.getObject()).setInt(0);
		}
		return true;
	}


	private boolean readAll() throws Exception {
		if( !readContiguous('%', packAmountTotal) )
			return false;

		Thread.sleep(100);

		buffout[0] = ';';
		buffout[1] = 'R';
		buffout[2] = 'Q';
		buffout[3] = 'E';
		buffout[4] = 'N';
		buffout[5] = 13;
		if( !port.writeBytes(buffout, 6) )
			return false;
		
		Thread.sleep(100);
		
		needToReadAll = false;
		return true;
	}


	private boolean readPeriodic() throws Exception {
		return readContiguous('+', packAmountPer);
	}
	
	private boolean readContiguous(char cmd, int packAmount) throws Exception {
		int i;
		for(int packnum=0; packnum < packAmount; ++packnum) {
			
			buffout[0] = cmd;
			i = Numbers.intToHex(packnum, buffout, 1);
			buffout[i++] = 13;
			
			final int packnum_fin = packnum;
			if( !request(i, () -> (checkCrc16('|', sizein-2)  &&  parseContiguousValues(packnum_fin)) ) ) {
				if( canLogError() ) 
	        		logError("readContiguous, Err2");
				return false;
			}
			
//			if( !parseContiguousValues(packnum) ) {
//				System.out.println(name + " readPeriodic: e2");
//				return false;
//			}
			
			Thread.sleep(10);
		}
		return true;
	}

	
	
	private boolean parseContiguousValues(int packnum) {

		int k = getPos('>', 0, sizein);
		if( k == -1 ) {
			if( canLogError() ) 
        		logError("parseContiguousValues, Err3");

			return false;
		}
		
		int packnumTheirs = Numbers.hexToInt(buffin, 0, k);
		if( packnum != packnumTheirs ) {
			if( canLogError() ) 
        		logError("parseContiguousValues, Err4: parseContiguousValues: " + packnum + "!=" + packnumTheirs);
			return false;
		}
		
		int b;
		readIndex = packnum * PACK_SIZE;
		int value = 0;
		while( true ) {
			b = buffin[++k];
			if( b == ';' ) {
				if( readIndex >= sizeTotal ) {
					readIndex++;
					break;
				}
				exttags[readIndex++].tag.setReadValInt(value);
				value = 0;
			} 
			else if( b == '|' ) 
				break;
			else
				value = (value << 4) + Numbers.asciiDigitToInt(b);
		}
		return true;
	}

	
	private boolean readRequested() throws Exception {
		buffout[0] = ':';
		buffout[1] = 13;
		if( !request(2, () -> buffin[0] == ':'  ||  checkCrc16('|', sizein-2)) ) {
			if( canLogError() ) 
        		logError("readRequested, Err5");
			return false;
		}

		if(buffin[0] == ':') {
			if( buffin[1] == 'N' )
				return true;
			needToReadAll = true;
			return readAll();
		}

		int b;
		int k = -1;
		readIndex = -1;
		int value = 0;
		while( true ) {
			b = buffin[++k];
			if( b == ':' ) {
				readIndex = value + sizePer;
				value = 0;
			} else
				
			if( b == ';' ) {
				if( readIndex < 0  ||  readIndex >= sizeTotal ) {
					if( canLogError() ) 
		        		logError("readRequested, Err6: index=" + readIndex + ", sizeTotal=" + sizeTotal);
					return false;
				}

				exttags[readIndex].tag.setReadValInt(value);
				readIndex = -1;
				value = 0;
			} else
					
			if( b == '|' ) 
				break;
			else
				value = (value << 4) + Numbers.asciiDigitToInt(b);
		}
		
		buffout[0] = ';';
		buffout[1] = 'O';
		buffout[2] = 'K';
		buffout[3] = 13;
		if( !request(4, () -> sizein == 1) ) {
			if( canLogError() ) 
        		logError("readRequested, Err7");
			return false;
		}
		
		Thread.sleep(50);
		
		return true;
	}


	private boolean readTimeCycle() throws Exception {
		buffout[0] = '*';
		buffout[1] = 13;
		if( !request(2, () -> buffin[0] == '!') ) {
			if( canLogError() ) 
        		logError("readTimeCycle, Err8");
			return false;
		}

		int value = Numbers.hexToInt(buffin, 1, sizein-2);
		tagTimeCycle.setInt( value );
		
		return true;
	}

	
	private boolean request(int sizeout, BooleanSupplier fnCheck) throws Exception {
		for (int trynum = 0; trynum < retrial; trynum++) {

			if( !port.writeBytes(buffout, sizeout)) {
				if( canLogError() ) 
	        		logError("request, Err9");
				return false;
			}
			
			sizein = port.readBytesDelim(buffin, 13);
			
			if( sizein > 0  &&  fnCheck.getAsBoolean() )
				return true;

			Thread.sleep(50);

			if( canLogError() ) { 
        		String s = "\r\nout:";
				if( sizeout > 0)
					for( int i=0; i<sizeout; i++)
						s += (char)(buffout[i]);
				s += "\r\ninp:";
				for( int i=0; i<Math.abs(sizein); i++)
					s += (char)(buffin[i]);
        		logError("request, Err10: try=" + trynum + s);
			}

		}
		return false;
	}

	
	

	private boolean checkCrc8(char delim) {
		int last = sizein - 2;
		int k = getPosReverse(delim, last);
		if( k == -1 )
			return false;
		
		int crcTheirs = Numbers.hexToInt(buffin, k+1, last-k);
		int crcOurs = CRC.getCrc8(buffin, 0, k);
		return crcTheirs == crcOurs;
	}

	
	private boolean checkCrc16(char delim, int last) {
		int k = getPosReverse(delim, last);
		if( k == -1 )
			return false;
		
		int crcTheirs = Numbers.hexToInt(buffin, k+1, last-k);
		int crcOurs = CRC.getCrc16(buffin, k);
		return crcTheirs == crcOurs;
	}
	

	private int getPos(int ch, int pos, int size) {
		while( pos < size )
			if( buffin[pos] == ch)
				return pos;
			else
				pos++;
		return -1;
	}

	private int getPosReverse(int ch, int pos) {
		while( pos >= 0 )
			if( buffin[pos] == ch)
				return pos;
			else
				pos--;
		return -1;
	}




	

	private void executeEmulate() {
		for(ExtTag exttag: exttags)
			exttag.tag.acceptWriteValue();
//			if( exttag.tag.hasWriteValue() )
//				exttag.tag.setReadValue( exttag.tag.getWriteValueInt() );
	}
	
	
	@Override
	public String getInfo() {
		return super.getInfo() + (enable? " progid:" + progid: ""); 
	}



	@Override
	protected boolean reload() {
		PaRoboModule tmp = new PaRoboModule(plugin, name);
		if( !tmp.load() )
			return false;

		copySettingsFrom(tmp);
		

		tagTimeWrite	.setInt( tmp.tagTimeWrite   .getInt() );
		tagTimeReadIrr	.setInt( tmp.tagTimeReadIrr .getInt() );
		tagTimeReadAll	.setInt( tmp.tagTimeReadAll .getInt() );
		tagTimeReadPer	.setInt( tmp.tagTimeReadPer .getInt() );
		tagTimeReadReq	.setInt( tmp.tagTimeReadReq .getInt() );
		tagTimeReadTCycle .setInt( tmp.tagTimeReadTCycle .getInt() );
		tagTimeCycle	.setInt( tmp.tagTimeCycle	.getInt() );
		tagErrorCode	.setInt( tmp.tagErrorCode	.getInt() );

		for(ExtTag exttag: exttags)
			if( tmp.tagtable.get(exttag.tag.getName()) == null )
				tagtable.remove( exttag.tag );
		
		for(ExtTag exttag: tmp.exttags) {
			Tag tag = tagtable.get(exttag.tag.getName());
			if( tag == null  ||  !(tag instanceof TagRWInt) )
				tagtable.add( exttag.tag );
			else {
				Tag auxtag = exttag.tag.getObject();
				if( (auxtag == null) != (tag.getObject() == null) )
					tag.setObject(auxtag);
				exttag.tag = (TagRWInt)tag;
			}
		}
		exttags = tmp.exttags;
		
		sizeTotal = tmp.sizeTotal;
		sizePer = tmp.sizePer;
		sizeReq = tmp.sizeReq;
		sizeIrr = tmp.sizeIrr;
		progid = tmp.progid;

		indexIrrBegin = tmp.indexIrrBegin;
		packAmountTotal = tmp.packAmountTotal;
		packAmountPer = tmp.packAmountPer;
		
		needToReadAll = true;

		updateTagsStatus();

		return true;
	}




}
