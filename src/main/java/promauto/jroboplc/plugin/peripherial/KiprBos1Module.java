package promauto.jroboplc.plugin.peripherial;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import promauto.jroboplc.core.api.Configuration;
import promauto.jroboplc.core.api.Flags;
import promauto.jroboplc.core.api.Plugin;
import promauto.jroboplc.core.api.Tag;
import promauto.jroboplc.core.tags.TagRW;
import promauto.utils.Numbers;
import promauto.utils.Strings;

import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;


public class KiprBos1Module extends PeripherialModule {
	private static final Charset CHARSET_WIN1251 = Charset.forName("windows-1251");
	public static final int STATUS_OK = 0;
	public static final int STATUS_ERROR = 1;
	public static final int STATUS_BROKEN = 2;
	public static final int STATUS_SHORT_CIRCUIT = 3;
	public static final int STATUS_BAD_NUMBER = 4;
	public static final int STATUS_REQUEST_ERROR = 5;
	private final Logger logger = LoggerFactory.getLogger(KiprBos1Module.class);
	protected ProtocolAscii protocol = new ProtocolAscii(this);

	private static class Sensor {
		int building;
		int box;
		int probe;
		int probeType;
		int num;
		TagRW tagValue;
		TagRW tagStatus;
		TagRW tagAnswer;
	}

	protected List<Sensor> sensors = new LinkedList<>();
	private TagRW tagCounter;


	public KiprBos1Module(Plugin plugin, String name) {
		super(plugin, name);
		protocol.adjustBuffers(14, 26);
	}


	@Override
	public boolean loadPeripherialModule(Object conf) {
		Configuration cm = env.getConfiguration();
		for(Object confBuilding: cm.toList(cm.get(conf, "buildings"))) {
			int buildingAddr = cm.get(confBuilding, "addr", 0);
			String buildingName = cm.get(confBuilding, "name", "" + buildingAddr);

			for(Object confBox: cm.toList(cm.get(confBuilding, "boxes"))) {
				int boxAddr = cm.get(confBox, "addr", 0);
				String boxName = cm.get(confBox, "name", "" + boxAddr);

				for(Object confProbe: cm.toList(cm.get(confBox, "probes"))) {
					int probeSize = cm.get(confProbe, "size", 0);
					int probeType = cm.get(confProbe, "type", 0);
					int probeAddr = cm.get(confProbe, "addr", 0);
					String probeName = cm.get(confProbe, "name", "" + probeAddr);

					for( int i = 0; i < probeSize; ++i) {
						Sensor sensor = new Sensor();
						sensor.building = buildingAddr;
						sensor.box = boxAddr;
						sensor.probe = probeAddr;
						sensor.probeType = probeType;
						sensor.num = i + 1;
						String tagname = String.format("%s_%s_%s_%d.", buildingName, boxName, probeName, sensor.num);
						sensor.tagValue = tagtable.createRWInt(tagname + "T", 0, Flags.STATUS);
						sensor.tagStatus = tagtable.createRWInt(tagname + "status", 0);
						sensor.tagAnswer = tagtable.createRWString(tagname + "answer", "");
						sensors.add(sensor);
					}
				}
			}
		}
		tagCounter = tagtable.createRWInt("Counter", 0);
		return true;
	}


	@Override
	protected void initChannelMap(List<String> chtags) {
//		addChannelMapTag(chtags, tagOutput);
//		addChannelMapTag(chtags, tagStart);
	}



	@Override
	public boolean executePeripherialModule() {
		if(emulated) {
			sensors.forEach(sensor -> {
				sensor.tagValue.acceptWriteValue();
				sensor.tagStatus.acceptWriteValue();
				sensor.tagAnswer.acceptWriteValue();
			});
			return true;
		}

		tagCounter.setReadValInt(0);
		boolean result = true;
		try {
			for(Sensor sensor: sensors) {
				protocol.buffout[0] = 0x1a;
				Numbers.intToAsciiByte(sensor.building, protocol.buffout, 1);
				Numbers.intToAsciiByte(sensor.box, 		protocol.buffout, 3);
				Numbers.intToAsciiByte(sensor.probe, 	protocol.buffout, 5);
				Numbers.intToAsciiByte(sensor.num, 		protocol.buffout, 7);
				protocol.buffout[9] = sensor.probeType + 0x30;
				int crc = calcCrc(protocol.buffout, 1, 10);
				protocol.buffout[10] = 0x30 + (crc >> 4);
				protocol.buffout[11] = 0x30 + (crc & 0xF);
				protocol.buffout[12] = 0xD;
				protocol.buffout[13] = 0xA;

				Arrays.fill(protocol.buffin, 0);
				result = protocol.request(14, 26, checkAnswer );

				int status = STATUS_OK;
				if( result ) {
					byte[] bb = new byte[8];
					for(int i = 0; i < 8; ++i)
						bb[i] = (byte)(protocol.buffin[i + 14] & 0xff);
					String answer = new String(bb, CHARSET_WIN1251);
					sensor.tagAnswer.setReadValString(answer);

					String value = answer.substring(2, 8).trim();
					if (value.equals("ОШИБКА"))
						status = STATUS_ERROR;
					else
					if (value.equals("ОБРЫВ"))
						status = STATUS_BROKEN;
					else
					if (value.equals("К.З."))
						status = STATUS_SHORT_CIRCUIT;
					else {
						try {
							long t = Math.round(Double.parseDouble(value) * 10.0);
							sensor.tagValue.setReadValLong(t);
						} catch (NumberFormatException e) {
							status = STATUS_BAD_NUMBER;
						}
					}

					sensor.tagValue.setStatus(Tag.Status.Good);
				} else {
					status = STATUS_REQUEST_ERROR;
					sensor.tagValue.setStatus(Tag.Status.Bad);
				}
				sensor.tagStatus.setReadValInt(status);
				tagCounter.setReadValInt( tagCounter.getInt() + 1 );

				if( Thread.interrupted() )
					break;
			}
		} catch (Exception e) {
			env.printError(logger, e, name);
			result = false;
		}
//		return result;
		return true;
	}


	private final ProtocolAscii.CheckAnswerFunc checkAnswer = (size) -> {
		boolean res =
				   protocol.buffin[0] == 0x1c
				&& protocol.buffin[11] == 0xd
				&& protocol.buffin[12] == 0xa
				&& protocol.buffin[13] == 0x1c
				&& protocol.buffin[24] == 0xd
				&& protocol.buffin[25] == 0xa;

		int crc = calcCrc(protocol.buffin, 1, 9);
		res &= protocol.buffin[9] == 0x30 + (crc >> 4);
		res &= protocol.buffin[10] == 0x30 + (crc & 0xF);

		crc = calcCrc(protocol.buffin, 14, 22);
		res &= protocol.buffin[22] == 0x30 + (crc >> 4);
		res &= protocol.buffin[23] == 0x30 + (crc & 0xF);
		return res;
	};


	private int calcCrc(int[] buff, int from, int to) {
		int crc = 0;
		for(int i = from; i < to; ++i)
			crc ^= buff[i];
		return crc & 0xFF;
	}


	@Override
	protected boolean reload() {
		KiprBos1Module tmp = new KiprBos1Module(plugin, name);

		if( !tmp.load() )
			return false;

		copySettingsFrom(tmp);
		sensors.forEach(sensor -> {
			tagtable.remove(sensor.tagAnswer);
			tagtable.remove(sensor.tagStatus);
			tagtable.remove(sensor.tagValue);
		});

		tmp.sensors.forEach(sensor -> {
			tagtable.add(sensor.tagAnswer);
			tagtable.add(sensor.tagStatus);
			tagtable.add(sensor.tagValue);
		});

		sensors = tmp.sensors;

		return true;
	}



}





















