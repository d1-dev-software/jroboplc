package promauto.jroboplc.plugin.peripherial;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import promauto.jroboplc.core.api.Flags;
import promauto.jroboplc.core.api.Plugin;
import promauto.jroboplc.core.api.Tag;
import promauto.jroboplc.core.tags.TagRW;

public class InnoVertISDModule extends PeripherialModule {
        
        private final Logger logger = LoggerFactory.getLogger(InnoVertISDModule.class);
        
        protected ProtocolModbus protocol = new ProtocolModbus(this);

        
    protected Tag tagR0001;		//setted freq
    protected Tag tagR0002;		//actual freq
    protected Tag tagR0003;		//current
    protected Tag tagR0004;		// speed 
    protected Tag tagR0005;		// Bus U
    protected Tag tagR0007;		// pid obr svjaz
    protected Tag tagR0008;		// motohr
    protected Tag tagR0010;		// err1
    
    protected Tag tagR0015;		// freq at last fault
    protected Tag tagR0016;		// current at last fault
    protected Tag tagR0017;		// U at last fault
    

    protected TagRW tagR2001;
    protected TagRW tagR2000;
          


        
        public InnoVertISDModule(Plugin plugin, String name) {
                super(plugin, name);
        }


        @Override
        public boolean loadPeripherialModule(Object conf) {
        	        	
        	tagR0001= tagtable.createInt("Reg.R0001", 0, Flags.STATUS);		//setted freq
        	tagR0002= tagtable.createInt("Reg.R0002", 0, Flags.STATUS);		//actual freq
        	tagR0003= tagtable.createInt("Reg.R0003", 0, Flags.STATUS);		//current
        	tagR0004= tagtable.createInt("Reg.R0004", 0, Flags.STATUS);		// speed
        	tagR0005= tagtable.createInt("Reg.R0005", 0, Flags.STATUS);		// Bus U
        	tagR0007= tagtable.createInt("Reg.R0007", 0, Flags.STATUS);		// pid obr svjaz
        	tagR0008= tagtable.createInt("Reg.R0008", 0, Flags.STATUS);		// motohr
        	tagR0010= tagtable.createInt("Reg.R0010", 0, Flags.STATUS);		// err1
        	    
        	tagR0015= tagtable.createInt("Reg.R0015", 0, Flags.STATUS);		// freq at last fault
        	tagR0016= tagtable.createInt("Reg.R0016", 0, Flags.STATUS);		// current at last fault
        	tagR0017= tagtable.createInt("Reg.R0017", 0, Flags.STATUS);		// U at last fault
        	
        	tagR2000       = protocol.addWriteTag( 0x2000, tagtable.createRWInt("Reg.R2000", 0, Flags.STATUS));
        	tagR2001       = protocol.addWriteTag( 0x2001, tagtable.createRWInt("Reg.R2001", 0, Flags.STATUS));
        	              
        	return true;
        }


        
        @Override
        public boolean executePeripherialModule() {             
                if(emulated) {
                	tagR2000.acceptWriteValue();
                	tagR2001.acceptWriteValue();                     
                        return true;
                }
                
                boolean result = true;
                
                try {
                        
                        result = protocol.sendWriteTags(6);                    
                        if( result )
                                if( result = protocol.requestCmd3(1, 8) ) {
                                	tagR0001.setInt(                protocol.getAnswerWord(0) );
                                	tagR0002.setInt(                protocol.getAnswerWord(1) );
                                	tagR0003.setInt(                protocol.getAnswerWord(2) );
                                	tagR0004.setInt(                protocol.getAnswerWord(3) ); 
                                	tagR0005.setInt(                protocol.getAnswerWord(4) );
                                	tagR0007.setInt(                protocol.getAnswerWord(5) );
                                	tagR0008.setInt(                protocol.getAnswerWord(6) );
                                	tagR0010.setInt(                protocol.getAnswerWord(7) );
                                }
                        		if( result = protocol.requestCmd3(15, 3) ) {                        			
                        			tagR0015.setInt(                protocol.getAnswerWord(0) );
                        			tagR0016.setInt(                protocol.getAnswerWord(1) );
                        			tagR0017.setInt(                protocol.getAnswerWord(2) );
                        		}
                                if( result = protocol.requestCmd3(0x2000, 1) ) {
                                	tagR2000       .setReadValInt(  protocol.getAnswerWord(0) );
                                }                       
                                if( result = protocol.requestCmd3(0x2001, 1) ) {
                                	tagR2001       .setReadValInt(  protocol.getAnswerWord(0) );
                                }
                                            
                        
                } catch (Exception e) {
                        env.printError(logger, e, name);
                        result = false;
                }
        
                return result;
        }

}

