package promauto.jroboplc.plugin.peripherial;

import promauto.jroboplc.core.api.Tag;
import promauto.jroboplc.core.tags.TagRW;
import promauto.utils.CRC;
import promauto.utils.Numbers;

public class ProtocolAA55 {

	public static final int CrcAA55 = 0;
	public static final int Crc8 = 1;
	public static final int Crc16 = 2;
	public static final int CrcAA55Word = 3;
	private static final int MULTIREQ_ATTEMPTS_MAX = 10;

	private PeripherialModule module = null;

	private int[] multiBufinp = null;
	public int[] bufinp = null;
	public int bufinpCount = 0;

	private int[] bufout = null;
	private int cntAA;
	private int cntRead;
	private long lastTimeResetCnt = 0;


	static final int[] pRksDatConv = {
			0x18, 0x00, 0x08, 0x10, 0x30, 0x20, 0x38, 0x28,
			0x1A, 0x02, 0x0A, 0x12, 0x32, 0x22, 0x3A, 0x2A,

			0x19, 0x01, 0x09, 0x11, 0x31, 0x21, 0x39, 0x29,
			0x1C, 0x04, 0x0C, 0x14, 0x34, 0x24, 0x3C, 0x2C,

			0x1B, 0x03, 0x0B, 0x13, 0x33, 0x23, 0x3B, 0x2B,
			0x1D, 0x05, 0x0D, 0x15, 0x35, 0x25, 0x3D, 0x2D,

			0x1E, 0x06,	0x0E, 0x16, 0x36, 0x26, 0x3E, 0x2E,
			0x1F, 0x07, 0x0F, 0x17, 0x37, 0x27, 0x3F, 0x2F
	};



	public ProtocolAA55(PeripherialModule module) {
		this.module = module;
	}


	private void adjustBufinp(int size) {
		if(bufinp == null) {
			bufinp = new int[size];
		} else

		if(bufinp.length < size) {
			int[] newbufinp = new int[size];
			System.arraycopy(bufinp, 0, newbufinp, 0, bufinp.length);
			bufinp = newbufinp;
		}
	}

	private void adjustBufout(int size) {
		if( bufout == null  ||  bufout.length < size )
			bufout = new int[size];
	}


	public void writeBytes(int[] data, int size) throws Exception {
		writeBytes1(data, size);

		if( module.isDebugLogging() ) {
			module.logDebug(data, size, "");
		}
	}


	private void writeBytes1(int[] data, int size) throws Exception {
		adjustBufout(data.length * 2);

		int b = 0;
		int k = 0;
		for (int i = 0; i < size; i++) {
			b = data[i];
			if (i > 0) {
				if (b == 0x55) {
					b = 0;
					bufout[k++] = 0xAA;
				} else

				if (b == 0xAA) {
					b = 1;
					bufout[k++] = 0xAA;
				}
			}
			bufout[k++] = b;
		}
		module.port.writeBytes(bufout, k);
	}



	public boolean readBytes(int[] data, int size) throws Exception {
		return readBytes(data, 0, size);
	}

	public boolean readBytes(int[] data, int offset, int size) throws Exception {
		int b = 0;
		int i = 0;
		cntAA = 0;
		boolean fl = false;
		for (i = 0; i < size; i++) {
			if ((b = module.port.readByte()) >= 0) {
				if (b == 0xAA) {
					fl = true;
					i--;
					cntAA++;
				} else {
					if (fl) {
						if (b == 0)
							b = 0x55;
						else if (b == 1)
							b = 0xAA;

						fl = false;
					}
					data[offset + i] = b;
				}
			} else
				break;
		}

		cntRead = i;
		return (b >= 0);
	}



	public boolean checkCrcAA55(int[] data, int size) {
		int last = size-1;
		int sum = 0;
		for (int i=0; i<last; i++)
			sum = sum + data[i];
		int a1 = ((sum + module.netaddr) & 0xFF);
		int a2 = data[last] & 0xFF;
		return a1==a2;
	}


	public boolean checkCrcAA55Word(int[] data, int size) {
		int last = size-2;
		int sum = 0;
		for (int i=0; i<last; i++)
			sum = sum + data[i];
		int a1 = ((sum + module.netaddr) & 0xFFFF);
		int a2 = Numbers.bytesToWord(data, last);
		return a1==a2;
	}


	public boolean checkCrc8(int[] data, int size) {
		int a1 = CRC.getCrc8(data, 0, size-2);
//        int a1 = CRC.getCrc8(data, 1, size-2); // it was a silent bug
		int a2 = data[size-1] & 0xFF;
		return a1==a2;
	}


	public boolean checkCrc16(int[] data, int size) {
		int a1 = CRC.getCrc16(data, 0, size-3);
		int a2 = Numbers.bytesToWord(data, size-2);
		return a1==a2;
	}



	public void setCrcAA55(int[] data, int size) {
		int last = size-1;
		int sum = 0;
		for (int i=2; i<last; i++)
			sum = sum + data[i];
		data[last] = ((sum + module.netaddr) & 0xFF);
	}


	public boolean multiRequest(int[] dataout, int sizeout, int[] datain, int sizein, int sizeMultiReq, int crctype) throws Exception {

		sizeMultiReq = Math.min(sizeMultiReq, sizein);
		if( multiBufinp == null  ||  multiBufinp.length < sizeMultiReq)
			multiBufinp = new int[sizeMultiReq];

		for (int i = 0; i < MULTIREQ_ATTEMPTS_MAX; ++i) {
			if (request(dataout, sizeout, datain, sizein, crctype)) {
				if( i > 0) {
					if( Numbers.compareIntArrays(datain, multiBufinp, sizeMultiReq)) {
						return true;
					} else {
						if( module.canLogError() )
							module.logError(i, false, dataout, sizeout, datain, cntRead, "multiReq Error");
					}
				}
				System.arraycopy(datain, 0, multiBufinp, 0, sizeMultiReq);
			} else
				break;
		}
		return false;
	}


	public boolean multiRequest(int[] dataout, int sizeout) throws Exception {
		for (int i = 0; i < MULTIREQ_ATTEMPTS_MAX; ++i) {
			if (request(dataout, sizeout)) {
				if( i > 0) {
					if( Numbers.compareIntArrays(bufinp, multiBufinp, bufinpCount)) {
						return true;
					} else {
						if( module.canLogError() )
							module.logError(i, false, dataout, sizeout, bufinp, bufinpCount, "multiReq Error");
					}
				}

				if( multiBufinp == null  ||  multiBufinp.length < bufinpCount)
					multiBufinp = new int[bufinpCount];
				System.arraycopy(bufinp, 0, multiBufinp, 0, bufinpCount);
			} else
				break;
		}
		return false;
	}


	public boolean request(int[] dataout, int sizeout, int[] datain, int sizein, int crctype) throws Exception {
		for (int trynum = 0; trynum < module.retrial; trynum++) {
			boolean badcrc = false;
			module.port.discard();
			module.delayBeforeWrite();
			writeBytes1(dataout, sizeout);
			boolean res = readBytes(datain, sizein);

			if( res ) {
				if( crctype == CrcAA55) {
					res = checkCrcAA55(datain, sizein);
				} else

				if( crctype == Crc8 ) {
					res = checkCrc8(datain, sizein);
				} else

				if( crctype == Crc16 ) {
					res = checkCrc16(datain, sizein);
				} else

				if( crctype == CrcAA55Word) {
					res = checkCrcAA55Word(datain, sizein);
				}

				badcrc = !res;
			}

			if( module.isDebugLogging() ) {
				module.logDebug(dataout, sizeout, datain, cntRead, (!res? " err": "") +  (badcrc? " crc" : ""));
			}

			if( res )
				return true;

			if( module.canLogError() )
				module.logError(trynum, badcrc, dataout, sizeout, datain, cntRead, "cntAA=" + cntAA);

			module.delayAfterError();
		}
		module.tagErrorCnt.setInt( module.tagErrorCnt.getInt() + 1 );

		return false;
	}


	// read variable size packet, using internal input buffer
	public boolean request(int[] dataout, int sizeout) throws Exception {
		for (int trynum = 0; trynum < module.retrial; trynum++) {
			boolean badcrc = false;
			module.port.discard();
			module.delayBeforeWrite();
			writeBytes1(dataout, sizeout);
			module.port.discard();

			bufinpCount = 0;
			adjustBufinp(2);
			boolean res = readBytes(bufinp, 2);
			if( res ) {
				int size = bufinp[1];
				bufinpCount = size + 2;

				adjustBufinp(bufinpCount);
				res = readBytes(bufinp, 2, size);
				if( res ) {
					res = checkCrc16(bufinp, bufinpCount);
					badcrc = !res;
				}
			}

			if( module.isDebugLogging() ) {
				module.logDebug(dataout, sizeout, bufinp, cntRead, (!res? " err": "") +  (badcrc? " crc" : ""));
			}

			if( res )
				return true;

			if( module.canLogError() )
				module.logError(trynum, badcrc, dataout, sizeout, bufinp, cntRead, "cntAA=" + cntAA);

			module.delayAfterError();
		}
		module.tagErrorCnt.setInt( module.tagErrorCnt.getInt() + 1 );

		return false;
	}



	public String requestString(int cmd) throws Exception {
		adjustBufout(6);

		bufout[0] = 0x55;
		bufout[1] = 0xEF;
		bufout[2] = Numbers.hexDigit[(module.netaddr >> 4) & 0xF];
		bufout[3] = Numbers.hexDigit[module.netaddr & 0xF];
		bufout[4] = cmd;
		bufout[5] = 13;


		for (int trynum = 0; trynum < module.retrial; trynum++) {
			module.port.discard();
			module.delayBeforeWrite();
			module.port.writeBytes(bufout, 6);
			module.port.discard();

			String answer = module.port.readStringDelim(13);

			boolean res = !answer.isEmpty();

			if( module.isDebugLogging() ) {
				module.logDebug(bufout, 6, '"' + answer + '"' + (!res? " err": ""));
			}

			if( res )
				return answer;

			if( module.canLogError() ) {
				module.logError(trynum, false, bufout, 6, bufinp, 0, "requestString: " + answer);
			}

			module.delayAfterError();
		}
		module.tagErrorCnt.setInt( module.tagErrorCnt.getInt() + 1 );

		return "";
	}


	public boolean requestFirmware(TagRW tag) throws Exception {
		String answer = requestString('M');
		if( answer.length() < 3 ) {
			// todo refactoring
			if( module.canLogError() ) {
				module.logError(0, false, bufout, 0, bufinp, 0, "requestFirmware error0: " + answer);
			}
			module.tagErrorCnt.setInt( module.tagErrorCnt.getInt() + 1 );
			return false;
		}

		try {
			int netaddr = Integer.parseInt(answer.substring(1, 3), 16);
			if( answer.charAt(0) != '!'  ||  netaddr != module.netaddr) {
				// todo refactoring
				if( module.canLogError() ) {
					module.logError(0, false, bufout, 0, bufinp, 0, "requestFirmware error1: " + answer);
				}
				module.tagErrorCnt.setInt( module.tagErrorCnt.getInt() + 1 );
				return false;
			}
		} catch (NumberFormatException e) {
			// todo refactoring
			if( module.canLogError() ) {
				module.logError(0, false, bufout, 0, bufinp, 0, "requestFirmware error2: " + answer);
			}
			module.tagErrorCnt.setInt( module.tagErrorCnt.getInt() + 1 );
			return false;
		}

		tag.setReadValString( answer.substring(3, answer.length()-1) );

		return true;
	}


	public boolean requestResetCnt(Tag tag, int resetCntPeriod) throws Exception {
		if( resetCntPeriod == 0  || (System.currentTimeMillis() - lastTimeResetCnt < resetCntPeriod))
			return true;

		String answer = requestString('R');
		if( answer.length() < 3) {
			// todo refactoring
			if( module.canLogError() ) {
				module.logError(0, false, bufout, 0, bufinp, 0, "requestResetCnt error1: " + answer);
			}
			module.tagErrorCnt.setInt( module.tagErrorCnt.getInt() + 1 );
			return false;
		}

		try {
			tag.setInt(Integer.parseInt(answer.substring(2, answer.length() - 1), 16));
		} catch (NumberFormatException|IndexOutOfBoundsException e) {
			// todo refactoring
			if( module.canLogError() ) {
				module.logError(0, false, bufout, 0, bufinp, 0, "requestResetCnt error2: " + answer);
			}
			module.tagErrorCnt.setInt( module.tagErrorCnt.getInt() + 1 );
			return false;
		}

		lastTimeResetCnt = System.currentTimeMillis();
		return true;
	}


}