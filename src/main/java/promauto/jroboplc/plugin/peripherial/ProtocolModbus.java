package promauto.jroboplc.plugin.peripherial;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import promauto.jroboplc.core.tags.TagRW;
import promauto.utils.CRC;
import promauto.utils.Numbers;
import promauto.utils.Strings;

public class ProtocolModbus {
	public Charset charset = Charset.forName("UTF-8");

    public static class WriteTag {
		int addr;
		TagRW tag;
		
		public WriteTag(int addr, TagRW tag) {
			this.addr = addr;
			this.tag = tag;
		}
	}

	private PeripherialModule module;
	protected int[] buffin = null;
	protected int[] buffout = null;
	protected List<WriteTag> writeTags = null;
	
	
	public ProtocolModbus(PeripherialModule module) { 
		this.module = module;
	}

	
	public TagRW addWriteTag(int addr, TagRW tag) {
		if( writeTags == null )
			writeTags = new LinkedList<>();
		
		writeTags.add( new WriteTag(addr, tag) );
		return tag;
	}
	
    public boolean requestCmd1(int addr, int size) throws Exception {
		int m = size / 8  +  (size % 8 > 0? 1: 0);
    	final int sizeout = 8;
    	final int sizein = 5 + m;
    	
    	adjustBuffers(sizeout, sizein);
		
		buffout[0] = module.netaddr;
		buffout[1] = 1;
		buffout[2] = (addr >> 8) & 0xFF;
		buffout[3] = addr & 0xFF;
		buffout[4] = (size >> 8) & 0xFF;
		buffout[5] = size & 0xFF;
		
		return request(sizeout, sizein, true);
    }
    

    public boolean requestCmd2(int addr, int size) throws Exception {
		int m = size / 8  +  (size % 8 > 0? 1: 0);
    	final int sizeout = 8;
    	final int sizein = 5 + m;
    	
    	adjustBuffers(sizeout, sizein);
		
		buffout[0] = module.netaddr;
		buffout[1] = 2;
		buffout[2] = (addr >> 8) & 0xFF;
		buffout[3] = addr & 0xFF;
		buffout[4] = (size >> 8) & 0xFF;
		buffout[5] = size & 0xFF;
		
		return request(sizeout, sizein, true);
    }
    

    public boolean requestCmd3(int addr, int size) throws Exception {
    	final int sizeout = 8;
    	final int sizein = size*2 + 5;
    	
    	adjustBuffers(sizeout, sizein);
		
		buffout[0] = module.netaddr;
		buffout[1] = 3;
		buffout[2] = (addr >> 8) & 0xFF;
		buffout[3] = addr & 0xFF;
		buffout[4] = (size >> 8) & 0xFF;
		buffout[5] = size & 0xFF;
		
		return request(sizeout, sizein, true);
    }


	public boolean requestEasyDriveCmd3(int addr) throws Exception {
//		12 03 0D 00 00 00 45 C5
//		12 03 0D 00 |11 40| 41 48 C6 4D
		final int sizeout = 8;
		final int sizein = 10;

		adjustBuffers(sizeout, sizein);

		buffout[0] = module.netaddr;
		buffout[1] = 3;
		buffout[2] = (addr >> 8) & 0xFF;
		buffout[3] = addr & 0xFF;
		buffout[4] = 0;
		buffout[5] = 0;

		return request(sizeout, sizein, true);
	}

	public boolean requestEasyDriveCmd10(int addr, int value) throws Exception {
//		12 10 20 01 00 02 04 |13 88| 00 00 74 88
//		12 10 20 01 00 02 19 6B
		final int sizeout = 13;
		final int sizein = 8;

		adjustBuffers(sizeout, sizein);

		buffout[0] = module.netaddr;
		buffout[1] = 0x10;
		buffout[2] = (addr >> 8) & 0xFF;
		buffout[3] = addr & 0xFF;
		buffout[4] = 0;
		buffout[5] = 2;
		buffout[6] = 4;
		buffout[7] = (value >> 8) & 0xFF;
		buffout[8] = value & 0xFF;
		buffout[9] = 0;
		buffout[10] = 0;

		return request(sizeout, sizein, false);
	}




	public boolean requestCmd4(int addr, int size) throws Exception {
    	final int sizeout = 8;
    	final int sizein = size*2 + 5;
    	
    	adjustBuffers(sizeout, sizein);
		
		buffout[0] = module.netaddr;
		buffout[1] = 4;
		buffout[2] = (addr >> 8) & 0xFF;
		buffout[3] = addr & 0xFF;
		buffout[4] = (size >> 8) & 0xFF;
		buffout[5] = size & 0xFF;
		
		return request(sizeout, sizein, true);
    }
    

    public boolean requestCmd5(int addr, int value) throws Exception {
    	final int sizeout = 8;
    	final int sizein = 8;
    	
    	adjustBuffers(sizeout, sizein);
		
		buffout[0] = module.netaddr;
		buffout[1] = 5;
		buffout[2] = (addr >> 8) & 0xFF;
		buffout[3] = addr & 0xFF;
		buffout[4] = value > 0? 0xFF: 0;
		buffout[5] = 0;
		
		return request(sizeout, sizein, false);
    }
    
    public boolean requestCmd6(int addr, int value) throws Exception {
    	final int cmd = 6;
    	final int sizeout = 8;
    	final int sizein = 8;
    	
    	adjustBuffers(sizeout, sizein);
		
		buffout[0] = module.netaddr;
		buffout[1] = cmd;
		buffout[2] = (addr >> 8) & 0xFF;
		buffout[3] = addr & 0xFF;
		buffout[4] = (value >> 8) & 0xFF;
		buffout[5] = value & 0xFF;
		
		return request(sizeout, sizein, false);
    }
    

    public boolean requestCmd0F(int addr, int size, int... values) throws Exception {
		size = Math.min(values.length, size);
		int m = size / 8  +  (size % 8 > 0? 1: 0);
    	final int sizeout = 9 + m;
    	final int sizein = 8;
    	
    	adjustBuffers(sizeout, sizein);
		
		buffout[0] = module.netaddr;
		buffout[1] = 15;
		buffout[2] = (addr >> 8) & 0xFF;
		buffout[3] = addr & 0xFF;
		buffout[4] = (size >> 8) & 0xFF;
		buffout[5] = size & 0xFF;
		buffout[6] = m;
		int mask = 0x100;
		int pos = 6;
		for(int i=0; i<size; ++i) {
			if( mask == 0x100) {
				mask=1;
				pos++;
				buffout[pos] = 0;
			}
			buffout[pos] |= values[i] > 0? mask: 0;
			mask <<= 1;
		}
		
		return request(sizeout, sizein, false);
    }
    

    public boolean requestCmd10(int addr, int size, int... values) throws Exception {
		size = Math.min(values.length, size);
    	final int sizeout = 9 + size * 2;
    	final int sizein = 8;
    	
    	adjustBuffers(sizeout, sizein);
		
		buffout[0] = module.netaddr;
		buffout[1] = 16;
		buffout[2] = (addr >> 8) & 0xFF;
		buffout[3] = addr & 0xFF;
		buffout[4] = (size >> 8) & 0xFF;
		buffout[5] = size  & 0xFF;
		buffout[6] = size * 2;
		for(int i=0; i<size; ++i) {
			buffout[7+i*2] = (values[i] >> 8) & 0xFF;
			buffout[8+i*2] = values[i] & 0xFF;
		}
		
		return request(sizeout, sizein, false);
    }

	public boolean requestCmd10(int addr, int size, long... values) throws Exception {
		size = Math.min(values.length, size);
		final int sizeout = 9 + size * 4;
		final int sizein = 8;

		adjustBuffers(sizeout, sizein);

		buffout[0] = module.netaddr;
		buffout[1] = 16;
		buffout[2] = (addr >> 8) & 0xFF;
		buffout[3] = addr & 0xFF;
		buffout[4] = ((size * 2) >> 8) & 0xFF;
		buffout[5] = (size * 2) & 0xFF;
		buffout[6] = size * 4;
		for(int i=0; i<size; ++i) {
			buffout[7+i*4] = (int)((values[i] >> 24) & 0xFF);
			buffout[8+i*4] = (int)((values[i] >> 16) & 0xFF);
			buffout[9+i*4] = (int)((values[i] >> 8) & 0xFF);
			buffout[10+i*4] = (int)(values[i] & 0xFF);
		}

		return request(sizeout, sizein, false);
	}


	public void adjustBuffers(int sizeout, int sizein) {
		if(buffout == null  ||  buffout.length < sizeout)
			buffout = new int[sizeout];
		
		if(buffin == null  ||  buffin.length < sizein)
			buffin = new int[sizein];
	}


	private boolean request(int sizeout, int sizein, boolean checkBadReq) throws Exception {


		int crc = CRC.getCrc16(buffout, sizeout-2);
		buffout[sizeout-2] = crc & 0xFF;
		buffout[sizeout-1] = (crc >> 8) & 0xFF;

//		System.out.println( Strings.bytesToHexString(buffout));

		int b = 0;
		for (int trynum = 0; trynum < module.retrial; trynum++) {
			module.port.discard();
			module.delayBeforeWrite();
			module.port.writeBytes(buffout, sizeout);
			
			int cntRead = 0;
			boolean badcrc = false;
			boolean badreq = false;
			for(int i=0; i<sizein; ++i) {
				if ((b = module.port.readByte()) >= 0) {
					buffin[i] = b;
					if(i==1  &&  (b & 0x80)>0 ) {
						sizein = 5;
						badreq = true;
					}
					cntRead++;
				} else
					break;
			}
			
			if(b>=0) {
				int crc1 = CRC.getCrc16(buffin, sizein-2);
				int crc2 = (buffin[sizein-1] << 8) | buffin[sizein-2];

				if( crc1 != crc2 )
					badcrc = true;
				else
					if( buffout[0] == buffin[0]  &&  (!checkBadReq  ||  !badreq) ) {
						if( module.canLogReq() )
							module.logReq(buffout, sizeout, buffin, sizein);
						return true;
					}
			}

			if( module.canLogError() )
				module.logError(trynum, badcrc, buffout, sizeout, buffin, cntRead, (badreq?"BadRequest":"") );

			module.delayAfterError();
		}
			
		module.tagErrorCnt.setInt( module.tagErrorCnt.getInt() + 1 );

    	return false;
    }


	
	public boolean sendWriteTags(int cmd) throws Exception {
		if( writeTags == null )
			return true;

		boolean result = true;
		for(WriteTag wt: writeTags) {
			if( wt.tag.hasWriteValue() ) {
				int value = wt.tag.getWriteValInt();
				
				if( cmd == 6 )
					result = requestCmd6(wt.addr, value);
				else if( cmd == 16 )
					result = requestCmd10(wt.addr, 1, value);
				else
					return false;
				
				if( !result )
					break;

				wt.tag.setReadValInt(value);
			}
		}
		
		return result;
	}


	public int getAnswerWord(int num) {
		return Numbers.bytesToWord( buffin, 3 + (num << 1));
	}

	public int getAnswerInt32(int num) {
		return getAnswerInt32(num, false);
	}

	public int getAnswerInt32(int num, boolean littleEndian) {
		if( littleEndian )
			return (getAnswerWord(num+1) << 16) + getAnswerWord(num);
		else
			return (getAnswerWord(num) << 16) + getAnswerWord(num+1);
	}



	public long getAnswerUInt32(int num) {
		return getAnswerUInt32(num, false);
	}

	public long getAnswerUInt32(int num, boolean littleEndian) {
		if( littleEndian )
			return (((long)getAnswerWord(num+1)) << 16) + getAnswerWord(num);
		else
			return (((long)getAnswerWord(num)) << 16) + getAnswerWord(num+1);
	}



	public int getAnswerBit(int num) {
		int pos = 3 + num/8;
		int mask = 1 << (num%8);
		return (buffin[pos] & mask) > 0? 1: 0;
	}

	private final byte[] buffbytes = new byte[4];
    public float getAnswerFloat(int num) {
        int k = 3 + (num << 1);
        for(int i=0; i<4; ++i)
            buffbytes[i] = (byte)(buffin[k + i]);
        return  ByteBuffer.wrap(buffbytes).getFloat();
    }



	public String getAnswerString(int pos, int size) {
		int ofs = pos*2 + 3;
		int n = size*2;
		byte[] bytes = new byte[n];
		for(int i=0; i<n; ++i)
			bytes[i] = (byte)(buffin[ofs + i]);

		return new String(bytes, charset );
	}



	private final static Charset charsetWIN1251 = Charset.forName("windows-1251");

	public String getAnswerGeliosString(int pos, int size) {
		int ofs = pos*2 + 3;
		int n = size*2;
		StringBuilder sb = new StringBuilder();
		for(int i=0; i<n; ++i) {
			int b = buffin[ofs + i];
			if( b > 0)
				sb.append(decodeGeliosChar(b));
		}
		return sb.toString();
	}



	private final static char[] tableGeliosChars = new char[]{
			'Б','Г','Ё','Ж','З','И','Й','Л','П','У','Ф','Ч','Ш','Ъ','Ы','Э','Ю','Я','б','в','г','ё','ж','з',
			'и','й','к','л','м','н','п','т','ч','ш','ъ','ы','ь','э','ю','я','»','Д','Ц','Щ','д','ф','ц','щ'
	};

	private final static int[] tableGeliosBytes = new int[]{
			0xA0, 0xA1, 0xA2, 0xA3, 0xA4, 0xA5, 0xA6, 0xA7, 0xA8, 0xA9, 0xAA, 0xAB, 0xAC, 0xAD, 0xAE, 0xAF,
			0xB0, 0xB1, 0xB2, 0xB3, 0xB4, 0xB5, 0xB6, 0xB7, 0xB8, 0xB9, 0xBA, 0xBB, 0xBC, 0xBD, 0xBE, 0xBF,
			0xC0, 0xC1, 0xC2, 0xC3, 0xC4, 0xC5, 0xC6, 0xC7, 0xDB, 0xE0, 0xE1, 0xE2, 0xE3, 0xE4, 0xE5, 0xE6
	};

	private char decodeGeliosChar(int b) {
		if( b < tableGeliosBytes[0] )
			return (char)b;

		if( b > tableGeliosBytes[ tableGeliosBytes.length-1 ] )
			return (char)b;

		int found = Arrays.binarySearch(tableGeliosBytes, b);
		if( found < 0)
			return (char)b;

		return tableGeliosChars[found];
	}


}
