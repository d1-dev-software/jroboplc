package promauto.jroboplc.plugin.peripherial;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import promauto.jroboplc.core.api.Configuration;
import promauto.jroboplc.core.api.Flags;
import promauto.jroboplc.core.api.Plugin;
import promauto.jroboplc.core.api.Tag;
import promauto.jroboplc.core.tags.TagRW;

public class SchneiderAtv31Module extends PeripherialModule {
	private final Logger logger = LoggerFactory.getLogger(SchneiderAtv31Module.class);
	
	protected ProtocolModbus protocol = new ProtocolModbus(this);

	protected TagRW 	tagCmd; // 8501 - cmd
    protected TagRW 	tagLfr; // 8502 - set freq

    protected Tag 		tagEta; // 3201 - status
    protected Tag 		tagRfr; // 3202 - output freq
    protected Tag 		tagFrh; // 3203 - cur required freq
    protected Tag 		tagLcr; // 3204 - current in the motor
    protected Tag 		tagOtr; // 3205 - motor torque
    protected Tag 		tagEti; // 3206 - extended status word
    protected Tag 		tagUln; // 3207 - line voltage

    Map<Integer,Integer> initRegs = new HashMap<>();
    boolean initiated;
	boolean keepFreqSet;


	public SchneiderAtv31Module(Plugin plugin, String name) {
		super(plugin, name);
	}


	@Override
	public boolean loadPeripherialModule(Object conf) {

		tagCmd 	= protocol.addWriteTag( 8501, tagtable.createRWInt("Control.CMD", 0, Flags.STATUS));
		tagLfr	= protocol.addWriteTag( 8502, tagtable.createRWInt("FreqSet.LFr", 0, Flags.STATUS));
		
		tagEta = tagtable.createInt("Status.ETA", 0, Flags.STATUS);
		tagRfr = tagtable.createInt("FreqOut.rFr", 0, Flags.STATUS);
		tagFrh = tagtable.createInt("FreqReq.FrH", 0, Flags.STATUS);
		tagLcr = tagtable.createInt("Current.LCr", 0, Flags.STATUS);
		tagOtr = tagtable.createInt("Torque.Otr", 0, Flags.STATUS);
		tagEti = tagtable.createInt("Status.ETI", 0, Flags.STATUS);
		tagUln = tagtable.createInt("Voltage.ULn", 0, Flags.STATUS);
		
		Configuration cm = env.getConfiguration();
		cm.toGenericMap(cm.get(conf, "init.regs"))
			.forEach(  (key,value) -> initRegs.put(
					Integer.parseInt(key.toString()),
					Integer.parseInt(value.toString()) ));

		keepFreqSet = cm.get(conf, "KeepFreqSet", false);
		if( keepFreqSet )
			tagLfr.setFlags(Flags.AUTOSAVE);


		return true;
	}


	
	@Override
	protected boolean preparePeripherialModule() {
		initiated = false;
		return true;
	}


	@Override
	public boolean executePeripherialModule() {
		
		if(emulated) {
			tagCmd.acceptWriteValue();
			tagLfr.acceptWriteValue();
			
			return true;
		}
		
		boolean result = true;
		
		try {
			if( !initiated ) {
				for(Map.Entry<Integer,Integer> ent: initRegs.entrySet() ) {
					if( result )  
						result = protocol.requestCmd3(ent.getKey(), 1);

					if( result  &&  ent.getValue() != protocol.getAnswerWord(0) )
						result = protocol.requestCmd6(ent.getKey(), ent.getValue());
				}
			}
			
			if( result )
				result = protocol.sendWriteTags(0x6);
			
			if( result )
				if( result = protocol.requestCmd3(8501, 2) ) {
					tagCmd.setReadValInt( protocol.getAnswerWord(0) );
					int lfr = decodeFreq(protocol.getAnswerWord(1));
					if( keepFreqSet  &&  lfr != tagLfr.getWriteValInt() ) {
						tagLfr.raiseWriteValue();
					}
					tagLfr.setReadValInt( lfr );
				}

			if( result )
				if( result = protocol.requestCmd3(3201, 7) ) {
					tagEta .setInt( protocol.getAnswerWord(0) );		
					tagRfr .setInt( decodeFreq(protocol.getAnswerWord(1)) );		
					tagFrh .setInt( protocol.getAnswerWord(2) );		
					tagLcr .setInt( protocol.getAnswerWord(3) );		
					tagOtr .setInt( protocol.getAnswerWord(4) );		
					tagEti .setInt( protocol.getAnswerWord(5) );		
					tagUln .setInt( protocol.getAnswerWord(6) );		
				}
			
			initiated = result;
			
		} catch (Exception e) {
			env.printError(logger, e, name);
			result = false;
		}
	
		return result;
	}


	private int decodeFreq(int freq) {
		if( freq < 0x8000 )
			return freq;
		else
			return freq - 0x10000;
	}


	@Override
	protected boolean reload() {

	 	SchneiderAtv31Module tmp = new SchneiderAtv31Module(plugin, name);
		if( !tmp.load() )
			return false;

		copySettingsFrom(tmp);
		keepFreqSet = tmp.keepFreqSet;
		initRegs = tmp.initRegs;

		return true;
	}


}
