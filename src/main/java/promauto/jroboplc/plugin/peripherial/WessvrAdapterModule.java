package promauto.jroboplc.plugin.peripherial;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import promauto.jroboplc.core.AbstractModule;
import promauto.jroboplc.core.api.*;
import promauto.jroboplc.core.tags.Ref;
import promauto.jroboplc.core.tags.TagRW;
import promauto.utils.CRC;

import java.time.LocalTime;
import java.util.*;
import java.util.stream.Collectors;

import static promauto.jroboplc.plugin.peripherial.PeripherialModule.updateWesSvrState;

public class WessvrAdapterModule extends AbstractModule {
    private final Logger logger = LoggerFactory.getLogger(WessvrAdapterModule.class);

    public static final long DEFAULT_COUNTER_SIZE = 0x1_0000_0000L;

    protected static class Item {
        Tag tag;
        Ref refh = null;
        Ref refl = null;
        boolean transform = false;

        private boolean isAssigned() {
            return refh != null  ||  refl != null;
        }
    }

    private static final class WeightHistoryRec implements Comparable<WeightHistoryRec> {
        long timems;
        long weight;

        public WeightHistoryRec(long timems, long weight) {
            this.timems = timems;
            this.weight = weight;
        }

        @Override
        public int compareTo(WeightHistoryRec o) {
            return timems == o.timems? 0: (timems > o.timems? 1: -1);
        }
    }
    private SortedSet<WeightHistoryRec> weightHistoryQueue = new TreeSet<>();
    private long outputPeriodMs = 10000;
    private long lastHistoryWeight = 0;


    protected String module;

    protected List<Item> items = new ArrayList<>();
    protected List<Tag> crctags;
    protected Item itemSumWeight;
    protected Item itemOutput;
    protected Item itemErrorFlag;
    protected Item itemUpdateTime;
    protected Item itemSN;

    protected TagRW tagMaxWeight;
    protected TagRW tagMaxNum;
    protected TagRW tagWesSvrState;

    protected Tag tagMul;
    protected Tag tagCrc32;

    private long wesSvrStateTimer;
    private boolean useNowForUpdateTime;

    // todo: profiles


    public WessvrAdapterModule(Plugin plugin, String name) {
        super(plugin, name);
    }

    @Override
    protected boolean loadModule(Object conf) {
        Configuration cm = env.getConfiguration();

        module = cm.get(conf, "module", "");

        outputPeriodMs = cm.get(conf, "outputPeriodS", 600L) * 1000;

        Map<String, Object> refmap = cm.toMap(cm.get(conf, "tags"));


        itemSumWeight = addItem("SumWeight",    refmap, Tag.Type.LONG);
        itemSumWeight.transform = true;

        addItem("SumNum",       refmap, Tag.Type.LONG);
        addItem("CurWeight",    refmap, Tag.Type.LONG);
        addItem("LastWeight",   refmap, Tag.Type.LONG);
        addItem("LastTime",     refmap, Tag.Type.LONG);
        addItem("Status1",      refmap, Tag.Type.INT);
        addItem("Status2",      refmap, Tag.Type.INT);
        addItem("Status3",      refmap, Tag.Type.INT);
        addItem("Status4",      refmap, Tag.Type.INT);
        addItem("Status5",      refmap, Tag.Type.INT);
        itemOutput = addItem("Output", refmap, Tag.Type.INT);

        items.forEach(item -> {
            item.tag.addFlag( Flags.STATUS );
            item.tag.setStatus( Tag.Status.Bad );
        } );

        itemErrorFlag = addItem("SYSTEM.ErrorFlag", "SYSTEM.ErrorFlag", Tag.Type.BOOL);

        // crc32Tags
        crctags = items.stream()
                .map(item -> item.tag)
                .collect(Collectors.toList());

        useNowForUpdateTime = cm.get(conf, "useNowForUpdateTime", false);
        itemUpdateTime = addItem("SYSTEM.UpdateTime", "SYSTEM.UpdateTime", Tag.Type.INT);

        addItem("Replacement", refmap, Tag.Type.BOOL);
        addItem("SYSTEM.SN", refmap, Tag.Type.STRING);


        tagMaxWeight = tagtable.createRWLong("MaxWeight", cm.get(conf, "MaxWeight", DEFAULT_COUNTER_SIZE));
        tagMaxNum    = tagtable.createRWLong("MaxNum",    cm.get(conf, "MaxNum", DEFAULT_COUNTER_SIZE));

        tagMul = tagtable.createInt(    "Mul", cm.get(conf, "Mul", 1));
        tagCrc32 = tagtable.createLong( "Crc32", 0);

        tagWesSvrState = tagtable.createRWInt("WesSvrState", 0);


        return true;
    }



    private Item addItem(String tagname, Map<String, Object> refmap, Tag.Type tagtype) {
        String refname = refmap.getOrDefault(tagname, "").toString();
        return addItem(tagname, refname, tagtype);
    }


    private Item addItem(String tagname, String refname, Tag.Type tagtype) {
        String[] ss = refname.split(",");
        Item item = new Item();

        item.tag = tagtable.createTag(tagtype, tagname);

        if( !refname.isEmpty() ) {
            if (ss.length == 1) {
                item.refh = env.getRefFactory().createRef(); //new Ref();
                initRef(item.refh, refname);
                item.refl = null;
            } else {
                item.refh = env.getRefFactory().createRef(); //new Ref();
                initRef(item.refh, ss[0].trim());
                item.refl = env.getRefFactory().createRef(); //new Ref();
                initRef(item.refl, ss[1].trim());
            }
        }
        items.add(item);
        return item;
    }


    private void initRef(Ref ref, String refname) {
        int k = refname.indexOf(':');
        if( k < 0 )
            ref.init(module, refname);
        else {
            String[] ss = refname.split(":", 2);
            ref.init(ss[0], ss[1]);
        }
    }

    @Override
    protected boolean prepareModule() {
        for(Item item: items) {
            if( item.refh != null )
                item.refh.prepare();

            if( item.refl != null )
                item.refl.prepare();
        }
        return true;
    }

    @Override
    protected boolean executeModule() {
        boolean linkedAll = true;

        for(Item item: items) {
            if( item == itemErrorFlag )
                continue;

            if( item == itemUpdateTime  &&  useNowForUpdateTime) {
                int tmpUpdateTime = Integer.parseInt( LocalTime.now().format( PeripherialModule.timeFormatter ));
                itemUpdateTime.tag.setInt( tmpUpdateTime );
                continue;
            }

            boolean linked = false;
            if( item.refh != null ) {
                linked = item.refh.linkIfNotValid();
                linkedAll &= linked;
            }

            if( item.refl != null ) {
                linked &= item.refl.linkIfNotValid();
                linkedAll &= linked;
            }

            if( linked ) {
                long value;

                if( item.refh.getTag().getType() == Tag.Type.DOUBLE ) {
                    if (item.transform && tagMul.getInt() != 1)
                        value = (long)(item.refh.getDouble() * tagMul.getDouble());
                    else
                        value = item.refh.getLong();
                } else {
                    if (item.refl == null) {
                        value = item.refh.getLong();
                    } else {
                        value = (item.refh.getLong() << 16) + item.refl.getLong();
                    }

                    if (item.transform && tagMul.getInt() != 1)
                        value = value * tagMul.getInt();
                }

                item.tag.setLong(value);
            }
        }

        if( itemErrorFlag.refh.linkIfNotValid()  &&  linkedAll )
            itemErrorFlag.tag.setBool( itemErrorFlag.refh.getBool() );
        else
            itemErrorFlag.tag.setBool( true );

        tagtable.setTagState(
                itemErrorFlag.tag.getBool()? Tag.Status.Bad: Tag.Status.Good,
                Flags.STATUS);

        if( !itemOutput.isAssigned() ) {
            updateWeightHistory();
            itemOutput.tag.setDouble( calcOutput() );
        }

        wesSvrStateTimer = updateWesSvrState(tagWesSvrState, wesSvrStateTimer);

        tagCrc32.setLong( CRC.getOctoCrc32FromTagStream( crctags.stream() ) );

        return true;
    }


    @Override
    public String check() {
        String res = "";
        for(Item item: items) {
            res += check1( item.refh );
            res += check1( item.refl );
        }
        return res;
    }

    private String check1(Ref ref) {
        String res = "";
        if( ref != null ) {
            res = ref.check();
            res += res.isEmpty()? "": "\n";
        }
        return res;
    }



    @Override
    public String getInfo() {
        return "wessvr.adapter for " + module;
    }


    @Override
    protected boolean reload() {
        WessvrAdapterModule tmp = new WessvrAdapterModule(plugin, name);
        if( !tmp.load() )
            return false;

        copySettingsFrom(tmp);

        module = tmp.module;
        tagMaxWeight.setReadValLong( tmp.tagMaxWeight.getLong() );
        tagMaxNum.setReadValLong( tmp.tagMaxNum.getLong() );



        List<Tag> addList = new ArrayList<>();
        List<Tag> delList = new ArrayList<>();

        for(Item item: tmp.items)
            item.tag = tagtable.findByNameOrAddToList(item.tag, addList);

        for(Item item: items)
            tmp.tagtable.findByNameOrAddToList(item.tag, delList);

        tagtable.add(addList);
        tagtable.remove(delList);

        items = tmp.items;
        itemErrorFlag = tmp.itemErrorFlag;
        itemUpdateTime = tmp.itemUpdateTime;

        prepareModule();

        return true;
    }




    private void updateWeightHistory() {
        long curtimems = System.currentTimeMillis();

        if( lastHistoryWeight == 0  &&  itemSumWeight.tag.getLong() > 0 ) {
            lastHistoryWeight = itemSumWeight.tag.getLong();
        }

        while( weightHistoryQueue.size() > 0  &&  curtimems - weightHistoryQueue.first().timems > outputPeriodMs)
            weightHistoryQueue.remove(weightHistoryQueue.first());

        while( weightHistoryQueue.size() > 0  &&  curtimems <= weightHistoryQueue.last().timems)
            weightHistoryQueue.remove( weightHistoryQueue.last() );

        if( lastHistoryWeight != itemSumWeight.tag.getLong() ) {
            weightHistoryQueue.add(new WeightHistoryRec(curtimems, itemSumWeight.tag.getLong()));
            lastHistoryWeight = itemSumWeight.tag.getLong();
        }
    }


    private double calcOutput() {
        if( weightHistoryQueue.size() == 0 )
            return 0;

        long weight = weightHistoryQueue.last().weight - weightHistoryQueue.first().weight;
        if( weight < 0 )
            weight = tagMaxWeight.getLong() - weightHistoryQueue.first().weight + weightHistoryQueue.last().weight;
        long time = weightHistoryQueue.last().timems - weightHistoryQueue.first().timems;

        // check for idle
        long avgDoseTime = time / weightHistoryQueue.size();
        long timeSinceLastDosing = System.currentTimeMillis() - weightHistoryQueue.last().timems;
        if( avgDoseTime > 0  &&  timeSinceLastDosing / avgDoseTime >= 3 )
            return 0;

        double output = time > 1?
                3600_000D * (double)weight / (double)time:
                0;

        return output;
    }


}