package promauto.jroboplc.plugin.peripherial;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import promauto.jroboplc.core.api.Configuration;
import promauto.jroboplc.core.api.Flags;
import promauto.jroboplc.core.api.Plugin;
import promauto.jroboplc.core.api.Tag;
import promauto.jroboplc.core.tags.TagRW;
import promauto.utils.CRC;
import promauto.utils.Numbers;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import static java.lang.Math.*;
import static promauto.utils.Numbers.limit;

public class PaGeliosDozkkmcModule extends PeripherialModule {

    private static final long SUM_WEIGHT_MAX = 3_999_999_999L; //0xFFFF_FFFFL;

    // standard state codes (bit mask)
    private final static int STATE_IDLE             = 1;
    private final static int STATE_WAITING_FOR_TASK = 2;
    private final static int STATE_LOADING          = 4;
    private final static int STATE_UNLOADING        = 8;
    private final static int STATE_ALARM            = 16;

    // original device state codes
    public final static int STATUS_IDLE            = 0;
    public final static int STATUS_LOAD_READY      = 1;
    public final static int STATUS_LOAD            = 2;
    public final static int STATUS_UNLOAD_READY    = 3;
    public final static int STATUS_TASK_READY      = 4;
    public final static int STATUS_ALARM           = 5;
    public final static int STATUS_UNLOAD_SUSP     = 6;
    public final static int STATUS_UNLOAD          = 7;

    // original device CMD codes
    private final static int CMD_START              = 0;
    private final static int CMD_TASK               = 6;
    private final static int CMD_LOAD               = 2;
    private final static int CMD_UNLOAD             = 4;
    private final static int CMD_SUSPEND            = 8;
    private final static int CMD_STOPLOAD           = 10;

    // protocol version
    private final static int PROTOCOL_VERSION_AA55_V1 = 1;
    private final static int PROTOCOL_VERSION_AA55_V2 = 2;
    private final static int PROTOCOL_VERSION_MODBUS  = 3;


    private final Logger logger = LoggerFactory.getLogger(PaGeliosDozkkmcModule.class);

    protected ProtocolAA55 protocolAA55 = new ProtocolAA55(this);
    protected ProtocolModbus protocolModbus = new ProtocolModbus(this);
    private boolean multireq;

    // standard tags (all multicomponental dosers must have them)
    private static class StorRec {
        protected Tag tagSumWeight;
        protected Tag tagReqWeight;
        protected Tag tagSetWeight;
        protected Tag tagActive;
        protected long onlineWeight = -1;
    }
    protected StorRec[] recs;
    protected Tag   tagOnlineWeight;
    protected Tag   tagSumWeightMax;
    protected Tag   tagSetWeightMax;
    protected Tag   tagDelayUnload;
    protected Tag   tagCurStor;
    protected Tag   tagCurWeight;
    protected Tag   tagCurPercent;
    protected Tag   tagCurPercentTotal;
    protected Tag   tagState;
    protected Tag   tagCmdStartCycle;
    protected Tag   tagCmdStartTask;
    protected Tag   tagCmdStartLoad;
    protected Tag   tagCmdStartUnload;
    protected Tag   tagCmdSuspUnload;
    protected Tag   tagCmdStopLoad;
    protected TagRW tagSendTask;
    // SendTask:
    // write 0 - nope
    // write 1..0x7fff_ffff - send
    // if read positive written value, then accepted
    // if read negative written value, then failed


    // original device specific tags
    protected TagRW tagStorQnt;
    protected Tag   tagStorQntActual;
    protected Tag   tagTaskMode;
//    protected Tag   tagStage;
//    protected Tag   tagStageTime;
//    protected Tag   tagInOut;
    protected Tag   tagStatus;
    protected Tag   tagErrorCode;
    protected TagRW tagCmd;
    protected Tag   tagReplacement;
    protected Tag   tagUnplugged;

    private TagRW tagFirmware;

    protected Tag   tagStable;
    protected Tag   tagStableTime;
    protected Tag   tagStableRange;
    private long stableMs;
    private int stableWeight;

    private int[] buffout;

    private int version;

    private int storqnt;

    private int lastSendTaskEmuVal;
    private boolean success;
    private final AtomicLong timerUnload;
    private int emulateSpeedLoad;
    private int emulateSpeedUnload;
    private int emulateDelay;
    private long emulateTimer;
    private long emulateSumWeight;




    public PaGeliosDozkkmcModule(Plugin plugin, String name) {
        super(plugin, name);
        useSN = true;
        lastSendTaskEmuVal = 0;
        timerUnload = new AtomicLong(0);
    }


    @Override
    public boolean loadPeripherialModule(Object conf) {
        Configuration cm = env.getConfiguration();

        multireq = cm.get(conf, "multireq", false);

        emulateDelay = cm.get(conf, "emulateDelay", 2000);
        emulateSpeedLoad = cm.get(conf, "emulateSpeedLoad", 0);
        emulateSpeedUnload = cm.get(conf, "emulateSpeedUnload", emulateSpeedLoad * 3);
        version = cm.get(conf, "version", PROTOCOL_VERSION_AA55_V2);
        storqnt = cm.get(conf, "storqnt", 1);
        if( storqnt < 1)
            storqnt = 1;

        buffout = new int[8 + 5 * storqnt];

        tagStorQnt = tagtable.createRWInt("StorQnt", storqnt);
        tagStorQntActual = tagtable.createInt("StorQntActual", 0, Flags.STATUS);

        recs = new StorRec[storqnt];

        int flags = Flags.STATUS + (emulated && emulateSpeedLoad > 0? Flags.AUTOSAVE: 0);
        for(int i = 0; i< storqnt; ++i) {
            recs[i] = new StorRec();
            recs[i].tagSumWeight   = tagtable.createLong("SumWeight"   + (i+1), 0, flags);
            recs[i].tagReqWeight   = tagtable.createLong("ReqWeight"   + (i+1), 0, flags);
            recs[i].tagSetWeight   = tagtable.createLong("SetWeight"   + (i+1), 0, flags);
            recs[i].tagActive      = tagtable.createBool("Active"      + (i+1), false, Flags.STATUS);
        }

        tagOnlineWeight     = tagtable.createLong("OnlineWeight"   , 0, Flags.STATUS);
        tagSumWeightMax     = tagtable.createRWLong("SumWeightMax" , cm.get(conf, "SumWeightMax", SUM_WEIGHT_MAX));
        tagSetWeightMax     = tagtable.createInt( "SetWeightMax"   , 0, Flags.AUTOSAVE);
        tagDelayUnload      = tagtable.createInt( "DelayUnload"    , 0, Flags.AUTOSAVE);

        tagCurStor          = tagtable.createInt( "CurStor"        , 0, flags);
        tagCurWeight        = tagtable.createLong("CurWeight"      , 0, Flags.STATUS);
        tagCurPercent       = tagtable.createInt( "CurPercent"     , 0, Flags.STATUS);
        tagCurPercentTotal  = tagtable.createInt( "CurPercentTotal", 0, Flags.STATUS);
        tagTaskMode         = tagtable.createInt( "TaskMode"       , 0, flags);
//        tagStage            = tagtable.createInt( "Stage"          , 0, Flags.STATUS);
//        tagStageTime        = tagtable.createInt( "StageTime"      , 0, Flags.STATUS);
//        tagInOut            = tagtable.createLong("InOut"          , 0, Flags.STATUS);
        tagStatus           = tagtable.createInt( "Status"         , 0, flags);
        tagState            = tagtable.createInt( "State"          , 0, flags);
        tagErrorCode        = tagtable.createInt( "ErrorCode"      , 0, Flags.STATUS);

        tagStable           = tagtable.createBool("Stable"        , false, Flags.STATUS);
        tagStableTime       = tagtable.createInt( "StableTime"     , 0, Flags.AUTOSAVE);
        tagStableRange      = tagtable.createInt( "StableRange"    , 0, Flags.AUTOSAVE);

        tagCmdStartCycle    = tagtable.createInt("CmdStartCycle"  , 0);
        tagCmdStartTask     = tagtable.createInt("CmdStartTask"   , 0);
        tagCmdStartLoad     = tagtable.createInt("CmdStartLoad"   , 0);
        tagCmdStartUnload   = tagtable.createInt("CmdStartUnload" , 0);
        tagCmdSuspUnload    = tagtable.createInt("CmdSuspUnload"  , 0);
        tagCmdStopLoad      = tagtable.createInt("CmdStopLoad"    , 0);

        tagCmd              = tagtable.createRWInt("Cmd"          , 0);
        tagSendTask         = tagtable.createRWInt("SendTask"     , 0);
        tagReplacement      = tagtable.createBool("Replacement"   , false, Flags.AUTOSAVE);
        tagUnplugged        = tagtable.createBool("Unplugged"     , false, Flags.AUTOSAVE);

        tagFirmware         = tagtable.createRWString("firmware", "");

        // --- crc32 tags ---
        initCrc32Tags();
        for(int i = 0; i< storqnt; ++i)
            crc32Tags.add( recs[i].tagSumWeight );
        crc32Tags.add( tagError );

        return true;
    }


    @Override
    protected void initChannelMap(List<String> chtags) {
        for(int i = 0; i< storqnt; ++i) {
            addChannelMapTag(chtags, recs[i].tagActive, "" + (i+1) );
        }
    }



    @Override
    public boolean executePeripherialModule() {
        if( tagUnplugged.getBool() )
            return false;

        if( emulated ) {
            return emulate();
        }

        boolean result = true;
        try {
            boolean versionModbus = version == PROTOCOL_VERSION_MODBUS;

            // read status
            result = versionModbus?
                    readStatusModbus():
                    readStatusAA55();

            if (result) {
                updateStable();
                updateActives();
                updateState();
            }

            // read sum counters
            result = result  &&  ( versionModbus?
                    readSumCountersModbus():
                    readSumCountersAA55()
            );
            calcCrc32();

            // read task weight
            result = result  &&  ( versionModbus?
                    readTaskWeightModbus():
                    readTaskWeightAA55()
            );

            if (result) {
                calcPercent();
            }

            // write task weight
            if( result  &&  tagSendTask.hasWriteValue() ){

                int sendtask = tagSendTask.getWriteValInt();

                if( sendtask <= 0 ) {
                    tagSendTask.setReadValInt(0);
                } else {
                    result = versionModbus?
                            writeTaskWeightModbus():
                            writeTaskWeightAA55();

                    if (result ) {
                        tagSendTask.setReadValInt( success? sendtask: -sendtask );
                    } else {
                        tagSendTask.raiseWriteValue();
                    }
                }
            }

            // cmd
            if (result) {
                processCmds();

                if( tagCmd.hasWriteValue() ) {
                    result = versionModbus ?
                            writeCmdModbus() :
                            writeCmdAA55();

                    if (!result) {
                        tagCmd.raiseWriteValue();
                    }
                }
            }

            if( result  &&  (firstPass  || tagError.getBool())) {
                result = versionModbus?
                        requestFirmwareModbus():
                        requestFirmwareAA55();
            }

        } catch (Exception e) {
            result = false;
            env.printError(logger, e, name);
        }

        return result;
    }


    private boolean emulate() {
        if( tagSendTask.hasWriteValue() ) {
            if( tagSendTask.getWriteValInt() == 0 )
                tagSendTask.setReadValInt(0);
            else {
                tagSendTask.setReadValInt(lastSendTaskEmuVal);
                lastSendTaskEmuVal = tagSendTask.getWriteValInt();
                if (tagSendTask.getInt() == 0)
                    tagSendTask.setReadValInt(lastSendTaskEmuVal);
            }
        }

        if( emulateSpeedLoad <= 0) {
            tagCmd.acceptWriteValue();
        } else {
            processCmds();

            Arrays.stream(recs).forEach(rec -> rec.tagSetWeight.copyValueTo(rec.tagReqWeight));

            int status = tagStatus.getInt();
            int stor = tagCurStor.getInt();
            long curWeight = tagCurWeight.getLong();

            if( status == STATUS_IDLE  &&  !isTimerStarted()  &&  isCmd(CMD_START)) {
                startTimer();
            }

            if( status == STATUS_IDLE  &&  isTimerStarted()  &&  isTimerDone() ) {
                status = STATUS_TASK_READY;
            }

            if( status == STATUS_TASK_READY  &&  !isTimerStarted()  &&  isCmd(CMD_TASK)) {
                startTimer();
            }

            if( status == STATUS_TASK_READY  &&  isTimerStarted()  &&  isTimerDone() ) {
                status = STATUS_LOAD_READY;
            }

            if( status == STATUS_LOAD_READY  &&  !isTimerStarted()  &&  isCmd(CMD_LOAD)) {
                startTimer();
            }

            if( status == STATUS_LOAD_READY  &&  isTimerStarted()  &&  isTimerDone() ) {
                status = STATUS_LOAD;
                stor = 1;
                curWeight = 0;
                emulateSumWeight = 0;
            }

            if( status == STATUS_LOAD  &&  !isTimerStarted() ) {
                if( isCmd(CMD_STOPLOAD) ) {
                    startTimer();
                    emulateSumWeight += curWeight;
                } else
                if( stor > 0  &&  stor <= recs.length ) {
                    StorRec rec = recs[stor - 1];
                    if (curWeight >= rec.tagReqWeight.getLong()) {
                        rec.tagSumWeight.setLong(rec.tagSumWeight.getLong() + curWeight);
                        emulateSumWeight += curWeight;
                        if (++stor > recs.length) {
                            startTimer();
                        } else {
                            curWeight = 0;
                        }
                    } else {
                        curWeight += Math.round(emulateSpeedLoad * (0.8 + 0.4 * Math.random()));
                    }
                }
            }

            if( status == STATUS_LOAD  &&  isTimerStarted()  &&  isTimerDone() ) {
                stor = 0;
                curWeight = emulateSumWeight;
                status = STATUS_UNLOAD_READY;
            }

            if( status == STATUS_UNLOAD_READY  &&  !isTimerStarted()  &&  isCmd(CMD_UNLOAD) ) {
                startTimer();
            }

            if( status == STATUS_UNLOAD_READY  &&  isTimerStarted()  &&  isTimerDone() ) {
                status = STATUS_UNLOAD;
            }

            if( status == STATUS_UNLOAD ) {
                if( isCmd(CMD_SUSPEND) ) {
                    status = STATUS_UNLOAD_SUSP;
                } else
                if( curWeight <= 0 ) {
                    curWeight = 0;
                    status = STATUS_IDLE;
                } else {
                    curWeight -= Math.round(emulateSpeedUnload * (0.8 + 0.4 * Math.random()));
                }
            }

            if( status == STATUS_ALARM  &&  isCmd(CMD_START) ) {
                status = STATUS_IDLE;
            }

            if( status == STATUS_UNLOAD_SUSP  &&  isCmd(CMD_UNLOAD) ) {
                status = STATUS_UNLOAD;
            }

            tagStatus.setInt(status);
            tagCurStor.setInt(stor);
            tagCurWeight.setLong(curWeight);
        }

        calcPercent();
        updateActives();
        updateStable();
        updateState();
        return true;
    }


    private boolean isCmd(int cmd) {
        return tagCmd.hasWriteValue()  &&  tagCmd.getWriteValInt() == cmd;
    }


    private boolean isTimerStarted() {
        return emulateTimer > 0;
    }

    private boolean isTimerDone() {
        if( emulateTimer < System.currentTimeMillis() ) {
            emulateTimer = 0;
            return true;
        }
        return false;
    }

    private void startTimer() {
        emulateTimer = System.currentTimeMillis() + Math.round(random() * emulateDelay);
    }

    private void processCmds() {
        int status = tagStatus.getInt();
        processCmd(     tagCmdStartCycle,    CMD_START,      status == STATUS_IDLE  ||  status == STATUS_ALARM);
        processCmd(     tagCmdStartTask,     CMD_TASK,       status == STATUS_TASK_READY);
        processCmd(     tagCmdStartLoad,     CMD_LOAD,       status == STATUS_LOAD_READY);
        processCmd(     tagCmdStopLoad,      CMD_STOPLOAD,   status == STATUS_LOAD);
        processCmdDly(  tagCmdStartUnload,   CMD_UNLOAD,     status == STATUS_UNLOAD_READY  ||  status == STATUS_UNLOAD_SUSP, tagDelayUnload, timerUnload);
        processCmd(     tagCmdSuspUnload,    CMD_SUSPEND,    status == STATUS_UNLOAD);
    }

    private void updateState() {
        int status = tagStatus.getInt();
        tagState.setInt(
                (status == STATUS_IDLE? STATE_IDLE: 0) +
                        (status == STATUS_TASK_READY? STATE_WAITING_FOR_TASK : 0) +
                        (status == STATUS_LOAD_READY
                                || status == STATUS_LOAD? STATE_LOADING: 0) +
                        (status == STATUS_UNLOAD_READY
                                || status == STATUS_UNLOAD
                                || status == STATUS_UNLOAD_SUSP? STATE_UNLOADING: 0) +
                        (status == STATUS_ALARM? STATE_ALARM: 0)
        );
    }

    private void updateStable() {
        boolean res = true;
        if( tagStableRange.getInt() > 0) {
            if (Math.abs(tagCurWeight.getInt() - stableWeight) > tagStableRange.getInt()) {
                stableWeight = tagCurWeight.getInt();
                stableMs = System.currentTimeMillis();
                res = false;
            } else {
                if( (System.currentTimeMillis() - stableMs) < (tagStableTime.getLong() * 1000) )
                    res = false;
            }
        }
        tagStable.setBool(res);
    }


//================================================ AA55 ================================================================

    protected boolean readStatusAA55() throws Exception {
        buffout[0] = 0x55;
        buffout[1] = 0x20 + netaddr;
        buffout[2] = 2;
        int crc = CRC.getCrc16(buffout, 1, 2);
        Numbers.wordToBytes(crc, buffout, 3);

        if( !protocolAA55.request(buffout, 5)  ||  protocolAA55.bufinp[0] != netaddr )
            return false;

        int status = 0;
        if( version == PROTOCOL_VERSION_AA55_V1) {
            tagTaskMode.setInt(Numbers.bytesToWord(protocolAA55.bufinp, 2));  //# 2
            tagCurWeight.setLong(Numbers.bytesToInt(protocolAA55.bufinp, 4));  //# 4
//            tagStage.setInt(protocolAA55.bufinp[8]);  //# 1
//            tagStageTime.setInt(Numbers.bytesToWord(protocolAA55.bufinp, 9));  //# 2
//            tagInOut.setInt(Numbers.bytesToWord(protocolAA55.bufinp, 11));  //# 2
            status = protocolAA55.bufinp[13]; //# 1
            tagStatus.setInt(status);
            tagErrorCode.setInt(protocolAA55.bufinp[14]);  //# 1

            if( status==STATUS_IDLE  ||  status==STATUS_ALARM )
                tagCurStor.setInt(0);
            else
                tagCurStor.setInt(1);
        }

        if( version == PROTOCOL_VERSION_AA55_V2) {
            tagTaskMode.setInt(Numbers.bytesToWord(protocolAA55.bufinp, 2));  //# 2
            tagCurStor.setInt(protocolAA55.bufinp[4]);  //# 2
            tagCurWeight.setLong(Numbers.bytesToInt(protocolAA55.bufinp, 6));  //# 4
//            tagStage.setInt(protocolAA55.bufinp[10]);  //# 1
//            tagStageTime.setInt(Numbers.bytesToWord(protocolAA55.bufinp, 11));  //# 2
//            tagInOut.setLong(Numbers.bytesToDWord(protocolAA55.bufinp, 13));  //# 4
            status = protocolAA55.bufinp[17]; //# 1
            tagStatus.setInt(status);
            tagErrorCode.setInt(protocolAA55.bufinp[18]);  //# 1
        }

        return true;
    }


    protected boolean readSumCountersAA55() throws Exception {
        buffout[0] = 0x55;
        buffout[1] = 0x40 + netaddr;
        buffout[2] = 3;
        buffout[3] = storqnt;
        int crc = CRC.getCrc16(buffout, 1, 3);
        Numbers.wordToBytes(crc, buffout, 4);

        boolean res;
        if (!multireq)
            res = protocolAA55.request(buffout, 6);
        else
            res = protocolAA55.multiRequest(buffout, 6);

        if( !res  ||  protocolAA55.bufinp[0] != netaddr )
            return false;

//        if( !protocolAA55.request(buffout, 6)  ||  protocolAA55.bufinp[0] != netaddr )
//            return false;

        int qnt = min(protocolAA55.bufinp[2], storqnt);
        int k = 4;
        long[] weights = new long[qnt];
        for (int i = 0; i < qnt; ++i) {
            weights[i] = Numbers.bytesToDWord(protocolAA55.bufinp, k);
            k += 5;
        }
        setWeights(weights);

        return true;
    }


    protected boolean readTaskWeightAA55() throws Exception {
        buffout[0] = 0x55;
        buffout[1] = 0x60 + netaddr;
        buffout[2] = 2;
        int crc = CRC.getCrc16(buffout, 1, 2);
        Numbers.wordToBytes(crc, buffout, 3);

        if( !protocolAA55.request(buffout, 5)  ||  protocolAA55.bufinp[0] != netaddr )
            return false;

        tagStorQntActual.setInt(protocolAA55.bufinp[2]);
        int qnt = min(tagStorQntActual.getInt(), storqnt);
        int k = 3;
        for (int i = 0; i < qnt; ++i) {
            recs[i].tagReqWeight.setLong(Numbers.bytesToDWord(protocolAA55.bufinp, k + 1));
            k += 5;
        }
        return true;
    }


    protected boolean writeTaskWeightAA55() throws Exception {
        buffout[0] = 0x55;
        buffout[1] = 0xA0 + netaddr;
        buffout[2] = 5 + 5 * storqnt;
        buffout[3] = 0;
        buffout[4] = 3;
        buffout[5] = storqnt;

        int k = 6;
        for (int i = 0; i < storqnt; ++i) {
            buffout[k] = i+1;
            Numbers.dwordToBytes(recs[i].tagSetWeight.getLong(), buffout, k + 1);
            k += 5;
        }

        int crc = CRC.getCrc16(buffout, 1, k-1);
        Numbers.wordToBytes(crc, buffout, k);

        if( !protocolAA55.request(buffout, 8 + 5*storqnt)  ||  protocolAA55.bufinp[0] != netaddr )
            return false;

        success = protocolAA55.bufinp[2] == 0;

        return true;
    }


    protected boolean writeCmdAA55() throws Exception {
        buffout[0] = 0x55;
        buffout[1] = 0xC0 + netaddr;
        buffout[2] = 4;
        int value = tagCmd.getWriteValInt();
        buffout[3] = value;
        buffout[4] = 0;
        int crc = CRC.getCrc16(buffout, 1, 4);
        Numbers.wordToBytes(crc, buffout, 5);

        if( !protocolAA55.request(buffout, 7)  ||  protocolAA55.bufinp[0] != netaddr )
            return false;

        if (protocolAA55.bufinp[2] == 0)
            tagCmd.setReadValInt(value);

        return true;
    }


    protected boolean requestFirmwareAA55() throws Exception {
        return protocolAA55.requestFirmware( tagFirmware );
    }


    //================================================ MODBUS ==============================================================
    protected boolean readStatusModbus() throws Exception {
        if( !protocolModbus.requestCmd4(0x1001, 11))
            return false;

        tagTaskMode.setInt( protocolModbus.getAnswerWord(0) );
        tagCurStor.setInt( protocolModbus.getAnswerWord(1) );
        tagStorQntActual.setInt( tagCurStor.getInt() + protocolModbus.getAnswerWord(2));
        tagCurWeight.setLong( protocolModbus.getAnswerInt32(3) );
//        tagStage.setInt( protocolModbus.getAnswerWord(5) );
//        tagStageTime.setInt( protocolModbus.getAnswerWord(6) );
//        tagInOut.setLong( protocolModbus.getAnswerUInt32(7) );
        tagStatus.setInt( protocolModbus.getAnswerWord(9) );
        tagErrorCode.setInt( protocolModbus.getAnswerWord(10) );
        return true;
    }

    protected boolean readSumCountersModbus() throws Exception {
        if( !protocolModbus.requestCmd4(0x1100, storqnt*2))
            return false;

        long[] weights = new long[storqnt];
        for (int i = 0; i < storqnt; ++i) {
            weights[i] = protocolModbus.getAnswerInt32(i * 2 );
        }
        setWeights(weights);
        return true;
    }

    protected boolean readTaskWeightModbus() throws Exception {
        if( !protocolModbus.requestCmd3(0x1200, storqnt*2))
            return false;

        for (int i = 0; i < storqnt; ++i) {
            recs[i].tagReqWeight.setLong( protocolModbus.getAnswerInt32(i * 2 ) );
        }
        return true;
    }

    protected boolean writeTaskWeightModbus() throws Exception {
        long[] weights = new long[storqnt];
        for (int i = 0; i < storqnt; ++i) {
            weights[i] = recs[i].tagSetWeight.getLong();
        }

        success = protocolModbus.requestCmd10(0x1200, storqnt, weights);

        return success;
    }

    protected boolean writeCmdModbus() throws Exception {
        int value =  tagCmd.getWriteValInt();
        if( !protocolModbus.requestCmd6(0x1000, value) )
            return false;

        tagCmd.setReadValInt(value);
        return true;
    }

    protected boolean requestFirmwareModbus() throws Exception {
        if( !protocolModbus.requestCmd3(0, 20 ) )
            return false;

        tagFirmware.setReadValString( protocolModbus.getAnswerString(0 ,10).trim() );
        return true;
    }

//======================================================================================================================
    private void setWeights(long[] weights) {
        long onlineWeight = 0;
        for (int i = 0; i < weights.length; ++i) {
            if( recs[i].onlineWeight == -1 ) {
                recs[i].onlineWeight = 0;
            } else {
                if( weights[i] > recs[i].tagSumWeight.getLong() ) {
                    recs[i].onlineWeight += weights[i] - recs[i].tagSumWeight.getLong();
                }
            }
            recs[i].tagSumWeight.setLong(weights[i]);
            onlineWeight += recs[i].onlineWeight;
        }
        tagOnlineWeight.setLong( onlineWeight );
//        calcCrc32();
    }


    private void updateActives() {
        for(int i = 0; i< storqnt; ++i) {
            recs[i].tagActive.setBool( i+1 == tagCurStor.getInt() );
        }
    }


    private void processCmd(Tag tagCmdRequest, int value, boolean active) {
        if( tagCmdRequest.getInt() > 0 ) {
            if( active )
                tagCmd.setInt(value);
            else
                tagCmdRequest.setInt(0);
        }
    }

    private void processCmdDly(Tag tagCmdRequest, int value, boolean active, Tag tagDelay, AtomicLong timer) {
        if( tagDelay.getInt() > 0 ) {
            if( tagCmdRequest.getInt() > 0  &&  active ) {
                if( timer.get() == 0 ) {
                    timer.set(System.currentTimeMillis() + tagDelay.getLong() * 1000);
                } else
                if(timer.get() <= System.currentTimeMillis() ) {
                    tagCmd.setInt(value);
                }
            } else {
                tagCmdRequest.setInt(0);
                timer.set(0);
            }
        } else {
            processCmd(tagCmdRequest, value, active);
        }
    }

    private void calcPercent() {
        int curStor = tagCurStor.getInt();

        long curWeight = tagCurWeight.getLong();
        long reqWeight = 0;
        long curWeightTotal = curStor == 0? curWeight: 0;
        long reqWeightTotal = 0;

        for (int i = 0; i < recs.length; ++i) {
            long w = recs[i].tagReqWeight.getLong();
            reqWeightTotal += w;
            if( curStor > 0 ) {
                int cmp = i + 1 - curStor;
                if (cmp < 0) {
                    curWeightTotal += w;
                } else if (cmp == 0) {
                    reqWeight = w;
                    curWeightTotal += curWeight;
                }
            }
        }

        int percent = reqWeight > 0? (int)(curWeight * 100 / reqWeight): 0;
        tagCurPercent.setInt(limit(percent, 0, 100));

        percent = reqWeightTotal > 0? (int)(curWeightTotal * 100 / reqWeightTotal): 0;
        tagCurPercentTotal.setInt(limit(percent, 0, 100));


    }


    @Override
	protected boolean reload() {
		PaGeliosDozkkmcModule tmp = new PaGeliosDozkkmcModule(plugin, name);
		if( !tmp.load() )
			return false;

		copySettingsFrom(tmp);

		// size decreased
        if( tmp.storqnt < storqnt ) {
            for(int i=tmp.storqnt; i<storqnt; ++i) {
                tagtable.remove(recs[i].tagSumWeight);
                tagtable.remove(recs[i].tagReqWeight);
                tagtable.remove(recs[i].tagSetWeight);
            }
            recs = Arrays.copyOf(recs, tmp.storqnt);
        } else

        // size increased
        if( tmp.storqnt > storqnt ) {
            recs = Arrays.copyOf(recs, tmp.storqnt);
            for(int i=storqnt; i<tmp.storqnt; ++i) {
                recs[i] = tmp.recs[i];
                tagtable.add(recs[i].tagSumWeight);
                tagtable.add(recs[i].tagReqWeight);
                tagtable.add(recs[i].tagSetWeight);
            }
        }

        version = tmp.version;
        storqnt = tmp.storqnt;
        buffout = tmp.buffout;
        multireq = tmp.multireq;

        tagStorQnt.setReadValInt(storqnt);

        updateTagsStatus();

		return true;
	}

}
