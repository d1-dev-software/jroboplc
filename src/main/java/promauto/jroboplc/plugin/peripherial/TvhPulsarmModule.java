package promauto.jroboplc.plugin.peripherial;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import promauto.jroboplc.core.api.Flags;
import promauto.jroboplc.core.api.Plugin;
import promauto.jroboplc.core.api.Tag;
import promauto.utils.CRC;
import promauto.utils.Numbers;

public class TvhPulsarmModule extends PeripherialModule {

    private final Logger logger = LoggerFactory.getLogger(PaGeliosFlowModule.class);

    protected Tag tagCounter;

    private int[] buffin = new int[20];

    private int[] buffout = new int[20];



    public TvhPulsarmModule(Plugin plugin, String name) {
        super(plugin, name);
    }

    @Override
    public boolean loadPeripherialModule(Object conf) {

        tagCounter = tagtable.createInt("counter", 0, Flags.STATUS);

        return true;
    }

    @Override
    public boolean preparePeripherialModule() {
        return true;
    }

    private boolean request(int sizeout, int sizein)  throws Exception {

        int b = 0;
        for (int trynum = 0; trynum < retrial; trynum++) {
            port.discard();
            delayBeforeWrite();
            port.writeBytes(buffout, sizeout);

            int cntRead = 0;
            boolean badcrc = false;
            boolean badreq = false;
            for(int i=0; i<sizein; ++i) {
                if ((b = port.readByte()) >= 0) {
                    buffin[i] = b;
                    cntRead++;
                } else
                    break;
            }

            if (b >= 0) {
                int crc1 = CRC.getCrc16(buffin, sizein-2);
                int crc2 = (buffin[sizein-1] << 8) | buffin[sizein-2];

                if( crc1 != crc2 )
                    badcrc = true;
                else
                    return true;
            }

            if( canLogError() )
                logError(trynum, badcrc, buffout, sizeout, buffin, cntRead, (badreq?"BadRequest":"") );

            delayAfterError();
        }

        tagErrorCnt.setInt( tagErrorCnt.getInt() + 1 );

        return false;
    }

    @Override
    public boolean executePeripherialModule() {

        if( !emulated ) {
            try {
                Numbers.intToBytes(netaddr, buffout, 0);
                buffout[4] = 1;                         // код команды
                buffout[5] = 0x0E;                      // длина пакета
                buffout[6] = 0x01;                      // маска запрашиваемых каналов
                buffout[7] = 0x00;
                buffout[8] = 0x00;
                buffout[9] = 0x00;
                buffout[10] = 0x00;                     // идентификатор посылки
                buffout[11] = 0x01;
                int crc = CRC.getCrc16(buffout, 12);
                buffout[12] = crc & 0xFF;
                buffout[13] = (crc >> 8) & 0xFF;

                if (!request(14, 14))
                    return false;

                int netaddr1 = Numbers.bytesToInt(buffin, 0);
                if (netaddr1 != netaddr
                    || (buffin[4] != 1)                             // код команды
                    || (buffin[5] != 0x0E)  )                      // длина пакета
                {
                    return false;
                }

                tagCounter.setInt(Numbers.bytesToIntReverse(buffin, 6));
            } catch (Exception e) {
                env.printError(logger, e, name);
                return false;
            }
        }

        return true;
    }

}
