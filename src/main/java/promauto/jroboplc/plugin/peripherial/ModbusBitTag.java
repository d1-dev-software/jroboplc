package promauto.jroboplc.plugin.peripherial;

import promauto.jroboplc.core.tags.TagRW;

public class ModbusBitTag {
    String refTagname;
    int mask;
    int maskInv;
    int shift;
    TagRW reftag = null;
    TagRW tag;

    public ModbusBitTag(String refTagname, int mask, TagRW tag) {
        this.refTagname = refTagname;
        this.mask = mask;
        this.tag = tag;
        maskInv = mask ^ 0xFFFF;
        shift = 0;
        while (mask > 0  &&  (mask & 1) == 0) {
            mask >>= 1;
            ++shift;
        }
    }

    public void writeValue() {
        int bits = (tag.getWriteValInt() << shift) & mask;
        int value = reftag.hasWriteValue()?
                reftag.getWriteValInt() :
                reftag.getInt();
        reftag.setInt((value & maskInv) + bits);
    }

    public void readValue() {
        tag.setReadValInt( (reftag.getInt() & mask) >> shift );
    }
}
