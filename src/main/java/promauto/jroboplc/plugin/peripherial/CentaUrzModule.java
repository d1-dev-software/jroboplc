package promauto.jroboplc.plugin.peripherial;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import promauto.jroboplc.core.api.Configuration;
import promauto.jroboplc.core.api.Flags;
import promauto.jroboplc.core.api.Plugin;
import promauto.jroboplc.core.api.Tag;
import promauto.jroboplc.core.tags.TagRW;

import java.util.List;


public class CentaUrzModule extends PeripherialModule {
	private final Logger logger = LoggerFactory.getLogger(CentaUrzModule.class);
	protected ProtocolAscii protocol = new ProtocolAscii(this);

	protected Tag tagSumWeight;
	protected TagRW tagOutput;
	protected TagRW tagSetOutput;
	protected TagRW tagStart;
	protected TagRW tagState;


	public CentaUrzModule(Plugin plugin, String name) {
		super(plugin, name);
	}


	@Override
	public boolean loadPeripherialModule(Object conf) {
		tagSumWeight 	= tagtable.createLong("SumWeight", 	0, Flags.STATUS);
		tagOutput 		= tagtable.createRWInt("Output", 		0, Flags.STATUS);
		tagSetOutput 	= tagtable.createRWInt("SetOutput", 	0, Flags.STATUS);
		tagStart 		= tagtable.createRWInt("Start", 		0, Flags.STATUS);
		tagState 		= tagtable.createRWInt("State", 		0, Flags.STATUS);
		return true;
	}


	@Override
	protected void initChannelMap(List<String> chtags) {
		addChannelMapTag(chtags, tagOutput);
		addChannelMapTag(chtags, tagStart);
	}



	@Override
	public boolean executePeripherialModule() {
		if(emulated) {
			tagOutput.acceptWriteValue();
			tagSetOutput.acceptWriteValue();
			tagStart.acceptWriteValue();
			tagState.acceptWriteValue();
			return true;
		}
		
		boolean result = true;
		try {
			result =
				readLong(0x11, tagSumWeight) &&
				writeAndReadInt(0x15, tagOutput) &&
				writeAndReadInt(0x19, tagSetOutput) &&
				writeAndReadInt(0x24, tagStart) &&
				writeAndReadInt(0x25, tagState);

		} catch (Exception e) {
			env.printError(logger, e, name);
			result = false;
		}
		return result;
	}


	private boolean readLong(int addr, Tag tag) throws Exception {
 		if( !protocol.requestCentaRead(addr) )
			 return false;
		tag.setLong(protocol.getAnswerDWord(3));
		return true;
	}

	private boolean writeAndReadInt(int addr, TagRW tag) throws Exception {
		if( tag.hasWriteValue() ) {
			if( !protocol.requestCentaWrite(addr, tag.getWriteValLong()) )
				return false;
		}
		if( !protocol.requestCentaRead(addr) )
			return false;
		tag.setReadValInt((int)protocol.getAnswerDWord(3));
		return true;
	}


}





















