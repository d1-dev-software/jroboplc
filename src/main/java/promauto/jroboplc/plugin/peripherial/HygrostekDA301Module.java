package promauto.jroboplc.plugin.peripherial;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import promauto.jroboplc.core.api.Configuration;
import promauto.jroboplc.core.api.Flags;
import promauto.jroboplc.core.api.Plugin;
import promauto.jroboplc.core.api.Tag;
import promauto.jroboplc.core.tags.TagRW;
import promauto.utils.Numbers;

public class HygrostekDA301Module extends PeripherialModule{
	
	private final Logger logger = LoggerFactory.getLogger(HygrostekDA301Module.class);

	private static final char cmdAdjust	=1;
	private static final char cmdRead	=2;
	private static final char cmdZero	=3;
	private static final char cmdStart	=4;
	private static final char cmdStop	=5;	
	
	private int[] buffout = new int[14];
	private int[] buffin  = new int[44];

    protected Tag tagStatus1;
    protected Tag tagInputDampness;
    protected Tag tagTemperature;
    protected Tag tagProductFlow;
    protected Tag tagWaterFlow;
    protected Tag tagProductAmount;
    protected Tag tagWaterAmount;
    protected Tag tagStatus2;
    protected Tag tagAbsoluteWeight;
    protected Tag tagAlarm;
    
    protected TagRW tagProgramme;
    protected TagRW tagRequiredDampness;
    protected TagRW tagCalibration;
    protected TagRW tagCmdStop;
    protected TagRW tagCmdStart;
    protected TagRW tagCmdZero;

	private int senddelay_ms;


	public HygrostekDA301Module(Plugin plugin, String name) {
		super(plugin, name);
	}
	
	
	@Override
	public boolean loadPeripherialModule(Object conf) {

		Configuration cm = env.getConfiguration();
		senddelay_ms = cm.get(conf, "send_delay_ms", 0);

		
		tagStatus1			= tagtable.createInt("Status1",		0, Flags.STATUS);
		tagInputDampness	= tagtable.createInt("InputDampness", 	0, Flags.STATUS);
		tagTemperature		= tagtable.createInt("Temperature", 	0, Flags.STATUS);
		tagProductFlow		= tagtable.createInt("ProductFlow",	0, Flags.STATUS);
		tagWaterFlow		= tagtable.createInt("WaterFlow",		0, Flags.STATUS);
		tagProductAmount	= tagtable.createInt("ProductAmount",	0, Flags.STATUS);
		tagWaterAmount		= tagtable.createInt("WaterAmount",	0, Flags.STATUS);
		tagStatus2			= tagtable.createInt("Status2",		0, Flags.STATUS);
		tagAbsoluteWeight	= tagtable.createInt("AbsoluteWeight",	0, Flags.STATUS);
		tagAlarm			= tagtable.createInt("Alarm",			0, Flags.STATUS);
		
		tagProgramme 		= tagtable.createRWInt("Programme",		0, Flags.STATUS);
		tagRequiredDampness = tagtable.createRWInt("RequiredDampness",	0, Flags.STATUS);
		tagCalibration		= tagtable.createRWInt("Calibration",		0, Flags.STATUS);
		tagCmdStop 			= tagtable.createRWInt("CmdStop", 0);
		tagCmdStart 		= tagtable.createRWInt("CmdStart", 0);
		tagCmdZero 			= tagtable.createRWInt("CmdZero", 0);
		
		return true;
	}



	@Override
	public boolean executePeripherialModule() {		
		if( emulated ) {
			tagProgramme.acceptWriteValue();
			tagRequiredDampness.acceptWriteValue();
			tagCalibration.acceptWriteValue();
			tagCmdStop.acceptWriteValue();
			tagCmdStart.acceptWriteValue();
			tagCmdZero.acceptWriteValue();
			return true;
		}
		
		boolean result = true;
		try {

			result &= command(tagCmdStop,  cmdStop);
			result &= command(tagCmdStart, cmdStart);
			result &= command(tagCmdZero,  cmdZero);

			if( result  &&  (
					 tagProgramme.hasWriteValue()  ||
					 tagRequiredDampness.hasWriteValue()  ||
					 tagCalibration.hasWriteValue() ))
			{
				Numbers.intToHexByte(tagProgramme.getWriteValInt(), 		buffout, 5);
				Numbers.intToHexByte(tagRequiredDampness.getWriteValInt(), 	buffout, 7);
				Numbers.intToHexByte(tagCalibration.getWriteValInt(), 		buffout, 9);
				result &= request(cmdAdjust, 6);
			}


			if( result  &&  (result &= request(cmdRead, 0))) {
				tagStatus1		 .setInt( Numbers.asciiByteToInt(buffin, 1) );
				tagInputDampness .setInt( Numbers.asciiByteToInt(buffin, 3) );
				tagTemperature 	 .setInt( Numbers.asciiWordToInt(buffin, 5) );
				tagProductFlow	 .setInt( Numbers.asciiWordToInt(buffin, 9) );
				tagWaterFlow	 .setInt( Numbers.asciiWordToInt(buffin, 13) );
				tagProductAmount .setInt( (Numbers.asciiWordToInt(buffin, 17) << 8) + Numbers.asciiByteToInt(buffin, 21) );
				tagWaterAmount	 .setInt( (Numbers.asciiWordToInt(buffin, 23) << 8) + Numbers.asciiByteToInt(buffin, 27) );
				tagRequiredDampness.setReadValInt( Numbers.asciiByteToInt(buffin, 29) );
				tagProgramme       .setReadValInt( Numbers.asciiByteToInt(buffin, 31) );
				tagStatus2		 .setInt( Numbers.asciiByteToInt(buffin, 33) );
				tagAbsoluteWeight.setInt( Numbers.asciiByteToInt(buffin, 35) );
				tagCalibration	   .setReadValInt( Numbers.asciiByteToInt(buffin, 37) );
				tagAlarm		 .setInt( Numbers.asciiByteToInt(buffin, 39) );
			}

		}catch (Exception e) {
			env.printError(logger, e, name);
		}		

		return result;
	}


	private boolean command(TagRW tag, int cmd) throws Exception {
		if( tag.hasWriteValue() ) {
			return request(cmd, 0);
		}
		return true;
	}


	public int calcCrc(int[] data, int size) {
		int crc = 0;
		for(int i = 1; i < size; i++)
		    crc += data[i]; 
		return crc & 0xFF;
	}


    public boolean request(int cmd, int sizedata) throws Exception {
		int sizeout = sizedata + 8;

		buffout[0] = '$';
		Numbers.intToHexByte(netaddr, buffout, 1);
		Numbers.intToHexByte(cmd, buffout, 3);
		Numbers.intToHexByte(calcCrc(buffout, sizeout-3), buffout, sizeout-3);
		buffout[sizeout-1] = 13;

		for (int trynum = 0; trynum < retrial; trynum++) {

			port.discard();

			if( senddelay_ms > 0 )
				for(int j=0; j<sizeout; ++j) {
					port.writeByte(buffout[j]);
					Thread.sleep(senddelay_ms);
				}
			else
				port.writeBytes(buffout, sizeout);


			int sizein = port.readBytesDelim(buffin, 13);

			boolean goodcrc = true;
			if( sizein >= 3  &&  buffin[0] == '*' ) {
				int crc1 = Numbers.asciiByteToInt(buffin, sizein-3);
				int crc2 = calcCrc( buffin, sizein-3 );
				if( goodcrc = (crc1 == crc2) )
					return true;
			}

			if( canLogError() )
				logError(trynum, !goodcrc, buffout, sizeout, buffin, Math.abs(sizein), "");

			try {
				Thread.sleep(20);
			} catch (InterruptedException e) {
			}

		}
		tagErrorCnt.setInt( tagErrorCnt.getInt()+1 );

		return false;
    }	

	
	@Override
	protected boolean reload() {
		HygrostekDA301Module tmp = new HygrostekDA301Module(plugin, name);

		if( !tmp.load() )
			return false;

		copySettingsFrom(tmp);
		senddelay_ms = tmp.senddelay_ms;

		return true;
	}

}
