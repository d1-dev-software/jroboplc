package promauto.jroboplc.plugin.peripherial;

import promauto.jroboplc.core.api.Flags;
import promauto.jroboplc.core.api.Plugin;
import promauto.jroboplc.core.api.Tag;
import promauto.jroboplc.core.tags.TagRW;

import java.util.List;

import static promauto.jroboplc.plugin.peripherial.ModbusTag.Access.RO;
import static promauto.jroboplc.plugin.peripherial.ModbusTag.Type.*;

public class PaGeliosDozmanModule extends ModbusModule {

    private static final int TERM_IO_SIZE = 8;
    private Tag tagUnderweight;
    private Tag tagOverweight;

    private Tag tagWeightReq;
    private Tag tagWeightCur;
    private Tag tagDiffDown;

    private Tag tagDiffUp;

    private TagRW[] bittagsRd = new TagRW[TERM_IO_SIZE];
    private TagRW[] bittagsWr = new TagRW[TERM_IO_SIZE];

    public PaGeliosDozmanModule(Plugin plugin, String name) {
        super(plugin, name);
    }

    @Override
    public boolean loadPeripherialModule(Object conf) {

        if( !super.loadPeripherialModule(conf) )
            return false;

        tagUnderweight = tagtable.createBool("Underweight", false, Flags.STATUS);
        tagOverweight = tagtable.createBool("Overweight", false, Flags.STATUS);

        addCrc32Tags(
                "WeightCur",
                "WeightTot",
                "Stable",
                "Finished",
                "Run",
                "Status",
                tagError
        );

        return true;
    }

    @Override
    protected boolean loadModbusTags(Object conf) {
                       addModbusTag(ModbusTag.create("IsEmptyCur", 0x1000).type(BOOL).access(RO));
                       addModbusTag(ModbusTag.create("IsEmptyTot", 0x1002).type(BOOL).access(RO));
                       addModbusTag(ModbusTag.create("SetEmptyCur",0x1001).type(BOOL));
                       addModbusTag(ModbusTag.create("SetEmptyTot",0x1003).type(BOOL));

        tagWeightReq = addModbusTag(ModbusTag.create("WeightReq",  0x1005).type(INT32)).tag;
        tagWeightCur = addModbusTag(ModbusTag.create("WeightCur",  0x1007).type(INT32).access(RO)).tag;
                       addModbusTag(ModbusTag.create("WeightTot",  0x1009).type(INT32).access(RO));
        tagDiffDown  = addModbusTag(ModbusTag.create("DiffDown",   0x100b).type(INT32)).tag;
        tagDiffUp    = addModbusTag(ModbusTag.create("DiffUp",     0x100d).type(INT32)).tag;

                       addModbusTag(ModbusTag.create("Stable",     0x100f).type(BOOL).access(RO));
                       addModbusTag(ModbusTag.create("Finished",   0x1010).type(BOOL));
                       addModbusTag(ModbusTag.create("Run",        0x1013).type(BOOL));
                       addModbusTag(ModbusTag.create("ResetError", 0x1014).type(BOOL));

                       addModbusTag(ModbusTag.create("Status",     0x1015).type(UINT16));
                       addModbusTag(ModbusTag.create("ErrorCode",  0x1016).type(UINT16));

        ModbusTag termInp = addModbusTag(ModbusTag.create("TermInp", 0x1017).type(UINT16));
        ModbusTag termOut = addModbusTag(ModbusTag.create("TermOut", 0x1018).type(UINT16));

        for(int i = 0; i < TERM_IO_SIZE; ++i) {
            bittagsRd[i] = addModbusBitTag(termInp, "" + i, 1 << i);
            bittagsWr[i] = addModbusBitTag(termOut, "" + i, 1 << i);
        }

        return true;
    }

    @Override
    protected void initChannelMap(List<String> chtags) {
        for(int i = 0; i < TERM_IO_SIZE; ++i) {
            addChannelMapTag(chtags, bittagsRd[i], i + "r");
            addChannelMapTag(chtags, bittagsWr[i], i + "w");
        }
    }


    @Override
    public boolean executePeripherialModule() {
        boolean result = super.executePeripherialModule();
        calcCrc32();
        tagUnderweight.setBool( tagWeightCur.getInt() < tagWeightReq.getInt() - tagDiffDown.getInt());
        tagOverweight.setBool( tagWeightCur.getInt() > tagWeightReq.getInt() + tagDiffUp.getInt());

        return result;
    }

    @Override
    protected boolean reload() {
        PaGeliosDozmanModule tmp = new PaGeliosDozmanModule(plugin, name);
        return reloadFrom(tmp);
    }

}
