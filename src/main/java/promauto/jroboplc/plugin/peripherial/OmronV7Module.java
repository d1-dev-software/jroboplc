package promauto.jroboplc.plugin.peripherial;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import promauto.jroboplc.core.api.Flags;
import promauto.jroboplc.core.api.Plugin;
import promauto.jroboplc.core.api.Tag;
import promauto.jroboplc.core.tags.TagRW;

public class OmronV7Module extends PeripherialModule {
	private final Logger logger = LoggerFactory.getLogger(OmronV7Module.class);
	
	protected ProtocolModbus protocol = new ProtocolModbus(this);

    protected TagRW tagReg0001;
    protected TagRW tagReg0002;
    protected Tag tagReg0020;
    protected Tag tagReg0021;
    protected Tag tagReg0022;
    protected Tag tagReg0023;
    protected Tag tagReg0024;
    protected Tag tagReg0027; 
    protected Tag tagReg0028;
    protected Tag tagReg0029; 
    protected Tag tagReg002A; 

	
	public OmronV7Module(Plugin plugin, String name) {
		super(plugin, name);
	}


	@Override
	public boolean loadPeripherialModule(Object conf) {

		tagReg0001 = protocol.addWriteTag( 0x0001, tagtable.createRWInt("Reg.R0001", 0, Flags.STATUS));
		tagReg0002 = protocol.addWriteTag( 0x0002, tagtable.createRWInt("Reg.R0002", 0, Flags.STATUS));
		
		tagReg0020 = tagtable.createInt("Reg.R0020", 0, Flags.STATUS);
		tagReg0021 = tagtable.createInt("Reg.R0021", 0, Flags.STATUS);
		tagReg0022 = tagtable.createInt("Reg.R0022", 0, Flags.STATUS);
		tagReg0023 = tagtable.createInt("Reg.R0023", 0, Flags.STATUS);
		tagReg0024 = tagtable.createInt("Reg.R0024", 0, Flags.STATUS);
		tagReg0027 = tagtable.createInt("Reg.R0027", 0, Flags.STATUS);
		tagReg0028 = tagtable.createInt("Reg.R0028", 0, Flags.STATUS);
		tagReg0029 = tagtable.createInt("Reg.R0029", 0, Flags.STATUS);
		tagReg002A = tagtable.createInt("Reg.R002A", 0, Flags.STATUS);

		return true;
	}


	
	@Override
	public boolean executePeripherialModule() {
		
		if(emulated) {
			tagReg0001.acceptWriteValue();
			tagReg0002.acceptWriteValue();
			
			return true;
		}
		
		boolean result = true;
		
		try {
			result = protocol.sendWriteTags(0x10);
			
			if( result )
				if( result = protocol.requestCmd3(1, 2) ) {
					tagReg0001.setReadValInt( protocol.getAnswerWord(0) ); 
					tagReg0002.setReadValInt( protocol.getAnswerWord(1) );
				}
				            
			if( result )
				if( result = protocol.requestCmd3(0x0020, 11) ) {
					tagReg0020.setInt( protocol.getAnswerWord(0) );		
					tagReg0021.setInt( protocol.getAnswerWord(1) );		
					tagReg0022.setInt( protocol.getAnswerWord(2) );		
					tagReg0023.setInt( protocol.getAnswerWord(3) );		
					tagReg0024.setInt( protocol.getAnswerWord(4) );
					//      25										   5
					//      26										   6
					tagReg0027.setInt( protocol.getAnswerWord(7) );		
					tagReg0028.setInt( protocol.getAnswerWord(8) );		
					tagReg0029.setInt( protocol.getAnswerWord(9) );		
					tagReg002A.setInt( protocol.getAnswerWord(10));		
				}
			
		} catch (Exception e) {
			env.printError(logger, e, name);
			result = false;
		}
	
		return result;
	}


}
