package promauto.jroboplc.plugin.peripherial;

import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import promauto.jroboplc.core.api.Configuration;
import promauto.jroboplc.core.api.Flags;
import promauto.jroboplc.core.api.Plugin;
import promauto.jroboplc.core.api.Tag;
import promauto.jroboplc.core.tags.TagRW;
import promauto.utils.CRC;
import promauto.utils.Numbers;

public class PaGeliosPassModule extends PeripherialModule {
	private final Logger logger = LoggerFactory.getLogger(PaGeliosPassModule.class);
	
	protected ProtocolAA55 protocol = new ProtocolAA55(this);
	
	protected Tag tagCrc;
	protected TagRW tagSumWeightHigh1;
	protected TagRW tagSumWeightLow1;
	protected TagRW tagSumNumHigh1;
	protected TagRW tagSumNumLow1;
	protected TagRW tagSumWeightHigh2;
	protected TagRW tagSumWeightLow2;
	protected TagRW tagSumNumHigh2;
	protected TagRW tagSumNumLow2;
	protected TagRW tagCurWeightHigh;
	protected TagRW tagCurWeightLow;
	protected TagRW tagLastWeightHigh;
	protected TagRW tagLastWeightLow;
	protected TagRW tagLastTimeHigh;
	protected TagRW tagLastTimeLow;

	protected Tag tagSumWeightKg1;
	protected Tag tagSumWeightKg2;
	protected Tag tagCurWeight;
	protected Tag tagCurWeightKg;
	protected Tag tagLastWeight;
	protected Tag tagLastWeightKg;
	protected Tag tagLastTime;
	
	protected Tag tagState;
	protected Tag tagErrorCode;
	protected Tag tagSetCmd;
	protected Tag tagOutput;
	protected Tag tagOutputKg;
	protected TagRW tagWesSvrState;
	
	protected Tag tagSetCmdCtrl;
	private static final int cmdDisabled = 0;
	private static final int cmdExternal = 1;
	private static final int cmdStop = 2;
	private static final int cmdStart = 3;

	protected static class ExtraTag {
		TagRW tag;
		int regAddr;
		boolean hasWrite = false;
		short writeCnt = 0;
		int writeVal;
	}

//	protected static class Tuple {
//		Tag tagA;
//		Tag tagB;
//		int addr;
//	}


	protected static class EmuTag {
		TagRW tag;
		boolean hasWrite = false;
		int writeVal;

		public EmuTag(TagRW tag) {
			this.tag = tag;
		}
	}

	protected LinkedList<ExtraTag> extraTags = new LinkedList<>();
	protected Tag[] crcTags;
	protected List<EmuTag> emuTags = null;

	protected Tag   tagReplacement;
	private TagRW tagFirmware;

	
	private final int[] buffin = new int[28];
	private final int[] buffout = new int[9];
	
	private boolean paired = false;
	private boolean emusync = false;
	private boolean packer;
	private boolean bitStateConv;

	protected boolean multireq = false;

	private long wesSvrStateTimer;


	private long outWeight;
	private long outTime;
	private boolean outFirstPass;

	private boolean skip20;


	
	
	public PaGeliosPassModule(Plugin plugin, String name) {
		super(plugin, name);
		useSN = true;
	}

	
	@Override
	public boolean loadPeripherialModule(Object conf) {
		Configuration cm = env.getConfiguration();

		paired = cm.get(conf, "paired", false);
		multireq = cm.get(conf, "multireq", false);
		packer = cm.get(conf, "packer", true);
		bitStateConv = cm.get(conf, "bitStateConv", false);
		emusync = cm.get(conf, "emusync", false);
		skip20 = cm.get(conf, "skip20", false);

		tagCrc 				= tagtable.createInt("Crc", 0);
		tagSumWeightHigh1 	= tagtable.createRWInt("SumWeightHigh1", 0, Flags.STATUS);
		tagSumWeightLow1 	= tagtable.createRWInt("SumWeightLow1", 0, Flags.STATUS);
		tagSumNumHigh1 		= tagtable.createRWInt("SumNumHigh1", 0, Flags.STATUS);
		tagSumNumLow1 		= tagtable.createRWInt("SumNumLow1", 0, Flags.STATUS);
		tagSumWeightHigh2 	= tagtable.createRWInt("SumWeightHigh2", 0, Flags.STATUS);
		tagSumWeightLow2 	= tagtable.createRWInt("SumWeightLow2", 0, Flags.STATUS);
		tagSumNumHigh2 		= tagtable.createRWInt("SumNumHigh2", 0, Flags.STATUS);
		tagSumNumLow2 		= tagtable.createRWInt("SumNumLow2", 0, Flags.STATUS);
		tagCurWeightHigh	= tagtable.createRWInt("CurWeightHigh", 0, Flags.STATUS);
		tagCurWeightLow		= tagtable.createRWInt("CurWeightLow", 0, Flags.STATUS);
		tagLastWeightHigh	= tagtable.createRWInt("LastWeightHigh", 0, Flags.STATUS);
		tagLastWeightLow	= tagtable.createRWInt("LastWeightLow", 0, Flags.STATUS);
		tagLastTimeHigh		= tagtable.createRWInt("LastTimeHigh", 0, Flags.STATUS);
		tagLastTimeLow		= tagtable.createRWInt("LastTimeLow", 0, Flags.STATUS);

		tagSumWeightKg1 	= tagtable.createInt("SumWeightKg1", 0, Flags.STATUS);
		tagSumWeightKg2 	= tagtable.createInt("SumWeightKg2", 0, Flags.STATUS);
		tagCurWeight		= tagtable.createInt("CurWeight", 0, Flags.STATUS);
		tagCurWeightKg		= tagtable.createInt("CurWeightKg", 0, Flags.STATUS);
		tagLastWeight		= tagtable.createInt("LastWeight", 0, Flags.STATUS);
		tagLastWeightKg		= tagtable.createInt("LastWeightKg", 0, Flags.STATUS);
		tagLastTime			= tagtable.createInt("LastTime", 0, Flags.STATUS);

		crcTags = new Tag[] {
				tagSumWeightHigh1,
				tagSumWeightLow1,
				tagSumNumHigh1,
				tagSumNumLow1,
				tagSumWeightHigh2,
				tagSumWeightLow2,
				tagSumNumHigh2,
				tagSumNumLow2,
				tagCurWeightHigh,
				tagCurWeightLow,
				tagLastWeightHigh,
				tagLastWeightLow,
				tagLastTimeHigh,
				tagLastTimeLow
		};

		initCrc16Tags();

		initEmuTags();



		tagState			= tagtable.createInt("State", 0, Flags.STATUS);
		tagErrorCode		= tagtable.createInt("ErrorCode", 0, Flags.STATUS);
		tagSetCmd			= tagtable.createInt("SetCmd", 0);
		tagSetCmdCtrl		= tagtable.createInt("SetCmdCtrl", 0, Flags.AUTOSAVE);
		tagOutput			= tagtable.createInt("Output", 0, Flags.STATUS);
		tagOutputKg			= tagtable.createInt("OutputKg", 0, Flags.STATUS);
		
		tagWesSvrState		= tagtable.createRWInt("WesSvrState", 0);
		tagReplacement      = tagtable.createBool("Replacement"   , false, Flags.AUTOSAVE);
		tagFirmware 		= tagtable.createRWString("firmware", "");

		Map<String,Object> confext = cm.toMap( cm.get(conf, "params") );
		for(String key: confext.keySet())
			addExtraTag(key, cm.get(confext, key, 0) );
		
		return true;
	}

	@Override
	protected void initCrc16Tags() {
		super.initCrc16Tags();
		for(Tag tag: crcTags)
			crc16Tags.add( tag );
		crc16Tags.add( tagError );
	}

	private void initEmuTags() {
		if( emulated  &&  emusync ) {
			emuTags = new ArrayList<>();
			for( Tag tag: crcTags)
				emuTags.add( new EmuTag((TagRW)tag) );
		} else
			emuTags = null;
	}


	protected void addExtraTag(String name, int regaddr) {
		ExtraTag et = new ExtraTag();
		et.tag = tagtable.createRWInt(name, 0, Flags.STATUS);
		et.regAddr = regaddr;
		extraTags.add(et);
	}

	
	@Override
	public boolean preparePeripherialModule() {
	    outWeight = 0;
	    outTime = 0;
	    outFirstPass = true;
		return true;
	}

	
	@Override
	public boolean executePeripherialModule() {
		
		boolean result = true;

		if( !emulated ) {
			try {
				buffout[0] = 0x55;
	
				// Write setcmd:
				//55 01 02 78 

				int setcmd = 0;
				switch( tagSetCmdCtrl.getInt() ) {
					case cmdExternal:
						setcmd = tagSetCmd.getInt();
						break;
					case cmdStart:
						setcmd = 1;
						break;
					case cmdStop:
						setcmd = 2;
						break;
				}
				if( setcmd == 1  &&  tagState.getInt() != 0)
					setcmd = 0;

				if( setcmd == 2  &&  tagState.getInt() == 0)
					setcmd = 0;

				if( setcmd > 0 ) {
					buffout[1] = netaddr;
					buffout[2] = setcmd;
					buffout[3] = CRC.getCrc8(buffout, 1, 2);
					protocol.writeBytes(buffout, 4);
					Thread.sleep(10);
				}


//				if( tagSetCmdCtrl.getInt() == cmdDisabled ) {
//					if( tagSetCmd.hasWriteValue()  ||  tagSetCmd.getInt() != 0)
//						tagSetCmd.setReadValInt(0);
//				} else {
//					int setcmd = -1;
//					switch( tagSetCmdCtrl.getInt() ) {
//						case cmdExternal:
//							if( tagSetCmd.hasWriteValue() ) {
//								setcmd = tagSetCmd.getWriteValInt();
//							}
//							break;
//						case cmdStart:
//							if( tagState.getInt() == 0 )
//								setcmd = 1;
//							break;
//						case cmdStop:
//							if( tagState.getInt() != 0 )
//								setcmd = 2;
//							break;
//					}
//					if( setcmd >= 0 ) {
//						tagSetCmd.setReadValInt(setcmd);
//						buffout[1] = netaddr;
//						buffout[2] = setcmd;
//						buffout[3] = CRC.getCrc8(buffout, 1, 2);
//			            protocol.writeBytes(buffout, 4);
//			            Thread.sleep(10);
//					}
//				}



				// read extra
				if( result ) {
					for(ExtraTag et: extraTags) {
						// Write extra tag:
						//55 81   00 0B   00 00 00 01  0B

						if( et.tag.hasWriteValue() ) {
							et.hasWrite = true;
							et.writeCnt = 0;
							et.writeVal = et.tag.getWriteValInt();
						}

						if( et.hasWrite )
						{
							if( et.writeVal == et.tag.getInt() ) {
								et.hasWrite = false;
							} else {
								buffout[1] = 0x80 + netaddr;
								if( packer ) {
									buffout[2] = (et.regAddr >> 8) & 0xFF;
									buffout[3] = et.regAddr & 0xFF;
									buffout[4] = (et.writeVal >> 24) & 0xFF;
									buffout[5] = (et.writeVal >> 16) & 0xFF;
									buffout[6] = (et.writeVal >> 8) & 0xFF;
									buffout[7] = et.writeVal & 0xFF;
									buffout[8] = CRC.getCrc8(buffout, 1, 7);
									protocol.writeBytes(buffout, 9);
								} else {
									buffout[2] = et.regAddr & 0xFF;
									buffout[3] = (et.writeVal >> 24) & 0xFF;
									buffout[4] = (et.writeVal >> 16) & 0xFF;
									buffout[5] = (et.writeVal >> 8) & 0xFF;
									buffout[6] = et.writeVal & 0xFF;
									buffout[7] = CRC.getCrc8(buffout, 1, 6);
									protocol.writeBytes(buffout, 8);
								}
								Thread.sleep(10);

								if( et.writeCnt++ >= 10 )
									et.hasWrite = false;
							}
						}

						// Read extra tag:
						//55 61 00 0B 2E
						//00 00 00 24
						// read
						buffout[1] = 0x60 + netaddr;
						int sizeout;
						if( packer ) {
							buffout[2] = (et.regAddr >> 8) & 0xFF;
							buffout[3] = et.regAddr & 0xFF;
							buffout[4] = CRC.getCrc8(buffout, 1, 3);
							sizeout = 5;
						} else {
							buffout[2] = et.regAddr & 0xFF;
							buffout[3] = CRC.getCrc8(buffout, 1, 2);
							sizeout = 4;
						}

						if (!multireq)
							result = protocol.request(buffout, sizeout, buffin, 5, ProtocolAA55.Crc8);
						else
							result = protocol.multiRequest(buffout, sizeout, buffin, 5, 5, ProtocolAA55.Crc8);

						if (result) {
							et.tag.setReadValInt(Numbers.bytesToInt(buffin, 0));
						}
					}
				}


				
				// Read main
				//55 21 7D  
				//00 00 00 24  00 4F 4B 8F  00 15 D5 D8  00 00 00 00  00 00 00 00  00    15        8A
				//SumNum       SumWeight    CurWeight    LastWeight   LastTime     State ErrorCode Crc
				if( result  &&  !skip20 ) {
					buffout[1] = 0x20 + netaddr;
					buffout[2] = CRC.getCrc8(buffout, 1, 1);

					if (!multireq)
						result = protocol.request(buffout, 3, buffin, 23, ProtocolAA55.Crc8);
					else {
						result = protocol.multiRequest(buffout, 3, buffin, 23, 8, ProtocolAA55.Crc8);
					}
				}

				if( result ) {
					tagSumNumHigh1.setReadValInt(Numbers.bytesToWord(buffin, 0));
					tagSumNumLow1.setReadValInt(Numbers.bytesToWord(buffin, 2));
					tagSumWeightHigh1.setReadValInt(Numbers.bytesToWord(buffin, 4));
					tagSumWeightLow1.setReadValInt(Numbers.bytesToWord(buffin, 6));
					tagCurWeightHigh.setReadValInt(Numbers.bytesToWord(buffin, 8));
					tagCurWeightLow.setReadValInt(Numbers.bytesToWord(buffin, 10));
					if( paired )
					{
						tagSumNumHigh2.setReadValInt(Numbers.bytesToWord(buffin, 12));
						tagSumNumLow2.setReadValInt(Numbers.bytesToWord(buffin, 14));
						tagSumWeightHigh2.setReadValInt(Numbers.bytesToWord(buffin, 16));
						tagSumWeightLow2.setReadValInt(Numbers.bytesToWord(buffin, 18));
					} else {
						tagLastWeightHigh.setReadValInt(Numbers.bytesToWord(buffin, 12));
						tagLastWeightLow.setReadValInt(Numbers.bytesToWord(buffin, 14));
						tagLastTimeHigh.setReadValInt(Numbers.bytesToWord(buffin, 16));
						tagLastTimeLow.setReadValInt(Numbers.bytesToWord(buffin, 18));
						calcWeightOutput();
					}

					int st = buffin[20];
					if( bitStateConv ) {
						if( (st & 0x40) > 0 )
							st = 2;
						else if( (st & 0x2e) > 0 )
							st = 1;
						else
							st = 0;
					}
					tagState.setInt(st);
					tagErrorCode.setInt(buffin[21]);

					calcTagCrc8(crcTags, tagCrc, buffin);
					result = true;
				}

				if( result  &&  (firstPass  || tagError.getBool())) {
					result = protocol.requestFirmware( tagFirmware );
				}

			} catch (Exception e) {
				env.printError(logger, e, name);
			}
		} else {

			// emulation
			if( emusync  &&  emuTags != null ) {
				boolean fl = true;
				for (EmuTag emutag : emuTags)
					if (emutag.tag.hasWriteValue()) {
						emutag.hasWrite = true;
						emutag.writeVal = emutag.tag.getWriteValInt();
					} else if (!emutag.hasWrite) {
						fl = false;
						break;
					}
				if (fl)
					for (EmuTag emutag : emuTags) {
						emutag.hasWrite = false;
						emutag.tag.setReadValInt(emutag.writeVal);
					}
			} else
				for (Tag tag : crcTags)
					((TagRW)tag).acceptWriteValue();

//			tagSetCmd.acceptWriteValue();
			for(ExtraTag et: extraTags) 
				et.tag.acceptWriteValue();
			
			calcWeightOutput();
			calcTagCrc8(crcTags, tagCrc, buffin);
		}


		// calculated tags
		tagCurWeight.setLong((tagCurWeightHigh.getLong() << 16) + tagCurWeightLow.getLong());
		tagCurWeightKg.setInt( tagCurWeight.getInt() / 1000 );

		tagLastWeight.setLong((tagLastWeightHigh.getLong() << 16) + tagLastWeightLow.getLong() );
		tagLastWeightKg.setInt( tagLastWeight.getInt() / 1000 );

		tagSumWeightKg1.setLong(((tagSumWeightHigh1.getLong() << 16) + tagSumWeightLow1.getInt()) / 1000 );
		tagSumWeightKg2.setLong(((tagSumWeightHigh2.getLong() << 16) + tagSumWeightLow2.getInt()) / 1000 );

		tagLastTime.setLong((tagLastTimeHigh.getLong() << 16) + tagLastTimeLow.getInt());

		wesSvrStateTimer = updateWesSvrState(tagWesSvrState, wesSvrStateTimer);
	
		return result;
	}
	

	
	
	private void calcWeightOutput() {
		int result = 0;
		long W = Numbers.wordsToLong(tagSumWeightHigh1.getInt(), tagSumWeightLow1.getInt());
		long L = Numbers.wordsToLong(tagLastWeightHigh.getInt(), tagLastWeightLow.getInt());
		long T = Numbers.wordsToLong(tagLastTimeHigh.getInt(), tagLastTimeLow.getInt());

		long ms = System.currentTimeMillis();

		if (outFirstPass) {
			outFirstPass = false;
			outWeight = W;
			outTime = ms - T * 3;
		} else

		if (T == 0 || L == 0) {
			outWeight = W;
			outTime = ms;
		} else {

			long tm = ms - outTime;

			if (outWeight != W || tm < T * 3) {

				double d = ((double)L / (double)T) * 3600 * 1000;
				result = (int) Math.round(d);

				if (outWeight != W) {
					outWeight = W;
					outTime = ms;
				}
			} else {
				if (tm > T * 3) {
					outTime = ms - T * 3;
				}
			}
		}
		
		tagOutput.setInt( result );
		tagOutputKg.setInt( result/1000 );
	}
	
	
	
	@Override
	protected boolean reload() {
		PaGeliosPassModule tmp = new PaGeliosPassModule(plugin, name);
		if( !tmp.load() )
			return false;

		copySettingsFrom(tmp);

		multireq = tmp.multireq;
		paired = tmp.paired;
		packer = tmp.packer;
		bitStateConv = tmp.bitStateConv;
		emusync = tmp.emusync;
		initEmuTags();

		//TODO extratags!!!

		
		return true;
	}
	
	



}

