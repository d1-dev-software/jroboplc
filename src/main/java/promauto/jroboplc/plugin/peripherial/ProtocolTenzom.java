package promauto.jroboplc.plugin.peripherial;

public class ProtocolTenzom {
    private final PeripherialModule module;

    public int[] buffout = new int[16];
    public int[] buffin = new int[128];
    public int error;


    public ProtocolTenzom(PeripherialModule module) {
        this.module = module;
    }

    public int getCrc(int[] buff, int beg, int end) {
        int tmp = buff[++end];
        buff[end] = 0;
        int crc = 0;
        boolean flag1, flag2;
        for( int i=beg; i<=end; ++i ) {
            int b = buff[i];
            for(int j=0; j<8; ++j) {
                flag1 = (b & 0x80) > 0;
                b <<= 1;

                flag2 = (crc & 0x80) > 0;
                crc = (crc << 1) & 0xFF;


                if( flag1 )
                    crc += 1;

                if( flag2 )
                    crc ^= 0x69;
            }
        }
        buff[end] = tmp;
        return crc;
    }



    public long getBCDValue(int beg, int len) {
        long val = 0;
        for(int i=beg+len-1; i>=beg; --i)
            val = val*100 + (buffin[i]>>4)*10 + (buffin[i] & 0x0F);
        return val;
    }


    @Deprecated
    public boolean requestA(int cmd, int data1) throws Exception {
        buffout[3] = data1;
        return requestA1(cmd, 1);
    }


    @Deprecated
    private boolean requestA1(int cmd, int datasize) throws Exception {

        buffout[0] = 0xFF;
        buffout[1] = module.netaddr;
        buffout[2] = cmd;
        buffout[datasize+3] = getCrc(buffout, 1, datasize+2);
        buffout[datasize+4] = 0xFF;
        buffout[datasize+5] = 0xFF;
        int sizeout = datasize+6;

        // replace FF with FF FE
        for(int i=datasize+3; i>2; --i)
            if( buffout[i] == 0xFF ) {
                for(int j=sizeout-1; j>i; --j)
                    buffout[j+1] = buffout[j];
                buffout[i+1] = 0xFE;
                sizeout += 1;
            }


        for (int trynum = 0; trynum < module.retrial; trynum++) {
            boolean badcrc = false;
            module.port.discard();
            module.delayBeforeWrite();
            module.port.writeBytes(buffout, sizeout);

            int b = 0;
            int i = -1;
            while( i < buffin.length ) {
                if ((b = module.port.readByte()) < 0)
                    break;

                if( b==0xFF ) {
                    if( i==-1 )
                        i=0;

                    if( i<2 )
                        continue;

                    if( (b = module.port.readByte()) < 0 )
                        break;

                    if( b == 0xFF ) {
                        if( getCrc(buffin, 0, i-1) != 0 ) {
                            badcrc = true;
                            break;
                        }

                        if( buffin[0] != module.netaddr )
                            break;

                        return true;

                    } else if( b == 0xFE ) {
                        b = 0xFF;
                    } else
                        break;
                }

                if( i < buffin.length ) {
                    buffin[i++] = b;
                } else {
                    break;
                }
            }

            if( module.canLogError() )
                module.logError(trynum, badcrc, buffout, datasize, buffin, i, "last byte = " + b);

            module.delayAfterError();
        }
        module.tagErrorCnt.setInt( module.tagErrorCnt.getInt() + 1 );

        return false;
    }



    public boolean requestCounters(int size) throws Exception {
        buffout[0] = 0xFF;
        buffout[1] = module.netaddr;
        buffout[2] = 0xC8;
        buffout[3] = size + 0x81;
        int sizeout = prepareBuffout(4);
        return request(sizeout, (size+1)*5+4);
    }

    public boolean requestValue(int cop, int len) throws Exception {
        buffout[0] = 0xFF;
        buffout[1] = module.netaddr;
        buffout[2] = cop;
        int sizeout = prepareBuffout(3);
        return request(sizeout, 3 + len);
    }

    private int prepareBuffout(int pos) {
        buffout[pos] = getCrc(buffout, 1, pos-1);
        buffout[pos+1] = 0xFF;
        buffout[pos+2] = 0xFF;
        int sizeout = pos+3;

        if( buffout[pos] == 0xFF ) {
            buffout[pos+1] = 0xFE;
            buffout[pos+3] = 0xFF;
            sizeout = pos+4;
        }
        return sizeout;
    }


    private boolean request(int sizeout, int sizein) throws Exception {
        for (int trynum = 0; trynum < module.retrial; trynum++) {
            error = 0;
            module.port.discard();
            module.delayBeforeWrite();
            module.port.writeBytes(buffout, sizeout);

            int b;
            int i = 0;
            boolean gotStartMarker = false;

            // waiting for start marker and address
            while( true ) {
                b = module.port.readByte();
                if( b < 0 ) {
                    gotStartMarker = false;
                    break;
                } else

                if( b == 0xFF ) {
                    gotStartMarker = true;
                } else {
                    if( gotStartMarker  &&  b == module.netaddr ) {
                        break;
                    } else {
                        gotStartMarker = false;
                    }
                }
            }

            if( !gotStartMarker ) {
                // error: no data received for given netaddr
                error = 1;
            } else {
                i = 1;
                buffin[0] = module.netaddr;
                do {
                    b = module.port.readByte();
                    if (b == 0xFF) {
                        b = module.port.readByte();
                        if (b == 0xFF) {
                            // got finish marker

                            if (getCrc(buffin, 0, i - 1) != 0) {
                                // error: bad crc
                                error = 2;
                                break;
                            }

                            if( i < sizein  ) {
                                // error: short answer
                                error = 3;
                                break;
                            }

                            // success:
                            return true;
                        } else if (b == 0xFE) {
                            b = 0xFF;
                        } else {
                            // error: received invalid byte after 0xFF
                            error = 4;
                            break;
                        }
                    }

                    buffin[i++] = b;
                } while (b >= 0 && i < buffin.length);
            }

            if( module.canLogError() )
                module.logError(trynum, error == 1, buffout, sizeout, buffin, i, "error = " + error + ", lastbyte" + b);

            module.delayAfterError();
        }
        module.tagErrorCnt.setInt( module.tagErrorCnt.getInt() + 1 );

        return false;
    }

}


