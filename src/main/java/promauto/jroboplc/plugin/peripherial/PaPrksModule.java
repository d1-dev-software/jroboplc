package promauto.jroboplc.plugin.peripherial;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import promauto.jroboplc.core.api.Configuration;
import promauto.jroboplc.core.api.Flags;
import promauto.jroboplc.core.api.Plugin;
import promauto.jroboplc.core.api.Tag;
import promauto.jroboplc.core.tags.TagRW;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PaPrksModule extends PeripherialModule {
	private final Logger logger = LoggerFactory.getLogger(PaPrksModule.class);

	protected ProtocolAA55 protocol = new ProtocolAA55(this);

	private static final int INP_SIZE = 64;
	
	private static final int F_OFSET = 16;
	private static final float DELITEL = 4;
	private static final int F_DIVISOR = 4;
	private static final int F_POROG = 20;

	protected Tag[] inps = new Tag[INP_SIZE];
	protected Tag[] frqs = new Tag[INP_SIZE];
	private int[] tmpBuffin = new int[INP_SIZE];
	private int[] buffin = new int[17];
	private int[] buffout = new int[2];

	private TagRW tagFirmware;
	private Tag tagResetCnt;
	private int resetCntPeriod;
	private boolean canUseCmdA0;
	private boolean noFreq;


	public PaPrksModule(Plugin plugin, String name) {
		super(plugin, name);
	}




	@Override
	public boolean loadPeripherialModule(Object conf) {
		Configuration cm = env.getConfiguration();
		resetCntPeriod = cm.get(conf, "resetCntPeriod_s", 0) * 1000;
		noFreq = cm.get(conf, "noFreq", false);

		for (int i = 0; i < INP_SIZE; i++) {
			inps[i] = tagtable.createInt(((i < 10) ? "inp0" : "inp") + i, 0, Flags.STATUS);
			if( !noFreq )
  				frqs[i] = tagtable.createInt(((i < 10) ? "frq0" : "frq") + i, 0, Flags.STATUS);
		}

		tagFirmware = tagtable.createRWString("firmware", "");
		tagResetCnt = tagtable.createInt("resetcnt", 0);

		return true;
	}


	@Override
	protected void initChannelMap(List<String> chtags) {
		for( int i=0; i<INP_SIZE; ++i ) {
			addChannelMapTag(chtags, inps[i], "" + i);
			if(!noFreq)
				addChannelMapTag(chtags, frqs[i], i + "a");
		}
	}


	@Override
	public boolean executePeripherialModule() {
		
		if(emulated) 
			return true;

		boolean result = true;
		try {
			if( firstPass  ||  tagError.getBool() ) {
//				pRKS 3.3
				result = protocol.requestFirmware( tagFirmware );
				canUseCmdA0 = false;
				if( result ) {
					Pattern p = Pattern.compile("pRKS (\\d+)\\.(\\d+)");
					Matcher m = p.matcher(tagFirmware.getString());
					if( m.matches() ) {
						int ver = Integer.parseInt(m.group(1)) * 100 + Integer.parseInt(m.group(2));
						canUseCmdA0 = ver >= 3_03;
					}
				}

//				if( !result ) {
//					System.out.println(name + " prks firmware fail");
//					Thread.sleep(50);
//					int b;
//					while( (b = port.readByte()) >= 0 ) {
//						System.out.println("b = " + b);
//					}
//				}
			}

			// todo Refactor it! Make pdat extending prks or vice versa!

			if( result ) {
				if (noFreq && canUseCmdA0) {
					buffout[0] = 0x55;

					// request logic data
					buffout[1] = 0xA0 + netaddr;
					result = protocol.request(buffout, 2, buffin, 9, ProtocolAA55.CrcAA55);
					if( result ) {
						int mask = 1;
						for (int i = 0; i < INP_SIZE; i++, mask <<= 1) {
							if( mask > 0x80)
								mask = 1;
							tmpBuffin[i] = (buffin[i / 8] & mask) > 0? 1: 0;
						}

						for( int i = 0; i < INP_SIZE; i++) {
							inps[i].setInt( tmpBuffin[ ProtocolAA55.pRksDatConv[i] ] );
						}
					}

				} else {
					int cmd = 0x20;
					int fstPos = 0;
					int lstPos = 16;

					buffout[0] = 0x55;
					buffout[1] = cmd + netaddr;
					for (int pos = 1; pos < 5; pos++) {
						lstPos = 16 * pos;
						if (protocol.request(buffout, 2, buffin, 17, ProtocolAA55.CrcAA55)) {
							for (int i = fstPos, j = 0; i < lstPos; i++, j++) {
								tmpBuffin[i] = buffin[j];
							}
						} else {
//							System.out.println(name + " prks cmd fail");
//							Thread.sleep(50);
//							int b;
//							while( (b = port.readByte()) >= 0 ) {
//								System.out.println(name + " b = " + b);
//							}

							result = false;
							break;
						}


						cmd += 0x20;
						buffout[1] = cmd + netaddr;
						fstPos = 16 * pos;
					}
					setInputsProcs();
				}
			}

			result = result & protocol.requestResetCnt( tagResetCnt, resetCntPeriod);

		} catch (Exception e) {
			env.printError(logger, e, name);
		}
		

		return result;
	}
	
	
	private void setInputsProcs() {
		int frq;
		for (int i = 0; i < INP_SIZE; i++) {
			inps[i].setInt( (tmpBuffin[ProtocolAA55.pRksDatConv[i]] & 0x80) > 0? 1: 0 );

			if( !noFreq ) {
				frq = (tmpBuffin[(ProtocolAA55.pRksDatConv[i])]) & 0x7F;
				frq = Math.round((float) ((frq <= F_POROG) ? frq : (frq - F_OFSET) * F_DIVISOR) / DELITEL);
				frqs[i].setInt(frq);
			}
		}
	}


	@Override
	protected boolean reload() {
		PaPrksModule tmp = new PaPrksModule(plugin, name);
		if (!tmp.load() || !tmp.prepare())
			return false;

		copySettingsFrom(tmp);

		if( !noFreq  &&  tmp.noFreq )
			for (Tag tag : frqs) {
				tagtable.remove(tag);
			}

		if( noFreq  &&  !tmp.noFreq )
			for( int i=0; i < frqs.length; ++i) {
				frqs[i] = tmp.frqs[i];
				tagtable.add(frqs[i]);
			}

		noFreq = tmp.noFreq;
		resetCntPeriod = tmp.resetCntPeriod;
		return true;
	}


}
