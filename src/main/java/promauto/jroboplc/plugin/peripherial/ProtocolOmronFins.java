package promauto.jroboplc.plugin.peripherial;

import promauto.utils.Numbers;


public class ProtocolOmronFins {

    private static final int BUFFIN_STATUS_OFFSET = 2;
    private static final int BUFFIN_DATA_OFFSET = 4;

    public int[] buffin = new int[0];
    public int[] buffout = new int[0];

    private int answerStatus = 0;
    private PeripherialModule module;


    @FunctionalInterface
    public interface SetDataBuffoutFunction {
        int apply(int tagidx, int pos);
    }


    public ProtocolOmronFins(PeripherialModule module) {
        this.module = module;
    }

    public static int getElementSize(int r) {
        if(
               ((r >= 0x30) && (r <= 0x33))
            ||  (r == 0x02)
            || ((r >= 0x20) && (r <= 0x2C))
            ||  (r == 0x06)
            ||  (r == 0x07)
        )
            return 1;

        if(
               ((r >= 0xB0) && (r <= 0xB3))
            ||  (r == 0x82)
            || ((r >= 0xA0) && (r <= 0xAC))
            ||  (r == 0x98)
            ||  (r == 0x07)
        )
            return 2;

        return 0;
    }



    // answer status
    public boolean isAnswerStatusOk() {
        return buffin.length >= BUFFIN_STATUS_OFFSET + 2
                && buffin[BUFFIN_STATUS_OFFSET]==0
                && buffin[BUFFIN_STATUS_OFFSET+1]==0;
    }

    private void updateLastBadAnswerStatus() {
        if (answerStatus == 0  &&  buffin.length >= BUFFIN_STATUS_OFFSET + 2 )
            answerStatus = Numbers.bytesToWord( buffin, BUFFIN_STATUS_OFFSET);
    }

    public int getLastBadAnswerStatus() {
        return answerStatus;
    }

    public void resetLastBadAnswerStatus() {
        answerStatus = 0;
    }



    // data getters
    public int getDataByte(int pos) {
        return buffin[pos + BUFFIN_DATA_OFFSET];
    }

    public int getDataWord(int pos) {
        return Numbers.bytesToWord( buffin, pos + BUFFIN_DATA_OFFSET);
    }

    public int getAnswerInt(int pos) {
        return Numbers.bytesToInt( buffin, pos + BUFFIN_DATA_OFFSET);
    }



    // buff adjasters
    public void adjustBuffout(int size) {
        if( buffout.length < size )
            buffout = new int[size];
    }

    public void adjustBuffin(int size) {
        if( buffin.length < size )
            buffin = new int[size];
    }






    // request:
    // 80 00 03 00 00 00 00 09 00 ZZ    01 01  B0  0C E4  00   00 02
    //          header                   cmd  reg   addr  bit   num
    //
    // answer:
    // C0 00 02 00 09 00 00 00 00 ZZ    01 01  00 00    0F 0C  00 00
    //          header                   cmd   status   data   data

    public boolean request0101(int region, int elementsize, int addressbit, int num) throws Exception {
        adjustBuffout( 8 );

        buffout[0] = 1;
        buffout[1] = 1;

        buffout[2] = region;

        buffout[3] = (addressbit >> 12) & 0xFF;
        buffout[4] = (addressbit >> 4) & 0xFF;
        buffout[5] = addressbit & 0xF;

        buffout[6] = (num >> 8) & 0xFF;
        buffout[7] = num  & 0xFF;

        return request(8, elementsize * num + 4);
    }


    // request:
    // 80 00 03 00 00 00 00 09 00 ZZ   01 02   B0   0C 80 00   00 01   00 FF
    //           header                 cmd    reg   addr bit  num     data
    //
    // answer:
    // C0 00 02 00 09 00 00 00 00 ZZ    01 02   00 00
    //          header                   cmd   status
    public boolean request0102(int region, int elementsize, int addressbit, int num,
                               int begidx, int lastidx, SetDataBuffoutFunction setdata) throws Exception {
        int sizeout = elementsize * num + 8;
        adjustBuffout( sizeout );

        buffout[0] = 1;
        buffout[1] = 2;

        buffout[2] = region;

        buffout[3] = (addressbit >> 12) & 0xFF;
        buffout[4] = (addressbit >> 4) & 0xFF;
        buffout[5] = addressbit & 0xF;

        buffout[6] = (num >> 8) & 0xFF;
        buffout[7] = num  & 0xFF;

        int k=8;
        for(int i=begidx; i<=lastidx; ++i)
            k += setdata.apply(i, k);

        return request(sizeout, 4);
    }


    // request:
    // 80 00 03 00 00 00 00 09 00 ZZ    01 04    B0  0C E4 00     B0  0C E4 00
    //          header                   cmd    reg  addr  bit   reg  addr  bit
    //
    // answer:
    // C0 00 02 00 09 00 00 00 00 ZZ    01 04  00 00    B0  0F 0C   B0  0F 0C
    //          header                   cmd   status   reg data    reg data
    public boolean request0104(int num, int datasizein, int begidx, int lastidx, SetDataBuffoutFunction setdata) throws Exception {
        int sizeout = num * 4 + 2;
        adjustBuffout( sizeout );

        buffout[0] = 1;
        buffout[1] = 4;

        int k=2;
        for(int i=begidx; i<=lastidx; ++i)
            k += setdata.apply(i, k);

        return request(sizeout, datasizein + 4);
    }



    public boolean request(int sizeout, int sizein) throws Exception {
        adjustBuffin( sizein );

        for (int trynum = 0; trynum < module.retrial; trynum++) {
            module.port.discard();
            module.delayBeforeWrite();
            module.port.writeBytes(buffout, sizeout);
            int cntRead = module.port.readBytes(buffin, sizein);

            if( cntRead == sizein ) {
                updateLastBadAnswerStatus();
                return true;
            }

            if( module.canLogError() )
                module.logError(trynum, false, buffout, sizeout, buffin, cntRead, "inp len expected = " + sizein);

            module.delayAfterError();
        }

        module.tagErrorCnt.setInt( module.tagErrorCnt.getInt() + 1 );

        return false;
    }

}
