package promauto.jroboplc.plugin.peripherial;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import promauto.jroboplc.core.api.Flags;
import promauto.jroboplc.core.api.Plugin;
import promauto.jroboplc.core.api.Tag;
import promauto.jroboplc.core.tags.TagRW;
import promauto.utils.CRC;
import promauto.utils.Numbers;

public class NotisA100Module extends PeripherialModule {
	private final Logger logger = LoggerFactory.getLogger(NotisA100Module.class);

	private static int BUFF_SIZE = 12;
	protected int[] buffin = new int[BUFF_SIZE];
	protected int[] buffout = new int[BUFF_SIZE];
	
	protected Tag tagCrc;

	protected Tag tagSumWeight;       
	protected Tag tagSumNum;     
	protected Tag tagState;     
	              
	protected Tag tagCode;   
	protected Tag tagOutput;   
	
	protected TagRW tagWesSvrState;

	private long wesSvrStateTimer;
    
	
	public NotisA100Module(Plugin plugin, String name) {
		super(plugin, name);
	}


	@Override
	public boolean loadPeripherialModule(Object conf) {

		tagCrc 			= tagtable.createInt("Crc",	0);
		
		tagSumWeight    = tagtable.createInt("SumWeight", 0, Flags.STATUS);
		tagSumNum       = tagtable.createInt("SumNum", 0, Flags.STATUS);
                       
		tagState        = tagtable.createInt("State", 0, Flags.STATUS);
		tagCode         = tagtable.createInt("Code", 0, Flags.STATUS);
		          
		tagOutput       = tagtable.createInt("Output", 0, Flags.STATUS);
		             
		tagWesSvrState  = tagtable.createRWInt("WesSvrState",  0);
		   
		return true;
	}


//  Out: 81 00 18 14 00 00 00 00 00 00 00 2A
//  In:  80 01 18 14 22 04 63 19 1D 2F 00 2E

//  Out: 81 00 18 13 00 00 00 00 00 00 00 63
//  In : 80 01 18 13 20 08 54 00 00 00 02 45
	
	@Override
	public boolean executePeripherialModule() {
		
		boolean result = true;
		
		if( !emulated ) {
			try {
				if( result = request(0x14) ) {
					tagState		.setInt( buffin[4] );		
					tagCode      	.setInt( buffin[5] );
					tagSumWeight   	.setInt( Numbers.bytesToIntReverse( buffin, 6) );
				}

				if( result )
					if( result = request(0x13) ) {
						tagSumNum  	.setInt( Numbers.bytesToIntReverse( buffin, 6) );
					}
				
			} catch (Exception e) {
				env.printError(logger, e, name);
				result = false;
			}
		}
		
		String crcstr = tagSumWeight.getString() + ';' + tagSumNum.getString();
		tagCrc.setInt( CRC.getCrc8(crcstr) );
		
		wesSvrStateTimer = updateWesSvrState(tagWesSvrState, wesSvrStateTimer);
		
		return result;
	}
	


	private boolean request(int param) throws Exception {
		if( port == null )
			return false;
		
		buffout[0] = 0x80 + netaddr;
		buffout[1] = 0;
		buffout[2] = 0x18;
		buffout[3] = param;
		buffout[4] = 0;
		buffout[5] = 0;
		buffout[6] = 0;
		buffout[7] = 0;
		buffout[8] = 0;
		buffout[9] = 0;
		buffout[10] = 0;
		buffout[11] = calcCrc(buffout, 11);
		
		for (int trynum = 0; trynum < retrial; trynum++) {
			
			port.discard();
			port.writeBytes(buffout, BUFF_SIZE);
			int n = port.readBytes(buffin, BUFF_SIZE);
			
			if( n == BUFF_SIZE  &&  buffin[11] == calcCrc(buffin, 11) ) {
				
				int highbits = buffin[10];
				for( int i=3; i<11; ++i ) {
					buffin[i] |= (highbits & 1) * 0x80;
					highbits >>= 1;
				}
				
				return true;
			}

			try {
				Thread.sleep(20);
			} catch (InterruptedException e) {
			}
			
		}
		tagErrorCnt.setInt( tagErrorCnt.getInt() + 1 );

    	return false;
	}
	
	
	
	private int calcCrc(int[] data, int size) {
		int crc = 0;
		
		for(int i = 0; i < size; i++) {  
		    int b = data[i] & 0xff;
		    for(int j=0; j<8; ++j) {
		      crc = (crc << 1) & 0xff;

		      if( (b & 1) > 0 )
		        crc = crc + 1;

		      if( (crc & 0x80) > 0 )
		        crc = crc ^ 9;

		      b = (b >> 1) & 0xff;
		    }
		}
		return crc & 0x7F;
	}


}
