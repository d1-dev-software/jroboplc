package promauto.jroboplc.plugin.peripherial;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import promauto.jroboplc.core.AbstractModule;
import promauto.jroboplc.core.AbstractPlugin;
import promauto.jroboplc.core.api.Module;

public class PeripherialPlugin extends AbstractPlugin {
    private static final String PLUGIN_NAME = "peripherial";
    private final Logger logger = LoggerFactory.getLogger(PeripherialPlugin.class);

    @Override
    public void initialize() {
        super.initialize();
    }

    @Override
    public String getPluginName() {
        return PLUGIN_NAME;
    }

    @Override
    public String getPluginDescription() {
        return "peripherial serial I/O devices";
    }


    @Override
    public Module createModule(String name, Object conf) {
        Module m = createPeripherialModule(name, conf);
        if (m != null)
            modules.add(m);
        return m;

    }

    public Module createPeripherialModule(String name, Object conf) {
        String modtype = env.getConfiguration().get(conf, "type", "");

        AbstractModule m = null;
        switch (modtype) {
            case "buhler.fbal":
                m = new BuhlerFBALModule(this, name);
                break;

            // promauto
            case "promauto.pdio":
                m = new PaPdioModule(this, name);
                break;

            case "promauto.pdiovlv":
                m = new PaPdioVlvModule(this, name);
                break;

            case "promauto.pdionb":
                m = new PaPdioNbModule(this, name);
                break;

            case "promauto.prks":
                m = new PaPrksModule(this, name);
                break;

            case "promauto.pdat":
                m = new PaPdatModule(this, name);
                break;

            case "promauto.gelios.pass":
                m = new PaGeliosPassModule(this, name);
                break;

            case "promauto.gelios.flow":
                m = new PaGeliosFlowModule(this, name);
                break;

            case "promauto.gelios.maslo2v2":
                m = new PaGeliosMaslo2v2Module(this, name);
                break;

            case "promauto.gelios.dozkkmc":
                m = new PaGeliosDozkkmcModule(this, name);
                break;

            case "promauto.gelios.dozman":
                m = new PaGeliosDozmanModule(this, name);
                break;

            case "promauto.gelios.multipacker":
                m = new PaGeliosMultiPackerModule(this, name);
                break;

            case "promauto.termo5":
                m = new PaTermo5Module(this, name);
                break;

            case "promauto.robo":
                m = new PaRoboModule(this, name);
                break;

            case "promauto.rfidreader":
                m = new PaRFIDReaderModule(this, name);
                break;

            case "promauto.mpt":
                m = new PaMPTModule(this, name);
                break;

            case "promauto.upsnano":
                m = new UpsNanoModule(this, name);
                break;


            case "hygrostek.da301":
                m = new HygrostekDA301Module(this, name);
                break;

            case "icpcon.i7017":
                m = new I7017Module(this, name);
                break;

            case "icpcon.i7024":
                m = new I7024Module(this, name);
                break;

            case "owen.trm200":
                m = new OwenTrm200Module(this, name);
                break;

            case "owen.trm210":
                m = new OwenTrm210Module(this, name);
                break;

            case "owen.trm212":
                m = new OwenTrm212Module(this, name);
                break;

            case "owen.trm138":
                m = new OwenTrm138Module(this, name);
                break;

            case "owen.ma110-1t":
                m = new OwenMa110_1tModule(this, name);
                break;

            case "owen.mu110-8i":
                m = new OwenMu110_8iModule(this, name);
                break;

            case "owen.mu110-16r":
                m = new OwenMu110_16rModule(this, name);
                break;

            case "owen.mv110-8a":
                m = new OwenMv110_8aModule(this, name);
                break;

            case "owen.mv110-16d":
                m = new OwenMv110_16dModule(this, name);
                break;

            case "omron.v7":
                m = new OmronV7Module(this, name);
                break;

            case "schule.flow":
                m = new SchuleFlowModule(this, name);
                break;

            case "notis.a100":
                m = new NotisA100Module(this, name);
                break;

            case "schneider.atv31":
                m = new SchneiderAtv31Module(this, name);
                break;

            case "innovert.isd":
                m = new InnoVertISDModule(this, name);
                break;

            case "modbus.ig5": // old name, will be removed
            case "lsis.sv-ig5a":
                m = new LsisSvIg5aModule(this, name);
                break;

            case "delta.vfdm":
                m = new DeltaVFDMModule(this, name);
                break;

            case "modbus":
                m = new ModbusModule(this, name);
                break;

            case "idsdrive.ecz":
                m = new IdsDriveEczModule(this, name);
                break;

            case "mercury.m230":
                m = new MercuryM230Module(this, name);
                break;

            case "kontakt1.tur01":
                m = new Kontakt1Tur01Module(this, name);
                break;

            case "tenzom.tb09":
                m = new TenzomTb09Module(this, name);
                break;

            case "tenzom":
                m = new TenzomModule(this, name);
                break;

            case "omron.fins":
                m = new OmronFinsModule(this, name);
                break;

            case "wessvr.adapter":
                m = new WessvrAdapterModule(this, name);
                break;

            case "akkont.pass":
                m = new AkkontPassModule(this, name);
                break;

            case "termo.mux":
                m = new TermoMuxModule(this, name);
                break;

            case "korenn.pass":
                m = new KorennPassModule(this, name);
                break;

            case "owen.pvt100":
                m = new OwenPVT100Module(this, name);
                break;

            case "kast.bot01":
                m = new KastBOT01Module(this, name);
                break;

            case "lsis.sv-ip5a":
                m = new LsisSvIp5aModule(this, name);
                break;

            case "tvh.pulsarm":
                m = new TvhPulsarmModule(this, name);
                break;

            case "centa.urz":
                m = new CentaUrzModule(this, name);
                break;

            case "easydrive.ed3100":
                m = new EasyDriveED3100Module(this, name);
                break;

            case "kipr.bos1":
                m = new KiprBos1Module(this, name);
                break;



            default:
                env.printError(logger, name, "Unknown module type:", modtype);
                return null;
        }

        if (!m.load(conf))
            return null;

        return m;
    }


}
