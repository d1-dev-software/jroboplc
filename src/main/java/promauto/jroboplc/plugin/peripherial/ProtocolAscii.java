package promauto.jroboplc.plugin.peripherial;

import promauto.utils.CRC;
import promauto.utils.Numbers;

import java.nio.charset.Charset;
import java.util.function.Function;

public class ProtocolAscii {
	private static final Charset CHARSET_WIN1251 = Charset.forName("windows-1251");
//	public Charset charset = Charset.forName("UTF-8");
	public interface CheckAnswerFunc extends Function<Integer,Boolean> {};

	private PeripherialModule module;
	protected int[] buffin = null;
	protected int[] buffout = null;


	public ProtocolAscii(PeripherialModule module) {
		this.module = module;
	}


	public long getAnswerDWord(int idx) {
		return Numbers.asciiDWordToLong(buffin, idx);
	}


	public void adjustBuffers(int sizeout, int sizein) {
		if(buffout == null  ||  buffout.length < sizeout)
			buffout = new int[sizeout];
		
		if(buffin == null  ||  buffin.length < sizein)
			buffin = new int[sizein];
	}


	public boolean request(int sizeout, int sizein, CheckAnswerFunc checkAnswer) throws Exception {
		int b = 0;
		for (int trynum = 0; trynum < module.retrial; trynum++) {
			module.port.discard();
			module.delayBeforeWrite();
			module.port.writeBytes(buffout, sizeout);
			
			int cntRead = 0;
			boolean badcrc = false;
			for(int i=0; i<sizein; ++i) {
				if ((b = module.port.readByte()) >= 0) {
					buffin[i] = b;
					cntRead++;
				} else
					break;
			}
			
			if( cntRead == sizein) {
				if (checkAnswer == null  ||  checkAnswer.apply(sizein))
					return true;
				badcrc = true;
			}

			if( module.canLogError() ) {
				byte[] bb = new byte[cntRead];
				for (int i = 0; i < cntRead; ++i)
					bb[i] = (byte) (buffin[i] & 0xff);
				String text = new String(bb, CHARSET_WIN1251);
				module.logError(trynum, badcrc, buffout, sizeout, buffin, cntRead, text);
			}

			module.delayAfterError();
		}
		module.tagErrorCnt.setInt( module.tagErrorCnt.getInt() + 1 );
    	return false;
    }


	/**
	 * Centa URZ
	 */
	private final CheckAnswerFunc checkAnswerCenta = (size) -> {
		boolean res = Numbers.asciiByteToInt(buffin, 1) == module.netaddr;

		int crc1 = calcCrcCenta(buffin, (size - 5) / 2);
		int crc2 = Numbers.asciiByteToInt(buffin,size - 4);
		res &= crc1 == crc2;

		res &= (buffin[size-2] == 13)  &&  (buffin[size-1] == 10);

		return res;
	};

	private int calcCrcCenta(int[] buff, int size) {
		int sum = 0;
		for( int i = 0; i<size; ++i) {
			sum += Numbers.asciiByteToInt(buff, i * 2 + 1);
		}
		return 0x100 - (sum & 0xFF);
	}

	public boolean requestCentaRead(int addr) throws Exception {
		// request:
		// # 01      05  11   0D0A
		//   netaddr cmd addr
		//
		// answer:
		// # 01      12345678 EB  0D0A
		//   netaddr value    crc

		final int sizeout = 9;
		final int sizein = 15;
		adjustBuffers(sizeout, sizein);

		buffout[0] = '#';
		Numbers.intToAsciiByte(module.netaddr, buffout, 1);
		buffout[3] = '0';
		buffout[4] = '5';
		Numbers.intToAsciiByte(addr, buffout, 5);
		buffout[7] = 13;
		buffout[8] = 10;

		return request(sizeout, sizein, checkAnswerCenta);
	}


	public boolean requestCentaWrite(int addr, long value) throws Exception {
		// request:
		// # 01      06  24      00000001 D4  0D0A
		//   netaddr cmd regaddr value    crc
		//
		// answer:
		// # 01      FF  0D0A
		//   netaddr crc

		final int sizeout = 19;
		final int sizein = 7;
		adjustBuffers(sizeout, sizein);

		buffout[0] = '#';
		Numbers.intToAsciiByte(module.netaddr, buffout, 1);
		buffout[3] = '0';
		buffout[4] = '6';
		Numbers.intToAsciiByte(addr, buffout, 5);
		Numbers.longToAsciiDWord(value, buffout, 7);
		int crc = calcCrcCenta(buffout, 7);
		Numbers.intToAsciiByte(crc, buffout, 15);
		buffout[17] = 13;
		buffout[18] = 10;

		return request(sizeout, sizein, checkAnswerCenta);
	}







}
