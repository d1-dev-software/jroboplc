package promauto.jroboplc.plugin.motohr;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import promauto.jroboplc.core.AbstractPlugin;
import promauto.jroboplc.core.api.Module;

public class MotohrPlugin extends AbstractPlugin {
	private static final String PLUGIN_NAME = "motohr";
	private final Logger logger = LoggerFactory.getLogger(MotohrPlugin.class);

	@Override
	public void initialize() {
		super.initialize();
	}

	@Override
	public String getPluginName() {
		return PLUGIN_NAME;
	}

	@Override
	public String getPluginDescription() {
		return "operating hours accounting";
	}


	@Override
	public Module createModule(String name, Object conf) {
    	Module m = createDatabaseModule(name, conf);
    	if( m != null )
    		modules.add(m);
    	return m;
		
	}
	
	private Module createDatabaseModule(String name, Object conf) {
		MotohrModule m = new MotohrModule(this, name);
		modules.add(m);
		return m.load(conf)? m: null;
	}



}
