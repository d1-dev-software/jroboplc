package promauto.jroboplc.plugin.motohr;

import promauto.jroboplc.core.api.Tag;
import promauto.jroboplc.core.tags.TagRW;

import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Map;

public class Mach {
    public static Map<Integer, String> cacheDescrs;

    private int id;
    private final Context ctx;
    public final Tag tag;
    public final String tagname;
    public final String name;
    public MachState state;
    public List<Counter> counters;


    private Counter counterTotal;
    public final TagRW tagDescr;
    public final TagRW tagTime;

    public Mach(Context ctx, Tag tag, String tagname, String name, String descr) {
        this.ctx = ctx;
        this.tag = tag;
        this.tagname = tagname;
        this.name = name;
        state = null;

        tagDescr = ctx.tagtable.createRWString(name + ".descr", descr);
        tagTime = ctx.tagtable.createRWLong(name + ".time", 0L);
    }




    public static boolean executeAll(Context ctx) throws SQLException {
        boolean res = true;
        for(Mach mach: ctx.machs)
            res &= mach.execute();
        return res;
    }

    public int getId() {
        return id;
    }

    public Counter getCounterTotal() {
        return counterTotal;
    }

    public void init() throws SQLException {
        id = ctx.service.syncMach(tagname, name, tagDescr.getString());

        if( tagDescr.getString().isEmpty()  &&  cacheDescrs.containsKey(id) )
            tagDescr.setReadValString( cacheDescrs.get(id) );

        state = MachState.cache.get(id);
        if( state == null ) {
            state = new MachState(ctx.now(), tag.getBool(), 0, 0);
            ctx.service.saveMachState(id, state);
        }
        updateTime();

        for(Counter counter: counters) {
            counter.init();
            if( counter.isTotal() )
                counterTotal = counter;
        }
    }


    public boolean execute() throws SQLException {
        if( tag.getStatus() == Tag.Status.Deleted )
            return false;

        if( tag.getBool() != state.running ) {
            LocalDateTime dtnow = ctx.now();
            state.running = tag.getBool();
            if( state.running ) {
                // mach started
                state.statcnt++;
            } else {
                // mach stopped
                int sec = (int)ChronoUnit.SECONDS.between(state.dt, dtnow);

                ctx.service.saveInterval(id, state.dt, dtnow, sec);

                for(Counter counter: counters)
                    counter.addSeconds(sec);
            }
            state.dt = dtnow;
            ctx.service.saveMachState(id, state);
            updateTime();
        }

        for(Counter counter: counters)
            counter.execute();

        if( tagDescr.hasWriteValue() ) {
            tagDescr.copyLastWriteToRead();
            ctx.service.syncMach(tagname, name, tagDescr.getString());
        }

        return true;
    }


    private void updateTime() {
        if(state.running) {
            tagTime.setReadValLong(ctx.convertTime(state.dt));
        } else {
            tagTime.setReadValLong(0);
        }
    }


    public void saveStat(int period) throws SQLException {
        if( counterTotal == null )
            return;

        int totalsec = counterTotal.getValue();
        if( period > 0 ) {
            int statsec = totalsec - state.statsec;
            ctx.service.saveStat(id, period, state.statcnt, statsec);
        }

        state.statcnt = 0;
        state.statsec = totalsec;
        ctx.service.saveMachState(id, state);
    }

    public int getRunningSec() {
        if( state.running )
            return (int)(ctx.tagTime.getLong() - tagTime.getLong());
        else
            return 0;
    }

    public void remove() {
        ctx.tagtable.remove(tagDescr);
        ctx.tagtable.remove(tagTime);
        counters.forEach(Counter::remove);
    }
}


