package promauto.jroboplc.plugin.motohr;

import java.time.LocalDateTime;
import java.util.Map;

public class MachState {
    public static Map<Integer, MachState> cache;

    public LocalDateTime dt;
    public boolean running;
    public int statcnt;
    public int statsec;

    public MachState(LocalDateTime dt, boolean running, int statcnt, int statsec) {
        this.running = running;
        this.dt = dt;
        this.statcnt = statcnt;
        this.statsec = statsec;
    }

}
