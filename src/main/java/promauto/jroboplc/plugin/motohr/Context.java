package promauto.jroboplc.plugin.motohr;

import promauto.jroboplc.core.TagTable;
import promauto.jroboplc.core.api.Configuration;
import promauto.jroboplc.core.api.Environment;
import promauto.jroboplc.core.api.Tag;
import promauto.jroboplc.core.tags.TagRW;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.*;

public class Context {
    public Environment env;
    public Configuration cfg;
    public MotohrModule module;
    public DataService service;
    public Stat stat;
    public SysUtil sysUtil;
    public String moduleName;
    public TagTable tagtable;
    public final Map<String, CounterType> counterTypes;
    public final List<MachGroup> machGroups;
    public final List<Mach> machs;
    public final List<Tag> repoTags;
    public int period;

    public String databaseModuleName;
    public Tag tagConnected;
    public TagRW tagTime;


    public Context() {
        repoTags = new ArrayList<>();
        counterTypes = new HashMap<>();
        machGroups = new LinkedList<>();
        machs = new LinkedList<>();
    }


    public void setModule(MotohrModule module) {
        this.module = module;
        this.moduleName = module.getName();
        this.tagtable = module.getTagTable();
    }


    public void addRepoTags(Tag... tags) {
        Collections.addAll(repoTags, tags);
    }

    public void setDataService(DataService service) {
        this.service = service;
    }


    public void setEnvironment(Environment environment) {
        this.env = environment;
        this.cfg = env.getConfiguration();
    }

    public void setStat(Stat stat) {
        this.stat = stat;
    }

    public void setSysUtil(SysUtil sysUtil) {
        this.sysUtil = sysUtil;
    }


    public LocalDateTime now() {
        if( service.getDb() == null )
            return LocalDateTime.now();
        else
            return service.getDb().getServerDatetime();
    }

    public long convertTime(LocalDateTime dt) {
        return dt.toEpochSecond(ZoneOffset.UTC);
    }

    public void updateTime() {
        tagTime.setReadValLong(convertTime(now()));
    }

}
