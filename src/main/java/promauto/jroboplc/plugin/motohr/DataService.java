package promauto.jroboplc.plugin.motohr;

import javafx.util.Pair;
import promauto.jroboplc.core.api.DatabaseProtoService;

import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.Map;

public interface DataService extends DatabaseProtoService {


    void startSync() throws SQLException;
    void finishSync() throws SQLException;


    int syncCounterType(String name, String descr, boolean hidden) throws SQLException;

    int syncMach(String tagname, String name, String descr) throws SQLException;

    Map<Integer, MachState> getMachStates() throws SQLException;

    void saveMachState(int id, MachState state) throws SQLException;

    Map<Pair<Integer,Integer>, Pair<Integer, Integer>> getCounters() throws SQLException;

    int createCounter(int machId, int counterTypeId) throws SQLException;

    void saveInterval(int id, LocalDateTime dt1, LocalDateTime dt2, int sec) throws SQLException;

    void saveCounter(int id, int sec) throws SQLException;

    void saveStat(int id, int period, int daycnt, int daysec) throws SQLException;

    int sweepInterval(LocalDateTime dt) throws SQLException;
    int sweepStat(int period) throws SQLException;

    int packMach() throws SQLException;

    void syncCounter(int id);

    Map<Integer, String> getMachDescrs() throws SQLException;
}
