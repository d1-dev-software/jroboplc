package promauto.jroboplc.plugin.motohr;


import javafx.util.Pair;
import promauto.jroboplc.core.tags.TagRW;

import java.sql.SQLException;
import java.util.Map;

import static promauto.jroboplc.plugin.motohr.CounterType.TOTAL_NAME;

public class Counter {

    private static final int COUNTER_DISABLED = 0x7fff_ffff; // 2147483647
    public static Map<Pair<Integer,Integer>, Pair<Integer, Integer>> cache;

    private int id;
    private final Context ctx;
    public final CounterType counterType;
    public final Mach mach;
    private final TagRW tagSec;
    private final boolean total;


    public Counter(Context ctx, CounterType counterType, Mach mach) {
        this.ctx = ctx;
        this.counterType = counterType;
        this.mach = mach;

        tagSec = ctx.tagtable.createRWInt(mach.name + ".cnt." + counterType.name, 0);
        total = counterType.name.equals(TOTAL_NAME);
    }


    public void init() throws SQLException {
        Pair<Integer, Integer> found = cache.get(new Pair<>(mach.getId(), counterType.getId()));
        if( found == null ) {
            id = ctx.service.createCounter(mach.getId(), counterType.getId());
            tagSec.setReadValInt( counterType.timer? COUNTER_DISABLED: 0 );
        } else {
            id = found.getKey();
            tagSec.setReadValInt(found.getValue());
            ctx.service.syncCounter(id);
        }
    }


    public void addSeconds(int sec) throws SQLException {
        if( tagSec.getInt() != COUNTER_DISABLED ) {
            tagSec.setReadValInt(tagSec.getInt() + (counterType.timer ? -1 : 1) * sec);
            ctx.service.saveCounter(id, tagSec.getInt());
        }
    }


    public void execute() throws SQLException {
        if( tagSec.hasWriteValue() ) {
            int value = tagSec.getWriteValInt();
            boolean valueDisabled = value == COUNTER_DISABLED;

            if( total  &&  (valueDisabled  ||  mach.state.running))
                return;

            if( !counterType.timer  &&  value != 0  &&  !valueDisabled )
                return;

            if( counterType.timer  &&  value < 0 )
                return;

            if( !valueDisabled )
                value += (counterType.timer? 1: -1) * mach.getRunningSec();

            tagSec.setReadValInt(value);
            ctx.service.saveCounter(id, tagSec.getInt());

            if( total ) {
                for (Counter counter : mach.counters) {
                    if (!counter.total && counter.tagSec.getInt() != COUNTER_DISABLED) {
                        counter.tagSec.setInt(0);
                        counter.execute();
                    }
                }
            }

        }
    }


    public int getValue() {
        return tagSec.getInt() + (counterType.timer? -1: 1) * mach.getRunningSec();
    }


    public boolean isTotal() {
        return total;
    }


    public int getId() {
        return id;
    }

    public void remove() {
        ctx.tagtable.remove(tagSec);
    }
}


