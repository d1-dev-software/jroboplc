package promauto.jroboplc.plugin.motohr;

import promauto.jroboplc.core.api.Configuration;
import promauto.jroboplc.core.api.Tag;
import promauto.jroboplc.core.exceptions.ConfigurationNotValidException;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class MachGroup {
    public static Map<String, Mach> cacheMachByName;
    private final Context ctx;
    public final List<CounterType> counterTypes;
    public final Pattern patternTag;
    public final Pattern patternDescr;
    public final Pattern patternIncludeTag;
    public final Pattern patternExcludeTag;
    public final Pattern patternIncludeDescr;
    public final Pattern patternExcludeDescr;
    public final String prefix;
    public final String replDescr;


    public MachGroup(Context ctx, Object conf) throws ConfigurationNotValidException {
        this.ctx = ctx;
        counterTypes = new LinkedList<>();

        Configuration cm = ctx.env.getConfiguration();
        patternTag = Pattern.compile(cm.get(conf, "expr.tag", ".*\\.MCHB_(.*)_Plata"));
        patternDescr = Pattern.compile(cm.get(conf, "expr.descr", "(.*)(MCHB.*)(_Plata)"));
        replDescr = cm.get(conf, "repl.descr", "$1$2.descr");
        prefix = cm.get(conf, "prefix", "");
        String exprIncludeTag = cm.get(conf, "include.tag", "");
        String exprExcludeTag = cm.get(conf, "exclude.tag", "");
        String exprIncludeDescr = cm.get(conf, "include.descr", "");
        String exprExcludeDescr = cm.get(conf, "exclude.descr", "");

        patternIncludeTag = compilePattern(exprIncludeTag);
        patternExcludeTag = compilePattern(exprExcludeTag);
        patternIncludeDescr = compilePattern(exprIncludeDescr);
        patternExcludeDescr = compilePattern(exprExcludeDescr);


        for (String ctname: cm.getStringList(conf, "counters")) {
            CounterType ct = ctx.counterTypes.get(ctname);
            if( ct == null ) {
                cm.throwConfigurationNotValidException("MachGroup has unknown counter type: " + ctname);
            } else {
                if( !counterTypes.contains(ct) )
                    counterTypes.add(ct);
            }
        }
    }

    private Pattern compilePattern(String expr) {
        if( expr.isEmpty() )
            return null;
        return Pattern.compile(expr);
    }


    public static void initAll(Context ctx) throws SQLException {
        ctx.machs.forEach(Mach::remove);
        ctx.machs.clear();
        cacheMachByName = new HashMap<>();
        MachState.cache = ctx.service.getMachStates();
        Counter.cache = ctx.service.getCounters();
        Mach.cacheDescrs = ctx.service.getMachDescrs();

        for(MachGroup mg: ctx.machGroups)
            mg.init();

        for(Mach mach: ctx.machs)
            mach.init();

        MachState.cache.clear();
        Counter.cache.clear();
        Mach.cacheDescrs.clear();
        cacheMachByName.clear();
    }


    public void init() throws SQLException {
        Map<String, Tag> alltags = ctx.env.getModuleManager().getTagsAll();

        for (String tagname: alltags.keySet()) {
            Matcher matcherTag = patternTag.matcher(tagname);
            if( !matcherTag.find() )
                continue;

            if( !checkPattern(patternIncludeTag, tagname, true) )
                continue;

            if( !checkPattern(patternExcludeTag, tagname, false) )
                continue;

            Tag tag = alltags.get(tagname);
            String name = prefix + matcherTag.group(1);
            String descr = "";

            Matcher matcherDescr = patternDescr.matcher(tagname);
            if( matcherDescr.find() ) {
                String descrTagname = matcherDescr.replaceFirst(replDescr);
                Tag tagDescr = alltags.get(descrTagname);
                if( tagDescr != null )
                    descr = tagDescr.getString();
            }

            if( !checkPattern(patternIncludeDescr, descr, true) )
                continue;

            if( !checkPattern(patternExcludeDescr, descr, false) )
                continue;

            Mach machFound = cacheMachByName.get(name);
            if( machFound == null ) {
                Mach mach = new Mach(ctx, tag, tagname, name, descr);
                mach.counters = counterTypes.stream()
                        .map(ct -> new Counter(ctx, ct, mach))
                        .collect(Collectors.toList());
//                mach.init();

                ctx.machs.add(mach);
                cacheMachByName.put(mach.name, mach);
            } else {
                counterTypes.stream()
                        .filter(ct -> machFound.counters.stream().noneMatch(cnt -> cnt.counterType == ct))
                        .forEach(ct -> machFound.counters.add( new Counter(ctx, ct, machFound) ));
            }
        }
    }

    private boolean checkPattern(Pattern pattern, String text, boolean match) {
        return pattern == null  ||  pattern.matcher(text).matches() == match;
    }

}

