package promauto.jroboplc.plugin.motohr;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import promauto.jroboplc.core.AbstractModule;
import promauto.jroboplc.core.api.*;

import java.sql.Statement;

import static promauto.jroboplc.core.api.Signal.SignalType.*;

public class MotohrModule extends AbstractModule implements Signal.Listener {
	private final Logger logger = LoggerFactory.getLogger(MotohrModule.class);

	public static final String DBSCR_MOTOHR = "dbscr/dbscr.motohr.yml";
	public static final String MOTOHR_INIT_1 = "motohr.init1";

	public static final String TABLE_REPO = "MH_REPO";

	private TagRepository tagrepo;
	private Database database;
	private boolean needInit;


	private Context ctx;

	public MotohrModule(Plugin plugin, String name) {
        super(plugin, name);
		ctx = new Context();
		ctx.setModule(this);
		ctx.setDataService( new DataServiceImpl() );
		ctx.setEnvironment( EnvironmentInst.get() );
		ctx.setStat( new Stat(ctx) );
		ctx.setSysUtil( new SysUtil(ctx) );
	}




	public final boolean loadModule(Object conf) {
		Configuration cm = env.getConfiguration();
		ctx.databaseModuleName = cm.get(conf, "database", "db");

		try {
			// load counter types
			ctx.counterTypes.put(
					CounterType.TOTAL_NAME,
					CounterType.createCounterTypeTotal(ctx));

			for (Object confCounterType : cm.toList(cm.get(conf, "counters"))) {
				CounterType ct = new CounterType(ctx, confCounterType);
				if( !ctx.counterTypes.containsKey(ct.name) )
					ctx.counterTypes.put(ct.name, ct);
			}

			// load machine groups
			for (Object confMachGroup : cm.toList(cm.get(conf, "machines"))) {
				ctx.machGroups.add(new MachGroup(ctx, confMachGroup));
			}

			// load ctx
			ctx.stat.load();
			ctx.sysUtil.load();

		} catch (Exception e) {
			env.printError(logger, e, name);
			return false;
		}

		ctx.tagConnected = tagtable.createBool("connected", false);
		ctx.tagTime = tagtable.createRWLong("system.time", 0L);
		return true;
	}


	@Override
	public final boolean prepareModule() {
		database = env.getModuleManager().getModule(ctx.databaseModuleName, Database.class);
		if (database == null) {
			env.printError(logger, name, "Database not found:", ctx.databaseModuleName);
			return false;
		}

		ctx.service.setDb(database);
		tagrepo = database.createTagRepository("", TABLE_REPO);

		addAsSignalListerToAllOthers();

		boolean res = database.loadScriptFromResource(MotohrPlugin.class, DBSCR_MOTOHR) != null;
		needInit = res;
		return res;
	}


	@Override
	public void onSignal(Module sender, Signal signal) {
		if (signal.type == CONNECTED  &&  sender == database  &&  !ctx.tagConnected.getBool()) {
			needInit = true;
		} else

		if (signal.type == DISCONNECTED  &&  sender == database) {
			ctx.tagConnected.setBool(false);
		} else

		if (signal.type == RELOADED) {
			needInit = true;
		}
	}


	private void init() {
		ctx.tagConnected.setBool( false );
		if(database == null  ||  !database.isConnected())
			return;

	 	boolean res =
				database.executeScript(MOTOHR_INIT_1).success;

		if( res ) {
			DataService svc = ctx.service;
			try (Statement st = svc.createStatement()) {
				svc.startSync();
				res = tagrepo.init();

				CounterType.initAll(ctx);
				MachGroup.initAll(ctx);

				ctx.stat.init();

				if( res )
					tagrepo.load(name, ctx.repoTags);

				svc.finishSync();
				svc.commit();

				postSignal(RELOADED);
			} catch (Exception e) {
				env.printError(logger, e, name);
				svc.rollback();
			}
		}

		if( !res )
			env.printError(logger, name, "Initialization error");

		ctx.tagConnected.setBool(res);
		needInit = false;
	}


	@Override
	public boolean executeModule() {
		if( database == null  ||  !database.isConnected())
			return true;

		if( needInit )
			init();

		if( ctx.tagConnected.getBool() ) {
			try(Statement st = ctx.service.createStatement()) {

				ctx.updateTime();

				if( !Mach.executeAll(ctx) )
					needInit = true;

				ctx.stat.execute();

				tagrepo.save(name, ctx.repoTags);
				tagrepo.persist();

				ctx.sysUtil.execute();

				ctx.service.commit();
			} catch (Exception e) {
				env.printError(logger, e, name, ", Last sql =", ctx.service.getLastSql());
				ctx.service.rollback();
				ctx.tagConnected.setBool( false );
				needInit = true;
			}
		}
		return true;
	}


	@Override
	public boolean closedownModule() {
		ctx.tagConnected.setBool(false);
		removeAsSignalListerFromAllOthers();
		return true;
	}


	@Override
	public String getInfo() {
		if( !enable )
			return "disabled";

		return String.format("%s%s machs=%d counters/types=%d/%d",
				ctx.tagConnected.getBool()? "": ANSI.redBold("NOT CONNECTED! "),
				ctx.databaseModuleName,
				ctx.machs.size(),
				ctx.machs.stream().mapToInt(mach -> mach.counters.size()).sum(),
				ctx.counterTypes.size()
		);
	}


//	@Override
//	public String check() {
//		return "";
////		return ctx.lines.stream()
////				.map(Line::check)
////				.filter(s -> !s.isEmpty())
////				.collect(Collectors.joining("\r\n"));
//	}


	@Override
	protected boolean reload() {
		MotohrModule tmp = new MotohrModule( plugin, name );
		if( !tmp.load() )
			return false;

		closedown();
		copySettingsFrom(tmp);

		tagtable.copyValuesTo(tmp.tagtable);
		tagtable.removeAll();
		tmp.tagtable.moveTagsTo(tagtable);

		ctx = tmp.ctx;
		ctx.setModule(this);

		prepare();
		return true;
	}


}

