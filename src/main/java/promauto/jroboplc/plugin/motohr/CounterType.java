package promauto.jroboplc.plugin.motohr;

import promauto.jroboplc.core.exceptions.ConfigurationNotValidException;
import promauto.jroboplc.core.tags.TagRW;

import java.sql.SQLException;

public class CounterType {
    public static final String TOTAL_NAME = "total";
    public static final String TOTAL_DESCR = "Всего";
    private int id;
    private final Context ctx;
    public final String name;
    public final String descr;
    public final boolean timer;
    public final TagRW tagDescr;
    public final TagRW tagTimer;


    public static CounterType createCounterTypeTotal(Context ctx) {
        return new CounterType(ctx, TOTAL_NAME, TOTAL_DESCR, false);
    }


    public CounterType(Context ctx, String name, String descr, boolean timer) {
        this.ctx = ctx;
        this.name = name;
        this.descr = descr;
        this.timer = timer;
        tagDescr = ctx.tagtable.createRWString("cnttype." + name + ".descr", descr);
        tagTimer = ctx.tagtable.createRWBool("cnttype." + name + ".timer", timer);

    }


    public CounterType(Context ctx, Object conf) throws ConfigurationNotValidException {
        this(ctx,
                ctx.cfg.get(conf, "name", ""),
                ctx.cfg.get(conf, "descr", ""),
                ctx.cfg.get(conf, "timer", false)
        );

        if (name.isEmpty()  ||  descr.isEmpty())
            ctx.cfg.throwConfigurationNotValidException("CounterType", conf);
    }


    public static void initAll(Context ctx) throws SQLException {
        for(CounterType ct: ctx.counterTypes.values())
            ct.init();
    }


    public int getId() {
        return id;
    }


    public void init() throws SQLException {
        id = ctx.service.syncCounterType(name, descr, timer);
    }
}
