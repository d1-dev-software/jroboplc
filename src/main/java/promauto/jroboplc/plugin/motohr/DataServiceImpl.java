package promauto.jroboplc.plugin.motohr;

import javafx.util.Pair;
import promauto.jroboplc.core.DatabaseProtoServiceImpl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

public class DataServiceImpl extends DatabaseProtoServiceImpl implements DataService {
    private static final String TABLE_COUNTER_TYPE  = "MH_COUNTER_TYPE";
    private static final String TABLE_MACH          = "MH_MACH";
    private static final String TABLE_COUNTER       = "MH_COUNTER";
    private static final String TABLE_INTERVAL      = "MH_INTERVAL";
    private static final String TABLE_MACH_STATE    = "MH_MACH_STATE";
    private static final String TABLE_STAT          = "MH_STAT";

    private static final int SYNCTYPE_COUNTER_TYPE = 0;
    private static final int SYNCTYPE_COUNTER = 1;
    private static final int SYNCTYPE_MACH = 2;



    @Override
    public void startSync() throws SQLException {
        startSyncTable(SYNCTYPE_COUNTER_TYPE, TABLE_COUNTER_TYPE);
        startSyncTable(SYNCTYPE_COUNTER, TABLE_COUNTER);
        startSyncTable(SYNCTYPE_MACH, TABLE_MACH);
    }


    @Override
    public void finishSync() throws SQLException {
        finishSyncTable(SYNCTYPE_COUNTER_TYPE, TABLE_COUNTER_TYPE);
        finishSyncTable(SYNCTYPE_COUNTER, TABLE_COUNTER);
    }



    @Override
    public int syncCounterType(String name, String descr, boolean timer) throws SQLException {
        sql = String.format(
                "update or insert into %s (name, descr, timer, deleted) values ('%s', '%s', %d, 0) matching (name)",
                TABLE_COUNTER_TYPE,
                name,
                descr,
                timer? 1: 0
        );
        int id = executeAndGetId(sql);
        removeSyncId(SYNCTYPE_COUNTER_TYPE, id);
        return id;
    }

    @Override
    public int syncMach(String tagname, String name, String descr) throws SQLException {
        if( descr.isEmpty() ) {
            sql = String.format(
                    "update or insert into %s (tagname, name) values ('%s', '%s') matching (name)",
                    TABLE_MACH,
                    tagname,
                    name
            );
        } else {
            sql = String.format(
                    "update or insert into %s (tagname, name, descr) values ('%s', '%s', '%s') matching (name)",
                    TABLE_MACH,
                    tagname,
                    name,
                    descr
            );
        }
        int id = executeAndGetId(sql);
        removeSyncId(SYNCTYPE_MACH, id);
        return id;
    }



    @Override
    public Map<Integer, MachState>  getMachStates() throws SQLException {
        Map<Integer, MachState> states = new HashMap<>();
        sql = String.format(
                "select mach_id, dt, running, statcnt, statsec from %s",
                TABLE_MACH_STATE
        );

        try(ResultSet rs = st.executeQuery(sql)) {
            while (rs.next()) {
                states.put(
                        rs.getInt(1),
                        new MachState(
                                rs.getTimestamp(2).toLocalDateTime(),   // dt
                                rs.getInt(3) == 1,                      // running
                                rs.getInt(4),                           // statcnt
                                rs.getInt(5)                            // statsec
                        )
                );
            }
        }

        return states;
    }


    @Override
    public void saveMachState(int id, MachState state) throws SQLException {
        sql = String.format(
                "update or insert into %s (mach_id, dt, running, statcnt, statsec) values (%d, '%s', %d, %d, %d) matching (mach_id)",
                TABLE_MACH_STATE,
                id,
                db.formatDatetime(state.dt),
                state.running? 1: 0,
                state.statcnt,
                state.statsec
        );
        st.executeUpdate(sql);
    }


    @Override
    public Map<Pair<Integer,Integer>, Pair<Integer, Integer>> getCounters() throws SQLException {
        Map<Pair<Integer,Integer>, Pair<Integer, Integer>> counters = new HashMap<>();
        sql = String.format(
                "select mach_id, counter_type_id, id, sec from %s where deleted=0",
                TABLE_COUNTER
        );

        try(ResultSet rs = st.executeQuery(sql)) {
            while (rs.next()) {
                counters.put(
                        new Pair<>(rs.getInt(1), rs.getInt(2)),
                        new Pair<>(rs.getInt(3), rs.getInt(4))
                );
            }
        }
        return counters;
    }


    @Override
    public int createCounter(int machId, int counterTypeId) throws SQLException {
        sql = String.format(
                "update or insert into %s (mach_id, counter_type_id, sec, deleted) values (%d, %d, 0, 0) matching (mach_id, counter_type_id)",
                TABLE_COUNTER,
                machId,
                counterTypeId
        );
        return executeAndGetId(sql);
    }


    @Override
    public void saveInterval(int id, LocalDateTime dt1, LocalDateTime dt2, int sec) throws SQLException {
        sql = String.format(
                "insert into %s (mach_id, dtbeg, dtend, sec) values (%d, '%s', '%s', %d)",
                TABLE_INTERVAL,
                id,
                db.formatDatetime(dt1),
                db.formatDatetime(dt2),
                sec
        );
        st.executeUpdate(sql);
    }


    @Override
    public void saveCounter(int id, int sec) throws SQLException {
        sql = String.format(
                "update %s set sec=%d where id=%d",
                TABLE_COUNTER,
                sec,
                id
        );
        st.executeUpdate(sql);
    }


    @Override
    public void saveStat(int id, int period, int cnt, int sec) throws SQLException {
        sql = String.format(
                "insert into %s (mach_id, period, cnt, sec) values (%d, %d, %d, %d)",
                TABLE_STAT,
                id,
                period,
                cnt,
                sec
        );
        st.executeUpdate(sql);
    }


    @Override
    public int sweepInterval(LocalDateTime dt) throws SQLException {
        sql = String.format(
                "delete from %s where dtbeg < '%s'",
                TABLE_INTERVAL,
                db.formatDatetime(dt)
        );
        return st.executeUpdate(sql);
    }

    @Override
    public int sweepStat(int period) throws SQLException {
        sql = String.format(
                "delete from %s where period <= %d",
                TABLE_STAT,
                period
        );
        return st.executeUpdate(sql);
    }


    @Override
    public int packMach() throws SQLException {
        int n = 0;
        HashSet<Integer> ids = sync.get(SYNCTYPE_MACH);
        for (Integer id: ids) {
            sql = String.format("delete from %s where id=%d", TABLE_MACH, id);
            n += st.executeUpdate(sql);
        }
        ids.clear();
        return n;
    }


    @Override
    public void syncCounter(int id) {
        removeSyncId(SYNCTYPE_COUNTER, id);
    }


    @Override
    public Map<Integer, String> getMachDescrs() throws SQLException {
        Map<Integer, String> descrs = new HashMap<>();
        sql = String.format(
                "select id, descr from %s",
                TABLE_MACH
        );

        try(ResultSet rs = st.executeQuery(sql)) {
            while (rs.next()) {
                descrs.put(
                        rs.getInt(1),
                        rs.getString(2)
                );
            }
        }
        return descrs;
    }
}
