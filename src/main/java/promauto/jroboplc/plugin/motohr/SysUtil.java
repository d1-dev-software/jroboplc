package promauto.jroboplc.plugin.motohr;

import promauto.jroboplc.core.TagTable;
import promauto.jroboplc.core.tags.TagRW;

import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

public class SysUtil {

    private final Context ctx;
    private TagRW tagSysUtil;
    private String[] args;
    private String answer;


    public SysUtil(Context ctx) {
        this.ctx = ctx;
    }


    public void load() {
        TagTable tt = ctx.tagtable;
        tagSysUtil = tt.createRWString( "system.util", "");
    }


    public void execute() throws SQLException {
        if( !tagSysUtil.hasWriteValue() )
            return;

        answer = "";
        try {
            args = tagSysUtil.getWriteValString().split(" ");
            switch (args[0]) {
                case "help":
                    doHelp();
                    break;
                case "sweep-interval":
                    doSweepInterval();
                    break;
                case "sweep-stat":
                    doSweepStat();
                    break;
                case "pack-mach":
                    doPackMach();
                    break;
            }
        } catch (Exception e) {
            answer = e.getMessage();
        }
        tagSysUtil.setReadValString(answer);
    }

    private void doHelp() {
        answer = "help\npack-mach\nsweep-interval <yyyymm>\nsweep-stat <yyyymm>";
    }


    private void doSweepInterval() throws SQLException {
        if( args.length != 2 )
            return;

        int ym = Integer.parseInt(args[1]);
        LocalDateTime dt = LocalDateTime.of(Math.abs(ym / 100), ym % 100, 1, 0, 0).plus(1, ChronoUnit.MONTHS);
        int res = ctx.service.sweepInterval(dt);
        answer = "Deleted " + res + " records from MH_INTERVAL";
    }

    private void doSweepStat() throws SQLException {
        if( args.length != 2 )
            return;

        int period = Integer.parseInt(args[1]) * 100 + 31;
        int res = ctx.service.sweepStat(period);
        answer = "Deleted " + res + " records from MH_STAT";
    }


    private void doPackMach() throws SQLException {
        int res = ctx.service.packMach();
        answer = "Deleted " + res + " records from MH_MACH";
    }


}
