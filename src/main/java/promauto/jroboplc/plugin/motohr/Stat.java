package promauto.jroboplc.plugin.motohr;

import promauto.jroboplc.core.TagTable;
import promauto.jroboplc.core.tags.TagRW;

import java.sql.SQLException;
import java.time.LocalDateTime;

public class Stat {
    private final Context ctx;
    private TagRW tagPeriod;


    public Stat(Context ctx) {
        this.ctx = ctx;
    }


    public void load() {
        TagTable tt = ctx.tagtable;
        ctx.addRepoTags(
                tagPeriod = tt.createRWInt( "Period", 0)
        );

    }


    public void init() {

    }


    public void execute() throws SQLException {
        int curPeriod = getPeriod();
        if( curPeriod != tagPeriod.getInt() ) {
            for(Mach mach: ctx.machs) {
                mach.saveStat(tagPeriod.getInt());
            }
        }
        tagPeriod.setReadValInt(curPeriod);
    }


    private int getPeriod() {
        LocalDateTime dt = ctx.now();
        return dt.getYear() * 10000 + dt.getMonthValue() * 100 + dt.getDayOfMonth();
//        return dt.getHour() * 100 + dt.getMinute();
    }


}
