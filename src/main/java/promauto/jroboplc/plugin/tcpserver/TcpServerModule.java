package promauto.jroboplc.plugin.tcpserver;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executor;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.DelimiterBasedFrameDecoder;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.Delimiters;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.string.StringEncoder;

import promauto.jroboplc.core.AbstractModule;
import promauto.jroboplc.core.api.Configuration;
import promauto.jroboplc.core.api.EnvironmentInst;
import promauto.jroboplc.core.api.Plugin;
import promauto.jroboplc.core.api.TcpServer;
import promauto.jroboplc.core.api.TcpServerChannel;
import promauto.jroboplc.core.api.TcpServerPort;
import promauto.jroboplc.core.api.TcpSubscriber;

public class TcpServerModule extends AbstractModule implements TcpServer, Runnable {

	private static final int TEXT_FRAME_SIZE_MAX = 16384;
	private static final int DISCONNECT_TIMEOUT_MS = 10000;

	private volatile boolean running = false;

	protected Map<Integer, TcpServerPortImpl> portnums = new HashMap<>();
	protected Map<Integer, TcpServerPortImpl> porttcps = new HashMap<>();

	private EventLoopGroup bossGroup = null;
	private EventLoopGroup workerGroup = null;

	private Thread thread = null;


	public TcpServerModule(Plugin plugin, String name) {
		super(plugin, name);
		env.setTcpServer(this);
		taskable = false;
	}

	
	public boolean loadModule(Object conf) {
		Configuration cm = EnvironmentInst.get().getConfiguration();
		
		for (Object portconf: cm.toList( cm.get(conf, "ports"))) {
			TcpServerPortImpl port = new TcpServerPortImpl();
			port.load(portconf);
			
			portnums.put(port.getPortnum(), port);
			porttcps.put(port.getPorttcp(), port);
		}
		return true;
	}
	
	


	@Override
	public boolean prepareModule() {
		return start();
	}

	
	
	@Override
	public boolean closedownModule() {
		return stop();
	}

	

	
	
	
	
	@Override
	public TcpServerPort getPortByNum(int portnum) {
		return portnums.get(portnum);
	}

	@Override
	public TcpServerPort getPortByTcp(int porttcp) {
		return porttcps.get(porttcp);
	}

	@Override
	public TcpServerPort getPortBySubscriber(TcpSubscriber subscriber) {
		for(TcpServerPort port: portnums.values())
			if( port.getSubscriber() == subscriber )
				return port;
		return null;
	}


	@Override
	public boolean subscribe(int portnum, TcpSubscriber subscriber) {
		TcpServerPort port = env.getTcpServer().getPortByNum(portnum);
		if (port==null) {
			env.printError(logger, name, "Subscribe port not found:", "" + portnum);
			return false;
		}

		port.setSubscriber(subscriber);
		return true;
	}


	public boolean start() {
		if (running) return false;
		running = true;
		boolean result = true;

		if( bossGroup == null )
			bossGroup = new NioEventLoopGroup();

		if( workerGroup == null )
			workerGroup = new NioEventLoopGroup();

		Map<String, ServerBootstrap> bootstraps = new HashMap<>();

		bootstraps.put("text", createBootstrap(
			new ChannelInitializer<SocketChannel>() {
				@Override
				public void initChannel(SocketChannel ch) throws Exception {
					ch.pipeline().addLast("framer",
							new DelimiterBasedFrameDecoder(TEXT_FRAME_SIZE_MAX, Delimiters.lineDelimiter() ));

					ch.pipeline().addLast("decoder", new StringDecoder());
					ch.pipeline().addLast("encoder", new StringEncoder());
					ch.pipeline().addLast( new TextHandler(TcpServerModule.this) );
				}
			}
		));

		for (TcpServerPortImpl port : portnums.values()) {
			if (!port.isEnabled()) {
				continue;
			}

			try {
				ServerBootstrap bs = bootstraps.get(port.getFormat());
				if (bs == null) {
					env.printError(logger, name, "Bad tcpserver port format:", port.getFormat());
					result = false;
					break;
				} else {
					bs.bind(port.getPorttcp()).sync();
				}
			} catch (Exception e) {
				EnvironmentInst.get().printError(logger, e, name, "Tcp channel error");
				result = false;
			}
		}

		if( result ) {
			if( thread == null )
				thread = new Thread(this);
			thread.setName("thread-" + name);
			thread.start();
		}

		return result;
	}


	private ServerBootstrap createBootstrap(ChannelHandler handler) {
		ServerBootstrap bs = new ServerBootstrap();
		bs.group(bossGroup, workerGroup)
				.channel(NioServerSocketChannel.class)
				.childHandler(handler)
				.option(ChannelOption.SO_BACKLOG, 128)
				.childOption(ChannelOption.SO_KEEPALIVE, true);
		return bs;
	}


//	@Override
	public boolean stop() {
		if (!running) return false;
		running = false;

		bossGroup.shutdownGracefully();
		workerGroup.shutdownGracefully();

		try {
			bossGroup.terminationFuture().sync();
			workerGroup.terminationFuture().sync();
		} catch (InterruptedException e) {
			env.printError(logger, e, name);
		}

		bossGroup = null;
		workerGroup = null;

		try {
			if( thread != null ) {
				thread.join(10000);
				thread = null;
			}
		} catch (InterruptedException e) {
		}
		
		return true;
	}

	
	@Override
	public boolean disconnect(int portnum) {
		if (!running) 
			return false;
		
		TcpServerPortImpl port = portnums.get(portnum);
		if( port == null )
			return false;
		
		try {
			if( port.close(DISCONNECT_TIMEOUT_MS) ) {
				long t = System.currentTimeMillis();
				while( System.currentTimeMillis() - t < DISCONNECT_TIMEOUT_MS )
					if( port.isDisconnected() )
						return true;
					else
						Thread.sleep(1);
			}
		} catch (Exception e) {
			env.printError(logger, e, name);
		}

		return false; 
	}
	
	
	@Override
	public String getInfo() {
		if( !enable )
			return "disabled";
		
		StringBuilder sb = new StringBuilder();
		for(TcpServerPort port: portnums.values()) {
			sb.append(port.getPortnum());
			sb.append(": tcp=");
			sb.append(port.getPorttcp());
			
			if (port.isEnabled()) {
				if( port.getSubscriber() == null )
					sb.append(", unused\r\n");
				else {
					sb.append(", connected ");
					sb.append(port.getChannels().size() );
					sb.append(" clients\r\n");
					for(TcpServerChannel ch: port.getChannels()) {
						sb.append("  ");
						sb.append(ch);
						sb.append("\r\n");
					}
				}
			} else 
				sb.append(", disabled\r\n");
		}
		return sb.toString();
	}

	@Override
	protected boolean reload() {
		new Thread(() -> reloadModule()).start();
		return true;
	}

	protected boolean reloadModule() {
		boolean valid = (new TcpServerModule(plugin, name)).load();
		env.setTcpServer(this);
		if( !valid )
			return false;
		
		if( running )
			stop();
		
		Map<Integer, TcpServerPortImpl> portnums_old = portnums;
		Map<Integer, TcpServerPortImpl> porttcps_old = porttcps;
		portnums = new HashMap<>();
		porttcps = new HashMap<>();
		
		if( load() ){
			for(TcpServerPort port_old: portnums_old.values()) {
				TcpServerPort port_new = portnums.get(port_old.getPortnum());
				if( port_new != null )
					port_new.setSubscriber( port_old.getSubscriber() );
			}
			
			portnums_old.clear();
			porttcps_old.clear();
			
			if( enable  &&  env.isRunning() )
				return start();
			
			return true;
		} 
		
		portnums = portnums_old;
		porttcps = porttcps_old;
		return false;
	}


	@Override
	public void run() {
		try {
			while( running ) {
				Thread.sleep(1000);
				for(TcpServerPort port: portnums.values())
					if (port.isEnabled()  &&  port.getSubscriber() != null)
						for(TcpServerChannel ch: port.getChannels())
							ch.checkAlive();
			}
		} catch (InterruptedException e) {
			EnvironmentInst.get().printError(logger, e, name);
		}
	}



}