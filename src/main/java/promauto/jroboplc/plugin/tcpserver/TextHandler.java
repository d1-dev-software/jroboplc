package promauto.jroboplc.plugin.tcpserver;

import java.net.InetSocketAddress;
import java.nio.channels.ClosedChannelException;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.util.AttributeKey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import promauto.jroboplc.core.api.EnvironmentInst;
import promauto.jroboplc.core.api.TcpServerChannel;
import promauto.jroboplc.core.api.TcpSubscriber;



public class TextHandler extends SimpleChannelInboundHandler<String> {

	private static final AttributeKey<TcpServerChannel> STATE =
			AttributeKey.valueOf("TcpServerChannel");


	private final Logger logger = LoggerFactory.getLogger(TextHandler.class);

	private TcpServerModule server;
	
	
	
	public TextHandler(TcpServerModule server) {
		this.server = server;
	}

    
    @Override
    public void channelActive(ChannelHandlerContext ctx) {
		int porttcp = ((InetSocketAddress) ctx.channel().localAddress()).getPort();
		TcpServerPortImpl port = server.porttcps.get( porttcp );
		TcpSubscriber sub = port.getSubscriber();
		TcpServerChannel ch = port.addChannel( ctx.channel() );

		ctx.channel().attr(STATE).set(ch);

		if( ch!=null  &&  sub!=null  &&  EnvironmentInst.get().isRunning() ) {
			sub.onTcpServerClientConnected(ch);
		} else {
			ctx.channel().close();
			EnvironmentInst.get().printInfo(logger, server.getName(), "Rejected:", getIp(ctx), ""+port.getPorttcp());
		}
	}


	private String getIp(ChannelHandlerContext ctx) {
		return ((InetSocketAddress) ctx.channel().remoteAddress()).getAddress().getHostAddress();
	}

    
	@Override
    public void channelInactive(ChannelHandlerContext ctx) {
		TcpServerChannel ch = ctx.channel().attr(STATE).get();
		if (ch!=null) {
			TcpServerPortImpl port = (TcpServerPortImpl)ch.getPort(); 
			TcpSubscriber sub = port.getSubscriber();
			
			if( sub != null )
				sub.onTcpServerClientDisconnected(ch);

			port.removeChannel(ctx.channel());
		}
    }




    @Override
    public void channelRead0(ChannelHandlerContext ctx, String msg) {
		TcpServerChannelImpl ch = (TcpServerChannelImpl)ctx.channel().attr(STATE).get();
		if (ch!=null) {
			TcpServerPortImpl port = (TcpServerPortImpl)ch.getPort(); 
			TcpSubscriber sub = port.getSubscriber();

			if (sub !=null) { 
				sub.onTcpServerRequest(ch, msg);
				ch.markChannelAsAlive();
			} else
				ctx.channel().disconnect();
		}
    }

    
    
    
    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
		TcpServerChannel ch = ctx.channel().attr(STATE).get();
		if( cause instanceof ClosedChannelException )
            EnvironmentInst.get().printError(logger, server.getName(), "ClosedChannel", ch.toString());
		else
            EnvironmentInst.get().printError(logger, cause, server.getName(), ch.toString());

        ctx.channel().close();
    }

	
}
