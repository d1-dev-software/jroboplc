package promauto.jroboplc.plugin.tcpserver;

import static promauto.utils.Strings.getFilterPattern;

import java.net.InetSocketAddress;
import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Pattern;

import io.netty.channel.Channel;

import promauto.jroboplc.core.api.Configuration;
import promauto.jroboplc.core.api.EnvironmentInst;
import promauto.jroboplc.core.api.TcpServerChannel;
import promauto.jroboplc.core.api.TcpServerPort;
import promauto.jroboplc.core.api.TcpSubscriber;

public class TcpServerPortImpl implements TcpServerPort {
//	private final Logger logger = LoggerFactory.getLogger(TcpServerPortImpl.class);
	
	private int portnum;
	private int porttcp;
	private boolean enable;
	private int maxcon;
	private boolean logging; 
	private String format;
	
	private volatile TcpSubscriber subscriber = null;
	private Map<Channel, TcpServerChannelImpl> channels = new ConcurrentHashMap<>();
	
	private Pattern incl = null;
	private Pattern excl = null;
	private long aliveTimeout;
	
//	private Channel nettyChannel;

	
	public void load(Object conf) {
		Configuration cm = EnvironmentInst.get().getConfiguration();
		
		portnum = cm.get(conf, "portnum", 	0); 
		porttcp = cm.get(conf, "porttcp", 	0);
		enable 	= cm.get(conf, "enable", 	true);
		maxcon 	= cm.get(conf, "maxcon", 	32);
		aliveTimeout = cm.get(conf, "aliveTimeout_s", 60)*1000;
		logging = cm.get(conf, "logging", 	false);
		format 	= cm.get(conf, "format", 	"text");
		incl 	= getFilterPattern( cm.get(conf, "incl", "") );
		excl 	= getFilterPattern( cm.get(conf, "excl", "") );
	}
	


	@Override
	public int getPortnum() {
		return portnum;
	}

	@Override
	public int getPorttcp() {
		return porttcp;
	}

	@Override
	public String getFormat() {
		return format;
	}

	@Override
	public boolean isEnabled() {
		return enable;
	}
	
	
	@Override
	public synchronized Collection<? extends TcpServerChannel> getChannels() {
		return channels.values();
	}

	@Override
	public TcpSubscriber getSubscriber() {
		return subscriber;
	}

	@Override
	public void setSubscriber(TcpSubscriber subscriber) {
		this.subscriber = subscriber;
	}


	public synchronized TcpServerChannel addChannel(Channel channel) {
		InetSocketAddress address = (InetSocketAddress) channel.remoteAddress();
		String hostip = address.getAddress().getHostAddress();
		
		if ((maxcon>0) && (channels.size() >= maxcon)) {
			return null;
		}
		
		if( incl!=null  &&  !incl.matcher(hostip).matches() ) {
			return null;
		} else
		if( excl!=null  &&  excl.matcher(hostip).matches() ) {
			return null;
		}
		
		TcpServerChannelImpl ch = new TcpServerChannelImpl(channel, this);
		ch.setLogEnable(logging);
		channels.put(channel, ch);
		return ch;
	}

	public synchronized void removeChannel(Channel channel) {
		channels.remove(channel);
	}


	public boolean close(long timeout) {
		return channels.values().stream().allMatch( ch -> ch.close(timeout) );
	}


	public boolean isDisconnected() {
		return channels.isEmpty();
	}


	@Override
	public long getAliveTimeout() {
		return aliveTimeout;
	}




}
