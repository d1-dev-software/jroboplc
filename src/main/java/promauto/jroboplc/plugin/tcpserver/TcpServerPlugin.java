package promauto.jroboplc.plugin.tcpserver;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import promauto.jroboplc.core.AbstractPlugin;
import promauto.jroboplc.core.api.Module;

public class TcpServerPlugin extends AbstractPlugin {
	private final Logger logger = LoggerFactory.getLogger(TcpServerPlugin.class);

	private static final String PLUGIN_NAME = "tcpserver";
	
	@Override
	public void initialize() {
		super.initialize();
		env.getCmdDispatcher().addCommand(this, CmdLog.class);
    }
	
	@Override
    public String getPluginName() {
    	return PLUGIN_NAME;
    }

	@Override
    public String getPluginDescription(){
    	return "Adds tcp server support. Based on io.netty library.";
    }
	
//	@Override
//    public Class<?> getModuleClass(){
//    	return TcpServerModule.class;
//    }
	
	
	@Override
	public Module createModule(String name, Object conf) {
		if (modules.size()>0) {
			env.printError(logger, PLUGIN_NAME, "No more than one module is allowed for this plugin");
			return null;
		}

		TcpServerModule m = new TcpServerModule(this, name);
    	modules.add(m);
    	return m.load(conf)? m: null;
	}



}
