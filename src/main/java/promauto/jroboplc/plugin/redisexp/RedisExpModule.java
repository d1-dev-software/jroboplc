package promauto.jroboplc.plugin.redisexp;

import io.lettuce.core.*;
import io.lettuce.core.api.StatefulRedisConnection;
import io.lettuce.core.api.async.RedisAsyncCommands;
import io.lettuce.core.output.KeyValueStreamingChannel;
import io.lettuce.core.pubsub.RedisPubSubListener;
import io.lettuce.core.pubsub.StatefulRedisPubSubConnection;
import io.lettuce.core.pubsub.api.async.RedisPubSubAsyncCommands;
import promauto.jroboplc.core.AbstractModule;
import promauto.jroboplc.core.api.*;

import java.time.Duration;
import java.util.*;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import static promauto.jroboplc.plugin.redisexp.ExportTag.State.*;
import static promauto.jroboplc.plugin.redisexp.TagInfoCodec.*;

public class RedisExpModule extends AbstractModule implements Signal.Listener, RedisPubSubListener<String, String> {


    private static final int MESSAGE_QUEUE_SIZE_MAX = 1000000;
    public static final int SCAN_LIMIT = 100000;

    private final Map<String, ExportTag> etags = new HashMap<>();

    private String descr;
    private String host;
    private int port;
    private int dbnum;
    private String domain;
    private boolean ssl;
    private int timeoutConn;
    private int timeoutCmd;
    private String filter;
    private boolean readonly;
    private boolean noScanDelete;

    private volatile boolean needCollect;
    private volatile boolean needResetAll;
    private volatile int cntUpdatingLast;
    private volatile int cntUpdatingWait;

    private Tag tagConnected;
    private Tag tagDisconnectCnt;
    private Tag tagLastError;
    
    private RedisURI uri;
    private RedisClient redisClient;
    private StatefulRedisConnection<String,String> connectionCmd;
    private StatefulRedisPubSubConnection<String,String> connectionPubSub;
    private RedisAsyncCommands<String, String> asyncCmd;
    private RedisPubSubAsyncCommands<String, String> asyncPubSub;

    private final Queue<String> queueWrites = new ConcurrentLinkedQueue<>();
    private final AtomicInteger cntWrites = new AtomicInteger(0);
    private final Queue<String> queueResets = new ConcurrentLinkedQueue<>();
    private final AtomicInteger cntResets = new AtomicInteger(0);

    private String serverTimeKey;
    private String tagsKey;
    private String channelUpdate;
    private String channelDelete;
    private String channelWrite;
    private String channelReset;

    private boolean firstPass;
    private boolean opened;


    public RedisExpModule(Plugin plugin, String name) {
        super(plugin, name);
    }


    @Override
    protected boolean loadModule(Object conf) {
        Configuration cm = env.getConfiguration();

        descr = cm.get(conf, "descr", "jrobo-" + name);
        host = cm.get(conf, "host", "localhost");
        port = cm.get(conf, "port", 6379);
        dbnum = cm.get(conf, "dbnum", 0);
        domain = cm.get(conf, "domain", "").trim();
        ssl = cm.get(conf, "ssl", false);
        timeoutConn = cm.get(conf, "timeoutConn_s", 10);
        timeoutCmd = cm.get(conf, "timeoutCmd_s", 60);
        String username = cm.get(conf, "username", "");
        CharSequence password = cm.get(conf, "password", "");
        buildUri(username, password);

        filter = cm.get(conf, "filter", "");
        readonly = cm.get(conf, "readonly", false);
        noScanDelete = cm.get(conf, "noScanDelete", false);

        tagConnected	 = tagtable.getOrCreateBool(    "client.connected", false);
        tagDisconnectCnt = tagtable.getOrCreateInt(     "client.disconnect.cnt", 0);
        tagLastError     = tagtable.getOrCreateString(  "client.last.error", "");

        if( !domain.isEmpty() )
            domain += ".";
        serverTimeKey = domain + SERVER_TIME_KEY;
        tagsKey = domain + TAGS_KEY;
        channelUpdate = domain + CHANNEL_UPDATE;
        channelDelete = domain + CHANNEL_DELETE;
        channelWrite = domain + CHANNEL_WRITE;
        channelReset = domain + CHANNEL_RESET;

        return true;
    }


    private void buildUri(String username, CharSequence password) {
        RedisURI.Builder uriBuilder = RedisURI.builder()
                .withHost(host)
                .withSsl(ssl)
                .withPort(port)
                .withDatabase(dbnum);

        if( !descr.isEmpty() )
            uriBuilder = uriBuilder.withClientName(descr);

        if( !username.isEmpty() )
            uriBuilder = uriBuilder.withAuthentication(username, password);
        else if( password.length() > 0 )
            uriBuilder = uriBuilder.withPassword(password);

        uri = uriBuilder.build();
    }


    @Override
    public void onSignal(Module sender, Signal signal) {
        if (signal.type == Signal.SignalType.RELOADED)
            needCollect = true;
    }


    @Override
    protected boolean prepareModule() {
        firstPass = true;
        needCollect = true;
        needResetAll = false;
        cntUpdatingLast = 0;
        cntUpdatingWait = 0;
        tagConnected.setBool(false);
        tagDisconnectCnt.setInt(0);
        tagLastError.setString("");

        addAsSignalListerToAllOthers();
//        for (Module m : env.getModuleManager().getModules())
//            m.addSignalListener(this);

        createClient();
        return true;
    }


    @Override
    protected boolean closedownModule() {
        shutdownClient();

        removeAsSignalListerFromAllOthers();
//        for (Module m: env.getModuleManager().getModules())
//            m.removeSignalListener(this);

        return true;
    }


    @Override
    protected boolean executeModule() {
        try {
            if (connectionCmd == null) {
                opened = false;
                if( !connect() ) {
                    if (firstPass)
                        env.printInfo(logger, name, "No connection");
                    return false;
                }
            }

            // on disconnected
            if (!connectionCmd.isOpen()  &&  opened ) {
                tagDisconnectCnt.setInt(tagDisconnectCnt.getInt() + 1);
                tagConnected.setBool( opened = false );
                env.printInfo(logger, name, "Connection lost: " + tagDisconnectCnt.getInt());
                return false;
            }

            // on connected
            if (connectionCmd.isOpen()  &&  !opened ) {
                if (!firstPass)
                    env.printInfo(logger, name, "Connection restored");

                tagConnected.setBool( opened = true );
                collectTags();
                deleteKeys();
                resetAllValues();
            }

            if (!connectionCmd.isOpen())
                return false;

            collectTags();
            processWrites();
            processResets();
            updateValues();
            updateServerTime();

        } catch (Exception e) {
            env.printError(logger, e, name);
            disconnect();
        }

        firstPass = false;
        return true;
    }

    private void updateServerTime() {
        asyncCmd.set(serverTimeKey, ""+System.currentTimeMillis());
        asyncCmd.flushCommands();
    }


    private void createClient() {
//  todo: use native epoll
//  https://github.com/lettuce-io/lettuce-core/issues/1428
//        ClientResources clientResources = ClientResources.builder()
//                .nettyCustomizer(new NettyCustomizer() {
//                    @Override
//                    public void afterBootstrapInitialized(Bootstrap bootstrap) {
//                        bootstrap.option(EpollChannelOption.TCP_KEEPIDLE, 15);
//                        bootstrap.option(EpollChannelOption.TCP_KEEPINTVL, 5);
//                        bootstrap.option(EpollChannelOption.TCP_KEEPCNT, 3);
//                        // Socket Timeout (milliseconds)
//                        bootstrap.option(EpollChannelOption.TCP_USER_TIMEOUT, 60000);
//                    }
//                })
//                .build();
//        redisClient = RedisClient.create(clientResources);

        SocketOptions socketOptions = SocketOptions.builder()
                .connectTimeout(Duration.ofSeconds(timeoutConn))
                .keepAlive(true)
                .build();
        ClientOptions clientOptions = ClientOptions.builder()
                .socketOptions(socketOptions)
                .build();

        if( redisClient != null )
            shutdownClient();
        redisClient = RedisClient.create();
        redisClient.setOptions(clientOptions);
    }


    private void shutdownClient() {
        try {
            disconnect();
            redisClient.shutdown();
        } catch (Exception e) {
            env.printError(logger, e, name);
        }
        redisClient = null;
        connectionCmd = null;
        connectionPubSub = null;
    }


    private boolean connect() {
        if( connectionCmd != null  || connectionPubSub != null )
            disconnect();

        try {
            connectionCmd = redisClient.connect(uri);
            connectionCmd.setAutoFlushCommands(false);
            asyncCmd = connectionCmd.async();

            connectionPubSub = redisClient.connectPubSub(uri);
            connectionPubSub.addListener(this);
            asyncPubSub = connectionPubSub.async();
            asyncPubSub.subscribe(channelWrite, channelReset);

            return true;
        } catch (Exception e) {
            tagLastError.setString(e.getLocalizedMessage());
        }
        return false;
    }


    private void disconnect() {
        tagConnected.setBool(false);
        try {
            if( connectionCmd != null)
                connectionCmd.close();
            connectionCmd = null;
        } catch (Exception e){
            env.printError(logger, e, name);
        }

        try {
            if( connectionPubSub != null)
                connectionPubSub.close();
            connectionPubSub = null;
        } catch (Exception e) {
            env.printError(logger, e, name);
        }
    }


    private String getModuleTagname(Module module, Tag tag) {
        return tag.hasFlags(Flags.EXTERNAL)? tag.getName(): module.getName() + '.' + tag.getName();
    }


    private void collectTags() {
        if (!needCollect)
            return;

        needCollect = false;

        etags.values().forEach(etag -> etag.relevant = false);

        filter = filter.trim().isEmpty()? ".*": filter;
        Pattern pfilter;
        try {
            pfilter = Pattern.compile(filter);
        } catch (PatternSyntaxException e) {
            env.printError(logger, name, "Bad filter: " + filter + ", changed to .*");
            pfilter = Pattern.compile( filter = ".*" );
        }

        long cntAdd = 0;
        for (Module m: env.getModuleManager().getModules()) {
            for(Tag tag: m.getTagTable().values()) {
                String tagname = getModuleTagname(m, tag);

                if( !pfilter.matcher(tagname).matches() )
                    continue;

                ExportTag etag = etags.get(tagname);
                if( etag == null ) {
                    etag = new ExportTag(tagname, tag);
                    etags.put(tagname, etag);
                    cntAdd++;
                }
                etag.relevant = true;
            }
        }

        long cntDel = etags.values().stream().filter(etag -> !etag.relevant).count();

        if( cntDel > 0) {
            etags.forEach((key, etag) -> {
                if (!etag.relevant) {
                    deleteKey(key);
                }
            });
            asyncCmd.flushCommands();
            etags.entrySet().removeIf(entry -> !entry.getValue().relevant);
        }

        if( cntAdd + cntDel > 0 )
            env.printInfo(logger, name, "Collected tags: "
                    + (cntAdd>0? cntAdd+" added": "")
                    + (cntDel>0? cntDel+" deleted": "")
            );

    }


    private RedisFuture<Long> deleteKey(String key) {
        asyncCmd.publish(channelDelete, TagInfoCodec.encodeMessageDelete(key, System.currentTimeMillis()));
        return asyncCmd.hdel(tagsKey, key);
    }

    private void deleteKeys() {
        if( noScanDelete )
            return;

        // scan and delete SYNCHRONOUSLY
        try {
            env.printInfo(logger, name, "Scanning existing keys...");
            long t1 = System.currentTimeMillis();
            List<CompletionStage<Long>> futures = new ArrayList<>();
            KeyValueStreamingChannel<String,String> channel = (key,value) -> {
                if (!etags.containsKey(key)) {
                    futures.add(deleteKey(key));
                }
            };
            ScanArgs args = ScanArgs.Builder.limit(SCAN_LIMIT);
            StreamScanCursor cursor;

            RedisFuture<StreamScanCursor> scanFuture = asyncCmd.hscan(channel, tagsKey, args);
            for(;;) {
                asyncCmd.flushCommands();
                boolean scanOk = scanFuture.await(timeoutCmd, TimeUnit.SECONDS);
                long t2 = System.currentTimeMillis();
                if( scanOk ) {
                    cursor = scanFuture.get();
                    env.printInfo(logger, name, String.format("Scanned %d keys in %d ms", cursor.getCount(), (t2 - t1)));
                } else {
                    cursor = null;
                    env.printInfo(logger, name, "Scan timeout!");
                }

                if (futures.size() > 0) {
                    env.printInfo(logger, name, String.format("Deleting %d keys...", futures.size()));
                    t1 = System.currentTimeMillis();
                    asyncCmd.flushCommands();
                    boolean delOk = LettuceFutures.awaitAll(timeoutCmd, TimeUnit.SECONDS, futures.toArray(new RedisFuture[0]));
                    t2 = System.currentTimeMillis();
                    env.printInfo(logger, name, String.format("Deleted %d keys in %d ms %s",
                            futures.size(), (t2 - t1), (delOk?"": "with timeout!")));
                }

                if( !asyncCmd.isOpen()  ||  cursor == null  ||  cursor.isFinished() ) {
                    break;
                }

                env.printInfo(logger, name, "Continue scanning existing keys...");
                futures.clear();
                scanFuture = asyncCmd.hscan(channel, tagsKey, cursor, args);
            }

        } catch (Exception e) {
            env.printError(logger, e, name);
        }
    }


    private int cntWait;
    private int cntLast;
    private void updateValues() {
        cntWait = 0;
        cntLast = 0;
        int timeout = timeoutCmd * 1000;
        long now = System.currentTimeMillis();
        etags.forEach((key, etag) -> {

            if( etag.state == HAS_FUTURE ) {
                if( etag.future.isDone() ) {
                    etag.state = IDLE;
                } else if( etag.future.isCancelled() ) {
                    etag.state = NEED_SET;
                } else {
                    if( (now - etag.timeSet) > timeout) {
                        etag.future.cancel(true);
                        etag.state = NEED_SET;
                        if(opened)
                            env.printInfo(logger, name, String.format("Update timeout: %s = %s", key, etag.curval.getString()));
                    }
                }
            }

            if( etag.state == IDLE ) {
                if( etag.isOutdated() ) {
                    etag.state = NEED_SET;
                }
            }

            if( etag.state == NEED_SET ) {
                etag.update();
                etag.future = asyncCmd.hset(tagsKey, key, etag.getEncodedData());
                etag.timeSet = now;
                etag.state = HAS_FUTURE;

                asyncCmd.publish(channelUpdate, etag.getEncodedMessageUpdate());
                cntLast++;
            }

            if( etag.state == HAS_FUTURE)
                cntWait++;
        });

        if(cntLast > 0) {
            asyncCmd.flushCommands();
            cntUpdatingLast = cntLast;
        }

        if( cntWait > 0) {
            cntUpdatingWait = cntWait;
        }
    }


    private void processWrites() {
        processMessages(queueWrites, cntWrites, this::writeValue);
    }


    private void writeValue(String message) {
        TagInfo info = TagInfoCodec.decodeMessageWrite(message);
        if( info != null ) {
            ExportTag etag = etags.get(info.name);
            if (etag != null ) {
                etag.tag.setString(info.value);
            }
        }
    }


    private void processResets() {
        if( needResetAll ) {
            resetAllValues();
            needResetAll = false;
            queueResets.clear();
            env.printInfo(logger, name, "Requested to RESET ALL!");
        } else
            processMessages(queueResets, cntResets, this::resetValue);
    }


    private void resetAllValues() {
        etags.values().forEach(etag -> {
            if( etag.state == HAS_FUTURE ) {
                etag.future.cancel(true);
            }
            etag.state = NEED_SET;
        });
    }


    private void resetValue(String key) {
        ExportTag etag = etags.get(key);
        if( etag != null ) {
            if( etag.state == HAS_FUTURE ) {
                etag.future.cancel(true);
            }
            etag.state = NEED_SET;
        }
    }


    private void offerMessage(String message, Queue<String> queue, AtomicInteger counter) {
        if( counter.get() < MESSAGE_QUEUE_SIZE_MAX) {
            queue.offer(message);
            counter.incrementAndGet();
        }
    }


    private String pollMessage(Queue<String> queue, AtomicInteger counter) {
        String message = queue.poll();
        if( message != null ) {
            counter.decrementAndGet();
        }
        return message;
    }


    private void processMessages(Queue<String> queue, AtomicInteger counter, Consumer<String> handler) {
        while( counter.get() > 0 ) {
            String message = pollMessage(queue, counter);
            if( message != null )
                handler.accept(message);
        }
    }


    // ====== from RedisPubSubListener ======
    @Override
    public void message(String channel, String message) {
        if( channel.equals(channelWrite)) {
            if (!readonly)
                offerMessage(message, queueWrites, cntWrites);
        } else

        if( channel.equals(channelReset)) {
            if( message.isEmpty()) {
                needResetAll = true;
            } else {
                offerMessage(message, queueResets, cntResets);
            }
        }
    }


    @Override
    public void message(String pattern, String channel, String message) {
        // do nothing
    }

    @Override
    public void subscribed(String channel, long count) {
        // do nothing
    }

    @Override
    public void psubscribed(String pattern, long count) {
        // do nothing
    }

    @Override
    public void unsubscribed(String channel, long count) {
        // do nothing
    }

    @Override
    public void punsubscribed(String pattern, long count) {
        // do nothing
    }


    @Override
    public String getInfo() {

        return !enable? "disabled":
                String.format("%s%s%s filter=%s%s%s, tags=%d, %sset/wait=%d/%d write=%d%s"
                        , !tagConnected.getBool()? ANSI.RED + ANSI.BOLD + "ERROR! " + ANSI.RESET: ""
                        , uri.toString()
                        , descr.isEmpty()? "": " " + descr
                        , filter
                        , (readonly? " RO": "")
                        , (ssl? " SSL": "")
                        , etags.size()
                        , ANSI.GREEN
                        , cntUpdatingLast
                        , cntUpdatingWait
                        , cntWrites.get()
                        , ANSI.RESET
                );

    }


    @Override
    protected boolean reload() {
        closedownModule();
        load();
        prepareModule();
        return true;
    }

}
