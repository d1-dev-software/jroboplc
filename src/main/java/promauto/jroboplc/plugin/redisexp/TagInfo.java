package promauto.jroboplc.plugin.redisexp;

// version 1.1
public class TagInfo {
    public String name;
    public String data;

    public boolean good;
    public char type;
    public long time;
    public String value;
}
