package promauto.jroboplc.plugin.redisexp;

// version 1.4
public class TagInfoCodec {

    public static final String CHANNEL_UPDATE = "update";
    public static final String CHANNEL_DELETE = "delete";
    public static final String CHANNEL_WRITE = "write";
    public static final String CHANNEL_RESET = "reset";
    public static final String TAGS_KEY = "tags";
    public static final String SERVER_TIME_KEY = "server.time";

    private static final long MAX_HEX_DIGITS_11 = 0xfff_ffff_ffffL;


    // Encode name
    // format: len_hex#2 name#len
    public static String encodeName(String name) {
        return String.format("%02X%s", name.length(), name);
    }

    // Encode data
    // format: good#1  type#1  time_hex#11 space#1 value#?
    public static String encodeData(String value, long time, char type, boolean good) {
        return String.format( (good? "=%c%011X %s": "!%c%011X %s"), type, time & MAX_HEX_DIGITS_11, value);
    }

    // Decode TIME from DATA
    public static long decodeTimeFromData(String data) {
        try {
            return Long.parseLong(data.substring(2, 13), 16);
        } catch (NumberFormatException e) {
            return 0;
        }
        // todo: return optional
    }



    // Encode UPDATE message
    // format: len_hex#2 name#len  good#1  type#1  time_hex#11 space#1 value#?
    public static String encodeMessageUpdate(String encodedName, String encodedData) {
        return encodedName + encodedData;
    }

    // Decode UPDATE message
    public static TagInfo decodeMessageUpdate(String message) {
        try {
            int nlen = hexFirstByte(message);
            TagInfo result = new TagInfo();
            result.name = message.substring(2, nlen + 2);
            result.data = message.substring(nlen + 2);
            result.good = message.charAt(nlen + 2) != '!';
            result.type = message.charAt(nlen + 3);
            result.time = Long.parseLong(message.substring(nlen + 4, nlen + 15), 16);
            result.value = message.substring(nlen + 16);
            return result;
        } catch (NumberFormatException|IndexOutOfBoundsException e) {
            return null;
        }
        // todo: return optional
    }



    // Encode DELETE message
    // format: time_hex#11 space#1 name#?
    public static String encodeMessageDelete(String name, long time) {
        return String.format("%011X %s", time & MAX_HEX_DIGITS_11, name);
    }

    // Decode DELETE message
    public static TagInfo decodeMessageDelete(String message) {
        try {
            TagInfo result = new TagInfo();
            result.time = Long.parseLong(message.substring(0, 11), 16);
            result.name = message.substring(12);
            return result;
        } catch (NumberFormatException|IndexOutOfBoundsException e) {
            return null;
        }
        // todo: return optional
    }





    // Encode WRITE message
    // format: len_hex#2 name#len  space#1  value#?
    public static String encodeMessageWrite(String name, String value) {
        return String.format("%02X%s %s", name.length(), name, value);
    }

    // Decode WRITE message
    public static TagInfo decodeMessageWrite(String message) {
        try {
            int nlen = hexFirstByte(message);
            TagInfo result = new TagInfo();
            result.name = message.substring(2, nlen + 2);
            result.value = message.substring(nlen + 3);
            return result;
        } catch (IndexOutOfBoundsException e) {
            return null;
        }
        // todo: return optional
    }




    private static int hexFirstByte(String s) {
        return (hexDigit(s.charAt(0)) << 4) + hexDigit(s.charAt(1));
    }

    private static int hexDigit(char digit) {
        if( digit > 48  &&  digit < 58 )
            return digit - 48;

        if( digit > 64  &&  digit < 71 )
            return digit - 55;

        if( digit > 96  &&  digit < 103 )
            return digit - 87;

        return 0;
    }

}
