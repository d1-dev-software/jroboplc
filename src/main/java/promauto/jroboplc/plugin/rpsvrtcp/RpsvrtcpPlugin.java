package promauto.jroboplc.plugin.rpsvrtcp;

import promauto.jroboplc.core.AbstractPlugin;
import promauto.jroboplc.core.api.Module;

public class RpsvrtcpPlugin extends AbstractPlugin {
//	private final Logger logger = LoggerFactory.getLogger(RpsvrtcpPlugin.class);

	private static final String PLUGIN_NAME = "rpsvrtcp";
	
	@Override
	public void initialize() {
		super.initialize();
    }
	
	@Override
    public String getPluginName() {
    	return PLUGIN_NAME;
    }

	@Override
    public String getPluginDescription(){
    	return "RpSvrTcp server support.";
    }
	
//	@Override
//    public Class<?> getModuleClass(){
//    	return RpsvrtcpModule.class;
//    }
	

	@Override
	public Module createModule(String name, Object conf) {
		RpsvrtcpModule m = new RpsvrtcpModule(this, name);
    	modules.add(m);
    	return m.load(conf)? m: null;
	}

}
