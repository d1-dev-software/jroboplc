package promauto.jroboplc.plugin.rpsvrtcp;

import static promauto.utils.Strings.getFilterPattern;

import java.nio.charset.Charset;
import java.util.*;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import promauto.jroboplc.core.api.*;
import promauto.utils.CRC;
import promauto.utils.Numbers;

public class RpsvrtcpClient implements Signal.Listener {
	private final Logger logger = LoggerFactory.getLogger(RpsvrtcpClient.class);
	private final static Charset charset = Charset.forName("UTF-8");

	private Environment env;

	private final static int ANSWER_LENGTH_MAX = 8000; // 16000;
	private final static int ANSWER_LENGTH_MAX_32 = ANSWER_LENGTH_MAX - 32;

	protected String tagFilter = "*";
	private List<ClientTag> cltags = new ArrayList<>();
	private int cltagcount;

	protected RpsvrtcpModule module;
	
	private String crcTagValues;
	private int[] buffCrcTagValuesH;
	private int[] buffCrcTagValuesL;

	private StringBuilder sb = new StringBuilder();
	private String clientAddress;

    private volatile boolean hasMessageReload = false;


    public static class ClientTag {
		public Module module;
		public Tag tag;
		public Integer fixval;
		public boolean skipModuleName;

		/**
		 * State of a client tag record: <br>
		 * 0-not changed, 1-fixed, but not sent, 2-fixed and sent
		 */
		public int state;

		public ClientTag(Module module, boolean skipModuleName, Tag tag) {
			this.module = module;
			this.tag = tag;
			this.fixval = 0;
			this.state = 1;
			this.skipModuleName = skipModuleName;
		}
	}

	public RpsvrtcpClient(RpsvrtcpModule module, String clientAddress) {
		this.env = EnvironmentInst.get();
		this.module = module;
		this.clientAddress = clientAddress;
	}

	// SETFILTER
	public String cmdSETFILTER(String reqbody) {
		tagFilter = reqbody.replace('\'', ' ').trim();
		return "101 OK\r\n";
	}

	// GETFILTER
	public String cmdGETFILTER() {
		return "102 '" + tagFilter + "'\r\n";
	}

	// // ADDEXTRATAG
	// public String cmdADDEXTRATAG() {
	// return "400\r\nNOT SUPPORTED COMMAND\r\n";
	// }
	//
	// // CLEAREXTRALIST
	// public String cmdCLEAREXTRALIST() {
	// return "400\r\nNOT SUPPORTED COMMAND\r\n";
	// }

	// CREATETAGLIST
	public String cmdCREATETAGLIST() {

        /*
		Pattern filter = getFilterPattern(tagFilter, ";");
		List<ClientTag> list = new LinkedList<>();
		for (Module m : env.getModuleManager().getModules()) {
			boolean hasCompatibilityFlagHidden = module.isFlagCompatibleWith(Flags.HIDDEN, m); 
			boolean hasCompatibilityFlagExternal = module.isFlagCompatibleWith(Flags.EXTERNAL, m); 

			boolean transparent = module.transparentModules.contains(m.getName())  &&  hasCompatibilityFlagExternal;
			
			String flagSkipModName = m.getFlag("rpsvrtcp.skipmodname");
			boolean modeSkipModNameAll = flagSkipModName.equals("all");
			boolean modeSkipModNameExternal = flagSkipModName.equals("external");
			
			for (Tag tag : m.getTagTable().getTags()) {

				if (tag.getType() == Tag.Type.STRING)
					continue;

				if ( !module.hiddentags  &&  hasCompatibilityFlagHidden  &&  tag.hasFlag(Flags.HIDDEN) )
					continue;

				Module clientTagModule = m;
				boolean skipModuleName = false;
				if( transparent  &&  tag.hasFlag(Flags.EXTERNAL) ) 
					skipModuleName = true;
				else
					if( modeSkipModNameAll  ||  (modeSkipModNameExternal  &&  tag.hasFlag(Flags.EXTERNAL)) ) 
						skipModuleName = true;
				
				String tagname = getTagname(clientTagModule, skipModuleName, tag);
				if( filter != null  &&  !filter.matcher(tagname).matches() )
					continue;
				if (module.incl != null && !(module.incl.matcher(tagname).matches()))
					continue;
				if (module.excl != null && (module.excl.matcher(tagname).matches()))
					continue;

				list.add(new ClientTag(clientTagModule, skipModuleName, tag));
			}
		}
		*/

		cltags = new ArrayList<>( collectClientTags() );
		cltagcount = cltags.size();

//        subscribeSignals();
		addAsSignalListerToAllOthers();
        hasMessageReload = false;

		return "103 " + Numbers.toHexString(cltags.size()) + "\r\n";
	}


	private List<ClientTag> collectClientTags() {
        Pattern filter = getFilterPattern(tagFilter, ";");

        List<ClientTag> list = new LinkedList<>();
        for (Module m : env.getModuleManager().getModules()) {
            boolean hasCompatibilityFlagHidden = module.isFlagCompatibleWith("HIDDEN", m);
            boolean hasCompatibilityFlagExternal = module.isFlagCompatibleWith("EXTERNAL", m);

            boolean transparent = module.transparentModules.contains(m.getName())  &&  hasCompatibilityFlagExternal;

            String flagSkipModName = m.getFlag("RPSVRTCP.SKIPMODNAME");
            boolean modeSkipModNameAll = flagSkipModName.equals("ALL");
            boolean modeSkipModNameExternal = flagSkipModName.equals("EXTERNAL");

            for (Tag tag : m.getTagTable().values()) {

                if (tag.getType() == Tag.Type.STRING)
                    continue;

                if ( !module.hiddentags  &&  hasCompatibilityFlagHidden  &&  tag.hasFlags(Flags.HIDDEN) )
                    continue;

				boolean skipModuleName = false;
                if( transparent  &&  tag.hasFlags(Flags.EXTERNAL) )
                    skipModuleName = true;
                else
                if( modeSkipModNameAll  ||  (modeSkipModNameExternal  &&  tag.hasFlags(Flags.EXTERNAL)) )
                    skipModuleName = true;

                String tagname = getTagname(m, skipModuleName, tag);
                if( filter != null  &&  !filter.matcher(tagname).matches() )
                    continue;
                if (module.incl != null && !(module.incl.matcher(tagname).matches()))
                    continue;
                if (module.excl != null && (module.excl.matcher(tagname).matches()))
                    continue;

                list.add(new ClientTag(m, skipModuleName, tag));
            }
        }

        return list;
    }


	private String getTagname(ClientTag cltag) {
		return getTagname(cltag.module, cltag.skipModuleName, cltag.tag);
	}


	private String getTagname(Module m, boolean skipModuleName, Tag tag) {
		return  (skipModuleName? "": m.getName() + ".") + tag.getName();
	}

	// GETTAGLIST
	public String cmdGETTAGLIST(String reqbody) {

		sb.setLength(0);
		try {
			int offset = Integer.parseInt(reqbody, 16);
			int size = cltags.size();
			int n = 0;
			int i;

			sb.append('#');
			sb.append(Numbers.toHexString(offset));
			sb.append('!');

			boolean flag = false;
			for (i = offset; i < size; i++) {
				if (sb.length() > ANSWER_LENGTH_MAX_32)
					break;

				if (flag)
					sb.append(';');
				else
					flag = true;

				sb.append(getTagname(cltags.get(i)));
				n++;
			}

			sb.insert(0, Numbers.toHexString(n));

			int crc = CRC.getCrc16(sb.toString().getBytes(charset));
			sb.append((i == size) ? '=' : '~');
			sb.append(Numbers.toHexString(crc));
			sb.append("\r\n");
			sb.insert(0, "104 ");
		} catch (NumberFormatException e) {
			return "?\r\n";
		}

		return sb.toString();
	}

	// FIXALL
	public String cmdFIXALL() {
		if (buffCrcTagValuesH == null || buffCrcTagValuesH.length != cltags.size()) {
			buffCrcTagValuesH = new int[cltags.size()];
			buffCrcTagValuesL = new int[cltags.size()];
		}

		int n = 0;
		int i = 0;
		int v;
		for (ClientTag cltag : cltags) {
			v = cltag.tag.getInt();

			if (cltag.fixval != v) {
				cltag.fixval = v;
				cltag.state = 1;
			}

			if (cltag.state == 1)
				n++;
			else if (cltag.state == 2)
				cltag.state = 0;

			buffCrcTagValuesH[i] = (v >> 8) & 0xFF;
			buffCrcTagValuesL[i] = v & 0xFF;
			++i;
		}

		long crcH = CRC.getCrc16(buffCrcTagValuesH, cltags.size());
		long crcL = CRC.getCrc16(buffCrcTagValuesL, cltags.size());
		long crc = (crcH << 16) + crcL;
		sb.setLength(0);
		for (int j = 0; j < 8; ++j) {
			sb.insert(0, Numbers.hexDigitChar[(int) (crc & 0xF)]);
			crc = crc >>> 4;
		}
		crcTagValues = sb.toString();

		return "105 " + Numbers.toHexString(n) + "\r\n";
	}

    // GETCRC
    public String cmdGETCRC() {
        return "118 " + crcTagValues + "\r\n";
    }


    // GETMSG
    public String cmdGETMSG() {
        sb.setLength(0);
        sb.append("119 ");

        if( hasMessageReload )
            sb.append("RELOAD");

        sb.append("\r\n");

//        System.out.println( sb.toString() ); // reload scr
        return sb.toString();
    }


    // GETALL
	public String cmdGETALL(String reqbody) {
		sb.setLength(0);
		try {
			int offset = Integer.parseInt(reqbody, 16);
			int size = cltags.size();
			int n = 0;
			int i;

			sb.append('#');
			sb.append(Numbers.toHexString(offset));
			sb.append('!');

			boolean flag = false;
			for (i = offset; i < size; i++) {
				if (sb.length() > ANSWER_LENGTH_MAX_32)
					break;

				if (flag)
					sb.append(';');
				else
					flag = true;

				if (cltags.get(i).fixval != 0) {
			        if(cltags.get(i).fixval < 0) {
			        	sb.append('-');
						sb.append(Numbers.toHexString( Math.abs( cltags.get(i).fixval )));
			        } else
			        	sb.append(Numbers.toHexString(cltags.get(i).fixval));
				}
//					sb.append(Numbers.toHexString(cltags.get(i).fixval));

				n++;
			}

			sb.insert(0, Numbers.toHexString(n));

			int crc = CRC.getCrc16(sb.toString().getBytes(charset));
			sb.append((i == size) ? '=' : '~');
			sb.append(Numbers.toHexString(crc));
			sb.append("\r\n");
			sb.insert(0, "106 ");
		} catch (NumberFormatException e) {
			return "?\r\n";
		}
		
//	   logger.debug( "[cmdGETALL] " + module.getName() + " " + reqbody);


		return sb.toString();
	}

	public String cmdGETCHG(String reqbody) {
		sb.setLength(0);
		try {
			int offset = Integer.parseInt(reqbody, 16);
			int size = cltags.size();
			int n = 0;
			int i;

			StringBuilder ssb = null;

			boolean flag = false;
			for (i = offset; i < size; i++) {

				if (cltags.get(i).state == 0) {
					if (ssb != null) {
						sb.append(ssb);
						ssb = null;
					}
					continue;
				}

				if ((sb.length() + (ssb == null ? 0 : ssb.length())) > ANSWER_LENGTH_MAX_32)
					break;

				if (ssb == null) {
					ssb = new StringBuilder();
					ssb.append('#');
					ssb.append(Numbers.toHexString(i));
					ssb.append('!');
					flag = false;
				}

				if (flag)
					ssb.append(';');
				else
					flag = true;

				cltags.get(i).state = 2;
				if( cltags.get(i).fixval != 0 ) {
					if( cltags.get(i).fixval < 0 ) {
						ssb.append('-');
						ssb.append(Numbers.toHexString( Math.abs( cltags.get(i).fixval) ));
					} else
						ssb.append(Numbers.toHexString(cltags.get(i).fixval));
				}

				n++;
			}
			if (ssb != null)
				sb.append(ssb);

			sb.insert(0, Numbers.toHexString(n));

			int crc = CRC.getCrc16(sb.toString().getBytes(charset));
			sb.append((i == size) ? '=' : '~');
			sb.append(Numbers.toHexString(crc));
			sb.append("\r\n");
			sb.insert(0, "107 ");
		} catch (NumberFormatException e) {
			return "?\r\n";
		}

		return sb.toString();
	}

	// WNM
	public String cmdWNM(String reqbody) {
		try {
			int k1 = reqbody.indexOf(' ');
			int k2 = reqbody.indexOf(' ', k1 + 1);

			int crc1 = CRC.getCrc16(reqbody.substring(0, k2).getBytes(charset));

			String crcstr = reqbody.substring(k2 + 1);
			int crc2;
			if (crcstr.equals("****"))
				crc2 = crc1;
			else
				crc2 = Integer.parseInt(crcstr, 16);
			int index = Integer.parseInt(reqbody.substring(0, k1), 16);
			Integer value = Integer.parseInt(reqbody.substring(k1 + 1, k2), 16);

			if (crc1 != crc2)
				throw new NumberFormatException();

			if ((index >= 0) && (index < cltagcount)) {
	        	if( module.debugInfoWrite  &&  module.debugConsole != null) {
	        		Tag tag = cltags.get(index).tag;
	        		
//	        		ClientTag cltag = cltags.get(index);
//	        		String sss = module.getName() + " write: ";
//	        		sss = sss + cltag.module.getName() + ":";
//	        		sss = sss + tag.getName() + " = "+ tag.getInt() + " -> " + value + "\r\n";
//	        		module.debugConsole.print(sss); 

	        		module.debugConsole.print(module.getName() + " write: " + 
	        				cltags.get(index).module.getName() + ":" + tag.getName() + " = "+ tag.getInt() + " -> " + value + "\r\n");
	        	}

	        	cltags.get(index).tag.setInt(value);
			}

			return "109 !\r\n";

		} catch (NumberFormatException e) {
			return "109 ?\r\n";
		}
	}
	
	
	// GETPROPS
	public String cmdGETPROPS(String reqbody) {
		sb.setLength(0);
		try {
			int offset = Integer.parseInt(reqbody, 16);
			int size = cltags.size();
			int n = 0;
			int i;

			sb.append('#');
			sb.append(Numbers.toHexString(offset));
			sb.append('!');

			boolean flag = false;
			for (i = offset; i < size; i++) {
				if (sb.length() > ANSWER_LENGTH_MAX_32)
					break;

				if (flag)
					sb.append(';');
				else
					flag = true;

				Tag auxtag = cltags.get(i).tag.getObject();
				if( auxtag != null  &&  auxtag.getName().equals("IRR") )
					sb.append('1');

				n++;
			}

			sb.insert(0, Numbers.toHexString(n));

			int crc = CRC.getCrc16(sb.toString().getBytes(charset));
			sb.append((i == size) ? '=' : '~');
			sb.append(Numbers.toHexString(crc));
			sb.append("\r\n");
			sb.insert(0, "116 ");
		} catch (NumberFormatException e) {
			return "?\r\n";
		}

		return sb.toString();
	}


	
	// SETFLAG
	public String cmdSETFLAG(String reqbody) {
		try {
			int k1 = reqbody.indexOf(' ');
			int k2 = reqbody.indexOf(' ', k1 + 1);

			int crc1 = CRC.getCrc16(reqbody.substring(0, k2).getBytes(charset));

			String crcstr = reqbody.substring(k2 + 1);
			int crc2;
			if (crcstr.equals("****"))
				crc2 = crc1;
			else
				crc2 = Integer.parseInt(crcstr, 16);
			int index = Integer.parseInt(reqbody.substring(0, k1), 16);
			Integer value = Integer.parseInt(reqbody.substring(k1 + 1, k2), 16);

			if (crc1 != crc2)
				throw new NumberFormatException();

			if ( index >= 0  &&  index < cltagcount ) {
				Tag auxtag = cltags.get(index).tag.getObject();
				if( auxtag != null  &&  auxtag.getName().equals("IRR") ) {
					auxtag.setInt(value);
					if( module.infoSetFlag )
						env.printInfo(logger, module.getName(), "SETFLAG " + cltags.get(index).tag.getName()  + " " + value);
					return "117 !\r\n";
				}
			}

		} catch (NumberFormatException e) {
		}
		return "117 ?\r\n";
	}


	public void onRequest(TcpServerChannel channel, String request) {
		String reqcmd = "";
		String reqbody = "";
		String answer = null;

		int k = request.indexOf(' ');
		int l = request.length();
		if (k > 0) {
			reqcmd = request.substring(0, k);
			if (k < l)
				reqbody = request.substring(k + 1, l);
		} else
			reqcmd = request;

		switch (reqcmd) {
		case "FIXALL":
			answer = cmdFIXALL();
			break;
		case "GETCHG":
			answer = cmdGETCHG(reqbody);
			break;
		case "WNM":
			answer = cmdWNM(reqbody);
			break;
		case "GETALL":
			answer = cmdGETALL(reqbody);
			break;
		case "CREATETAGLIST":
			answer = cmdCREATETAGLIST();
			break;
		case "GETTAGLIST":
			answer = cmdGETTAGLIST(reqbody);
			break;
		case "SETFILTER":
			answer = cmdSETFILTER(reqbody);
			break;
		case "GETFILTER":
			answer = cmdGETFILTER();
			break;
		case "GETPROPS":
			answer = cmdGETPROPS(reqbody);
			break;
		case "SETFLAG":
			answer = cmdSETFLAG(reqbody);
			break;
		case "GETCRC":
			answer = cmdGETCRC();
			break;

        case "GETMSG":
            answer = cmdGETMSG();
            break;

		default:
			answer = "400 NOT SUPPORTED\r\n";
		}

		channel.write(answer);

		// unsupported commands
		// ADDEXTRATAG
		// CLEAREXTRALIST
		// RNM
		// GETTAGNAMENM

	}

	public String getInfo() {
		return clientAddress + " tags=" + cltagcount + " filter=" + tagFilter;
	}


    @Override
    public void onSignal(Module sender, Signal signal) {
        if( signal.type == Signal.SignalType.RELOADED ) {
            List<ClientTag> list = collectClientTags();
            if( cltags.size() != list.size() ) {
                hasMessageReload = true;
            } else
                for(int i=0; i<list.size(); ++i)
                    if( list.get(i).tag != cltags.get(i).tag ) {
                        hasMessageReload = true;
                        break;
                    }
        }

    }


    public void release() {
//        unsubscribeSignals();
		removeAsSignalListerFromAllOthers();
    }


//    private void subscribeSignals() {
//        for(Module m : env.getModuleManager().getModules())
//            m.addSignalListener(this);
//
//    }
//
//
//    private void unsubscribeSignals() {
//        for(Module m : env.getModuleManager().getModules())
//            m.removeSignalListener(this);
//    }


	List<ClientTag> getCltags() {
		return cltags;
	}

	void addCltag(ClientTag cltag) {
    	cltags.add(cltag);
    	cltagcount = cltags.size();
	}

}
