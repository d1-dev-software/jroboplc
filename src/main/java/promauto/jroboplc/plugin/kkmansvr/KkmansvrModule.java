package promauto.jroboplc.plugin.kkmansvr;


import java.sql.Statement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import promauto.jroboplc.core.AbstractModule;
import promauto.jroboplc.core.api.*;
import promauto.jroboplc.core.tags.TagRW;

import static promauto.jroboplc.core.api.Signal.SignalType.*;

public class KkmansvrModule extends AbstractModule implements Signal.Listener {
	private final Logger logger = LoggerFactory.getLogger(KkmansvrModule.class);

	public static final String DBSCR_KKMANSVR = "dbscr/dbscr.kkmansvr.yml";
	public static final String KKMANSVR_INIT_1 = "kkmansvr.init1";
	public static final String KKMANSVR_INIT_2 = "kkmansvr.init2";

	public static final String TABLE_REPO = "KM_REPO";

	private DataService service = new DataServiceImpl();

	private Line line;
	private TagRepository repo;
	private Database database;
	private String databaseModuleName;
	private boolean connected;
	private boolean emulated;
	private boolean needInit;

	private Tag tagConnected;

	public KkmansvrModule(Plugin plugin, String name) {
        super(plugin, name);
		line = new Line(service, name);
//		env.getCmdDispatcher().addCommand(this, CmdImport.class);
	}

	public TagRepository getRepo() {
		return repo;
	}


	public final boolean loadModule(Object conf) {
		Configuration cm = env.getConfiguration();
		emulated 	= cm.get(conf, "emulate", false);
		databaseModuleName = cm.get(conf, "database", "db");
		line.load(conf, tagtable);

		tagConnected = tagtable.createBool("connected", false);
		return true;
	}
	

	@Override
	public final boolean prepareModule() {
		if( emulated )
			return true;

		database = env.getModuleManager().getModule(databaseModuleName, Database.class);
		if (database == null) {
			env.printError(logger, name, "Database not found:", databaseModuleName);
			return false;
		}

		database.addSignalListener(this);
		service.setDb(database);
		repo = database.createTagRepository("", TABLE_REPO);
		line.setRepo(repo);

		if (database.loadScriptFromResource(KkmansvrPlugin.class, DBSCR_KKMANSVR) == null)
			return false;

		needInit = true;
		return true;
	}


	@Override
	public void onSignal(Module sender, Signal signal) {
		if (signal.type == CONNECTED  &&  !connected) {
			needInit = true;
			postSignal(RELOADED);
		}

		if (signal.type == DISCONNECTED) {
			tagConnected.setBool(connected = false);
		}
	}


	private void init() {
		tagConnected.setBool( connected = false );
		if( emulated  ||  database == null  ||  !database.isConnected())
			return;

	 	connected =
				database.executeScript(KKMANSVR_INIT_1).success &&
				database.executeScript(KKMANSVR_INIT_2).success;

		try(Statement st = service.createStatement()) {
			connected &=
					repo.init() &&
					line.init();

			service.commit();
		} catch (Exception e) {
			env.printError(logger, e, name);
			service.rollback();
		}

		if(!connected)
			env.printError(logger, name, "Initialization error");

		tagConnected.setBool(connected);
		needInit = false;
	}


	@Override
	public boolean executeModule() {
		if( emulated ) {
			tagtable.getMap().values().stream()
					.filter(tag -> tag instanceof TagRW)
					.forEach(tag -> ((TagRW) tag).acceptWriteValue());
			return true;
		}

		if( database == null  ||  !database.isConnected())
			return true;

		if( needInit )
			init();

		if( connected ) {
			try(Statement st = service.createStatement()) {
				line.execute();
				repo.persist();

				service.commit();
			} catch (Exception e) {
				env.printError(logger, e, name);
				service.rollback();
				tagConnected.setBool( connected = false );
				needInit = true;
			}
		}
		return true;
	}


	@Override
	public boolean closedownModule() {
		tagConnected.setBool(connected = false);
		if (database != null)
			database.removeSignalListener(this);
		return true;
	}


	@Override
	public String getInfo() {
		if( !enable )
			return "disabled";

		if( emulated )
			return "emulated";

		return String.format("%s%s, %s\r\n%s",
				(connected? "": ANSI.redBold("NOT CONNECTED! ")),
				line.getInfoLine(),
				databaseModuleName,
				line.getInfoDosers()
		);

	}


	@Override
	public String check() {
		return line.check();
	}


	@Override
	protected boolean reload() {
		KkmansvrModule tmp = new KkmansvrModule( plugin, name );
		if( !tmp.load() )
			return false;

		closedown();

		copySettingsFrom(tmp);

		tagtable.removeAll();
		tmp.tagtable.values().forEach(tag -> tagtable.add(tag));
		tmp.tagtable.clear();

		tagConnected = tmp.tagConnected;
		service = tmp.service;
		line = tmp.line;
		prepare();
		return true;
	}


}

