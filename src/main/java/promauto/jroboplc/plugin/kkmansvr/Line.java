package promauto.jroboplc.plugin.kkmansvr;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import promauto.jroboplc.core.TagTable;
import promauto.jroboplc.core.api.*;
import promauto.jroboplc.core.tags.TagRW;

import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

public class Line {
    public static final String NOT_FOUND = "NOT FOUND";
    private final Logger logger = LoggerFactory.getLogger(Line.class);

    public static final String TASK_INSTALL_OK = "OK";
    public static final String TASK_INSTALL_NOT_EMPTY = "NOT EMPTY";
    public static final String TASK_INSTALL_LINK_ERROR = "LINK ERROR";
    public static final String TASK_INSTALL_WRONG_LINE = "WRONG LINE";
    public static final String TASK_INSTALL_NOT_FOUND = "NOT FOUND";
    public static final String TASK_INSTALL_TOO_MANY_PRODUCTS = "TOO MANY PRODUCTS";
    public static final String TASK_INSTALL_RESET = "RESET";

    public static final String CRC_ERROR = "CRC ERROR";

    final static int LINE_IDLE = 0;
    final static int LINE_PREPARING = 1;
    final static int LINE_LOADING = 2;
    final static int LINE_COMMITTING = 3;
    final static int LINE_UNLOADING = 4;

    final List<Tag> repoTags = new ArrayList<>();
    final List<Component> comps = new ArrayList<>();
    final List<Doser> dosers = new ArrayList<>();
    final Map<Integer, Doser> idDosers = new HashMap<>();
    final DataService service;
    final String modname;

    private int lineId;
    private String lineName;

    private TagRW tagTaskInstall;
    private TagRW tagTaskId;
    private TagRW tagRecipeId;
    private TagRW tagRecipeName;
    private Tag   tagCycleReq;
    private TagRW tagCycleCnt;
    private TagRW tagDisable;
    private TagRW tagUnloadStart;
    private Tag   tagUnloadFinished;
    private TagRW tagUnloadTimerCnt;
    private Tag   tagUnloadTimerSet;
    private TagRW tagRun;
    private TagRW tagCancel;
    private TagRW tagCommit;
    private TagRW tagStatus;
    private TagRW tagSumWeightReq;
    private TagRW tagSumWeightFin;
    private TagRW tagSumWeightCtl;
    private TagRW tagControlId;
    private TagRW tagControlName;
    private TagRW tagControlDescr;
    private TagRW tagControlWeight;
    private TagRW tagControlStable;

    private TagRepository repo;


    public Line(DataService service, String modname) {
        this.service = service;
        this.modname = modname;
    }


    public boolean load(Object conf, TagTable tt) {
        Configuration cm = EnvironmentInst.get().getConfiguration();
        lineId   = cm.get(conf, "lineId", 0);
        lineName = cm.get(conf, "lineName", "Line" + lineId);
        int compQnt = cm.get(conf, "compQnt", 8);
        loadTags(tt);

        dosers.clear();
        cm.toList(cm.get(conf, "dosers")).forEach(conf1 -> {
            Doser doser = new Doser();
            doser.load(conf1);
            dosers.add(doser);
        });

        comps.clear();
        for(int i = 0; i< compQnt; ++i) {
            Component comp = new Component(this, i);
            comp.loadTags(tt);
            comps.add(comp);
        }

        return true;
    }


    private void loadTags(TagTable tt) {
        repoTags.clear();
                                           tt.createRWInt(    "LineId",             lineId);
                                           tt.createRWString( "LineName",           lineName);
        repoTags.add( tagTaskInstall     = tt.createRWString( "TaskInstall",   ""));
        repoTags.add( tagTaskId          = tt.createRWInt(    "TaskId",        0));
        repoTags.add( tagRecipeId        = tt.createRWInt(    "RecipeId",     0));
        repoTags.add( tagRecipeName      = tt.createRWString( "RecipeName",   ""));
        repoTags.add( tagCycleReq        = tt.createInt(      "CycleReq",      0));
        repoTags.add( tagCycleCnt        = tt.createRWInt(    "CycleCnt",      0));
        repoTags.add( tagDisable         = tt.createRWBool(   "Disable",       false));
        repoTags.add( tagUnloadStart     = tt.createRWBool(   "UnloadStart",   false));
        repoTags.add( tagUnloadFinished  = tt.createBool(     "UnloadFinished",false));
        repoTags.add( tagUnloadTimerCnt  = tt.createRWInt(    "UnloadTimerCnt",0));
        repoTags.add( tagUnloadTimerSet  = tt.createInt(      "UnloadTimerSet",0));
        repoTags.add( tagRun             = tt.createRWBool(   "Run",           false));
        repoTags.add( tagStatus          = tt.createRWInt(    "Status",        0));
        repoTags.add( tagSumWeightReq    = tt.createRWInt(    "SumWeightReq",  0));
        repoTags.add( tagSumWeightFin    = tt.createRWInt(    "SumWeightFin",  0));
        repoTags.add( tagSumWeightCtl    = tt.createRWInt(    "SumWeightCtl",  0));
        repoTags.add( tagControlId       = tt.createRWInt(    "ControlId",     0));
        repoTags.add( tagControlName     = tt.createRWString( "ControlName",   ""));
        repoTags.add( tagControlDescr    = tt.createRWString( "ControlDescr",  ""));
        repoTags.add( tagControlStable   = tt.createRWBool(   "ControlStable", false));
        tagControlWeight                 = tt.createRWInt(    "ControlWeight", 0);
        tagCancel                        = tt.createRWBool(   "Cancel",        false);
        tagCommit                        = tt.createRWBool(   "Commit",        false);
    }


    public boolean init() throws SQLException {
        repo.load(modname, repoTags);
        comps.forEach(comp -> repo.load(modname, comp.repoTags));

        service.syncLine(lineId, lineName);

        idDosers.clear();
        for(Doser doser: dosers) {
            doser.init();
            int doserId = service.syncDoser(lineId, doser.getName(), doser.getDescr(), doser.isCanControl());
            doser.setDoserId(doserId);
            idDosers.put(doserId, doser);
        }

        service.deleteOtherDosers(lineId, idDosers.keySet());
        return true;
    }


    // line loop
    public void execute() throws SQLException {
        if( !readDosers() )
            return;

        updateLinkedTags();

        // cancel
        if( tagCancel.hasWriteValue()  &&  tagCancel.getWriteValBool() ) {
            cancelTask();
        }

        // disable
        if( tagDisable.hasWriteValue() ) {
            if( tagDisable.getWriteValBool()  &&  isStatus(LINE_IDLE) )
                tagDisable.setReadValBool(true);

            if( !tagDisable.getWriteValBool() )
                tagDisable.setReadValBool(false);
        }

        // install
        if( tagTaskInstall.hasWriteValue()  &&  isStatus(LINE_IDLE) ) {
            if( tagTaskInstall.getWriteValString().equalsIgnoreCase(TASK_INSTALL_RESET)) {
                resetTask();
            } else if( tagTaskInstall.getWriteValString().isEmpty()  ) {
                tagTaskInstall.setReadValString("");
            } else if( tagTaskInstall.getString().isEmpty()  ) {
                doTaskInstall( tagTaskInstall.getWriteValInt());
            } else {
                tagTaskInstall.setReadValString(TASK_INSTALL_NOT_EMPTY);
            }
        }

        // run
        if( tagRun.hasWriteValue()  &&  tagRun.getWriteValBool()  &&  isStatus(LINE_IDLE) ) {
            if( !tagDisable.getBool() )
                if( tagTaskId.getInt() > 0 )
                    if( tagCycleReq.getInt() == 0  ||  tagCycleCnt.getInt() < tagCycleReq.getInt() ) {
                        tagRun.setReadValBool(true);
                        setStatus(LINE_PREPARING);
                    }
        }

        if( isStatus(LINE_IDLE) ) {
            resetCycle();
        }

        if( isStatus(LINE_PREPARING) ) {
            if( doTaskPreparing() )
                setStatus(LINE_LOADING);
        }

        if( isStatus(LINE_LOADING) ) {
            if( doTaskLoading() )
                setStatus(LINE_COMMITTING);
        }

        if( tagCommit.hasWriteValue()  &&  isStatus(LINE_COMMITTING) ) {
            if( doTaskCommitting() )
                setStatus(LINE_UNLOADING);
        }

        if( isStatus(LINE_UNLOADING) ) {
            if( doTaskUnloading() )
                setStatus(LINE_IDLE);
        }

        // execute components
        comps.forEach(Component::execute);

        // calc sums
        calcSumWeights();

        // save repo tags
        repo.save(modname, repoTags);
        comps.forEach(comp -> repo.save(modname, comp.repoTags));
    }


    public void setRepo(TagRepository repo) {
        this.repo = repo;
    }


    public Doser getDoser(Tag tagDoserId) {
        if( tagDoserId == null  ||  tagDoserId.getInt() == 0 )
            return null;
        return idDosers.get(tagDoserId.getInt());
    }


    private void calcSumWeights() {
        tagSumWeightReq.setReadValInt( comps.stream()
                .mapToInt(Component::getWeightReq)
                .sum() );

        tagSumWeightFin.setReadValInt( comps.stream()
                .mapToInt(Component::getWeightFin)
                .sum() );
    }


    public boolean isStatus(int status) {
        return tagStatus.getInt() == status;
    }


    private void setStatus(int status) {
        tagStatus.setReadValInt(status);
    }


    private void updateLinkedTags() {
        updateControlDoserTags();
        comps.forEach(Component::updateDoserTags);
    }


    private boolean readDosers() {
        for(Doser doser: dosers) {
            if( !doser.refgr.link() ) {
                tagTaskInstall.setReadValString(TASK_INSTALL_LINK_ERROR);
                return false;
            }
            doser.refgr.read();
            if( !doser.refgr.checkCrc32() ) {
                EnvironmentInst.get().printError(logger, lineName, doser.getName(), CRC_ERROR);
                return false;
            }
        }
        return true;
    }


    private void updateControlDoserTags() {
        if( tagControlId.getInt() == 0) {
            tagControlName.setReadValString("");
            tagControlDescr.setReadValString("");
            tagControlWeight.setReadValInt(0);
            tagControlStable.setReadValBool(false);
        } else {
            Doser doser = getDoser(tagControlId);
            if (doser == null) {
                tagControlName.setReadValString(NOT_FOUND);
                tagControlDescr.setReadValString(NOT_FOUND);
                tagControlWeight.setReadValInt(0);
                tagControlStable.setReadValBool(false);
            } else {
                tagControlName.setReadValString(doser.getName());
                tagControlDescr.setReadValString(doser.getDescr());
                tagControlWeight.setReadValInt(doser.getWeightTot());
                tagControlStable.setReadValBool(doser.getStable());
            }
        }
    }


    private void cancelTask() {
        setStatus(LINE_IDLE);
        tagRun.setReadValBool(false);
        tagCancel.setBool(false);
        resetCycle();
    }


    private void resetCycle() {
        tagUnloadStart.setReadValBool(false);
        tagUnloadTimerCnt.setReadValInt(0);
        tagSumWeightCtl.setReadValInt(0);
        comps.forEach(Component::resetCycle);
    }


    private void resetTask() {
        tagTaskId.setReadValInt(0);
        tagRecipeId.setReadValInt(0);
        tagRecipeName.setReadValString("");
        tagControlId.setReadValInt(0);
        tagCycleReq.setInt(0);
        comps.forEach(Component::resetTask);
        tagTaskInstall.setReadValString("");
    }


    private void doTaskInstall(int taskId) throws SQLException {
        Task task = service.findTask(taskId);
        if (task == null) {
            tagTaskInstall.setReadValString(TASK_INSTALL_NOT_FOUND);
            return;
        }

        if (task.lineId != lineId) {
            tagTaskInstall.setReadValString(TASK_INSTALL_WRONG_LINE);
            return;
        }

        if (task.products.size() > comps.size()) {
            tagTaskInstall.setReadValString(TASK_INSTALL_TOO_MANY_PRODUCTS);
            return;
        }

        tagTaskId.setReadValInt(task.taskId);
        tagRecipeId.setReadValInt(task.recipeId);
        tagRecipeName.setReadValString(task.recipeName);
        tagControlId.setReadValInt(task.controlId);
        tagCycleReq.setInt(task.cycleReq);
        tagCycleCnt.setReadValInt(task.cycleCnt);

        for (int i = 0; i < task.products.size(); ++i) {
            Component comp = comps.get(i);
            Task.Product tp = task.products.get(i);
            comp.setProductId(tp.productId);
            comp.setProductName(tp.productName);
            comp.setDoserId(tp.doserId);
            comp.setWeightReq(tp.weightReq);
        }

        for (int i = task.products.size(); i < comps.size(); ++i) {
            Component comp = comps.get(i);
            comp.setProductId(0);
            comp.setProductName("");
            comp.setDoserId(0);
            comp.setWeightReq(0);
        }

        tagTaskInstall.setReadValString(TASK_INSTALL_OK);
        updateLinkedTags();
    }


    private boolean doTaskPreparing() {
        resetCycle();
        if( tagControlId.getInt() == 0)
            return true;

        Doser doser = getDoser(tagControlId);
        if( doser == null)
            return false;
        doser.setEmptyTot(true);
        return doser.getIsEmptyTot();
    }


    private boolean doTaskLoading() {
        return comps.stream()
                .filter(Component::isComponentInUse)
                .allMatch(Component::isFinished);
    }


    private boolean doTaskCommitting() throws SQLException {
        if( !tagCommit.getWriteValBool() )
            return false;

        if( tagControlId.getInt() > 0  &&  !tagControlStable.getBool() )
            return false;

        tagCycleCnt.setReadValInt( tagCycleCnt.getInt() + 1);
        tagSumWeightCtl.setReadValInt( tagControlWeight.getInt() );
        tagUnloadTimerCnt.setReadValInt(0);

        Cycle cycle = new Cycle();
        cycle.taskId = tagTaskId.getInt();
        cycle.dt = LocalDateTime.now();
        cycle.cycleCnt = tagCycleCnt.getInt();
        cycle.weightReq = tagSumWeightReq.getInt();
        cycle.weightFin = tagSumWeightFin.getInt();
        cycle.weightCtl = tagSumWeightCtl.getInt();
        cycle.products = new ArrayList<>();

        comps.stream()
                .filter(comp -> comp.getWeightReq() > 0  &&  comp.getWeightFin() > 0  &&  comp.isFinished())
                .forEach(comp -> {
                    Cycle.Product cp = new Cycle.Product();
                    cp.productId = comp.getProductId();
                    cp.weightReq = comp.getWeightReq();
                    cp.weightFin = comp.getWeightFin();
                    cycle.products.add(cp);
                });

        service.saveCycle(cycle);

        return true;
    }


    private boolean doTaskUnloading() {
        tagUnloadStart.setReadValBool(true);

        if( tagUnloadTimerCnt.getInt() < tagUnloadTimerSet.getInt() ) {
            tagUnloadTimerCnt.setReadValInt( tagUnloadTimerCnt.getInt() + 1);
            return false;
        }

        if( !tagUnloadFinished.getBool() )
            return false;

        tagRun.setReadValBool(false);
        tagUnloadFinished.setBool(false);
        resetCycle();
        return true;
    }


    public String getInfoLine() {
        return String.format("%s (%d), comp=%d",
                lineName,
                lineId,
                comps.size()
        );
    }

    public String getInfoDosers() {
        return String.format("dosers:\r\n%s",
                dosers.stream()
                        .map(Doser::getInfo)
                        .collect(Collectors.joining("\r\n"))
        );
    }


    public String check() {
        return dosers.stream()
                .map(Doser::check)
                .collect(Collectors.joining("\r\n"));
    }


}
