package promauto.jroboplc.plugin.kkmansvr;

import promauto.jroboplc.core.TagTable;
import promauto.jroboplc.core.api.Tag;
import promauto.jroboplc.core.tags.TagRW;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import static promauto.jroboplc.plugin.kkmansvr.Line.LINE_LOADING;

public class Component {

    private final static int COMP_IDLE       = 0;
    private final static int COMP_PREPARING  = 1;
    private final static int COMP_LOADING    = 2;
    private final static int COMP_FINISHED   = 3;
    public static final String NOT_FOUND = "NOT FOUND";

    final List<Tag> repoTags = new ArrayList<>();

    public final Line line;
    public final int num;


    private TagRW tagDoserId;
    private TagRW tagDoserName;
    private TagRW tagDoserDescr;
    private TagRW tagManual;
    private TagRW tagProdId;
    private TagRW tagProdName;
    private TagRW tagWeightReq;
    private TagRW tagWeightFin;
    private TagRW tagWeightCur;
    private TagRW tagStatus;
    private TagRW tagRun;
    private TagRW tagCancel;


    public Component(Line line, int num) {
        this.line = line;
        this.num = num;
    }


    public void loadTags(TagTable tt) {
        repoTags.clear();
        String name = "Comp" + num;

        tagCancel                     = tt.createRWBool(  name + ".Cancel",       false);
        tagWeightCur                  = tt.createRWInt(   name + ".WeightCur",    0);
        repoTags.add( tagDoserId      = tt.createRWInt(   name + ".DoserId",     0));
        repoTags.add( tagDoserName    = tt.createRWString(name + ".DoserName",   ""));
        repoTags.add( tagDoserDescr   = tt.createRWString(name + ".DoserDescr",   ""));
        repoTags.add( tagManual       = tt.createRWBool(  name + ".Manual",       false));
        repoTags.add( tagProdId       = tt.createRWInt(   name + ".ProdId",       0));
        repoTags.add( tagProdName     = tt.createRWString(name + ".ProdName",     ""));
        repoTags.add( tagWeightReq    = tt.createRWInt(   name + ".WeightReq",    0));
        repoTags.add( tagWeightFin    = tt.createRWInt(   name + ".WeightFin",    0));
        repoTags.add( tagStatus       = tt.createRWInt(   name + ".Status",       0));
        repoTags.add( tagRun          = tt.createRWBool(  name + ".Run",          false));

    }

    // comp loop
    public void execute() {

        if( tagCancel.hasWriteValue()  &&  tagCancel.getWriteValBool() ) {
            setStatus(COMP_IDLE);
            tagRun.setReadValBool(false);
        }

        if( tagRun.hasWriteValue()  &&  tagRun.getWriteValBool()  &&  isStatus(COMP_IDLE) ) {
            boolean hasRunning = line.comps.stream()
                    .filter(comp -> comp.tagDoserId.getInt() == tagDoserId.getInt())
                    .anyMatch(comp -> comp.tagRun.getBool());

            if( isComponentInUse()  &&  line.isStatus(LINE_LOADING)  &&  !hasRunning ) {
                tagRun.setReadValBool(true);
                setStatus(COMP_PREPARING);
            }
        }

        if( isStatus(COMP_PREPARING) ) {
            if( doCompPreparing() )
                setStatus(COMP_LOADING);
        }

        if( isStatus(COMP_LOADING) ) {
            if( doCompLoading() )
                setStatus(COMP_FINISHED);
        }

        if( isStatus(COMP_FINISHED) ) {
            // just waiting for status change by the Line
        }

    }


    private boolean isStatus(int status) {
        return tagStatus.getInt() == status;
    }


    private void setStatus(int status) {
        tagStatus.setReadValInt(status);
    }


    public void updateDoserTags() {
        if( tagDoserId.getInt() == 0 ) {
            tagDoserName.setReadValString( "" );
            tagDoserDescr.setReadValString( "" );
            tagManual.setReadValBool( false );
            tagWeightCur.setReadValInt(0);
        } else {
            Doser doser = line.getDoser(tagDoserId);
            if (doser == null) {
                tagDoserName.setReadValString(NOT_FOUND);
                tagDoserDescr.setReadValString(NOT_FOUND);
                tagManual.setReadValBool(false);
                tagWeightCur.setReadValInt(0);
            } else {
                tagDoserName.setReadValString(doser.getName());
                tagDoserDescr.setReadValString(doser.getDescr());
                tagManual.setReadValBool(doser.isManual());
                tagWeightCur.setReadValInt(doser.getWeightCur());
            }
        }
    }


    public void resetTask() {
        tagDoserId.setReadValInt(0);
        tagDoserName.setReadValString("");
        tagDoserDescr.setReadValString("");
        tagProdId.setReadValInt(0);
        tagProdName.setReadValString("");
        tagWeightReq.setReadValInt(0);
        resetCycle();
    }

    public void resetCycle() {
        tagStatus.setReadValInt(0);
        tagRun.setReadValInt(0);
        tagWeightFin.setReadValInt(0);
    }


    public int getWeightReq() {
        return tagWeightReq.getInt();
    }


    public int getWeightFin() {
        return tagWeightFin.getInt();
    }

    public boolean isFinished() {
        return tagStatus.getInt() == COMP_FINISHED;
    }


    public boolean isComponentInUse() {
        return tagWeightReq.getInt() > 0;
    }


    private boolean withDoserInUse(Function<Doser,Boolean> action) {
        if( !isComponentInUse() )
            return true;

        Doser doser = line.getDoser(tagDoserId);
        if( doser == null  ||  !doser.isConnected() )
            return false;

        return action.apply(doser);
    }


    private boolean doCompPreparing() {
        return withDoserInUse(doser -> {
            doser.setWeightReq( tagWeightReq.getInt() );
            doser.setEmptyCur(true);
            doser.setRun(false);

            if( doser.isEmptyCur()  &&  doser.isStatusStop() ) {
                doser.setRun(true);
                return true;
            }
            return false;
        });
    }


    private boolean doCompLoading() {
        return withDoserInUse(doser -> {
            doser.setWeightReq(tagWeightReq.getInt());
            doser.setRun(true);

            if (doser.isFinished()) {
//                if (doser.isFinished() && doser.isStable()) {
                tagRun.setReadValBool(false);
                doser.setRun(false);
                tagWeightFin.setReadValInt(doser.getWeightCur());
                return true;
            }
            return false;
        });
    }


    public void setProductId(int value) {
        tagProdId.setReadValInt(value);
    }


    public void setProductName(String value) {
        tagProdName.setReadValString(value);
    }


    public void setDoserId(int value) {
        tagDoserId.setReadValInt(value);
    }


    public void setWeightReq(int value) {
        tagWeightReq.setReadValInt(value);
    }


    public int getProductId() {
        return tagProdId.getInt();
    }
}
