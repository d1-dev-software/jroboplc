package promauto.jroboplc.plugin.kkmansvr;

import promauto.jroboplc.core.api.Database;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Locale;
import java.util.Set;
import java.util.stream.Collectors;

public class DataServiceImpl implements DataService {
    private static final String TABLE_LINE = "KM_LINE";
    private static final String TABLE_DOSER = "KM_DOSER";
    private static final String TABLE_TASK = "KM_TASK";
    private static final String TABLE_TASK_PRODUCT = "KM_TASK_PRODUCT";
    private static final String TABLE_RECIPE = "KM_RECIPE";
    private static final String TABLE_PRODUCT = "KM_PRODUCT";
    private static final String TABLE_CYCLE = "KM_CYCLE";
    private static final String TABLE_CYCLE_PRODUCT = "KM_CYCLE_PRODUCT";

    Database db;
    private Statement st;

    @Override
    public void setDb(Database db) {
        this.db = db;
    }

    @Override
    public Statement createStatement() throws SQLException {
        if( db == null )
            throw new RuntimeException("Database is not set!");

        st = db.getConnection().createStatement();
        return st;
    }

    @Override
    public void commit() throws SQLException {
        if( st != null )
            st.close();
        st = null;
        db.commit();
    }

    @Override
    public void rollback()  {
        try {
            if (st != null)
                st.close();
        } catch (SQLException e) {
        }
        st = null;
        db.rollback();
    }


    @Override
    public void syncLine(int lineId, String lineName) throws SQLException {
        String sql = String.format(
                "update or insert into %s (id, name, deleted) values (%d, '%s', 0) matching (id)",
                TABLE_LINE,
                lineId,
                lineName
        );

        st.executeUpdate(sql);
    }


    @Override
    public int syncDoser(int lineId, String name, String descr, boolean canControl) throws SQLException {
        String sql = String.format(
                "update or insert into %s (line_id, name, descr, can_control, deleted) " +
                        "values (%d, '%s', '%s', %d, 0) matching (name) returning id",
                TABLE_DOSER,
                lineId,
                name,
                descr,
                canControl? 1: 0
        );

        try(ResultSet rs = st.executeQuery(sql)) {
            if (rs.next())
                return rs.getInt(1);
        }
        return 0;
    }


    @Override
    public void deleteOtherDosers(int lineId, Set<Integer> doserIds) throws SQLException {
        if( doserIds.isEmpty() )
            return;

        String sql = String.format(
                "update %s set deleted=1 where (line_id=%d) and not (id in (%s))",
                TABLE_DOSER,
                lineId,
                doserIds.stream()
                        .map(Object::toString)
                        .collect(Collectors.joining(","))
        );

        st.executeUpdate(sql);
    }


    @Override
    public Task findTask(int taskId) throws SQLException {
        Task task = null;
        String sql = String.format(
                "select  t.line_id, t.recipe_id, r.name, t.cycle_req, max(c.cycle_cnt), t.control_doser_id from %s t " +
                        "join %s r on r.id=t.recipe_id " +
                        "left join %s c on c.task_id=t.id " +
                        "where t.id=%d and t.deleted=0 " +
                        "group by 1,2,3,4,6",
                TABLE_TASK,
                TABLE_RECIPE,
                TABLE_CYCLE,
                taskId
        );

/*
 */

        try(ResultSet rs = st.executeQuery(sql)) {
            if (rs.next()) {
                task = new Task();
                task.taskId = taskId;
                task.lineId = rs.getInt(1);
                task.recipeId = rs.getInt(2);
                task.recipeName = rs.getString(3);
                task.cycleReq = rs.getInt(4);
                task.cycleCnt = rs.getInt(5);
                task.controlId = rs.getInt(6);
            }
        }

        if( task != null ) {
            sql = String.format(
                    "select tp.product_id, p.name, tp.doser_id, cast(weight_req*1000 as bigint) weight_req from %s tp " +
                            "join %s p on p.id=tp.product_id " +
                            "where tp.task_id=%d order by tp.ord",
                    TABLE_TASK_PRODUCT,
                    TABLE_PRODUCT,
                    taskId
            );

            try(ResultSet rs = st.executeQuery(sql)) {
                while (rs.next()) {
                    Task.Product tp = new Task.Product();
                    tp.productId = rs.getInt(1);
                    tp.productName = rs.getString(2);
                    tp.doserId = rs.getInt(3);
                    tp.weightReq = rs.getInt(4);
                    task.products.add(tp);
                }
            }
        }

        return task;
    }


    @Override
    public void saveCycle(Cycle cycle) throws SQLException {
        int cycleId = 0;
        String sql = String.format(Locale.ROOT,
                "insert into %s (task_id, dt, cycle_cnt, weight_req, weight_fin, weight_ctl) " +
                        "values (%d, '%s', %d, %f, %f, %f) returning id",
                TABLE_CYCLE,
                cycle.taskId,
                cycle.dt.format(db.getTimestampFormatter()),
                cycle.cycleCnt,
                (double)cycle.weightReq / 1000,
                (double)cycle.weightFin / 1000,
                (double)cycle.weightCtl / 1000
        );

        try(ResultSet rs = st.executeQuery(sql)) {
            if (rs.next())
                cycleId = rs.getInt(1);
        }

        if( cycleId > 0 ) {
            for(Cycle.Product cp: cycle.products) {
                sql = String.format(Locale.ROOT,
                        "insert into %s (cycle_id, product_id, weight_req, weight_fin) " +
                                "values (%d, %d, %f, %f)",
                        TABLE_CYCLE_PRODUCT,
                        cycleId,
                        cp.productId,
                        (double)cp.weightReq / 1000,
                        (double)cp.weightFin / 1000
                );

                st.executeUpdate(sql);
            }
        }
    }

}
