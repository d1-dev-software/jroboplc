package promauto.jroboplc.plugin.wessvr;

import java.sql.SQLException;

public class DeviceFactory {

	public static DeviceBase create(WessvrModule module, String modtype) throws SQLException {
		switch( modtype.toLowerCase() ) {
			case "standard":
				return new DeviceStandard(module);
			case "gelios":
				return new DeviceGelios(module);
			case "gelios.maslo2v2":
				return new DeviceGeliosMaslo2v2(module);
			case "akkont 908":
				return new DeviceAkkont(module);
			case "atara":
				return new DeviceAtara(module);
			case "mercury":
				return new DeviceMercuryECnt(module);
			case "tenzom_tb009":
				return new DeviceTenzomTb09(module);
			case "centaurz":
				return new DeviceCentaURZ(module);
		}
		return null;
	}

}
