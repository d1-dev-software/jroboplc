package promauto.jroboplc.plugin.wessvr;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import promauto.jroboplc.core.AbstractCommand;
import promauto.jroboplc.core.api.*;

import java.sql.ResultSet;
import java.sql.Statement;

public class CmdWmaxlist extends AbstractCommand {
	private final Logger logger = LoggerFactory.getLogger(CmdWmaxlist.class);


	@Override
	public String getName() {
		return "wmaxlist";
	}

	@Override
	public String getUsage() {
		return "";
	}

	@Override
	public String getDescription() {
		return "lists wesincmax setting of modules";
	}

	
	
	
	@Override
	public String execute(Console console, Module module, String args) {
		module.postCommand(this, console, module, args);
		return "";
	}


	@Override
	public void executePosted(Console console, Module module, String args) {

        WessvrModule w = (WessvrModule) module;

        Database db = w.getDatabase();
        if (!db.isConnected()) {
            console.print("Database " + db.getName() + " is not connected\n");
            return;
        }


        boolean firstpass = true;
        try( Statement st = db.getConnection().createStatement()) {
            for(Device device: w.getDevices()) {
                if( !(device instanceof  DeviceBase) )
                    continue;

                DeviceBase d = (DeviceBase)device;
                int wesincmax = 0;
                long lastwes = 0;
                long deltawes = 0;

                String sql = "select wesincmax from modules where idm=" + d.idm;
                try(ResultSet rs = st.executeQuery(sql)) {
                    if( rs.next() )
                        wesincmax = rs.getInt(1);
                }

                sql = "select avg(lastwes) from (select first 10 idm, lastwes from arcval where idm=" + d.idm + " order by dt desc)";
                try(ResultSet rs = st.executeQuery(sql)) {
                    if( rs.next() )
                        lastwes = rs.getLong(1);
                }

                sql = "select first  10 sumwes from arcval where idm=" + d.idm + " order by dt desc";
                long sumwes = 0;
                try(ResultSet rs = st.executeQuery(sql)) {
                    while( rs.next() ) {
                        long l = sumwes - rs.getLong(1);
                        if( l > 0  &&  ( deltawes == 0  ||  deltawes > sumwes ) )
                            deltawes = l;
                        sumwes = rs.getLong(1);
                    }
                }

                if( firstpass ) {
                    console.print(ANSI.BOLD + "\n IDM DESCR                 WESINCMAX    LASTWES   DELTAWES\n" + ANSI.RESET);
                    firstpass = false;
                }
                String s = String.format("%1$4d:%2$-20s %3$10d %4$10d %5$10d\n", d.idm, d.name, wesincmax, lastwes, deltawes);
                console.print(s);
/*
wessvr:wmaxlist
*/

            }
            db.commit();
        } catch (Exception e) {
            EnvironmentInst.get().printError(logger, e);
            try {
                db.rollback();
            } catch (Exception e1) {
            }
        }

        console.print("OK\n");
    }



}
