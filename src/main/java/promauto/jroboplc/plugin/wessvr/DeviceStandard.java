package promauto.jroboplc.plugin.wessvr;

import promauto.jroboplc.core.tags.RefGroup;
import promauto.jroboplc.core.tags.RefItem;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class DeviceStandard extends DeviceBase {

	public static final String MODTYPE = "Standard";

	protected RefItem refCrc;
	protected RefItem refSumWeight;
	protected RefItem refSumNum;
	protected RefItem refCurWeight;
	protected RefItem refLastWeight;
	protected RefItem refLastTime;
	protected RefItem refStatus1;
	protected RefItem refStatus2;
	protected RefItem refStatus3;
	protected RefItem refStatus4;
	protected RefItem refStatus5;
	protected RefItem refOutput;

	private   RefItem refMaxWeight;
	private   RefItem refMaxNum;

	public DeviceStandard(WessvrModule module) {
		super(module);
		sizeSumWeight = 0x8000_0000L;
		sizeSumNum = 0x8000_0000L;
	}


	@Override
	public String getModtype() {
		return MODTYPE;
	}


//	@Override
//	public boolean load(ResultSet rs) throws SQLException {
//		boolean res = super.load(rs);
//		outCalcMode = 1;
//		return res;
//	}

	@Override
	protected void createRefs() {
		super.createRefs();

		refCrc = refs.createItemCrcSum(name, "Crc32");
		refCrc.setOptional(true);

		refSumWeight  = addVldZero( refs.createItemCrc(name, "SumWeight") );
		refSumNum     = addVldZero( refs.createItemCrc(name, "SumNum") );
		refCurWeight  = addVldZero( refs.createItemCrc(name, "CurWeight") );
		refLastWeight = addVldZero( refs.createItemCrc(name, "LastWeight") );
		refLastTime   = addVldZero( refs.createItemCrc(name, "LastTime") );
		refStatus1    = addVldZero( refs.createItemCrc(name, "Status1") );
		refStatus2    = addVldZero( refs.createItemCrc(name, "Status2") );
		refStatus3    = addVldZero( refs.createItemCrc(name, "Status3") );
		refStatus4    = addVldZero( refs.createItemCrc(name, "Status4") );
		refStatus5    = addVldZero( refs.createItemCrc(name, "Status5") );
		refOutput     =             refs.createItemCrc(name, "Output");

		refs.addItemCrc(refErrorFlag);

		refMaxWeight 	= refs.createItem(name, "MaxWeight");
		refMaxNum 	 	= refs.createItem(name, "MaxNum");
	}


	@Override
	protected void doAfterRefsRead() {
		sizeSumWeight = refMaxWeight.getValue().getLong();
		sizeSumNum = refMaxNum.getValue().getLong();
	}


	@Override
	protected boolean checkCrc() {
		return !refCrc.isLinked() || refs.checkCrc32();
	}


	@Override
	protected long getSumWeight() {
		return refSumWeight.getValue().getLong();
	}


	@Override
	protected long getSumNum() {
		return refSumNum.getValue().getLong();
	}


	@Override
	protected boolean updateValuesStatus() {
		if( !hasStatusValuesChanged(refStatus1, refStatus2, refStatus3, refStatus4, refStatus5) )
			return false;

		statusValues[0] = refStatus1.getValue().getInt();
		statusValues[1] = refStatus2.getValue().getInt();
		statusValues[2] = refStatus3.getValue().getInt();
		statusValues[3] = refStatus4.getValue().getInt();
		statusValues[4] = refStatus5.getValue().getInt();

		return true;
	}



	@Override
	protected boolean updateValuesArcval() {
		if( arcvalValues[0] == getSumWeight()  &&  arcvalValues[1] == getSumNum() )
			return false;

		arcvalValues[0] = getSumWeight();
		arcvalValues[1] = getSumNum();
		arcvalValues[2] = refLastWeight.getValue().getInt();
		arcvalValues[3] = refLastTime.getValue().getInt();
		arcvalValues[4] = 0;

		return true;
	}


	@Override
	protected double getArcoutValue() {
		return refOutput.getValue().getDouble();
	}


}
