package promauto.jroboplc.plugin.wessvr;

import promauto.jroboplc.core.tags.RefGroup;
import promauto.jroboplc.core.tags.RefItem;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DeviceGeliosMaslo2v2 extends DeviceBase {

	public static final String MODTYPE = "GeliosMaslo2v2";


	protected RefItem refCrc;
	protected RefItem refWeightHigh1;
	protected RefItem refWeightLow1;
	protected RefItem refNumHigh1;
	protected RefItem refNumLow1;

	protected RefItem refWeightHigh2;
	protected RefItem refWeightLow2;
	protected RefItem refNumHigh2;
	protected RefItem refNumLow2;

	protected RefItem refWeightHigh;
	protected RefItem refWeightLow;
	protected RefItem refNumHigh;
	protected RefItem refNumLow;

	protected RefItem refState;
	protected RefItem refErrorCode;
	protected RefItem refStopCode;
	private int counter;
	private String name_without_counter;


	public DeviceGeliosMaslo2v2(WessvrModule module) {
		super(module);
		sizeSumWeight = 0x1_0000_0000L;
		sizeSumNum = 0x1_0000_0000L;
	}


	@Override
	public String getModtype() {
		return MODTYPE;
	}


	@Override
	public boolean load(ResultSet rs) throws SQLException {
		name_without_counter = name;
		boolean res = super.load(rs);
		counter=1;
		Matcher m = Pattern.compile("(.*)\\.(\\d+)$").matcher(name);
		if( m.matches() ) {
			name_without_counter = m.group(1);
			counter = Integer.parseInt( m.group(2) );
		}
		return res;
	}


	@Override
	protected void createRefs() {
		String name_original = name;
		name = name_without_counter;
		super.createRefs();

		refCrc = refs.createItemCrcSum(name, "Crc16");

		addVldZero( addVldFFFF( refWeightHigh1    = refs.createItemCrc(name, "WeightHigh1")));
		addVldZero( addVldFFFF( refWeightLow1     = refs.createItemCrc(name, "WeightLow1")));
		addVldZero( addVldFFFF( refNumHigh1       = refs.createItemCrc(name, "NumHigh1")));
		addVldZero( addVldFFFF( refNumLow1 	      = refs.createItemCrc(name, "NumLow1")));

		addVldZero( addVldFFFF( refWeightHigh2    = refs.createItemCrc(name, "WeightHigh2")));
		addVldZero( addVldFFFF( refWeightLow2     = refs.createItemCrc(name, "WeightLow2")));
		addVldZero( addVldFFFF( refNumHigh2       = refs.createItemCrc(name, "NumHigh2")));
		addVldZero( addVldFFFF( refNumLow2 	      = refs.createItemCrc(name, "NumLow2")));

		if( counter == 2 ) {
			refWeightHigh = refWeightHigh2;
			refWeightLow  = refWeightLow2;
			refNumHigh    = refNumHigh2;
			refNumLow     = refNumLow2;
		} else {
			refWeightHigh = refWeightHigh1;
			refWeightLow  = refWeightLow1;
			refNumHigh    = refNumHigh1;
			refNumLow     = refNumLow1;
		}

		refs.addItemCrc(refErrorFlag);

		refState	 		= refs.createItem(name, "State");
		refErrorCode 		= refs.createItem(name, "ErrorCode");
		refStopCode	 		= refs.createItem(name, "StopCode");

		name = name_original;
	}



	@Override
	protected long getSumWeight() {
		return (((long)(refWeightHigh.getValue().getInt())) << 16) + refWeightLow.getValue().getInt();
	}


	@Override
	protected long getSumNum() {
		return (((long)(refNumHigh.getValue().getInt())) << 16) + refNumLow.getValue().getInt();
	}


	@Override
	protected boolean updateValuesStatus() {
		if( !hasStatusValuesChanged(refState, refErrorCode, refStopCode) )
			return false;

		statusValues[0] = refState.getValue().getInt();
		statusValues[1] = refErrorCode.getValue().getInt();
		statusValues[2] = refStopCode.getValue().getInt();

		return true;
	}



	@Override
	protected boolean updateValuesArcval() {
		if( arcvalValues[0] == getSumWeight()  &&  arcvalValues[1] == getSumNum() )
			return false;

		arcvalValues[0] = getSumWeight();
		arcvalValues[1] = getSumNum();
//		arcvalValues[2] = (((long)(refLastWeightHigh.getValue().getInt())) << 16) + refLastWeightLow.getValue().getInt();
//		arcvalValues[3] = (((long)(refLastTimeHigh.getValue().getInt())) << 16) + refLastTimeLow.getValue().getInt();
		arcvalValues[4] = refErrorCode.getValue().getInt();

		return true;
	}




}
