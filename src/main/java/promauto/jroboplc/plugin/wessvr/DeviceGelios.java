package promauto.jroboplc.plugin.wessvr;

import promauto.jroboplc.core.tags.RefGroup;
import promauto.jroboplc.core.tags.RefItem;

import java.sql.SQLException;

public class DeviceGelios extends DeviceBase {

	public static final String MODTYPE = "Gelios";

	protected RefItem refCrc;
	protected RefItem refSumWeightHigh1;
	protected RefItem refSumWeightLow1;
	protected RefItem refSumNumHigh1;
	protected RefItem refSumNumLow1;
	protected RefItem refCurWeightHigh;
	protected RefItem refCurWeightLow;
	protected RefItem refLastWeightHigh;
	protected RefItem refLastWeightLow;
	protected RefItem refLastTimeHigh;
	protected RefItem refLastTimeLow;
	protected RefItem refState;
	protected RefItem refErrorCode;
	protected RefItem refOutput;


	public DeviceGelios(WessvrModule module) {
		super(module);
		sizeSumWeight = 0x1_0000_0000L;
		sizeSumNum = 0x1_0000_0000L;
	}


	@Override
	public String getModtype() {
		return MODTYPE;
	}




	@Override
	protected void createRefs() {
		super.createRefs();
		refCrc			 	= refs.createItemCrcSum(name, "Crc16");

		addVldZero( addVldFFFF( refSumWeightHigh1 = refs.createItemCrc(name, "SumWeightHigh1")));
		addVldZero( addVldFFFF( refSumWeightLow1  = refs.createItemCrc(name, "SumWeightLow1")));
		addVldZero( addVldFFFF( refSumNumHigh1 	  = refs.createItemCrc(name, "SumNumHigh1")));
		addVldZero( addVldFFFF( refSumNumLow1 	  = refs.createItemCrc(name, "SumNumLow1")));
		addVldZero( 							    refs.createItemCrc(name, "SumWeightHigh2"));
		addVldZero( 							    refs.createItemCrc(name, "SumWeightLow2"));
		addVldZero( 							    refs.createItemCrc(name, "SumNumHigh2"));
		addVldZero( 							    refs.createItemCrc(name, "SumNumLow2"));
		addVldZero( 			refCurWeightHigh  = refs.createItemCrc(name, "CurWeightHigh"));
		addVldZero( 			refCurWeightLow   = refs.createItemCrc(name, "CurWeightLow"));
		addVldZero( 			refLastWeightHigh = refs.createItemCrc(name, "LastWeightHigh"));
		addVldZero( 			refLastWeightLow  = refs.createItemCrc(name, "LastWeightLow"));
		addVldZero( 			refLastTimeHigh   = refs.createItemCrc(name, "LastTimeHigh"));
		addVldZero( 			refLastTimeLow 	  = refs.createItemCrc(name, "LastTimeLow"));

		refs.addItemCrc(refErrorFlag);

		refState	 		= refs.createItem(name, "State");
		refErrorCode 		= refs.createItem(name, "ErrorCode");
		refOutput	 		= refs.createItem(name, "Output");
	}



	@Override
	protected long getSumWeight() {
		return (((long)(refSumWeightHigh1.getValue().getInt())) << 16) + refSumWeightLow1.getValue().getInt();
	}


	@Override
	protected long getSumNum() {
		return (((long)(refSumNumHigh1.getValue().getInt())) << 16) + refSumNumLow1.getValue().getInt();
	}


	@Override
	protected boolean updateValuesStatus() {
		if( !hasStatusValuesChanged(refState, refErrorCode) )
			return false;

		statusValues[0] = refState.getValue().getInt();
		statusValues[1] = refErrorCode.getValue().getInt();

		return true;
	}



	@Override
	protected boolean updateValuesArcval() {
		if( arcvalValues[0] == getSumWeight()  &&  arcvalValues[1] == getSumNum() )
			return false;

		arcvalValues[0] = getSumWeight();
		arcvalValues[1] = getSumNum();
		arcvalValues[2] = (((long)(refLastWeightHigh.getValue().getInt())) << 16) + refLastWeightLow.getValue().getInt();
		arcvalValues[3] = (((long)(refLastTimeHigh.getValue().getInt())) << 16) + refLastTimeLow.getValue().getInt();
		arcvalValues[4] = refErrorCode.getValue().getInt();

		return true;
	}


	@Override
	protected double getArcoutValue() {
		return refOutput.getValue().getDouble();
	}


}
