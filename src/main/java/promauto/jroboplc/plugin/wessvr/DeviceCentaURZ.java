package promauto.jroboplc.plugin.wessvr;

import promauto.jroboplc.core.tags.RefItem;

public class DeviceCentaURZ extends DeviceBase {

	public static final String MODTYPE = "CentaURZ";

	protected RefItem refCrc;
	protected RefItem refSumWeightHigh;
	protected RefItem refSumWeightLow;
	protected RefItem refOutput;


	public DeviceCentaURZ(WessvrModule module) {
		super(module);
		sizeSumWeight = 4294967296L;
		sizeSumNum = 4294967296L;
	}

	@Override
	public String getModtype() {
		return MODTYPE;
	}


	@Override
	protected void createRefs() {
		super.createRefs();
		refCrc			 	= refs.createItemCrcSum(name, "Crc");

		addVldZero( addVldFFFF( refSumWeightHigh 	= refs.createItemCrc(name, "SumWeightHigh")));
		addVldZero( addVldFFFF( refSumWeightLow 	= refs.createItemCrc(name, "SumWeightLow")));
		refOutput  = refs.createItem(name, "Output");
	}

	@Override
	protected boolean checkCrc() {
		return refs.checkCrc8();
	}


	@Override
	protected long getSumWeight() {
		return (((long)(refSumWeightHigh.getValue().getInt())) << 16) + refSumWeightLow.getValue().getInt();
	}


	@Override
	protected long getSumNum() {
		return 0;
	}


	@Override
	protected boolean updateValuesStatus() {
		return true;
	}


	@Override
	protected boolean updateValuesArcval() {
		if( arcvalValues[0] == getSumWeight() )
			return false;

		arcvalValues[0] = getSumWeight();
		return true;
	}


	@Override
	protected double getArcoutValue() {
		return refOutput.getValue().getDouble();
	}


}
