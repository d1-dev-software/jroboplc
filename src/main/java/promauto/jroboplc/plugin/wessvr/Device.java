package promauto.jroboplc.plugin.wessvr;

import java.sql.ResultSet;
import java.sql.SQLException;

public interface Device {

    String getModtype();

    boolean isEnable();

    String getInfo();

    boolean load(ResultSet rs) throws SQLException;

    boolean init() throws SQLException;

    void execute() throws SQLException;

}
