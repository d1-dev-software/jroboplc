package promauto.jroboplc.plugin.wessvr;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import promauto.jroboplc.core.api.Configuration;
import promauto.jroboplc.core.api.Environment;
import promauto.jroboplc.core.api.EnvironmentInst;
import promauto.utils.ParamsMap;

public class ArchiveConfig {
	private final Logger logger = LoggerFactory.getLogger(ArchiveConfig.class);
	
	private WessvrModule module;
	
	public static class Item {
		public String tblMain;
		public String tblStat;
		public int interval; // =0 - by shift, >0 - minutes
		
		public Item(String tblMain, String tblStat, int interval) {
			this.tblMain = tblMain;
			this.tblStat = tblStat;
			this.interval = interval;
		}
		
		@Override
		public String toString() {
			return "main=" + tblMain + " stat=" + tblStat + " interval=" + interval;
		}


	}
	
	public final List<Item> items = new ArrayList<>();
	
	
	
	public ArchiveConfig(WessvrModule module) {
		this.module = module;
	}
	

	
	public boolean load(Object conf) {
		items.clear();
		
		Environment env = EnvironmentInst.get();
		Configuration cm = env.getConfiguration();
		
		List<Object> recs = cm.toList( cm.get(conf, "arctables"));
		
		if( recs.size() == 0) {
			items.add( new Item("MAIN", "STAT", 0) );
			items.add( new Item("MAINHR", "STATHR", 60) );
			return true;
		}
		
		for(Object rec: recs) {
			Item item = new Item(
					cm.get(rec, "main", "").trim(), 
					cm.get(rec, "stat", "").trim(),
					cm.get(rec, "interval", 0));
			
			if( item.tblMain.isEmpty()  ||  item.tblStat.isEmpty() ) {
				env.printError(logger, module.getName(), "Empty table names", item.toString());
			}
			
			items.add(item);
		}
		
		return true;
	}

	
	
	public boolean createTables() {
		
		for(Item item: items) {
			ParamsMap params = new ParamsMap(
					"schema=" + module.getSchema(),
					"tblmain=" + item.tblMain, 
					"tblstat=" + item.tblStat);
			
			if( !module.getDatabase().executeScript("wessvr.arctables", params ).success ) {
				EnvironmentInst.get().printError(logger, module.getName(), "Failed to create table", item.toString());
				return false;
			}
		}
		
		return true;
	}

}
