package promauto.jroboplc.plugin.wessvr;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import promauto.jroboplc.core.api.EnvironmentInst;
import promauto.jroboplc.core.api.Tag;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Locale;

public class ArchiveBase {
    private final Logger logger = LoggerFactory.getLogger(ArchiveBase.class);

    public static final int CHECK_DELAY_SECS = 60;
    public static final int DELTAWES_BOOST_COEFF = 5;

    private final DeviceBase device;
    private final String tblMain;
    private final String tblStat;
    private final int interval;

    private LocalDateTime dtbeg;
    private LocalDateTime dtend;
    private int pertype;
    private int period;

    private long wes;
    private long swbeg;
    private long swend;

    private long wnum;
    private long wnbeg;
    private long wnend;

    private double outwes;
    private double outnum;

    private int idprod;
    private int idbrig;
    private int idbatch;
    private boolean initialized;
    private final String sqlForeignIds;

    private int timeStepBack;
    private int timeBigDeltaWes;
    private long lastDeltaWes;
    private final DateTimeFormatter formatter;

    private final Tag tagPertype;
    private final Tag tagPeriod;
    private final Tag tagWes;
    private final Tag tagWnum;
    private long currentStatWes;
    private long currentStatWnum;


    public ArchiveBase(DeviceBase device, ArchiveConfig.Item archiveConfigItem) {
        this.device = device;

        formatter = device.module.getDatabase().getTimestampFormatter();

        tblMain = device.module.makeTableName(archiveConfigItem.tblMain);
        tblStat = device.module.makeTableName(archiveConfigItem.tblStat);
        interval = archiveConfigItem.interval;

        String tblModules = device.module.makeTableName("modules");

        sqlForeignIds = String.format(
                "select idprod, idbrig, idbatch from %s where idm=%d",
                tblModules, device.idm);

        tagPertype = device.getOrCreateTag(Tag.Type.INT, archiveConfigItem.tblMain + ".pertype");
        tagPeriod = device.getOrCreateTag(Tag.Type.INT, archiveConfigItem.tblMain + ".period");
        tagWes = device.getOrCreateTag(Tag.Type.INT, archiveConfigItem.tblMain + ".wes");
        tagWnum = device.getOrCreateTag(Tag.Type.INT, archiveConfigItem.tblMain + ".wnum");

        initialized = false;
    }


    public void init() throws SQLException {
        dtend = LocalDateTime.now();

//        Statement st = device.module.getStatement();
        boolean res = false;

        String sql = String.format(
                "select dtbeg, period, pertype, wes, swbeg, swend, wnum, wnbeg, wnend, idprod, idbrig, idbatch "+
                        "from %s where idm=%d",
                tblMain,device.idm);

        try(ResultSet rs = device.module.executeQuery(sql)) {
            if (rs.next()) {
                dtbeg = rs.getTimestamp("dtbeg").toLocalDateTime();
                pertype = calcPertype(dtbeg);
                period = calcPeriod(dtbeg);
                wes = rs.getLong("wes");
                swbeg = rs.getLong("swbeg");
                swend = rs.getLong("swend");
                wnum = rs.getLong("wnum");
                wnbeg = rs.getLong("wnbeg");
                wnend = rs.getLong("wnend");
                idprod = rs.getInt("idprod");
                idbrig = rs.getInt("idbrig");
                idbatch = rs.getInt("idbatch");
                res = true;
            }
        }

        if(!res) {
            dtbeg = device.refs.getDtRead();
            pertype = calcPertype(dtbeg);
            period = calcPeriod(dtbeg);
            wes = 0;
            swbeg = device.getSumWeight();
            swend = swbeg;
            wnum = 0;
            wnbeg = device.getSumNum();
            wnend = wnbeg;

            try(ResultSet rs = device.module.executeQuery( sqlForeignIds )) {
                if (rs.next()) {
                    idprod = rs.getInt(1);
                    idbrig = rs.getInt(2);
                    idbatch = rs.getInt(3);
                } else {
                    idprod = 0;
                    idbrig = 0;
                    idbatch = 0;
                }
            }

            writeMain();
        }

        fetchCurrentStats();

        timeStepBack = 0;
        timeBigDeltaWes = 0;
        lastDeltaWes = 0;

        initialized = true;
    }


    private void fetchCurrentStats() throws SQLException {
        String sql = String.format(
                "select sum(wes), sum(wnum) from %s where idm=%d and period=%d",
                tblStat, device.idm, period);

        try(ResultSet rs = device.module.executeQuery(sql)) {
            if (rs.next()) {
                currentStatWes = rs.getLong(1);
                currentStatWnum = rs.getLong(2);
            } else
                resetCurrentStats();
        }
    }


    private void resetCurrentStats() {
        currentStatWes = 0;
        currentStatWnum = 0;
    }


    private void addCurrentStats() {
        currentStatWes += wes;
        currentStatWnum += wnum;
    }


    public void execute() throws SQLException {
        if( !initialized )
            init();

        dtend = device.refs.getDtRead();

        long swend_new = device.getSumWeight();
        long wnend_new = device.getSumNum();
        int pertype_new = calcPertype( dtend );
        int period_new = calcPeriod( dtend );

        int idprod_new = 0;
        int idbrig_new = 0;
        int idbatch_new = 0;

        // get new foreign ids
        try(ResultSet rs = device.module.executeQuery( sqlForeignIds )) {
            if (rs.next()) {
                idprod_new = rs.getInt(1);
                idbrig_new = rs.getInt(2);
                idbatch_new = rs.getInt(3);
            }
        }

        if( device.isReplaced() ) {
            if (swend != swend_new  ||  wnend != wnend_new) {
                writeStat(2);

                dtbeg = dtend;
                swend = swend_new;
                wnend = wnend_new;
                swbeg = swend;
                wnbeg = wnend;

                idprod = idprod_new;
                idbrig = idbrig_new;
                idbatch = idbatch_new;
                pertype = pertype_new;
                period = period_new;

                wes = wnum = 0;
                writeMain();
            }
        }

        if( checkStepBack(swend_new, wnend_new)  &&  checkBigDeltaWes(swend_new) ) {

            // case #1: has decrement in wes or wnum
            if (swend > swend_new || wnend > wnend_new) {
                writeStat(0);

                dtbeg = dtend;
                swbeg = swend;
                swend = swend_new;
                wes = swbeg <= swend ? swend - swbeg : device.sizeSumWeight - swbeg + swend;
                wnbeg = wnend;
                wnend = wnend_new;
                wnum = wnbeg <= wnend ? wnend - wnbeg : device.sizeSumNum - wnbeg + wnend;

                if( period == period_new )
                    addCurrentStats();

                idprod = idprod_new;
                idbrig = idbrig_new;
                idbatch = idbatch_new;
                pertype = pertype_new;
                period = period_new;

                if( device.sizeSumWeight == 0  &&  device.sizeSumNum == 0) {
                    swbeg = 0;
                    wnbeg = 0;
                } else {
                    writeStat(1);
                    swbeg = swend;
                    wnbeg = wnend;
                }

                wes = wnum = 0;
                writeMain();
            }


            // case #2:  has changes in period or foreign ids
            if( (period != period_new  &&  interval >= 0)
                    || idprod != idprod_new
                    || idbrig != idbrig_new
                    || idbatch != idbatch_new) {

                swend = swend_new;
                wes = swend - swbeg;
                wnend = wnend_new;
                wnum = wnend - wnbeg;

                boolean samePeriod = period == period_new;

                boolean ignoreStat = device.module.isIgnoreZeroStat()  &&  samePeriod  &&  (wes == 0)  &&  (wnum == 0);
                if( !ignoreStat )
                    writeStat(0);

                if( samePeriod )
                    addCurrentStats();
                else
                    resetCurrentStats();


                dtbeg = dtend;
                idprod = idprod_new;
                idbrig = idbrig_new;
                idbatch = idbatch_new;
                pertype = pertype_new;
                period = period_new;
                swbeg = swend;
                wnbeg = wnend;
                wes = wnum = 0;
                writeMain();
            }


            // case #3:  has increment in wes or wnum
            if (swend < swend_new || wnend < wnend_new) {
                swend = swend_new;
                wes = swend - swbeg;
                wnend = wnend_new;
                wnum = wnend - wnbeg;
                writeMain();
            }
        }

        tagPertype.setInt(pertype);
        tagPeriod.setInt(period);
        tagWes.setInt((int)(wes + currentStatWes));
        tagWnum.setInt((int)(wnum + currentStatWnum));
    }



    private boolean checkStepBack(long swend_new, long wnend_new) {
        if( swend <= swend_new  &&  wnend <= wnend_new ) {
            timeStepBack = 0;
            return true;
        }

        if( timeStepBack == 0 ) {
            timeStepBack = device.refUpdateTime.getValue().getInt();

            EnvironmentInst.get().logInfo(logger, device.module.getName() + " " + device.name + " " + tblMain + " StepBack started," +
                    " swend: " + swend + "->" + swend_new + " wnend: " + wnend + "->" + wnend_new + " time=" + timeStepBack);

        } else {
            if (substructTimes(device.refUpdateTime.getValue().getInt(), timeStepBack) > CHECK_DELAY_SECS) {

                EnvironmentInst.get().logInfo(logger, device.module.getName() + " " + device.name + " " + tblMain + " StepBack PASSED," +
                        " swend: " + swend + "->" + swend_new + " wnend:" + wnend + "->" + wnend_new + " time=" + device.refUpdateTime.getValue().getInt() );

                timeStepBack = 0;
                return true;
            }
        }

        return false;
    }


    private boolean checkBigDeltaWes(long swend_new) {
        if( swend >= swend_new ) {
            timeBigDeltaWes = 0;
            return true;
        }

        long deltaWes = swend_new - swend;
        if( device.wesIncMax != 0?
                deltaWes < device.wesIncMax :
                lastDeltaWes == 0  ||  deltaWes < lastDeltaWes * DELTAWES_BOOST_COEFF)
        {
            lastDeltaWes = deltaWes;
            timeBigDeltaWes = 0;
            return true;
        }

        if( timeBigDeltaWes == 0 ) {
            timeBigDeltaWes = device.refUpdateTime.getValue().getInt();

            EnvironmentInst.get().logInfo(logger, device.module.getName() + " " + device.name + " " + tblMain + " BigDeltaWes started," +
                            " swend: " + swend + "->" + swend_new + " time=" + timeBigDeltaWes +
                            " deltawes="+deltaWes + " wmax=" + (device.wesIncMax != 0? device.wesIncMax: lastDeltaWes * DELTAWES_BOOST_COEFF) );

        } else {
            if (substructTimes(device.refUpdateTime.getValue().getInt(), timeBigDeltaWes) > CHECK_DELAY_SECS) {
                lastDeltaWes = deltaWes;
                timeBigDeltaWes = 0;

                EnvironmentInst.get().logInfo(logger, device.module.getName() + " " + device.name + " " + tblMain + " BigDeltaWes PASSED," +
                        " swend: " + swend + "->" + swend_new + " time=" + device.refUpdateTime.getValue().getInt() +
                        " deltawes="+deltaWes );

                return true;
            }
        }

        return false;
    }


    private int substructTimes(int time1, int time2) {
        time1 = converTimeToSeconds(time1);
        time2 = converTimeToSeconds(time2);
        if( time1 < time2 )
            time1 += 86400;
        return time1 - time2;
    }

    private int converTimeToSeconds(int time) {
        return (time / 10000)*3600 + ((time % 10000) / 100)*60 + time % 100;
    }


    private void writeMain() throws SQLException {
        calcOutputs();

        String sql = String.format(Locale.ROOT,
                "update or insert into %s (idm, " +
                "pertype, period, dtbeg, wes, swbeg, " +
                "swend, wnum, wnbeg, wnend, outwes, " +
                "outnum, idprod, idbrig, idbatch) values (%d, " +
                "%d, %d, '%s', %d, %d, " +
                "%d, %d, %d, %d, %f, " +
                "%f, %d, %d, %d) matching (idm)",
                tblMain, device.idm,
                pertype, period, dtbeg.format(formatter), wes, swbeg,
                swend, wnum, wnbeg, wnend, outwes,
                outnum, idprod, idbrig, idbatch);

        device.module.executeUpdate( sql );
    }



    private void writeStat(int flagSetback) throws SQLException {
        calcOutputs();
        String sql = String.format(Locale.ROOT,
                "insert into %s (idm, " +
                "pertype, period, dtbeg, dtend, setback, " +
                "wes, swbeg, swend, wnum, wnbeg, " +
                "wnend, outwes, outnum, idprod, idbrig, " +
                "idbatch) values (%d, " +
                "%d, %d, '%s', '%s', %d, " +
                "%d, %d, %d, %d, %d, " +
                "%d, %f, %f, %d, %d, " +
                "%d)",
                tblStat, device.idm,
                pertype, period, dtbeg.format(formatter), dtend.format(formatter), flagSetback,
                wes, swbeg, swend, wnum, wnbeg,
                wnend, outwes, outnum, idprod, idbrig,
                idbatch);

        device.module.executeUpdate( sql );
    }



    private void calcOutputs() {
        double hours = ((double)ChronoUnit.SECONDS.between(dtbeg, dtend))/3600;
        if( hours > 0 ) {
            outwes = wes / hours;
            outnum = wnum / hours;
        } else {
            outwes = 0;
            outnum = 0;
        }
    }



    private int calcPertype(LocalDateTime ldt) {
        int pertype;

        if( interval <= 0 )
            pertype = 10 + device.shiftset.getShiftNum(ldt);

        else if( interval < 60 )
            pertype = 10000 + ldt.getHour() * 100 + ldt.getMinute() - ldt.getMinute() % interval;

        else
            pertype = 100 + ldt.getHour();

        return pertype;
    }


    protected int calcPeriod(LocalDateTime ldt) {
        if( interval == 0  &&  device.shiftset.isAfterMidnight(ldt) )
            ldt = ldt.minusDays(1);

        int period = ldt.getYear() * 10000 + ldt.getMonth().getValue() * 100 + ldt.getDayOfMonth();

        if( interval <= 0 ) {
            period = period * 10 + device.shiftset.getShiftNum(ldt);
        }

        else if( interval == 60 ) {
            period = period * 100 + ldt.getHour();
        }

        else if( interval < 60 ) {
            period = (period % 1000000) * 10000 + ldt.getHour() * 100 + ldt.getMinute() - ldt.getMinute() % interval;
        }

        else if( interval < 1440 ) {
            period = period * 100 + ldt.getHour() - ldt.getHour() % (interval/60);
        }

        return period;
    }


    public void onConnectError() {
        timeStepBack = 0;
        timeBigDeltaWes = 0;
    }
}

