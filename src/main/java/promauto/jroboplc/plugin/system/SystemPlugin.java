package promauto.jroboplc.plugin.system;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLClassLoader;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import promauto.jroboplc.core.AbstractPlugin;
import promauto.jroboplc.core.api.Module;
import promauto.utils.CRC;

public class SystemPlugin extends AbstractPlugin {
	private static final String PLUGIN_NAME = "system";

	public enum OsType {
		Linux,
		Windows
	}
	private OsType osType;
	private String osName;

	
	@Override
	public void initialize() {
		super.initialize();
		initOsType();
    }
	
	@Override
    public String getPluginName() {
    	return PLUGIN_NAME;
    }

	@Override
    public String getPluginDescription(){
    	return "Platform specific features";
    }
	

	@Override
	public Module createModule(String name, Object conf) {
		SystemModule m = new SystemModule(this, name);
    	modules.add(m);
    	return m.load(conf)? m: null;
	}


////////////////////////////////////////////////////////////////////////////////////////////////////////

	private void initOsType() {
		osName = System.getProperty("os.name");
        if(osName.equals("Linux")){
        	osType = OsType.Linux;
            osName = "linux";
        }
        else if(osName.startsWith("Win")){
        	osType = OsType.Windows;
            osName = "windows";
        }
	}
	
	public OsType getOsType() {
		return osType;
	}
	
	public String getOsName() {
		return osName;
	}

}
