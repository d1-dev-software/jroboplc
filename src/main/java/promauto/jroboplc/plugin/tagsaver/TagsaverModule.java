package promauto.jroboplc.plugin.tagsaver;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.zip.CRC32;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import promauto.jroboplc.core.AbstractModule;
import promauto.jroboplc.core.api.*;
import promauto.jroboplc.core.tags.TagPlain;
import promauto.utils.Numbers;

public class TagsaverModule extends AbstractModule implements Signal.Listener {
	private final static Charset charset = Charset.forName("UTF-8");
	
	private static final int IDX_CRC = 0;
	private static final int IDX_MODNAME = 2;
	private static final int IDX_TAGNAME = 3;
	private static final int IDX_VALUE = 4;
	private static final String CR_LF = "\r\n";
	private static final String CRC_OFF = "crc:off";
	private static final int CRC_LENGHT = 8;
	private static final String TAGSAVER_FORMAT_1 = "tagsaver.format:1";

	private final Logger logger = LoggerFactory.getLogger(TagsaverModule.class);
	static private DateTimeFormatter dtFormatter = DateTimeFormatter.ofPattern("yyyyMMdd_HHmmss_");

	private volatile boolean hasSignalReload = false;

	protected static class TagRec implements  Comparable<TagRec> {
		Module module;
		Tag tag;
		Tag value;

		public TagRec(Module module, Tag tag) {
			this.module = module;
			this.tag = tag;
			this.value = TagPlain.create(tag);
		}


		@Override
		public int compareTo(TagRec o) {
			int res = module.getName().compareTo(o.module.getName());
			if (res != 0)
				return res;

			res = tag.getName().compareTo(o.tag.getName());
			if( res != 0)
				return res;

			return tag.getType().ordinal() - o.tag.getType().ordinal();
		}

		@Override
		public boolean equals(Object o) {
			if (o == this)
				return true;

			if (!(o instanceof TagRec)) {
				return false;
			}

			TagRec tagrec = (TagRec) o;

			return module.getName().equals(tagrec.module.getName())
					&& tag.getName().equals(tagrec.tag.getName())
					&& tag.getType() == tagrec.tag.getType();
		}

		@Override
		public int hashCode() {
			int result = 17;
			result = 31 * result + module.getName().hashCode();
			result = 31 * result + tag.getName().hashCode();
			result = 31 * result + tag.getType().ordinal();
			return result;
		}

	}

    protected Set<TagRec> tags;

	private Path pathSave;
	private Path pathChng;
	private Path pathSaveMir;
	private Path pathChngMir;
	private int historySize;
	private int chngLinesMax;
	private int chngLinesCnt;
	private boolean hasChanges;
	
	private FileChannel fchannelChng = null;
	private FileChannel fchannelChngMir = null;
	private boolean openedChng = false;
	private boolean modeReload;
	
	private StringBuilder sb = new StringBuilder();
	private ByteBuffer lastWriteBuffer;
	
	private CRC32 crcWrite = new CRC32();
	private SimpleDateFormat sdf = new SimpleDateFormat("yy-MM-dd.HH:mm:ss");


	
	public TagsaverModule(Plugin plugin, String name) {
		super(plugin, name);
		env.getCmdDispatcher().addCommand(this, CmdLoad.class); 
	}

	
	
	public boolean loadModule(Object conf) {

		Configuration cm = env.getConfiguration();
		
		enable = cm.get(conf, "enable", true);
		historySize = cm.get(conf, "history", 	10);
		chngLinesMax = cm.get(conf, "chng.max", 10000);

		pathSave = cm.getPath(conf, "file.save", "saves/" + name + ".save");
		
		Map<String,Object> map = cm.toMap(conf); 
		
		pathChng = map.containsKey("file.chng")?
			cm.getPath(conf, "file.chng", "") :
			Paths.get( pathSave.toString() + ".chng");
		
		pathSaveMir = map.containsKey("mirror.save")?
			cm.getPath(conf, "mirror.save", "") :
			Paths.get( pathSave + ".mirror");
		
		pathChngMir = map.containsKey("mirror.chng")?
			cm.getPath(conf, "mirror.chng", "") :
			Paths.get( pathSaveMir + ".chng");
		
		modeReload = false;
		
		return true;
	}
	
	
	
	@Override
	public boolean prepareModule() {

		addAsSignalListerToAllOthers();
//		env.getModuleManager().getModules().stream()
//				.filter( m -> m != this )
//				.forEach( m -> m.addSignalListener(this) );

		tags = collectTags();
		
		hasChanges = false;
		chngLinesCnt = 0;

		try {
			Files.createDirectories(pathSave.getParent());
			Files.createDirectories(pathChng.getParent());
			Files.createDirectories(pathSaveMir.getParent());
			Files.createDirectories(pathChngMir.getParent());
		} catch (IOException e) {
			env.printError(logger, e, name );
			return false;
		}

		if( !modeReload ) {
			if( !Files.exists(pathSave)  ||  !readValues(pathSave, pathChng) ) 
				if( Files.exists(pathSaveMir) ) {
					env.printInfo(logger, name, "Reading from mirror...");
					if( readValues(pathSaveMir, pathChngMir) ) {
						env.printInfo(logger, name, "SUCCESS");
						hasChanges = true;
					} else {
						hasChanges = false;
						return false;
					}
				}
		
			hasChanges |= !Files.exists(pathSave);
			hasChanges |= !Files.exists(pathSaveMir);
			hasChanges |= Files.exists(pathChng);
			hasChanges |= Files.exists(pathChngMir);
			
			merge();
		} 
		
		for (TagRec rec: tags)
			rec.tag.copyValueTo( rec.value );

		return true;
	}

	private boolean readValues(Path main, Path chng) {
		if( !readValues(main, true) ) 
			return false;
	
		if(Files.exists(chng)) 
			if( !readValues(chng, false) ) 
				return false;
		
		return true;
	};

	
	@Override
	public boolean executeModule() {

		if(hasSignalReload) {
			hasSignalReload = false;
			onSignalReload();
		}
		
		try {
			boolean needTotalCrc = false;
			for (TagRec rec : tags) {
				if( rec.tag.getStatus() == Tag.Status.Deleted ) {
					env.printInfo(logger, "Reinitialization due to a deleted tag being found: " +
							rec.module.getName() + ':' + rec.tag.getName());
					onSignalReload();
					return true;
				}

				if (!rec.tag.equalsValue(rec.value)) {
					hasChanges = true;
					rec.tag.copyValueTo(rec.value);
					openFileChg();
					if (!openedChng)
						break;

					writeTag(fchannelChng, rec, new Date());
					lastWriteBuffer.rewind();
					fchannelChngMir.write(lastWriteBuffer);

					++chngLinesCnt;
					needTotalCrc = true;
				}
			}

			if( needTotalCrc ) {
				try {
					writeTotalCrc(fchannelChng);
					fchannelChng.force(false);
					
					lastWriteBuffer.rewind();
					fchannelChngMir.write( lastWriteBuffer );
					fchannelChngMir.force(false);
				} catch (IOException e) {
					env.printError(logger, e, name);
				}
			}

			closeChng();
		} catch (Exception e) {
			env.printError(logger, e, name, "File:", pathChng.toString());
		}
		
		if( chngLinesCnt > chngLinesMax )
			merge();
		
		return true;
	}



	private Path getHistoryDir(Path file) {
		Path dir = file.getParent();
		if (dir == null)
			dir = Paths.get(".");
		dir = dir.resolve("history");

		try {
			Files.createDirectories(dir);
		} catch (IOException e) {
			env.printError(logger, e, name);
		}
		return dir;
	}

	private void deleteOldHistory(Path file) {
		Path dir = getHistoryDir(file);
		
		Map<String,Path> map = new TreeMap<>();
		try (DirectoryStream<Path> stream = Files.newDirectoryStream(dir, "????????_??????_" + file.getFileName())) {
			for (Path path: stream) 
				map.put(path.getFileName().toString(), path);
			
			int i = map.size() - historySize;
			for(Path path: map.values()) {
				if( i-- <= 0 )
					break;
				
				Files.delete(path);
			}
		} catch (IOException e) {
			env.printError(logger, e, name);
		}
	}



	private Set<TagRec> collectTags() {
		Set<TagRec> tags = new TreeSet<>();
		for (Module module: env.getModuleManager().getModules())
			if( isFlagCompatibleWith("AUTOSAVE", module))
				for (Tag tag: module.getTagTable().getTags("", true))
					if (tag.hasFlags(Flags.AUTOSAVE)  &&  tag.getStatus() != Tag.Status.Deleted)
						tags.add( new TagRec(module,tag));
		return tags;
	}


	/**
	 * @param file - path to values file
	 * @param checkTotalHash - this parameter is for version 0 only! True for main values, False for changes. 
	 * @return true if ok
	 */
	protected boolean readValues(Path file, boolean checkTotalHash) {
		CRC32 crcRead = new CRC32();

		int version = -1;
		boolean result = false;
		
		// for version 0
		boolean lineHashError = false;
		int totalCnt = 0;
		boolean totalHashError = true;
		boolean hasInitialHash = false;
		
		String line = null;
		
		if (Files.exists(file)) {
			try (BufferedReader reader = Files.newBufferedReader(file, charset)) {
				
				Module module = null;
				Tag tag = null;
				String[] ss;
				boolean hasTotalCrc = false;
				boolean enableCrc = true;
				crcRead.reset();

				/// version 0 - begin 
				String hash1;
				String hash2;
				boolean digested;
				StringBuilder sb = new StringBuilder();
				/// version 0 - end 

				while ((line = reader.readLine()) != null) {
					
					if( version == -1 ) {
						ss = line.split("\\s*;\\s*");
						for(String s: ss) {
							if( s.equals(TAGSAVER_FORMAT_1) )
								version = 1;
							else if( s.equals(CRC_OFF) ) {
								enableCrc = false;
								hasChanges = true;
							}
						}
						
						if( version > 0)
							continue;
						else
							version = 0;
					}
					
					if( version == 1 ) {
						ss = line.split("\\t+");
						
						if( ss.length == 4  ||  ss.length == 5) {
							// 0-crc   1-dt   2-mod   3-tag   4-value
							String crc = ss[IDX_CRC];
							String mod = ss[IDX_MODNAME];
							String tagname = ss[IDX_TAGNAME];
							String value = ss.length == 5? ss[IDX_VALUE]: "";

							if( enableCrc ) {
								String line1 = line.substring(crc.length());
								crcRead.update( line1.getBytes(charset) );
								long crc_ours = crcRead.getValue();
								long crc_theirs = Long.parseLong(crc, 16);
								if( crc_ours != crc_theirs ) {
									env.printError(logger, name, "Bad line crc:", file.toString() + CR_LF + line);
									return false;
								}
							}

							digested = false;
							module = env.getModuleManager().getModule(mod);
							if( module != null  &&  isFlagCompatibleWith("AUTOSAVE", module) ) {
								tag = module.getTagTable().get(tagname);
								if( tag != null  &&  tag.hasFlags(Flags.AUTOSAVE) ) {
									try {
										tag.setString(value);
										digested = true;
									} catch (NumberFormatException e) {
										env.printError(logger, e, name, file.toString() + CR_LF + line);
									}
									totalCnt++;
								} 
							}
							
							if( !digested ) {
								env.printInfo(logger, name, "Forgetting save value: ", 
										mod + "." + tagname + " = " + value);
								hasChanges = true;
							}
							
						} else
							
						if( ss.length == 1 ) {
							// total crc
							if( enableCrc ) {
								long crc_ours = 0xFFFFFFFFL - crcRead.getValue();
								long crc_theirs = Long.parseLong(ss[IDX_CRC], 16);
								if( crc_ours != crc_theirs ) {
									env.printError(logger, name, "Bad TOTAL crc:", file.toString() + CR_LF + line);
									return false;
								}
							}
							hasTotalCrc = true;
						} else 
						
						{
							env.printError(logger, name, "Bad line format:", file.toString() + CR_LF + line);
							return false;
						}
					} else

					/// version 0 - begin 
					if( version == 0 ) {
						if (line.length() > 0 && line.charAt(0) == '#') {
							if (hasInitialHash)
								totalHashError = false;
							else {
								String hhash1 = Integer.toString( Math.abs(sb.toString().hashCode()), Character.MAX_RADIX);
								String hhash2 = line.substring(1);
								totalHashError = !hhash1.equals(hhash2);
							}
							break;
						}
	
						ss = line.split("\\t");
						if (ss.length == 4) {
							digested = false;
							hash1 = Integer.toString( Math.abs(line.substring(0, line.length() - ss[3].length()).hashCode()), Character.MAX_RADIX);
							if (ss[3].equals("initial")) {
								hasInitialHash = true;
								hash2 = hash1;
							} else
								hash2 = ss[3];
	
							if (!hash1.equals(hash2)) {
								lineHashError = true;
								env.printError(logger, name, "File:", file.toString(), "Hash error:", line);
							} else {
								module = env.getModuleManager().getModule(ss[0]);
								if( module != null  &&  isFlagCompatibleWith("AUTOSAVE", module) ) {
									tag = module.getTagTable().get(ss[1]);
									if( tag != null  &&  tag.hasFlags(Flags.AUTOSAVE) ) {
										try {
											tag.setString(ss[2]);
											digested = true;
										} catch (NumberFormatException e) {
											env.printError(logger, e, name, "File:", file.toString(), "Line:", line);
										}
										totalCnt++;
									} 
								}
							}
							
							if( !digested ) {
								env.printInfo(logger, name, "Forgetting save value: ", ss[0]+"."+ss[1] + " = " + ss[2]);
								hasChanges = true;
							}
							sb.append(ss[3]); // ????????
						}
					} 
					/// version 0 - end 
				}
				
				/// version 0 - begin 
				if( version == 1  &&  !hasTotalCrc )
					throw new Exception("Total crc is absent");
				/// version 0 - end 

				result = true;
				
			} catch (Exception e) {
				env.printError(logger, e, name, "File:", file.toString());
				result = false;
			}

			
			if( version == 1  &&  result ) {
				if( totalCnt != tags.size() )
					hasChanges = true;
			} else
			
			/// version 0 - begin 
			if( version == 0  &&  result ) {
				if(hasInitialHash  ||  totalCnt != tags.size())
					hasChanges = true;
				if(checkTotalHash  &&  totalHashError) {
					env.printError(logger, name, "File:", file.toString(), "Total hash error");
					result = false;
				}
				if(lineHashError)
					result = false;
			}
			/// version 0 - end
			
		}

		return result;
	}

	private boolean saveValues(Path path) {
		boolean result = false;
		renameFile(path);
		
		Date date = new Date();
		crcWrite.reset();
		try (FileChannel fchannel = FileChannel.open(path, StandardOpenOption.WRITE, StandardOpenOption.CREATE)) {
			
			writeHeader(fchannel);
			
			for (TagRec rec: tags)
				writeTag(fchannel, rec, date);
			
			writeTotalCrc(fchannel);
			
			result = true;
		} catch (IOException e) {
			env.printError(logger, e, name, "File:", path.toString());
		}
		return result;
	}


	private void writeHeader(FileChannel fchannel) throws IOException {
		lastWriteBuffer = ByteBuffer.wrap((TAGSAVER_FORMAT_1 + CR_LF).getBytes(charset));
		fchannel.write( lastWriteBuffer );
	}


	private void writeTotalCrc(FileChannel fchannel) throws IOException {
		String totalcrc = Numbers.toHexString( 0xFFFFFFFF - crcWrite.getValue(), CRC_LENGHT);
		lastWriteBuffer = ByteBuffer.wrap( totalcrc.getBytes(charset) );
		fchannel.write( lastWriteBuffer );
		fchannel.force(false);
	}





	private void writeTag(FileChannel fchannel, TagRec rec, Date date) throws IOException {
		sb.setLength(0);
		sb.append( '\t');
		sb.append( sdf.format(date) );
		sb.append( '\t');
		sb.append( rec.module.getName() );
		sb.append( '\t');
		sb.append( rec.tag.getName() );
		sb.append( "\t\t\t");
		sb.append( rec.tag.getString() );
		crcWrite.update( sb.toString().getBytes(charset) );
		String linecrc = Numbers.toHexString( (int) crcWrite.getValue(), CRC_LENGHT);
		sb.insert( 0, linecrc );
		sb.append( CR_LF );

		lastWriteBuffer = ByteBuffer.wrap(sb.toString().getBytes(charset));
		fchannel.write( lastWriteBuffer	);
	}


	@Override
	public boolean closedownModule() {
		removeAsSignalListerFromAllOthers();
//		env.getModuleManager().getModules().stream()
//				.forEach( m -> m.removeSignalListener(this) );

		merge();
		return true;
	}



	protected void merge() {
		if( hasChanges ) {
			if (saveValues(pathSave)) {
				renameFile(pathChng);
				deleteOldHistory(pathSave);
				deleteOldHistory(pathChng);
			}
			
			if (saveValues(pathSaveMir)) {
				renameFile(pathChngMir);
				deleteOldHistory(pathSaveMir);
				deleteOldHistory(pathChngMir);
			}
		}
		hasChanges = false;
		chngLinesCnt = 0;
		crcWrite.reset();
	}



	private void renameFile(Path file) {
		try {
			if (Files.exists(file)) {
				String newname = LocalDateTime.now().format(dtFormatter) + file.getFileName();
				Path newfile = Paths.get(newname);
				Path dir = getHistoryDir(file);
				newfile = dir.resolve(newfile);
				Files.move(file, newfile);
			}
		} catch (IOException e) {
			env.printError(logger, e, name, "File renaming error:", file.toString());
		}
	}
	
	
	

	private boolean openFileChg() {
		if( openedChng ) 
			return true;
		
		try {
			fchannelChng = openForAppend(pathChng);
		} catch (IOException e) {
			env.printError(logger, e, name, "File:", pathChng.toString() );
			return false;
		}
		
		try {
			fchannelChngMir = openForAppend(pathChngMir);
		} catch (IOException e) {
			env.printError(logger, e, name, "File:", pathChngMir.toString() );
			try {
				fchannelChng.close();
			} catch (Exception e1) {
			}
			return false;
		}
		
		openedChng = true; 
		return true;
	}
	
	
	private FileChannel openForAppend(Path path) throws IOException {
		FileChannel fc;
		if (Files.exists(path)) {
			fc = FileChannel.open(path, StandardOpenOption.WRITE); //, StandardOpenOption.APPEND
			long pos = fc.size() - CRC_LENGHT;// - 1;
			if( pos > 0 )
				fc.position(pos);
		} else {
			fc = FileChannel.open(path, StandardOpenOption.WRITE, StandardOpenOption.CREATE);
			writeHeader(fc);
		}
		return fc;
	}

	
	
	private void closeChng() {
		if( openedChng ) {
				
			try {
				fchannelChng.close();
			} catch (IOException e) {
				env.printError(logger, e, name);
			}
		
			try {
				fchannelChngMir.close();
			} catch (IOException e) {
				env.printError(logger, e, name);
			}
			
			openedChng = false;
		}
	}




	@Override
	public void onSignal(Module sender, Signal signal) {
		hasSignalReload = true;
		env.logInfo(logger, name, "Will be reinitialized by " + sender.getName() + " " + signal);
	}


	private void onSignalReload() {
		int size_before = tags.size();
		Set<TagRec> newtags = collectTags();
		tags.retainAll( newtags );
		tags.removeIf(rec -> rec.tag.getStatus() == Tag.Status.Deleted);
		int size_after = tags.size();

		newtags.removeAll(tags);
		tags.addAll( newtags );

		hasChanges = hasChanges  ||  size_before != size_after  ||  newtags.size() > 0;
		merge();

		env.logInfo(logger, name, "Reinitialized");
	}



	@Override
	public String getInfo() {
		return enable? "tags=" + tags.size(): "disabled";
	}


	@Override
	protected boolean reload() {
		
		boolean res = false;
		TagsaverModule tmp = new TagsaverModule( plugin, name );
		
		if( tmp.load() ) {
			tmp.modeReload = true;
			try {
				if (tmp.prepare()) {
					copySettingsFrom(tmp);
					pathSave = tmp.pathSave;
					pathChng = tmp.pathChng;
					pathSaveMir = tmp.pathSaveMir;
					pathChngMir = tmp.pathChngMir;
					fchannelChng = tmp.fchannelChng;
					fchannelChngMir = tmp.fchannelChngMir;
					historySize = tmp.historySize;
					chngLinesCnt = tmp.chngLinesCnt;
					chngLinesMax = tmp.chngLinesMax;
					tags.clear();
					tags.addAll(tmp.tags);

					hasChanges = true;
					merge();

					res = true;
				}
			} finally {
				tmp.closedown();
			}
		}

		return res;
	}



}