package promauto.jroboplc.plugin.tagsaver;

import promauto.jroboplc.core.AbstractPlugin;
import promauto.jroboplc.core.api.Module;

public class TagsaverPlugin extends AbstractPlugin {

	private static final String PLUGIN_NAME = "tagsaver";
	
	
	@Override
    public String getPluginName() {
    	return PLUGIN_NAME;
    }

	@Override
    public String getPluginDescription(){
    	return "Tagsaver makes tags persistent";
    }
	
//	@Override
//    public Class<?> getModuleClass(){
//    	return TagsaverModule.class;
//    }
	

	@Override
	public Module createModule(String name, Object conf) {
		TagsaverModule m = new TagsaverModule(this, name);
    	modules.add(m);
    	return m.load(conf)? m: null;
	}

}
