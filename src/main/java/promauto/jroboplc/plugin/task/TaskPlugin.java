package promauto.jroboplc.plugin.task;

import promauto.jroboplc.core.AbstractPlugin;
import promauto.jroboplc.core.api.Module;

public class TaskPlugin extends AbstractPlugin {

	private static final String PLUGIN_NAME = "task";

	@Override
	public void initialize() {
		super.initialize();
		
		// making TaskManager know about our TaskModule class
//		env.getTaskManager().addTaskClass(TaskModule.class);
    }
	
	
	@Override
    public String getPluginName() {
    	return PLUGIN_NAME;
    }

	@Override
    public String getPluginDescription(){
    	return 
    			"Task support plugin. Creates and installs TaskManager, and further " +
    			"creates task. Each is a single module running in its own thread.";
    }
	
//	@Override
//    public Class<?> getModuleClass(){
//    	return TaskModule.class;
//    }
	
	
	@Override
	public Module createModule(String name, Object conf) {
    	TaskModule m = new TaskModule(this, name);
    	modules.add(m);
    	return m.load(conf)? m: null;
	}



}
