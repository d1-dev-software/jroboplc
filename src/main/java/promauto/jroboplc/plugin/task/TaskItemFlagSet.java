package promauto.jroboplc.plugin.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TaskItemFlagSet extends TaskItemFlagWait {
	final Logger logger = LoggerFactory.getLogger(TaskItemFlagSet.class);


	public TaskItemFlagSet(TaskModule parent, String args) {
		super(parent, args);
	}
	
	
	@Override
	public void execute(long maxTime) throws InterruptedException {
		
//		System.out.println(
//				String.format("%s: set(%s,%d) now is %d", 
//					parentTask.getModule().getName(), 
//					targetTaskModule.getName(), 
//					value,
//					targetTaskModule.stateBaton 
//					));
		if( targetTaskModule == null )
			return;
		
		targetTaskModule.stateBaton = value;
		targetTaskModule.tagStatBaton.setInt( targetTaskModule.stateBaton );
	}

}
