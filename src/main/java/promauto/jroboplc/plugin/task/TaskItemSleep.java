package promauto.jroboplc.plugin.task;

public class TaskItemSleep extends TaskItem {
	private long sleepTimeMs;

	public TaskItemSleep(TaskModule parent, String args) {
		super(parent, args);
		sleepTimeMs = Integer.parseInt( args.trim() );
	}
	

	@Override
	public boolean init() {
		return true;
	}

	@Override
	public void execute(long maxTime) throws InterruptedException {
		long timeend = System.currentTimeMillis() + sleepTimeMs;
		while ( (System.currentTimeMillis() - timeend) < 0  &&  !parentModule.isStopped ) {
			Thread.sleep(1);
		}
	}

}
