package promauto.jroboplc.plugin.messenger;

import java.nio.charset.Charset;
import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import promauto.jroboplc.core.AbstractModule;
import promauto.jroboplc.core.api.Configuration;
import promauto.jroboplc.core.api.Plugin;
import promauto.jroboplc.core.api.Tag;
import promauto.jroboplc.core.tags.TagRW;

import javax.mail.*;
import javax.mail.internet.*;


public class MessengerModule extends AbstractModule {
	private static final int BUFF_SIZE = 32768;
	private final Logger logger = LoggerFactory.getLogger(MessengerModule.class);
	private final static Charset charset = Charset.forName("UTF-8");


	protected Tag 		tagSubj;
	protected Tag 		tagText;
	protected TagRW		tagSend;
	protected Tag 		tagSuccess;
	protected Tag 		tagError;
	protected Tag 		tagErrcnt;

	protected String host;
	protected int    port;
	protected String user;
	protected String pass;
	protected String from;
	protected String to;

	
	public MessengerModule(Plugin plugin, String name) {
		super(plugin, name);
	}


//	type:   smtp
//	host:   smtp.mail.ru
//	port:   465
//	user:   550174@mail.ru
//	pass:   c.....lmail
//	subj:   test mail
//	text:   Hello, world!

	public boolean loadModule(Object conf) {
		Configuration cm = env.getConfiguration();

		tagSubj				= tagtable.createString("subj", cm.get(conf, "subj", ""));
		tagText				= tagtable.createString("text", cm.get(conf, "text", ""));
		tagSend			    = tagtable.createRWInt("send", 0);
		tagSuccess		    = tagtable.createInt("success", 0);
		tagError		    = tagtable.createBool("error", false);
		tagErrcnt		    = tagtable.createInt("errcnt", 0);

		port			= cm.get(conf, "port", 25);
		user			= cm.get(conf, "user", "");
		pass			= cm.get(conf, "pass", "");
		host			= cm.get(conf, "host", "");
		from			= cm.get(conf, "from", "");
		to			    = cm.get(conf, "to", "");

		return true;
	}


	@Override
	public boolean prepareModule() {
		return true;
	}



	@Override
	public boolean closedownModule() {
		return true;
	}



	@Override
	public boolean executeModule() {

		if( tagSend.hasWriteValue() ) {
			int sendval = tagSend.getWriteValInt();
			tagSend.setReadValInt( sendval );

			if( sendval > 0) {
				boolean res = sendEmail();

				if (res) {
					tagSuccess.setInt(tagSuccess.getInt() + 1);
					tagError.setBool(false);
				} else {
					tagErrcnt.setInt(tagErrcnt.getInt() + 1);
					tagError.setBool(true);
				}
			}
		}

		return true;
	}



	private boolean sendEmail() {
		Properties properties = System.getProperties();
		properties.put("mail.smtp.host"               , host);
		properties.put("mail.smtp.port"               , port);
		properties.put("mail.smtp.socketFactory.port" , port);
		properties.put("mail.smtp.auth"               , "true");
		properties.put("mail.smtp.ssl.enable"         , "true");
		properties.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");

		Session session = Session.getDefaultInstance(properties,
				new javax.mail.Authenticator() {
					protected javax.mail.PasswordAuthentication getPasswordAuthentication() {
						return new javax.mail.PasswordAuthentication(user, pass);
					}
				});

		try {
			MimeMessage message = new MimeMessage(session);

			message.setFrom(new InternetAddress(from));

			String[] tos = to.split("\\s* \\s*");
			for(String s: tos)
				message.addRecipient(Message.RecipientType.TO, new InternetAddress(s));

			message.setSubject( tagSubj.getString() );
			message.setText( tagText.getString() );
			Transport.send(message);

			return true;
		} catch (MessagingException e) {
			env.printError(logger, e, name);
		}
		return false;
	}


	@Override
	protected boolean reload() {
		MessengerModule tmp = new MessengerModule(plugin, name);
		if( !tmp.load() )
			return false;

		closedown();

		copySettingsFrom(tmp);

		tagSubj.copyFlagsFrom( tmp.tagSubj );
		tagText.copyFlagsFrom( tmp.tagText );

		port = tmp.port;
		user = tmp.user;
		pass = tmp.pass;
		host = tmp.host;
		from = tmp.from;
		to	 = tmp.to;

		return prepare();
	}

	

}











