package promauto.jroboplc.plugin.raduga;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import promauto.jroboplc.core.TagTable;
import promauto.jroboplc.core.api.ANSI;
import promauto.jroboplc.core.api.Configuration;
import promauto.jroboplc.core.api.Flags;
import promauto.jroboplc.core.tags.RefGroup;
import promauto.jroboplc.core.tags.RefItem;
import promauto.jroboplc.core.tags.TagRW;
import promauto.jroboplc.plugin.peripherial.PaGeliosDozkkmcModule;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Doser {
    private final Logger logger = LoggerFactory.getLogger(Doser.class);


//    public final static int DOSER_STATUS_IDLE            = 0;
//    public final static int DOSER_STATUS_LOAD_READY      = 1;
//    public final static int DOSER_STATUS_LOAD            = 2;
//    public final static int DOSER_STATUS_UNLOAD_READY    = 3;
//    public final static int DOSER_STATUS_TASK_READY      = 4;
//    public final static int DOSER_STATUS_ALARM           = 5;
//    public final static int DOSER_STATUS_UNLOAD_SUSP     = 6;
//    public final static int DOSER_STATUS_UNLOAD          = 7;


    public static final String ERROR_NONE = "";
    public static final String ERROR_NOT_LINKED = "NOT LINKED";
    public static final String ERROR_NOT_CONNECTED = "NOT CONNECTED";
    public static final String ERROR_CRC = "CRC ERROR";
    public static final String ERROR_TASK_DIFFER = "TASK DIFFER ERROR";

    private final Context ctx;
    public final int doserNum;
    public final Line line;
    private int prodtype;
    private int id;
    private String name;
    private String dupName;
    private String doserModuleName;

    public final List<Feeder> feeders;

    protected TagRW tagName;
    protected TagRW tagRcpWeight;
    protected TagRW tagSetWeight;
    protected TagRW tagExeWeight;
    protected TagRW tagCurWeight;
    protected TagRW tagCurProductName;
    protected TagRW tagSN;

    protected TagRW tagConnected;
    protected TagRW tagError;
    protected TagRW tagPassQty;
    protected TagRW tagPassCnt;


    // todo Сделать задержку разгрузке в dozkkmc
    //    protected Tag   tagUnloadDelay;

    protected RefItem refCurStor;
    protected RefItem refCurWeight;
    protected RefItem refStatus;
    protected RefItem refErrorFlag;
    protected RefItem refSN;
    protected RefItem refReplacement;
    protected RefItem refSendTask;
    protected RefItem refCmdStartCycle;
    protected RefItem refCmdStartTask;
    protected RefItem refCmdStartLoad;
    protected RefItem refCmdStartUnload;
    protected RefItem refSumWeightMax;
    protected RefItem refSetWeightMax;

    public final RefGroup refgr;


    public Doser(Context ctx, Line line, int doserNum) {
        this.ctx = ctx;
        this.line = line;
        this.doserNum = doserNum;
        prodtype = Prodtype.PRIMARY;
        feeders = new ArrayList<>();
        refgr = ctx.env.getRefFactory().createRefGroup();

        dupName = "";
    }

    public Doser(Context ctx, Line line, int doserNum, int prodtype) {
        this(ctx, line, doserNum);
        this.prodtype = prodtype;
    }

    public boolean load(Object conf) {
        Configuration cm = ctx.env.getConfiguration();
        name = cm.get(conf, "name", "");
        doserModuleName = cm.get(conf, "module", "");
        boolean res = true;

        for (String srcBunker : cm.getStringList(conf, "srcBunkers")) {
            Feeder feeder = new Feeder(ctx, this, feeders.size() + 1, srcBunker);
            res &= feeder.load();
            feeder.createRefs(doserModuleName);
            feeders.add(feeder);
        }

        TagTable tt = ctx.tagtable;
        ctx.addRepoTags(
                tagPassQty = tt.createRWInt(makeTagName("PassQty"), 0),
                tagPassCnt = tt.createRWInt(makeTagName("PassCnt"), 0),
                tagSN      = tt.createRWString(makeTagName("SN"), "", Flags.HIDDEN)
        );

        tagName             = tt.createRWString(makeTagName("Name"), name);
        tagConnected        = tt.createRWBool(makeTagName(  "Connected"), false);
        tagError            = tt.createRWString(makeTagName("Error"), "");
        tagRcpWeight        = tt.createRWLong(makeTagName(   "RcpWeight"), 0);
        tagSetWeight        = tt.createRWLong(makeTagName(   "SetWeight"), 0);
        tagExeWeight        = tt.createRWLong(makeTagName(   "ExeWeight"), 0);
        tagCurWeight        = tt.createRWLong(makeTagName(   "CurWeight"), 0);
        tagCurProductName   = tt.createRWString(makeTagName("CurProductName"), "");

        refCurStor          = refgr.createItem(doserModuleName, "CurStor");
        refCurWeight        = refgr.createItem(doserModuleName, "CurWeight");
        refStatus           = refgr.createItem(doserModuleName, "Status");
        refSendTask         = refgr.createItem(doserModuleName, "SendTask");
        refCmdStartCycle    = refgr.createItem(doserModuleName, "CmdStartCycle");
        refCmdStartTask     = refgr.createItem(doserModuleName, "CmdStartTask");
        refCmdStartLoad     = refgr.createItem(doserModuleName, "CmdStartLoad");
        refCmdStartUnload   = refgr.createItem(doserModuleName, "CmdStartUnload");
        refSumWeightMax     = refgr.createItem(doserModuleName, "SumWeightMax");
        refSetWeightMax     = refgr.createItem(doserModuleName, "SetWeightMax");
        refReplacement      = refgr.createItem(doserModuleName, "Replacement");
        refSN               = refgr.createItem(doserModuleName, "SYSTEM.SN");
        refErrorFlag        = refgr.createItemCrc(doserModuleName, "SYSTEM.ErrorFlag");
        refgr.createItemCrcSum(doserModuleName, "Crc32");

        return res;
    }

    public String makeTagName(String tagname) {
        return line.makeTagName("D" + doserNum + "_" + tagname);
    }

    public void init() throws SQLException {
        id = ctx.service.syncDoser(name, prodtype);
        for (Feeder feeder : feeders)
            feeder.init();
    }

    public void prepare() {
        refgr.prepare();
    }


    public void execute() throws SQLException {
        execute(false);
    }
    public void execute(boolean readonly) throws SQLException {
        String error = ERROR_NONE;
        boolean connected = false;

        if (!refgr.linkAndRead()) {
            error = ERROR_NOT_LINKED;
        } else if (!refgr.checkCrc32()) {
            error = ERROR_CRC;
        } else if (refErrorFlag.getTag().getBool()) {
            error = ERROR_NOT_CONNECTED;
        } else {
            connected = true;

            if( updateIfChangedSN()  ||  isReplacementMode() ) {
                acceptSumWeights();
            }

            executeFeeders(readonly);
            updateTags();
            if( !readonly  &&  isState(PaGeliosDozkkmcModule.STATUS_LOAD)  &&  isReqWeightDiffer() )
                error = ERROR_TASK_DIFFER;
        }

        tagConnected.setReadValBool(connected);
        tagError.setReadValString(error);
    }

    private boolean isReplacementMode() {
        return refReplacement.getValue().getBool();
    }


    private boolean updateIfChangedSN() {
        if( !refSN.getValue().getString().equals(tagSN.getString()) ) {
            tagSN.setReadValString(refSN.getValue().getString());
            return true;
        }
        return false;
    }

    private void updateTags() {
        long rcpWeightSum = 0;
        long setWeightSum = 0;
        long exeWeightSum = 0;
        for(Feeder feeder: feeders) {
            rcpWeightSum += feeder.tagRcpWeight.getLong();
            setWeightSum += feeder.tagSetWeight.getLong();
            exeWeightSum += feeder.tagExeWeight.getLong();
        }
        tagRcpWeight.setReadValLong( rcpWeightSum );
        tagSetWeight.setReadValLong( setWeightSum );
        tagExeWeight.setReadValLong( exeWeightSum );

        int curstor = refCurStor.getValue().getInt();
        if( curstor > 0  &&  curstor <= feeders.size() ) {
            Feeder feeder = feeders.get(curstor-1);
            tagCurProductName.setReadValString( feeder.tagProductName.getString() );
        } else {
            tagCurProductName.setReadValString("");
        }
        tagCurWeight.setReadValLong( refCurWeight.getValue().getLong() );
    }

    public void reset() {
        tagRcpWeight.setReadValLong(0);
        tagSetWeight.setReadValLong(0);
        tagExeWeight.setReadValLong(0);
        tagCurProductName.setReadValString("");
        tagPassQty.setReadValInt(0);
        tagPassCnt.setReadValInt(0);

        feeders.forEach(Feeder::reset);
    }


    public void installTask(Task task) {
        reset();
        task.products.stream()
                .filter(tp -> tp.doserName.equals(name)  ||  tp.doserName.equals(dupName))
                .filter(tp -> tp.feederNum > 0  &&  tp.feederNum <= feeders.size())
                .forEach(tp -> {
                    Feeder feeder = feeders.get(tp.feederNum - 1);
                    feeder.tagRcpWeight.setReadValLong(tp.weight);
                    feeder.tagSetWeight.setReadValLong(tp.weight);
                    feeder.tagProductId.setReadValInt(tp.productId);
                    feeder.tagProductName.setReadValString(tp.productName);
                });
        updateTags();
        updatePassQty();
        feeders.forEach(Feeder::calcSetWeightPass);
    }

    public void setSetWeight(long weight) {
        tagSetWeight.setReadValLong(weight);
        updatePassQty();

        long rcpweightSum = feeders.stream().mapToLong(Feeder::getRcpWeight).sum();

        if( rcpweightSum == 0)
            return;
        double ratio = (double)weight / tagRcpWeight.getDouble();
        long sum = 0;
        for(Feeder feeder: feeders) {
            rcpweightSum -= feeder.tagRcpWeight.getLong();
            long w = (rcpweightSum == 0)?
                    weight - sum:
                    Math.round( feeder.tagRcpWeight.getDouble() * ratio );
            feeder.setSetWeight(w);// tagSetWeight.setReadValLong(w);
            sum += w;
        }
    }

    private void updatePassQty() {
        int passqty;
        if(tagSetWeight.getInt() <= 0) {
            passqty = 0;
        } else
        if( refSetWeightMax.getValue().getLong() <= 0 ) {
            passqty = 1;
        } else {
            passqty = (int)Math.ceil(tagSetWeight.getDouble() / refSetWeightMax.getValue().getDouble());
        }
        tagPassQty.setReadValInt(passqty);
    }


    private void executeFeeders(boolean readonly) throws SQLException {
        for(Feeder feeder: feeders)
            feeder.execute(readonly);
    }

    private void acceptSumWeights() {
        for(Feeder feeder: feeders)
            feeder.acceptSumWeight();
    }


    private boolean isState(int state) {
        return refStatus.getValue().getInt() == state;
    }

    private boolean isReqWeightDiffer() {
        return feeders.stream().anyMatch(Feeder::isReqWeightDiffer);
    }

    public void beginCycle() {
        tagPassCnt.setReadValInt(0);
        feeders.forEach(Feeder::beginCycle);
    }

    public void resetExeWeight() {
        tagExeWeight.setReadValInt(0);
        feeders.forEach(Feeder::resetExeWeight);
    }

    public boolean isInUse() {
        return tagRcpWeight.getLong() > 0L  &&  tagPassCnt.getInt() < tagPassQty.getInt();
    }


    public void prepareTaskSend() {
        if( tagConnected.getBool()  &&  !isState(PaGeliosDozkkmcModule.STATUS_ALARM) ) {
            refCmdStartCycle.getTag().setInt(1);
            refSendTask.getTag().setInt(0);
        }
    }

    public boolean isTaskSendReady() {
        return tagConnected.getBool()  &&
                refSendTask.getValue().getInt() == 0  &&
                isState(PaGeliosDozkkmcModule.STATUS_TASK_READY);
    }

    public void sendTask() {
        if (tagConnected.getBool()  &&  refSendTask.getValue().getInt() == 0 ) {
            refSendTask.getTag().setInt(1);
        }
    }

    public boolean isTaskAccepted() {
        return tagConnected.getBool()  &&  refSendTask.getValue().getInt() > 0;
    }

    public boolean isTaskRejected() {
        return tagConnected.getBool()  &&  refSendTask.getValue().getInt() < 0;
    }

    public boolean isUnloadReady() {
        return tagConnected.getBool()  &&  isState(PaGeliosDozkkmcModule.STATUS_UNLOAD_READY);
    }

    public void startLoad() {
        if (tagConnected.getBool()) {
            refCmdStartTask.getTag().setInt(1);
            refCmdStartLoad.getTag().setInt(1);
        }
    }

    public boolean isUnloaded() {
        return tagConnected.getBool()  &&  isState(PaGeliosDozkkmcModule.STATUS_IDLE);
    }

    public void startUnload() {
        if (tagConnected.getBool()) {
            refCmdStartUnload.getTag().setInt(1);
        }
    }

    public void incrementPassCnt() {
        tagPassCnt.setReadValInt( tagPassCnt.getInt() + 1);
    }


    public int getNumberOfFeedersUsed() {
        return (int)feeders.stream().filter(feeder -> feeder.tagRcpWeight.getLong() > 0).count();
    }


    //////////////////////////////////////////   getters   ////////////////////////////////////////////////
    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setDupName(String name) {
        dupName = name;
    }


    public long getSumWeightMax() {
        return refSumWeightMax.getValue().getLong();
    }

    public long getSetWeight() {
        return tagSetWeight.getLong();
    }

    public long getRcpWeight() {
        return tagRcpWeight.getLong();
    }


    public String check() {
        return refgr.checkln();
    }

    public String getInfo() {
        return String.format("\r\n\r        D%d %s%s %s, f: %d, id=%d %s",
                doserNum,
                name,
                dupName.isEmpty() || dupName.equals(name)? "": "/" + dupName,
                ANSI.yellow(doserModuleName),
                feeders.size(),
                id,
                ANSI.redBold(tagError.getString())
        );
    }
}
