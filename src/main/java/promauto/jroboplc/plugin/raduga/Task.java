package promauto.jroboplc.plugin.raduga;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

public class Task {

    public static final int STATUS_EDIT               = 0;
    public static final int STATUS_READY              = 1;
//    public static final int STATUS_INUSE              = 2;
//    public static final int STATUS_FINISHED           = 3;

    public static class Param {
        public String paramName;
        public String val;

        public Param() {
        }

        public Param(String paramName, String val) {
            this.paramName = paramName;
            this.val = val;
        }
    }

    public static class Dispenser {
        public String dispenserName;
        public int productId;
        public String productName;
        public int percentage;
        public Dispenser() {
        }

        public Dispenser(String dispenserName, int productId, String productName, int percentage) {
            this.dispenserName = dispenserName;
            this.productId = productId;
            this.productName = productName;
            this.percentage = percentage;
        }
    }

    public static class Product {
        public int productId;
        public String productName;
        public String doserName;
        public int feederNum;
        public long weight;

        public Product() {
        }

        public Product(int productId, String productName, String doserName, int feederNum, long weight) {
            this.productId = productId;
            this.productName = productName;
            this.doserName = doserName;
            this.feederNum = feederNum;
            this.weight = weight;
        }
    }

    public static class Exec {
        public int taskId;
        public int recipeId;
        public int feederId;
        public int cycleId;
        public int inputProductId;
        public int inputBunkerId;
        public int outputProductId;
        public int outputBunkerId;
        public int shiftId;
        public int lineId;
        public LocalDateTime dt;
        public int periodShift;
        public int periodHour;
        public long weight;
        public long weightErr;
        public long sumWeightBeg;
        public long sumWeightEnd;
    }

    public static class ExecDsp {
        public int taskId;
        public int recipeId;
        public int dispenserId;
        public int inputProductId;
        public int outputProductId;
        public int lineId;
        public LocalDateTime dt;
        public int periodShift;
        public int periodHour;
        public long weight;
        public long sumWeightBeg;
        public long sumWeightEnd;
    }

    public int id;
    public int lineId;
    public String lineName;
    public int recipeId;
    public String recipeName;
    public int productId;
    public long totalExeWeight;
    public int status;
    public final List<Task.Product> products = new LinkedList<>();
    public final List<Task.Param> params = new LinkedList<>();
    public final List<Task.Dispenser> dispensers = new LinkedList<>();

    public boolean used;
    public boolean presented;


    public Task() {
    }

    public Task(
            int id,
            int lineId,
            String lineName,
            int recipeId,
            String recipeName,
            long totalExeWeight,
            int status
    ) {
        this.id = id;
        this.lineId = lineId;
        this.lineName = lineName;
        this.recipeId = recipeId;
        this.recipeName = recipeName;
        this.totalExeWeight = totalExeWeight;
        this.status = status;
        presented = true;
    }
}
