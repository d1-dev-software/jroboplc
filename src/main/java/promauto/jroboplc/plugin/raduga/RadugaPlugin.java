package promauto.jroboplc.plugin.raduga;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import promauto.jroboplc.core.AbstractModule;
import promauto.jroboplc.core.AbstractPlugin;
import promauto.jroboplc.core.api.Module;
import promauto.jroboplc.plugin.kkmansvr.KkmansvrModule;

public class RadugaPlugin extends AbstractPlugin {
	private static final String PLUGIN_NAME = "raduga";
	private final Logger logger = LoggerFactory.getLogger(RadugaPlugin.class);

	@Override
	public void initialize() {
		super.initialize();
	}

	@Override
	public String getPluginName() {
		return PLUGIN_NAME;
	}

	@Override
	public String getPluginDescription() {
		return "raduga kkorm server";
	}


	@Override
	public Module createModule(String name, Object conf) {
    	Module m = createDatabaseModule(name, conf);
    	if( m != null )
    		modules.add(m);
    	return m;
		
	}
	
	private Module createDatabaseModule(String name, Object conf) {
		RadugaModule m = new RadugaModule(this, name);
		modules.add(m);
		return m.load(conf)? m: null;
	}



}
