package promauto.jroboplc.plugin.raduga;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import promauto.jroboplc.core.TagTable;
import promauto.jroboplc.core.api.ANSI;
import promauto.jroboplc.core.tags.RefGroup;
import promauto.jroboplc.core.tags.RefItem;
import promauto.jroboplc.core.tags.TagRW;
import promauto.jroboplc.plugin.peripherial.PaGeliosDozkkmcModule;

import java.sql.SQLException;

public class CtlDoser {
    private final Logger logger = LoggerFactory.getLogger(CtlDoser.class);


    public static final String ERROR_NONE = "";
    public static final String ERROR_NOT_LINKED = "NOT LINKED";
    public static final String ERROR_NOT_CONNECTED = "NOT CONNECTED";
    private final Context ctx;
    public final Line line;
    protected final String doserModuleName;




    protected TagRW tagCurWeight;
    protected TagRW tagStable;

    protected TagRW tagConnected;
    protected TagRW tagError;


    protected RefItem refSetWeight1;
    protected RefItem refCurWeight;
    protected RefItem refStable;
    protected RefItem refStatus;
    protected RefItem refErrorFlag;
    protected RefItem refSendTask;
    protected RefItem refCmdStartCycle;
    protected RefItem refCmdStartTask;
    protected RefItem refCmdStartLoad;
    protected RefItem refCmdStopLoad;
    protected RefItem refCmdStartUnload;


    public final RefGroup refgr;


    public CtlDoser(Context ctx, Line line, String doserModuleName) {
        this.ctx = ctx;
        this.line = line;
        this.doserModuleName = doserModuleName;
        refgr = ctx.env.getRefFactory().createRefGroup();
    }

    public boolean load() {
        boolean res = true;

        TagTable tt = ctx.tagtable;
        tagConnected        = tt.createRWBool(makeTagName(   "Connected"), false);
        tagError            = tt.createRWString(makeTagName( "Error"    ), "");
        tagCurWeight        = tt.createRWLong(makeTagName(   "CurWeight"), 0);
        tagStable           = tt.createRWBool(makeTagName(   "Stable"   ), false);

        refSetWeight1       = refgr.createItem(doserModuleName, "SetWeight1");
        refCurWeight        = refgr.createItem(doserModuleName, "CurWeight");
        refStable           = refgr.createItem(doserModuleName, "Stable");
        refStatus           = refgr.createItem(doserModuleName, "Status");
        refSendTask         = refgr.createItem(doserModuleName, "SendTask");
        refCmdStartCycle    = refgr.createItem(doserModuleName, "CmdStartCycle");
        refCmdStartTask     = refgr.createItem(doserModuleName, "CmdStartTask");
        refCmdStartLoad     = refgr.createItem(doserModuleName, "CmdStartLoad");
        refCmdStopLoad      = refgr.createItem(doserModuleName, "CmdStopLoad");
        refCmdStartUnload   = refgr.createItem(doserModuleName, "CmdStartUnload");
        refErrorFlag        = refgr.createItem(doserModuleName, "SYSTEM.ErrorFlag");

        return res;
    }

    public String makeTagName(String tagname) {
        return line.makeTagName("DC_" + tagname);
    }


    public void prepare() {
        refgr.prepare();
    }


    public void execute() throws SQLException {
        String error = ERROR_NONE;
        boolean connected = false;

        if (!refgr.linkAndRead()) {
            error = ERROR_NOT_LINKED;
        } else if (refErrorFlag.getTag().getBool()) {
            error = ERROR_NOT_CONNECTED;
        } else {
            connected = true;
            refSetWeight1.getTag().setLong(2_000_000L);
            updateTags();
        }

        tagConnected.setReadValBool(connected);
        tagError.setReadValString(error);
    }

    private void updateTags() {
        tagCurWeight.setReadValLong( refCurWeight.getValue().getLong() );
        tagStable.setReadValBool( refStable.getValue().getBool() );
    }


    private boolean isState(int state) {
        return refStatus.getValue().getInt() == state;
    }

    public void prepareTaskSend() {
        if( tagConnected.getBool()  &&  !isState(PaGeliosDozkkmcModule.STATUS_ALARM) ) {
            refCmdStartCycle.getTag().setInt(1);
            refSendTask.getTag().setInt(0);
        }
    }

    public boolean isTaskSendReady() {
        return tagConnected.getBool()  &&
                refSendTask.getValue().getInt() == 0  &&
                isState(PaGeliosDozkkmcModule.STATUS_TASK_READY);
    }

    public void sendTask() {
        if (tagConnected.getBool()) {
            refSendTask.getTag().setInt(1);
        }
    }

    public boolean isTaskAccepted() {
        return tagConnected.getBool()  &&  refSendTask.getValue().getInt() > 0;
    }

    public boolean isTaskRejected() {
        return tagConnected.getBool()  &&  refSendTask.getValue().getInt() < 0;
    }


    public boolean isLoading() {
        return tagConnected.getBool()  &&  isState(PaGeliosDozkkmcModule.STATUS_LOAD);
    }

    public boolean isUnloadReady() {
        return tagConnected.getBool()  &&  isState(PaGeliosDozkkmcModule.STATUS_UNLOAD_READY);
    }

    public void startLoad() {
        if (tagConnected.getBool()) {
            refCmdStartTask.getTag().setInt(1);
            refCmdStartLoad.getTag().setInt(1);
        }
    }

    public void stopLoad() {
        if (tagConnected.getBool()) {
            refCmdStopLoad.getTag().setInt(1);
        }
    }

    public boolean isUnloaded() {
        return tagConnected.getBool()  &&  isState(PaGeliosDozkkmcModule.STATUS_IDLE);
    }

    public void startUnload() {
        if (tagConnected.getBool()) {
            refCmdStartUnload.getTag().setInt(1);
        }
    }

    public boolean isStable() {
        return tagConnected.getBool()  &&  refStable.getValue().getBool();
    }


//////////////////////////////////////////   getters   ////////////////////////////////////////////////

    public String check() {
        return refgr.checkln();
    }

    public String getInfo() {
        return String.format("\r\n\r        CtlDoser %s %s",
                ANSI.yellow(doserModuleName),
                ANSI.redBold(tagError.getString())
        );
    }

}
