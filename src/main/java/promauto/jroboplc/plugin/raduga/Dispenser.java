package promauto.jroboplc.plugin.raduga;

import promauto.jroboplc.core.TagTable;
import promauto.jroboplc.core.api.Configuration;
import promauto.jroboplc.core.api.Tag;
import promauto.jroboplc.core.tags.TagRW;

import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

public class Dispenser {
    private static final int SUM_WEIGHT_SIZE = 65536000;
    //    private final Logger logger = LoggerFactory.getLogger(Doser.class);

    private final Context ctx;
    public final Line line;
    private int id;
    private String name;
    private String descr;

    protected TagRW tagDescr;
    protected TagRW tagProductId;
    protected TagRW tagProductName;
    protected Tag   tagPercentage;
    protected TagRW tagSumWeight;
//    protected TagRW tagSumWeightBeg;
//    protected TagRW tagSumWeightEnd;
//    protected TagRW tagPeriod;

    private int lastSaveHour;


    public Dispenser(Context ctx, Line line) {
        this.ctx = ctx;
        this.line = line;
        lastSaveHour = getHour(ctx.now());
    }

    private int getHour(LocalDateTime dt) {
        return dt.getHour();
//        return dt.getMinute();
    }

    public boolean load(Object conf) {
        Configuration cm = ctx.env.getConfiguration();
        cm.toMap(conf).forEach((k, v) -> {
            name = k;
            descr = (String)v;
        });

        TagTable tt = ctx.tagtable;
        ctx.addRepoTags(
                tagProductId     = tt.createRWInt(      makeTagName("ProductId")     , 0),
                tagProductName   = tt.createRWString(   makeTagName("ProductName")   , ""),
                tagSumWeight     = tt.createRWLong(     makeTagName("SumWeight")     , 0L),
//                tagSumWeightEnd  = tt.createRWLong(     makeTagName("SumWeightBeg")  , 0L, Flags.HIDDEN),
//                tagSumWeightEnd  = tt.createRWLong(     makeTagName("SumWeightEnd")  , 0L, Flags.HIDDEN),
//                tagPeriod        = tt.createRWInt(      makeTagName("Period")        , 0, Flags.HIDDEN),
                tagPercentage    = tt.createInt(        makeTagName("Percentage")    , 0)
        );

        tagDescr = tt.createRWString(makeTagName("Descr"), "");

        return true;
    }

    private String makeTagName(String tagname) {
        return "dsp." + line.makeTagName(name + '_' + tagname);
    }


    public void init() throws SQLException {
        id = ctx.service.syncDispenser(
                line.getId(),
                name,
                descr
        );
    }

    public int getId() {
        return id;
    }

    public void reset() {
        tagProductId.setReadValInt(0);
        tagProductName.setReadValString("");
        tagPercentage.setInt(0);
    }

    public String getName() {
        return name;
    }


    public boolean isInUse() {
        return tagProductId.getInt() > 0;
    }

    public void installTask(Task task) {
        reset();
        for(Task.Dispenser td: task.dispensers) {
            if( td.dispenserName.equals(name)) {
                tagProductId.setReadValInt(td.productId);
                tagProductName.setReadValString(td.productName);
                tagPercentage.setInt(td.percentage);
                break;
            }
        }
    }

    public void execute() throws SQLException {
        if( tagProductId.hasWriteValue() ) {
            int productId = tagProductId.getWriteValInt();
            String productName = ctx.service.getProductName(productId);
            if( productName.isEmpty() ) {
                tagProductId.setReadValInt(0);
                tagProductName.setReadValString(productName);
            } else {
                tagProductId.setReadValInt(productId);
                tagProductName.setReadValString(productName);
            }
        }

//        if( tagSumWeightBeg.getLong() == 0  &&  tagSumWeight.getLong() > 0) {
//            tagSumWeightBeg.setReadValLong(tagSumWeight.getLong());
//            tagSumWeightEnd.setReadValLong(tagSumWeight.getLong());
//        }
//
//        if( tagSumWeight.hasWriteValue() ) {
//            long value1 = tagSumWeight.getLong();
//            long value2 = tagSumWeight.getWriteValLong();
//            long value = value2 - value1;
//            if( value < 0 ) {
//                if( value2 == 65535000 )
//            }
//        }


        LocalDateTime dt = ctx.now();
        int hour = getHour(dt);

        if( lastSaveHour == hour )
            return;
        lastSaveHour = hour;

        if( !tagSumWeight.hasWriteValue())
            return;

        long weightBeg = tagSumWeight.getLong();
        long weightEnd = tagSumWeight.getWriteValLong();
        tagSumWeight.setReadValLong(weightEnd);
        long weight = weightEnd - weightBeg;

        if( weight == 0 ) //  ||  weightBeg == 0)
            return;

        if( weight < 0 )
            weight += SUM_WEIGHT_SIZE;

        DataService svc = ctx.service;

        Task.ExecDsp te = new Task.ExecDsp();
        te.dt = dt;
        LocalDateTime dtPeriod = dt.minus(1, ChronoUnit.HOURS);
        te.periodShift = ctx.periods.calcPeriodShift(dtPeriod);
        te.periodHour = Periods.calcPeriodHour(dtPeriod);
        te.dispenserId = id;
        te.lineId = line.getId();
        te.taskId = line.getTaskId();
        te.recipeId = line.getRecipeId();
        te.inputProductId = tagProductId.getInt();
        te.outputProductId = line.getDstProductId();
        te.weight = weight;
        te.sumWeightBeg = weightBeg;
        te.sumWeightEnd = weightEnd;

        svc.saveTaskExecDsp(te);

    }
}





































