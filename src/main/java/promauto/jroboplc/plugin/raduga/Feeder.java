package promauto.jroboplc.plugin.raduga;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import promauto.jroboplc.core.TagTable;
import promauto.jroboplc.core.tags.RefItem;
import promauto.jroboplc.core.tags.TagRW;

import java.sql.SQLException;
import java.util.Optional;

public class Feeder {
    private final Logger logger = LoggerFactory.getLogger(Feeder.class);

    private final Context ctx;
    private int id;
    public final int feederNum;
    public final Doser doser;
    public final String bunkerName;
    private int bunkerId;

    protected TagRW tagRcpWeight;
    protected TagRW tagSetWeight;
    protected TagRW tagSetWeightPass;
    protected TagRW tagExeWeight;
    protected TagRW tagSumWeight;
    protected TagRW tagSumWeightBeg;
    protected TagRW tagProductId;
    protected TagRW tagProductName;

    protected RefItem refSumWeight;
    protected RefItem refReqWeight;
    protected RefItem refSetWeight;
    private boolean newborn;



    public Feeder(Context ctx, Doser doser, int feederNum, String bunkerName) {
        this.ctx = ctx;
        this.feederNum = feederNum;
        this.doser = doser;
        this.bunkerName = bunkerName;
        newborn = true;
    }

    public boolean load() {
        TagTable tt = ctx.tagtable;
        ctx.addRepoTags(
            tagRcpWeight     = tt.createRWLong(      makeTagName("RcpWeight")     , 0),
            tagSetWeight     = tt.createRWLong(      makeTagName("SetWeight")     , 0),
            tagSetWeightPass = tt.createRWLong(      makeTagName("SetWeightPass") , 0),
            tagExeWeight     = tt.createRWLong(     makeTagName("ExeWeight")     , 0),
            tagSumWeight     = tt.createRWLong(     makeTagName("SumWeight")     , 0L),
            tagSumWeightBeg  = tt.createRWLong(     makeTagName("SumWeightBeg")  , -1L),
            tagProductId     = tt.createRWInt(      makeTagName("ProductId")     , 0),
            tagProductName   = tt.createRWString(   makeTagName("ProductName")   , "")
        );
        return true;
    }

    public void createRefs(String bindModuleName) {
        refSumWeight = doser.refgr.createItemCrc(bindModuleName, "SumWeight" + feederNum);
        refReqWeight = doser.refgr.createItem(   bindModuleName, "ReqWeight" + feederNum);
        refSetWeight = doser.refgr.createItem(   bindModuleName, "SetWeight" + feederNum);
    }


    private String makeTagName(String tagname) {
        return doser.makeTagName("F" + feederNum + "_" + tagname);
    }

    public void init() throws SQLException {
        id = ctx.service.syncFeeder(doser.getId(), feederNum);
        bunkerId = ctx.service.syncFeederBunker(id, bunkerName);
    }

    public void execute() throws SQLException {
        execute(false);
    }

    public void execute(boolean readonly) throws SQLException {
        calcSetWeightPass();

        if( !readonly )
            refSetWeight.getTag().setLong( tagSetWeightPass.getLong() );

        if(newborn) {
            newborn = false;
            tagSumWeight.setReadValLong( refSumWeight.getValue().getLong() );
        }

        if( tagSumWeight.getLong() != refSumWeight.getValue().getLong() ) {
            if( !readonly )
                saveWeight();

            tagSumWeight.setReadValLong( refSumWeight.getValue().getLong() );
        }

        if( !readonly )
            updateTags();
    }

    public void acceptSumWeight() {
        tagSumWeight.setReadValLong( refSumWeight.getValue().getLong() );
        if( tagSumWeightBeg.getLong() != -1 )
            tagSumWeightBeg.setReadValLong( refSumWeight.getValue().getLong() );
    }


    private long getSubstractedWeight(long weightBeg, long weightEnd) {
        long weight = weightEnd - weightBeg;
        if( weight < 0 )
            weight += doser.getSumWeightMax() + 1;
        return weight;
    }

    private void saveWeight() throws SQLException {
        DataService svc = ctx.service;

        Task.Exec te = new Task.Exec();
        te.dt = ctx.now();
        te.periodShift = ctx.periods.calcPeriodShift(te.dt);
        te.periodHour = Periods.calcPeriodHour(te.dt);
        te.feederId = id;
        te.lineId = doser.line.getId();

        long weightBeg = tagSumWeight.getLong();
        long weightEnd = refSumWeight.getValue().getLong();

        Optional<Long> weightEndLast = svc.getTaskExecLastSumWeightEnd(id);
        if( weightEndLast.isPresent()) {
            te.weight = getSubstractedWeight(weightEndLast.get(), weightBeg);
            if (te.weight > 0) {
                te.sumWeightBeg = weightEndLast.get();
                te.sumWeightEnd = weightBeg;
                svc.saveTaskExec(te);
            }
        }

        te.taskId = doser.line.getTaskId();
        te.recipeId = doser.line.getRecipeId();
        te.cycleId = doser.line.getCycleId();
        te.weight = getSubstractedWeight(weightBeg, weightEnd);
        te.weightErr = te.weight - tagSetWeightPass.getLong();
        te.sumWeightBeg = weightBeg;
        te.sumWeightEnd = weightEnd;
        te.inputProductId = tagProductId.getInt();
        te.inputBunkerId = bunkerId;
        te.outputProductId = doser.line.getDstProductId();
        te.outputBunkerId = doser.line.getDstBunkerId();
        svc.saveTaskExec(te);

//        if( canCalcExeWeight() )
//            ctx.taskrepo.addTotalExeWeight( te.taskId, te.weight);

        doser.line.updateShift(te.periodShift, te.weight);
    }

    private void updateTags() {
        tagExeWeight.setReadValLong( canCalcExeWeight() ?
                getSubstractedWeight( tagSumWeightBeg.getLong(), refSumWeight.getValue().getLong()): 0
        );
    }

    private boolean canCalcExeWeight() {
        return tagSumWeightBeg.getLong() >= 0;
    }

    public void calcSetWeightPass() {
        int passQty = doser.tagPassQty.getInt();
        int passCnt = doser.tagPassCnt.getInt();
        long weight = 0;
        if( passCnt < passQty ) {
            if( passQty == 1 )
                weight = tagSetWeight.getLong();
            else {
                weight = Math.round(tagSetWeight.getDouble() / passQty);
                if( passCnt == passQty - 1 ) {
                    weight = tagSetWeight.getLong() - weight * passCnt;
                }
            }
        }
        tagSetWeightPass.setReadValLong( weight );
    }

    public void setSetWeight(long weight) {
        tagSetWeight.setReadValLong(weight);
        calcSetWeightPass();
    }

    public void reset() {
        tagRcpWeight.setReadValLong(0);
        tagSetWeight.setReadValLong(0);
        tagSetWeightPass.setReadValLong(0);
        tagExeWeight.setReadValLong(0);
        tagSumWeightBeg.setReadValLong(-1);
        tagProductId.setReadValInt(0);
        tagProductName.setReadValString("");
    }

    public void beginCycle() {
        tagSumWeightBeg.setReadValLong(refSumWeight.getValue().getLong());
    }

    public void resetExeWeight() {
        tagExeWeight.setReadValLong(0);
        tagSumWeightBeg.setReadValLong(-1);
    }

    public boolean isReqWeightDiffer() {
        return tagSetWeightPass.getLong() != refReqWeight.getValue().getLong();
    }


    public long getRcpWeight() {
        return tagRcpWeight.getLong();
    }

    public int getId() {
        return id;
    }

}