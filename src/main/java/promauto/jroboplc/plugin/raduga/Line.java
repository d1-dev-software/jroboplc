package promauto.jroboplc.plugin.raduga;

import java.sql.SQLException;
import java.util.List;

public interface Line {



    static class DstBunker {
        String name;
        int id;

        public DstBunker(String name) {
            this.name = name;
        }
    }

    boolean load(Object conf);

    boolean prepare();

    boolean init() throws SQLException;

    void execute() throws SQLException;

    String check();

    String getInfo();


    String makeTagName(String tagname);


    String getName();

    int getId();

    int getTaskId();
    int getRecipeId();

    int getCycleId();

    List<Doser> getDosers();

    int getDstBunkerId();
    int getDstProductId();

    void updateShift(int period, long plusWeight);

//    void addLineIntoGroup(Line line);
    List<Line> getLineGroup();

    long getExeWeightShf();

    void setExeWeightGrp(long weight);

}
