package promauto.jroboplc.plugin.raduga;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import promauto.jroboplc.core.AbstractModule;
import promauto.jroboplc.core.api.*;

import java.sql.Statement;
import java.util.stream.Collectors;

import static promauto.jroboplc.core.api.Signal.SignalType.*;

public class RadugaModule extends AbstractModule implements Signal.Listener {
	private final Logger logger = LoggerFactory.getLogger(RadugaModule.class);

	public static final String DBSCR_RADUGA = "dbscr/dbscr.raduga.yml";
	public static final String RADUGA_INIT_1 = "raduga.init1";
	public static final String RADUGA_INIT_2 = "raduga.init2";

	public static final String TABLE_REPO = "RD_REPO";

	private TagRepository tagrepo;

	private Database database;
//	private boolean connected;
	private boolean needInit;


	private Context ctx;

	public RadugaModule(Plugin plugin, String name) {
        super(plugin, name);
		ctx = new Context();
		ctx.setModule(this);
		ctx.setDataService( new DataServiceImpl() );
//		ctx.setTaskrepo( new TaskRepo(ctx) );
		ctx.setEnvironment( EnvironmentInst.get() );

	}




	public final boolean loadModule(Object conf) {
		Configuration cm = env.getConfiguration();
		ctx.emulated    = cm.get(conf, "emulate", false);
		ctx.databaseModuleName = cm.get(conf, "database", "db");
		ctx.deleteOtherStuff = cm.get(conf, "deleteOtherStuff", true);
		ctx.service.setArcSize(
				cm.get(conf, "shortRecs", 100_000),
				cm.get(conf, "shortDays", 60),          // 2 months
				cm.get(conf, "longRecs", 1_000_000),
				cm.get(conf, "longDays", 365*10)        // 10 years
		);

		ctx.periods.load(conf);

		// lines
		for(Object confLine: cm.toList(cm.get(conf, "lines"))) {
			String type = cm.get(confLine, "type", "simple").toLowerCase();
			Line line;

			if( type.equals("simple")) {
				line = new LineSimple(ctx);
			}
			else if( type.equals("combo")) {
				line = new LineCombo(ctx);
			}
			else {
				env.printError(logger, name, "Unknown line type:", type);
				return false;
			}

			if( !line.load(confLine) )
				return false;

			ctx.lines.add(line);
		}

		ctx.tagConnected = tagtable.createBool("connected", false);
		return true;
	}


	@Override
	public final boolean prepareModule() {
		if( ctx.emulated )
			return true;

		database = env.getModuleManager().getModule(ctx.databaseModuleName, Database.class);
		if (database == null) {
			env.printError(logger, name, "Database not found:", ctx.databaseModuleName);
			return false;
		}

		database.addSignalListener(this);
		ctx.service.setDb(database);
		tagrepo = database.createTagRepository("", TABLE_REPO);

		boolean res = ctx.lines.stream()
				.allMatch(Line::prepare);

		res &= database.loadScriptFromResource(RadugaPlugin.class, DBSCR_RADUGA) != null;

		needInit = res;
		return res;
	}


	@Override
	public void onSignal(Module sender, Signal signal) {
		if (signal.type == CONNECTED  &&  !ctx.tagConnected.getBool()) {
			needInit = true;
//			postSignal(RELOADED); ???
		}

		if (signal.type == DISCONNECTED) {
			ctx.tagConnected.setBool(false);
		}
	}


	private void init() {
		ctx.tagConnected.setBool( false );
		if( ctx.emulated  ||  database == null  ||  !database.isConnected())
			return;

	 	boolean res =
				database.executeScript(RADUGA_INIT_1).success &&
				database.executeScript(RADUGA_INIT_2).success;

		if( res ) {
			DataService svc = ctx.service;
			try (Statement st = svc.createStatement()) {
				svc.startSync();
				res = tagrepo.init();

				for(Line line: ctx.lines)
					res &= line.init();

				if( res )
					tagrepo.load(name, ctx.repoTags);

				svc.finishSync(ctx.deleteOtherStuff);

				svc.commit();
			} catch (Exception e) {
				env.printError(logger, e, name);
				svc.rollback();
			}
		}

		if( !res )
			env.printError(logger, name, "Initialization error");

//		ctx.taskrepo.clear();
		ctx.tagConnected.setBool(res);
		needInit = false;
	}


	@Override
	public boolean executeModule() {
		if( ctx.emulated ) {
			tagtable.acceptWriteValues();
			return true;
		}

		if( database == null  ||  !database.isConnected())
			return true;

		if( needInit )
			init();

		if( ctx.tagConnected.getBool() ) {
			try(Statement st = ctx.service.createStatement()) {

//				ctx.taskrepo.beforeUsing();

				for (Line line : ctx.lines)
					line.execute();

//				ctx.taskrepo.removeUnused();

				tagrepo.save(name, ctx.repoTags);
				tagrepo.persist();

				ctx.service.commit();
			} catch (Exception e) {
				env.printError(logger, e, name, ", Last sql =", ctx.service.getLastSql());
				ctx.service.rollback();
				ctx.tagConnected.setBool( false );
				needInit = true;
			}
		}
		return true;
	}


	@Override
	public boolean closedownModule() {
		ctx.tagConnected.setBool(false);
		if (database != null)
			database.removeSignalListener(this);
		return true;
	}


	@Override
	public String getInfo() {
		if( !enable )
			return "disabled";

		if( ctx.emulated )
			return "emulated";

		return String.format("%s%s, lines: %d %s\r\n",
				ctx.tagConnected.getBool()? "": ANSI.redBold("NOT CONNECTED! "),
				ctx.databaseModuleName,
				ctx.lines.size(),
				ctx.lines.stream()
						.map(Line::getInfo)
						.collect(Collectors.joining())
		);
	}


	@Override
	public String check() {
		return ctx.lines.stream()
				.map(Line::check)
				.filter(s -> !s.isEmpty())
				.collect(Collectors.joining("\r\n"));
	}


	@Override
	protected boolean reload() {
		RadugaModule tmp = new RadugaModule( plugin, name );
		if( !tmp.load() )
			return false;

		closedown();
		copySettingsFrom(tmp);

		tagtable.copyValuesTo(tmp.tagtable);
		tagtable.removeAll();
		tmp.tagtable.moveTagsTo(tagtable);

//		tmp.tagtable.values().forEach(tag -> {
//			Tag found = tagtable.get(tag.getName());
//			if( found != null  &&  found.getStatus() != Tag.Status.Deleted )
//				found.copyValueTo(tag);
//		});
//		tagtable.removeAll();
//		tmp.tagtable.values().forEach(tag -> tagtable.add(tag));
//		tmp.tagtable.clear();

		ctx = tmp.ctx;
		ctx.setModule(this);

		prepare();
//		postSignal(Signal.SignalType.RELOADED);
		return true;
	}


}

