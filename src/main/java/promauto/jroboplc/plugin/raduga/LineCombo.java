package promauto.jroboplc.plugin.raduga;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import promauto.jroboplc.core.TagTable;
import promauto.jroboplc.core.api.Configuration;
import promauto.jroboplc.core.api.Tag;
import promauto.jroboplc.core.tags.TagRW;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import static promauto.jroboplc.core.api.Flags.AUTOSAVE;

public class LineCombo extends LineSimple {
    private final Logger logger = LoggerFactory.getLogger(LineCombo.class);


    protected static final int STATE_HOLD_DOSERS                  = 8;
    protected static final int STATE_PREPARING_C                  = 9;
    protected static final int STATE_SETTING_C                    = 10;
    protected static final int STATE_START_LOAD_C                 = 11;
    protected static final int STATE_PREPARING_P                  = 12;
    protected static final int STATE_SETTING_P                    = 13;
    protected static final int STATE_CAN_LOAD_P                   = 14;
    protected static final int STATE_LOADING_P                    = 15;
    protected static final int STATE_HOLD_TRANSPORT               = 16;
    protected static final int STATE_UNLOADING_P                  = 17;
    protected static final int STATE_PREPARING_LOOP_P             = 18;
    protected static final int STATE_SETTING_LOOP_P               = 19;
    protected static final int STATE_LOADING_LOOP_P               = 20;
    protected static final int STATE_UNLOADING_LOOP_P             = 21;
    protected static final int STATE_UNLOADED_P                   = 22;
    protected static final int STATE_DELAY_TRANSPORT_P            = 23;
    protected static final int STATE_WAIT_CRUSHER_EMPTY           = 24;
    protected static final int STATE_LOADING_C                    = 25;
    protected static final int STATE_STOP_LOAD_C                  = 26;
    protected static final int STATE_PREPARING_M                  = 27;
    protected static final int STATE_SETTING_M                    = 28;
    protected static final int STATE_CAN_LOAD_M                   = 29;
    protected static final int STATE_LOADING_M                    = 30;
    protected static final int STATE_WAIT_MIXER_READY             = 31;
    protected static final int STATE_MIXER_START                  = 32;
    protected static final int STATE_UNLOADING_M                  = 33;
    protected static final int STATE_PREPARING_LOOP_M             = 34;
    protected static final int STATE_SETTING_LOOP_M               = 35;
    protected static final int STATE_LOADING_LOOP_M               = 36;
    protected static final int STATE_UNLOADING_LOOP_M             = 37;
    protected static final int STATE_UNLOADING_C                  = 38;
    protected static final int STATE_DELAY_AFTER_UNLOAD_C_M       = 39;
    protected static final int STATE_MIXER_LOADED                 = 40;

    static {
        STATES.put(STATE_HOLD_DOSERS                , "Ожидание освобождения дозаторов другой линией");
        STATES.put(STATE_PREPARING_C                , "Подготовка дозатора контроля к старту цикла");
        STATES.put(STATE_SETTING_C                  , "Установка задания дозатору контроля");
        STATES.put(STATE_START_LOAD_C               , "Старт загрузки дозатора контроля");
        STATES.put(STATE_PREPARING_P                , "Подготовка дозаторов ОК к старту цикла");
        STATES.put(STATE_SETTING_P                  , "Установка задания дозаторам ОК");
        STATES.put(STATE_CAN_LOAD_P                 , "Ожидание внешнего разрешения загрузки дозаторов ОК");
        STATES.put(STATE_LOADING_P                  , "Загрузка дозаторов ОК");
        STATES.put(STATE_HOLD_TRANSPORT             , "Ожидание освобождения транспорта ОК другой линией");
        STATES.put(STATE_UNLOADING_P                , "Разгрузка дозаторов ОК");
        STATES.put(STATE_PREPARING_LOOP_P           , "Подготовка дозаторов ОК к старту цикла (доп.проход)");
        STATES.put(STATE_SETTING_LOOP_P             , "Установка задания дозаторам ОК (доп.проход)");
        STATES.put(STATE_LOADING_LOOP_P             , "Загрузка дозаторов ОК (доп.проход)");
        STATES.put(STATE_UNLOADING_LOOP_P           , "Разгрузка дозаторов ОК (доп.проход)");
        STATES.put(STATE_UNLOADED_P                 , "Разгрузка дозаторов ОК выполнена");
        STATES.put(STATE_DELAY_TRANSPORT_P          , "Задержка времени для транспорта ОК");
        STATES.put(STATE_WAIT_CRUSHER_EMPTY         , "Ожидание опустошения дробилки");
        STATES.put(STATE_CAN_LOAD_M                 , "Ожидание внешнего разрешения загрузки дозаторов МК");
        STATES.put(STATE_LOADING_C                  , "Загрузка дозатора контроля (набор мин.вес, стаб.вес)");
        STATES.put(STATE_STOP_LOAD_C                , "Остановка загрузки дозатора контроля");
        STATES.put(STATE_PREPARING_M                , "Подготовка дозаторов МК к старту цикла");
        STATES.put(STATE_SETTING_M                  , "Установка задания дозаторам МК");
        STATES.put(STATE_LOADING_M                  , "Загрузка дозаторов МК");
        STATES.put(STATE_WAIT_MIXER_READY           , "Ожидание готовности смесителя к загрузке");
        STATES.put(STATE_MIXER_START                , "Запуск смесителя и шнеков разгрузки в смеситель");
        STATES.put(STATE_UNLOADING_M                , "Разгрузка дозаторов в смеситель");
        STATES.put(STATE_PREPARING_LOOP_M           , "Подготовка дозаторов МК к старту цикла (доп.проход)");
        STATES.put(STATE_SETTING_LOOP_M             , "Установка задания дозаторам МК (доп.проход)");
        STATES.put(STATE_LOADING_LOOP_M             , "Загрузка дозаторов МК (доп.проход)");
        STATES.put(STATE_UNLOADING_LOOP_M           , "Разгрузка дозаторов МК в смеситель (доп.проход)");
        STATES.put(STATE_UNLOADING_C                , "Разгрузка контрольного дозатора в смеситель");
        STATES.put(STATE_DELAY_AFTER_UNLOAD_C_M     , "Задержка времени после разгрузки дозаторов в смеситель");
        STATES.put(STATE_MIXER_LOADED               , "Запуск цикла смешивания");
    }

    protected static final String ERROR_CONFLICT_HOLD_DOSERS    = "Конфликт использования дозаторов ОК";
    protected static final String ERROR_CONFLICT_HOLD_TRANSPORT  = "Конфликт использования транспорта ОК";
    protected static final String ERROR_DOSER_REJECTED_TASK_C     = ERROR_DOSER_REJECTED_TASK + " (контрольный)";
    protected static final String ERROR_DOSER_REJECTED_TASK_P     = ERROR_DOSER_REJECTED_TASK + " (ОК)";
    protected static final String ERROR_DOSER_REJECTED_TASK_M     = ERROR_DOSER_REJECTED_TASK + " (МК)";

    protected TagRW tagHoldDosers;
    protected TagRW tagHoldTransport;
    protected TagRW tagRcpWeightP;
    protected TagRW tagSetWeightP;
    protected TagRW tagExeWeightP;
    protected TagRW tagExeWeightC;
    protected TagRW tagRcpWeightM;
    protected TagRW tagSetWeightM;
    protected TagRW tagExeWeightM;
    protected TagRW tagSetWeightPM;
    protected TagRW tagTimeCnt;

    protected Tag   tagTimeTransportP;
    protected Tag tagTimeAfterUnloadCM;
    protected Tag tagUnderweightLimitC;

    protected Tag   tagInpCrusherEmpty;
    protected TagRW tagCmdMixerStart;
    protected Tag tagInpMixerLoadReady;
    protected TagRW tagCmdMixerLoaded;
    protected Tag   tagInpMixerBusy;

    protected Tag   tagEnableLoadP;
    protected Tag   tagEnableUnloadP;
    protected Tag   tagEnableLoadM;
    protected Tag   tagEnableUnloadM;

    protected final List<Doser> dosersP;
    protected final List<Doser> dosersM;
    protected CtlDoser ctlDoser;
    protected long timeHoldCatching;

    protected LineCombo(Context ctx) {
        super(ctx);
        dosersP = new ArrayList<>();
        dosersM = new ArrayList<>();
    }

    @Override
    public boolean load(Object conf) {
        boolean res = super.load(conf);

        Configuration cm = ctx.env.getConfiguration();
        String ctlDoserModule = cm.get(conf, "ctlDoserModule", "");
        if( !validatePresented("ctlDoserModule", ctlDoserModule) )
            return false;

        ctlDoser = new CtlDoser(ctx, this, ctlDoserModule);
        res &= ctlDoser.load();

        dosersP.addAll(dosers);

        for(Object confDoser: cm.toList(cm.get(conf, "microDosers"))) {
            Doser doser = new Doser(ctx, this, dosers.size() + 1, Prodtype.MICRO);
            res &= doser.load(confDoser);
            dosers.add(doser);
            dosersM.add(doser);
        }

        TagTable tt = ctx.tagtable;
        ctx.addRepoTags(
                tagHoldDosers    = tt.createRWInt(  makeTagName("HoldDosers"), 0),
                tagHoldTransport = tt.createRWInt(  makeTagName("HoldTransport"), 0),
                tagExeWeightC    = tt.createRWLong( makeTagName("ExeWeightC"), 0),
                tagTimeCnt       = tt.createRWInt(  makeTagName("TimeCnt"), 0),
                tagSetWeightP    = tt.createRWLong( makeTagName("SetWeightP"), 0),
                tagSetWeightM    = tt.createRWLong( makeTagName("SetWeightM"), 0),
                tagSetWeightPM    = tt.createRWLong( makeTagName("SetWeightPM"), 0)
        );

        tagRcpWeightP            = tt.createRWLong( makeTagName("RcpWeightP")           , 0);
        tagExeWeightP            = tt.createRWLong( makeTagName("ExeWeightP")           , 0);
        tagRcpWeightM            = tt.createRWLong( makeTagName("RcpWeightM")           , 0);
        tagExeWeightM            = tt.createRWLong( makeTagName("ExeWeightM")           , 0);

        tagTimeTransportP        = tt.createInt(    makeTagName("TimeTransportP")       , 0, AUTOSAVE);
        tagTimeAfterUnloadCM     = tt.createInt(    makeTagName("TimeAfterUnloadCM")    , 0, AUTOSAVE);
        tagUnderweightLimitC     = tt.createLong(   makeTagName("UnderweightLimitC")    , 0, AUTOSAVE);

        tagInpCrusherEmpty       = tt.createBool(   makeTagName("InpCrusherEmpty")      , false);

        tagCmdMixerStart     = tt.createRWBool( makeTagName("CmdMixerStart"), false);
        tagInpMixerLoadReady = tt.createBool(   makeTagName("InpMixerLoadReady"), false);

        tagCmdMixerLoaded    = tt.createRWBool( makeTagName("CmdMixerLoaded"), false);
        tagInpMixerBusy      = tt.createBool(   makeTagName("InpMixerBusy")  , false);

        tagEnableLoadP    = tt.createBool(     makeTagName("EnableLoadP")   , false);
        tagEnableUnloadP  = tt.createBool(     makeTagName("EnableUnloadP") , false);
        tagEnableLoadM    = tt.createBool(     makeTagName("EnableLoadM")   , false);
        tagEnableUnloadM  = tt.createBool(     makeTagName("EnableUnloadM") , false);

        return res;
    }

    @Override
    public boolean prepare() {
        ctlDoser.prepare();
        return super.prepare();
    }


    @Override
    public void execute() throws SQLException {
        updateDstBunker();
        executeDosers();
        ctlDoser.execute();
        executeDispensers();
        updateTags();

        updateButtons();
        updateTaskInstall();

        int state = tagState.getInt();

        // 0
        if( state == STATE_NONE  &&  hasTask() ) {
            state = STATE_IDLE;
        }

        // 1
        if( state == STATE_IDLE ) {
            if( hasTask() ) {
                updateSetWeight();
                if (tagStart.getBool() && tagSetWeight.getLong() > 0) {
                    state = STATE_HOLD_DOSERS;
                }
            } else {
                state = STATE_NONE;
            }
        }

        // 8
        if( state == STATE_HOLD_DOSERS ) {
            if( canHoldDosers() ) {
                state = STATE_PREPARING_C;
                startHoldCatching();
                generateCycleId();
                beginCycle();
            }
        }

        // 9
        if( state == STATE_PREPARING_C ) {
            ctlDoser.prepareTaskSend();
            if( ctlDoser.isTaskSendReady() ) {
                ctlDoser.sendTask();
                state = STATE_SETTING_C;
            }
        }

        // 10
        if( state == STATE_SETTING_C) {
            if( ctlDoser.isTaskAccepted() ) {
                state = STATE_START_LOAD_C;
            } else
            if( ctlDoser.isTaskRejected() ) {
                state = STATE_ERROR;
                setError(ERROR_DOSER_REJECTED_TASK_C);
            } else {
                ctlDoser.sendTask();
            }
        }

        // 11
        if( state == STATE_START_LOAD_C) {
            if( ctlDoser.isLoading() ) {
                state = STATE_PREPARING_P;
            } else {
                ctlDoser.startLoad();
            }
        }

        // 12
        if( state == STATE_PREPARING_P ) {
            prepareTaskSend(dosersP);
            if( isTaskSendReady(dosersP) ) {
                sendTask(dosersP);
                state = STATE_SETTING_P;
            }
        }

        // 13
        if( state == STATE_SETTING_P) {
            if( isTaskAccepted(dosersP) ) {
                state = STATE_CAN_LOAD_P;
            } else
            if( isTaskRejected(dosersP) ) {
                state = STATE_ERROR;
                setError(ERROR_DOSER_REJECTED_TASK_P);
            } else {
                sendTask(dosersP);
            }
        }

        // 14
        if( state == STATE_CAN_LOAD_P  &&  canLoadP() ) {
            state = STATE_LOADING_P;
        }

        // 15
        if( state == STATE_LOADING_P) {
            if( isUnloadReady(dosersP) ) {
                state = STATE_HOLD_TRANSPORT;
                startHoldCatching();
            } else {
                startLoad(dosersP);
            }
        }

        // 16
        if( state == STATE_HOLD_TRANSPORT ) {
            if( canHoldTransport() ) {
                tagHoldTransport.setReadValInt(1);
                state = STATE_UNLOADING_P;
            }
        }

        // 17
        if( state == STATE_UNLOADING_P) {
            if( isUnloaded(dosersP) ) {
                incrementPassCnt(dosersP);
                if( needMorePasses(dosersP) ) {
                    state = STATE_PREPARING_LOOP_P;
                } else {
                    state = STATE_UNLOADED_P;
                }
            } else {
                if (canUnloadP())
                    startUnload(dosersP);
            }
        }

        //
        if( state == STATE_PREPARING_LOOP_P ) {
            prepareTaskSend(dosersP);
            if( isTaskSendReady(dosersP) ) {
                sendTask(dosersP);
                state = STATE_SETTING_LOOP_P;
            }
        }

        //
        if( state == STATE_SETTING_LOOP_P) {
            if( isTaskAccepted(dosersP) ) {
                state = STATE_LOADING_LOOP_P;
            } else
            if( isTaskRejected(dosersP) ) {
                state = STATE_ERROR;
                setError(ERROR_DOSER_REJECTED_TASK_P);
            } else {
                sendTask(dosersP);
            }
        }

        //
        if( state == STATE_LOADING_LOOP_P) {
            if( isUnloadReady(dosersP) ) {
                state = STATE_UNLOADING_LOOP_P;
            } else {
                startLoad(dosersP);
            }
        }

        //
        if( state == STATE_UNLOADING_LOOP_P) {
            if( isUnloaded(dosersP) ) {
                incrementPassCnt(dosersP);
                if( needMorePasses(dosersP) ) {
                    state = STATE_PREPARING_LOOP_P;
                } else {
                    state = STATE_UNLOADED_P;
                }
            } else {
                if (canUnloadP())
                    startUnload(dosersP);
            }
        }

        if( state == STATE_UNLOADED_P) {
            state = STATE_DELAY_TRANSPORT_P;
            tagTimeCnt.setReadValInt(tagTimeTransportP.getInt());
        }

        // 18
        if( state == STATE_DELAY_TRANSPORT_P) {
            if(tagTimeCnt.getInt() <= 0) {
                state = STATE_WAIT_CRUSHER_EMPTY;
            } else {
                tagTimeCnt.setReadValInt( tagTimeCnt.getInt() - 1 );
            }
        }

        // 19
        if( state == STATE_WAIT_CRUSHER_EMPTY ) {
            if( tagInpCrusherEmpty.getBool() ) {
                state = STATE_LOADING_C;
            }
        }

        // 20
        if( state == STATE_LOADING_C ) {
            if( ctlDoser.isStable()  &&  (tagExeWeightP.getLong() - ctlDoser.tagCurWeight.getLong() <= tagUnderweightLimitC.getLong()) ) {
                state = STATE_STOP_LOAD_C;
            }
        }

        // 21
        if( state == STATE_STOP_LOAD_C ) {
            if( ctlDoser.isStable()  &&  ctlDoser.isUnloadReady() ) {
                tagExeWeightC.setReadValLong( ctlDoser.tagCurWeight.getLong() );
                saveExeWeightC();
                updateSetWeightM();
                state = STATE_PREPARING_M;
            } else {
                ctlDoser.stopLoad();
            }
        }

        // 22
        if( state == STATE_PREPARING_M ) {
            prepareTaskSend(dosersM);
            if( isTaskSendReady(dosersM) ) {
                sendTask(dosersM);
                state = STATE_SETTING_M;
            }
        }

        // 23
        if( state == STATE_SETTING_M) {
            if( isTaskAccepted(dosersM) ) {
                state = STATE_CAN_LOAD_M;
            } else
            if( isTaskRejected(dosersM) ) {
                state = STATE_ERROR;
                setError(ERROR_DOSER_REJECTED_TASK_M);
            } else {
                sendTask(dosersM);
            }
        }

        // 24
        if( state == STATE_CAN_LOAD_M  &&  canLoadM() ) {
            state = STATE_LOADING_M;
        }

        // 25
        if( state == STATE_LOADING_M) {
            if( isUnloadReady(dosersM) ) {
                state = STATE_WAIT_MIXER_READY;
            } else {
                startLoad(dosersM);
            }
        }

        // 26
        if( state == STATE_WAIT_MIXER_READY) {
            if( !tagInpMixerBusy.getBool() ) {
                state = STATE_MIXER_START;
            }
        }

        // 27
        if( state == STATE_MIXER_START) {
            if( tagInpMixerLoadReady.getBool() ) {
                state = STATE_UNLOADING_M;
            }
        }

        // 28
        if( state == STATE_UNLOADING_M) {
            if( isUnloaded(dosersM) ) {
                incrementPassCnt(dosersM);
                if( needMorePasses(dosersM) ) {
                    state = STATE_PREPARING_LOOP_M;
                } else {
                    state = STATE_UNLOADING_C;
                }
            } else {
                if (canUnloadM()) {
                    ctlDoser.startUnload();
                    startUnload(dosersM);
                }
            }
        }

        // 29
        if( state == STATE_PREPARING_LOOP_M) {
            prepareTaskSend(dosersM);
            if( isTaskSendReady(dosersM) ) {
                sendTask(dosersM);
                state = STATE_SETTING_LOOP_M;
            }
        }

        // 30
        if( state == STATE_SETTING_LOOP_M) {
            if( isTaskAccepted(dosersM) ) {
                state = STATE_LOADING_LOOP_M;
            } else
            if( isTaskRejected(dosersM) ) {
                state = STATE_ERROR;
                setError(ERROR_DOSER_REJECTED_TASK_M);
            } else {
                sendTask(dosersM);
            }
        }

        // 31
        if( state == STATE_LOADING_LOOP_M) {
            if( isUnloadReady(dosersM) ) {
                state = STATE_UNLOADING_LOOP_M;
            } else {
                startLoad(dosersM);
            }
        }

        // 32
        if( state == STATE_UNLOADING_LOOP_M) {
            if( isUnloaded(dosersM) ) {
                incrementPassCnt(dosersM);
                if( needMorePasses(dosersM) ) {
                    state = STATE_PREPARING_LOOP_M;
                } else {
                    state = STATE_UNLOADING_C;
                }
            } else {
                if (canUnloadM()) {
                    ctlDoser.startUnload();
                    startUnload(dosersM);
                }
            }
        }

        // 33
        if( state == STATE_UNLOADING_C) {
            if( ctlDoser.isUnloaded() ) {
                state = STATE_DELAY_AFTER_UNLOAD_C_M;
                tagTimeCnt.setReadValInt(tagTimeAfterUnloadCM.getInt());
            } else {
                ctlDoser.startUnload();
            }
        }


        // 34
        if( state == STATE_DELAY_AFTER_UNLOAD_C_M) {
            if(tagTimeCnt.getInt() <= 0) {
                state = STATE_MIXER_LOADED;
            } else {
                tagTimeCnt.setReadValInt( tagTimeCnt.getInt() - 1 );
            }
        }

        // 35
        if( state == STATE_MIXER_LOADED) {
            if( tagInpMixerBusy.getBool() ) {
                state = STATE_CYCLE_FINISHED;
            }
        }

        // 100
        else if( state == STATE_CYCLE_FINISHED) {
            // todo delay before next cycle
            state = STATE_IDLE;
            resetExeWeight();
            if( !autostart )
                tagStart.setOff();
        }


        tagHoldDosers.setReadValBool( state > STATE_HOLD_DOSERS  &&  state <= STATE_UNLOADED_P );
        if( tagHoldDosers.getBool()  && isTooManyHoldDosers() ) {
            tagHoldDosers.setReadValBool(false);
            setError(ERROR_CONFLICT_HOLD_DOSERS);
            state = STATE_ERROR;
        }

//        if( tagHoldDosers.getBool()  &&  tagHoldTransport.getBool() ) {
//            // if second (or above) pass - keep holding transport
//        } else {
//            tagHoldTransport.setReadValBool(state > STATE_HOLD_TRANSPORT && state <= STATE_DELAY_TRANSPORT_P);
//        }

        tagHoldTransport.setReadValBool(state > STATE_HOLD_TRANSPORT && state <= STATE_DELAY_TRANSPORT_P);
        if( tagHoldTransport.getBool()  &&  isTooManyHoldTransport() ) {
            tagHoldTransport.setReadValBool(false);
            setError(ERROR_CONFLICT_HOLD_TRANSPORT);
            state = STATE_ERROR;
        }

        tagCmdMixerStart.setReadValBool( state >= STATE_MIXER_START &&  state <= STATE_MIXER_LOADED);
        tagCmdMixerLoaded.setReadValBool( state == STATE_MIXER_LOADED);
        tagCmdUnload.setReadValBool( state >= STATE_UNLOADING_P &&  state <= STATE_UNLOADING_LOOP_P);

        setState(state);
    }

    private boolean canLoadP() {
        return canLoad()  &&  tagEnableLoadP.getBool();
    }

    private boolean canUnloadP() {
        return canUnload()  &&  tagEnableUnloadP.getBool();
    }

    private boolean canLoadM() {
        return canLoad()  &&  tagEnableLoadM.getBool();
    }

    private boolean canUnloadM() {
        return canUnload()  &&  tagEnableUnloadM.getBool();
    }


    private void saveExeWeightC() throws SQLException {
        ctx.service.saveTaskCtl(
                id,
                tagTaskId.getInt(),
                tagCycleId.getInt(),
                ctx.now(),
                tagExeWeightC.getLong()
        );
    }

    protected boolean canCancel() {
        return tagState.getInt() < STATE_LOADING_P;
    }

//    protected boolean canInstall() {
//        return tagState.getInt() <= STATE_IDLE;
////        return (tagState.getInt() <= STATE_IDLE  ||  tagState.getInt() > STATE_UNLOADING_C_M)  &&  !isStateError();
//    }

    @Override
    protected void reset() {
        super.reset();
        tagHoldDosers.setReadValInt(0);
        tagHoldTransport.setReadValInt(0);
        tagRcpWeightP.setReadValLong(0);
        tagSetWeightP.setReadValLong(0);
        tagExeWeightP.setReadValLong(0);
        tagExeWeightC.setReadValLong(0);
        tagRcpWeightM.setReadValLong(0);
        tagSetWeightM.setReadValLong(0);
        tagExeWeightM.setReadValLong(0);
        tagSetWeightPM.setReadValLong(0);
        tagTimeCnt.setReadValInt(0);

        tagCmdMixerStart.setReadValBool(false);
        tagCmdMixerLoaded.setReadValBool(false);
    }

    @Override
    protected void updateTags() throws SQLException {
        super.updateTags();
        updateSumTags(dosersP, tagRcpWeightP, tagExeWeightP);
        updateSumTags(dosersM, tagRcpWeightM, tagExeWeightM);
    }

    private void updateSetWeight() {
        updateSetWeight(dosers, tagSetWeight.getLong());
        tagSetWeightP.setReadValLong( dosersP.stream().mapToLong(Doser::getSetWeight).sum() );
        tagSetWeightM.setReadValLong( dosersM.stream().mapToLong(Doser::getSetWeight).sum() );
        tagSetWeightPM.setReadValLong( tagSetWeightP.getLong() + tagSetWeightM.getLong() );
    }


    private void updateSetWeightM() {
        double ratio = tagRcpWeightP.getLong() == 0? 0: tagExeWeightC.getDouble() / tagRcpWeightP.getDouble();
        tagSetWeightM.setReadValLong( Math.round(tagRcpWeightM.getDouble() * ratio) );
        updateSetWeight(dosersM, tagSetWeightM.getLong());
        tagSetWeightPM.setReadValLong( tagSetWeightP.getLong() + tagSetWeightM.getLong() );
    }

//////////////////////////////////////////   task   //////////////////////////////////////////////////
    @Override
    protected boolean installTask(int taskId) throws SQLException {
        boolean res = super.installTask(taskId);
        updateSetWeight();
        return res;
    }



//////////////////////////////////////////   hold   //////////////////////////////////////////////////
    private long countHold(Function<LineCombo,Tag> funcTagHold) {
        return ctx.lines.stream()
                .filter(line -> line instanceof LineCombo)
                .filter(line -> funcTagHold.apply((LineCombo)line).getBool())
                .count();
    }

    private boolean isTooManyHoldDosers() {
        return countHold(line -> line.tagHoldDosers) > 1;
    }

    private boolean isTooManyHoldTransport() {
        return countHold(line -> line.tagHoldTransport) > 1;
    }


    private boolean canHold(int stateHold, Function<LineCombo,Tag> funcTagHold) {
        return ctx.lines.stream()
                .filter(line -> line != this)
                .filter(line -> line instanceof LineCombo)
                .map(line -> (LineCombo) line)
                .noneMatch(line ->
                        funcTagHold.apply(line).getInt() > 0 ||
                                (line.tagState.getInt() == stateHold  &&  line.getTimeHoldCatching() < timeHoldCatching));
    }

    private boolean canHoldDosers() {
        return canHold(STATE_HOLD_DOSERS, line -> line.tagHoldDosers);
    }

    private boolean canHoldTransport() {
        return canHold(STATE_HOLD_TRANSPORT, line -> line.tagHoldTransport);
    }

    private void startHoldCatching() {
        timeHoldCatching = System.currentTimeMillis();
    }


//////////////////////////////////////////   dosers   //////////////////////////////////////////////////
private void executeDosers() throws SQLException {
    for(Doser doser: dosersP)
        doser.execute(tagHoldDosers.getInt() == 0);

    for(Doser doser: dosersM)
        doser.execute();
}

    @Override
    protected void resetExeWeight() {
        super.resetExeWeight();
        tagExeWeightP.setReadValInt(0);
        tagExeWeightM.setReadValInt(0);
        tagExeWeightC.setReadValInt(0);
    }

////////////////////////////////////////////   info   ////////////////////////////////////////////////////

//    @Override
//    public String getInfo() {
//        return super.getInfo() + ctlDoser.getInfo();
//    }


//////////////////////////////////////////   getters   //////////////////////////////////////////////////

    @Override
    protected String getLineType() {
        return "combo";
    }

    public long getTimeHoldCatching() {
        return timeHoldCatching;
    }
}

