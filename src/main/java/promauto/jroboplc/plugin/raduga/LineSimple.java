package promauto.jroboplc.plugin.raduga;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import promauto.jroboplc.core.TagTable;
import promauto.jroboplc.core.api.Configuration;
import promauto.jroboplc.core.api.Tag;
import promauto.jroboplc.core.tags.TagRW;

import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class LineSimple implements Line {
    public static final int DISABLE_EXE_WEIGHT_CNT_VALUE = 0x7fff_ffff;
    private final Logger logger = LoggerFactory.getLogger(LineSimple.class);

    public static final String PARAMETER_IS_NOT_PRESENTED = "Parameter is not presented: ";

    protected static final int STATE_NONE                     = 0;
    protected static final int STATE_IDLE                     = 1;

    protected static final int STATE_PREPARING                = 100;
    protected static final int STATE_SETTING                  = 101;
    protected static final int STATE_CAN_LOAD                 = 102;
    protected static final int STATE_LOADING                  = 103;
    protected static final int STATE_CAN_UNLOAD               = 104;
    protected static final int STATE_UNLOADING                = 105;
    protected static final int STATE_PREPARING_LOOP           = 106;
    protected static final int STATE_SETTING_LOOP             = 107;
    protected static final int STATE_LOADING_LOOP             = 108;
    protected static final int STATE_UNLOADING_LOOP           = 109;
    protected static final int STATE_UNLOADED                 = 110;

    protected static final int STATE_ERROR                    = 88888;
    protected static final int STATE_CYCLE_FINISHED           = 99999;

    protected static final Map<Integer,String> STATES = new HashMap<>();
    static {
        STATES.put(STATE_NONE       , "Задача не установлена");
        STATES.put(STATE_IDLE       , "Ожидание разрешения цикла дозирования");
        STATES.put(STATE_PREPARING  , "Подготовка дозаторов к старту цикла");
        STATES.put(STATE_CAN_LOAD   , "Ожидание внешнего разрешения загрузки");
        STATES.put(STATE_SETTING    , "Установка задания дозаторам");
        STATES.put(STATE_CAN_UNLOAD , "Ожидание внешнего разрешения разгрузки");
        STATES.put(STATE_LOADING    , "Загрузка дозаторов");
        STATES.put(STATE_UNLOADING  , "Разгрузка дозаторов");

        STATES.put(STATE_PREPARING_LOOP  , "Подготовка дозаторов к старту цикла (доп.проход)");
        STATES.put(STATE_SETTING_LOOP    , "Установка задания дозаторам (доп.проход)");
        STATES.put(STATE_LOADING_LOOP    , "Загрузка дозаторов (доп.проход)");
        STATES.put(STATE_UNLOADING_LOOP  , "Разгрузка дозаторов (доп.проход)");

        STATES.put(STATE_UNLOADED   , "Разгрузка дозаторов завершена");

        STATES.put(STATE_ERROR          , "Ошибка");
        STATES.put(STATE_CYCLE_FINISHED , "Цикл дозирования закончен");
//        STATES.put(STATE_TASK_FINISHED  , "Задача выполнена");
    }

    protected static final String ERROR_TASK_NOT_FOUND          = "Задача не найдена";
    protected static final String ERROR_TASK_HAS_WRONG_LINE     = "Задача не предназначена для этой линии";
    protected static final String ERROR_TASK_HAS_WRONG_STATUS   = "Задача не готова к использованию";
    protected static final String ERROR_TASK_HAS_WRONG_BUNKER   = "Задача использует бункер, не соответствующий этой линии";
    protected static final String ERROR_TASK_HAS_WRONG_PARAM    = "Задача использует параметр, не соответствующий этой линии";
    protected static final String ERROR_TASK_HAS_WRONG_DISPENSER = "Задача использует диспенсер, не соответствующий этой линии";

    protected static final String ERROR_DOSER_REJECTED_TASK     = "Дозатор не принимает задачу";



    public final Context ctx;
    protected int id;
    protected String name;
    protected String dupLineName;
    protected boolean autostart;
    protected boolean hidden;

    protected TagRW tagName;
    protected TagRW tagDescr;
    protected TagRW tagTaskId;
    protected Tag   tagTaskInstall;
    protected TagRW tagState;
    protected TagRW tagError;
    protected TagRW tagStateMsg;
    protected Tag   tagStart;
    protected Tag   tagCancel;
    protected Tag   tagReset;
    protected Tag   tagRestart;
    protected TagRW tagCycleId;
    protected TagRW tagRecipeId;
    protected TagRW tagRecipeName;
    protected TagRW tagRcpWeight;
    protected Tag   tagSetWeight;
    protected TagRW tagSetWeightCur;
    protected TagRW tagExeWeight;
//    protected TagRW tagExeWeightTot; // todo deprecated!
    protected TagRW tagExeWeightGrp;
    protected TagRW tagExeWeightShf;
    protected Tag   tagExeWeightCnt; // user's countdown counter
    protected TagRW tagPeriodShf;
    protected Tag   tagEnableLoad;
    protected Tag   tagEnableUnload;
    protected Tag   tagDisable;
    protected TagRW tagDstBunkerNum;
    protected TagRW tagDstBunkerName;
    protected TagRW tagDstProductId;

    protected TagRW tagCmdUnload;

    protected final List<Doser> dosers;
    protected final List<Param> params;
    protected final List<Dispenser> dispensers;
    protected final List<Line.DstBunker> dstBunkers;

    protected List<Line> lineGroup;


    protected LineSimple(Context ctx) {
        this.ctx = ctx;
        dosers = new ArrayList<>();
        params = new ArrayList<>();
        dispensers = new ArrayList<>();
        dstBunkers = new ArrayList<>();
    }



    @Override
    public boolean load(Object conf) {
        boolean res = true;
        Configuration cm = ctx.env.getConfiguration();

        name = cm.get(conf, "name", "");
        String descr = cm.get(conf, "descr", "");
        hidden = cm.get(conf, "hidden", false);
//        lineNum = cm.get(conf, "num", -1);

        dstBunkers.addAll( cm.getStringList(conf, "dstBunkers").stream()
                .map(DstBunker::new)
                .collect(Collectors.toList()) );

        Map<String, Object> confParams = cm.toMap(cm.get(conf, "params"));
        for(String paramName: confParams.keySet()) {
            Param param = new Param(ctx, this, paramName);
            param.load(confParams.get(paramName));
            params.add(param);
        }

        dupLineName = cm.get(conf, "duplicate", "");
        autostart = cm.get(conf, "autostart", false);

        for(Object confDoser: cm.toList(cm.get(conf, "dosers"))) {
            Doser doser = new Doser(ctx, this, dosers.size() + 1);
            res &= doser.load(confDoser);
            dosers.add(doser);
        }

        for(Object confDispenser: cm.toList(cm.get(conf, "dispensers"))) {
            Dispenser dispenser = new Dispenser(ctx, this);
            res &= dispenser.load(confDispenser);
            dispensers.add(dispenser);
        }

        if( !validatePresented("name", name) )
            return false;

        TagTable tt = ctx.tagtable;
        ctx.addRepoTags(
            tagTaskId         = tt.createRWInt(    makeTagName("TaskId")        , 0),
            tagTaskInstall    = tt.createInt(      makeTagName("TaskInstall")   , 0),
            tagState          = tt.createRWInt(    makeTagName("State")         , STATE_NONE),
            tagStateMsg       = tt.createRWString( makeTagName("StateMsg")      , STATES.get(STATE_NONE)),
            tagError          = tt.createRWString( makeTagName("Error")         , ""),
            tagRecipeId       = tt.createRWInt(    makeTagName("RecipeId")      , 0),
            tagRecipeName     = tt.createRWString( makeTagName("RecipeName")    , ""),
            tagCycleId        = tt.createRWInt(    makeTagName("CycleId")       , 0),
            tagSetWeight      = tt.createLong(     makeTagName("SetWeight")     , 0),
            tagDstBunkerNum   = tt.createRWInt(    makeTagName("DstBunkerNum")  , -1),
            tagDstBunkerName  = tt.createRWString( makeTagName("DstBunkerName") , ""),
            tagDstProductId   = tt.createRWInt(    makeTagName("DstProductId")  , 0),
            tagStart          = tt.createBool(     makeTagName("Start")         , false),
            tagPeriodShf      = tt.createRWInt(    makeTagName("PeriodShf")     , 0),
            tagExeWeightShf   = tt.createRWLong(   makeTagName("ExeWeightShf")  , 0),
            tagExeWeightCnt   = tt.createLong(     makeTagName("ExeWeightCnt")  , 0),
            tagExeWeightGrp   = tt.createRWLong(   makeTagName("ExeWeightGrp")  , 0)
        );

        tagName          = tt.createRWString( makeTagName("Name")         ,      name);
        tagDescr         = tt.createRWString( makeTagName("Descr")        ,      descr);
        tagCancel        = tt.createBool(     makeTagName("Cancel")       , false);
        tagRestart       = tt.createBool(     makeTagName("Restart")      , false);
        tagReset         = tt.createBool(     makeTagName("Reset")        , false);

        tagRcpWeight     = tt.createRWLong(   makeTagName("RcpWeight")    , 0);
        tagExeWeight     = tt.createRWLong(   makeTagName("ExeWeight")    , 0);
        tagSetWeightCur  = tt.createRWLong(   makeTagName("SetWeightCur") , 0);

//        tagExeWeightTot = tt.createRWLong(   makeTagName("ExeWeightTot") , 0);

        tagEnableLoad    = tt.createBool(     makeTagName("EnableLoad")   , true);
        tagEnableUnload  = tt.createBool(     makeTagName("EnableUnload") , true);
        tagDisable       = tt.createBool(     makeTagName("Disable")      , false);

        tagCmdUnload     = tt.createRWBool( makeTagName("CmdUnload"), false);

        return res;
    }

    public String makeTagName(String tagname) {
        return name + '_' + tagname;
    }


    protected boolean validatePresented(String paramName, String paramValue) {
        if( !paramValue.isEmpty() )
            return true;

        ctx.env.printError(logger, ctx.moduleName, name, PARAMETER_IS_NOT_PRESENTED + paramName);
        return false;
    }


    @Override
    public boolean prepare() {
        dosers.forEach(Doser::prepare);

        if( !dupLineName.isEmpty() ) {
            Line dupLine = ctx.getLine(dupLineName);
            if( dupLine == null ) {
                ctx.env.printError(logger, ctx.moduleName, name, "Duplicate line is not found");
                return false;
            }

            boolean res = dosers.size() == dupLine.getDosers().size();
            for(int i = 0; res && i < dosers.size(); ++i) {
                Doser doser = dosers.get(i);
                Doser dupDoser = dupLine.getDosers().get(i);
                doser.setDupName(dupDoser.getName());
                res = doser.feeders.size() == dupDoser.feeders.size();
            }

            if( !res ) {
                ctx.env.printError(logger, ctx.moduleName, name, "Duplicate line must have the " +
                        "same number of dosers and feeders");
                return false;
            }

            // add this line to group
            lineGroup = dupLine.getLineGroup();
            if( lineGroup.size() == 0 )
                lineGroup.add(dupLine);
            lineGroup.add(this);
        }

        return true;
    }

    @Override
    public List<Line> getLineGroup() {
        if( lineGroup == null )
            lineGroup = new LinkedList<>();
        return lineGroup;
    }


    @Override
    public boolean init() throws SQLException {
        DataService svc = ctx.service;
        id = svc.syncLine(
                tagName.getString(),
                tagDescr.getString(),
                hidden
        );

        for(Doser doser: dosers)
            doser.init();
        svc.syncLineDosers(id, dosers.stream().map(Doser::getId).collect(Collectors.toList()));

        for(Param param: params)
            param.init();
        svc.syncLineParams(id, params.stream().map(Param::getId).collect(Collectors.toList()));

        for(Dispenser dispenser: dispensers)
            dispenser.init();

        for(Line.DstBunker bunker: dstBunkers)
            bunker.id = svc.syncBunker(bunker.name);

        return true;
    }


    @Override
    public void execute() throws SQLException {
        updateDstBunker();
        executeDosers();
        executeDispensers();
        updateTags();
        updateButtons();
        updateTaskInstall();

        int state = tagState.getInt();

        // state none
        if( state == STATE_NONE  &&  hasTask() ) {
            state = STATE_IDLE;
        }

        // state idle
        if( state == STATE_IDLE ) {
            if( hasTask() ) {
                updateSetWeight();
                if (tagStart.getBool() && tagSetWeight.getLong() > 0) {
                    generateCycleId();
                    beginCycle();
                    state = STATE_PREPARING;
                }
            } else {
                state = STATE_NONE;
            }
        }

        if( state == STATE_PREPARING ) {
            prepareTaskSend(dosers);
            if( isTaskSendReady(dosers) ) {
                sendTask(dosers);
                state = STATE_SETTING;
            }
        }

        if( state == STATE_SETTING) {
            if( isTaskAccepted(dosers) ) {
                state = STATE_CAN_LOAD;
            } else
            if( isTaskRejected(dosers) ) {
                state = STATE_ERROR;
                setError(ERROR_DOSER_REJECTED_TASK);
            } else {
                sendTask(dosers);
            }
        }

        if( state == STATE_CAN_LOAD  &&  canLoad() ) {
            state = STATE_LOADING;
        }

        if( state == STATE_LOADING) {
            if( isUnloadReady(dosers) ) {
                state = STATE_CAN_UNLOAD;
            } else {
                startLoad(dosers);
            }
        }

        if( state == STATE_CAN_UNLOAD  &&  canUnload() ) {
            state = STATE_UNLOADING;
        }

        if( state == STATE_UNLOADING) {
            if( isUnloaded(dosers) ) {
                incrementPassCnt(dosers);
                if( needMorePasses(dosers) ) {
                    state = STATE_PREPARING_LOOP;
                } else {
                    state = STATE_UNLOADED;
                }
            } else {
                if( canUnload() )
                    startUnload(dosers);
            }
        }


        if( state == STATE_PREPARING_LOOP ) {
            prepareTaskSend(dosers);
            if( isTaskSendReady(dosers) ) {
                sendTask(dosers);
                state = STATE_SETTING_LOOP;
            }
        }

        if( state == STATE_SETTING_LOOP) {
            if( isTaskAccepted(dosers) ) {
                state = STATE_LOADING_LOOP;
            } else
            if( isTaskRejected(dosers) ) {
                state = STATE_ERROR;
                setError(ERROR_DOSER_REJECTED_TASK);
            } else {
                sendTask(dosers);
            }
        }

        if( state == STATE_LOADING_LOOP) {
            if( isUnloadReady(dosers) ) {
                state = STATE_UNLOADING_LOOP;
            } else {
                startLoad(dosers);
            }
        }

        if( state == STATE_UNLOADING_LOOP) {
            if( isUnloaded(dosers) ) {
                incrementPassCnt(dosers);
                if( needMorePasses(dosers) ) {
                    state = STATE_PREPARING_LOOP;
                } else {
                    state = STATE_UNLOADED;
                }
            } else {
                if( canUnload() )
                    startUnload(dosers);
            }
        }


        if( state == STATE_UNLOADED ) {
            state = STATE_CYCLE_FINISHED;
        }
        else if( state == STATE_CYCLE_FINISHED) {
            // todo delay before next cycle
            state = STATE_IDLE;
            resetExeWeight();
            if( !autostart )
                tagStart.setOff();
        }

        tagCmdUnload.setReadValBool( state >= STATE_CAN_UNLOAD &&  state <= STATE_UNLOADING_LOOP);
        setState(state);
    }


    protected void updateDstBunker() {
        if( dstBunkers.size() == 0 ) {
            if( tagDstBunkerNum.getInt() != -1 ) {
                tagDstBunkerNum.setReadValInt(-1);
                tagDstBunkerName.setReadValString("");
            }
        } else {
            int num;
            if( tagDstBunkerNum.hasWriteValue() )
                num = tagDstBunkerNum.getWriteValInt();
            else
                num = tagDstBunkerNum.getInt();

            if( num < 0  ||  num >= dstBunkers.size() )
                num = 0;

            if( num != tagDstBunkerNum.getInt() ) {
                tagDstBunkerNum.setReadValInt(num);
                tagDstBunkerName.setReadValString( dstBunkers.get(num).name );
            }
        }
    }

    @Override
    public int getDstBunkerId() {
        int num = tagDstBunkerNum.getInt();
        return num < 0  ||  num >= dstBunkers.size()? 0: dstBunkers.get(num).id;
    }

    protected void updateButtons() {

        // reset
        if(tagReset.getBool()) {
            tagReset.setOff();
            reset();
            setState(STATE_NONE);
        }

        // cancel
        if(tagCancel.getBool()  &&  canCancel() ) {
            tagCancel.setOff();
            reset();
            setState(STATE_NONE);
        }

        // restart
        if(tagRestart.getBool()) {
            tagRestart.setOff();
            if( canRestart() ) {
                setState(STATE_IDLE);
                tagStart.setOff();
            }
        }
    }

    private boolean canRestart() {
        return !isStateError()  &&  hasTask();
    }

    protected boolean canCancel() {
        return tagState.getInt() < STATE_LOADING;
    }

    protected boolean canInstall() {
        return tagState.getInt() <= STATE_IDLE;
    }

    protected boolean isStateError() {
        return tagState.getInt() == STATE_ERROR;
    }

    protected boolean hasTask() {
        return tagTaskId.getInt() > 0;
    }


    protected void reset() {
        tagError.setReadValString("");
        tagTaskId.setReadValInt(0);
        tagCycleId.setReadValInt(0);
        tagRecipeId.setReadValInt( 0 );
        tagRecipeName.setReadValString("");
        tagRcpWeight.setReadValLong(0);
        tagSetWeight.setLong(0);
        tagSetWeightCur.setLong(0);
        tagExeWeight.setReadValLong(0);
        tagExeWeightGrp.setReadValLong(0);

        dosers.forEach(Doser::reset);
        params.forEach(Param::reset);
        dispensers.forEach(Dispenser::reset);
    }



    protected static void updateSumTags(List<Doser> dosers, TagRW tagRcpWeight, TagRW tagExeWeight) {
        long rcp = 0;
        long exe = 0;
        for (Doser doser : dosers) {
            rcp += doser.tagRcpWeight.getLong();
            exe += doser.tagExeWeight.getLong();
        }
        tagRcpWeight.setReadValLong(rcp);
        tagExeWeight.setReadValLong(exe);
    }


    protected void updateTags() throws SQLException {
        updateSumTags(dosers, tagRcpWeight, tagExeWeight);

        tagSetWeightCur.setReadValLong(
                dosers.stream().mapToLong(Doser::getSetWeight).sum()
        );

        updateShift(ctx.periods.calcPeriodShift(LocalDateTime.now()), 0);
    }


    protected static void updateSetWeight(List<Doser> theDosers, long newSetWeight) {
        if( newSetWeight < 0 )
            newSetWeight = 0;

        long oldSetWeight = theDosers.stream().mapToLong(Doser::getSetWeight).sum();
        if( newSetWeight != oldSetWeight ) {

            long rcpweight = theDosers.stream().mapToLong(Doser::getRcpWeight).sum();
            if( rcpweight <= 0) {
                theDosers.forEach(doser -> doser.setSetWeight(0));
                return;
            }

            double ratio = (double)newSetWeight / rcpweight;
            long sum = 0;
            for(Doser doser: theDosers) {
                rcpweight -= doser.tagRcpWeight.getLong();
                long w = (rcpweight == 0)?
                        newSetWeight - sum:
                        Math.round( doser.tagRcpWeight.getDouble() * ratio );
                doser.setSetWeight(w);
                sum += w;
            }
        }
    }

    private void updateSetWeight() {
        updateSetWeight(dosers, tagSetWeight.getLong());
    }



    protected void setState(int state) {
        if( tagState.getInt() == state )
            return;

        tagState.setReadValInt(state);
        tagStateMsg.setReadValString(STATES.getOrDefault(state, state + "?"));
    }

    protected void setError(String errmes) {
        tagError.setReadValString(errmes);
    }


    protected void generateCycleId() throws SQLException {
        tagCycleId.setReadValInt( ctx.service.generateCycleId() );
    }


    protected boolean canLoad() {
        return tagEnableLoad.getBool()  &&  !tagDisable.getBool();
    }

    protected boolean canUnload() {
        return tagEnableUnload.getBool()  &&  !tagDisable.getBool();
    }


    @Override
    public void updateShift(int period, long plusWeight) {
        if(tagPeriodShf.getInt() != period ) {
            tagPeriodShf.setReadValInt( period );
            tagExeWeightShf.setReadValLong( plusWeight );
            updateLineGroup();
        } else {
            if( plusWeight != 0 ) {
                tagExeWeightShf.setReadValLong(tagExeWeightShf.getLong() + plusWeight);
                updateLineGroup();
            }
        }

        if(tagExeWeightCnt.getLong() < DISABLE_EXE_WEIGHT_CNT_VALUE)
            tagExeWeightCnt.setLong(tagExeWeightCnt.getLong() - plusWeight);
    }

    public void updateLineGroup() {
        if( lineGroup != null ) {
            long weight = lineGroup.stream().mapToLong(Line::getExeWeightShf).sum();
            lineGroup.forEach(line -> line.setExeWeightGrp(weight));
        }
    }

    @Override
    public long getExeWeightShf() {
        return tagExeWeightShf.getLong();
    }

    @Override
    public void setExeWeightGrp(long weight) {
        tagExeWeightGrp.setReadValLong( weight );
//        tagExeWeightTot.setReadValLong( weight );
    }

    //////////////////////////////////////////   task   //////////////////////////////////////////////////
    protected void updateTaskInstall() throws SQLException {
        if( tagTaskInstall.getInt() > 0  &&  canInstall() ) {
            if( installTask(tagTaskInstall.getInt()) ) {
                tagTaskId.setReadValInt( tagTaskInstall.getInt() );
            } else {
                tagTaskId.setReadValInt(0);
                if( canCancel() )
                    setState(STATE_NONE);
            }
            tagTaskInstall.setInt(0);
        }
    }


    protected boolean installTask(int taskId) throws SQLException {
        reset();

//        tagTaskId.setReadValInt(taskId);

//        Task task = ctx.taskrepo.get(taskId);
        Task task = ctx.service.getTask(taskId);

        if( !task.presented ) {
            setError(ERROR_TASK_NOT_FOUND);
            return false;
        }

        if( !task.lineName.equals(name)  &&  !task.lineName.equals(dupLineName)) {
            setError(ERROR_TASK_HAS_WRONG_LINE);
            return false;
        }

        if( task.status != Task.STATUS_READY) {
            setError(ERROR_TASK_HAS_WRONG_STATUS);
            return false;
        }

        // dosers
        dosers.forEach(doser -> doser.installTask(task));

        int numberOfFeedersUsed = dosers.stream()
                .mapToInt(Doser::getNumberOfFeedersUsed)
                .sum();

        if( numberOfFeedersUsed < task.products.size() ) {
            setError(ERROR_TASK_HAS_WRONG_BUNKER);
            return false;
        }

        // dispensers
        dispensers.forEach(dispenser -> dispenser.installTask(task));

        long numberOfDispenserUsed = dispensers.stream()
                .filter(Dispenser::isInUse)
                .count();

        if( numberOfDispenserUsed < task.dispensers.size() ) {
            setError(ERROR_TASK_HAS_WRONG_DISPENSER);
            return false;
        }




        tagRecipeId.setReadValInt( task.recipeId );
        tagRecipeName.setReadValString( task.recipeName );
        tagDstProductId.setReadValInt( task.productId );

        updateTags();
        tagSetWeight.setLong( tagRcpWeight.getLong() );

        // params
        for(Task.Param tprm: task.params) {
            Param param = findParam(tprm.paramName);
            if( param == null ) {
                setError(ERROR_TASK_HAS_WRONG_PARAM);
                return false;
            }
            param.setValue(tprm.val);
        }


        tagStart.setOff();

        return true;
    }



//////////////////////////////////////////   params   //////////////////////////////////////////////////

    private Param findParam(String paramName) {
        return params.stream()
                .filter(param -> param.getName().equals(paramName))
                .findAny()
                .orElse(null);
    }


////////////////////////////////////////////   dispensers   //////////////////////////////////////////////////
protected void executeDispensers() throws SQLException {
    for(Dispenser dispenser: dispensers)
        dispenser.execute();
}



//////////////////////////////////////////   dosers   //////////////////////////////////////////////////

    private void executeDosers() throws SQLException {
        for(Doser doser: dosers)
            doser.execute();
    }

    protected void beginCycle() {
        dosers.forEach(Doser::beginCycle);
    }

    protected void resetExeWeight() {
        tagExeWeight.setReadValInt(0);
        dosers.forEach(Doser::resetExeWeight);
    }


    protected static Stream<Doser> dosersInUse(List<Doser> dosers) {
        return dosers.stream().filter(Doser::isInUse);
    }

    protected static void prepareTaskSend(List<Doser> dosers) {
        dosersInUse(dosers).forEach(Doser::prepareTaskSend);
    }

    protected static boolean isTaskSendReady(List<Doser> dosers) {
        return dosersInUse(dosers).allMatch(Doser::isTaskSendReady);
    }

    protected static void sendTask(List<Doser> dosers) {
        dosersInUse(dosers).forEach(Doser::sendTask);
    }

    protected static boolean isTaskAccepted(List<Doser> dosers) {
        return dosersInUse(dosers).allMatch(Doser::isTaskAccepted);
    }

    protected static boolean isTaskRejected(List<Doser> dosers) {
        return dosersInUse(dosers).anyMatch(Doser::isTaskRejected);
    }

    protected static void startLoad(List<Doser> dosers) {
        dosersInUse(dosers).forEach(Doser::startLoad);
    }

    protected static boolean isUnloadReady(List<Doser> dosers) {
        return dosersInUse(dosers).allMatch(Doser::isUnloadReady);
    }

    protected static void startUnload(List<Doser> dosers) {
        dosersInUse(dosers).forEach(Doser::startUnload);
    }

    protected static boolean isUnloaded(List<Doser> dosers) {
        return dosersInUse(dosers).allMatch(Doser::isUnloaded);
    }

    protected static void incrementPassCnt(List<Doser> dosers) {
        dosersInUse(dosers).forEach(Doser::incrementPassCnt);
    }

    protected static boolean needMorePasses(List<Doser> dosers) {
        return dosersInUse(dosers).findAny().isPresent();
    }



////////////////////////////////////////////   info   ////////////////////////////////////////////////////
    @Override
    public String check() {
        String res1 = dosers.stream()
                .map(Doser::check)
                .collect(Collectors.joining());

        return res1;
    }

    @Override
    public String getInfo() {
        return String.format("\r\n\r    %s, %s, id:%d, d:%d, f:%d %s%s",
                getLineType(),
                name + (dupLineName.isEmpty()? "": "/" + dupLineName ),
                id,
                dosers.size(),
                dosers.stream().mapToInt(d -> d.feeders.size()).sum(),
                hidden? ", hidden": "",
                autostart? ", autostart": ""
        );

//        dosers.stream()
//                .map(Doser::getInfo)
//                .collect(Collectors.joining())
    }



//////////////////////////////////////////   getters   //////////////////////////////////////////////////

    @Override
    public String getName() {
        return name;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public int getTaskId() {
        return tagTaskId.getInt();
    }

    @Override
    public int getRecipeId() {
        return tagRecipeId.getInt();
    }

    @Override
    public int getCycleId() {
        return tagCycleId.getInt();
    }

    @Override
    public List<Doser> getDosers() {
        return dosers;
    }

    protected String getLineType() {
        return "simple";
    }

//    @Override
//    public int getShiftId() {
//        return tagShiftId.getInt();
//    }

    @Override
    public int getDstProductId() {
        return tagDstProductId.getInt();
    }


}

