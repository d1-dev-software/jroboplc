package promauto.jroboplc.plugin.raduga;

public class Prodtype {
    public static final int PRIMARY = 0;
    public static final int MICRO   = 1;
    public static final int LIQUID  = 2;
}
