package promauto.jroboplc.plugin.raduga;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import promauto.jroboplc.core.api.ANSI;
import promauto.jroboplc.core.api.Configuration;
import promauto.jroboplc.core.api.EnvironmentInst;
import promauto.jroboplc.core.api.Tag;
import promauto.jroboplc.core.tags.Ref;

import java.sql.SQLException;

public class Param {
    private final Logger logger = LoggerFactory.getLogger(Param.class);

    public final Context ctx;
    public final Line line;

    private int id;
    private String name;
    private String descr;

    private Tag tag;


    public Param(Context ctx, Line line, String name) {
        this.ctx = ctx;
        this.line = line;
        this.name = name;
    }

    public boolean load(Object conf) {
        Configuration cm = ctx.env.getConfiguration();
        descr = cm.get(conf, "descr", "");

        String valtypeStr = cm.get(conf, "valtype", "int");
        Tag.Type valtype;
        try {
            valtype = Tag.Type.valueOf(valtypeStr.toUpperCase());
        } catch (IllegalArgumentException e) {
            ctx.env.printError(logger, ctx.moduleName, line.getName(), name, "Bad param type:", valtypeStr);
            return false;
        }

        tag = ctx.tagtable.createTag(valtype, "prm." + line.makeTagName(name));
//                line.makeTagName("Prm_" + name));

        ctx.addRepoTags(tag);

        return true;
    }


    public void init() throws SQLException {
        id = ctx.service.syncParam(
                name,
                descr,
                tag.getType().toString()
        );
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }


    public void setValue(String val) {
        tag.setString(val);
    }

    public void reset() {
        setValue("");
    }

}
