package promauto.jroboplc.plugin.raduga;

import promauto.jroboplc.core.ConfigurationYaml;
import promauto.jroboplc.core.api.Configuration;
import promauto.jroboplc.core.api.EnvironmentInst;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Periods {
    private final List<Integer> shifts = new ArrayList<>();

    public void load(Object conf) {
        Configuration cm = EnvironmentInst.get().getConfiguration();
        shifts.clear();
        shifts.addAll(
            cm.toList(cm.get(conf, "shifts")).stream()
                    .map(val -> (Integer)val)
                    .collect(Collectors.toList())
        );
    }

    public int calcPeriodShift(LocalDateTime dt) {
        int hhmm = dt.getHour() * 100 + dt.getMinute();

        int i = shifts.size();
        int j = 0;
        for(; j < shifts.size(); ++j) {
            if( hhmm < shifts.get(j) ) {
                break;
            }
            i = j + 1;
        }

        if(j < i)
            dt = dt.minusDays(1);

        return dt.getYear() * 100000 + dt.getMonthValue() * 1000 + dt.getDayOfMonth() * 10 + i;
    }

    public static int calcPeriodHour(LocalDateTime dt) {
        return dt.getYear() * 1000000 + dt.getMonthValue() * 10000 + dt.getDayOfMonth() * 100 + dt.getHour();
    }

}
