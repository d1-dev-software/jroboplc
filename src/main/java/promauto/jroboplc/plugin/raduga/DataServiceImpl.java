package promauto.jroboplc.plugin.raduga;

import parsii.eval.BinaryOperation;
import promauto.jroboplc.core.api.Database;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

public class DataServiceImpl implements DataService {
    private static final String TABLE_LINE          = "RD_LINE";
    private static final String TABLE_LINE_DOSER    = "RD_LINE_DOSER";
//    private static final String TABLE_LINE_BUNKER   = "RD_LINE_BUNKER";
//    private static final String TABLE_LINE_BUNKER_CUR = "RD_LINE_BUNKER_CUR";
    private static final String TABLE_LINE_PARAM    = "RD_LINE_PARAM";
    private static final String TABLE_DOSER         = "RD_DOSER";
    private static final String TABLE_FEEDER        = "RD_FEEDER";
    private static final String TABLE_BUNKER        = "RD_BUNKER";
    private static final String TABLE_PARAM         = "RD_PARAM";
    private static final String TABLE_DISPENSER     = "RD_DISPENSER";
    private static final String TABLE_RECIPE        = "RD_RECIPE";
    private static final String TABLE_TASK          = "RD_TASK";
    private static final String TABLE_TASK_PRODUCT  = "RD_TASK_PRODUCT";
    private static final String TABLE_TASK_PRODUCT_DSP  = "RD_TASK_PRODUCT_DSP";
    private static final String TABLE_TASK_PARAM    = "RD_TASK_PARAM";
    private static final String TABLE_TASK_EXEC     = "RD_TASK_EXEC";
    private static final String TABLE_TASK_EXEC_DSP = "RD_TASK_EXEC_DSP";
    private static final String TABLE_TASK_CTL      = "RD_TASK_CTL";
    private static final String TABLE_PRODUCT       = "RD_PRODUCT";
    private static final String TABLE_PLACE         = "RD_PLACE";
    private static final String TABLE_STATSH        = "RD_STATSH";
    private static final String TABLE_STATHR        = "RD_STATHR";
    private static final String TABLE_USER          = "RD_USER";
    private static final String SEQ_CYCLE_ID        = "SQ_RD_CYCLE_ID";
    private static final long INTERVAL_DELETE_OLD_RECORDS = 1000*60*60; // one hour


    enum SyncType {LINE, DOSER, FEEDER, BUNKER, PARAM, DISPENSER};

    Database db;
    private Statement st;
    private String sql;

    private Map<SyncType,Set<Integer>> sync;

    int shortRecs = 0;
    int shortDays = 0;
    int longRecs = 0;
    int longDays = 0;
    private long lastTimeDeleteOldRecords = 0;

    @Override
    public void setDb(Database db) {
        this.db = db;
    }

    @Override
    public Database getDb() {
        return db;
    }

    @Override
    public Statement createStatement() throws SQLException {
        if( db == null )
            throw new RuntimeException("Database is not set!");

        st = db.getConnection().createStatement();
        return st;
    }

    @Override
    public void commit() throws SQLException {
        if( st != null )
            st.close();
        st = null;
        if( db != null )
            db.commit();
    }

    @Override
    public void rollback()  {
        try {
            if (st != null)
                st.close();
        } catch (SQLException e) {
        }
        st = null;
        if( db != null )
            db.rollback();
    }

    private void delete(String table, String field, int id) throws SQLException {
        sql = String.format(
                "delete from %s where %s=%d",
                table,
                field,
                id
        );
        st.executeUpdate(sql);
    }

    @Override
    public String getLastSql() {
        return sql;
    }

    private int executeAndGetId(String sql) throws SQLException {
        this.sql = sql;
        st.executeUpdate(sql, new String[]{"id"});
        try (ResultSet keys = st.getGeneratedKeys()) {
            if (keys.next())
                return keys.getInt(1);
        }
        return 0;
    }

    @Override
    public void startSync() {
        sync = new HashMap<>();
    }

    private void addSyncId(SyncType syncType, int id) {
        if( sync == null )
            return;

        sync.computeIfAbsent(syncType, k -> new HashSet<>()).add(id);
    }

    @Override
    public void finishSync(boolean deleteOtherStuff) throws SQLException {
        if( deleteOtherStuff ) {
            deleteNotSynced(SyncType.DISPENSER, TABLE_DISPENSER);
            deleteNotSynced(SyncType.PARAM, TABLE_PARAM);
            deleteNotSynced(SyncType.BUNKER, TABLE_BUNKER);
            deleteNotSynced(SyncType.DOSER, TABLE_DOSER);
            deleteNotSynced(SyncType.FEEDER, TABLE_FEEDER);
            deleteNotSynced(SyncType.LINE, TABLE_LINE);
        }
        sync = null;
    }


    private void deleteNotSynced(SyncType syncType, String table) throws SQLException {
        if( sync == null  ||  !sync.containsKey(syncType) )
            return;

        sql = String.format(
                "update %s set deleted=1 where id not in (%s)",
                table,
                sync.get(syncType).stream().map(String::valueOf).collect(Collectors.joining(","))
        );

        st.executeUpdate(sql);
    }


    @Override
    public int syncLine(String name, String descr, boolean hidden) throws SQLException {
        sql = String.format(
                "update or insert into %s (name, descr, hidden, deleted) values ('%s', '%s', %d, 0) matching (name)",
                TABLE_LINE,
                name,
                descr,
                hidden? 1: 0
        );

        int id = executeAndGetId(sql);
        addSyncId(SyncType.LINE, id);
        return id;
    }


    @Override
    public int syncDoser(String name, int prodtype) throws SQLException {
        sql = String.format(
                "update or insert into %s (name, prodtype_id, deleted) values ('%s', %d, 0) matching (name)",
                TABLE_DOSER,
                name,
                prodtype
        );

        int id = executeAndGetId(sql);
        addSyncId(SyncType.DOSER, id);
        return id;
    }

    @Override
    public int syncDoser(String name) throws SQLException {
        return syncDoser(name, Prodtype.PRIMARY);
    }

    @Override
    public int syncBunker(String name) throws SQLException {
        String descr = "";
        String[] ss = name.split("\\|");
        if( ss.length > 1 ) {
            name = ss[0];
            descr = ss[1];
        }
        sql = String.format(
                "update or insert into %s (name, descr, deleted) values ('%s', '%s', 0) matching (name)",
                TABLE_BUNKER,
                name,
                descr
        );
        int id = executeAndGetId(sql);
        addSyncId(SyncType.BUNKER, id);
        return id;
    }


    @Override
    public int syncFeeder(int doserId, int feederNum) throws SQLException {
        sql = String.format(
                "update or insert into %s (doser_id, feeder_num, deleted) values (%d, %d, 0) matching (doser_id, feeder_num)",
                TABLE_FEEDER,
                doserId,
                feederNum
        );
        int id = executeAndGetId(sql);
        addSyncId(SyncType.FEEDER, id);

        return id;
    }


    @Override
    public int syncFeederBunker(int feederId, String bunkerName) throws SQLException {
        int bunkerId = syncBunker(bunkerName);
        sql = String.format(
                "update %s set bunker_id=%d where id=%d",
                TABLE_FEEDER,
                bunkerId,
                feederId
        );
        st.executeUpdate(sql);
        return bunkerId;
    }


    @Override
    public int syncParam(String name, String descr, String valtype) throws SQLException {
        sql = String.format(
                "update or insert into %s (name, descr, valtype, deleted) values ('%s', '%s', '%s', 0) matching (name)",
                TABLE_PARAM,
                name,
                descr,
                valtype
        );

        int id = executeAndGetId(sql);
        addSyncId(SyncType.PARAM, id);
        return id;
    }


    @Override
    public int syncDispenser(int lineId, String name, String descr) throws SQLException {
        sql = String.format(
                "update or insert into %s (line_id, name, descr, deleted) values (%d, '%s', '%s', 0) matching (line_id, name)",
                TABLE_DISPENSER,
                lineId,
                name,
                descr
        );

        int id = executeAndGetId(sql);
        addSyncId(SyncType.DISPENSER, id);
        return id;
    }

    @Override
    public void syncLineDosers(int lineId, List<Integer> doserIds) throws SQLException {
        if( doserIds.isEmpty() ) {
            delete(TABLE_LINE_DOSER, "line_id", lineId);
            return;
        }

        for(Integer doserId: doserIds) {
            sql = String.format(
                    "update or insert into %s (line_id, doser_id) values (%d, %d) matching (line_id, doser_id)",
                    TABLE_LINE_DOSER,
                    lineId,
                    doserId
            );
            st.executeUpdate(sql);
        }

        sql = String.format(
                "delete from %s where line_id=%d and doser_id not in (%s)",
                TABLE_LINE_DOSER,
                lineId,
                doserIds.stream().map(String::valueOf).collect(Collectors.joining(","))
        );
        st.executeUpdate(sql);
    }


    @Override
    public void syncLineParams(int lineId, List<Integer> paramIds) throws SQLException {
        if( paramIds.isEmpty() ) {
            delete(TABLE_LINE_PARAM, "line_id", lineId);
//            sql = String.format(
//                    "delete from %s where line_id=%d",
//                    TABLE_LINE_PARAM,
//                    lineId
//            );
//            st.executeUpdate(sql);
            return;
        }

        for(Integer paramId: paramIds) {
            sql = String.format(
                    "update or insert into %s (line_id, param_id) values (%d, %d) matching (line_id, param_id)",
                    TABLE_LINE_PARAM,
                    lineId,
                    paramId
            );
            st.executeUpdate(sql);
        }

        sql = String.format(
                "delete from %s where line_id=%d and param_id not in (%s)",
                TABLE_LINE_PARAM,
                lineId,
                paramIds.stream().map(String::valueOf).collect(Collectors.joining(","))
        );
        st.executeUpdate(sql);
    }



    @Override
    public Task getTask(int taskId) throws SQLException {
        Task task = new Task();

        // task
        sql = String.format(
                "select l.id, l.name, r.id, r.name, cast(t.set_weight_tot * 1000 as bigint), t.status, t.product_id " +
                        "from %s t " +
                        "join %s l on l.id=t.line_id " +
                        "join %s r on r.id=t.recipe_id " +
                        "where t.id=%d",
                TABLE_TASK,
                TABLE_LINE,
                TABLE_RECIPE,
                taskId
        );

        try(ResultSet rs = st.executeQuery(sql)) {
            if (rs.next()) {
                task.id = taskId;
                task.lineId = rs.getInt(1);
                task.lineName = rs.getString(2);
                task.recipeId = rs.getInt(3);
                task.recipeName = rs.getString(4);
//                task.totalSetWeight = rs.getLong(5);
                task.status = rs.getInt(6);
                task.productId = rs.getInt(7);
                task.presented = true;
            }
        }

        // totalSetWeight
        sql = String.format(
                "select cast(sum(weight) * 1000 as bigint) from %s te where te.task_id=%d",
                TABLE_TASK_EXEC,
                taskId
        );

        try(ResultSet rs = st.executeQuery(sql)) {
            if (rs.next()) {
                task.totalExeWeight = rs.getLong(1);
            }
        }

        // task products
        sql = String.format(
                "select tp.product_id, p.name, d.name, f.feeder_num, cast(tp.weight * 1000 as bigint) " +
                        "from %s tp " +
                        "join %s f on f.bunker_id=tp.bunker_id " +
                        "join %s d on d.id=f.doser_id " +
                        "join %s p on p.id=tp.product_id " +
                        "where tp.task_id=%d",
                TABLE_TASK_PRODUCT,
                TABLE_FEEDER,
                TABLE_DOSER,
                TABLE_PRODUCT,
                taskId
        );

        try(ResultSet rs = st.executeQuery(sql)) {
            while (rs.next()) {
                Task.Product tp = new Task.Product();
                tp.productId = rs.getInt(1);
                tp.productName = rs.getString(2);
                tp.doserName = rs.getString(3);
                tp.feederNum = rs.getInt(4);
                tp.weight = rs.getLong(5);
                task.products.add(tp);
            }
        }

        // task products for dispensers
        sql = String.format(
                "select d.name, tp.product_id, p.name, cast(tp.percentage * 1000 as bigint) " +
                        "from %s tp " +
                        "join %s d on d.id=tp.dispenser_id " +
                        "join %s p on p.id=tp.product_id " +
                        "where tp.task_id=%d",
                TABLE_TASK_PRODUCT_DSP,
                TABLE_DISPENSER,
                TABLE_PRODUCT,
                taskId
        );

        try(ResultSet rs = st.executeQuery(sql)) {
            while (rs.next()) {
                Task.Dispenser td = new Task.Dispenser();
                td.dispenserName = rs.getString(1);
                td.productId = rs.getInt(2);
                td.productName = rs.getString(3);
                td.percentage = rs.getInt(4);
                task.dispensers.add(td);
            }
        }

        // task params
        sql = String.format(
                "select p.name, tp.val " +
                        "from %s tp " +
                        "join %s p on p.id=tp.param_id " +
                        "where tp.task_id=%d",
                TABLE_TASK_PARAM,
                TABLE_PARAM,
                taskId
        );

        try(ResultSet rs = st.executeQuery(sql)) {
            while (rs.next()) {
                Task.Param tp = new Task.Param();
                tp.paramName = rs.getString(1);
                tp.val = rs.getString(2);
                task.params.add(tp);
            }
        }

        return task;
    }


    @Override
    public int generateCycleId() throws SQLException {
        sql = String.format(
                "select next value for %s from rdb$database",
                SEQ_CYCLE_ID
        );

        try(ResultSet rs = st.executeQuery(sql)) {
            if (rs.next()) {
                return rs.getInt(1);
            }
        }
        return 0;
    }


    @Override
    public Optional<Long> getTaskExecLastSumWeightEnd(int feederId) throws SQLException {
        sql = String.format(
                "select first 1 cast(te.sum_weight_end * 1000 as bigint) from %s te where te.feeder_id=%d order by id desc",
                TABLE_TASK_EXEC,
                feederId
        );

        try(ResultSet rs = st.executeQuery(sql)) {
            if (rs.next()) {
                return Optional.of(rs.getLong(1));
            }
        }
        return Optional.empty();
    }


    @Override
    public void saveTaskExec(Task.Exec te) throws SQLException {
        String sdt = db.formatDatetime(te.dt);

        sql = String.format(Locale.ROOT,
                "insert into %s (feeder_id, line_id, task_id, product_id, cycle_id, dt, weight, weight_err, sum_weight_beg, sum_weight_end) " +
                        "values (%d, %d, %s, %s, %s, '%s', %.3f, %.3f, %.3f, %.3f)",
                TABLE_TASK_EXEC,
                te.feederId,
                te.lineId,
                toStringNullable( te.taskId ),
                toStringNullable( te.inputProductId ),
                toStringNullable( te.cycleId ),
                sdt,
                (double)te.weight / 1000,
                (double)te.weightErr / 1000,
                (double)te.sumWeightBeg / 1000,
                (double)te.sumWeightEnd / 1000
        );
        st.executeUpdate(sql);

        updateStatWeight(TABLE_STATSH,
                te.periodShift,
                te.lineId,
                te.recipeId,
                te.inputProductId,
                -te.weight
        );

        updateStatWeight(TABLE_STATSH,
                te.periodShift,
                te.lineId,
                te.recipeId,
                te.outputProductId,
                +te.weight
        );

        updateStatWeight(TABLE_STATHR,
                te.periodHour,
                te.lineId,
                te.recipeId,
                te.inputProductId,
                -te.weight
        );

        updateStatWeight(TABLE_STATHR,
                te.periodHour,
                te.lineId,
                te.recipeId,
                te.outputProductId,
                +te.weight
        );

        updateBunkerWeight(te.inputBunkerId, -te.weight);
        updateBunkerWeight(te.outputBunkerId, te.weight);

        deleteOldRecords();

    }


    @Override
    public void saveTaskExecDsp(Task.ExecDsp te) throws SQLException {
        String sdt = db.formatDatetime(te.dt);

        sql = String.format(Locale.ROOT,
                "insert into %s (dispenser_id, task_id, product_id, dt, weight, sum_weight_beg, sum_weight_end) " +
                        "values (%d, %s, %s, '%s', %.3f, %.3f, %.3f)",
                TABLE_TASK_EXEC_DSP,
                te.dispenserId,
                toStringNullable( te.taskId ),
                toStringNullable( te.inputProductId ),
                sdt,
                (double)te.weight / 1000,
                (double)te.sumWeightBeg / 1000,
                (double)te.sumWeightEnd / 1000
        );
        st.executeUpdate(sql);

        updateStatWeight(TABLE_STATSH,
                te.periodShift,
                te.lineId,
                te.recipeId,
                te.inputProductId,
                -te.weight
        );

        updateStatWeight(TABLE_STATSH,
                te.periodShift,
                te.lineId,
                te.recipeId,
                te.outputProductId,
                +te.weight
        );

        updateStatWeight(TABLE_STATHR,
                te.periodHour,
                te.lineId,
                te.recipeId,
                te.inputProductId,
                -te.weight
        );

        updateStatWeight(TABLE_STATHR,
                te.periodHour,
                te.lineId,
                te.recipeId,
                te.outputProductId,
                +te.weight
        );

        deleteOldRecords();

    }

    private void updateBunkerWeight(int bunkerId, long weight) throws SQLException {
        final String sqlUpdate = "update %s set weight = weight + %.3f where id=%d";
        if( bunkerId > 0) {
            sql = String.format(Locale.ROOT, sqlUpdate,
                    TABLE_BUNKER,
                    (double) weight / 1000,
                    bunkerId
            );
            st.executeUpdate(sql);
        }
    }



    private void updateStatWeight(String table, int period, int lineId, int recipeId, int productId, long weight) throws SQLException {
        final String sqlSelect = "select id, cast(weight * 1000 as bigint) from %s " +
                "where period=%d and line_id=%d and recipe_id=%d and product_id=%d";

        final String sqlInsert = "update or insert into %s (id, period, line_id, recipe_id, product_id, weight) " +
                "values (%s, %d, %d, %d, %s, %.3f)";


        if( lineId > 0  &&  productId > 0  &&  recipeId > 0 ) {
            sql = String.format(sqlSelect,
                    table,
                    period,
                    lineId,
                    recipeId,
                    productId
            );

            int id = 0;
            try (ResultSet rs = st.executeQuery(sql)) {
                if (rs.next()) {
                    id = rs.getInt(1);
                    weight += rs.getLong(2);
                }
            }

//            if( id > 0)
//                delete(table, "id", id);

            sql = String.format(Locale.ROOT, sqlInsert,
                    table,
                    toStringNullable( id ),
                    period,
                    lineId,
                    recipeId,
                    productId,
                    (double) weight / 1000
            );
            st.executeUpdate(sql);
        }
    }


    @Override
    public void saveTaskCtl(int lineId, int taskId, int cycleId, LocalDateTime dt, long weight) throws SQLException {
        sql = String.format(Locale.ROOT,
                "insert into %s (line_id, task_id, cycle_id, dt, weight) values (%d, %d, %d, '%s', %.3f)",
                TABLE_TASK_CTL,
                lineId,
                taskId,
                cycleId,
                db.formatDatetime(dt),
                (double)weight/1000
        );
        st.executeUpdate(sql);

        deleteOldRecords();
    }


    private String toStringNullable(int value) {
        return value == 0? "null": String.valueOf(value);
    }

    @Override
    public void setArcSize(int shortRecs, int shortDays, int longRecs, int longDays) {
        this.shortRecs = shortRecs;
        this.shortDays = shortDays;
        this.longRecs = longRecs;
        this.longDays = longDays;

    }
    private void deleteOldRecords() throws SQLException {
        if( System.currentTimeMillis() - lastTimeDeleteOldRecords < INTERVAL_DELETE_OLD_RECORDS )
            return;
        lastTimeDeleteOldRecords = System.currentTimeMillis();

        Set<Integer> taskIds = new HashSet<>();
        deleteOldRecordsShort(TABLE_TASK_EXEC, taskIds);
        deleteOldRecordsShort(TABLE_TASK_CTL, taskIds);

        if( taskIds.size() > 0 ) {
            String taskIdsStr = taskIds.stream().map(String::valueOf).collect(Collectors.joining(","));

            sql = String.format("update %s set deleted=1 where id in (%s)", TABLE_TASK, taskIdsStr);
            st.executeUpdate(sql);

            String deleteSql = "delete from %s where %s in (%s)";
            sql = String.format(deleteSql, TABLE_TASK_PRODUCT, "task_id", taskIdsStr);
            st.executeUpdate(sql);

            sql = String.format(deleteSql, TABLE_TASK_PARAM, "task_id", taskIdsStr);
            st.executeUpdate(sql);

            sql = String.format(deleteSql, TABLE_TASK_PRODUCT_DSP, "task_id", taskIdsStr);
            st.executeUpdate(sql);
        }

        int period = Periods.calcPeriodHour(LocalDateTime.now().minusDays(longDays));
        sql = String.format("select max(id) from %s where id < ((select max(id) from %s) - %d) and period < %d",
                TABLE_STATHR,
                TABLE_STATHR,
                longRecs,
                period
        );
        int maxId = 0;
        try (ResultSet rs = st.executeQuery(sql)) {
            if (rs.next())
                maxId = rs.getInt(1);
        }
        if( maxId > 0 ) {
            sql = String.format("delete from %s where id <= %d", TABLE_STATHR, maxId);
            st.executeUpdate(sql);
        }
    }

    private void deleteOldRecordsShort(String table, Set<Integer> taskIds) throws SQLException {

        sql = String.format("select max(id) from %s where id < ((select max(id) from %s) - %d) and dt < '%s'",
                table,
                table,
                shortRecs,
                db.formatDatetimeWithOffset( LocalDateTime.now().minusDays(shortDays))
        );
        int maxId = 0;
        try (ResultSet rs = st.executeQuery(sql)) {
            if (rs.next())
                maxId = rs.getInt(1);
        }
        if( maxId == 0 )
            return;

        sql = String.format("select task_id from %s where id <= %d and task_id is not null group by 1",
                table,
                maxId
        );
        try (ResultSet rs = st.executeQuery(sql)) {
            while (rs.next()) {
                taskIds.add(rs.getInt(1));
            }
        }

        sql = String.format("delete from %s where id <= %d", table, maxId);
        st.executeUpdate(sql);
    }


    @Override
    public String getProductName(int productId) throws SQLException {
        sql = String.format(
                "select name from %s where id=%d",
                TABLE_PRODUCT,
                productId
        );

        try(ResultSet rs = st.executeQuery(sql)) {
            if (rs.next())
                return rs.getString(1);
        }
        return "";
    }
}
