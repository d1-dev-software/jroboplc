package promauto.jroboplc.plugin.raduga;

import promauto.jroboplc.core.api.Database;

import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface DataService {
    void setDb(Database db);

    Database getDb();

    Statement createStatement() throws SQLException;

    void commit() throws SQLException;

    void rollback();

    void startSync();
    void finishSync(boolean deleteOtherStuff) throws SQLException;


    int syncLine(String name, String descr, boolean hidden) throws SQLException;
    int syncDoser(String name) throws SQLException;
    int syncDoser(String name, int prodtype) throws SQLException;
    int syncFeeder(int doserId, int feederNum) throws SQLException;
    int syncBunker(String name) throws SQLException;
    int syncParam(String name, String descr, String valtype) throws SQLException;
    int syncDispenser(int lineId, String name, String descr) throws SQLException;

    void syncLineDosers(int lineId, List<Integer> doserIds) throws SQLException;
//    void syncLineBunkers(int lineId, List<String> bunkers) throws SQLException;
    void syncLineParams(int lineId, List<Integer> paramIds) throws SQLException;
    int syncFeederBunker(int feederId, String bunkerName) throws SQLException;


    int generateCycleId() throws SQLException;
    Task getTask(int taskId) throws SQLException;

    void saveTaskExec(Task.Exec te) throws SQLException;
    void saveTaskExecDsp(Task.ExecDsp te) throws SQLException;

    Optional<Long> getTaskExecLastSumWeightEnd(int id) throws SQLException;

    void saveTaskCtl(int lineId, int taskId, int cycleId, LocalDateTime dt, long weight) throws SQLException;

    String getLastSql();


    void setArcSize(int shortRecs, int shortDays, int longRecs, int longDays);

    String getProductName(int productId) throws SQLException;
}
