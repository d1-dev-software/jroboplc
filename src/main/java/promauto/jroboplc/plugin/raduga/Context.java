package promauto.jroboplc.plugin.raduga;

import promauto.jroboplc.core.TagTable;
import promauto.jroboplc.core.api.Environment;
import promauto.jroboplc.core.api.Tag;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Context {
    public Environment env;
    public RadugaModule module;
    public DataService service;
//    public TaskRepo taskrepo;
    public String moduleName;
    public TagTable tagtable;
    public final List<Line> lines;
    public final List<Tag> repoTags;
    public final Periods periods;

    public boolean emulated;
    public String databaseModuleName;
    public Tag tagConnected;
    public boolean deleteOtherStuff;


    public Context() {
        lines = new ArrayList<>();
        repoTags = new ArrayList<>();
        periods = new Periods();
    }

    public void setModule(RadugaModule module) {
        this.module = module;
        this.moduleName = module.getName();
        this.tagtable = module.getTagTable();
    }


    public Line getLine(String lineName) {
        return lines.stream()
                .filter(line -> line.getName().equals(lineName))
                .findFirst().orElse(null);
    }

    public void addRepoTags(Tag... tags) {
        Collections.addAll(repoTags, tags);
    }

    public void setDataService(DataService service) {
        this.service = service;
    }

//    public void setTaskrepo(TaskRepo taskrepo) {
//        this.taskrepo = taskrepo;
//    }

    public void setEnvironment(Environment environment) {
        this.env = environment;
    }

    public LocalDateTime now() {
        if( service.getDb() == null )
            return LocalDateTime.now();
        else
            return service.getDb().getServerDatetime();
    }

    public List<Line> getSiblingLines(String lineName) {

        return null;
    }
}
