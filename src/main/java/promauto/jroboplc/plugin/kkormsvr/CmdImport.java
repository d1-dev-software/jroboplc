package promauto.jroboplc.plugin.kkormsvr;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import promauto.jroboplc.core.AbstractCommand;
import promauto.jroboplc.core.DatabaseUtils;
import promauto.jroboplc.core.api.Console;
import promauto.jroboplc.core.api.Database;
import promauto.jroboplc.core.api.EnvironmentInst;
import promauto.jroboplc.core.api.Module;

public class CmdImport extends AbstractCommand {
	private final Logger logger = LoggerFactory.getLogger(CmdImport.class);


	@Override
	public String getName() {
		return "import";
	}

	@Override
	public String getUsage() {
		return "db";
	}

	@Override
	public String getDescription() {
		return "imports kkormsvr data from database db";
	}

	
	
	
	@Override
	public String execute(Console console, Module module, String args) {
		module.postCommand(this, console, module, args);
		return "";
	}


	@Override
	public void executePosted(Console console, Module module, String args) {
		
		KkormsvrModule w = (KkormsvrModule) module;

		Database dbdst = w.getDatabase();
		if( !dbdst.isConnected() ) {
			console.print( "Database " + dbdst.getName() + " is not connected\n" );
			return;
		}

		Module m = EnvironmentInst.get().getModuleManager().getModule(args);
		if( !(m instanceof Database) ) {
			console.print( "Module " + args + " is not a database\n" );
			return;
		}
		Database dbsrc = (Database)m;
		if( !dbsrc.isConnected() ) {
			console.print( "Database " + dbsrc.getName() + " is not connected\n" );
			return;
		}

		DatabaseUtils.clear(console, dbdst,
				"kk_editlog, kk_rashod, kk_output, kk_execute, kk_taskcontent, kk_task, " +
						"kk_linestor, kk_line, kk_storage, kk_rcpprod, kk_receipt, kk_product, " +
						"kk_userpriv, users, kk_livedataval, kk_livedata");


		DatabaseUtils.copytbl(console, dbsrc, dbdst, "users", "*");
		DatabaseUtils.copytbl(console, dbsrc, dbdst, "kk_userpriv", "*");
		DatabaseUtils.copytbl(console, dbsrc, dbdst, "kk_product", "*");
		DatabaseUtils.copytbl(console, dbsrc, dbdst, "kk_receipt", "*");
		DatabaseUtils.copytbl(console, dbsrc, dbdst, "kk_rcpprod", "*");
		DatabaseUtils.copytbl(console, dbsrc, dbdst, "kk_storage", "*");
		DatabaseUtils.copytbl(console, dbsrc, dbdst, "kk_line", "*");
		DatabaseUtils.copytbl(console, dbsrc, dbdst, "kk_linestor", "*");
		DatabaseUtils.copytbl(console, dbsrc, dbdst, "kk_task", "*");
		DatabaseUtils.copytbl(console, dbsrc, dbdst, "kk_taskcontent", "*");
		DatabaseUtils.copytbl(console, dbsrc, dbdst, "kk_execute", "*");
		DatabaseUtils.copytbl(console, dbsrc, dbdst, "kk_output", "*");
		DatabaseUtils.copytbl(console, dbsrc, dbdst, "kk_rashod", "*");
		DatabaseUtils.copytbl(console, dbsrc, dbdst, "kk_editlog", "*");


		DatabaseUtils.copygen(console, dbdst, "kk_execute",		"gen_kk_execute_id", 	"id");
		DatabaseUtils.copygen(console, dbdst, "kk_line", 			"gen_kk_line_id", 		"id");
		DatabaseUtils.copygen(console, dbdst, "kk_output", 		"gen_kk_output_id",     "id");
		DatabaseUtils.copygen(console, dbdst, "kk_privilegs",		"gen_kk_privilegs_id", 	"idpriv");
		DatabaseUtils.copygen(console, dbdst, "kk_product", 		"gen_kk_product_id", 	"id");
		DatabaseUtils.copygen(console, dbdst, "kk_rashod", 		"gen_kk_rashod_id", 	"id");
		DatabaseUtils.copygen(console, dbdst, "kk_rcpprod", 		"gen_kk_rcpprod_id", 	"id");
		DatabaseUtils.copygen(console, dbdst, "kk_receipt", 		"gen_kk_receipt_id", 	"id");
		DatabaseUtils.copygen(console, dbdst, "kk_storage", 		"gen_kk_storage_id",	"id");
		DatabaseUtils.copygen(console, dbdst, "kk_taskcontent",	"gen_kk_taskcontent_id","id");
		DatabaseUtils.copygen(console, dbdst, "kk_task", 			"gen_kk_task_id", 		"id");
		DatabaseUtils.copygen(console, dbdst, "users", 			"gen_users_id", 		"iduser");


		console.print("Done\n");
	}


}
