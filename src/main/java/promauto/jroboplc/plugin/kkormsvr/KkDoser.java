package promauto.jroboplc.plugin.kkormsvr;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import promauto.jroboplc.core.TagTable;
import promauto.jroboplc.core.api.ANSI;
import promauto.jroboplc.core.api.Configuration;
import promauto.jroboplc.core.api.EnvironmentInst;
import promauto.jroboplc.core.api.Tag;
import promauto.jroboplc.core.tags.RefGroup;
import promauto.jroboplc.core.tags.RefItem;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;
import java.util.stream.Collectors;

public class KkDoser {
    private final Logger logger = LoggerFactory.getLogger(KkDoser.class);

    // standard state codes (bit mask)
    private final static int STATE_IDLE             = 1;
    private final static int STATE_WAITING_FOR_TASK = 2;
    private final static int STATE_LOADING          = 4;
    private final static int STATE_UNLOADING        = 8;
    private final static int STATE_ALARM            = 16;

    private final static int ERROR_NONE          = 0;
    private final static int ERROR_NOLINK        = 1;
    private final static int ERROR_DISCONNECT    = 2;
    private final static int ERROR_CRC           = 3;
    private final static int ERROR_TASKDIFFER    = 4;


    public final KkormsvrModule module;
    public final KkLine line;
    private KkGroup group = null;

    private String name;
    private String bindModuleName;
    int grouplevel;
    private List<KkStorage> stors = new ArrayList<>();
    private KkStorage[] storsByAddr;


    private RefGroup refgr = EnvironmentInst.get().getRefFactory().createRefGroup(); //new RefGroup();
    private RefItem refCurStor;
    private RefItem refCurWeight;
    private RefItem refState;
    private RefItem refErrorFlag;
    private RefItem refSendTask;
    private RefItem refCmdStartCycle;
    private RefItem refCmdStartTask;
    private RefItem refSumWeightMax;

    private int error;

    private Tag tagError;
    private Tag tagCurStorNum;
    private Tag tagCurStorName;
    private Tag tagCurProduct;
    private Tag tagReqWeightZero;

    private boolean hasTask;
    private boolean connected;


    KkDoser(KkLine line) {
        this.line = line;
        this.module = line.module;

        error = ERROR_NONE;
        hasTask = false;
        connected = false;
    }


    boolean load(Object conf) {
        Configuration cm = EnvironmentInst.get().getConfiguration();
        name = cm.get(conf, "name", "");
        bindModuleName = cm.get(conf, "bind", name);
        grouplevel = cm.get(conf, "grouplevel", 0);

        try {
            Map<Integer, Object> storsconf = cm.toGenericMap(cm.get(conf, "storages"));
            TreeSet<Integer> addrs = new TreeSet<>(storsconf.keySet());

            for(Integer addr: addrs) {
                KkStorage stor = new KkStorage(this);
                if( !stor.load(addr, storsconf.get(addr) ) )
                    return false;

                stor.createRefs(refgr, bindModuleName);
                stors.add( stor );
            }
        } catch (ClassCastException e) {
            EnvironmentInst.get().printError(logger, module.getName(), name,
                    "Storage address must be a number");
            return false;
        }

        int maxStorAddr = stors.stream().mapToInt(stor -> stor.getAddr()).max().orElse(0);
        storsByAddr = new KkStorage[maxStorAddr+1];
        stors.forEach(stor -> storsByAddr[stor.getAddr()] = stor);

        refCurStor       = refgr.createItem(   bindModuleName, "CurStor");
        refCurWeight     = refgr.createItem(   bindModuleName, "CurWeight");
        refState         = refgr.createItem(   bindModuleName, "State");
        refErrorFlag     = refgr.createItemCrc(bindModuleName, "SYSTEM.ErrorFlag");
        refSendTask      = refgr.createItem(   bindModuleName, "SendTask");
        refCmdStartCycle = refgr.createItem(   bindModuleName, "CmdStartCycle");
        refCmdStartTask  = refgr.createItem(   bindModuleName, "CmdStartTask");
        refSumWeightMax  = refgr.createItem(   bindModuleName, "SumWeightMax");

        refgr.createItemCrcSum(bindModuleName, "Crc32");

        TagTable tt = module.getTagTable();
        tagError       = tt.createInt(   prefix() + "Error"      , 0);
        tagCurStorNum  = tt.createInt(   prefix() + "CurStorNum" , 0);
        tagCurStorName = tt.createString(prefix() + "CurStorName", "");
        tagCurProduct  = tt.createString(prefix() + "CurProduct" , "");
        tagReqWeightZero  = tt.createBool(prefix() + "ReqWeightZero" , false);

        return true;
    }


    String prefix() {
//        return line.prefix() + name + ".";
        return name + ".";
    }

    // --- live data ---
    void initLiveData() {
        stors.forEach(KkStorage::initLiveData);
    }

    void readLiveData() {
        stors.forEach(KkStorage::readLiveData);
    }

    void updateLiveData() {
        stors.forEach(KkStorage::updateLiveData);
    }


    void prepare() {
        refgr.prepare();
    }


    void init() throws SQLException {
        for (KkStorage stor : stors)
            stor.init();

        checkHasTask();
    }


    // main loop
    void execute() throws SQLException {
        error = ERROR_NONE;

        connected = refgr.link();
        if( !connected )
            error = ERROR_NOLINK;

        if( connected ) {
            refgr.read();
            connected = refErrorFlag.getValue().getInt() == 0;
            if (!connected) {
                error = ERROR_DISCONNECT;
            }
        }

        if( connected ) {
            if (refgr.checkCrc32()) {
                for (KkStorage stor : stors)
                    stor.execute();
            } else
                error = ERROR_CRC;

            if (isState(STATE_LOADING))
                if (isReqWeightDiffer())
                    error = ERROR_TASKDIFFER;
        }

        updateTags();
    }


    private void updateTags() {
        tagError.setInt(error);
        if( connected ) {
            KkStorage stor = null;
            int curstor = refCurStor.getValue().getInt();

            if( curstor >= 0  &&  curstor < storsByAddr.length )
                stor = storsByAddr[curstor];

            if( stor == null ) {
                tagCurStorNum.setInt(0);
                tagCurStorName.setString("");
                tagCurProduct.setString("");
            } else {
                tagCurStorNum.setInt(stor.getNum());
                tagCurStorName.setString(stor.getName());
                tagCurProduct.setString(stor.getProduct());
            }
        }

        tagReqWeightZero.setBool( stors.stream().allMatch(stor -> stor.reqWeight <= 0) );
    }


    // --- actions ---
    void prepareTaskSend() {
        if( connected  &&  !isState(STATE_ALARM) ) {
            refCmdStartCycle.getTag().setInt(1);
            refSendTask.getTag().setInt(0);
        }
    }


    void sendTask() {
        if( connected ) {
            refSendTask.getTag().setInt(1);
        }
    }


    void startDosing() {
        if( connected ) {
            refCmdStartTask.getTag().setInt(1);
        }
    }


    // --- checks ---
    private boolean isState(int state) {
        if( connected )
            return (refState.getValue().getInt() & state) > 0;
        else
            return false;
    }


    private boolean isReqWeightDiffer() {
        return stors.stream().anyMatch(stor -> stor.reqWeight != stor.refReqWeight.getValue().getLong());
    }


    boolean isTaskSendReady() {
        return connected  &&  refSendTask.getValue().getInt() == 0  &&  isState(STATE_WAITING_FOR_TASK);

    }


    boolean isTaskAccepted() {
        return connected  &&  refSendTask.getValue().getInt() > 0;
    }


    boolean isTaskRejected() {
        return connected  &&  refSendTask.getValue().getInt() < 0;
    }


    boolean isDosingFinished() {
        return connected  &&  isState(STATE_IDLE);
    }


    // --- task stuff ---
    private void checkHasTask() {
        hasTask = stors.stream().anyMatch(stor -> stor.reqWeight > 0);
    }


    void applyTask(KkTask task) {
        stors.forEach(stor -> stor.applyTask(task));

        checkHasTask();
    }


    // --- getters and setters ---
    public String getName() {
        return name;
    }

    boolean isHasTask() {
        return hasTask;
    }


    long getSumWeightMax() {
        return refSumWeightMax.getValue().getLong();
    }


    KkGroup getGroup() {
        return group;
    }


    void setGroup(KkGroup group) {
        assert this.group==null;
        this.group = group;
    }



    // --- check any problem ---
    String getProblems() {
        String res = refgr.check();
        if( error == ERROR_CRC )
            res += (res.isEmpty()? "": "\r\n") + "Crc32 error!";
        if( !res.isEmpty() )
            res =  "Doser \"" + bindModuleName + "\":\r\n" + res;
        return res;
    }


    public String getInfo() {
        return "    " + name + ": " + (bindModuleName.equals(name)? "": "bind=" + bindModuleName + " ")
                + getStateStr() + " " + ANSI.redBold(getErrorStr()) + "\r\n"
                + stors.stream()
                    .map(KkStorage::getInfo)
                    .collect(Collectors.joining("\r\n"));
    }

    private String getStateStr() {
        return
              (isState(STATE_IDLE)?       "IDLE "      : "")
            + (isState(STATE_WAITING_FOR_TASK)? "TASK READY ": "")
            + (isState(STATE_LOADING)?    "LOADING "   : "")
            + (isState(STATE_UNLOADING)?  "UNLOADING "   : "")
            + (isState(STATE_ALARM)?      "ALARM "   : "");
    }

    private String getErrorStr() {
        switch (error) {
            case ERROR_NOLINK:     return "NOLINK";
            case ERROR_DISCONNECT: return "DISCONNECT";
            case ERROR_CRC:        return "CRC";
            case ERROR_TASKDIFFER: return "TASKDIFFER";
        }
        return "";
    }

}

