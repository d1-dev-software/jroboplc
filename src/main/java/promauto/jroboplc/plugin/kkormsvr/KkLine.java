package promauto.jroboplc.plugin.kkormsvr;

import promauto.jroboplc.core.TagTable;
import promauto.jroboplc.core.api.Configuration;
import promauto.jroboplc.core.api.EnvironmentInst;
import promauto.jroboplc.core.api.Tag;
import promauto.jroboplc.core.tags.TagRW;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

public class KkLine {
    private final static int TASK_STATUS_EXECUTED = 1;
    private final static int TASK_STATUS_ERROR = 2;
    private final static int TASK_STATUS_FAKE_TASK = 4;

    private final static int STATE_IDLE = 0;
    private final static int STATE_RUN = 1;
    private final static int STATE_SUSPEND = 2;
    private final static int STATE_ERROR_TASK_NOT_APPLIED = 3;
    private final static int STATE_ERROR_TASK_REJECTED = 4;

    private static final String ZERO_RECEIPT_NAME = "ДОЗИРОВАНИЕ БЕЗ ЗАДАЧИ (РУЧНОЕ)";
    private static final String ZERO_RECEIPT_DESCR = "Пустой рецепт. Создается автоматически. " +
            "Используется для учета отвешанного продукта при отсутствии активной задачи на линии.)";

    private static final String ZERO_PRODUCT_NAME = "НЕИЗВЕСТНЫЙ ПРОДУКТ";
    private static final String ZERO_PRODUCT_DESCR = "Неизвестный продукт. Создается автоматически.";

    public final KkormsvrModule module;
    private int id;
    private int linenum;
    private String name;

    private List<KkDoser> dosers = new ArrayList<>();
    private List<KkGroup> groups = new ArrayList<>();

    // --- live data ---
    private int taskId;
    private int receiptId;
    private Tag tagCycleReq;
    private int cycleCnt;
    private int state;
    // --- live data ---

    private String prmTaskId;
    private String prmReceiptId;
    private String prmCycleReq;
    private String prmCycleCnt;
    private String prmState;

    private Tag tagTaskId;
//    private Tag tagReceiptId;
    private Tag tagCycleCnt;
    private Tag tagState;

    private Tag tagReset;
    private Tag tagSuspend;

    private TagRW tagReceiptName;


    public KkLine(KkormsvrModule module) {
        this.module = module;

        taskId = 0;
        cycleCnt = 0;
        state = STATE_IDLE;
    }



    public boolean load(Object conf) {
        Configuration cm = EnvironmentInst.get().getConfiguration();
        linenum = cm.get(conf, "linenum", 0);
        name = cm.get(conf, "name", "Line" + linenum);

        Map<Integer, KkGroup> grmap = new TreeMap<>();

        for(Object doserconf: cm.toList(cm.get(conf, "dosers"))) {
            KkDoser doser = new KkDoser(this);
            dosers.add(doser);
            if( !doser.load(doserconf) )
                return false;

            KkGroup gr = grmap.get(doser.grouplevel);
            if( gr == null )
                gr = new KkGroup(this, doser.grouplevel);
            gr.addDoser(doser);
            doser.setGroup(gr);
            grmap.put(doser.grouplevel, gr);
        }

        groups = new ArrayList<>(grmap.values());
        groups.forEach(KkGroup::load);

        TagTable tt    = module.getTagTable();
        tagTaskId      = tt.createInt(prefix() + "TaskId", 0);
//        tagReceiptId   = tt.createInt(prefix() + "ReceiptId", 0);
        tagReceiptName = tt.createRWString(prefix() + "Receipt", "");
        tagCycleCnt    = tt.createInt(prefix() + "CycleCnt", 0);
        tagCycleReq    = tt.createInt(prefix() + "CycleReq", 0);
        tagState = tt.createInt(prefix() + "State", 0);
        tagReset       = tt.createBool(prefix() + "Reset", false);
        tagSuspend     = tt.createBool(prefix() + "Suspend", false);


        initLiveData();

        return true;
    }

    public String prefix() {
        return "Line" + linenum + ".";
    }


    private void initLiveData() {
        prmTaskId       = prefix() + "TaskId";
        prmReceiptId    = prefix() + "ReceiptId";
        prmCycleCnt     = prefix() + "CycleCnt";
        prmCycleReq     = prefix() + "CycleReq";
        prmState        = prefix() + "State";
        groups.forEach(KkGroup::initLiveData);
        dosers.forEach(KkDoser::initLiveData);
    }


    void loadLiveData() {
        taskId      = module.livedata.getInt(prmTaskId,    taskId);
        receiptId   = module.livedata.getInt(prmReceiptId, taskId);
        cycleCnt    = module.livedata.getInt(prmCycleCnt,  cycleCnt);
        state = module.livedata.getInt(prmState, state);
        tagCycleReq.setInt( module.livedata.getInt(prmCycleReq, tagCycleReq.getInt()) );
        groups.forEach(KkGroup::readLiveData);
        dosers.forEach(KkDoser::readLiveData);
    }


    private void updateLiveData() {
        module.livedata.set(prmTaskId,    taskId);
        module.livedata.set(prmReceiptId, receiptId);
        module.livedata.set(prmCycleCnt,  cycleCnt);
        module.livedata.set(prmCycleReq,  tagCycleReq.getInt());
        module.livedata.set(prmState, state);
        groups.forEach(KkGroup::updateLiveData);
        dosers.forEach(KkDoser::updateLiveData);
    }


    public void prepare() {
        dosers.forEach(KkDoser::prepare);
    }


    public void init() throws SQLException {
        Statement st = module.getStatement();

        String sql = String.format(
                "select id, name from kk_line where linenum=%d",
                linenum);

        try(ResultSet rs = st.executeQuery(sql)) {
            if (rs.next()) {
                id = rs.getInt("id");
                if( !rs.getString("name").equals(name)) {
                    sql = String.format(
                            "update kk_line set name='%s' where id=%d",
                            name,
                            id);
                    st.executeUpdate(sql);
                }
            } else {
                sql = String.format(
                        "insert into kk_line (linenum, name) values (%d, '%s')",
                        linenum,
                        name);
                id = module.getDatabase().insertReturningId(st, sql);
            }
        }


        // fetch receipt name
        if( receiptId > 0 ) {
            sql = String.format(
                    "select name from kk_receipt where id=%d",
                    receiptId);

            try(ResultSet rs = st.executeQuery(sql)) {
                if (rs.next()) {
                    tagReceiptName.setReadValString(rs.getString(1));
                }
            }
        }


        for (KkDoser doser : dosers) {
            doser.init();
        }

        updateLineStatus();
    }


    // main loop
    public void execute() throws SQLException {

        if( isResetRequested() ) {
            state = STATE_IDLE;
            reset(true);
        }

        for( KkDoser doser: dosers)
            doser.execute();

        for( KkGroup group: groups)
            group.execute();


        if( state == STATE_IDLE) {
            KkTask task = findTask();
            if( task != null )
                if( applyTask(task) ) {
                    state = STATE_RUN;
                } else {
                    state = STATE_ERROR_TASK_NOT_APPLIED;
                }
        }


        if( state == STATE_SUSPEND) {
            if( !tagSuspend.getBool() )
                state = STATE_RUN;
        }


        if( state == STATE_RUN) {
            if( tagSuspend.getBool() )
                state = STATE_SUSPEND;

            cycleCnt = getMinGroupCycleCnt();
            if( isAllGroupsIdle()  &&  cycleCnt >= tagCycleReq.getInt() ) {
                state = STATE_IDLE;
                reset(false);
            } else {
                if( isGroupsError() )
                    state = STATE_ERROR_TASK_REJECTED;
            }
        }



        if( taskId != tagTaskId.getInt()  ||  cycleCnt != tagCycleCnt.getInt()  ||  state != tagState.getInt() )
            updateLineStatus();

        updateTags();

        updateLiveData();
    }


    private void updateTags() {
        tagTaskId.setInt(taskId);
        tagCycleCnt.setInt(cycleCnt);
        tagState.setInt(state);
    }


    private boolean isAllGroupsIdle() {
        return groups.stream().allMatch(KkGroup::isIdle);
    }


    private boolean isGroupsError() {
        return groups.stream().anyMatch(KkGroup::isError);
    }


    private int getMinGroupCycleCnt() {
        return groups.stream().mapToInt(KkGroup::getCycleCnt).min().orElse(0);
    }


    // apply task
    private boolean applyTask(KkTask task) throws SQLException {
        taskId = task.taskId;
        receiptId = task.receiptId;
        tagReceiptName.setReadValString( task.receiptName );
        tagCycleReq.setInt(task.cycleReq);
        cycleCnt = 0;

        groups.forEach(KkGroup::reset);

        for( KkDoser doser: dosers)
            doser.applyTask(task);

        boolean res = task.applied();

        updateTaskStatus( res? TASK_STATUS_EXECUTED: TASK_STATUS_ERROR );

        return res;
    }


    // reset
    private void reset(boolean resetReceptCounters) {
        taskId = 0;
        groups.forEach(KkGroup::reset);

        if( resetReceptCounters ) {
            receiptId = 0;
            tagReceiptName.setReadValString("");
            cycleCnt = 0;
            tagCycleReq.setInt(0);
        }
    }


    // check reset
    private boolean isResetRequested() throws SQLException {
        boolean res = tagReset.getBool();

        if( !res ) {
            String sql = String.format(
                    "select resetflag from kk_line where id=%d",
                    id);

            try (ResultSet rs = module.getStatement().executeQuery(sql)) {
                if (rs.next())
                    res = rs.getInt(1) > 0;
            }
        }

        if( res ) {
            tagReset.setBool(false);

            String sql = String.format(
                    "update kk_line set  resetflag=0 where id=%d",
                    id);

            module.getStatement().executeUpdate(sql);

            return true;
        }

        return false;
    }


    // update task status
    private void updateTaskStatus(int taskStatus) throws SQLException {
        String dt = LocalDateTime.now().format(module.getFormatter());
        String sql = String.format(
                "update kk_task set status=%d, dtexec='%s' where id=%d",
                taskStatus,
                dt,
                taskId);

        module.getStatement().executeUpdate(sql);
    }


    // update line status
    private void updateLineStatus() throws SQLException {
        int status = 0;
        switch (state) {
            case STATE_IDLE:
                status = 0; break;
            case STATE_RUN:
            case STATE_SUSPEND:
                status = 2; break;
            case STATE_ERROR_TASK_NOT_APPLIED:
            case STATE_ERROR_TASK_REJECTED:
                status = 1; break;
        }
        String sql = String.format(
                "update kk_line set status=%d, cur_task_id=%d, cur_cyclenum=%d where id=%d",
                status,
                taskId,
                cycleCnt,
                id);

        module.getStatement().executeUpdate(sql);
    }



    // save weight result to kk_execute, kk_output and kk_rashod
    void saveWeight(int storageId, int productId, int aCycleCnt, long weight, long weightBeg, long weightEnd) throws SQLException {

        if( receiptId > 0  &&  !isExistReceiptId() )
            receiptId = 0;

        if( receiptId == 0 )
            createZeroReceipt();

        if( taskId == 0  ||  !isExistTaskId() ) {
            createZeroReceipt();
            createFakeTask();
        }

        if( receiptId == 0 )
            productId = getCurrentProductId(storageId);

        if( productId > 0  &&  !isExistProductId(productId) )
            productId = 0;

        if( productId == 0 )
            createZeroProduct();


        int executeId = findOrCreateExecuteId(aCycleCnt);

        insertIntoRashod(storageId, productId, executeId, weight, weightBeg, weightEnd);

        updateOrCreateOutput(executeId);
    }



    // exist taskId
    private boolean isExistTaskId() throws SQLException {
        String sql = String.format(
                "select 1 from kk_task where id=%d",
                taskId);

        try (ResultSet rs = module.getStatement().executeQuery(sql)) {
            if (rs.next())
                return true;
        }
        return false;
    }


    // exist receiptId
    private boolean isExistReceiptId() throws SQLException {
        String sql = String.format(
                "select 1 from kk_receipt where id=%d",
                receiptId);

        try (ResultSet rs = module.getStatement().executeQuery(sql)) {
            if (rs.next())
                return true;
        }
        return false;
    }


    // exist productId
    private boolean isExistProductId(int productId) throws SQLException {
        String sql = String.format(
                "select 1 from kk_product where id=%d",
                productId);

        try (ResultSet rs = module.getStatement().executeQuery(sql)) {
            if (rs.next())
                return true;
        }
        return false;
    }


    // zero receipt
    private void createZeroReceipt() throws SQLException {
        receiptId = 0;
        tagReceiptName.setReadValString( ZERO_RECEIPT_NAME );

        if( !isExistReceiptId() ) {
            String sql = "insert into kk_receipt (id, name, descr) values " +
                    "(0, '" + ZERO_RECEIPT_NAME + "', '" + ZERO_RECEIPT_DESCR + "')";

            module.getStatement().executeUpdate(sql);
        }
    }

    // zero product
    private void createZeroProduct() throws SQLException {
        if( !isExistProductId(0) ) {
            String sql = "insert into kk_product (id, name, descr) values " +
                    "(0, '" + ZERO_PRODUCT_NAME + "', '" + ZERO_PRODUCT_DESCR + "')";

            module.getStatement().executeUpdate(sql);
        }
    }


    // fake task
    private void createFakeTask() throws SQLException {
        String dt = LocalDateTime.now().format(module.getFormatter());
        String sql = String.format(
                "insert into kk_task (line_id, receipt_id, dt, dtexec, cycleqnt, status, errcode, enabled ) " +
                        "values (%d, %d, '%s', '%s', %d, %d, %d, %d)",
                id,
                receiptId,
                dt,
                dt,
                0,
                TASK_STATUS_FAKE_TASK,
                0,
                1
        );

        taskId = module.getDatabase().insertReturningId(module.getStatement(), sql);
    }


    // productId from kk_storage
    private int getCurrentProductId(int storageId) throws SQLException {
        String sql = String.format(
                "select product_id from kk_storage where id=%d",
                storageId);

        try (ResultSet rs = module.getStatement().executeQuery(sql)) {
            if (rs.next())
                return rs.getInt(1);
        }
        return 0;
    }


    // kk_execute
    private int findOrCreateExecuteId(int aCycleCnt) throws SQLException {
        String sql = String.format(
                "select id from kk_execute where task_id=%d and cyclenum=%d",
                taskId,
                aCycleCnt+1);

        int executeId = 0;
        try (ResultSet rs = module.getStatement().executeQuery(sql)) {
            if (rs.next())
                executeId = rs.getInt(1);
        }

        if( executeId == 0 ) {
            String dt = LocalDateTime.now().format(module.getFormatter());
            sql = String.format(
                    "insert into kk_execute (task_id, dt, cyclenum, status, mixtime) " +
                            "values (%d, '%s', %d, %d, %d)",
                    taskId,
                    dt,
                    aCycleCnt+1,
                    0,
                    0
            );

            executeId = module.getDatabase().insertReturningId(module.getStatement(), sql);

        }

        return executeId;
    }


    // kk_output
    private void updateOrCreateOutput(int executeId) throws SQLException {
        Statement st = module.getStatement();

        // get existing outputId
        String sql = String.format(
                "select id from kk_output where execute_id=%d",
                executeId);

        int outputId = 0;
        try (ResultSet rs = st.executeQuery(sql)) {
            if (rs.next())
                outputId = rs.getInt(1);
        }

        // get sum kg from rashod
        sql = String.format(
                "select sum(kg) from kk_rashod where execute_id=%d",
                executeId);

        double sumKg = 0;
        try (ResultSet rs = st.executeQuery(sql)) {
            if (rs.next())
                sumKg = rs.getDouble(1);
        }

        // insert new output
        String dt = LocalDateTime.now().format(module.getFormatter());
        if( outputId == 0 ) {
            sql = String.format(Locale.ROOT,
                    "insert into kk_output (execute_id, receipt_id, dt, kg) " +
                            "values (%d, %d, '%s', %f)",
                    executeId,
                    receiptId,
                    dt,
                    sumKg
            );
            st.executeUpdate(sql);
        } else {

            // or update existing output
            sql = String.format(
                    "delete from kk_output where id=%d",
                    outputId
            );
            st.executeUpdate(sql);

            sql = String.format(Locale.ROOT,
                    "insert into kk_output (id, execute_id, receipt_id, dt, kg) " +
                            "values (%d, %d, %d, '%s', %f)",
                    outputId,
                    executeId,
                    receiptId,
                    dt,
                    sumKg
            );
            st.executeUpdate(sql);
        }
    }


    // kk_rashod
    private void insertIntoRashod(int storageId, int productId, int executeId, long weight, long weightBeg, long weightEnd) throws SQLException {
        String dt = LocalDateTime.now().format(module.getFormatter());
        String sql = String.format(Locale.ROOT,
                "insert into kk_rashod (storage_id, product_id, execute_id, dt, kg, w1, w2) " +
                        "values (%d, %d, %d, '%s', %f, %d, %d)",
                storageId,
                productId,
                executeId,
                dt,
                (double)weight/1000.0,
                weightBeg,
                weightEnd
        );
        module.getStatement().executeUpdate(sql);
    }


    // find new task
    private KkTask findTask() throws SQLException {
        Statement st = module.getStatement();

        // find new task id
        String sql = String.format(
                "select id from kk_task where line_id=%d and dtexec is null and enabled=1 " +
                        "order by dt",
                id);

        int newTaskId = 0;
        try (ResultSet rs = st.executeQuery(sql)) {
            if (rs.next())
                newTaskId = rs.getInt(1);
        }

        if( newTaskId == 0 )
            return null;


        // if found fetch details
        sql = String.format(
                "select t.id, t.receipt_id, t.cycleqnt, tc.product_id, tc.storage_id, " +
                        "tc.kg, r.name rname, p.name pname " +
                        "from kk_task t " +
                        "join kk_taskcontent tc on tc.task_id=t.id " +
                        "join kk_receipt r on r.id=t.receipt_id " +
                        "join kk_product p on p.id=tc.product_id " +
                        "where t.id=%d",
                newTaskId);

        KkTask task = null;
        try (ResultSet rs = st.executeQuery(sql)) {
            while (rs.next()) {
                if( task == null ) {
                    task = new KkTask(
                            rs.getInt("id"),
                            rs.getInt("receipt_id"),
                            rs.getString("rname"),
                            rs.getInt("cycleqnt"));
                }

                task.addDetail(
                        rs.getString("pname"),
                        rs.getInt("product_id"),
                        rs.getInt("storage_id"),
                        Math.round(rs.getDouble("kg") * 1000));
            }
        }

        return task;
    }



    // --- getters ---
    public int getId() {
        return id;
    }

    int getCycleReq() {
        return tagCycleReq.getInt();
    }

    int getCycleCnt() {
        return cycleCnt;
    }

    public boolean isRunning() {
        return state == STATE_RUN;
    }


    // check any problems
    public String getProblems() {
        String res =  dosers.stream()
                .map(KkDoser::getProblems)
                .filter(check -> !check.isEmpty())
                .collect(Collectors.joining("\r\n \r\n"));

        if( !res.isEmpty() )
            res = "Line \"" + name + "\":\r\n" + res;

        return res;
    }


    public String getInfo() {
        return "line" + linenum + ": " + name + ", " + getStateStr() + " task=" + taskId
                + " req=" + tagCycleReq + " cnt=" + cycleCnt + " [" + tagReceiptName + "]\r\n"
                + groups.stream()
                    .map(KkGroup::getInfo)
                    .collect(Collectors.joining("\r\n"));

    }

    private String getStateStr() {
        switch (state) {
            case STATE_IDLE:                   return "IDLE";
            case STATE_RUN:                    return "RUN";
            case STATE_SUSPEND:                return "SUSPEND";
            case STATE_ERROR_TASK_NOT_APPLIED: return "ERROR TASK NOT APPLIED";
            case STATE_ERROR_TASK_REJECTED:    return "ERROR TASK REJECTED";
        }
        return "";
    }


}
