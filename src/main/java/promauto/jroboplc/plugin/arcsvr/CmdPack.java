package promauto.jroboplc.plugin.arcsvr;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import promauto.jroboplc.core.AbstractCommand;
import promauto.jroboplc.core.api.Console;
import promauto.jroboplc.core.api.Database;
import promauto.jroboplc.core.api.EnvironmentInst;
import promauto.jroboplc.core.api.Module;
import promauto.jroboplc.plugin.wessvr.ArchiveConfig;
import promauto.jroboplc.plugin.wessvr.WessvrModule;

import java.sql.Statement;

public class CmdPack extends AbstractCommand {
	private final Logger logger = LoggerFactory.getLogger(CmdPack.class);


	@Override
	public String getName() {
		return "pack";
	}

	@Override
	public String getUsage() {
		return "[arcname]";
	}

	@Override
	public String getDescription() {
		return "deletes null-columns in one (if name is specified) or all archive tables";
	}

	
	
	
	@Override
	public String execute(Console console, Module module, String args) {
		module.postCommand(this, console, module, args);
		return "";
	}


	@Override
	public void executePosted(Console console, Module module, String args) {

        console.print( ((ArcsvrModule) module).pack(args) +  "\nOK\n");
    }



}
