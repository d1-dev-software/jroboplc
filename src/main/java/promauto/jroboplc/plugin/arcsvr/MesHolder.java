package promauto.jroboplc.plugin.arcsvr;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import promauto.jroboplc.core.api.*;
import promauto.utils.IniFile;
import promauto.utils.ParamsMap;

import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;
import java.util.stream.Collectors;

public class MesHolder {
    public static final String STR_SERVER_ARCH = "Сервер архивации";
    public static final int CLB_START = 0xff8000;
    public static final int CLF_START = 0xffffff;
    public static final int CLB_STOP  = 0x8c4600;
    public static final int CLF_STOP  = 0xffffff;
    public static final int CLB_ERROR = 0;
    public static final int CLF_ERROR = 0xff;

    private final Logger logger = LoggerFactory.getLogger(MesHolder.class);
    private final static Charset charset = Charset.forName("windows-1251");

    protected final ArcsvrModule module;
    protected final Environment env;

    protected Map<Integer,MesBase> mess = new TreeMap<>();
    private boolean enable;

    private Map<String, Integer> places = new HashMap<>();

    private boolean version2;
    private PreparedStatement pstInsert = null;
    private PreparedStatement pstUpdateDtend = null;
    private PreparedStatement pstUpdateDtack = null;


    public MesHolder(ArcsvrModule module) {
        this.module = module;
        this.env = EnvironmentInst.get();
    }

    public boolean isEnable() {
        return enable;
    }

    public boolean isVersion2() {
        return version2;
    }

    public PreparedStatement getPstInsert() {
        return pstInsert;
    }

    public PreparedStatement getPstUpdateDtend() {
        return pstUpdateDtend;
    }

    public PreparedStatement getPstUpdateDtack() {
        return pstUpdateDtack;
    }

    public boolean load(Object conf) {
        Configuration cm = env.getConfiguration();

        version2 = cm.get(conf, "version", 1) == 2;


        boolean res = true;

        mess.clear();

        // robo
        for(Object o: cm.getStringList( conf, "arcmes.robo") ) {
            res &= loadFile(o.toString());
        }


        // plain
        for(Object conf_plain: cm.toList(cm.get(conf, "arcmes.plain")) ) {
            int id = cm.get(conf_plain, "id", 0);
            if( mess.containsKey(id) ) {
                env.printError(logger, module.getName(), "id="+id, "Duplicate name");
                return false;
            }

            MesPlain mesplain = new MesPlain(this, id);
            res &= mesplain.load(conf_plain);
            mess.put(id, mesplain);
        }


        // regex
        for(Object conf_regex: cm.toList(cm.get(conf, "arcmes.regex")) ) {
            int id = cm.get(conf_regex, "id", 0);
            if( mess.containsKey(id) ) {
                env.printError(logger, module.getName(), "id="+id, "Duplicate name");
                return false;
            }

            MesRegex arcval = new MesRegex(this, id);
            res &= arcval.load(conf_regex);
            mess.put(id, arcval);
        }




        enable = mess.size() > 0;
        return res;
    }


    private boolean loadFile(String pathplace) {

        String ss[] = pathplace.split(";");
        Path path = Paths.get(ss[0].trim());
        path = path.isAbsolute()? path: env.getConfiguration().getConfDir().resolve(path);
        String place = ss.length > 1? ss[1].trim(): module.getPlaceName();

        try {

            IniFile ini = new IniFile(path);

            String tagpath = ini.getString("MesLogMain", "OpcAccessPath", "");
            int mescount = ini.getInt("MesLogMain", "MesCount", 0);

            for(int mesnum=0; mesnum<mescount; ++mesnum) {
                String section = "MesLogArc" + mesnum;
                int id = ini.getInt(section, "MesId", 0);
                int alarm = ini.getInt(section, "Alarm", 0);
                if( alarm == 0)
                    continue;

                MesBase mesbase = mess.get(id);
                if( mesbase != null  &&  !(mesbase instanceof  MesRobo) ) {
                    env.printError(logger, module.getName(), "id=" + id, "Duplicate name");
                    return false;
                }
                MesRobo mes = (MesRobo)mesbase;
                if( mes == null ) {
                    mes = new MesRobo(this, id);
                    mess.put(id, mes);
                }

                mes.load(ini, section, tagpath, place);
            }

        } catch(Exception e) {
            env.printError(logger, e, "File loading error:", path.toString());
            return false;
        }

        return true;
    }


    public void link() {
        if( !enable )
            return;

        for(MesBase mes: mess.values())
            mes.link();
    }


    public boolean init() {
        if( !enable )
            return true;

        ParamsMap params = new ParamsMap("schema=" + module.getSchema());
        if (!module.getDatabase().executeScript("arcsvr_arcmes_init1", params).success ||
                !module.getDatabase().executeScript("arcsvr_arcmes_init2", params).success ||
                !module.getDatabase().executeScript("arcsvr_arcmes_update1", params).success
        )
            return false;

        isDbReadyForVersion2();

        boolean res = true;

        initPreparedStatements();

        for(MesBase arc: mess.values())
            if( !arc.init() )
                res = false;

        removeOldMeslistRecords();

        if( res )
            saveMessage(STR_SERVER_ARCH, "СТАРТ", module.getName(), CLB_START, CLF_START);
        else
            saveMessage(STR_SERVER_ARCH, "Ошибка запуска", module.getName(), CLB_ERROR, CLF_ERROR);

        return res;
    }

    private void isDbReadyForVersion2() {
        if( !version2 )
            return;

        boolean res = false;
        Database db = module.getDatabase();
        try(Statement st = db.getConnection().createStatement()) {
            res = db.hasTable(st, module.getSchema(), "MESMONITOR");
            db.commit();
        } catch (Exception e) {
            env.printError(logger, e, module.getName());
            db.rollback();
        }

        if( !res ) {
            version2 = false;
            env.printError(logger, module.getName(), "Version 2 mode has been disabled. You must recreate database " +
                    db.getName() + "!");
        }
    }


    private void removeOldMeslistRecords() {
        String idmsgs = mess.keySet().stream().map( Object::toString ).collect( Collectors.joining(", "));

        Database db = module.getDatabase();
        try(Statement st = db.getConnection().createStatement()) {
            String sql = String.format("delete from %s where (idmsg not in (%s)) and (idmsg < 100000)",
                    module.makeTableName("meslist"),
                    idmsgs);

            st.executeUpdate(sql);
            db.commit();
        } catch (Exception e) {
            env.printError(logger, e, module.getName());
            db.rollback();
        }
    }


    public boolean execute() throws SQLException {
        if( !enable )
            return true;

        boolean res = true;

        for(MesBase arc: mess.values())
            if( !arc.execute() )
                res = false;

        return res;
    }


    public void closedown() {
        if( !enable )
            return;

        closePreparedStatements();

        saveMessage(STR_SERVER_ARCH, "СТОП", module.getName(), CLB_STOP, CLF_STOP);
    }


    private void saveMessage(String text1, String text2, String text3, int clb, int clf) {
        if( !version2 )
            return;

        Database db = module.getDatabase();
        if( db == null  ||  db.getConnection() == null )
            return;

        try(Statement st = db.getConnection().createStatement()) {
            String sql = String.format("select * from %s('%s', '%s', '%s', %d, %d, %d, %d)",
                    module.makeTableName("save_message"),
                    text1, text2, text3, 0, 0, clb, clf);
            ResultSet rs = st.executeQuery(sql);
            rs.next();

            db.commit();
        } catch (Exception e) {
            env.printError(logger, e, module.getName());
            db.rollback();
        }
    }


    public String getInfo() {
        String res = "";

        if( version2 )
            res += ANSI.GREEN + "\r\narcmes version 2" + ANSI.RESET;

        for(MesBase arc: mess.values())
            res += arc.getInfo();

        return res;
    }


    public String showNolinks() {
        String res = "";
        String s;
        for(MesBase mes: mess.values())
            if( !(s = mes.getNolinks()).isEmpty() )
                res += ANSI.RED + ANSI.BOLD + mes.idmsg + ": " + mes.mestext  + ":\r\n" + ANSI.RESET + s;

        return res;
    }


    public void addPlace(String placeName, int placeId) {
        places.put(placeName, placeId);
    }

    public int getPlace(String placeName) {
        Integer placeId = places.get(placeName);
        return placeId == null? 0: placeId;
    }


    protected void initPreparedStatements() {
        closePreparedStatements();

        String tblMessages = module.makeTableName("messages");
        try {
            pstInsert = module.getDatabase().getConnection().prepareStatement( String.format(
                    "insert into %s (dt, idmsg, idtag, idplace, act, clb, clf, data) values (?, ?, ?, ?, ?, ?, ?, ?)",
                    tblMessages
            ));

            if(version2) {
                pstUpdateDtend = module.getDatabase().getConnection().prepareStatement(String.format(
                        "update %s set dtend=? where idmsg=? and idtag=? and dtend is null",
                        tblMessages
                ));
                pstUpdateDtack = module.getDatabase().getConnection().prepareStatement(String.format(
                        "update %s set dtack=? where idmsg=? and idtag=? and dtack is null",
                        tblMessages
                ));
            }
            module.getDatabase().commit();
        } catch (SQLException e) {
            env.logError(logger, e, module.getName());
            module.getDatabase().rollback();
        }
    }


    protected void closePreparedStatements() {
        closePreparedStatement(pstInsert);
        pstInsert = null;

        if(version2) {
            closePreparedStatement(pstUpdateDtend);
            closePreparedStatement(pstUpdateDtack);
            pstUpdateDtend = null;
            pstUpdateDtack = null;
        }
    }

    private void closePreparedStatement(PreparedStatement pst) {
        if (pst != null) {
            try {
                pst.close();
            } catch (SQLException e) {
                env.printError(logger, e);
            }
        }
    }

}
