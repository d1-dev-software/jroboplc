package promauto.jroboplc.plugin.arcsvr;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import promauto.jroboplc.core.api.Module;
import promauto.jroboplc.core.api.Tag;
import promauto.utils.IniFile;

import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MesRobo extends MesPlain {
    private final Logger logger = LoggerFactory.getLogger(MesRobo.class);


    public MesRobo(MesHolder holder, int idmsg) {
        super(holder, idmsg);
    }


    public void load(IniFile ini, String section, String tagpath, String placename) throws Exception {

        idmsg = ini.getInt(section, "MesId", 0);
        mestext = ini.getString(section, "Name", "");
        alarm = ini.getInt(section, "Alarm", alarm);
        clb_inp = ini.getInt(section, "Clb_inp", clb_inp);
        clf_inp = ini.getInt(section, "Clf_inp", clf_inp);
        clb_inp = ini.getInt(section, "Clb_inp", clb_inp);
        clf_inp = ini.getInt(section, "Clf_inp", clf_inp);
        clb_inp = ini.getInt(section, "Clb_inp", clb_inp);
        clf_inp = ini.getInt(section, "Clf_inp", clf_inp);

        noaffirm = (alarm & ALARM_NO_AFFIRM) > 0;

        eventtype = ini.getInt(section, "EventType", eventtype);
        eventval  = ini.getInt(section, "EventVal", eventval);
        initEventFunc();

//        Pattern p1 = Pattern.compile("\\s*([^;]+)\\s*;\\s*([^;]*)\\s*;\\s*(\\d*)");
        Pattern p1 = Pattern.compile("\\s*([^;]+)\\s*;\\s*([^;]*)");
        Pattern p2 = Pattern.compile("\\{place=(.*)\\}");
        Matcher m;

        int tagcount = ini.getInt(section, "TagCount", 0);
        for(int i=0; i<tagcount; ++i) {
            String s = ini.getString(section, "Tag"+i, "");
            if( !(m = p1.matcher(s)).find() )
                throw new Exception("Tag rec error: " + section + ", " + i + " = " + s);

            String tagname;
            if( tagpath == null  ||  tagpath.trim().isEmpty() )
                tagname = m.group(1);
            else
                tagname = tagpath + '.' + m.group(1);

            MesRec rec = new MesRec();
            recs.put(tagname, rec);

            rec.descr = m.group(2);
            if( (m = p2.matcher(rec.descr)).find() ) {
                rec.placename = m.group(1);
                rec.descr = m.replaceFirst("");
            } else
                rec.placename = placename;

        }
    }



}
