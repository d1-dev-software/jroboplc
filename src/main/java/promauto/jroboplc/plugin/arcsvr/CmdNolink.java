package promauto.jroboplc.plugin.arcsvr;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import promauto.jroboplc.core.AbstractCommand;
import promauto.jroboplc.core.api.Console;
import promauto.jroboplc.core.api.Database;
import promauto.jroboplc.core.api.EnvironmentInst;
import promauto.jroboplc.core.api.Module;

import java.sql.Statement;

public class CmdNolink extends AbstractCommand {
	private final Logger logger = LoggerFactory.getLogger(CmdNolink.class);


	@Override
	public String getName() {
		return "nolink";
	}

	@Override
	public String getUsage() {
		return "";
	}

	@Override
	public String getDescription() {
		return "show not linked tags";
	}

	
	
	
	@Override
	public String execute(Console console, Module module, String args) {
        ArcsvrModule m = (ArcsvrModule) module;

		return m.showNolinks();
	}





}
