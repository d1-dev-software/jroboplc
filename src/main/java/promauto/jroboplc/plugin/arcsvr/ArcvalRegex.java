package promauto.jroboplc.plugin.arcsvr;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import promauto.jroboplc.core.PatternList;
import promauto.jroboplc.core.api.*;
import promauto.jroboplc.core.tags.TagPlain;

import java.sql.*;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.*;
import java.util.function.Function;
import java.util.regex.PatternSyntaxException;
import java.util.stream.Collectors;


public class ArcvalRegex extends ArcvalBase {
    private final Logger logger = LoggerFactory.getLogger(ArcvalRegex.class);

    enum Mode {PERIOD, CHANGE}


//    private final static class Filter {
//        Pattern pattern;
//        boolean exclude = false;
//    }


    private static class Rec {
        Module mod = null;
        Tag tag = null;
        int idtag;
        Tag value = null;
        boolean phantom = false;
        boolean tobeRemoved = false;


        Rec(int idtag) {
            this.idtag = idtag;
            phantom = true;
        }

        Rec() {}
    }

    private int force_ms;
    private long lastforce_ms;

    private Mode mode;

    private PatternList filters = new PatternList();
    private Map<String,Rec> recs = new TreeMap<>();

    private boolean needInit;
    private boolean phantomsOnly;

    private Function<Rec,Tag> getTagByModeFunc;


    ArcvalRegex(ArcvalHolder holder, String arcname) {
        super(holder, arcname);
    }


    public boolean load(Object conf) {
        Configuration cm = env.getConfiguration();

        period_ms = cm.get(conf, "period_ms", 1000);
        force_ms = cm.get(conf, "force_s", 60) * 1000;
        arcsize = cm.get(conf, "size", 14*24*60*60);
        descr = cm.get(conf, "descr", "");
        period_align = cm.get(conf, "period_align", false);
        String stype = cm.get(conf, "type", "INT32");
        String speriod = cm.get(conf, "mode", "period");

        try {
            ftype = FieldType.valueOf( stype.toUpperCase() );
        } catch (IllegalArgumentException e) {
            env.printError(logger, e, module.getName(), arcname, "Bad type = " + stype);
            return false;
        }

        try {
            mode = Mode.valueOf( speriod.toUpperCase() );
        } catch (IllegalArgumentException e) {
            env.printError(logger, e, module.getName(), arcname, "Bad period = " + speriod);
            return false;
        }


        try {
            filters.load(conf, "tags");
        } catch(PatternSyntaxException e) {
            env.printError(logger, e, module.getName(), arcname, "Tags loading error");
            return false;
        }


        getTagByModeFunc = (mode == Mode.PERIOD)?
                (rec) -> rec.tag:
                (rec) -> rec.value;



        return true;
    }


    @Override
    public void link() {
        recs.values().forEach(rec -> rec.tobeRemoved = true);

//        Set<Module> srcset = new HashSet<>(module.getTagSrcModules());

        for(Module mod: env.getModuleManager().getModules() ) {
//            boolean issrcmod = srcset.contains(mod);

            for (Tag tag : mod.getTagTable().values()) {
                if( tag.getType() == Tag.Type.STRING )
                    continue;

//                String tagname = issrcmod?
                String tagname = tag.hasFlags(Flags.EXTERNAL)?
                        tag.getName() :
                        mod.getName() + '.' + tag.getName();


                if ( filters.match(tagname) ) {
                    Rec rec = recs.get(tagname);
                    if (rec == null) {
                        rec = new Rec();
                        recs.put(tagname, rec);
                        needInit = true;
                    }
                    needInit |= rec.phantom;

                    rec.mod = mod;
                    rec.tag = tag;
                    rec.phantom = false;
                    rec.tobeRemoved = false;
                }
            }
        }

        needInit |= recs.values().removeIf(rec -> !rec.phantom  &&  rec.tobeRemoved);

        phantomsOnly = recs.values().stream().allMatch(rec -> rec.phantom);
    }




    @Override
    public boolean init() {
        super.init();

        long curms = System.currentTimeMillis();
        if( period_align ) {
            long curdayms = LocalTime.now().toNanoOfDay() / 1000000;
            lastpass_ms = curms - (curdayms % period_ms);
            lastforce_ms = curms - (curdayms % force_ms);
        } else {
            lastforce_ms = curms;
        }

        boolean res = false;

        Database db = module.getDatabase();
        try(Statement st = db.getConnection().createStatement()) {

            initArc(st);
            initTags(st);
            db.commit();


            List<Integer> idtags = recs.values().stream()
                    .map(rec -> rec.idtag)
                    .collect(Collectors.toList());

            initArctable(st, idtags);
            db.commit();

            initPst(idtags);
            initCurrec(st);
            db.commit();

            initValues(st);
            db.commit();

            res = true;
        } catch (Exception e) {
            env.printError(logger, e, module.getName(), arcname, "Init error", lastSql);
            db.rollback();
        }

        needInit = false;
        return res;
    }



    private void initTags(Statement st) throws SQLException {

        Map<String, DescrTagid> tmp = getTaglistMap(st);

        for(Map.Entry<String,Rec> ent: recs.entrySet()) {
            String tagname = ent.getKey();
            Rec rec = ent.getValue();
            DescrTagid dti = tmp.get(tagname);
            if( dti == null)
                rec.idtag = createTaglistRec(st, tagname, tagname);
            else {
                rec.idtag = dti.tagid;
                tmp.remove(tagname);
            }
        }

        for(String tagname: tmp.keySet())
            if( filters.match(tagname) )
                recs.put(tagname, new Rec(tmp.get(tagname).tagid) );
            else
                deleteTaglistRec(st, tmp.get(tagname).tagid, tagname);
    }


    private void initValues(Statement st) throws SQLException {
        if( mode == Mode.CHANGE ) {

            for(Rec rec: recs.values())
                if( rec.phantom )
                    rec.value = null;
                else {
                    if (rec.value == null)
                        rec.value = TagPlain.create(rec.tag);
                    else
                        rec.tag.copyValueTo(rec.value);
                }


            String sql = String.format("select first 1 * from %s order by rec desc",
                    module.makeTableName(arcname));
            lastSql = sql;

            try( ResultSet rs = st.executeQuery(sql) ) {
                if (rs.next()) {
                    Map<Integer,Integer> tmp = new HashMap<>();
                    ResultSetMetaData md = rs.getMetaData();
                    int n = md.getColumnCount();
                    for( int i=1; i<=n; ++i) {
                        String field = md.getColumnName(i);
                        if (field.startsWith("T"))
                            tmp.put( Integer.parseInt(field.substring(1)), i );
                    }

                    for(Rec rec: recs.values()) {
                        if( rec.value != null) {
                            Integer idx = tmp.get(rec.idtag);
                            if (idx != null) {
                                    if( rec.value.getType() == Tag.Type.DOUBLE )
                                        rec.value.setDouble( rs.getDouble(idx) );
                                    else
                                        rec.value.setLong( rs.getLong(idx) );

                                    if( rs.wasNull() )
                                        rec.value.setBool( !rec.tag.getBool() );
                            }
                        }
                    }
                } else {
                    for(Rec rec: recs.values())
                        if( !rec.phantom )
                            rec.value.setBool( !rec.tag.getBool() );

                }
            }
        }
    }



    @Override
    protected void setPstInsertData() throws SQLException {
        int i = 0;
        for (Rec rec : recs.values()) {
            Tag tag = getTagByModeFunc.apply(rec);
            if (tag == null)
                pstInsert.setNull(++i, ftypeSql);
            else
                setPstInsertFunc.apply(++i, tag);
        }

        pstInsert.setLong(++i, currec);
        pstInsert.setTimestamp(++i, Timestamp.valueOf(LocalDateTime.now()));
    }


    @Override
    public boolean execute() throws SQLException {
        if( needInit )
            if( !init() )
                closePreparedStatements();


        if(phantomsOnly )
            return true;


        long curms = System.currentTimeMillis();

        if( !hasExecTimeCome(curms) )
            return true;

        if( mode == Mode.CHANGE ) {
            boolean hasChanges = false;
            for (Rec rec : recs.values()) {
                if( !rec.phantom  &&  !rec.tag.equalsValue(rec.value) ) {
                    hasChanges = true;
                    rec.tag.copyValueTo(rec.value);
                }
            }

            if( force_ms > 0 ) {
                long ms = curms - lastforce_ms;
                if( ms >= force_ms ) {
                    lastforce_ms = lastforce_ms + force_ms * (int)(ms/force_ms);
                    hasChanges = true;
                }
            }

            if( !hasChanges )
                return true;
        }


        return super.execute();

    }



    @Override
    public String pack() {
        long count = recs.values().stream().filter(rec -> rec.phantom).count();
        if( count == 0)
            return "";

        Database db = module.getDatabase();
        try(Statement st = db.getConnection().createStatement()) {

            for(Map.Entry<String,Rec> ent: recs.entrySet())
                if( ent.getValue().phantom )
                    deleteTaglistRec(st, ent.getValue().idtag, ent.getKey());

            recs.values().removeIf(rec -> rec.phantom);

            needInit = true;
        } catch (Exception e) {
            env.printError(logger, e, module.getName(), arcname, "Pack error");
            db.rollback();
        }


        return arcname + ": " + count + "\r\n";
    }


    @Override
    public String getInfo() {
        long nolink_cnt = recs.values().stream().filter(rec -> rec.phantom).count();
        return String.format("\r\n%s: %s per=%d%s size=%d cur=%d tags=%d%s",
                arcname,
                mode,
                period_ms,
                mode==Mode.PERIOD? "": '/' + force_ms/1000,
                arcsize,
                currec,
                recs.size(),
                (nolink_cnt == 0? "": "/"+ ANSI.RED + nolink_cnt + " nolink" + ANSI.RESET)
        );
    }

    @Override
    public String getNolinks() {
        StringBuilder sb = new StringBuilder();

        recs.entrySet().stream().filter(rec -> rec.getValue().phantom).forEach(rec ->
                sb.append("  ").append(rec.getKey()).append("\r\n"));

        return sb.toString();
    }


}
