package promauto.jroboplc.plugin.arcsvr;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import promauto.jroboplc.core.api.*;
import promauto.utils.IniFile;

import java.sql.*;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class ArcvalRobo extends  ArcvalBase {
    private final Logger logger = LoggerFactory.getLogger(ArcvalRobo.class);


    private static class Rec {
        String tagpath;
        String tagname;
        String descr;
        Tag tag;
        int idtag;
        boolean hasError;

        public String getPathTagname() {
            if( tagpath.isEmpty() )
                return tagname;
            else
                return tagpath + '.' + tagname;
        }
    }


    private Map<String,Rec> recs = new TreeMap<>();



    public ArcvalRobo(ArcvalHolder arcvalHolder, String arcname) {
        super(arcvalHolder, arcname);

        ftype = FieldType.INT16;
    }

    public void load(IniFile ini, String section, String tagpath) throws Exception {
        descr = ini.getString(section, "Description", "");
        period_ms = ini.getInt(section, "Period", 2000);
        arcsize = ini.getInt(section, "Size", 604800);
        int tagcount = ini.getInt(section, "TagCount", 0);
        String stype = ini.getString(section, "Type", "INT16");

        try {
            ftype = FieldType.valueOf( stype.toUpperCase() );
        } catch (IllegalArgumentException e) {
            env.printError(logger, e, module.getName(), arcname, "Bad type = " + stype);
        }

        Pattern p = Pattern.compile("([^.]*)\\.(.*)");
        for(int i=0; i<tagcount; ++i) {
            String s = ini.getString(section, "Tag"+i, "");
            String[] ss = s.split(";");

            Rec rec = new Rec();

            if( tagpath.isEmpty() ) {
                Matcher m = p.matcher( ss[0] );
                if( !m.find() )
                    throw new Exception("Tag rec error: " + section + ", " + i + " = " + s);
                rec.tagpath = m.group(1).trim();
                rec.tagname = m.group(2).trim();
            } else {
                rec.tagpath = tagpath;
                rec.tagname = ss[0].trim();
            }

            rec.descr = ss.length>1? ss[1]: "";
            rec.tag = null;

            recs.put(rec.getPathTagname(), rec);
        }
    }

    @Override
    public void link() {
        for(Module mod: env.getModuleManager().getModules() ) {

            for (Tag tag : mod.getTagTable().values()) {
                if( tag.getType() == Tag.Type.STRING   ||  tag.getStatus() == Tag.Status.Deleted )
                    continue;

                String tagname = tag.hasFlags(Flags.EXTERNAL)?
                        tag.getName() :
                        mod.getName() + '.' + tag.getName();

                Rec rec = recs.get(tagname);

                if (rec != null) {
                    rec.tag = tag;
                }
            }
        }

    }



    @Override
    public boolean init() {
        super.init();

        period_align = period_ms >= 60000;
        if( period_align ) {
            long curms = System.currentTimeMillis();
            long curdayms = LocalTime.now().toNanoOfDay() / 1000000;
            lastpass_ms = curms - (curdayms % period_ms);
        }

        boolean res = false;

        Database db = module.getDatabase();
        try(Statement st = db.getConnection().createStatement()) {

            initArc(st);
            initTags(st);
            db.commit();


            List<Integer> idtags = recs.values().stream()
                    .map(rec -> rec.idtag)
                    .collect(Collectors.toList());

            initArctable(st, idtags);
            db.commit();

            initPst(idtags);
            initCurrec(st);
            db.commit();

            res = true;
        } catch (Exception e) {
            env.printError(logger, e, module.getName(), arcname, "Init error", lastSql);
            db.rollback();
        }

        return res;
    }




    private void initTags(Statement st) throws SQLException {

        Map<String, DescrTagid> tmp = getTaglistMap(st);

        for(Rec rec: recs.values()) {
            DescrTagid dti = tmp.get(rec.getPathTagname());
            if( dti == null)
                rec.idtag = createTaglistRec(st, rec.getPathTagname(), rec.descr);
            else {
                rec.idtag = dti.tagid;
                if( !rec.descr.equals(dti.descr) )
                    updateTaglistRec(st, rec.idtag, rec.descr);
                tmp.remove(rec.getPathTagname());
            }
            rec.hasError = false;
        }

        for(String tagname: tmp.keySet())
            deleteTaglistRec(st, tmp.get(tagname).tagid, tagname);
    }




    @Override
    protected void setPstInsertData() throws SQLException {
        int i=0;
        for(Rec rec: recs.values() ) {
            if( rec.tag == null )
                pstInsert.setNull(i+1, ftypeSql);
            else {
                setPstInsertFunc.apply(i + 1, rec.tag);
            }
            i++;
        }
        pstInsert.setLong(++i, currec);
        pstInsert.setTimestamp(++i, Timestamp.valueOf(LocalDateTime.now()));
    }


    @Override
    public boolean execute() throws SQLException {

        long curms = System.currentTimeMillis();
        if( !hasExecTimeCome(curms) )
            return true;

        return super.execute();
    }


    @Override
    public String getInfo() {
        long nolink_cnt = recs.values().stream().filter(rec -> rec.tag == null).count();
        return String.format("\r\n%s: ROBO per=%d size=%d cur=%d tags=%d%s",
                arcname,
                period_ms,
                arcsize,
                currec,
                recs.size(),
                (nolink_cnt == 0? "": "/"+ ANSI.RED + nolink_cnt + " nolink" + ANSI.RESET)
        );
    }


    @Override
    public String getNolinks() {
        StringBuilder sb = new StringBuilder();
        recs.values().stream().filter(rec -> rec.tag == null).forEach(rec -> sb.append("  " +rec.tagname + "\r\n"));
        return sb.toString();
    }


}




