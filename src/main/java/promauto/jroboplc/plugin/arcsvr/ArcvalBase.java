package promauto.jroboplc.plugin.arcsvr;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import promauto.jroboplc.core.api.*;
import promauto.utils.ParamsMap;

import java.sql.*;
import java.util.*;
import java.util.stream.Collectors;

public abstract class ArcvalBase {
    private final Logger logger = LoggerFactory.getLogger(ArcvalBase.class);
    private static final long REC_MAX = Long.MAX_VALUE;


    enum FieldType {INT16, INT32, INT64, FLOAT, DOUBLE}

    protected static class DescrTagid {
        String descr;
        int tagid;
    }

    @FunctionalInterface
    public interface SqlExceptionBiConsumer<T, U> {
        void apply(T t, U u) throws SQLException;
    }


    protected final ArcsvrModule module;
    protected final ArcvalHolder holder;
    protected String arcname;

    protected Environment env;
    protected int idarc;
    protected String descr;
    protected int period_ms;
    protected int arcsize;
    protected long currec;

    protected int timemax;
    protected int timecnt;

    protected boolean period_align;
    protected long lastpass_ms;

    protected FieldType ftype;
    protected int ftypeSql;
    protected String ftypeStr = "";


    protected PreparedStatement pstInsert = null;
    protected PreparedStatement pstDelete = null;

    protected SqlExceptionBiConsumer<Integer,Tag> setPstInsertFunc;

    protected String lastSql;


    public ArcvalBase(ArcvalHolder arcvalHolder, String arcname) {
        this.module = arcvalHolder.module;
        this.holder = arcvalHolder;
        this.arcname = arcname;
        env = EnvironmentInst.get();
    }


    public abstract void link();


    public boolean init() {

        timemax = calcTaskTimes(period_ms);
        timecnt = 0;

        switch (ftype) {
            case INT16:
                ftypeSql = Types.SMALLINT;
                ftypeStr = "SMALLINT";
                setPstInsertFunc = (index,tag) -> pstInsert.setShort(index, (short)tag.getInt());
                break;
            case INT32:
                ftypeSql = Types.INTEGER;
                ftypeStr = "INTEGER";
                setPstInsertFunc = (index,tag) -> pstInsert.setInt(index, tag.getInt());
                break;
            case INT64:
                ftypeSql = Types.BIGINT;
                ftypeStr = "BIGINT";
                setPstInsertFunc = (index,tag) -> pstInsert.setLong(index, tag.getLong());
                break;
            case FLOAT:
                ftypeSql = Types.FLOAT;
                ftypeStr = "FLOAT";
                setPstInsertFunc = (index,tag) -> pstInsert.setFloat(index, (float)tag.getDouble());
                break;
            case DOUBLE:
                ftypeSql = Types.DOUBLE;
                ftypeStr = "DOUBLE PRECISION";
                setPstInsertFunc = (index,tag) -> pstInsert.setDouble(index, tag.getDouble());
                break;
        }

        return true;
    }



    protected final int calcTaskTimes(int ms) {
        Task task = env.getTaskManager().getTask(module);
        return (task == null || task.getPeriod() == 0)? 0: ms / task.getPeriod();
    }


    protected boolean hasExecTimeCome(long curms) {
        if(period_align) {
            long ms = curms - lastpass_ms;
            if( ms < period_ms )
                return false;
            else
                lastpass_ms = lastpass_ms + period_ms * (int)(ms/period_ms);
        } else {
            if(++timecnt < timemax)
                return false;
            else
                timecnt = 0;
        }
        return true;
    }


    // *** arclist procs ***
    protected void initArc(Statement st) throws SQLException {
        String sql = String.format("select idarc, period, reccnt, descr from %s where arcname='%s'",
                module.makeTableName("arclist"),
                module.makeTableName(arcname));
        lastSql = sql;
        try( ResultSet rs = st.executeQuery(sql) ) {
            if( rs.next() ) {
                idarc = rs.getInt(1);
                if( period_ms != rs.getInt(2)  ||  arcsize != rs.getInt(3)  ||  !descr.equals(rs.getString(4)) )
                    updateArclistRec(st);
            } else
                insertArclistRec(st);
        }
    }


    protected void insertArclistRec(Statement st) throws SQLException {
        String sql = String.format("insert into %s (arcname, period, reccnt, descr) values ('%s', %d, %d, '%s')",
                module.makeTableName("arclist"),
                module.makeTableName(arcname),
                period_ms,
                arcsize,
                descr);
        lastSql = sql;
        idarc = module.getDatabase().insertReturningId(st, sql, "idarc");
        env.logInfo(logger, module.getName(), arcname, "Add archive: " + idarc);
    }


    protected void updateArclistRec(Statement st) throws SQLException {
        String sql = String.format("update %s set arcname='%s', period=%d, reccnt=%d, descr='%s' where idarc=%d",
                module.makeTableName("arclist"),
                module.makeTableName(arcname),
                period_ms,
                arcsize,
                descr,
                idarc);
        lastSql = sql;
        env.logInfo(logger, module.getName(), arcname, "Update archive: " + idarc);
        st.executeUpdate(sql);
    }



    // *** taglist procs ***
    protected Map<String, DescrTagid> getTaglistMap(Statement st) throws SQLException {
        String sql = String.format("select idtag, tagname, descr from %s where idarc=%d",
                module.makeTableName("taglist"),
                idarc);
        lastSql = sql;

        Map<String, DescrTagid> map = new HashMap<>();
        try (ResultSet rs = st.executeQuery(sql)) {
            while (rs.next()) {
                DescrTagid dti = new DescrTagid();
                dti.descr = rs.getString(3);
                dti.tagid = rs.getInt(1);
                map.put(rs.getString(2), dti);
            }
        }

        return map;
    }


    protected int createTaglistRec(Statement st, String tagname, String descr) throws SQLException {
        String sql = String.format("insert into %s (idarc, tagname, descr) values (%d, '%s', '%s')",
                module.makeTableName("taglist"),
                idarc,
                tagname,
                descr);
        lastSql = sql;
        int idtag = module.getDatabase().insertReturningId(st, sql, "idtag");
        env.logInfo(logger, module.getName(), arcname, "Add tag: " + idtag, tagname);
        return idtag;
    }


    protected void updateTaglistRec(Statement st, int idtag, String descr) throws SQLException {
        String sql = String.format("update %s set descr='%s' where idtag=%d",
                module.makeTableName("taglist"),
                descr,
                idtag);
        lastSql = sql;
        st.executeUpdate(sql);
        env.logInfo(logger, module.getName(), arcname, "Update tag: " + idtag, descr);
    }


    protected void deleteTaglistRec(Statement st, int idtag, String tagname) throws SQLException {
        String sql = String.format("delete from %s where idtag=%d",
                module.makeTableName("taglist"),
                idtag);
        lastSql = sql;
        st.executeUpdate(sql);
        env.logInfo(logger, module.getName(), arcname, "Delete tag: " + idtag, tagname);
    }




    // *** arctable ***
    protected void initArctable(Statement st, List<Integer> idtags) throws Exception {
        Database db = module.getDatabase();
        ParamsMap params = new ParamsMap(
                "schema=" + module.getSchema(),
                "table=" + arcname
        );

        if( !db.executeScript("arcsvr_arcval_arctable", params).success )
            throw new Exception("Arctable create error: " + arcname);

        List<Integer> delfields = new LinkedList<>();
        Set<Integer> oldfields = new HashSet<>();

        String sql = String.format("select first 0 * from %s",
                module.makeTableName(arcname));
        lastSql = sql;
        try(ResultSet rs = st.executeQuery(sql)) {
            ResultSetMetaData md = rs.getMetaData();
            for( int i=1; i<=md.getColumnCount(); ++i) {
                String field = md.getColumnName(i);
                if(field.startsWith("T")) {
                    int idtag = Integer.parseInt(field.substring(1));
                    if( md.getColumnType(i) == ftypeSql )
                        oldfields.add(idtag);
                    else
                        delfields.add(idtag);
                }
            }
        }

        alterTable(st,"drop", delfields); // delete fields with wrong data type

        List<Integer> addfields = idtags.stream()
                .filter(idtag -> !oldfields.contains(idtag))
                .collect(Collectors.toList());
        Collections.sort(addfields);

        delfields = oldfields.stream()
                .filter(idtag -> !idtags.contains(idtag))
                .collect(Collectors.toList());

        alterTable(st,"drop", delfields);
        alterTable(st,"add", addfields);
    }


    private void alterTable(Statement st, String cmd, List<Integer> fields) throws SQLException {
        if(fields.size() > 0) {
            String stype = cmd.equals("add")? " " + ftypeStr: "";
            String sqlfields = fields.stream()
                    .map(idtag -> cmd + " T" + idtag + stype)
                    .collect(Collectors.joining(","));

            env.logInfo(logger, module.getName(), arcname, "Alter table:", cmd, fields.stream()
                    .map(idtag -> "T" + idtag)
                    .collect(Collectors.joining(",")));

            String sql = String.format("alter table %s %s",
                    module.makeTableName(arcname),
                    sqlfields);
            lastSql = sql;
            st.executeUpdate(sql);
        }
    }


    // prepare statements
    protected void initPst(List<Integer> idtags) throws SQLException {
        closePreparedStatements();

        if( idtags.size() > 0 ) {
            String fields = idtags.stream()
                    .map(idtag -> "T" + idtag)
                    .collect(Collectors.joining(","));

            String values = String.join(",", Collections.nCopies(idtags.size(), "?"));

            String sql = String.format("insert into %s (%s, rec, dt) values (%s, ?, ?)",
                    module.makeTableName(arcname),
                    fields,
                    values);
            lastSql = sql;

            Database db = module.getDatabase();
            pstInsert = db.getConnection().prepareStatement(sql);

            sql = String.format("delete from %s where rec=?",
                    module.makeTableName(arcname));
            lastSql = sql;
            pstDelete = db.getConnection().prepareStatement(sql);
        }

    }



    protected void initCurrec(Statement st) throws SQLException {
        // get reccnt
        String sql = String.format("select first 1 rec from %s order by rec desc",
                module.makeTableName(arcname));
        lastSql = sql;
        try(ResultSet rs = st.executeQuery(sql)) {
            if( rs.next() ) {
                currec = (currec = rs.getLong(1)) > 0? currec: 0;

            } else
                currec = 0;
        }

        // delete all old
        sql = String.format("delete from %s where rec<=%d",
                module.makeTableName(arcname),
                currec - arcsize);
        lastSql = sql;
        st.executeUpdate(sql);
    }



    protected void closePreparedStatements() {
        closePreparedStatement(pstInsert);
        closePreparedStatement(pstDelete);
        pstInsert = null;
        pstDelete = null;
    }


    private void closePreparedStatement(PreparedStatement pst) {
        if (pst != null) {
            try {
                pst.close();
            } catch (SQLException e) {
                env.printError(logger, e);
            }
        }
    }



    public boolean execute() throws SQLException {

        if( pstInsert == null  ||  pstDelete == null )
            return false;

        // delete old
        if( ++currec > arcsize) {
            pstDelete.setLong( 1, currec - arcsize);
            pstDelete.executeUpdate();
        }

        // insert new
        setPstInsertData();
        pstInsert.executeUpdate();

        // overflow (the far future)
        if( currec > REC_MAX ) {
            try(Statement st = module.getDatabase().getConnection().createStatement()) {
                String sql = String.format("delete from %s where rec<=%d and rec>%d",
                        module.makeTableName(arcname),
                        currec - arcsize,
                        currec);
                lastSql = sql;
                st.executeUpdate(sql);

                sql = String.format("update %s set rec=rec-%d",
                        module.makeTableName(arcname),
                        currec - arcsize,
                        currec - arcsize);
                lastSql = sql;
                st.executeUpdate(sql);
                currec = arcsize;
            }
        }

        return true;
    }


    public String pack() {
        return "";
    }


    protected abstract void setPstInsertData() throws SQLException;

    public abstract String getInfo();

    public abstract String getNolinks();

}
