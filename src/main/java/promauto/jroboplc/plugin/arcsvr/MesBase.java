package promauto.jroboplc.plugin.arcsvr;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import promauto.jroboplc.core.api.*;

import java.sql.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.function.Function;

abstract public class MesBase {
    private final Logger logger = LoggerFactory.getLogger(MesBase.class);

    private static Map<String,Integer> colortable = new HashMap<>();
    static {
        colortable.put("AQUA",       0xFFFF00);
        colortable.put("BLACK",      0x000000);
        colortable.put("BLUE",       0xFF0000);
        colortable.put("CREAM",      0xF0FBFF);
        colortable.put("DARKGREY",   0x808080);
        colortable.put("FUCHSIA",    0xFF00FF);
        colortable.put("GRAY",       0x808080);
        colortable.put("GREEN",      0x008000);
        colortable.put("LIMEGREEN",  0x00FF00);
        colortable.put("LIGHTGRAY",  0xC0C0C0);
        colortable.put("MAROON",     0x000080);
        colortable.put("MEDIUMGRAY", 0xA4A0A0);
        colortable.put("MINTGREEN",  0xC0DCC0);
        colortable.put("NAVYBLUE",   0x800000);
        colortable.put("OLIVEGREEN", 0x008080);
        colortable.put("PURPLE",     0x800080);
        colortable.put("RED",        0x0000FF);
        colortable.put("SILVER",     0xC0C0C0);
        colortable.put("SKYBLUE",    0xF0CAA6);
        colortable.put("TEAL",       0x808000);
        colortable.put("WHITE",      0xFFFFFF);
        colortable.put("YELLOW",     0x00FFFF);

    }



    protected final static int ALARM_NO_VISUAL = 2;
    protected final static int ALARM_NO_AFFIRM = 4;
    protected final static int ALARM_NO_OUT = 0x28;
    protected final static int ALARM_NO_IN = 16;

    protected final static int EVENTTYPE_BY_CHANGE = 7;



    protected static class DescrTagid {
        String descr;
        int tagid;
        int placeid;
    }

    protected final ArcsvrModule module;
    protected final MesHolder holder;
    protected Environment env;
    protected int idmsg;
    protected String mestext;
    protected int clb_inp = colortable.get("RED");
    protected int clf_inp = colortable.get("WHITE");
    protected int clb_out = colortable.get("MAROON");
    protected int clf_out = colortable.get("WHITE");
    protected int clb_ack = colortable.get("GRAY");
    protected int clf_ack = colortable.get("BLACK");
    protected int eventtype = 0;
    protected int eventval = 1;
    protected int alarm = 1;
    protected boolean noaffirm = false;

    protected Function<Integer,Boolean> eventfunc = (x) -> false;

    protected Map<String,MesRec> recs = new TreeMap<>();


/*
EventType:
  0. x = val
  1. x >= val
  2. x <= val
  3. (x & val) = val
  4. (x & val) = 0
  5. (x & val) > 0
  6. ((x & val) > 0) && (x & val) != val
  7. by change

Alarm:
    b0 = 1
    b1 = no visualization
    b2 = no affirmation
    b3 = no 'out'
    b4 = no 'in'
*/

    public MesBase(MesHolder holder, int idmsg) {
        this.module = holder.module;
        this.holder = holder;
        this.idmsg = idmsg;
        env = EnvironmentInst.get();
    }


    public boolean load(Object conf) {
        Configuration cm = env.getConfiguration();

        idmsg = cm.get(conf, "id", 0);
        mestext = cm.get(conf, "mestext", "");

        String colors = cm.get(conf, "colors", "").trim();
        if( colors.isEmpty() ) {
            clb_inp = parseColor(cm.get(conf, "clb_inp", "" + clb_inp));
            clf_inp = parseColor(cm.get(conf, "clf_inp", "" + clf_inp));
            clb_out = parseColor(cm.get(conf, "clb_out", "" + clb_out));
            clf_out = parseColor(cm.get(conf, "clf_out", "" + clf_out));
            clb_ack = parseColor(cm.get(conf, "clb_ack", "" + clb_ack));
            clf_ack = parseColor(cm.get(conf, "clf_ack", "" + clf_ack));
        } else {
            String[] cc = colors.split(";");
            clb_inp = parseColor( cc.length>0? cc[0].trim(): "" + clb_inp);
            clf_inp = parseColor( cc.length>1? cc[1].trim(): "" + clf_inp);
            clb_out = parseColor( cc.length>2? cc[2].trim(): "" + clb_out);
            clf_out = parseColor( cc.length>3? cc[3].trim(): "" + clf_out);
            clb_ack = parseColor( cc.length>4? cc[4].trim(): "" + clb_ack);
            clf_ack = parseColor( cc.length>5? cc[5].trim(): "" + clf_ack);
        }

        alarm = 1;
        alarm |= cm.get(conf, "no_visual",  false)? ALARM_NO_VISUAL: 0;
        alarm |= cm.get(conf, "no_affirm",  false)? ALARM_NO_AFFIRM: 0;
        alarm |= cm.get(conf, "no_out",     false)? ALARM_NO_OUT: 0;
        alarm |= cm.get(conf, "no_in",      false)? ALARM_NO_IN: 0;

        noaffirm = cm.get(conf, "no_affirm",  false);

        eventtype = cm.get(conf, "eventtype", 0);
        eventval = cm.get(conf, "eventval", 1);
        if( !initEventFunc() )
            return false;
        return true;
    }


    protected int parseColor(String colorstr) {
        Integer color = colortable.get(colorstr.trim().toUpperCase());
        if( color == null ) {
            color = 0;
            colorstr = colorstr.trim();
            if( !colorstr.isEmpty() ) {
                try {
                    if (colorstr.charAt(0) == '$') {
                        color = Integer.parseInt(colorstr.substring(1), 16);
                    } else {
                        color = Integer.parseInt(colorstr, 10);
                    }
                } catch (NumberFormatException e) {
                    env.printError(logger, e, module.getName(), "colorstr=" + colorstr);
                }
            }
        }
        return color;
    }



    public boolean init() {
        boolean res = false;
        Database db = module.getDatabase();
        try(Statement st = db.getConnection().createStatement()) {

            initMes(st);
            initPlaces(st);
            initTags(st);
            db.commit();

            res = true;
        } catch (Exception e) {
            env.printError(logger, e, module.getName(), "id=" + idmsg, "Init error");
            db.rollback();
        }
        return res;
    }

    protected boolean initEventFunc() {
        switch (eventtype) {
            case 0: eventfunc = (x) -> x == eventval; break;
            case 1: eventfunc = (x) -> x >= eventval; break;
            case 2: eventfunc = (x) -> x <= eventval; break;
            case 3: eventfunc = (x) -> (x & eventval) == eventval; break;
            case 4: eventfunc = (x) -> (x & eventval) == 0; break;
            case 5: eventfunc = (x) -> (x & eventval) > 0; break;
            case 6: eventfunc = (x) -> ((x & eventval) > 0) && (x & eventval) != eventval; break;
            case 7: eventfunc = (x) -> true; break;
            default:
                env.printError(logger, module.getName(), "idmsg=" + idmsg, "Bad eventtype: " + eventtype);
                return false;
        }
        return true;
    }

    protected void initTags(Statement st) throws SQLException {
    }

    // *** arclist procs ***
    protected void initMes(Statement st) throws SQLException {


        String sql = String.format(
                "select name, alarm, clb_inp, clf_inp, clb_out, clf_out, clb_ack, clf_ack, eventtype, eventval from %s where idmsg=%d",
                module.makeTableName("meslist"),
                idmsg);
        try( ResultSet rs = st.executeQuery(sql) ) {
            if( rs.next() ) {
                boolean flag = false;
                flag |= !rs.getString(1).equals(mestext);
                flag |= rs.getInt(2) != alarm;
                flag |= rs.getInt(3) != clb_inp;
                flag |= rs.getInt(4) != clf_inp;
                flag |= rs.getInt(5) != clb_out;
                flag |= rs.getInt(6) != clf_out;
                flag |= rs.getInt(7) != clb_ack;
                flag |= rs.getInt(8) != clf_ack;
                flag |= rs.getInt(9) != eventtype;
                flag |= rs.getInt(10) != eventval;

                if( flag )
                    updateMeslistRec(st);
            } else
                insertMeslistRec(st);
        }
    }


    protected void insertMeslistRec(Statement st) throws SQLException {
        String sql = String.format("insert into %s (" +
                        "idmsg, name, alarm, clb_inp, clf_inp, " +
                        "clb_out, clf_out, clb_ack, clf_ack, eventtype, " +
                        "eventval" +
                     ") values (" +
                        "%d, '%s', %d, %d, %d," +
                        "%d, %d, %d, %d, %d, " +
                        "%d)",
                    module.makeTableName("meslist"),
                    idmsg, mestext, alarm, clb_inp, clf_inp,
                    clb_out, clf_out, clb_ack, clf_ack, eventtype,
                    eventval);

        env.logInfo(logger, module.getName(), mestext, "Add message: " + idmsg);
        st.executeUpdate(sql);
    }


    protected void updateMeslistRec(Statement st) throws SQLException {
        String sql = String.format("update %s set name='%s', alarm=%d, clb_inp=%d, clf_inp=%d, clb_out=%d, clf_out=%d," +
                        " clb_ack=%d, clf_ack=%d, eventtype=%d, eventval=%d where idmsg=%d",
                module.makeTableName("meslist"),
                mestext, alarm, clb_inp, clf_inp,
                clb_out, clf_out, clb_ack, clf_ack, eventtype,
                eventval, idmsg);

        env.logInfo(logger, module.getName(), mestext, "Update message: " + idmsg);
        st.executeUpdate(sql);
    }



    // *** taglist procs ***
    protected Map<String, DescrTagid> getTaglistMap(Statement st) throws SQLException {
        String sql = String.format("select idtag, tagname, descr, idplace from %s where idmsg=%d",
                module.makeTableName("mestags"),
                idmsg);

        Map<String, DescrTagid> map = new HashMap<>();
        try (ResultSet rs = st.executeQuery(sql)) {
            while (rs.next()) {
                DescrTagid dti = new DescrTagid();
                dti.descr = rs.getString(3);
                dti.tagid = rs.getInt(1);
                dti.placeid = rs.getInt(4);
                map.put(rs.getString(2), dti);
            }
        }

        return map;
    }

    protected int createTaglistRec(Statement st, String tagname, String descr, int idplace) throws SQLException {
        String sql = String.format("insert into %s (idmsg, tagname, descr, idplace) values (%d, '%s', '%s', %d)",
                module.makeTableName("mestags"),
                idmsg,
                tagname,
                descr,
                idplace);
        int idtag = module.getDatabase().insertReturningId(st, sql, "idtag");
        env.logInfo(logger, module.getName(), "idmsg: " + idmsg, "Add tag: " + idtag, tagname);
        return idtag;
    }


    protected void updateTaglistRec(Statement st, int idtag, String descr, int idplace) throws SQLException {
        String sql = String.format("update %s set descr='%s', idplace=%d where idtag=%d",
                module.makeTableName("mestags"),
                descr,
                idplace,
                idtag);
        st.executeUpdate(sql);
        env.logInfo(logger, module.getName(), "idmsg: " + idmsg, "Update tag: " + idtag, descr, ""+idplace);
    }


    protected void deleteTaglistRec(Statement st, int idtag, String tagname) throws SQLException {
        String sql = String.format("delete from %s where idtag=%d",
                module.makeTableName("mestags"),
                idtag);
        st.executeUpdate(sql);
        env.logInfo(logger, module.getName(), "idmsg: " + idmsg, "Delete tag: " + idtag, tagname);
    }










    private void initPlaces(Statement st) throws SQLException {
        for(MesRec rec: recs.values())
            rec.placeid = getOrInsertPlace(st, rec.placename);
    }


    private int getOrInsertPlace(Statement st, String thePlacename) throws SQLException {

        thePlacename = thePlacename.trim();
        if( thePlacename.isEmpty() )
            return 0;

        int resPlaceid = 0;
        resPlaceid = holder.getPlace(thePlacename);
        if( resPlaceid > 0 )
            return resPlaceid;


        String sql = String.format(
                "select id from %s where name='%s'",
                module.makeTableName("places"),
                thePlacename);

        try( ResultSet rs = st.executeQuery(sql) ) {
            if (rs.next()) {
                resPlaceid = rs.getInt(1);
            } else {
                sql = String.format(
                        "insert into %s (name) values ('%s')",
                        module.makeTableName("places"),
                        thePlacename);

                resPlaceid = module.getDatabase().insertReturningId(st, sql);
                env.logInfo(logger, module.getName(), "Add place: " + resPlaceid, thePlacename);
            }

            holder.addPlace(thePlacename, resPlaceid);
        }


        return resPlaceid;
    }




//    protected void saveMes(int idtag, boolean active, int value, String datatext) throws SQLException {
    protected void saveMes(MesRec rec, String datatext) throws SQLException {
        if( !holder.isVersion2() ) {
            if (rec.active) {
                if ((alarm & ALARM_NO_IN) > 0)
                    return;
            } else {
                if ((alarm & ALARM_NO_OUT) > 0)
                    return;
            }
        }

        boolean inserting = rec.active  ||  !holder.isVersion2();
        PreparedStatement pst = inserting? holder.getPstInsert(): holder.getPstUpdateDtend();

        Timestamp ts = Timestamp.valueOf(LocalDateTime.now());
        pst.setTimestamp(1, ts);
        pst.setInt(2, idmsg);
        pst.setInt(3, rec.idtag);

        if( inserting ) {
            pst.setInt(4, rec.placeid);
            if (rec.active) {
                pst.setString(5, "I");
                pst.setInt(6, clb_inp);
                pst.setInt(7, clf_inp);
            } else {
                pst.setString(5, "O");
                pst.setInt(6, clb_out);
                pst.setInt(7, clf_out);
            }
            pst.setString(8, datatext);
        }

        pst.executeUpdate();

        if( noaffirm  &&  !inserting ) {
            pst = holder.getPstUpdateDtack();
            pst.setTimestamp(1, ts);
            pst.setInt(2, idmsg);
            pst.setInt(3, rec.idtag);
            pst.executeUpdate();
        }
    }

    protected void perfomPostFactum(Statement st) throws SQLException {
        if( !holder.isVersion2() )
            return;

        String sql = String.format(
                "select idtag from %s where idmsg=%d and dtend is null",
                module.makeTableName("messages"),
                idmsg);

        Set<Integer> idtags = new HashSet<>();
        try( ResultSet rs = st.executeQuery(sql) ) {
            while (rs.next()) {
                idtags.add(rs.getInt(1));
            }
        }

//        if( idtags.size() == 0 )
//            return;

        DateTimeFormatter formatter = module.getDatabase().getTimestampFormatter();
        String dt = LocalDateTime.now().format(formatter);

        // deactivate finished messages
        if( idtags.size() > 0 ) {
            for (MesRec rec : recs.values()) {
                if (!rec.active && idtags.contains(rec.idtag)) {
                    sql = String.format(
                            "update %s set dtend='%s' where idmsg=%d and idtag=%d and dtend is null",
                            module.makeTableName("messages"),
                            dt,
                            idmsg,
                            rec.idtag);
                    st.executeUpdate(sql);
                }
            }
        }

        // create messages for active but not existing in db
        if( eventtype != EVENTTYPE_BY_CHANGE ) {
            for (MesRec rec : recs.values())
                if (rec.active && !idtags.contains(rec.idtag)) {
                    saveMes(rec, "");
                }
        }

    }




    public void link() {

    }

    public boolean execute() throws SQLException {
        for(MesRec rec: recs.values()) {
            if( rec.tag == null )
                continue;

            if( eventtype == EVENTTYPE_BY_CHANGE ) {
                if( rec.value == rec.tag.getInt() )
                    continue;

                rec.value = rec.tag.getInt();
                rec.active = true;
                saveMes(rec, "" + rec.value);

                if( holder.isVersion2() ) {
                    rec.active = false;
                    saveMes(rec, "");
                }

            } else {
                if (rec.active == eventfunc.apply(rec.tag.getInt()))
                    continue;

                rec.active = !rec.active;
                saveMes(rec, "");
            }

        }
        return true;
    }


    public String getNolinks() {
        StringBuilder sb = new StringBuilder();
        recs.entrySet().stream().filter(rec -> rec.getValue().phantom).forEach(rec -> sb.append("  " +rec.getKey() + "\r\n"));
        return sb.toString();
    }


    public String getInfo() {
        long not_linked_cnt = recs.entrySet().stream().filter(rec -> rec.getValue().phantom).count();

        if( recs.size() == 0)
            return "";
        else
            return String.format("\r\n%d: %s, tags=%d%s",
                    idmsg,
                    mestext,
                    recs.size(),
                    (not_linked_cnt == 0? "": "/"+ ANSI.RED + not_linked_cnt + " nolink" + ANSI.RESET)
            );
    }

}
