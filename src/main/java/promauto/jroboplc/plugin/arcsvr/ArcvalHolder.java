package promauto.jroboplc.plugin.arcsvr;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import promauto.jroboplc.core.api.ANSI;
import promauto.jroboplc.core.api.Configuration;
import promauto.jroboplc.core.api.Environment;
import promauto.jroboplc.core.api.EnvironmentInst;
import promauto.utils.IniFile;
import promauto.utils.ParamsMap;

import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

public class ArcvalHolder {
    private final Logger logger = LoggerFactory.getLogger(ArcvalHolder.class);
    private final static Charset charset = Charset.forName("windows-1251");

    protected final ArcsvrModule module;
    protected final Environment env;

    protected Map<String,ArcvalBase> arcs = new TreeMap<>();
    private boolean enable;



    public ArcvalHolder(ArcsvrModule module) {
        this.module = module;
        this.env = EnvironmentInst.get();
    }

    public boolean isEnable() {
        return enable;
    }

    public boolean load(Object conf) {
        Configuration cm = env.getConfiguration();
        boolean res = true;

        arcs.clear();
        List<Path> paths = cm.getStringList( conf, "arcval.robo").stream()
                .map(filename -> Paths.get(filename))
                .map(path -> path.isAbsolute()? path: cm.getConfDir().resolve(path))
                .collect(Collectors.toList());

        for(Path path: paths)
            res &= loadFile(path);


        for(Map.Entry<String, Object> ent: cm.toMap(cm.get(conf, "arcval.regex")).entrySet() ) {
            String arcname = ent.getKey();
            if( arcs.containsKey(arcname) ) {
                env.printError(logger, module.getName(), arcname, "Duplicate name");
                return false;
            }

            ArcvalRegex arcval = new ArcvalRegex(this, arcname);
            res &= arcval.load(ent.getValue());
            arcs.put(arcname, arcval);
        }


        enable = arcs.size() > 0;
        return res;
    }


    private boolean loadFile(Path path) {

        try {
            IniFile ini = new IniFile(path);

            String tagpath = ini.getString("TagLogMain", "OpcAccessPath", "");
            int arccount = ini.getInt("TagLogMain", "ArcCount", 0);

            for(int arcnum=0; arcnum<arccount; ++arcnum) {
                String section = "TagLogArc" + arcnum;
                String arcname = ini.getString(section, "Name", "");

                ArcvalBase arcbase = arcs.get(arcname);
                if( arcbase != null  &&  !(arcbase instanceof  ArcvalRobo) ) {
                    env.printError(logger, module.getName(), arcname, "Duplicate name");
                    return false;
                }
                ArcvalRobo arc = (ArcvalRobo)arcbase;
                if( arc == null ) {
                    arc = new ArcvalRobo(this, arcname);
                    arcs.put(arcname, arc);
                }

                arc.load(ini, section, tagpath);
            }

        } catch(Exception e) {
            env.printError(logger, e, "File loading error:", path.toString());
            return false;
        }
        return true;
    }


    public void link() {
        if( !enable )
            return;

        for(ArcvalBase arc: arcs.values())
            arc.link();
    }


    public boolean init() {
        if( !enable )
            return true;

        ParamsMap params = new ParamsMap("schema=" + module.getSchema());
        if( !module.getDatabase().executeScript("arcsvr_arcval_init", params ).success )
            return false;

        boolean res = true;

        for(ArcvalBase arc: arcs.values())
            if( !arc.init() )
                res = false;

        return res;
    }


    public boolean execute() throws SQLException {
        if( !enable )
            return true;

        boolean res = true;

        for(ArcvalBase arc: arcs.values())
            if( !arc.execute() )
                res = false;

        return res;
    }


    public void closedown() {
        if( !enable )
            return;

        for(ArcvalBase arc: arcs.values())
            arc.closePreparedStatements();
    }


    public String getInfo() {
        String res = "";

        for(ArcvalBase arc: arcs.values())
            res += arc.getInfo();

        return res;
    }


    public String pack(String arcname) {
        String res = "";

        for(ArcvalBase arc: arcs.values())
            if( arcname.isEmpty()  ||  arc.arcname.equals(arcname) )
                res += arc.pack();

        return res;
    }

    public String showNolinks() {
        String res = "";
        String s;
        for(ArcvalBase arc: arcs.values())
            if( !(s = arc.getNolinks()).isEmpty() )
                res += ANSI.RED + ANSI.BOLD + arc.arcname  + ":\r\n" + ANSI.RESET + s;

        return res;
    }
}
