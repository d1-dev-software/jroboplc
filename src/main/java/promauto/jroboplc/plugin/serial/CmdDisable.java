package promauto.jroboplc.plugin.serial;

import java.util.regex.Pattern;

import promauto.jroboplc.core.AbstractCommand;
import promauto.jroboplc.core.api.Console;
import promauto.jroboplc.core.api.Module;

public class CmdDisable extends AbstractCommand {
	protected boolean modeDisable = true; 
	


	@Override
	public String getName() {
		return "disable";
	}

	@Override
	public String getUsage() {
		return "all|port";
	}

	@Override
	public String getDescription() {
		return (modeDisable? "dis": "en") + "ables for all ports or for one port if specified";
	}

	
	
	
	@Override
	public String execute(Console console, Module module, String args) {
		SerialManagerModule smm = (SerialManagerModule)module;

		if( !Pattern.matches("^all$|^\\d+$", args) )
			return "";
		
		String res;
		
		if( args.equals("all") ) {
			smm.ports.keySet().stream()
				.forEach( port_id -> smm.enables.put(port_id, !modeDisable ) );
			
			res = (modeDisable? "Dis": "En") + "abled all ports";
		} else {
			int port_id = Integer.parseInt(args);
			smm.enables.put(port_id, !modeDisable);
			res = (modeDisable? "Dis": "En") + "abled port " + port_id;
		}

		smm.saveEnables();
		
		return res + " (use reload to apply)";
	}


}
