package promauto.jroboplc.plugin.serial;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import promauto.jroboplc.core.api.Configuration;
import promauto.jroboplc.core.api.EnvironmentInst;
import promauto.jroboplc.core.api.SerialPort;
import promauto.jroboplc.core.tags.TagRW;
import promauto.utils.CRC;
import promauto.utils.Numbers;

public class SerialPortModbusTcp implements SerialPort {
	final Logger logger = LoggerFactory.getLogger(SerialPortModbusTcp.class);

	private Socket socketData;
	
	private InputStream inputData;
	private OutputStream outputData;
	
	private volatile boolean valid = true;
	private volatile boolean opened = false;

	private String host;
	private int port;
	private InetSocketAddress addrData;
	
	private static final int BUFFIN_SIZE = 2048;
	private int[] buffin = new int[BUFFIN_SIZE];
	private int buffinPos;
	private int buffinActualSize;

	private int id;
	
	private int timeout_ms;
	private boolean noTcpDelay;

	private boolean connectionLost = false;
	private long connectionLostTime = 0;
	private long recon_ms = 3000;
	private long liveness = 0;
	private SerialManagerModule module;
	private TagRW tagOpened;
	private int transactSent;



	
	public SerialPortModbusTcp(SerialManagerModule module) {
		this.module = module;
		transactSent = 0;
	}

	public synchronized boolean load(Object conf) {
		Configuration cm = EnvironmentInst.get().getConfiguration();
		
		id 			= cm.get(conf, "id",			0);
		recon_ms 	= cm.get(conf, "recon_ms", 		3000);
		timeout_ms 	= cm.get(conf, "timeout", 		200);
		noTcpDelay 	= cm.get(conf, "no_tcpdelay",	true);

		host = cm.get(conf, "host", "");
		port = cm.get(conf, "port", 502);
		addrData = new InetSocketAddress(host, port);

		tagOpened = module.getTagOpened(id);
		
		return true;
	}
	
	
	
	@Override
	public int getId() {
		return id;
	}

	@Override
	public synchronized boolean setParams(int baud, int databits, int parity, int stopbits, int timeout) {
		boolean reopen = this.timeout_ms != timeout;
		reopen &= opened;
		
		this.timeout_ms = timeout;

		boolean res = true;
		if( reopen ) {
			close();
			res = open();
		}
		return res;
	}

	@Override
	public synchronized boolean open() {
		return open(false);
	}

	public synchronized boolean open(boolean silent) {
		if( opened ) 
			return true;
		
		opened = connectSocket(silent);
		if( !opened ) {
			connectionLost = true;
			disconnectSocket();
		} else {
			connectionLost = false;
			tagOpened.setReadValInt(1);
		}
	
		return opened;
	}

	
	
	
	protected boolean connectSocket(boolean silent) {
		resetBuffin();
		try {
			socketData = null;
			
			socketData = new Socket();
			socketData.setSoTimeout(timeout_ms);
			socketData.setTcpNoDelay(noTcpDelay);
			socketData.connect( addrData, timeout_ms );
			inputData = socketData.getInputStream();
			outputData = socketData.getOutputStream();
		} catch (Exception e) {
			if( !silent )
				EnvironmentInst.get().printError(logger, e, module.getName(), "Connect socket:", getDescr());
			return false;
		}
		return true;
	}

	
	private void disconnectSocket() {
		resetBuffin();
		if( socketData != null ) {
			try {
				socketData.close();
			} catch (Exception e) {
				EnvironmentInst.get().printError(logger, e, module.getName(), "Disconnect socket:", getDescr());
			}
			socketData = null;
		}
		opened = false;
		tagOpened.setReadValInt( 0 );
	}

	

	
	@Override
	public synchronized void close() {
		if( !opened ) 
			return;

		disconnectSocket();
		connectionLost = false;
	}

	
	private void checkLiveness() {
		
		boolean ok = socketData.isBound();
		ok &= !socketData.isClosed();
		ok &= socketData.isConnected();
		ok &= !socketData.isInputShutdown();
		ok &= !socketData.isOutputShutdown();
 		
		if( ok ) {
			if( liveness == 0 ) {
				liveness  = System.currentTimeMillis();
				return;
			}
			
			if( System.currentTimeMillis() - liveness < recon_ms )
				return;
		}
		
		liveness = 0;
		
		if( opened ) {
			disconnectSocket();
			if( open(true) ) 
				return;
			
			EnvironmentInst.get().printError(logger, module.getName(), getDescr(), "Connection lost");
			connectionLost  = true;
			connectionLostTime = System.currentTimeMillis();
		}
	}
	
	private void affirmLiveness() {
		liveness = 0;
	}


	
	private String getDescr() {
		return "Serial port " + id + " (ModbusTcp " + host + ":" + port +  ")";
	}

	private boolean restoreConnectionIfLost() {
		if( !connectionLost )
			return true;
		
		if( System.currentTimeMillis() - connectionLostTime < recon_ms )
			return false;
		
		if( open(true) ) {
			EnvironmentInst.get().printInfo(logger, getDescr(), "Connection restored");
			return true;
		}
		
		connectionLostTime = System.currentTimeMillis();
		return false;
	}

	


	@Override
	public boolean isOpened() {
		return opened;
	}

	@Override
	public boolean isValid() {
		return valid;
	}

	@Override
	public void setInvalid() {
		valid = false;
	}

	
	
	@Override
	public int getAvailable() throws IOException {
		return inputData.available();
	}

	
	
	
//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<	
	
	private boolean updateAnswer() {
		if( !restoreConnectionIfLost()  ||  !opened )
			return false;

		if( isBuffinAvailable() ) 
			if( !receiveAnswer() ) {
				checkLiveness();
				return false;
			}
		
		affirmLiveness();

		return true;
	}

	
	private boolean receiveAnswer() {
		byte[] buff = readBytes(6);
		if( buff == null )
			return false;
		
		int transactReceived = Numbers.bytesToWord(buff, 0);
		if( transactReceived != transactSent)
			return false;

		int msgLen = Numbers.bytesToWord(buff, 4);
		if( msgLen-2 > BUFFIN_SIZE )
			return false;
		
		buff = readBytes(msgLen);
		if( buff == null )
			return false;
		
		for( int i=0; i<msgLen; i++)
			buffin[i] = buff[i] & 0xff;
		
		buffinPos = 0;
		buffinActualSize = msgLen + 2;

		int crc = CRC.getCrc16(buff);
		buffin[msgLen] = crc & 0xFF;
		buffin[msgLen+1] = (crc >> 8) & 0xFF;
		
		return true;
	}
	

	private byte[] readBytes(int size) {
		long time = System.currentTimeMillis();
		int p = 0;
		byte[] buff = new byte[size];
		try {
			while (true) {
				p += inputData.read(buff, p, size - p);

				if (p == size)
					return buff;
				else if ((System.currentTimeMillis() - time) >= timeout_ms)
					return null;
			}
		} catch (IOException e) {
		}

		return null;
	}


	private int getByteFromBuffin() {
		return buffin[ buffinPos++ ];
	}


	private boolean isBuffinAvailable() {
		return buffinPos >= buffinActualSize;
	}


	private void resetBuffin() {
		buffinPos = 0;
		buffinActualSize = 0;
	}

//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>	

	
	@Override
	public synchronized int readByte() {
		if( !updateAnswer() )
			return -1;
		
		if( isBuffinAvailable() )
			return -1;
		
		return getByteFromBuffin();
	}


	@Override
	public synchronized int readBytes(int[] buff, int size) {
		if( !updateAnswer() )
			return 0;
		
		if( isBuffinAvailable() )
			return 0;

		int n = Math.min(buffinActualSize - buffinPos, size);
		for( int i=0; i<n; i++)
			buff[i] = getByteFromBuffin(); 
				
		return n;
	}


	
	@Override
	public synchronized int readBytesDelim(int[] buff, int delim) {
		// not implemented
		return 0;
	}


	
	@Override
	public synchronized String readString(int size) {
		// not implemented
		return "";
	}

	
	@Override
	public synchronized String readStringDelim(int delim) {
		// not implemented
		return "";
	}

	
	@Override
	public synchronized boolean writeByte(int data) {
		// not implemented
		return false;
	}

	
	@Override
	public synchronized boolean writeBytes(int[] data, int size) {
		if( !restoreConnectionIfLost() )
			return false;
		
		if( !opened )
			return false;
		
		resetBuffin();
		
		transactSent = (++transactSent) & 0xFFFF;
		
		int bufflen = size+4;
		byte[] buff = new byte[bufflen];
		buff[0] = (byte)((transactSent >> 8) & 0xFF);
		buff[1] = (byte)(transactSent & 0xFF);
		buff[2] = 0;
		buff[3] = 0;
		int msglen = size - 2;
		buff[4] = (byte)((msglen >> 8) & 0xFF);
		buff[5] = (byte)(msglen & 0xFF);
		
		for(int i=6, j=0; j<msglen; ++i, ++j)
			buff[i] = (byte)data[j];
		
		try {
			outputData.write(buff);
		} catch (IOException e) {
			checkLiveness();
			return false;
		}
		return true;
	}

	
	@Override
	public synchronized boolean writeString(String data) {
		// not implemented
		return false;
	}



	@Override
	public synchronized boolean discard() {
		if( !opened )
			return false;
		
		resetBuffin();
		try {
			if( inputData.available() > 0 )
				inputData.skip( inputData.available() );
		} catch (IOException e) {
			return false;
		}
		return true;
	}

	
	@Override
	public String getInfo() {
		return String.format("%d: modbustcp %s:%d %d %s",
				id,
				host,
				port,
				timeout_ms,
				(opened? "opened": "closed"));
	}

}
