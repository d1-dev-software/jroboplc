package promauto.jroboplc.plugin.serial;


import static java.nio.charset.Charset.defaultCharset;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import promauto.jroboplc.core.AbstractModule;
import promauto.jroboplc.core.api.Configuration;
import promauto.jroboplc.core.api.Plugin;
import promauto.jroboplc.core.api.SerialManager;
import promauto.jroboplc.core.api.SerialPort;
import promauto.jroboplc.core.api.Tag;
import promauto.jroboplc.core.tags.TagRW;
import promauto.jroboplc.core.tags.TagRWInt;


public class SerialManagerModule extends AbstractModule implements SerialManager {

	private final Logger logger = LoggerFactory.getLogger(SerialManagerModule.class);
	
	protected HashMap<Integer,SerialPort> ports = new HashMap<>();

	private boolean opened = false;

	protected Path pathEnables;
	protected Map<Integer,Boolean> enables = new HashMap<>();

	
	
	public SerialManagerModule(Plugin plugin, String name) {
		super(plugin, name);
		taskable = false;
		
		env.getCmdDispatcher().addCommand(this, CmdEnable.class);
		env.getCmdDispatcher().addCommand(this, CmdDisable.class);
	}

	
	
	public boolean loadModule(Object conf) {
		
		Configuration cm = env.getConfiguration();
		
		enables.clear();
		pathEnables = cm.getPath(conf, "file.enable", getPlugin().getPluginName() + '.' + name + ".enable");
		loadEnables();

		for (Object portconf: cm.toList( cm.get(conf, "ports"))) {

			int port_id = cm.get(portconf, "id", 0);
			boolean port_enable = cm.get(portconf, "enable", true);
			String type = cm.get(portconf, "type", "jssc");
			SerialPort port = null;
			
			if( enables.containsKey(port_id) ) {
				if( port_enable == enables.get(port_id) )
					enables.remove(port_id);
				else
					port_enable = enables.get(port_id);
			}
			
			if( !enable  ||  !port_enable ) {
				SerialPortDisabled portimpl = new SerialPortDisabled(this);
				port = portimpl.load(portconf)? portimpl: null;
			} else {
				switch( type ) {
					
					case "jssc":
						SerialPortJSSC jssc = new SerialPortJSSC(this);
						port = jssc.load(portconf)? jssc: null;
						break;

					case "nport":
						SerialPortNPort nport = new SerialPortNPort(this);
						port = nport.load(portconf)? nport: null;
						break;

					case "usr410":
					case "usriot":
						SerialPortUsriot usriot = new SerialPortUsriot(this);
						port = usriot.load(portconf)? usriot: null;
						break;

					case "modbustcp":
						SerialPortModbusTcp modbustcp = new SerialPortModbusTcp(this);
						port = modbustcp.load(portconf)? modbustcp: null;
						break;

					case "finsudp":
						SerialPortFinsUdp finsudp = new SerialPortFinsUdp(this);
						port = finsudp.load(portconf)? finsudp: null;
						break;

					default:
					env.printError(logger, name, "Unknown port type:", type);
				}
			}
			
			if( port == null )
				return false;
			
			int id = port.getId();
			if( ports.containsKey(id) ) {
				env.printError(logger, name, "Duplicate port id:", ""+id);
				return false;
			}
				
			ports.put(id, port);
		}
		
		return true;
	}


	protected void loadEnables() {
		if( Files.exists(pathEnables) ) {
			try (BufferedReader reader = Files.newBufferedReader(pathEnables, defaultCharset())) {
				Pattern p = Pattern.compile("(\\d+)\\s*:\\s*(on|off)");
				String line;
				while ((line = reader.readLine()) != null) {
					Matcher m = p.matcher(line);
					if( !m.find() )
						continue;
					
					int port_id = Integer.parseInt( m.group(1) );
					boolean port_enabled = m.group(2).equals("on");
					enables.put(port_id, port_enabled);
				}
			} catch (Exception e) {
				env.printError(logger, e, name);
			}
		}
	}

	protected void saveEnables() {
		try (BufferedWriter writer = Files.newBufferedWriter(pathEnables, defaultCharset())) {

			StringBuilder sb = new StringBuilder();
			enables.entrySet().stream()
					.filter( e -> ports.containsKey(e.getKey()) )
					.forEach( e -> sb.append( e.getKey() + ":" + (e.getValue()? "on": "off")  + "\r\n") );
			
			writer.write( sb.toString() ); 
			writer.flush();
		} catch (IOException e) {
			env.printError(logger, e, name);
		}
	}

	
	
	
	@Override
	public boolean prepareModule() {
		openAll();
		return true;
	}

	
	
	@Override
	public boolean closedownModule() {
		closeAll();
		return true;
	}


	
	@Override
	public String getInfo() {
		StringBuilder sb = new StringBuilder();
		for(SerialPort port: ports.values()) {
			sb.append( port.getInfo() );
			sb.append("\r\n");
		}
		return sb.toString(); 
	}



	@Override
	public synchronized SerialPort getPort(int portid) {
		return ports.get(portid);
	}



	public boolean openAll() {
		for(SerialPort port: ports.values())
			port.open();

		opened = true;
		return true;
	}



	public void closeAll() {
		for(SerialPort port: ports.values())
			port.close();
		opened = false;
	}
	
	
	@Override
	public synchronized boolean reload() {
		boolean canLoad = (new SerialManagerModule(plugin, name)).load();
		if( !canLoad )
			return false;
		
		if( opened )
			closeAll();
		
		HashMap<Integer,SerialPort> ports_old = ports; 
		ports = new HashMap<>();
		
		if( load() ){
			for(SerialPort port: ports_old.values())
				port.setInvalid();
			
			ports_old.clear();

			if( enable  &&  env.isRunning() )
				return openAll();
			
			return true;
		}
		
		ports = ports_old;
		return false;
	}



	public TagRW getTagOpened(int portnum) {
		String tagname = portnum + ".opened";
		Tag tag = tagtable.get( tagname );
		if( tag == null )
			return tagtable.createRWInt(tagname, 0);
		else
			return (TagRWInt)tag;
	}
	
	

}
