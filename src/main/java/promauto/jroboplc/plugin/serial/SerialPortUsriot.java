package promauto.jroboplc.plugin.serial;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.charset.Charset;
import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import promauto.jroboplc.core.api.Configuration;
import promauto.jroboplc.core.api.EnvironmentInst;
import promauto.jroboplc.core.api.SerialPort;
import promauto.jroboplc.core.tags.TagRW;

public class SerialPortUsriot implements SerialPort {
	final Logger logger = LoggerFactory.getLogger(SerialPortUsriot.class);
	private final static Charset charset = Charset.forName("windows-1251");

	private Socket socketData;
	
	private InputStream inputData;
	private OutputStream outputData;
	
	private volatile boolean valid = true;
	private volatile boolean opened = false;

	private String host;
	private int port;
	private InetSocketAddress addrData;
	
	private static final int BUFFIN_SIZE = 2048;
	private byte[] buffin = new byte[BUFFIN_SIZE];

	private int id;
	
	private int baud;
	private int databits;
	private int parity; 
	private int stopbits;

	private int timeout_ms;
	private boolean noTcpDelay;

	private boolean connectionLost = false;
//	private boolean canInfoConnectionRestored = false;
//	private boolean canInfoConnectionLost = false;
	private long connectionLostTime = 0;
	private long recon_ms = 3000;
	private long liveness = 0;
	private SerialManagerModule module;
	private TagRW tagOpened;



	
	public SerialPortUsriot(SerialManagerModule module) {
		this.module = module;
	}

	public synchronized boolean load(Object conf) {
		Configuration cm = EnvironmentInst.get().getConfiguration();
		
		id 			= cm.get(conf, "id",			0);
		baud 		= cm.get(conf, "baud", 			9600);
		recon_ms 	= cm.get(conf, "recon_ms", 		3000);
		timeout_ms 	= cm.get(conf, "timeout", 		500);
		noTcpDelay 	= cm.get(conf, "no_tcpdelay",	true);

		host = cm.get(conf, "host", "");
		port = cm.get(conf, "port", 0);
		addrData = new InetSocketAddress(host, port);

		String sbits = cm.get(conf, "bits", "8/0/1");
		String[] bits = sbits.split("/");
		databits = bits.length > 0? Integer.parseInt(bits[0]): 8; 
		parity 	 = bits.length > 1? Integer.parseInt(bits[1]): 0; 
		stopbits = bits.length > 2? Integer.parseInt(bits[2]): 1;
		
		tagOpened = module.getTagOpened(id);

		if( !checkParams(databits, parity, stopbits) )
			return false;
		
		return true;
	}
	
	private boolean checkParams(int databits, int parity, int stopbits) {
		if( 115200 % baud > 0   ||  baud < 50  ||  
				databits < 5 || databits > 8  ||
				parity < 0  ||  parity > 4  ||
				stopbits < 1  || stopbits > 2 )
		{
			EnvironmentInst.get().printError(logger, module.getName(), getDescr(), "Bad bits configuration");
			return false;
		}
		return true;
	}
	
	
	
	@Override
	public int getId() {
		return id;
	}

	@Override
	public synchronized boolean setParams(int baud, int databits, int parity, int stopbits, int timeout) {
		boolean reopen = false;
		reopen |= baud != this.baud; 
		reopen |= databits != this.databits; 
		reopen |= parity != this.parity; 
		reopen |= stopbits != this.stopbits;
		reopen &= opened;
		
		if( !checkParams(databits, parity, stopbits) )
			return false;
		
		this.baud = baud;
		this.databits = databits;
		this.parity = parity;
		this.stopbits = stopbits;
		this.timeout_ms = timeout;

		boolean res = true;
		if( reopen ) {
			close();
			res = open();
		}
		return res;
	}

	@Override
	public synchronized boolean open() {
		return open(false);
	}

	public synchronized boolean open(boolean silent) {
		if( opened ) 
			return true;
		
		opened = connectSocket(silent)  &&  applyParams();
		if( !opened ) {
			connectionLost = true;
			disconnectSocket();
		} else {
			connectionLost = false;
			tagOpened.setReadValInt(1);
		}
	
		return opened;
	}
	
	
	
	protected boolean connectSocket(boolean silent) {
		try {
			socketData = null;
			
			socketData = new Socket();
			socketData.setSoTimeout(timeout_ms);
			socketData.setTcpNoDelay(noTcpDelay);
			socketData.connect( addrData, timeout_ms );
			inputData = socketData.getInputStream();
			outputData = socketData.getOutputStream();
		} catch (Exception e) {
			if( !silent )
				EnvironmentInst.get().printError(logger, e, module.getName(), "Connect socket:", getDescr());
			return false;
		}
		return true;
	}

	
	private void disconnectSocket() {
		if( socketData != null ) {
			try {
				socketData.close();
			} catch (Exception e) {
				EnvironmentInst.get().printError(logger, e, module.getName(), "Disconnect socket", getDescr());
			}
			socketData = null;
		}
		opened = false;
		tagOpened.setReadValInt( 0 );
	}

	
	private boolean applyParams() {
		try {
			buffin[0] = 0x55;
			buffin[1] = (byte)0xAA;
			buffin[2] = 0x55;

			buffin[3] = (byte)((baud >> 16) & 0xFF);
			buffin[4] = (byte)((baud >> 8) & 0xFF);
			buffin[5] = (byte)(baud & 0xFF);

			buffin[6] = (byte)(
					databits - 5  
					+  (stopbits > 1? 4: 0) 
					+  (parity > 0? 8 + (parity << 4): 0) );

			buffin[7] = (byte)((buffin[3] + buffin[4] + buffin[5] + buffin[6]) & 0xFF);
			
			outputData.write(buffin, 0, 8);

		} catch (Exception e) {
			EnvironmentInst.get().printError(logger, e, module.getName(), "Apply params:", getDescr());
			return false;
		}
		return true;
	}

	
	@Override
	public synchronized void close() {
		if( !opened ) 
			return;

		disconnectSocket();
		connectionLost = false;
	}

	
	private void checkLiveness() {
		
		boolean ok = socketData.isBound();
		ok &= !socketData.isClosed();
		ok &= socketData.isConnected();
		ok &= !socketData.isInputShutdown();
		ok &= !socketData.isOutputShutdown();
 		
		if( ok ) {
			if( liveness == 0 ) {
				liveness  = System.currentTimeMillis();
				return;
			}
			
			if( System.currentTimeMillis() - liveness < recon_ms )
				return;
		}
		
		liveness = 0;
		
		if( opened ) {
			disconnectSocket();
			try {
				Thread.sleep(timeout_ms);
			} catch (InterruptedException e) {
			}
			if( open(true) )
				return;

//			if( canInfoConnectionLost ) {
			EnvironmentInst.get().printError(logger, module.getName(), getDescr(), "Connection lost");
//				canInfoConnectionLost = false;
//			}
			connectionLost  = true;
			connectionLostTime = System.currentTimeMillis();
		}
	}
	
	private void affirmLiveness() {
		liveness = 0;
//		if(canInfoConnectionRestored) {
//			EnvironmentInst.get().printInfo(logger, getDescr(), "Connection restored");
//			canInfoConnectionRestored = false;
//		}
//		canInfoConnectionLost = true;

	}


	
	private String getDescr() {
		return "Serial port " + id + " (Usr410 " + host + ":" + port +  ")";
	}

	private boolean restoreConnectionIfLost() {
		if( !connectionLost )
			return true;
		
		if( System.currentTimeMillis() - connectionLostTime < recon_ms )
			return false;
		
		if( open(true) ) {
//			canInfoConnectionRestored = true;
			EnvironmentInst.get().printInfo(logger, getDescr(), "Connection restored");
			return true;
		}
		
		connectionLostTime = System.currentTimeMillis();
		return false;
	}

	


	@Override
	public boolean isOpened() {
		return opened;
	}

	@Override
	public boolean isValid() {
		return valid;
	}

	@Override
	public void setInvalid() {
		valid = false;
	}

	
	
	@Override
	public int getAvailable() throws IOException {
		return inputData.available();
	}


	
	@Override
	public synchronized int readByte() {
		if( !restoreConnectionIfLost()  ||  !opened )
			return -1;
		
		try {
			int b = inputData.read();
			affirmLiveness();
			return b;
		} catch(IOException e) {
		}

		checkLiveness();
		return -1;
	}

	
	@Override
	public synchronized int readBytes(int[] buff, int size) {
		if( !restoreConnectionIfLost() )
			return 0;
		
		if( !opened )
			return 0;
		
		long time = System.currentTimeMillis();
		int n = 0;
		byte[] bufftmp = new byte[size];
		try {
			int p = 0;
			while(true) {
				n += inputData.read(bufftmp, p, size-p);
				
				if( n < size ) {
					if ( (System.currentTimeMillis() - time) >= timeout_ms)
						break;
					p = n;
				} else
					break;
			}
		} catch(IOException e) {
		}

		if( n > size )
			n = size;
		
		for (int i=0; i<n; i++)
			buff[i] = bufftmp[i] & 0xFF;
		
		if( n==size ) {
			affirmLiveness();
			return n;
		}

		checkLiveness();
		return -n;
	}

	
	
	@Override
	public synchronized int readBytesDelim(int[] buff, int delim) {
		if( !restoreConnectionIfLost() )
			return 0;
		
		if( !opened )
			return 0;

		long time = System.currentTimeMillis();
		int sizemax = buff.length;
		int size = 0;
		int b;
		
		try {
			while (true) {
				if ( (System.currentTimeMillis() - time) >= timeout_ms) {
					checkLiveness();
					return -size; // timeout
				} 
				
				b = inputData.read();
				if( size < sizemax )
				{
					buff[size++] = b;
					if (b == delim )
						break;
				} else
					return -size; // overflow
			}
		} catch(IOException e) {
			checkLiveness();
			return -size; // timeout
		}
		affirmLiveness();
		return size; 
	}


	
	@Override
	public synchronized String readString(int size) {
		if( !restoreConnectionIfLost()  ||  !opened )
			return "";

		long time = System.currentTimeMillis();
		int n = 0;
		try {
			byte[] bufftmp = new byte[size];
			int p = 0;
			while(true) {
				n += inputData.read(bufftmp, p, size-p);
				
				if( n != size ) {
					if ( (System.currentTimeMillis() - time) >= timeout_ms) {
						checkLiveness();
						return ""; // timeout
					} 
					p = n;
				} else
					break;
					
			}
			
			if( n==size ) {
				affirmLiveness();
				return new String(bufftmp, charset);
			}
		
		} catch(IOException e) {
		}

		checkLiveness();
		return "";

	}

	
	@Override
	public synchronized String readStringDelim(int delim) {
		if( !restoreConnectionIfLost()  ||  !opened )
			return "";

		long time = System.currentTimeMillis();
		StringBuilder sb = null;
		int i = 0;
		int b = -1;
		try {
			while (true) {
			
				if ( (System.currentTimeMillis() - time) >= timeout_ms) {
					checkLiveness();
					return ""; // timeout
				} 
				
				b = inputData.read();
				buffin[i++] = (byte)b;
				
				if (b == delim )
					break;
				
				if( i >= BUFFIN_SIZE )
				{
					i = 0;
					if( sb == null )
						sb = new StringBuilder();
					sb.append( new String( Arrays.copyOf(buffin, i), charset) );
				}
			}
		} catch(IOException e) {
		}
		
		if( b != delim )
			checkLiveness();
		else
			affirmLiveness();

		if( sb == null )
			return new String( Arrays.copyOf(buffin, i), charset );
		else
		{
			sb.append( new String( Arrays.copyOf(buffin, i), charset ) );
			return sb.toString();
		}
	}

	
	@Override
	public synchronized boolean writeByte(int data) {
		if( !restoreConnectionIfLost() )
			return false;
		
		if( !opened )
			return false;
		
		try {
			outputData.write(data);
		} catch (IOException e) {
			checkLiveness();
			return false;
		}
		
		return true;
	}

	
	@Override
	public synchronized boolean writeBytes(int[] data, int size) {
		if( !restoreConnectionIfLost() )
			return false;
		
		if( !opened )
			return false;
		
		byte[] bufftmp = new byte[size];
		for(int i=0; i<size; ++i)
			bufftmp[i] = (byte)data[i];
		
		try {
			outputData.write(bufftmp);
		} catch (IOException e) {
			checkLiveness();
			return false;
		}
		return true;
	}

	
	@Override
	public synchronized boolean writeString(String data) {
		if( !restoreConnectionIfLost() )
			return false;

		if( !opened )
			return false;
		
		try {
			outputData.write( data.getBytes(charset) );
		} catch (IOException e) {
			checkLiveness();
			return false;
		}
		return true;
	}

	

	@Override
	public synchronized boolean discard() {
		if( !opened )
			return false;
		
		try {
			if( inputData.available() > 0 )
				inputData.skip( inputData.available() );
		} catch (IOException e) {
			return false;
		}
		return true;
	}

	
	@Override
	public String getInfo() {
		return String.format("%d: usr410 %s:%d %d %d/%d/%d %d %s",
				id,
				host,
				port,
				baud,
				databits,
				parity,
				stopbits,
				timeout_ms,
				(opened? "opened": "closed"));
	}

}
