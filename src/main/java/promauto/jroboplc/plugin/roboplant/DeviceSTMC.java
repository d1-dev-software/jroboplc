package promauto.jroboplc.plugin.roboplant;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import promauto.jroboplc.core.State;
import promauto.jroboplc.core.api.Tag;

public class DeviceSTMC extends Device {

	protected static final int STATE_OFF 			     = 0;
	protected static final int STATE_START_DLY	       	 = 1;
	protected static final int STATE_BEFORE_LOAD         = 2;
	protected static final int STATE_LOAD_OPEN           = 3;
	protected static final int STATE_LOAD_DLY            = 4;
	protected static final int STATE_WARM_PREPARE 		 = 5;
	protected static final int STATE_WARM                = 6;
	protected static final int STATE_INFLATE             = 7;
	protected static final int STATE_INFLATE_FINISH      = 8;
//														   = 9;
	protected static final int STATE_EXPOSITION_INFL     = 10;
	protected static final int STATE_EXPOSITION          = 11;
	protected static final int STATE_DEFLATE             = 12;
	protected static final int STATE_UNLOAD_OPEN         = 13;
	protected static final int STATE_UNLOAD_DLY          = 14;
	protected static final int STATE_BLOW_PREPARE        = 15;
	protected static final int STATE_BLOW_INFLATE        = 16;
//	protected static final int STATE_BLOW_K2_PREPARE     = 17;
//														   = 18;
	protected static final int STATE_BLOW_K2_DLY         = 19;
	protected static final int STATE_BLOW_K2_CLOSE       = 20;
	protected static final int STATE_CYCLE_PAUSE         = 21;

	private static List<Integer> stateseq = new ArrayList<>();
	static {
		stateseq.add(STATE_OFF);
		stateseq.add(STATE_START_DLY);
		stateseq.add(STATE_BEFORE_LOAD);
		stateseq.add(STATE_LOAD_OPEN);
		stateseq.add(STATE_LOAD_DLY);
		stateseq.add(STATE_WARM_PREPARE);
		stateseq.add(STATE_WARM);
		stateseq.add(STATE_INFLATE);
		stateseq.add(STATE_INFLATE_FINISH);
		stateseq.add(STATE_EXPOSITION_INFL);
		stateseq.add(STATE_EXPOSITION);
		stateseq.add(STATE_DEFLATE);
		stateseq.add(STATE_UNLOAD_OPEN);
		stateseq.add(STATE_UNLOAD_DLY);
		stateseq.add(STATE_BLOW_PREPARE);
		stateseq.add(STATE_BLOW_INFLATE);
//		stateseq.add(STATE_BLOW_K2_PREPARE);
		stateseq.add(STATE_BLOW_K2_DLY);
		stateseq.add(STATE_BLOW_K2_CLOSE);
		stateseq.add(STATE_CYCLE_PAUSE);
	}


	protected static final int WAIT_TIMER      = 1<<0;
	protected static final int WAIT_DVU1       = 1<<1;
	protected static final int WAIT_DVU2       = 1<<2;
	protected static final int WAIT_PLINE      = 1<<3;
	protected static final int WAIT_PWORK      = 1<<4;
	protected static final int WAIT_PLOAD      = 1<<5;
	protected static final int WAIT_PUNLOAD    = 1<<6;
	protected static final int WAIT_NEIGHBOUR  = 1<<7;

	private static final int NCL = 1; // not closed
	private static final int CLS = 2; // closed
	private static final int NOP = 3; // not opened
	private static final int OPN = 4; // opened
	
	private static final int ALARM_INFLATE_TIMEOUT 	= 1;
	private static final int ALARM_PALARM 			= 2;
	private static final int ALARM_PDIR 			= 3;

	
	Input inpEnable;
	Input inpSuspend;
	Input inpP;
	Input inpPEmrg;
	Input inpPLineHigh;
	Input inpNbState;
	Input inpDVU1;
	Input inpDVU2;
	Input inpClosedK1;
	Input inpClosedK2;
	Input inpClosedK3;
	Input inpClosedK4;
	Input inpClosedK5;
	Input inpClosedK6;
	Input inpClosedK7;
	Input inpClosedK8;
	Input inpOpenedK1;
	Input inpOpenedK2;
	
	Tag tagState;
	Tag tagWaitCl;
	Tag tagWaitOp;
	Tag tagWaitEx;
	Tag tagAlarm;
	Tag tagTimer;
	Tag tagCycleNum;
	Tag tagBlowNum;
	Tag tagOpenK1;
	Tag tagOpenK2;
	Tag tagOpenK3;
	Tag tagOpenK4;
	Tag tagOpenK5;
	Tag tagOpenK6;
	Tag tagFullK6;
	Tag tagOpenK7;
	Tag tagOpenK8;
	Tag tagBtnStart;
	Tag tagBtnStop;
	Tag tagNextState;
	Tag tagFlags;
	Tag tagCycleQnt;
	Tag tagPAlarm;
	Tag tagPWork;
	Tag tagPWorkMax;
	Tag tagPDeair;
	Tag tagPDelta;
	Tag tagPDeltaMax;
	Tag tagPOpenK6;
	Tag tagPOpenK5;
	Tag tagPLoad;
	Tag tagPUnload;
	Tag tagTimeBlowInfl;
	Tag tagTimeStartDly;
	Tag tagTimeLoad;
	Tag tagTimeWarm;
	Tag tagTimeInflMax;
	Tag tagTimeExp;
	Tag tagTimeExpMax;
	Tag tagTimeUnload;
	Tag tagTimeUnloadMin;
	Tag tagTimeBlow;
	Tag tagTimeCycDly;
	Tag tagBlowQnt;
	Tag tagBlowQntMax;
	Tag tagTimeNoExpInfl;
	Tag tagSuspend;

	private int state;
	private int stateidx;
	private int waitCl;
	private int waitOp;   
	private int waitEx;   
	private int alarm;   
	private int timer;   
	private int openK1;  
	private int openK2;  
	private int openK3;  
	private int openK4;  
	private int openK5;  
	private int openK6;  
	private int fullK6;  
	private int openK7;  
	private int openK8;  
	private int openK9;

	private int pdirK;     // P (steam pressure) direction: 0 - not changing, 1 - going up, -1 - going down
	private int pdirValue; // P value from previous cycle
	private boolean pdirAlarm; // true, if P is going down for specified time while inflating
	private int pdirTimer;

	

    @Override
	public void prepareTags(RefBool res) {
    	
    	inpEnable    = getInput("Enable"	, res);
    	inpSuspend   = getInput("Suspend"	, res);
    	inpP         = getInput("P"			, res);
    	inpPEmrg     = getInput("PEmrg"		, res);
		inpPLineHigh = getInput("PLineHigh"	, res);
		inpNbState   = getInput("NbState"	, res);
    	inpDVU1      = getInput("DVU1"		, res);
    	inpDVU2      = getInput("DVU2"		, res);
    	inpClosedK1  = getInput("ClosedK1"	, res);
    	inpOpenedK1  = getInput("OpenedK1"  , res);
    	inpClosedK2  = getInput("ClosedK2"  , res);
    	inpOpenedK2  = getInput("OpenedK2"  , res);
    	inpClosedK3  = getInput("ClosedK3"  , res);
    	inpClosedK4  = getInput("ClosedK4"  , res);
    	inpClosedK5  = getInput("ClosedK5"  , res);
    	inpClosedK6  = getInput("ClosedK6"  , res);
    	inpClosedK7  = getInput("ClosedK7"  , res);
    	inpClosedK8  = getInput("ClosedK8"  , res);

    	tagState         = getOutputTag( "State"        	, res);
    	tagWaitCl        = getOutputTag( "WaitCl"        	, res);
    	tagWaitOp        = getOutputTag( "WaitOp"        	, res);
    	tagWaitEx        = getOutputTag( "WaitEx"        	, res);
    	tagAlarm         = getOutputTag( "Alarm"        	, res);
    	tagTimer         = getOutputTag( "Timer"        	, res);
    	tagCycleNum      = getOutputTag( "CycleNum"     	, res);
    	tagBlowNum       = getOutputTag( "BlowNum"		, res);
    	tagOpenK1        = getOutputTag( "OpenK1"       	, res);
    	tagOpenK2        = getOutputTag( "OpenK2"       	, res);
    	tagOpenK3        = getOutputTag( "OpenK3"       	, res);
    	tagOpenK4        = getOutputTag( "OpenK4"       	, res);
    	tagOpenK5        = getOutputTag( "OpenK5"       	, res);
    	tagOpenK6        = getOutputTag( "OpenK6"       	, res);
    	tagFullK6        = getOutputTag( "FullK6"       	, res);
    	tagOpenK7        = getOutputTag( "OpenK7"       	, res);
    	tagOpenK8        = getOutputTag( "OpenK8"       	, res);
    	tagBtnStart      = getOutputTag( "BtnStart"     	, res);
    	tagBtnStop       = getOutputTag( "BtnStop"      	, res);
    	tagNextState     = getOutputTag( "NextState"    	, res);
    	tagFlags         = getOutputTag( "Flags"        	, res);
    	tagCycleQnt      = getOutputTag( "CycleQnt"     	, res);
    	tagPAlarm        = getOutputTag( "PAlarm"       	, res);
		tagPWork         = getOutputTag( "PWork"        	, res);
    	tagPWorkMax      = getOutputTag( "PWorkMax"     	, res);
		tagPDeair 		 = getOutputTag( "PDeair"        	, res);
    	tagPDelta        = getOutputTag( "PDelta"       	, res);
    	tagPDeltaMax     = getOutputTag( "PDeltaMax"    	, res);
    	tagPOpenK6       = getOutputTag( "POpenK6"      	, res);
    	tagPOpenK5       = getOutputTag( "POpenK5"      	, res);
    	tagPLoad         = getOutputTag( "PLoad"        	, res);
    	tagPUnload       = getOutputTag( "PUnload"      	, res);
    	tagTimeBlowInfl  = getOutputTag( "TimeBlowInfl"  	, res);
    	tagTimeStartDly  = getOutputTag( "TimeStartDly"  	, res);
    	tagTimeLoad      = getOutputTag( "TimeLoad"     	, res);
		tagTimeWarm      = getOutputTag( "TimeWarm"      	, res);
		tagTimeInflMax   = getOutputTag( "TimeInflMax"  	, res);
    	tagTimeExp       = getOutputTag( "TimeExp"      	, res);
    	tagTimeExpMax    = getOutputTag( "TimeExpMax"   	, res);
    	tagTimeUnload    = getOutputTag( "TimeUnload"   	, res);
    	tagTimeUnloadMin = getOutputTag( "TimeUnloadMin"	, res);
    	tagTimeBlow      = getOutputTag( "TimeBlow"     	, res);
    	tagTimeCycDly    = getOutputTag( "TimeCycDly"   	, res);
    	tagBlowQnt       = getOutputTag( "BlowQnt"      	, res);
		tagBlowQntMax    = getOutputTag( "BlowQntMax"   	, res);
		tagTimeNoExpInfl = getOutputTag( "TimeNoExpInfl" 	, res);
		tagSuspend       = getOutputTag( "Suspend" 		, res);


//		if (res.value)
//			resetState();
		state = 0;
		stateidx = 0;
		waitCl = 0;
		waitOp = 0;
		waitEx = 0;
		alarm = 0;
		timer = 0;
		openK1 = 0;
		openK2 = 0;
		openK3 = 0;
		openK4 = 0;
		openK5 = 0;
		openK6 = 0;
		fullK6 = 0;
		openK7 = 0;
		openK8 = 0;
		openK9 = 0;

		pdirK = 0;
		pdirValue = 0;
		pdirAlarm = false;
		pdirTimer = 0;
	}


//	protected void resetState() {
//		tagState     .setInt(0);
//		tagWaitCl    .setInt(0);
//		tagWaitOp    .setInt(0);
//		tagWaitEx    .setInt(0);
//		tagAlarm     .setInt(0);
//		tagTimer     .setInt(0);
//		tagCycleNum  .setInt(0);
//		tagBlowNum   .setInt(0);
//		tagOpenK1    .setInt(0);
//		tagOpenK2    .setInt(0);
//		tagOpenK3    .setInt(0);
//		tagOpenK4    .setInt(0);
//		tagOpenK5    .setInt(0);
//		tagOpenK6    .setInt(0);
//		tagFullK6    .setInt(0);
//		tagOpenK7    .setInt(0);
//		tagOpenK8    .setInt(0);
//		tagBtnStart  .setInt(0);
//		tagBtnStop   .setInt(0);
//		tagNextState .setInt(0);
//		tagSuspend   .setInt(0);
//
//		state = 0;
//		stateidx = 0;
//		waitCl = 0;
//		waitOp = 0;
//		waitEx = 0;
//		alarm = 0;
//		timer = 0;
//		openK1 = 0;
//		openK2 = 0;
//		openK3 = 0;
//		openK4 = 0;
//		openK5 = 0;
//		openK6 = 0;
//		fullK6 = 0;
//		openK7 = 0;
//		openK8 = 0;
//		openK9 = 0;
//
//		pdirK = 0;
//		pdirValue = 0;
//		pdirAlarm = false;
//		pdirTimer = 0;
//
//	}
	
	
	@Override
	public boolean execute() {
		
		makeConsistency();

		if( tagSuspend.getInt() == 0 )
			updateTimer();

		waitCl = 0;
		waitOp = 0;
		waitEx = 0;
		alarm = 0;


		if( tagNextState.getInt() > 0 ) {
			if (state > 0 && tagNextState.getInt() == 777) {
				nextState();
				resetTimer();
			}
			tagNextState.setInt(0);
		}
		
		
		
		if( state == STATE_OFF ) {
			setValves( 0,  0,   0,  0,   0,  0,   0,  0);
			tagBtnStop.setInt(0);
			if( tagBtnStart.getBool() ) {
				nextState();
				resetLast();
			}
		}
		tagBtnStart.setInt(0);

		if( tagSuspend.getBool()  &&  tagBtnStop.getBool() ) {
			setState(STATE_OFF);
			resetTimer();
		}


		if( tagSuspend.getInt() == 0 ) {

			if (state == STATE_START_DLY) {
//			tagCycleNum.setInt(0);
				setValves(0, 0, 0, 0, 0, 0, 0, 0);
				if (tagBtnStop.getBool()) {
					setState(STATE_OFF);
					resetTimer();
				} else {
					useTimer(tagTimeStartDly);
					nextStateIfChecked();
				}
			}


			if (state == STATE_BEFORE_LOAD) {
				if (tagBtnStop.getBool()) {
					setState(STATE_OFF);
				} else {
					setValves(0, 0, 0, 0, 1, 0, 0, 0);
					chkValves(CLS, CLS, CLS, CLS, NCL, CLS, CLS, CLS);

					chkEx(WAIT_PLINE, inpPLineHigh.getInt() > 0);
					chkEx(WAIT_PLOAD, inpP.getInt() <= tagPLoad.getInt());
					chkEx(WAIT_DVU1, inpDVU1.getInt() > 0);
					chkEx(WAIT_DVU2, inpDVU2.getInt() == 0);
					chkEx(WAIT_NEIGHBOUR, inpNbState.getInt() < STATE_LOAD_OPEN || inpNbState.getInt() > STATE_INFLATE);
					nextStateIfChecked();
				}
			}


			if (state == STATE_LOAD_OPEN) {
				setValves(1, 0, 0, 0, 1, 0, 0, 0);
				chkValves(NCL, 0, 0, 0, 0, 0, 0, 0);
				nextStateIfChecked();
			}


			if (state == STATE_LOAD_DLY) {
				setValves(1, 0, 0, 0, 1, 0, 0, 0);
				useTimer(tagTimeLoad);
				nextStateIfChecked();
			}


			if (state == STATE_WARM_PREPARE) {
				setValves(0, 0, 0, 0, 1, 0, 0, 0);
				chkValves(CLS, CLS, CLS, CLS, NCL, CLS, CLS, CLS);
				chkEx(WAIT_PLINE, inpPLineHigh.getInt() > 0);
				chkEx(WAIT_NEIGHBOUR,
						inpNbState.getInt() != STATE_WARM
								&& inpNbState.getInt() != STATE_INFLATE
								&& inpNbState.getInt() != STATE_EXPOSITION_INFL
								&& inpNbState.getInt() != STATE_BLOW_INFLATE
				);
				nextStateIfChecked();
			}


			if (state == STATE_WARM) {
				setValves(0, 0, 0, 1, 1, 0, 0, 1);

				if (inpNbState.getInt() == STATE_EXPOSITION_INFL
						|| inpNbState.getInt() == STATE_BLOW_INFLATE)
					openK4 = 0;

				useTimer(tagTimeWarm);
				if (nextStateIfChecked()) {
					resetTimer();
					setupTimer(tagTimeInflMax);
				}
			}


			if (state == STATE_INFLATE) {

				setValves(0, 0, 0, 1, 0, 0, 0, 1);

				if (inpNbState.getInt() == STATE_EXPOSITION_INFL
						|| inpNbState.getInt() == STATE_BLOW_INFLATE)
					openK4 = 0;

				chkEx(WAIT_PWORK, inpP.getInt() >= tagPWork.getInt());
				nextStateIfChecked();

				if (timer <= 0 && !checked())
					alarm = ALARM_INFLATE_TIMEOUT;
			}


			if (state == STATE_INFLATE_FINISH) {
				resetTimer();
				setValves(0, 0, 0, 0, 0, 0, 0, 1);
				chkValves(CLS, CLS, CLS, CLS, CLS, CLS, CLS, NCL);
				nextStateIfChecked();
			}


			if (state == STATE_EXPOSITION_INFL) {
				if (isTimerOff()) {
					nextState();
				} else {
					setValves(0, 0, 0, 1, 0, 0, 0, 1);
					chkEx(WAIT_PWORK, inpP.getInt() >= tagPWork.getInt());
					nextStateIfChecked();
				}
			}

			if (state == STATE_EXPOSITION) {
				setValves(0, 0, 0, 0, 0, 0, 0, 1);
				useTimer(tagTimeExp);
				if (!nextStateIfChecked())
					if ((inpP.getInt() <= tagPWork.getInt() - tagPDelta.getInt()) && (timer > tagTimeNoExpInfl.getInt())) {
						setState(STATE_EXPOSITION_INFL);
						openK4 = 1;
					}
			}


			if (state == STATE_DEFLATE) {
				setValves(0, 0, 0, 0, 1, 1, 0, 1);
				chkEx(WAIT_PUNLOAD, inpP.getInt() <= tagPUnload.getInt());
				nextStateIfChecked();
			}


			if (state == STATE_UNLOAD_OPEN) {
				setValves(0, 1, 0, 0, 1, 1, 0, 1);
				chkValves(0, NCL, 0, 0, 0, 0, 0, 0);
				nextStateIfChecked();
			}


			if (state == STATE_UNLOAD_DLY) {
				setValves(0, 1, 0, 0, 1, 0, 0, 1);
				useTimer(tagTimeUnload);
				nextStateIfChecked();
			}


			if (state == STATE_BLOW_PREPARE) {
				tagBlowNum.setInt(0);
				setValves(0, 1, 0, 0, 0, 0, 0, 0);
				chkValves(CLS, NCL, CLS, CLS, CLS, CLS, CLS, CLS);
				nextStateIfChecked();
			}


			if (state == STATE_BLOW_INFLATE) {
				if (tagBlowNum.getInt() >= tagBlowQnt.getInt())
					setState(STATE_BLOW_K2_CLOSE);
				else {
					setValves(0, 1, 0, 1, 0, 0, 1, 0);
					useTimer(tagTimeBlowInfl);
					if (nextStateIfChecked())
						resetTimer();
				}
			}


//		if( state == STATE_BLOW_K2_PREPARE ) {
//			setValves( 0,  1,   0,  0,   0,  0,   0,  0);
//			chkValves(CLS,NCL, CLS,CLS, CLS,CLS, CLS,CLS);
//			nextStateIfChecked();
//		}


			if (state == STATE_BLOW_K2_DLY) {
				setValves(0, 1, 0, 0, 0, 0, 0, 0);
				useTimer(tagTimeBlow);
				nextStateIfChecked();
			}


			if (state == STATE_BLOW_K2_CLOSE) {
				setValves(0, 1, 0, 0, 0, 0, 0, 0);
				chkValves(CLS, NCL, CLS, CLS, CLS, CLS, CLS, CLS);
				if (checked()) {
					tagBlowNum.setInt(tagBlowNum.getInt() + 1);
					if (tagBlowNum.getInt() >= tagBlowQnt.getInt())
						setState(STATE_CYCLE_PAUSE);
					else
						setState(STATE_BLOW_INFLATE);
				}
				resetTimer();
			}


			if (state == STATE_CYCLE_PAUSE) {
				if (tagTimeCycDly.getInt() <= 0)
					tagTimeCycDly.setInt(2);
				setValves(0, 0, 0, 0, 0, 0, 0, 0);
				useTimer(tagTimeCycDly);
				if (checked()) {
					setState(STATE_BEFORE_LOAD);

					tagCycleNum.setInt(tagCycleNum.getInt() + 1);
					if (tagCycleQnt.getInt() > 0) {
						tagCycleQnt.setInt(tagCycleQnt.getInt() - 1);
						if (tagCycleQnt.getInt() <= 0)
							setState(STATE_OFF);
					}

					if (tagBtnStop.getBool())
						setState(STATE_OFF);

					tagBlowNum.setInt(0);

					copyLast();
					resetLast();
				}
			}
		}


		openK5 = openK5 > 0  &&  (inpP.getInt() <= tagPOpenK5.getInt()  ||  state==STATE_WARM)? 1: 0;
		fullK6 =  openK6 > 0  &&  inpP.getInt() <= tagPOpenK6.getInt()? 1: 0;
		
		if( inpPEmrg.getInt() > 0  
				||  inpClosedK1.getInt()==0  ||  (inpClosedK2.getInt()==0  &&  state!=STATE_BLOW_INFLATE)
		  )
			openK3 = openK4 = 0;
		
		if( inpP.getInt() >= tagPAlarm.getInt() )
			alarm = ALARM_PALARM;



		// preventing steam going back while inflating
		if( inpP.getInt() < pdirValue )
			pdirK = -1;
		else if( inpP.getInt() > pdirValue )
			pdirK = 1;

		pdirValue = inpP.getInt();

//		if( state == STATE_INFLATE  ||  state == STATE_EXPOSITION_INFL ) {
		if( state == STATE_INFLATE ) {
			if( !pdirAlarm ) {
				if( pdirK < 0 ) {
					if (pdirTimer >= tagPDeair.getInt())
						pdirAlarm = true;
					else
						pdirTimer++;
				} else
					pdirTimer = 0;
			}

			if( pdirAlarm ) {
				alarm = ALARM_PDIR;
				openK3 = openK4 = 0;
			}


		} else {
			pdirTimer = 0;
			pdirAlarm = false;
			pdirK = 0;
		}




		tagState    .setInt( state );
		tagWaitCl   .setInt( waitCl );
		tagWaitOp   .setInt( waitOp );
		tagWaitEx   .setInt( waitEx );
		tagAlarm    .setInt( alarm  );
		tagTimer    .setInt( timer  );
		tagOpenK1   .setInt( openK1 );
		tagOpenK2   .setInt( openK2 );
		tagOpenK3   .setInt( openK3 );
		tagOpenK4   .setInt( openK4 );
		tagOpenK5   .setInt( openK5 );
		tagOpenK6   .setInt( openK6 );
		tagFullK6   .setInt( fullK6 );
		tagOpenK7   .setInt( openK7 );
		tagOpenK8   .setInt( openK8 );

		return true;
	}


	private void resetLast() {
	}

	private void copyLast() {
	}


	private void makeConsistency() {
		if( tagPWork.getInt() > tagPWorkMax.getInt() )
			tagPWork.setInt(tagPWorkMax.getInt());
		
		if( tagPDelta.getInt() > tagPDeltaMax.getInt() )
			tagPDelta.setInt( tagPDeltaMax.getInt() );
		
		if( tagTimeExp.getInt() > tagTimeExpMax.getInt() )
			tagTimeExp.setInt( tagTimeExpMax.getInt() );
		
		if( tagTimeUnload.getInt() < tagTimeUnloadMin.getInt() )
			tagTimeUnload.setInt( tagTimeUnloadMin.getInt() );

		if( tagBlowQnt.getInt() > tagBlowQntMax.getInt() )
			tagBlowQnt.setInt( tagBlowQntMax.getInt() );
	}


	private void useTimer(Tag time) {
		setupTimer(time);
		chkEx(WAIT_TIMER, timer <= 0);
	}

	private boolean isTimerOff() {
    	return timer <= 0;
	}


	private void setupTimer(Tag time) {
		if( timer < 0 ) 
			timer = time.getInt();
	}

	private void updateTimer() {
		if( timer >= 0 )
			timer--;
	}

	private void resetTimer() {
		timer = -1;
	}





	private void setState(int newstate) {
		for(int i=0; i < stateseq.size(); ++i)
			if( stateseq.get(i) == newstate) {
				stateidx = i;
				break;
			}
		state = stateseq.get(stateidx);
	}

	private void nextState() {
		stateidx++;
		if( stateidx >= 0  &&  stateidx < stateseq.size() )
			state = stateseq.get(stateidx);
		else
			setState( STATE_OFF );
	}

	private boolean nextStateIfChecked() {
		if( checked() ) {
			nextState();
			return true;
		}
		return false;
	}


	private boolean checked() {
		return (waitCl | waitOp | waitEx) == 0;
	}


	private void setValves(int k1, int k2, int k3, int k4, int k5, int k6, int k7, int k8) {
		openK1 = k1;  
		openK2 = k2; 
		openK3 = k3;  
		openK4 = k4;  
		openK5 = k5;  
		openK6 = k6;  
		openK7 = k7;  
		openK8 = k8;  
	}
	
	private void chkValves(int k1, int k2, int k3, int k4, int k5, int k6, int k7, int k8) {
		chkValve(0, k1, inpClosedK1, inpOpenedK1);
		chkValve(1, k2, inpClosedK2, inpOpenedK2);
		chkValve(2, k3, inpClosedK3);
		chkValve(3, k4, inpClosedK4);
		chkValve(4, k5, inpClosedK5);
		chkValve(5, k6, inpClosedK6);
		chkValve(6, k7, inpClosedK7);
		chkValve(7, k8, inpClosedK8);
	}


	private boolean chkValve(int bit, int mode, Input inp) {
		if( mode==NCL  &&  inp.getInt() > 0) 
			waitCl += 1<<bit;
		else
		if( mode==CLS  &&  inp.getInt() <= 0)
			waitCl += 1<<(bit + 8);
		else
			return true;
		
		return false;
	}

	private boolean chkValve(int bit, int mode, Input inpClosed, Input inpOpened) {
		if( !chkValve(bit, mode, inpClosed) )
			return false;
		
		if( mode==NOP  &&  inpOpened.getInt() > 0)
			waitOp += 1<<bit;
		else
		if( mode==OPN  &&  inpOpened.getInt() <= 0) 
			waitOp += 1<<(bit + 8);
		else
			return true;
		
		return false;
	}

	
	private boolean chkEx(int waitMask, boolean is_ok) {
		if( !is_ok ) {
			waitEx += waitMask;
			return false;
		}
		return true;
	}

	    


	@Override
	public void saveStateExtra(State state) {
		state.saveVar("state"     , this.state     );
		state.saveVar("stateidx"  , stateidx       );
		state.saveVar("waitCl"    , waitCl         );
		state.saveVar("waitOp"    , waitOp         );
		state.saveVar("waitEx"    , waitEx         );
		state.saveVar("alarm"     , alarm          );
		state.saveVar("timer"     , timer          );
		state.saveVar("openK1"    , openK1         );
		state.saveVar("openK2"    , openK2         );
		state.saveVar("openK3"    , openK3         );
		state.saveVar("openK4"    , openK4         );
		state.saveVar("openK5"    , openK5         );
		state.saveVar("openK6"    , openK6         );
		state.saveVar("fullK6"    , fullK6         );
		state.saveVar("openK7"    , openK7         );
		state.saveVar("openK8"    , openK8         );
		state.saveVar("openK9"    , openK9         );
		state.saveVar("pdirValue" , pdirValue      );
		state.saveVar("pdirAlarm" , pdirAlarm      );
		state.saveVar("pdirK"     , pdirK          );
		state.saveVar("pdirTimer" , pdirTimer      );
	}

	@Override
	public void loadStateExtra(State state) {
		this.state = state.loadVar("state"   	 , this.state     );
		stateidx   = state.loadVar("stateidx" 	 , stateidx       );
        waitCl     = state.loadVar("waitCl"      , waitCl         );
        waitOp     = state.loadVar("waitOp"      , waitOp         );
        waitEx     = state.loadVar("waitEx"      , waitEx         );
        alarm      = state.loadVar("alarm"       , alarm          );
        timer      = state.loadVar("timer"       , timer          );
        openK1     = state.loadVar("openK1"      , openK1         );
        openK2     = state.loadVar("openK2"      , openK2         );
        openK3     = state.loadVar("openK3"      , openK3         );
        openK4     = state.loadVar("openK4"      , openK4         );
        openK5     = state.loadVar("openK5"      , openK5         );
        openK6     = state.loadVar("openK6"      , openK6         );
        fullK6     = state.loadVar("fullK6"      , fullK6         );
        openK7     = state.loadVar("openK7"      , openK7         );
        openK8     = state.loadVar("openK8"      , openK8         );
		openK9     = state.loadVar("openK9"      , openK9         );
		pdirValue  = state.loadVar("pdirValue"   , pdirValue      );
		pdirTimer  = state.loadVar("pdirTimer"   , pdirTimer      );
		pdirAlarm  = state.loadVar("pdirAlarm"   , pdirAlarm      );
		pdirK      = state.loadVar("pdirK"       , pdirK          );
	}

}