package promauto.jroboplc.plugin.roboplant;

import java.util.Map;

import promauto.jroboplc.core.State;
import promauto.jroboplc.core.api.Tag;

public class DeviceBTN extends Device {
	Tag 	tagOutput ;
	Tag 	tagInput  ;
	Tag 	tagTimeDel;
	Tag 	tagTimeImp;
	int 	cnt;
	boolean output;
	
    @Override
	public void prepareTags(RefBool res) {
    	tagOutput    	= getOutputTag(		"Output"	, res);
    	tagInput     	= getOutputTag(		"Input"		, res);
    	tagTimeDel   	= getOutputTag(		"TimeDel"	, res);
    	tagTimeImp   	= getOutputTag(		"TimeImp"	, res);
    	
//		if (res.value)
//			resetState();
		cnt = 0;
		output = false;
	}

//	protected void resetState() {
//		tagInput.setInt(0);
//		tagOutput.setInt(0);
//		cnt = 0;
//		output = false;
//	}
    
	@Override
	public boolean execute() {

		if (tagInput.getBool()) {
			if (output) {
				if (cnt++ == tagTimeImp.getInt()) {
					output = false;
					cnt = 0;
					tagInput.setInt(0);
				}
			} else {
				if (cnt++ == tagTimeDel.getInt()) {
					output = true;
					cnt = 0;
				}
			}
		} else {
			output = false;
			cnt = 0;
		}

		tagOutput.setBool(output);

		return true;
	}

	
	@Override
	public void saveStateExtra(State state) {
		state.saveVar("cnt", 		cnt);
		state.saveVar("output", 	output);
	}
	
	@Override
	public void loadStateExtra(State state) {
		cnt    = state.loadVar("cnt", 		cnt);
		output = state.loadVar("output", 	output);
	}

}