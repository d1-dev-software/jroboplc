package promauto.jroboplc.plugin.roboplant;

import promauto.jroboplc.core.State;
import promauto.jroboplc.core.api.Tag;

import java.util.*;

public class DeviceFRQA extends Device {

	Input inpInput;
	Input inpEnable;

	Tag tagFreq;
	Tag tagLow;
	Tag tagNormal;
	Tag tagHigh;
	Tag tagPeriod;
	Tag tagMul;
	Tag tagDiv;
	Tag tagFreqLow;
	Tag tagFreqHigh;


	boolean lastInputValue;
	Deque<Long> values = new LinkedList<>();


    @Override
	public void prepareTags(RefBool res) {

		inpInput  	= getInput("Input"  	, res);
		inpEnable  	= getInput("Enable"  	, res);

		tagFreq 	= getOutputTag("Freq"		, res);
		tagLow 		= getOutputTag("Low"		, res);
		tagNormal 	= getOutputTag("Normal"	, res);
		tagHigh 	= getOutputTag("High"		, res);
		tagPeriod 	= getOutputTag("Period"	, res);
		tagMul 		= getOutputTag("Mul"		, res);
		tagDiv 		= getOutputTag("Div"		, res);
		tagFreqLow 	= getOutputTag("FreqLow"	, res);
		tagFreqHigh = getOutputTag("FreqHigh"	, res);

//		if (res.value)
//			resetState();

		lastInputValue = false;

	}


//	protected void resetState() {
//        tagFreq.setInt(0);
//        lastInputValue = false;
//	}
	
	
	@Override
	public boolean execute() {
    	if( inpEnable.getInt() == 0 ) {
    		tagFreq.setInt(0);
			tagLow.setInt(0);
			tagHigh.setInt(0);
			tagNormal.setInt(0);
			return true;
		}

    	long timeend = getCurrentTime();
    	long timebeg = timeend - tagPeriod.getInt();

    	int sizeold = values.size();

		while( values.size() > 0  &&  values.getFirst() <= timebeg ) {
			values.removeFirst();
		}

		while( values.size() > 0  &&  values.getLast() >= timeend ) {
			values.removeLast();
		}

		boolean inputValue = inpInput.tag.getBool();
		if( inputValue  &&  !lastInputValue ) {
			values.add(timeend);
		}
		lastInputValue = inputValue;

		int freq = tagFreq.getInt();
		if( values.size() == 0 ) {
			freq = 0;
		} else if( values.size() != sizeold ) {
			freq = values.size();

			if( tagMul.getInt() > 0 )
				freq *= tagMul.getInt();

			if( tagDiv.getInt() > 0 )
				freq = (int)Math.round( (double)freq / tagDiv.getDouble() );
		}
		tagFreq.setInt(freq);

		boolean low = freq <= tagFreqLow.getInt();
		boolean high = freq >= tagFreqHigh.getInt();
		tagLow.setBool( low );
		tagHigh.setBool( high );
		tagNormal.setBool( !low  &&  !high );

		return true;
	}

	protected long getCurrentTime() {
		return System.currentTimeMillis();
	}


	@Override
	public void saveStateExtra(State state) {
		state.saveVar("lastInputValue",  lastInputValue );

		int size = values.size();
		state.saveVar("size", size );

		List<Long> a = new ArrayList<>(values);
		for(int i=0; i<size; ++i) {
			state.saveVar("value" + i, a.get(i));
		}
	}

	@Override
	public void loadStateExtra(State state) {
		lastInputValue = state.loadVar("lastInputValue", lastInputValue );

		int size = state.loadVar("size", 0 );

		values.clear();
		for(int i=0; i<size; ++i) {
			long value = state.loadVar("value" + i, 0L );
			values.addLast(value);
		}
	}

}