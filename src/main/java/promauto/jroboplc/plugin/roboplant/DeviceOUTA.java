package promauto.jroboplc.plugin.roboplant;

import promauto.jroboplc.core.api.Tag;

import java.time.LocalDateTime;
import java.util.Map;

public class DeviceOUTA extends Device {

	protected Input inpInput;
	protected Channel channel;


    @Override
	public void prepareTags(RefBool res) {
		inpInput = getInput("Input", res);
		channel	 = createChannel("Channel", Channel.Type.Out, res);
	}


	@Override
	public boolean execute() {
		if( channel.isOk()  &&  !inpInput.isEmpty() ) {
			channel.tagValue.setInt( inpInput.getInt() );
		}
		return true;
	}



}