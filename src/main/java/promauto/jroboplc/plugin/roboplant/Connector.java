package promauto.jroboplc.plugin.roboplant;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import promauto.jroboplc.core.api.Module;
import promauto.jroboplc.core.api.Tag;

public class Connector {
	private static final int BIND_STATUS_OK = 0;
	private static final int BIND_MODULE_NOT_FOUND = -1;


	public static class Link {
		Tag tagOwn;
		Tag tagBind;
		String tagnameBind;
		Mode mode;
	}
	
	public enum Mode {
		READ,
		WRITE,
		MANUAL
	}
	
	private final Device device;
	private final Module bindmod;
	private final List<Link> links = new LinkedList<>();
	private final Map<Tag, Link> linksMap = new HashMap<>();

	
	private Tag tagBindStatus;
	
	

	
	
	public Connector(Device _device) {
		RoboplantPlugin plugin = (RoboplantPlugin)_device.module.getPlugin();

		device = _device;
		bindmod = plugin.getConnectManager().getConnectedModule(_device.module, _device);

		String bindtagname = _device.devtype + "_" + _device.tagname + "_" + "BindStatus";
		tagBindStatus = device.module.getTagTable().get( bindtagname );
		if( tagBindStatus == null)
			tagBindStatus = device.module.getTagTable().createInt( bindtagname, 0);
		else
			tagBindStatus.setInt(BIND_STATUS_OK);
		
		if( bindmod == null) {
			if( plugin.getConnectManager().isNotConnected(_device.module, _device) )
				tagBindStatus.setInt(BIND_STATUS_OK);
			else
				tagBindStatus.setInt(BIND_MODULE_NOT_FOUND);
		}
	}


	public Module getBindmod() {
		return bindmod;
	}


	public boolean connect(String _deviceOutputName, String _tagnameBind, Mode _mode, Device.RefBool _res) {
		if( device == null  ||  bindmod == null )
			return false;
		
		Tag tagOwn = device.getOutputTag(_deviceOutputName, _res);
		if( tagOwn == null )
			return false;
		
		Link link = new Link();
		link.tagOwn = tagOwn;
		link.tagBind = bindmod.getTagTable().get(_tagnameBind);
		link.tagnameBind = _tagnameBind;
		link.mode = _mode;
		links.add(link);
		linksMap.put(tagOwn, link);
		
		return true;
	}


	public Tag getTagBind(Tag tag) {
		Link link = linksMap.get(tag);
		if( link != null )
			return link.tagBind;
		return null;
	}
	
	
	public boolean execute() {
		if( links.size() == 0 )
			return true;
		
		int errcnt = 0;
		for(Link link: links) {
			if( link.tagBind == null  ||  link.tagBind.getStatus() == Tag.Status.Deleted ) {
				link.tagBind = bindmod.getTagTable().get(link.tagnameBind);
				if( link.tagBind == null)
					++errcnt;
			} else {
				if( !link.tagBind.equalsValue( link.tagOwn ) ) {
					if( link.mode == Mode.READ )
						link.tagBind.copyValueTo(link.tagOwn);
					else
					if( link.mode == Mode.WRITE )
						link.tagOwn.copyValueTo(link.tagBind);
				}
			}
		}
		
		if( tagBindStatus.getInt() != errcnt )
			tagBindStatus.setInt( errcnt );
		
		return true;
	}


	public String checkErrors() {
		if( tagBindStatus.getInt() == BIND_STATUS_OK )
			return "";
					
		if( tagBindStatus.getInt() == BIND_MODULE_NOT_FOUND  ||  bindmod == null)
			return "Connection module not found";
		
		StringBuilder res = new StringBuilder();
		for(Link link: links) {
			if (link.tagBind == null || link.tagBind.getStatus() == Tag.Status.Deleted) {
				res.append("\r\n    ")
						.append(link.tagOwn.getName())
						.append(link.mode == Mode.READ ? " <- " : "")
						.append(link.mode == Mode.WRITE ? " -> " : "")
						.append(link.mode == Mode.MANUAL ? " -- " : "")
						.append(bindmod.getName())
						.append(":")
						.append(link.tagnameBind);
			}
		}
		return res.toString();
	}


}




























