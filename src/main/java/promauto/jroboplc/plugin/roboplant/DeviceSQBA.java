package promauto.jroboplc.plugin.roboplant;

import java.util.Map;

import promauto.jroboplc.core.State;
import promauto.jroboplc.core.api.Tag;

public class DeviceSQBA extends Device {
	Input 	inpCntOn	;
	Input 	inpCntOff	;
	Tag 	tagOutput	;
	Tag 	tagValOn	;
	Tag 	tagValOff	;
	
	boolean output;
	boolean trigger;
	
    @Override
	public void prepareTags(RefBool res) {

    	inpCntOn	= getInput( "CnrOn", res);
    	inpCntOff 	= getInput( "CntOff", res);
    	
    	tagOutput	 = getOutputTag( "Output"		, res);
    	tagValOn	 = getOutputTag( "ValOn"		, res);
    	tagValOff	 = getOutputTag( "ValOff"		, res);

//		if (res.value)
//			resetState();
		output = false;
		trigger = false;
	}


//	protected void resetState() {
//		tagOutput.setInt(0);
//		output = false;
//		trigger = false;
//	}

	@Override
	public boolean execute() {

		int cnton = inpCntOn.getInt();
		int cntoff = inpCntOff.getInt();

		if (!trigger)
			if ((cnton == 0) && (cntoff != 0) && (output))
				trigger = true;

		if (trigger)
			if ((tagValOff.getInt() <= cntoff) && (cntoff == 0))
				trigger = false;

		if ((tagValOn.getInt() == 0) || (tagValOff.getInt() == 0))
			output = false;
		else
			output = ((tagValOn.getInt() <= cnton) && (tagValOff.getInt() > cntoff))
					|| (trigger);

		tagOutput.setBool(output);

		return true;
	}

	
	
	@Override
	public void saveStateExtra(State state) {
		state.saveVar("trigger", 	trigger);
		state.saveVar("output", 	output);
	}
	
	@Override
	public void loadStateExtra(State state) {
		trigger  = state.loadVar("trigger", 	trigger);
		output	 = state.loadVar("output", 	output);
	}

}