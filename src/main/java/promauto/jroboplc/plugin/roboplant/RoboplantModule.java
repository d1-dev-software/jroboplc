package promauto.jroboplc.plugin.roboplant;

import static javax.xml.stream.XMLStreamConstants.END_ELEMENT;
import static javax.xml.stream.XMLStreamConstants.START_ELEMENT;

import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import promauto.jroboplc.core.AbstractModule;
import promauto.jroboplc.core.State;
import promauto.jroboplc.core.api.Configuration;
import promauto.jroboplc.core.api.Flags;
import promauto.jroboplc.core.api.Module;
import promauto.jroboplc.core.api.Signal;

public class RoboplantModule extends AbstractModule implements Signal.Listener{

	protected String project;
	private boolean useDescr;

	private int descrFlags;

	protected Device[] devicesByAddr;
	protected Device[] devicesByOrder;

	protected int devicesByAddrCount = 0;
	

	protected ChannelManager channelManager = new ChannelManager(this);


	public RoboplantModule(RoboplantPlugin plugin, String name) {
		super(plugin, name);
	}


	public boolean isUseDescr() {
		return useDescr;
	}

	public int getDescrFlags() {
		return descrFlags;
	}


	public boolean loadModule(Object conf) {
		Configuration cm = env.getConfiguration();
		project = cm.get(conf, "project", "");

		String s = cm.get(conf, "use.descr", cm.get(conf, "useDescr", ""));
		if( s.equals("hidden") ) {
			useDescr = true;
			descrFlags = Flags.HIDDEN;
		} else {
			useDescr = Boolean.parseBoolean(s);
			descrFlags = Flags.NONE;
		}

		Path prjfile = cm.getConfDir().resolve(project);

		((RoboplantPlugin)plugin).getConnectManager().loadConnectors(this, conf);
		
//		flagAutosave = getFlagOverload(Flags.AUTOSAVE);
//		flagHidden = getFlagOverload(Flags.HIDDEN);
		
		boolean res = false;
		if (loadProject(prjfile))
			if (initDevices())
				res = true;

		return res;
	}



	
	

	protected boolean loadProject(Path prjfile) {
		boolean result = true;
		List<Device> devlist = new LinkedList<>();

		XMLInputFactory xmlif = env.getXMLInputFactory();
		XMLStreamReader xmlr = null;

		//TODO if file not exists?
		
		try (InputStream prjstream = Files.newInputStream(prjfile)) {
			xmlr = xmlif.createXMLStreamReader(prjstream);

			int eventType;
			String xmltag;
			Device currentDevice = null;
			while (xmlr.hasNext()) {
				eventType = xmlr.next();

				switch (eventType) {
				case START_ELEMENT:
					xmltag = xmlr.getName().getLocalPart();

					switch (xmltag) {
					case "dev":
						currentDevice = createDevice(xmlr, devlist);
						break;

					case "comp":
						createComp(xmlr, currentDevice);
						break;
					}

					break;

				case END_ELEMENT:
					if (xmlr.getName().getLocalPart().equals("dev") && (currentDevice != null)) 
						currentDevice = null;
					break;
				}
			}

		} catch (Exception e) {
			env.printError(logger, e, name, prjfile.toString(),
					xmlr==null? "": "line: " + xmlr.getLocation().getLineNumber() );
			result = false;
		} finally {
			try {
				if (xmlr != null) xmlr.close();
			} catch (XMLStreamException e) {
				env.printError(logger, e, name, "XML stream error");
				result = false;
			}
		}
		
		Collections.sort(devlist);
		devicesByOrder = devlist.toArray( new Device[0] );
		copyToDevicesByAddr();

		return result;
	}

	
	
	private Device createDevice(XMLStreamReader xmlr, List<Device> devlist)
			throws InstantiationException, IllegalAccessException, ClassNotFoundException {

		String devtype = xmlr.getAttributeValue("", "DevType");
		String className = Device.class.getName() + devtype;

		Device device = (Device) Class.forName(className).newInstance();
		devlist.add(device);
		
		int n = xmlr.getAttributeCount();
		for (int i=0; i<n; i++)
			device.setAttribute(
					xmlr.getAttributeName(i).toString(), 
					xmlr.getAttributeValue(i));

		return device;
	}

	
	
	private void createComp(XMLStreamReader xmlr, Device device) {
		if (device==null) return;
		
		boolean isInp = (xmlr.getAttributeValue("", "IsInp") != null);
		boolean isVar = (xmlr.getAttributeValue("", "IsVar") != null);
		
		if (isInp || isVar) {

			Comp comp = device.addComp(isInp);

			int n = xmlr.getAttributeCount();
			for (int i = 0; i < n; i++)
				comp.setAttribute(xmlr.getAttributeName(i).toString(),
						xmlr.getAttributeValue(i));
		}
	}


	
	
	private void copyToDevicesByAddr() {
		int max = -1;
		for (Device dev: devicesByOrder)
			if (max < dev.addr)
				max = dev.addr;
		
		devicesByAddr = new Device[max + 1];
		for (int i=0; i<max; i++)
			devicesByAddr[i] = null;
		
		for(Device dev: devicesByOrder)
			devicesByAddr[dev.addr] = dev;

		devicesByAddrCount = devicesByAddr.length; 
	}


	
	
	protected boolean initDevices() {
		for(Device dev: devicesByOrder)
			if (!dev.init(this))
				return false;
		return true;
	}

	
	
	public Device getDevice(int addr) {
		if ((addr >= 0)  &&  (addr < devicesByAddrCount))
			return devicesByAddr[addr];
		else
			return null;
	}

	

	
	@Override
	public boolean prepareModule() {
		if (enable && devicesByOrder!=null) {
			for(Device dev: devicesByOrder)
				if (!dev.prepare())
					return false;
		}

		addAsSignalListerToAllOthers();
//		env.getModuleManager().getModules().stream()
//				.filter( m -> m != this )
//				.forEach( m -> m.addSignalListener(this) );

		return true;
	}
	
	
	
	
	@Override
	public boolean closedownModule() {
		removeAsSignalListerFromAllOthers();
//		env.getModuleManager().getModules().stream()
//				.filter( m -> m != this )
//				.forEach( m -> m.removeSignalListener(this) );

		return true;
	}

	
	
	@Override
	public String getInfo() {
		return !enable? "disabled":
				String.format("devices=%d tags=%d prj=%s",
						devicesByOrder.length, tagtable.getSize(), project);
	}

	
	@Override
	protected boolean reload() {
		Reloader r = new Reloader(this);
		boolean res = r.reload();
		return res;
	}



	@Override
	public boolean executeModule() {
		
		channelManager.execute();
		
		boolean res = true;
		if (enable)
			for(Device dev: devicesByOrder)
				res &= dev.execute(); 
		
		return res;
	}




	public ChannelManager getChannelManager() {
		return channelManager ;
	}
	
	
	
	@Override
	public void loadState(State state) {
//		Map<String,Integer> stateconv;
//		try {
//			stateconv = state.entrySet().stream()
//					.collect(Collectors.toMap(
//							ent -> ent.getKey(),
//							ent -> Integer.parseInt( ent.getValue() ) ));
//		} catch (NumberFormatException e ) {
//			env.printError(logger, e, name);
//			return false;
//		}
//
//		for(Device d: devicesByOrder)
//			d.loadState(stateconv);

		for(Device d: devicesByOrder)
			d.loadState(state);
	}
	
	
	@Override
	public void saveState(State state) {
		for(Device d: devicesByOrder)
			d.saveState(state);

//		Map<String,Integer> state = new HashMap<>();
//		for(Device d: devicesByOrder)
//			d.saveState(state);
//
//		Map<String,String> stateconv = state.entrySet().stream()
//			.collect(Collectors.toMap(
//                    ent -> ent.getKey(),
//					ent -> ent.getValue().toString(),
//					(a,b) -> a,
//					TreeMap::new ));
//
//		return stateconv;
	}


	
	@Override
	public String check() { 
		String res = "";
		for(Device dev: devicesByOrder) {
			String check = dev.checkErrors();
			if( check.isEmpty() )
				continue;
			
			res += dev.devtype + '.' + dev.tagname + ": " + check + "\r\n";
		}
		
		return res;
	}


	@Override
	public void onSignal(Module sender, Signal signal) {

		if( signal.type == Signal.SignalType.RELOADED )
			channelManager.refreshErrorForeignChannels();

	}
}