package promauto.jroboplc.plugin.roboplant;

import promauto.jroboplc.core.State;

import java.util.Map;

/**
 * @author Denis Lobanov
 */
public class DeviceALM extends DeviceSimpleLogic {
	Input inpReset;
	int[] values;
	int inpAmount;
	
	
    @Override
	public void prepareTags(RefBool res) {
		super.prepareTags(res);
		inpReset = getInput( "Reset", res);
		inpAmount = inpInput.size();
		values = new int[ inpAmount ];
	}

	protected void resetState() {
		super.resetState();
		for (int i=0; i<inpAmount; i++)
			values[i] = 0;
	}



	@Override
	public boolean execute() {

		if (inpAmount == 0)
			return true;

		int output = 0;
		int value;

		for (int i = 0; i < inpAmount; i++) {
			value = inpInput.get(i).getInt();
			if (inpReset.getInt() > 0) {
				values[i] = value;
			} else {
				if ((value > 0) && (values[i] == 0))
					output = 1;
				if ((value == 0) && (values[i] > 0))
					values[i] = 0;
			}
		}

		tagOutput.setInt(output);

		return true;
	}

	
	
	@Override
	public void saveStateExtra(State state) {
		for (int i=0; i<inpAmount; i++)
			state.saveVar( inpInput.get(i).tag.getName() + "_value", values[i]);
	}

	
	
	@Override
	public void loadStateExtra(State state) {
		String s;
		for (int i=0; i<inpAmount; i++) {
			s = inpInput.get(i).tag.getName() + "_value";
			if( state.hasVar(s) )
				values[i] = state.loadVar(s, values[i]);
		}
	}
	
	
	
	

}