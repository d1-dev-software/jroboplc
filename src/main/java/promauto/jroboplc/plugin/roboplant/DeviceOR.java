package promauto.jroboplc.plugin.roboplant;

/**
 * @author Denis Lobanov
 */
public class DeviceOR extends DeviceSimpleLogic {

	@Override
	public boolean execute() {
		
		boolean output = false; 
		for (Input inp: inpInput)
			output |= (inp.getInt() > 0);
		
		tagOutput.setBool(output);
		
		return true;
	}

}