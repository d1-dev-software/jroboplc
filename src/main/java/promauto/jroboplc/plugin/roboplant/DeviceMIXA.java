package promauto.jroboplc.plugin.roboplant;

import promauto.jroboplc.core.api.Tag;

public class DeviceMIXA extends Device {

	Input inpInput;
	Input inpDosZagrRd;
	Input inpDosRazgRd;
	Input inpDosRazgruz;
	Input inpInMixer;
	Input inpMixClose;
	Input inpMixOpen;
	Input inpSeq1;
	Input inpSeq2;
	Input inpDNP;
	
	Tag tagSost;
	Tag tagForce;
	Tag tagCnt;
	Tag tagDosZagr;
	Tag tagDosRazg;
	Tag tagMixClose;
	Tag tagMixOpen;
	Tag tagMixer;
	Tag tagVibrator;
	Tag tagSeq1;
	Tag tagSeq2;
	Tag tagTimeMix;
	Tag tagTimeVibr;
	Tag tagParam1;
	Tag tagParam2;
	Tag tagParam3;
	Tag tagParam4;

	
    @Override
	public void prepareTags(RefBool res) {
    	
    	inpInput		= getInput("Input", res);
    	inpDosZagrRd	= getInput("DosZagrRd", res);
    	inpDosRazgRd	= getInput("DosRazgRd", res);
    	inpDosRazgruz	= getInput("DosRazgruz", res);
    	inpInMixer		= getInput("InMixer", res);
    	inpMixClose		= getInput("MixClose", res);
    	inpMixOpen		= getInput("MixOpen", res);
    	inpSeq1			= getInput("Seq1", res);
    	inpSeq2			= getInput("Seq2", res);
    	inpDNP			= getInput("DNP", res);
    			
    	tagSost	    	= getOutputTag(		"Sost"	    , res);
    	tagForce  		= getOutputTag(		"Force"  	, res);
    	tagCnt    	 	= getOutputTag(		"Cnt"    	, res);
    	tagDosZagr	 	= getOutputTag(		"DosZagr"	, res);
    	tagDosRazg	 	= getOutputTag(		"DosRazg"	, res);
    	tagMixClose 	= getOutputTag(		"MixClose"  , res);
    	tagMixOpen	    = getOutputTag(		"MixOpen"	, res);
    	tagMixer	    = getOutputTag(		"Mixer" 	, res);
    	tagVibrator     = getOutputTag(		"Vibrator"  , res);
    	tagSeq1    	  	= getOutputTag(		"Seq1"    	, res);
    	tagSeq2 		= getOutputTag(		"Seq2"   	, res);
    	tagTimeMix		= getOutputTag(		"TimeMix"	, res);
    	tagTimeVibr	  	= getOutputTag(		"TimeVibr"	, res); 
    	tagParam1	  	= getOutputTag(		"Param1"	, res);
    	tagParam2	  	= getOutputTag(		"Param2"	, res); 
		tagParam3	  	= getOutputTag(		"Param3"	, res); 
		tagParam4	  	= getOutputTag(		"Param4"	, res); 	
		
//		if (res.value)
//			resetState();
	}


//	protected void resetState() {
//		tagSost.setInt(0);
//		tagForce.setInt(0);
//		tagCnt.setInt(0);
//		tagDosZagr.setInt(0);
//		tagDosRazg.setInt(0);
//		tagMixClose.setInt(0);
//		tagMixOpen.setInt(0);
//		tagMixer.setInt(0);
//		tagVibrator.setInt(0);
//		tagSeq1.setInt(0);
//		tagSeq2.setInt(0);
//	}
	
	
	
	/* Warning!
	 * Code of this method was ported from C++ original RoboplantMtr sources. 
	 * Needs get refactored! Do not repeat this style!
	 * 
	 * 
	 * 
	 *                 !!!!!!! NOT TESTED !!!!!!!
	 *                 
	 *                 
	 */
	@Override
	public boolean execute() {
		int Force 	 = tagForce		.getInt();
		int Sost 	 = tagSost		.getInt();
		int DosZagr  = tagDosZagr 	.getInt();
		int DosRazg  = tagDosRazg 	.getInt();
		int MixClose = tagMixClose	.getInt();
		int MixOpen  = tagMixOpen 	.getInt();
		int Mixer    = tagMixer   	.getInt();
		int Vibrator = tagVibrator	.getInt();
		int Seq1     = tagSeq1    	.getInt();
		int Seq2     = tagSeq2    	.getInt();
		int Cnt      = tagCnt    	.getInt();
		int TimeMix  = tagTimeMix	.getInt();
		int TimeVibr = tagTimeVibr	.getInt();
		
		if( Force > 0  &&  Force <= 10 ) {
			Sost = Force;
			Force = 0;
		}
		
		if( inpInput.getInt() <= 0 )
			Sost = 0;
		
		// Выключен и готов к работе
		if( Sost == 0 ) {
			if( inpInput.getInt() > 0 ) {
				Sost = 1;
			} else {
				DosZagr = 0;
				DosRazg = 0;
				MixClose = 0;
				MixOpen = 0;
				Mixer = 0;
				Vibrator = 0;
				Seq1 = 0;
				Seq2 = 0;
			}
		}
		
		// Ожидание готовности к загрузке дозаторов
		if (Sost == 1) {
			if( inpDosZagrRd.getInt() > 0  || inpDosRazgRd.getInt() > 0  ||  DosZagr > 0) {
				Sost = 2;
			} else {
				DosZagr = 0;
				DosRazg = 0;
				MixClose = 0;
				MixOpen = 0;
				Vibrator = 0;
				Seq1 = 0;
			}
		}
		
		// Подача команды загрузки на дозаторы. Ожидание готовности к разгрузке
		if (Sost == 2) {
			if( inpDosRazgRd.getInt() > 0 ) {
				Sost = 3;
			} else {
				DosZagr = 1;
				DosRazg = 0;
				MixClose = 0;
				MixOpen = 0;
				Vibrator = 0;
				Seq1 = 0;
			}
		}
		
		// Закрытие заслонки 310. Включение смесителя 305. Ожидание выполнения
		if (Sost == 3) {
			if( inpInMixer.getInt() > 0  &&  inpMixClose.getInt() > 0 ) {
				Sost = 4;
			} else {
				DosZagr = 0;
				DosRazg = 0;
				MixClose = 1;
				MixOpen = 0;
				Mixer = 1;
				Vibrator = 0;
				Seq1 = 0;
			}
		}
		
		// Включение цепочки оборудования №1. Ожидание выполнения
		if (Sost == 4) {
			if( inpSeq1.getInt() > 0) {
				Sost = 5;
			} else {
				DosZagr = 0;
				DosRazg = 0;
				MixClose = 1;
				MixOpen = 0;
				Mixer = 1;
				Vibrator = 0;
				Seq1 = 1;
			}
		}
		
		// Подача команды разгрузки на дозаторы. Если какое-либо устройство
		// цепочки останавливается, разрешение разгрузки снимается. Ожидание окончания
		// разгрузки.
		if (Sost == 5) {
			if( inpDosRazgRd.getInt() == 0  &&  inpDosRazgruz.getInt() == 0 ) {
				Sost = 6;
				Cnt = TimeMix;
			} else {
				DosZagr = 0;
				DosRazg = 1;
				MixClose = 1;
				MixOpen = 0;
				Mixer = 1;
				Vibrator = 0;
				Seq1 = (inpSeq1.getInt() > 0  &&  inpInMixer.getInt() > 0  &&  inpMixClose.getInt() > 0)? 1 : 0;
			}
		}

		// Остановить цепочку. Выдержать время смешения.
		if (Sost == 6) {
			if (Cnt-- == 0) {
				Sost = 7;
			} else {
				DosZagr = (DosZagr > 0 || inpDosZagrRd.getInt() > 0)? 1 : 0;
				DosRazg = 0;
				MixClose = 1;
				MixOpen = 0;
				Mixer = 1;
				Vibrator = 0;
				Seq1 = 0;
			}
		}

		// Остановить 305. Включение цепочки 2. Ожидание выполнения
		if (Sost == 7) {
			if( inpSeq2.getInt() > 0 ) {
				Sost = 8;
			} else {
				DosZagr = (DosZagr > 0  ||  inpDosZagrRd.getInt() > 0)? 1 : 0;
				DosRazg = 0;
				MixClose = 1;
				MixOpen = 0;
				// Mixer=0;
				Vibrator = 0;
				Seq1 = 0;
				Seq2 = 1;
			}
		}
		
		// Открытие заслонки 310. Ожидание выполнения
		if (Sost == 8) {
			if( inpMixOpen.getInt() > 0) {
				Sost = 9;
				Cnt = TimeVibr;
			} else {
				// DosZagr= GetData(DosZagrRd.Addr, DosZagrRd.Num); //0;
				DosZagr = (DosZagr > 0  ||  inpDosZagrRd.getInt() > 0)? 1: 0;
				DosRazg = 0;
				MixClose = 0;
				MixOpen = 1;
				// Mixer=0;
				Vibrator = 0;
				Seq1 = 0;
				Seq2 = 1;
			}
		}

		// Включение вибратора на 5 сек. Ожидание выполнения
		if (Sost == 9) {
			if (Cnt-- == 0) {
				Sost = 10;
			} else {
				DosZagr = (DosZagr > 0  || inpDosZagrRd.getInt() > 0)? 1: 0;
				DosRazg = 0;
				MixClose = 0;
				MixOpen = 1;
				// Mixer=0;
				Vibrator = 1;
				Seq1 = 0;
				Seq2 = 1;
			}
		}

		// Ожидание ухода продукта с ДНП.
		if (Sost == 10) {
			if( inpDNP.getInt() == 0 ) {
				if( inpDosRazgRd.getInt() > 0)
					Sost = 3;
				else
					Sost = 1;
			} else {
				DosZagr = (DosZagr > 0  ||  inpDosZagrRd.getInt() > 0)? 1: 0;
				DosRazg = 0;
				MixClose = 0;
				MixOpen = 1;
				Vibrator = 0;
				Seq1 = 0;
				Seq2 = 1;
			}
		}

		settag(tagForce	  , Force 	);
		settag(tagSost	  ,	Sost 	);
		settag(tagDosZagr , DosZagr );
		settag(tagDosRazg , DosRazg );
		settag(tagMixClose, MixClose);
		settag(tagMixOpen , MixOpen );
		settag(tagMixer   , Mixer   );
		settag(tagVibrator, Vibrator);
		settag(tagSeq1    , Seq1    );
		settag(tagSeq2    , Seq2    );
		settag(tagCnt     ,	Cnt     );
		settag(tagTimeMix ,	TimeMix );
		settag(tagTimeVibr, TimeVibr);

		return true;
	}
	
	private void settag(Tag tag, int value) {
		if( tag.getInt() != value )
			tag.setInt(value);
	}

}















