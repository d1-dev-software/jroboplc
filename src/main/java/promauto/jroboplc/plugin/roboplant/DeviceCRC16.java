package promauto.jroboplc.plugin.roboplant;

import promauto.jroboplc.core.api.Tag;
import promauto.utils.CRC;

import java.util.List;
import java.util.Map;

public class DeviceCRC16 extends DeviceSimpleLogic {

	@Override
	public boolean execute() {

        int crc = CRC.getCrc16FromWordStream( inpInput.stream().map(a -> a.getInt()));
		tagOutput.setInt(crc);

		return true;
	}


}