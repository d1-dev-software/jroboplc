package promauto.jroboplc.plugin.roboplant;

import promauto.jroboplc.core.api.Tag;

public class Comp implements Comparable<Comp> {
	public Tag tag;
	public String name;
	public Tag.Type type = Tag.Type.INT;

	public void setAttribute(String attr, String value) {
		if (attr.equals("Name"))
			name = (!value.contains(" "))? value: value.replace(" ", "_");
		else

		if (attr.equals("Type"))
			type = Tag.Type.valueOf( value.toUpperCase() );
	}
	
	@Override
	public int compareTo(Comp o) {
		if (o.name==null)
			return 1;
		else
			return name.compareTo(o.name);
	}

}

