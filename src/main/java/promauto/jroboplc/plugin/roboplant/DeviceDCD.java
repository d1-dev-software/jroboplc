package promauto.jroboplc.plugin.roboplant;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import promauto.jroboplc.core.State;
import promauto.jroboplc.core.api.Tag;

public class DeviceDCD extends Device {
	Input inpInput;
	List<Tag> tagOutput = new ArrayList<>();
	
	int inputval;
	
	
    @Override
	public void prepareTags(RefBool res) {
		inpInput = getInput( "Input", res);

		tagOutput.clear(); 
    	for(Output out: outputs) 
    		if (out.name.startsWith("Output"))
    			tagOutput.add( out.tag ); 

//		if (res.value)
//			resetState();

		inputval = -1;
	}


//	protected void resetState() {
//		for (Tag tag: tagOutput)
//			tag.setInt(0);
//
//		inputval = -1;
//	}
	
	@Override
	public boolean execute() {
		
		if (inpInput.getInt() != inputval) {
			inputval = inpInput.getInt();
			for (int i=0; i<tagOutput.size(); i++)
				tagOutput.get(i).setBool( inputval >= i );
		}


		return true;
	}


	@Override
	public void saveStateExtra(State state) {
		state.saveVar("inputval", inputval);
	}
	
	@Override
	public void loadStateExtra(State state) {
		inputval = state.loadVar("inputval", inputval);
	}

}