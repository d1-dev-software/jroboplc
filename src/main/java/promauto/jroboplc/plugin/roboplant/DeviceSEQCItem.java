package promauto.jroboplc.plugin.roboplant;

import promauto.jroboplc.core.api.Tag;

public class DeviceSEQCItem {
    int state;
    int address;
    private Tag tagMasterout;
    private Tag tagSost;
    private Tag tagControl;

    public DeviceSEQCItem() {
        state = 0;
        address = -1;
        tagMasterout = null;
        tagSost = null;
        tagControl = null;
    }

    public int getMasteroutValue() {
        return (tagMasterout ==null)? 0: tagMasterout.getInt();
    }

    public int getSostValue() {
        return (tagSost ==null)? 0: tagSost.getInt();
    }

    public int getControlValue() {
        return (tagControl ==null)? 0: tagControl.getInt();
    }

    public void setControlValue(int value) {
        if (tagControl !=null)
            tagControl.setInt(value);
    }

    public void reset() {
        tagMasterout = null;
        tagSost = null;
        tagControl = null;
    }

    public void setTagMasterout(Tag tag) {
        tagMasterout = tag;
    }

    public void setTagSost(Tag tag) {
        tagSost = tag;
    }

    public void setTagControl(Tag tag) {
        tagControl = tag;
    }
}
