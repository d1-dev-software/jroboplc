package promauto.jroboplc.plugin.roboplant;

import java.util.Map;

import promauto.jroboplc.core.State;
import promauto.jroboplc.core.api.Tag;

public class DeviceDIFFA extends Device {
	Input inpInpVal	;
	Input inpSetVal	;
	
	Tag tagOutVal	;
	Tag tagInpVal	;
	Tag tagSetVal	;
	Tag tagCycleTime;
	Tag tagDlyStart	;
	Tag tagGain		;
	Tag tagMaxOutVal;
	Tag tagEnable	;
	
    int cnt;
    int startCnt;

    int outval;
    int inpval;
    int setval;

	
    @Override
	public void prepareTags(RefBool res) {

    	inpInpVal = getInput( "InpVal", res);
    	inpSetVal = getInput( "SetVal", res);
    	
    	tagOutVal	 = getOutputTag( "OutVal"		, res);
    	tagInpVal	 = getOutputTag( "InpVal"		, res);
    	tagSetVal	 = getOutputTag( "InpSet"		, res);
    	tagCycleTime = getOutputTag( "CycleTime"	, res);
    	tagDlyStart	 = getOutputTag( "DlyStart"		, res);
    	tagGain		 = getOutputTag( "Gain"			, res);
    	tagMaxOutVal = getOutputTag( "maxOutVal"	, res);
    	tagEnable	 = getOutputTag( "Enable"		, res);

//		if (res.value)
//			resetState();

		cnt = 0;
		startCnt = 0;
		outval = 0;
		inpval = 0;
		setval = 0;
	}


//	protected void resetState() {
//		tagOutVal.setInt(0);
//		cnt = 0;
//		startCnt = 0;
//		outval = 0;
//		inpval = 0;
//		setval = 0;
//	}
	
	@Override
	public boolean execute() {

		if (inpInpVal.refaddr != 0x7000) {
			inpval = inpInpVal.getInt();
			tagInpVal.setInt(inpval);
		} else
			inpval = tagInpVal.getInt();

		if (inpSetVal.refaddr != 0x7000) {
			setval = inpSetVal.getInt();
			tagSetVal.setInt(setval);
		} else
			setval = tagSetVal.getInt();

		if ((inpval == 0) || (!tagEnable.getBool())) {
			outval = setval;
			startCnt = 0;
		} else

		if (startCnt < tagDlyStart.getInt()) {
			startCnt++;
			outval = setval;
			cnt = 0;
		} else

		if ((++cnt) >= tagCycleTime.getInt()) {
			cnt = 0;

			float r = (float) outval
					+ (((float) setval - (float) inpval) * (float) tagGain
							.getInt()) / 100;

			if (r < 0)
				r = 0;

			if (r > 0xFFFF)
				r = 0xFFFF;

			if (tagMaxOutVal.getInt() > 0)
				if (r > tagMaxOutVal.getInt())
					r = tagMaxOutVal.getInt();

			outval = Math.round(r);
		}

		tagOutVal.setInt(outval);

		return true;
	}


	@Override
	public void saveStateExtra(State state) {
		state.saveVar("cnt", 		cnt);
		state.saveVar("startCnt", startCnt);
		state.saveVar("outval", 	outval);
		state.saveVar("inpval", 	inpval);
		state.saveVar("setval", 	setval);
	}

	
	
	@Override
	public void loadStateExtra(State state) {
		cnt    		= state.loadVar("cnt", 		cnt);
		startCnt 	= state.loadVar("startCnt", startCnt);
		outval	 	= state.loadVar("outval", 	outval);
		inpval 		= state.loadVar("inpval", 	inpval);
		setval 		= state.loadVar("setval", 	setval);
	}

	
}