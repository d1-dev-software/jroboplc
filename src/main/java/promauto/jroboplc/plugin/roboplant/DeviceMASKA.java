package promauto.jroboplc.plugin.roboplant;

import promauto.jroboplc.core.api.Tag;

import java.util.ArrayList;
import java.util.List;

public class DeviceMASKA extends Device {
    List<Input> inpInput 	= new ArrayList<>();
    List<Tag> 	tagValue 	= new ArrayList<>();

    Tag 		tagVal;
    Tag 		tagEnable;



    @Override
    public void prepareTags(RefBool res) {

        tagVal = getOutputTag( "Value", res);
        tagEnable = getOutputTag( "Enable", res);

        inpInput.clear();
        tagValue.clear();

        Input  inp;
        Output val;
        int i=0;
        while (true) {
            inp = getInput( "Input"+i );
            val = getOutput("Value"+i);
            if (inp==null || val==null) break;

            inpInput.add( inp );
            tagValue.add( val.tag );
            i++;
        }

//        if (res.value)
//            resetState();
    }

//    protected void resetState() {
//        tagEnable.setInt(0);
//        for(Tag tag: tagValue)
//            tag.setInt(0);
//    }


    @Override
    public boolean execute() {

        boolean en = false;

        for (int i=0; i<inpInput.size(); i++) {
            boolean val = (inpInput.get(i).getInt() & tagVal.getInt()) > 0;
            tagValue.get(i).setBool( val );
            en |= val;
        }

        tagEnable.setBool(en);

        return true;
    }

}