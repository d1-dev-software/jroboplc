package promauto.jroboplc.plugin.roboplant;

import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import promauto.jroboplc.core.State;
import promauto.jroboplc.core.api.EnvironmentInst;
import promauto.jroboplc.core.api.Flags;
import promauto.jroboplc.core.api.Tag;
import promauto.jroboplc.core.tags.TagPlain;

public class Device implements Comparable<Device> {
	protected final Logger logger = LoggerFactory.getLogger(Device.class);

	private static CharsetEncoder asciiEncoder =
			Charset.forName("US-ASCII").newEncoder(); // or "ISO-8859-1" for ISO Latin 1

	public static class RefBool {
		public boolean value;
		
		public RefBool(boolean value) {
			this.value = value;
		}
	}


	RoboplantModule module;
	
	public String devtype;
	public String tagname; 
	public String name; 
	public int addr; 
	public int order;
	
	protected List<Input> inputs = new LinkedList<>();
	protected List<Output> outputs = new LinkedList<>();
	protected List<Output> outputsByNum = new LinkedList<>();

	
	

	public Input getInput(String searchName) {
		int i = Collections.binarySearch(inputs, new Input(searchName)); 
		return (i<0)? null: inputs.get(i);
	}
	
	public Output getOutput(String searchName) {
		int i = Collections.binarySearch(outputs, new Output(searchName));
		return (i<0)? null: outputs.get(i);
	}

	public Output getOutput(int searchNum) {
		int i = Collections.binarySearch(outputsByNum, new Output(searchNum), Output.CmpNum.get());
		return (i<0)? null: outputsByNum.get(i);
	}


	
	public Input getInput(String searchName, RefBool res) {
		Input input = getInput(searchName);
		return checkres(input, res, searchName)? input: null;
	}

	public Tag getOutputTag(String searchName, RefBool res) {
		Output output = getOutput(searchName);
		return checkres(output, res, searchName)? output.tag: null;
	}

	public Tag getOutputTag(int searchNum, RefBool res) {
		Output output = getOutput(searchNum);
		return checkres(output, res, searchNum)? output.tag: null;
	}

	public Tag getOutputTagOrFake(String searchName) {
		Output output = getOutput(searchName);
		return output == null? TagPlain.create(Tag.Type.INT, searchName): output.tag;
	}

	
	
	
	public boolean checkres(Comp comp, RefBool res, Object obj) {
		if (comp == null) {
			if (res.value) {
				EnvironmentInst.get().printError(logger, module.getName(),
						String.format( "DeviceBase component doesn't exist (addr.name.comp): %04X.%s.%s%n",
								addr, name, obj.toString() ));
				
				res.value = false;
			}
			return false;
		}
		return true;
	}
	


	protected void sortInputOutputArrays() {
		inputs = new ArrayList<>(inputs);
		outputs = new ArrayList<>(outputs);
		outputsByNum = new ArrayList<>(outputs);

		Collections.sort(inputs); // sort by name
		Collections.sort(outputs); // sort by name
		Collections.sort(outputsByNum, Output.CmpNum.get()); // sort by num
	}

	
	
	@Override
	public int compareTo(Device o) {
		return order - o.order;
	}

	
	
	public void setAttribute(String attrName, String value) {
		switch(attrName) {
		case "DevType": devtype = value; break;
		case "TagName": tagname = value; break;
		case "Name": name = value; break;
		case "Addr": addr = Integer.parseInt(value, 16); break;
		case "Order": order = Integer.parseInt(value, 16); break;
		}
	}
	
	
	public Comp addComp(boolean isInp) {
		if (isInp) {
			Input input = new Input();
			inputs.add(input);
			return input;
		} else {
			Output output = new Output();
			outputs.add(output);
			return output;
		}
	}
	
		
	public boolean init(RoboplantModule module) {
		this.module = module;
		
		correctTagname();
		
		createTagForEachOutputs();

		createTagForConstAndEmptyInputs();

		sortInputOutputArrays();
		
		return true;
	}

	


	private void correctTagname() {
		if (tagname == null)
			tagname = String.format("T%04X", addr);
		else 
			tagname = correctTagnameText( tagname );
		
		if( !asciiEncoder.canEncode(tagname) ) {
			EnvironmentInst.get().printError(logger, module.getName(),
					String.format( "None ascii tagname: '%s', device: %04X", tagname, addr) );
		}
	}

	
	private String correctTagnameText(String s) {
		if (s.contains(" "))
			s = s.replace(" ", "_");
		if (s.contains("."))
			s = s.replace(".", "_");
		if (s.contains(":"))
			s = s.replace(":", "_");
		
		return s;
	}

	
	
	
	protected void createTagForEachOutputs() {
		String s1 = devtype + "_" + tagname + "_";
		String s2, sname;
		int i;
		for (Output output: outputs)
			if( output.tag == null ) {
				i = 1;
				s2 =  correctTagnameText( s1 + output.name );
				sname = s2;
				while (module.getTagTable().get(sname) != null)
					sname = s2 + "_" + i++;

				output.tag = module.getTagTable().createTag(output.type, sname);
				if( output.tag != null ) {

					if( output.value == null )
						output.tag.setInt(output.inival);
					else
						output.tag.setString(output.value);

					if (output.saveable)
						output.tag.addFlag(Flags.AUTOSAVE);

					if (!output.istag)
						output.tag.addFlag(Flags.HIDDEN);
				}
			}
	}


	private void createTagForConstAndEmptyInputs() {
		String s = devtype + "_" + tagname + "@";
		int i = 0;
		for (Input input: inputs) { 
			if (input.isConst()) {
				input.isTagOwner = true;
				input.invertor = false;
				input.tag = module.getTagTable().createTag(input.type, s + input.name + "_" + i);
				input.tag.setInt( input.refaddr==0x7000? 0: input.refnum );
				input.tag.addFlag( Flags.HIDDEN );
			}
			++i;
		}
	}

	
	

	
	
	public boolean prepare() {
		String s = devtype + "_" + tagname + "@BADREF_";
		Output output = null;
		
		for (Input input: inputs) 
			if (!input.isConst())	{
				Device dev = module.getDevice(input.refaddr);
				output = (dev==null)? null: dev.getOutput(input.refnum);
				
				if (output==null) { 
					input.isTagOwner = true;
					input.tag = module.getTagTable().createTag(input.type,s + input.name );
				} else {
					input.isTagOwner = false;
					input.tag = output.tag;
				}
			}
		
		RefBool res = new RefBool(true);
		
		prepareTags(res);
		
		return res.value;
	}
	
	
	protected void prepareTags(RefBool res) {
//		res.value = false;
	}

	protected void registerAsChannelProvider(String outputNameAddr, Channel.Type type, RefBool res) {
		module.getChannelManager().registerChannelProvider(this, getOutputTag(outputNameAddr, res), type);
	}

	protected Channel createChannel(String outputName, Channel.Type type, RefBool res) {
		return module.getChannelManager().createChannel( getOutputTag(outputName, res), type);
	}

	
	
	
	
	public boolean execute() {
		System.out.println( devtype+  " not implemented: " +  name );
		return true;
	}





	public final void saveState(State state) {
		state.setVarPrefix( getVarPrefix() );
		for (Output out: outputs)
			state.saveTag(out.tag);
		saveStateExtra(state);
	}

	public void saveStateExtra(State state) {}


	public final void loadState(State state) {
		state.setVarPrefix( getVarPrefix() );
		for (Output out: outputs)
			state.loadTag(out.tag);
		loadStateExtra(state);
	}

	public void loadStateExtra(State state) {}


	private String getVarPrefix() {
		return "$" + devtype + "_" + tagname + "_";
	}


	public String checkErrors() {
		return "";
	}


	protected final void createTagDescr() {
		if( module.isUseDescr() ) {
			String tagnameDescr = devtype + "_" + tagname + "." + "descr";
			module.getTagTable().createRWString(tagnameDescr, name, module.getDescrFlags());
		}
	}


}
