package promauto.jroboplc.plugin.roboplant;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import promauto.jroboplc.core.State;
import promauto.jroboplc.core.api.Tag;
import promauto.jroboplc.core.tags.TagPlain;
import promauto.jroboplc.core.tags.TagPlainInt;

public class DeviceZCTA extends Device {

	public static final int MARKER_END = 0xFFFF;
	public static final int ZCTA_SIZE = 100;

	Input inpStart;
	Input inpStop;

	Tag tagSost;
	Tag tagAlarm;
	Tag tagStopFlow;
	Tag tagStopRout;
//	Tag tagFlags;
	Tag tagStepSize;
	Tag tagStepTime;
	Tag tagDlyAlarm;
	Tag tagDlyStopFlow;
	Tag tagDlyStopRout;
	Tag tagCode;
	Tag tagEnable;

	DeviceZCTAItem[] zdv;
	
    int cntAlarm;
    int cntStopFlow;
    int cntStopRout;
    int stepCnt;
    int current;
    
    int sost;
    boolean alarm;
    boolean stopFlow;
    boolean stopRout;

	

	@Override
	public void prepareTags(RefBool res) {
		inpStart    	= getInput("Start"		, res);
		inpStop     	= getInput("Stop"		, res);

		tagSost          		= getOutputTag("Sost"       	, res);
		tagAlarm         		= getOutputTag("Alarm"      	, res);
		tagStopFlow      		= getOutputTag("StopFlow"   	, res);
		tagStopRout      		= getOutputTag("StopRout"   	, res);
//		tagFlags         		= getOutputTag("Flags"      	, res);
		tagStepSize      		= getOutputTag("StepSize"   	, res);
		tagStepTime      		= getOutputTag("StepTime"   	, res);
		tagDlyAlarm      		= getOutputTag("DlyAlarm"   	, res);
		tagDlyStopFlow    		= getOutputTag("DlyStopFlow"	, res);
		tagDlyStopRout    		= getOutputTag("DlyStopRout"	, res);
		tagCode          		= getOutputTag("Code"       	, res);
		tagEnable        		= getOutputTag("Enable"     	, res);


		initZdv();
		for (int i = 0; i < ZCTA_SIZE; i++) {
			zdv[i].tagAddress = getOutputTag("Address" + i, res);
			zdv[i].tagMode = getOutputTag("Mode" + i, res);
		}

//		if (res.value)
//			resetState();
		cntAlarm    = 0;
		cntStopFlow = 0;
		cntStopRout = 0;
		stepCnt     = 0;
		current     = 0;
		sost = 0;
		alarm 	 = false;
		stopFlow = false;
		stopRout = false;
	}

	void initZdv() {
		zdv = new DeviceZCTAItem[ZCTA_SIZE];
		for (int i = 0; i < ZCTA_SIZE; i++) {
			zdv[i] = new DeviceZCTAItem();
		}
	}



//	protected void resetState() {
//		tagSost		.setInt(0);
//		tagAlarm    .setInt(0);
//		tagStopFlow .setInt(0);
//		tagStopRout .setInt(0);
//
//	    cntAlarm    = 0;
//	    cntStopFlow = 0;
//	    cntStopRout = 0;
//	    stepCnt     = 0;
//	    current     = 0;
//
//	    sost = 0;
//	    alarm 	 = false;
//	    stopFlow = false;
//	    stopRout = false;
//	}

	protected void linkZdvItems() {
		int addr;
		for (int i = 0; i < ZCTA_SIZE; i++) {
			addr = zdv[i].tagAddress.getInt();

			if (addr == MARKER_END) {
				if( current > i) {
					for (int j = i; j < current; j++) {
						setZdvControl(j, 0);
						zdv[j].address = -1;
						zdv[j].reset();
					}
					current = i;
				}
				break;
			}

			if (zdv[i].address != addr) {
				zdv[i].address = addr;
				zdv[i].reset();

				Device d = module.getDevice(addr);
				Output output;
				if (d != null) {
					if ((output = d.getOutput("Output")) != null)
						zdv[i].setOutput( output.tag );

					if ((output = d.getOutput("Control")) != null)
						zdv[i].setControl( output.tag );
				}

			}
		}
	}
	

	/*
	 * Warning! The source code of this method was ported from C++ original
	 * RoboplantMtr sources. Needs get refactored! Do not repeat this style!
	 */
	// @Override
	public boolean execute() {

		linkZdvItems();

		// инициализация переменных

		boolean _ok = false;
		boolean _alarm = false;
		boolean _stopflow = false;
		boolean _stoprout = false;

		boolean enable = tagEnable.getBool();

		int i;

		// Стоп
		if ((sost > 0) && (inpStop.getInt() > 0)) {
			sost = 0;

			for (i = 0; i < current; i++)
				setZdvControl(i, 0);
		}

		// 0 - выключено
		if (sost == 0) {

			alarm = false;

			if (enable) {
				stopFlow = true;
				stopRout = true;
			} else {
				stopFlow = false;
				stopRout = false;
			}

			if ((inpStart.getInt() > 0) && (enable) && (tagCode.getInt() > 0)) {
				cntAlarm = 0;
				cntStopFlow = 0;
				cntStopRout = 0;

				stepCnt = 0;
				current = 0;
				sost = 2;
			}
		}

		if (sost > 0) {
			// Контроль положений
			_alarm = false;
			_stopflow = false;
			_stoprout = false;
			_ok = true;

			for (i = 0; i < current; i++) {

				if (getZdvControl(i) != (zdv[i].tagMode.getInt() & 0xF))
					setZdvControl(i, zdv[i].tagMode.getInt() & 0xF);

				if (!getZdvOutput(i)) {
					if ((zdv[i].tagMode.getInt() & 0x0100) > 0)
						_alarm = true;
					if ((zdv[i].tagMode.getInt() & 0x0200) > 0)
						_stopflow = true;
					if ((zdv[i].tagMode.getInt() & 0x0400) > 0)
						_stoprout = true;
					_ok = false;
				}
			}

		}

		// 1 - контроль ОК
		if ((sost == 1) || (sost == 4)) {
			alarm = getZdvDly(_alarm, tagDlyAlarm.getInt(), cntAlarm);
			cntAlarm = getZdvDly_cnt;

			stopFlow = getZdvDly(_stopflow, tagDlyStopFlow.getInt(),
					cntStopFlow);
			cntStopFlow = getZdvDly_cnt;

			stopRout = getZdvDly(_stoprout, tagDlyStopRout.getInt(),
					cntStopRout);
			cntStopRout = getZdvDly_cnt;

			if (_ok)
				sost = 1;
			else
				sost = 4;
		}

		// 2 - установка
		if (sost == 2) {

			if (stepCnt == 0) {
				int n = current + tagStepSize.getInt();
				for (i = current; i < n; i++) {
					if ((i >= ZCTA_SIZE)
							|| (zdv[i].tagAddress.getInt() == MARKER_END)
							|| ((zdv[i].tagMode.getInt() & 0x00F0) == 0)) {
						sost = 3;
						break;
					}

					setZdvControl(i, zdv[i].tagMode.getInt() & 0xF);
				}
				current = i;

				stepCnt = tagStepTime.getInt();
			} else {
				stepCnt--;
			}

		}

		// 3 - ожидание установки после старта
		if (sost == 3) {
			if (_ok)
				sost = 1;
		}

		tagSost.setInt(sost);
		tagAlarm.setBool(alarm);
		tagStopFlow.setBool(stopFlow);
		tagStopRout.setBool(stopRout);

		return true;
	}

	int getZdvDly_cnt = 0;

	protected boolean getZdvDly(boolean v, int dly, int cnt) {
		getZdvDly_cnt = cnt;

		if (!tagEnable.getBool())
			return false;

		if (v) {
			if (getZdvDly_cnt < dly) {
				getZdvDly_cnt++;
				v = false;
			}
		} else
			getZdvDly_cnt = 0;

		return v;
	}

	// Возвращает 1, если задвижка используется и установлена правильно
	// Также возвращает 1, если задвижка не используется
	protected boolean getZdvOutput(int idx) {
//		int k = 0;
		// у ZDVA и ZDVT выход Output имеет num=0
		return (zdv[idx].getOutputValue() > 0)
				|| ((zdv[idx].tagMode.getInt() & 0xF) == 0);
	}

	// Установка положения. Номер положения в Value. Значение 0 - задвижка не
	// используется.
	protected void setZdvControl(int idx, int value) {
		int zdv_type = (zdv[idx].tagMode.getInt() & 0x00F0) >> 4;
//		int a;
		int v = 0;
		int m = 0;

		// ZDVA
		if (zdv_type == 1) {
//			a = 4;
			m = 0xFF0F;

			if (value == 1)
				v = 0x20;
			else if (value == 2)
				v = 0x30;
			else
				v = 0;

		} else

		// ZDVT
		if (zdv_type == 2) {
//			a = 2;
			m = 0x00FF;
			v = value;
			v = v << 8;
		}

		v = (zdv[idx].getControlValue() & m) + v;
		zdv[idx].setControlValue(v);

		zdv[idx].state = value;
	}

	// Установка положения. Номер положения в Value. Значение 0 - задвижка не
	// используется.
	protected int getZdvControl(int idx) {
		int zdv_type = (zdv[idx].tagMode.getInt() & 0x00F0) >> 4;
		int v = 0;

		// ZDVA
		if (zdv_type == 1) {
			v = zdv[idx].getControlValue() >> 4;
		} else

		// ZDVT
		if (zdv_type == 2) {
			v = zdv[idx].getControlValue() >> 8;
		}

		return v;
	}

	
	@Override
	public void saveStateExtra(State state) {
		state.saveVar("cntAlarm"   , cntAlarm    );
		state.saveVar("cntStopFlow", cntStopFlow );
		state.saveVar("cntStopRout", cntStopRout );
		state.saveVar("stepCnt"    , stepCnt     );
		state.saveVar("current"    , current     );
		state.saveVar("sost"       , sost        );
		state.saveVar("alarm"      , alarm       );
		state.saveVar("stopFlow"   , stopFlow    );
		state.saveVar("stopRout"   , stopRout    );
		
		for (int i=0; i<ZCTA_SIZE; i++) {
			state.saveVar("state_" + i,  zdv[i].state );
			state.saveVar("address_" + i,  zdv[i].address );
		}
	}

	@Override
	public void loadStateExtra(State state) {
	    cntAlarm    = state.loadVar("cntAlarm"   , cntAlarm    );
	    cntStopFlow = state.loadVar("cntStopFlow", cntStopFlow );
	    cntStopRout = state.loadVar("cntStopRout", cntStopRout );
	    stepCnt     = state.loadVar("stepCnt"    , stepCnt     );
	    current     = state.loadVar("current"    , current     );
	    sost        = state.loadVar("sost"       , sost        );
	    alarm       = state.loadVar("alarm"      , alarm       );
	    stopFlow    = state.loadVar("stopFlow"   , stopFlow    );
	    stopRout    = state.loadVar("stopRout"   , stopRout    );

	    for (int i=0; i<ZCTA_SIZE; i++) {
	    	zdv[i].state   = state.loadVar("state_" + i,  	zdv[i].state );
//	    	zdv[i].address = state.loadVar("address_" + i, 	zdv[i].address );
		}
	}


}