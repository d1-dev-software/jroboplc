package promauto.jroboplc.plugin.roboplant;

import java.util.Map;

import promauto.jroboplc.core.State;
import promauto.jroboplc.core.api.Tag;

public class DeviceCNT extends Device {
	Input   inpInput	;
	Input   inpReset	;
	Tag 	tagCnt		;
	Tag 	tagLimit	;
	Tag 	tagCntLimit	;
	Tag 	tagTime		;
	int 	cntTime;
	
    @Override
	public void prepareTags(RefBool res) {
    	inpInput		= getInput(			"Input"		, res);
    	inpReset		= getInput(			"Reset"		, res);
    	tagCnt		   	= getOutputTag(		"Cnt"		, res);
    	tagLimit	   	= getOutputTag(		"Limit"		, res);
    	tagCntLimit	   	= getOutputTag(		"CntLimit"	, res);
    	tagTime		   	= getOutputTag(		"Time"		, res);
    	
//		if (res.value)
//			resetState();

		cntTime = 0;
	}

//	protected void resetState() {
//		tagCnt      .setInt(0);
//		tagLimit	.setInt(0);
//		cntTime = 0;
//	}

    
	@Override
	public boolean execute() {

		int cnt = tagCnt.getInt();

		if (inpInput.getInt() > 0) {
			cntTime++;
			if (cntTime >= tagTime.getInt()) {
				cntTime = 0;
				if (cnt < tagCntLimit.getInt())
					cnt++;
			}
		}

		if (inpReset.getInt() > 0)
			cnt = 0;

		tagLimit.setBool(cnt >= tagCntLimit.getInt());
		tagCnt.setInt(cnt);

		return true;
	}

	
	@Override
	public void saveStateExtra(State state) {
		state.saveVar("cntTime", cntTime);
	}
	
	@Override
	public void loadStateExtra(State state) {
		cntTime = state.loadVar("cntTime", 	cntTime);
	}

}