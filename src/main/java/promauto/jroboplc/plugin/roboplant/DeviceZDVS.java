package promauto.jroboplc.plugin.roboplant;

import java.util.Map;

import promauto.jroboplc.core.State;
import promauto.jroboplc.core.api.Tag;

public class DeviceZDVS extends Device {
	Input inpInput;
	Input inpEnable;
	Input inpDoOpenFull;
	Input inpDoOpenMid;
	
	Tag tagOutput;
	Tag tagSost;
    Tag tagInOut;
    Tag tagControl;
    Tag tagBlok;
    Tag tagFlags;
    Channel chnlClOut	;
    Channel chnlOpOut	;
    Channel chnlMiOut	;
    Channel chnlClIn	;
    Channel chnlOpIn	;
    Channel chnlMiIn	;
    
    int cmd;
    int cur_pos;
    int last_pos;

	
	

    @Override
	public void prepareTags(RefBool res) {
		createTagDescr();

    	inpInput     	= getInput(	"Input"		, res);
    	inpEnable    	= getInput(	"Enable"	, res);
    	inpDoOpenFull	= getInput(	"DoOpenFull", res);
    	inpDoOpenMid 	= getInput(	"DoOpenMid"	, res);
    	
    	tagOutput    	= getOutputTag(		"Output"	, res);
    	tagSost      	= getOutputTag(		"Sost"		, res);
    	tagInOut     	= getOutputTag(		"InOut"		, res);
    	tagControl   	= getOutputTag(		"Control"	, res);
    	tagBlok      	= getOutputTag(		"Blok"		, res);
    	tagFlags     	= getOutputTag(		"Flags"		, res);
    	
    	chnlClOut		= createChannel(	"ChnlClOut"	, Channel.Type.Out, res);
    	chnlOpOut		= createChannel(	"ChnlOpOut"	, Channel.Type.Out, res);
    	chnlMiOut		= createChannel(	"ChnlMiOut"	, Channel.Type.Out, res);
    	chnlClIn		= createChannel(	"ChnlClIn"	, Channel.Type.In, res);
    	chnlOpIn		= createChannel(	"ChnlOpIn"	, Channel.Type.In, res);
    	chnlMiIn		= createChannel(	"ChnlMiIn"	, Channel.Type.In, res);

//		if (res.value)
//			resetState();
		cmd = 0;
		cur_pos = 0;
		last_pos = 0;
	}


//	protected void resetState() {
//		tagOutput.setInt(0);
//		tagSost.setInt(0);
//		tagInOut.setInt(0);
//
//	    cmd = 0;
//	    cur_pos = 0;
//	    last_pos = 0;
//	}
	
	
	/* Warning!
	 * Code of this method was ported from C++ original RoboplantMtr sources. 
	 * Needs get refactored! Do not repeat this style!
	 */
	@Override
	public boolean execute() {

		if (!(chnlClIn.isOk() && chnlClOut.isOk()
				&& chnlMiIn.isOk() && chnlMiOut.isOk()
				&& chnlOpIn.isOk() && chnlOpOut.isOk())) {
			tagSost.setInt(8);
			return false;
		}

		int flags = tagFlags.getInt();
		
		int control = tagControl.getInt();

		int ctrl;
		int ctrl1; 
		int ctrl2; 
		ctrl1 = control & 0xFF;
		ctrl2 = (control >> 8) & 0xFF;
		if ((ctrl1 == 0) || ((ctrl2 > 0) && ((flags & 0x40) > 0)))
			ctrl = ctrl2;
		else
			ctrl = ctrl1;

		if (ctrl > 0) {
			cmd = ctrl;
		} else {
			if (inpEnable.getInt() == 0)
				cmd = 1;
			else

			if (inpDoOpenMid.getInt() > 0)
				cmd = 4;
			else

			if (inpDoOpenFull.getInt() > 0)
				cmd = 3;
			else
				cmd = 2;
		}
		cmd = cmd - 2;

		int inOp = (chnlOpIn.tagValue.getBool() == ((flags & 0x80) > 0))? 1: 0;
		int inMi = (chnlMiIn.tagValue.getBool() == ((flags & 0x80) > 0))? 1: 0;
		int inCl = (chnlClIn.tagValue.getBool() == ((flags & 0x80) > 0))? 1: 0;
		int new_pos = 0; 

		if ((tagBlok.getBool()) && (cmd >= 0)) {
			new_pos = cmd;
		} else {
			if (inOp>0)
				new_pos = 1;
			else

			if (inMi>0)
				new_pos = 2;
			else

			if (inCl>0)
				new_pos = 0;
			else
				new_pos = 3;
		}

		if (new_pos != cur_pos) {
			last_pos = cur_pos;
			cur_pos = new_pos;
		}

		boolean outCl = false;
		boolean outMi = false;
		boolean outOp = false;

		int x = cmd * 10 + cur_pos;
		int z = x * 10 + last_pos;

		if (x == 00 || x == 11 || z == 220) {
			outCl = false;
			outOp = false;
		}

		if (x == 01 || x == 02 || x == 03 || x == 21 || z == 231 || z == 233 || z == 221 ) {
			outCl = true;
			outOp = false;
		}

		if (x == 10 || x == 12 || x == 13 || x == 20 || z == 230 || z == 232) {
			outCl = false;
			outOp = true;
		}


		if (cmd < 0) {
			outCl = false;
			outOp = false;
			outMi = false;
		}

		tagInOut.setInt(inCl + inOp*2 + inMi*4 + 
				(outCl ? 16 : 0) + (outOp ? 32 : 0) + (outMi ? 64 : 0));

		tagOutput.setBool( (!outCl) && (!outOp) && inpEnable.getInt() > 0  && inpInput.getInt() > 0);
			
		
		int sost = tagSost.getInt();

		if( inCl + inOp > 1 ) 
			sost = 7;
		else {
			if( cur_pos == 2  &&  last_pos == 1 ) {
				sost = 5;
			} else
				
			if (cur_pos == 3) {
				if (last_pos == 0)
					sost = 4;
				else

				if (last_pos == 1)
					sost = 5;
				else

				if (last_pos == 2 && (cmd == 0 || cmd == 2))
					sost = 4;
				else

				if (last_pos == 2 && cmd == 1)
					sost = 5;
				else
					sost = 3;
			} else
				sost = cur_pos;
		}

		tagSost.setInt(sost);

		chnlClOut.tagValue.setBool(outCl);
		chnlMiOut.tagValue.setBool(outMi);
		chnlOpOut.tagValue.setBool(outOp);

		return true;
	}
	
	
	@Override
	public void saveStateExtra(State state) {
		state.saveVar("cmd"     , cmd      );
		state.saveVar("cur_pos" , cur_pos  );
		state.saveVar("last_pos", last_pos );
	}

	@Override
	public void loadStateExtra(State state) {
	    cmd      = state.loadVar("cmd"     , cmd      );
	    cur_pos  = state.loadVar("cur_pos" , cur_pos  );
	    last_pos = state.loadVar("last_pos", last_pos );
	}


}