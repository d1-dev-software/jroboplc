package promauto.jroboplc.plugin.roboplant;

public class DeviceTRM200 extends ConnectDevice {
	
    @Override
	public void prepareTags(RefBool res) {

   		connector.connect( "STAT", "LvoP.STAT", Connector.Mode.READ, res);
   		connector.connect( "PV1", "LvoP.PV1", Connector.Mode.READ, res);
   		connector.connect( "PV2", "LvoP.PV2", Connector.Mode.READ, res);
   		connector.connect( "LUPV1", "LvoP.LUPV1", Connector.Mode.READ, res);
   		connector.connect( "LUPV2", "LvoP.LUPV2", Connector.Mode.READ, res);
    	
//    	connector.connect("ErrorFlag", "error", Connector.Mode.READ, res);
//    	connector.connect("ErrorCounter", "errorcnt", Connector.Mode.READ, res);
		connector.connect("ErrorFlag", "SYSTEM.ErrorFlag", Connector.Mode.READ, res);
		connector.connect("ErrorCounter", "SYSTEM.ErrorCount", Connector.Mode.READ, res);

    	
    	registerAsChannelProvider( "DipNetAddress", Channel.Type.In, res);
    }

    
}