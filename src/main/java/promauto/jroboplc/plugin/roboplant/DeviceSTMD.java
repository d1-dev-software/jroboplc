package promauto.jroboplc.plugin.roboplant;

import promauto.jroboplc.core.State;
import promauto.jroboplc.core.api.Tag;

import java.util.Map;

public class DeviceSTMD extends Device {

	protected static final int STATE_OFF 			     = 0;
	protected static final int STATE_START_DLY	       	 = 1;
	protected static final int STATE_BEFORE_LOAD         = 2;
	protected static final int STATE_LOAD_OPEN           = 3;
	protected static final int STATE_LOAD_VIBRATE        = 4;
	protected static final int STATE_LOAD_DLY            = 5;
	protected static final int STATE_LOAD_FINISH         = 6;
	protected static final int STATE_BEFORE_INFLATE      = 7;
	protected static final int STATE_INFLATE_WARM        = 8;
	protected static final int STATE_INFLATE_BLOW1       = 9;
	protected static final int STATE_INFLATE_BLOW2       = 10;
	protected static final int STATE_INFLATE             = 11;
	protected static final int STATE_INFLATE_FINISH      = 12;
	protected static final int STATE_EXPOSITION          = 13;
	protected static final int STATE_DEFLATE1            = 14;
	protected static final int STATE_DEFLATE2            = 15;
	protected static final int STATE_UNLOAD_OPEN         = 16;
	protected static final int STATE_UNLOAD_DLY          = 17;
	protected static final int STATE_UNLOAD_EMPTY        = 18;
	protected static final int STATE_UNLOAD_VIBRATE      = 19;
	protected static final int STATE_UNLOAD_FINISH       = 20;
	protected static final int STATE_CYCLE_PAUSE         = 21;

	protected static final int WAIT_TIMER                = 1<<0;
	protected static final int WAIT_DVU1                 = 1<<1;
	protected static final int WAIT_DVU2                 = 1<<2;
	protected static final int WAIT_CAN_INFL             = 1<<3;
	protected static final int WAIT_PSENSOR_OK           = 1<<4;
	protected static final int WAIT_PLINE_OK             = 1<<5;
	protected static final int WAIT_PAIR_OK              = 1<<6;
	protected static final int WAIT_ENERGY_OK            = 1<<7;
	protected static final int WAIT_PWORK                = 1<<8;
	protected static final int WAIT_PLOAD                = 1<<9;
	protected static final int WAIT_PUNLOAD              = 1<<10;
	protected static final int WAIT_PBLOW                = 1<<11;
	protected static final int WAIT_PFAN                 = 1<<12;
	protected static final int WAIT_TIMEFAN              = 1<<13;

	private static final int NCL = 1; // not closed
	private static final int CLS = 2; // closed
	private static final int NOP = 3; // not opened
	private static final int OPN = 4; // opened

	private static final int ALARM_PALARM 			= 1;
	private static final int ALARM_INFLATE_TIMEOUT 	= 2;
	private static final int ALARM_DEFLATE_TIMEOUT 	= 3;
	private static final int ALARM_UNLOAD_TIMEOUT 	= 4;

	
	Input inpEnable;
	Input inpSuspend;
	Input inpCanInfl;
	Input inpP;
	Input inpPEmrg;
	Input inpPSensorOk;
	Input inpPLineOk;
	Input inpPAirOk;
	Input inpEnergyOk;
	Input inpDVU1;
	Input inpDVU2;
	Input inpClosedK1;
	Input inpClosedK2;
	Input inpClosedK3;
	Input inpClosedK4;
	Input inpClosedK5;
	Input inpClosedK6;
	Input inpOpenedK1;
	Input inpOpenedK2;

	Tag tagState;
	Tag tagWaitCl;
	Tag tagWaitOp;
	Tag tagWaitEx;
	Tag tagAlarm;
	Tag tagTimer;
	Tag tagCycleNum;
	Tag tagOpenK1;
	Tag tagOpenK2;
	Tag tagOpenK3;
	Tag tagOpenK4;
	Tag tagOpenK5;
	Tag tagOpenK6;
	Tag tagStopFan;
	Tag tagVibr;


	Tag tagBtnStart;
	Tag tagBtnStop;
	Tag tagNextState;
	Tag tagFlags;
	Tag tagCycleQnt;
	Tag tagPAlarm;
	Tag tagPWork;
	Tag tagPWorkMax;
	Tag tagPDelta;
	Tag tagPDeltaMax;
	Tag tagPOpenK5;
	Tag tagPLoad;
	Tag tagPUnload;
	Tag tagPFan;
	Tag tagTimeStartDly;
	Tag tagTimeLoad;
	Tag tagTimeWarm;
	Tag tagTimeInflMax;
	Tag tagTimeDeflMax;
	Tag tagTimeExp;
	Tag tagTimeExpMax;
	Tag tagTimeStopFan;
	Tag tagTimeUnload;
	Tag tagTimeUnloadMin;
	Tag tagTimeUnloadMax;
	Tag tagTimeVibr;
	Tag tagTimeCycDly;
	Tag tagTimeLoadVibr;
	Tag tagPBlow;




	private int state;   
	private int waitCl;   
	private int waitOp;   
	private int waitEx;   
	private int alarm;   
	private int timer;   
	private int openK1;  
	private int openK2;  
	private int openK3;  
	private int openK4;  
	private int openK5;
	private int openK6;
	private int stopfan;
	private int vibr;

	private int timerFan;


	@Override
	public void prepareTags(RefBool res) {
    	
    	inpEnable		= getInput("Enable"		, res);
		inpSuspend		= getInput("Suspend"	, res);
		inpCanInfl		= getInput("CanInfl"	, res);
    	inpP			= getInput("P"			, res);
    	inpPEmrg		= getInput("PEmrg"		, res);
    	inpPSensorOk    = getInput("PSensorOk"	, res);
    	inpPLineOk		= getInput("PLineOk"	, res);
    	inpPAirOk		= getInput("PAirOk"		, res);
    	inpEnergyOk     = getInput("EnergyOk"	, res);
    	inpDVU1			= getInput("DVU1"  		, res);
    	inpDVU2			= getInput("DVU2"  		, res);
    	inpClosedK1		= getInput("ClosedK1"  	, res);
    	inpClosedK2		= getInput("ClosedK2"  	, res);
    	inpClosedK3		= getInput("ClosedK3"  	, res);
    	inpClosedK4		= getInput("ClosedK4"  	, res);
    	inpClosedK5		= getInput("ClosedK5"  	, res);
    	inpClosedK6		= getInput("ClosedK6"  	, res);
		inpOpenedK1		= getInput("OpenedK1"  	, res);
		inpOpenedK2		= getInput("OpenedK2"  	, res);

    	tagState		= getOutputTag( "State"        	, res);
    	tagWaitCl		= getOutputTag( "WaitCl"        , res);
    	tagWaitOp		= getOutputTag( "WaitOp"        , res);
    	tagWaitEx		= getOutputTag( "WaitEx"        , res);
    	tagAlarm		= getOutputTag( "Alarm"        	, res);
    	tagTimer		= getOutputTag( "Timer"        	, res);
    	tagCycleNum		= getOutputTag( "CycleNum"     	, res);
    	tagOpenK1		= getOutputTag( "OpenK1"		, res);
    	tagOpenK2		= getOutputTag( "OpenK2"       	, res);
    	tagOpenK3		= getOutputTag( "OpenK3"       	, res);
    	tagOpenK4		= getOutputTag( "OpenK4"       	, res);
    	tagOpenK5		= getOutputTag( "OpenK5"       	, res);
    	tagOpenK6		= getOutputTag( "OpenK6"       	, res);
    	tagStopFan		= getOutputTag( "StopFan"       , res);
    	tagVibr			= getOutputTag( "Vibr"       	, res);

    	tagBtnStart		= getOutputTag( "BtnStart"      , res);
    	tagBtnStop		= getOutputTag( "BtnStop"       , res);
    	tagNextState	= getOutputTag( "NextState"     , res);
    	tagFlags		= getOutputTag( "Flags"      	, res);
    	tagCycleQnt		= getOutputTag( "CycleQnt"    	, res);
    	tagPAlarm		= getOutputTag( "PAlarm"        , res);
    	tagPWork		= getOutputTag( "PWork"     	, res);
    	tagPWorkMax		= getOutputTag( "PWorkMax"      , res);
		tagPDelta		= getOutputTag( "PDelta"        , res);
    	tagPDeltaMax	= getOutputTag( "PDeltaMax"     , res);
		tagPOpenK5		= getOutputTag( "POpenK5"       , res);
    	tagPLoad		= getOutputTag( "PLoad"       	, res);
    	tagPUnload		= getOutputTag( "PUnload"    	, res);
    	tagPFan			= getOutputTag( "PFan"      	, res);
    	tagTimeStartDly	= getOutputTag( "TimeStartDly"  , res);
    	tagTimeLoad		= getOutputTag( "TimeLoad"      , res);
    	tagTimeWarm		= getOutputTag( "TimeWarm"      , res);
    	tagTimeInflMax	= getOutputTag( "TimeInflMax"   , res);
    	tagTimeDeflMax	= getOutputTag( "TimeDeflMax"   , res);
    	tagTimeExp		= getOutputTag( "TimeExp"     	, res);
		tagTimeExpMax	= getOutputTag( "TimeExpMax"    , res);
		tagTimeStopFan	= getOutputTag( "TimeStopFan"   , res);
		tagTimeUnload	= getOutputTag( "TimeUnload"  	, res);
    	tagTimeUnloadMin= getOutputTag( "TimeUnloadMin" , res);
    	tagTimeUnloadMax= getOutputTag( "TimeUnloadMax" , res);
    	tagTimeVibr		= getOutputTag( "TimeVibr"   	, res);
    	tagTimeCycDly	= getOutputTag( "TimeCycDly"	, res);
		tagTimeLoadVibr	= getOutputTag( "TimeLoadVibr"   	, res);
		tagPBlow        = getOutputTag( "PBlow"   	, res);


//		if (res.value)
//			resetState();
		state = 0;
		waitCl = 0;
		waitOp = 0;
		waitEx = 0;
		alarm = 0;
		timer = 0;
		openK1 = 0;
		openK2 = 0;
		openK3 = 0;
		openK4 = 0;
		openK5 = 0;
		openK6 = 0;
		stopfan = 0;
		vibr = 0;
		timerFan = 0;
	}


//	protected void resetState() {
//		tagState	.setInt(0);
//		tagWaitCl	.setInt(0);
//		tagWaitOp	.setInt(0);
//		tagWaitEx	.setInt(0);
//		tagAlarm	.setInt(0);
//		tagTimer	.setInt(0);
//		tagCycleNum	.setInt(0);
//		tagOpenK1	.setInt(0);
//		tagOpenK2	.setInt(0);
//		tagOpenK3	.setInt(0);
//		tagOpenK4	.setInt(0);
//		tagOpenK5	.setInt(0);
//		tagOpenK6	.setInt(0);
//		tagStopFan	.setInt(0);
//		tagVibr		.setInt(0);
//
//		state = 0;
//		waitCl = 0;
//		waitOp = 0;
//		waitEx = 0;
//		alarm = 0;
//		timer = 0;
//		openK1 = 0;
//		openK2 = 0;
//		openK3 = 0;
//		openK4 = 0;
//		openK5 = 0;
//		openK6 = 0;
//		stopfan = 0;
//		vibr = 0;
//
//		timerFan = 0;
//
//	}
	
	
	@Override
	public boolean execute() {

		makeConsistency();
		
		updateTimer();

		waitCl = 0;
		waitOp = 0;
		waitEx = 0;
		alarm = 0;

		
		
		if( tagNextState.getInt() > 0 ) {
			if( state > 0  &&  tagNextState.getInt() == state + 1 ) {
				state++;
				resetTimer();
			}
			tagNextState.setInt(0);
		}
		
		
		
		if( state == STATE_OFF ) {
			setValves( 0,  0,   0,  0,   1,  1 );
			tagBtnStop.setDouble(0);
			if( tagBtnStart.getBool() )
				nextState();
		}
		tagBtnStart.setDouble(0);

		
		
		if( state == STATE_START_DLY ) {
			tagCycleNum.setInt(0);
			setValves( 0,  0,   0,  0,   1,  1 );
			if( tagBtnStop.getBool()) { 
				state = STATE_OFF;
				resetTimer();
			} else {
				useTimer( tagTimeStartDly );
				nextStateIfChecked();
			} 
		}

			
			
		if( state == STATE_BEFORE_LOAD ) {        
			if( tagBtnStop.getBool()) { 
				state = STATE_OFF;
			} else {
				setValves( 0,  0,   0,  0,   1,  1 );
				chkValves(CLS,CLS, CLS,CLS, NCL,NCL );

				chkEx(WAIT_PLOAD, inpP.getInt() <= tagPLoad.getInt());
				chkEx(WAIT_PLINE_OK, inpPLineOk.getInt() > 0);
				chkEx(WAIT_PAIR_OK, inpPAirOk.getInt() > 0);
				chkEx(WAIT_PSENSOR_OK, inpPSensorOk.getInt() > 0);
				chkEx(WAIT_ENERGY_OK, inpEnergyOk.getInt() > 0);
				chkEx(WAIT_DVU1, inpDVU1.getInt() > 0);
				chkEx(WAIT_DVU2, inpDVU2.getInt() == 0);
				nextStateIfChecked();
			}
		}
			
			
		if( state == STATE_LOAD_OPEN ) {
			setValves( 1,  0,   0,  0,   1,  1 );
			chkValves(OPN, 0,   0,  0,   0,  0 );
			nextStateIfChecked();
		}



		if( state == STATE_LOAD_VIBRATE) {
			setValves( 1,  0,   0,  0,   1,  1 );
			useTimer( tagTimeLoadVibr );

			if( nextStateIfChecked() ) {
				resetTimer();
				setupTimer(tagTimeLoad);
			}


		}



		if( state == STATE_LOAD_DLY ) {
			setValves( 1,  0,   0,  0,   1,  1 );
			useTimer( tagTimeLoad );
			nextStateIfChecked();
		}
		
		
		if( state == STATE_LOAD_FINISH ) {
			if( tagTimeWarm.getInt() > 0 ) {
				// warm on
				setValves( 0,   0,   0,   0,   1,   0);
				chkValves(CLS, CLS, CLS, CLS, NCL, CLS);
			} else {
				// warm off
				setValves( 0,   0,   0,   0,   1,   0);
				chkValves(CLS, CLS, CLS, CLS, NCL, CLS);
			}
			nextStateIfChecked();
		}


		if( state == STATE_BEFORE_INFLATE ) {
			chkEx(WAIT_CAN_INFL, inpCanInfl.getInt() > 0);
			chkEx(WAIT_PLINE_OK, inpPLineOk.getInt() > 0);
			nextStateIfChecked();
		}


		if( state == STATE_INFLATE_WARM ) {
			setValves( 0,  0,   1,  0,   1,  0 );
			useTimer(tagTimeWarm);
			if( nextStateIfChecked() ) {
				resetTimer();
				setupTimer(tagTimeInflMax);
			}
		}

		if( state == STATE_INFLATE_BLOW1 ) {
			setValves( 0,  0,   0,  0,   1,  0 );
			chkValves( 0,  0,     CLS,  0,     NCL,  0 );

			nextStateIfChecked();
		}

		if( state == STATE_INFLATE_BLOW2 ) {
			setValves( 0,  0,   1,  0,   1,  0 );

			chkEx(WAIT_PBLOW, inpP.getInt() >= tagPBlow.getInt());
			nextStateIfChecked();

			if( !checked()  &&  timer <= 0 )
				alarm = ALARM_INFLATE_TIMEOUT;
		}

		if( state == STATE_INFLATE ) {

			setValves( 0,  0,   1,  0,   0,  0 );
			chkValves( 0,  0,     0,  0,     0,  0 );
			chkEx(WAIT_PWORK, inpP.getInt() >= tagPWork.getInt());
			nextStateIfChecked();

			if( !checked()  &&  timer <= 0 )
				alarm = ALARM_INFLATE_TIMEOUT;
		}
		
		
		if( state == STATE_INFLATE_FINISH ) {
			resetTimer();
			setValves( 0,  0,   0,  0,   0,  0 );
			chkValves(CLS,CLS, CLS,CLS, CLS,CLS );
			nextStateIfChecked();
		}


		if( state == STATE_EXPOSITION ) {
			if( (inpP.getInt() <= tagPWork.getInt() - tagPDelta.getInt())  &&  openK4==0)
				openK4 = 1;
			if( (inpP.getInt() >= tagPWork.getInt())  &&  openK4==1)
				openK4 = 0;
			setValves( 0,  0,   0,  openK4,   0,  0 );
			useTimer( tagTimeExp );
			if( nextStateIfChecked() ) {
				resetTimer();
				setupTimer(tagTimeDeflMax);
			}
		}


		if( state == STATE_DEFLATE1 ) {
			setValves( 0,  0,   0,  0,   1,  1 );
			chkEx(WAIT_PFAN, inpP.getInt() <= tagPFan.getInt());

			if( nextStateIfChecked() )
				timerFan = -1;

			if( !checked()  &&  timer <= 0  )
				alarm = ALARM_DEFLATE_TIMEOUT;

		}


		if( state == STATE_DEFLATE2 ) {
			setValves( 0,  0,   0,  0,   1,  1 );

			timerFan++;

			chkEx(WAIT_PUNLOAD, inpP.getInt() <= tagPUnload.getInt());
			chkEx(WAIT_TIMEFAN, timerFan >= tagTimeStopFan.getInt());

			if( !checked()  &&  timer <= 0  )
				alarm = ALARM_DEFLATE_TIMEOUT;

			if( nextStateIfChecked() )
				resetTimer();

		}


		if( state == STATE_UNLOAD_OPEN ) {
			setValves( 0,  1,   0,  0,   1,  1 );
			chkValves( 0, OPN,  0,  0,   0,  0 );
			nextStateIfChecked();
		}

		
		if( state == STATE_UNLOAD_DLY ) {
			setValves( 0,  1,   0,  0,   1,  1 );
			useTimer( tagTimeUnload );
			nextStateIfChecked();
		}

		
		if( state == STATE_UNLOAD_EMPTY ) {
			resetTimer();
			setValves( 0,  1,   0,  0,   1,  1 );
			chkEx(WAIT_DVU2, inpDVU2.getInt() == 0);
			if( !nextStateIfChecked() )
				alarm = ALARM_UNLOAD_TIMEOUT;
		}
		
		
		if( state == STATE_UNLOAD_VIBRATE ) {
			setValves( 0,  1,   0,  0,   1,  1 );
			useTimer( tagTimeVibr );
			nextStateIfChecked();
		}
		
		
		if( state == STATE_UNLOAD_FINISH ) {
			setValves( 0,  0,   0,  0,   1,  1 );
			chkValves(CLS,CLS, CLS,CLS, NCL,NCL );
			nextStateIfChecked();
		}
		

		if( state == STATE_CYCLE_PAUSE ) {
			setValves( 0,  0,   0,  0,   1,  1 );
			useTimer( tagTimeCycDly );
			if( checked() ) {
				state = STATE_BEFORE_LOAD;
				
				tagCycleNum.setInt( tagCycleNum.getInt() + 1 );
				if( tagCycleNum.getInt() >= tagCycleQnt.getInt()  &&  tagCycleQnt.getInt() > 0)
					state = STATE_OFF;

				if( tagBtnStop.getBool() )
					state = STATE_OFF;
			}
		}
		

		openK5 = openK5 > 0  &&  inpP.getInt() <= tagPOpenK5.getInt() ? 1: 0;

		if( inpPEmrg.getInt() > 0  
				||  inpClosedK1.getInt()==0  ||  inpClosedK2.getInt()==0
				||  inpPSensorOk.getInt() == 0
		  )
			openK3 = openK4 = 0;
		
		if( inpP.getInt() >= tagPAlarm.getInt() )
			alarm = ALARM_PALARM;

		vibr = (state == STATE_UNLOAD_VIBRATE) || (state ==  STATE_LOAD_VIBRATE)? 1: 0;

		stopfan = (state >= STATE_DEFLATE2  &&  state <= STATE_UNLOAD_FINISH )? 1: 0;

		tagState    .setInt( state  );
		tagWaitCl   .setInt( waitCl );
		tagWaitOp   .setInt( waitOp );
		tagWaitEx   .setInt( waitEx );
		tagAlarm    .setInt( alarm  );
		tagTimer    .setInt( timer  );
		tagOpenK1   .setInt( openK1 );
		tagOpenK2   .setInt( openK2 );
		tagOpenK3   .setInt( openK3 );
		tagOpenK4   .setInt( openK4 );
		tagOpenK5   .setInt( openK5 );
		tagOpenK6   .setInt( openK6 );
		tagStopFan  .setInt( stopfan );
		tagVibr	    .setInt( vibr );



		return true;
	}



	private void makeConsistency() {
		if( tagPWork.getInt() > tagPWorkMax.getInt() )
			tagPWork.setInt(tagPWorkMax.getInt());
		
		if( tagPDelta.getInt() > tagPDeltaMax.getInt() )
			tagPDelta.setInt( tagPDeltaMax.getInt() );
		
		if( tagTimeExp.getInt() > tagTimeExpMax.getInt() )
			tagTimeExp.setInt( tagTimeExpMax.getInt() );
		
		if( tagTimeUnload.getInt() < tagTimeUnloadMin.getInt() )
			tagTimeUnload.setInt( tagTimeUnloadMin.getInt() );

	}


	private void useTimer(Tag time) {
		setupTimer(time);
		chkEx(WAIT_TIMER, timer <= 0);
	}


	private void setupTimer(Tag time) {
		if( timer < 0 ) 
			timer = time.getInt();
	}

	private void updateTimer() {
		if( timer >= 0 )
			timer--;
	}

	private void resetTimer() {
		timer = -1;
	}


	
	
	

	private void nextState() {
		state++;
	}

	private boolean nextStateIfChecked() {
		if( checked() ) {
			nextState();
			return true;
		}
		return false;
	}


	private boolean checked() {
		return (waitCl | waitOp | waitEx) == 0;
	}


	private void setValves(int k1, int k2, int k3, int k4, int k5, int k6) {
		openK1 = k1;  
		openK2 = k2; 
		openK3 = k3;  
		openK4 = k4;  
		openK5 = k5;  
		openK6 = k6;  
	}
	
	private void chkValves(int k1, int k2, int k3, int k4, int k5, int k6) {
		chkValve(0, k1, inpClosedK1, inpOpenedK1);
		chkValve(1, k2, inpClosedK2, inpOpenedK2);
		chkValve(2, k3, inpClosedK3);
		chkValve(3, k4, inpClosedK4);
		chkValve(4, k5, inpClosedK5);
		chkValve(5, k6, inpClosedK6);
	}


	private boolean chkValve(int bit, int mode, Input inp) {
		if( mode==NCL  &&  inp.getInt() > 0) 
			waitCl += 1<<bit;
		else
		if( mode==CLS  &&  inp.getInt() <= 0)
			waitCl += 1<<(bit + 8);
		else
			return true;
		
		return false;
	}

	private boolean chkValve(int bit, int mode, Input inpClosed, Input inpOpened) {
		if( !chkValve(bit, mode, inpClosed) )
			return false;
		
		if( mode==NOP  &&  inpOpened.getInt() > 0)
			waitOp += 1<<bit;
		else
		if( mode==OPN  &&  inpOpened.getInt() <= 0) 
			waitOp += 1<<(bit + 8);
		else
			return true;
		
		return false;
	}

	
	private boolean chkEx(int waitMask, boolean is_ok) {
		if( !is_ok ) {
			waitEx += waitMask;
			return false;
		}
		return true;
	}

	    


	@Override
	public void saveStateExtra(State state) {
		state.saveVar("state"     , this.state     );
		state.saveVar("waitCl"    , waitCl         );
		state.saveVar("waitOp"    , waitOp         );
		state.saveVar("waitEx"    , waitEx         );
		state.saveVar("alarm"     , alarm          );
		state.saveVar("timer"     , timer          );
		state.saveVar("openK1"    , openK1         );
		state.saveVar("openK2"    , openK2         );
		state.saveVar("openK3"    , openK3         );
		state.saveVar("openK4"    , openK4         );
		state.saveVar("openK5"    , openK5         );
		state.saveVar("openK6"    , openK6         );
		state.saveVar("stopfan"   , stopfan        );
		state.saveVar("vibr"      , vibr           );
		state.saveVar("timerFan"  , timerFan       );
	}

	@Override
	public void loadStateExtra(State state) {
        this.state 	= state.loadVar("state"   	  , this.state     );
        waitCl   	= state.loadVar("waitCl"      , waitCl         );
        waitOp   	= state.loadVar("waitOp"      , waitOp         );
        waitEx   	= state.loadVar("waitEx"      , waitEx         );
        alarm    	= state.loadVar("alarm"       , alarm          );
        timer    	= state.loadVar("timer"       , timer          );
        openK1   	= state.loadVar("openK1"      , openK1         );
        openK2   	= state.loadVar("openK2"      , openK2         );
        openK3   	= state.loadVar("openK3"      , openK3         );
        openK4   	= state.loadVar("openK4"      , openK4         );
        openK5   	= state.loadVar("openK5"      , openK5         );
        openK6   	= state.loadVar("openK6"      , openK6         );
        stopfan  	= state.loadVar("stopfan"     , stopfan        );
        vibr     	= state.loadVar("vibr"        , vibr           );
		timerFan    = state.loadVar("timerFan"    , timerFan       );
	}

}