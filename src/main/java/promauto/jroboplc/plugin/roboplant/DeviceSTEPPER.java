package promauto.jroboplc.plugin.roboplant;

import promauto.jroboplc.core.api.Tag;

import java.util.HashMap;
import java.util.Map;

public class DeviceSTEPPER extends Device {

    private static int 	 STEPPER_STATE_STOP     = 0;
    private static int 	 STEPPER_STATE_OUTTER   = 1;
    private static int 	 STEPPER_STATE_START    = 2;
    private static int 	 STEPPER_STATE_WORK_INC = 3;
    private static int 	 STEPPER_STATE_WORK_DEC = 4;
    private static int 	 STEPPER_STATE_ALM      = 5;

    Input inpEnable;
    Input inpCurTmp;
    Input inpStartOut;
    Input inpError;

    Tag tagOutVal;
    Tag tagState;
    Tag tagFlags;
    Tag tagMinOut;
    Tag tagMaxOut;
    Tag tagIncStep;
    Tag tagDecStep;
    Tag tagMaxTmp;
    Tag tagMinTmp;
    Tag tagAlmTHgh;
    Tag tagAlmTLow;
    Tag tagAlmTDelta;
    Tag tagTimeOnInc;
    Tag tagTimeOnDec;
    Tag tagOldMaxTmp;
    Tag tagTimeCnt;
    Tag tagIncDecSost;


    // helpers
    int			iOutVal; 		// current product consumer
    int         iState;
    int         iOldMaxTmp;
    int         iUpCnt;
    int         iDwnCnt;
    boolean     bFlIncDecAllow;

    Map<Integer, Runnable> commands = new HashMap<>();

    @Override
    protected void prepareTags(RefBool res) {
        iOutVal     = 0;
        iState      = 0;
        iOldMaxTmp  = 0;
        iUpCnt      = 0;
        iDwnCnt     = 0;

        inpEnable  			= getInput(	"Enable"    , res);
        inpCurTmp  			= getInput(	"CurTmp"    , res);
        inpStartOut			= getInput(	"StartOut"  , res);
        inpError  			= getInput(	"Error"     , res);


        tagOutVal   	    = getOutputTag( "OutVal"    , res);
        tagState       	    = getOutputTag( "State"     , res);
        tagFlags       	    = getOutputTag( "Flags"     , res);
        tagMinOut   	    = getOutputTag( "MinOut"    , res);
        tagMaxOut   	    = getOutputTag( "MaxOut"    , res);
        tagIncStep   	    = getOutputTag( "IncStep"   , res);
        tagDecStep   	    = getOutputTag( "DecStep"   , res);
        tagMaxTmp   	    = getOutputTag( "MaxTmp"    , res);
        tagMinTmp   	    = getOutputTag( "MinTmp"    , res);
        tagAlmTHgh   	    = getOutputTag( "AlmTHigh"  , res);
        tagAlmTLow   	    = getOutputTag( "AlmTLow"   , res);
        tagAlmTDelta        = getOutputTag( "AlmTDelta" , res);
        tagTimeOnInc   	    = getOutputTag( "TimeOnInc" , res);
        tagTimeOnDec   	    = getOutputTag( "TimeOnDec" , res);
        tagOldMaxTmp   	    = getOutputTag( "OldMaxTmp"    , res);
        tagTimeCnt   	    = getOutputTag( "TimeCnt"   , res);
        tagIncDecSost  	    = getOutputTag( "IncDecSost", res);


        commands.put(STEPPER_STATE_STOP, () -> {
            tagOutVal.setInt(0);
        });

        commands.put(STEPPER_STATE_START, () -> {
            tagOutVal.setInt(0);
        });

//        private static int 	 STEPPER_STATE_OUTTER   = 1;
//        private static int 	 STEPPER_STATE_START    = 2;
//        private static int 	 STEPPER_STATE_WORK_INC = 3;
//        private static int 	 STEPPER_STATE_WORK_DEC = 4;
//        private static int 	 STEPPER_STATE_ALM      = 5;

        super.prepareTags(res);
    }

    //TODO how to procs alarm?
    @Override
    public boolean execute() {
        if(inpEnable.getInt()==1 && inpError.getInt() == 0){
            if (isTemperatureOk()){
                if(STEPPER_STATE_STOP == iState) iState = STEPPER_STATE_START;
                else
                if(STEPPER_STATE_START == iState || STEPPER_STATE_WORK_DEC == iState) {
                    initCounters();
                    iState = STEPPER_STATE_WORK_INC;
                }else if(STEPPER_STATE_WORK_INC == iState){
                    procsNormalHeating();
                }
            }else{
                if(STEPPER_STATE_START == iState || STEPPER_STATE_WORK_INC == iState) {
                    initCounters();
                    fixOldMaxTemperature();
                    iState = STEPPER_STATE_WORK_DEC;
                }else
                if(STEPPER_STATE_STOP != iState || STEPPER_STATE_ALM != iState){
                    if(isOverHeating()) iState = STEPPER_STATE_ALM;
                    else procsHighHeating();
                }
            }
        }else{
            iState = STEPPER_STATE_STOP;
        };

        tagState.setInt(iState);
        tagOutVal.setInt(iOutVal);
        tagOldMaxTmp.setInt(iOldMaxTmp);
        tagTimeCnt.setInt(iDwnCnt > iUpCnt ? iDwnCnt : iUpCnt);


        return true;
    }

    private void initCounters(){
        iDwnCnt = 0;
        iUpCnt  = 0;
    }
    //
    private boolean isTemperatureOk(){
        return inpCurTmp.getInt() < tagMaxTmp.getInt();
    }

    private boolean isOverHeating(){
        return (inpCurTmp.getInt()) >= (iOldMaxTmp + tagAlmTDelta.getInt());
    }
    //
    private void procsHighHeating(){
        if(iDwnCnt >= tagTimeOnDec.getInt()){
            iDwnCnt = 0;
            // descrease out on step
            if(iOutVal > 0)
                iOutVal -= tagDecStep.getInt();
            iOutVal = iOutVal <= 0 ? tagMinOut.getInt() : iOutVal;

        }else iDwnCnt++;
    }

    private void procsNormalHeating(){
        if(iUpCnt >= tagTimeOnInc.getInt()){
            iUpCnt = 0;
            if(iOutVal < tagMaxOut.getInt())
                iOutVal += tagIncStep.getInt();
            iOutVal = iOutVal > tagMaxOut.getInt() ? tagMaxOut.getInt() : iOutVal;
        }else iUpCnt++;
    }

    private void fixOldMaxTemperature(){
        iOldMaxTmp = inpCurTmp.getInt();
    }
    //



//    @Override
//    public void loadState(Map<String, Integer> state) {
//        super.loadState(state);
//    }
//
//    @Override
//    public void saveState(Map<String, Integer> state) {
//        super.saveState(state);
//    }



}
