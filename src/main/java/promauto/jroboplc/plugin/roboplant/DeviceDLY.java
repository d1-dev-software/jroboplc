package promauto.jroboplc.plugin.roboplant;

import java.util.Map;

import promauto.jroboplc.core.State;
import promauto.jroboplc.core.api.Tag;

public class DeviceDLY extends Device {
	Input 	inpInput;
	Tag 	tagOutput;
	Tag 	tagTime;
	Tag 	tagCnt;
	
	int cnt;
	boolean output;
	
    @Override
	public void prepareTags(RefBool res) {

    	inpInput = getInput( "Input", res);
    	
    	tagOutput	 = getOutputTag( "Output"		, res);
    	tagTime		 = getOutputTag( "Time"			, res);
    	tagCnt		 = getOutputTag( "Cnt"			, res);

//		if (res.value)
//			resetState();

		cnt = 0;
		output = false;
	}


//	protected void resetState() {
//		tagOutput.setInt(0);
//		cnt = 0;
//		output = false;
//	}

	@Override
	public boolean execute() {

		if( tagTime.getInt() >= 0 ) {
			if (output) {
				if (inpInput.getInt() == 0) {
					output = false;
					cnt = 0;
				}
			} else {
				if (inpInput.getInt() > 0) {
					cnt++;
					if (cnt > tagTime.getInt())
						output = true;
				} else
					cnt = 0;
			}
		} else {
			if (!output) {
				if (inpInput.getInt() > 0) {
					output = true;
					cnt = 0;
				}
			} else {
				if (inpInput.getInt() == 0) {
					cnt++;
					if (cnt > -tagTime.getInt())
						output = false;
				} else
					cnt = 0;
			}
		}

		tagOutput.setBool(output);
		tagCnt.setInt(cnt);

		return true;
	}

	
	@Override
	public void saveStateExtra(State state) {
		state.saveVar("cnt", 		cnt);
		state.saveVar("output", 	output);
	}
	
	@Override
	public void loadStateExtra(State state) {
		cnt    = state.loadVar("cnt", 		cnt);
		output = state.loadVar("output", 	output);
	}

	
}