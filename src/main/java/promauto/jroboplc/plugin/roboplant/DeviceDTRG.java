package promauto.jroboplc.plugin.roboplant;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import promauto.jroboplc.core.State;
import promauto.jroboplc.core.api.Tag;

public class DeviceDTRG extends Device {
	Input 	inpMasterIn	;
	Input 	inpTrigSet;
	List<Input> 	inpEnable = new LinkedList<>();
	Tag 	tagOutput;
	Tag 	tagReady;
	
	int output;
	int ready;
	
    @Override
	public void prepareTags(RefBool res) {

    	inpMasterIn	 = getInput( "MasterIn", res);
    	inpTrigSet = getInput( "TrigSet", res);
    	
    	tagOutput	 = getOutputTag( "Output"		, res);
    	tagReady	 = getOutputTag( "Ready"		, res);
    	
    	for(Input inp: inputs) 
    		if( inp.name.equals("Enable") )
    			inpEnable.add(inp);

//		if(res.value)
//			resetState();
		output = 0;
	}



//	protected void resetState() {
//		tagOutput.setInt(0);
//		output = 0;
//	}

	@Override
	public boolean execute() {

		ready = 1;
		for(Input inp: inpEnable)
			ready &= inp.getInt() > 0? 1: 0;
			
		if( ready > 0  &&  inpTrigSet.getInt() > 0 ) {
		    output = inpMasterIn.getInt();
		};
			
		tagOutput.setInt(output);
		tagReady.setInt(ready);

		return true;
	}

	
	
	@Override
	public void saveStateExtra(State state) {
		state.saveVar("output", 	output);
		state.saveVar("ready", 		ready);
	}
	
	@Override
	public void loadStateExtra(State state) {
		output = state.loadVar("output", 	output);
		ready  = state.loadVar(	"ready", 	ready);
	}

}