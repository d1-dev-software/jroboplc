package promauto.jroboplc.plugin.roboplant;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import promauto.jroboplc.core.api.Configuration;
import promauto.jroboplc.core.api.Environment;
import promauto.jroboplc.core.api.Module;

public class ConnectManager {
//	private final Logger logger = LoggerFactory.getLogger(ConnectManager.class);
	
	public static class State {
		public Map<String,String> deviceModule = new HashMap<>();
		public Set<RoboplantModule> roboplantModules = new HashSet<>();
		public Set<String> notConnectedModuleNames = new HashSet<>();
	}
	
	private final State state = new State();
	private State backup = null;
	
	
	private Environment env;
	
	public ConnectManager(Environment env) {
		this.env = env;
	}
	

	public boolean loadConnectors(RoboplantModule _module, Object conf) {
		boolean result = true;
		removeConnectors(_module);
		
		
		Configuration cm = env.getConfiguration();
		Map<String,Object> conf_connect = cm.toMap( cm.get(conf, "connect") );
		for(Map.Entry<String, Object> ent: conf_connect.entrySet() ) {
			String devicename = _module.getName() + ':' + ent.getKey();
			String modulename = ent.getValue().toString();
			
			if( modulename.equals("---") ) {
				state.notConnectedModuleNames.add(devicename);
			}

			state.deviceModule.put(devicename, modulename);
		}
		
		if( conf_connect.size() > 0  ||  state.notConnectedModuleNames.size() > 0 )
			state.roboplantModules.add(_module);
		
		return result;
	}
	
	public void removeConnectors(RoboplantModule roboplantModule) {
		if( state.roboplantModules.contains(roboplantModule) ) {
			String modname = roboplantModule.getName() + ":";
			state.deviceModule.entrySet().removeIf( entry -> entry.getKey().startsWith(modname) );
			state.notConnectedModuleNames.removeIf( devmodname -> devmodname.startsWith(modname) );
			state.roboplantModules.remove(roboplantModule);
		}
	}
	
	public Module getConnectedModule(RoboplantModule module, Device device) {
		String modulename = state.deviceModule.get( module.getName() + ':' + device.devtype + '.' + device.tagname );
		if( modulename == null)
			return null;
		
		return env.getModuleManager().getModule( modulename );
	}

	public void replaceModule(RoboplantModule targetModule, RoboplantModule newModule) {
		if( state.roboplantModules.remove(targetModule) )
			state.roboplantModules.add(newModule);
	}


	public boolean isNotConnected(RoboplantModule module, Device device) {
		return state.notConnectedModuleNames.contains( 
				module.getName() + ':' + device.devtype + '.' + device.tagname );
	}
	
	public void backupState() {
		backup = new State();
		backup.deviceModule.putAll( 			state.deviceModule );
		backup.roboplantModules.addAll( 		state.roboplantModules );
		backup.notConnectedModuleNames.addAll( 	state.notConnectedModuleNames );
	}

	public void restoreState() {
		if( backup == null )
			return;

		state.deviceModule.clear();
		state.roboplantModules.clear();
		state.notConnectedModuleNames.clear();

		state.deviceModule = backup.deviceModule;
		state.roboplantModules = backup.roboplantModules;
		state.notConnectedModuleNames = backup.notConnectedModuleNames;
		backup = null;
	}


	public void resetState() {
		backup.deviceModule.clear();
		backup.roboplantModules.clear();
		backup.notConnectedModuleNames.clear();
		backup = null;
	}

	
}





















