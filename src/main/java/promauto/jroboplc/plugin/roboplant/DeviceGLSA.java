package promauto.jroboplc.plugin.roboplant;

import promauto.jroboplc.core.api.Tag;

public class DeviceGLSA extends ConnectDevice {
	Input inpInput;
	Input inpSuspend;
	Input inpAlarmEnable;
	
	Tag tagSetCmd	;
	Tag tagSost		;
	Tag tagAlarm	;
	Tag tagState	;
	Tag tagControl	;
	Tag tagSuspend	;
	Tag tagBlok		;

    @Override
	public void prepareTags(RefBool res) {
    	
    	connector.connect("State", "State", Connector.Mode.READ, res);
    	connector.connect("SetCmd", "SetCmd", Connector.Mode.WRITE, res);


    	inpInput 		= getInput( "Input"			, res);
    	inpSuspend 		= getInput( "Suspend"		, res);
    	inpAlarmEnable 	= getInput( "AlarmEnable"	, res);

    	tagSetCmd		 = getOutputTag( "SetCmd"	, res);
    	tagSost			 = getOutputTag( "Sost"		, res);
    	tagAlarm		 = getOutputTag( "Alarm"	, res);
    	tagState		 = getOutputTag( "State"	, res);
    	tagControl		 = getOutputTag( "Control"	, res);
    	tagSuspend		 = getOutputTag( "Suspend"	, res);
    	tagBlok			 = getOutputTag( "Blok"		, res);
    	
    	
//		if (res.value)
//			resetState();
	}


//	protected void resetState() {
//		tagSetCmd	.setInt(0);
//		tagSost		.setInt(0);
//		tagAlarm	.setInt(0);
//	}

	@Override
	public boolean execute() {
		super.execute();

		int flags = tagBlok.getInt();

		boolean inp = inpInput.getInt() > 0;
		boolean susp = (inpSuspend.getInt() > 0) || (tagSuspend.getBool());

		boolean state_on;
		boolean state_off;
		boolean state_alm;
		if ((flags & 0xF0) == 0) {
			state_on = (tagState.getInt() & 0x2E) > 0;
			state_off = (tagState.getInt() & 0x01) > 0;
			state_alm = (tagState.getInt() & 0x40) > 0;
		} else {
			state_on = (tagState.getInt() == 1);
			state_off = (tagState.getInt() == 0);
			state_alm = (tagState.getInt() == 2);
		}

		boolean control_stop = false;

		tagAlarm.setBool(  state_alm  &&  (flags & 1) == 0  &&  inpAlarmEnable.getInt() > 0  );

		if ((tagControl.getInt() & 1) > 0) {
			inp = (tagControl.getInt() & 2) > 0;
			control_stop = !inp;
		}

		if (susp)
			inp = false;

		if (state_alm) {
			tagSetCmd.setInt((control_stop) ? 2 : 0);
		} else {
			if ((!inp) && (!state_off))
				tagSetCmd.setInt(2);
			else

			if ((inp) && (!state_on))
				tagSetCmd.setInt(1);
			else
				tagSetCmd.setInt(0);
		}

		// SOST
		int sost = tagSost.getInt();

		if (state_alm)
			sost = 4;
		else // авария

		if (tagSetCmd.getInt() == 1)
			sost = 2;
		else // запуск

		if (tagSetCmd.getInt() == 2)
			sost = 3;
		else // останов

		if (state_on)
			sost = 1;
		else // работает

		if ((susp) && (state_off))
			sost = 5;
		else // приостанов

		if (state_off)
			sost = 0;
		else
			// не работает

			sost = 6; // не определенное состояние

		tagSost.setInt(sost);

		return true;
	}

}