package promauto.jroboplc.plugin.roboplant;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DeviceBITR extends Device {
	List<Boolean> values = new ArrayList<>();


	@Override
	public void prepareTags(RefBool res) {
		for(Output output: outputs)
			values.add(false);

		if (res.value)
			resetState();
	}


	protected void resetState() {
//		outputs.forEach(output -> output.tag.setOff());
		Collections.fill(values, Boolean.FALSE);
	}

	@Override
	public boolean execute() {
		for(int i = 0; i < values.size(); ++i) {
			boolean val1 = outputs.get(i).tag.getBool();
			boolean val2 = values.get(i);
			if( val1 != val2 ) {
				values.set(i, val1);
				if( val1 ) {
					for(int j = 0; j < values.size(); ++j) {
						if( j != i ) {
							outputs.get(j).tag.setOff();
							values.set(j, false);
						}
					}
					return true;
				}
			}
		}
		return true;
	}
}