package promauto.jroboplc.plugin.roboplant;

public class DeviceI7017 extends ConnectDevice {
	
    @Override
	public void prepareTags(RefBool res) {
    	
    	for(int i=0; i<8; ++i)
    		connector.connect( 
    				"Input_" + i, 
    				"inp" + i + ".value", 
    				Connector.Mode.READ, res);
    	
    	connector.connect("ErrorFlag", "SYSTEM.ErrorFlag", Connector.Mode.READ, res);
    	connector.connect("ErrorCounter", "SYSTEM.ErrorCount", Connector.Mode.READ, res);
    	
    	registerAsChannelProvider( "DipNetAddress", Channel.Type.In, res);
    }

    
}