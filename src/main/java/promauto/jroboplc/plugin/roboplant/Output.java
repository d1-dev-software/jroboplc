package promauto.jroboplc.plugin.roboplant;

import promauto.jroboplc.core.api.Tag;

import java.util.Comparator;

public class Output extends Comp  {
	public int num = 0;
	public boolean initiated = false;
	public int inival = 0; // IniVal is used when there's no Value presented
	public String value = null;
	public boolean istag = false;
	public boolean saveable = false;
	public boolean access_readonly = false;

	public Output() {
	}

	public Output(int num) {
		this.num = num;
	}

	public Output(String name) {
		this.name = name;
	}

	@Override
	public void setAttribute(String attr, String value) {
		switch (attr) {
		case "Num":
			num = Integer.parseInt(value, 16);
			break;
		case "Initiated":
			initiated = value.equals("1");
			break;
		case "IniVal":
			inival = Integer.parseInt(value, 16);
			break;
		case "Value":
			this.value = value;
			break;
		case "IsTag":
			istag = value.equals("1");
			break;
		case "TagSaveable":
			saveable = Integer.parseInt(value)>0;
			break;
		case "Access":
			access_readonly = value.equals("2");
			break;
		default:
			super.setAttribute(attr, value);
		}
	}


	
	public static class CmpNum implements Comparator<Output>{
		private static CmpNum instance = null;
		
		private CmpNum() {}
		
		public static CmpNum get() {
			if (instance==null)
				instance = new CmpNum();
			return instance; 
		}

		@Override
		public int compare(Output o1, Output o2) {
			return o1.num - o2.num;
		}
		
	}
	
}
