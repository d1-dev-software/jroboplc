package promauto.jroboplc.plugin.roboplant;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import promauto.jroboplc.core.api.Tag;

public class DeviceSCN extends Device {
	List<Input> inpInput 	= new ArrayList<>();
	List<Tag> 	tagValue 	= new ArrayList<>();
	
	Tag 		tagOutput	;
	Tag 		tagOper	    ;
//	int output;
	int inpAmount;

	Map<Integer,Boolean> dupls;


    @Override
	public void prepareTags(RefBool res) {

    	tagOutput		 = getOutputTag( "Output"		, res);
    	tagOper			 = getOutputTag( "Oper"		, res);

		inpInput.clear(); 
		tagValue.clear();

		Input  inp;
		Output val;
    	int i=0;
    	while (true) {
    		inp = getInput( String.format("Input_%02X", i) );
    		val = getOutput(i+2);
    		if (inp==null || val==null) break;
    		
    	    inpInput.add( inp );
    	    tagValue.add( val.tag );
    	    
    	    i++;
    	}
    	
    	inpAmount = inpInput.size();

//		if (res.value)
//			resetState();
	}

//	protected void resetState() {
//		tagOutput.setInt(0);
//	}

	
	/*
	 * Warning! The source code of this method was ported from C++ original
	 * RoboplantMtr sources. Needs get refactored! Do not repeat this style!
	 */
	@Override
	public boolean execute() {
		int oper = tagOper.getInt();

		if( oper == 8) {
			if( dupls == null )
				dupls = new HashMap<>();
			else
				dupls.clear();

			for (int i = 0; i < inpAmount; i++) {
				int inp = inpInput.get(i).getInt();
				if( inp != 0)
					dupls.put(
							inpInput.get(i).getInt(),
							dupls.containsKey(inpInput.get(i).getInt())
					);
			}

			int output = 0;
			for (int i = 0; i < inpAmount; i++) {
				int inp = inpInput.get(i).getInt();
				Boolean res = dupls.get(inp);
				if( res != null  &&  res ) {
					tagValue.get(i).setInt(inp);
					output = 1;
				} else
					tagValue.get(i).setInt(0);
			}

			tagOutput.setInt(output);
			return true;
		}

		int c;
		int val;
		int inp;
		int v = (oper == 0) ? 1 : 0;
		int ind = 1;
		int el = 0;

		for (int i = 0; i < inpAmount; i++) {
			val = tagValue.get(i).getInt();
			inp = inpInput.get(i).getInt();
			if ((val < 0x100) && (oper < 4) ) {
				c = (inp == val) ? 1 : 0;

				// &
				if (oper == 0)
					v &= c;
				else

				// |
				if (oper == 1)
					v |= c;
				else

				// +
				if (oper == 2)
					v += c;
				else

				// bin to dec
				if (oper == 3) {
					v |= ind * ((inp > 0 ? 1 : 0));
					ind = ind << 1;
				}

			}


			if (oper >= 4) {
				c = inp;
				// MAX  IdxMAX
				if ((oper == 4) || (oper == 6)) {
					if ((i == 0) || (el < c)) {
						el = c;
						v = i;
					}
				} else
				// MIN  IdxMIN
				if ((oper == 5) || (oper == 7)) {
					if ((i == 0) || (el > c)) {
						el = c;
						v = i;
					}
				}
			}

		}

		if ((oper == 3) || (oper == 4) || (oper == 5) || (oper == 6) || (oper == 7)) {
			for (int i = 0; i < inpAmount; i++)
				tagValue.get(i).setBool(v == i);
		}

		if ((oper == 4) || (oper == 5))
			tagOutput.setInt(el);
		else
			tagOutput.setInt(v);

		return true;
	}	

}