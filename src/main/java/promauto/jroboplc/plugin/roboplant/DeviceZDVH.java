package promauto.jroboplc.plugin.roboplant;

import promauto.jroboplc.core.api.Tag;

public class DeviceZDVH extends Device {
    Tag tagClosed;
    Tag tagOpened;
    Tag tagBlok;
    Tag tagFlags;
    Channel chnlIn;

    
    @Override
	public void prepareTags(RefBool res) {
		createTagDescr();

    	tagClosed   	= getOutputTag(		"Closed"	, res);
    	tagOpened   	= getOutputTag(		"Opened"	, res);
    	tagBlok     	= getOutputTag(		"Blok"		, res);
    	tagFlags        = getOutputTag(		"Flags"		, res);
    	chnlIn			= createChannel(	"ChnlIn"	, Channel.Type.In, res);
    	
//		if (res.value)
//			resetState();
	}


//	protected void resetState() {
//		tagOpened.setInt(0);
//		tagClosed.setInt(0);
//
//	}
	
	
	/* Warning!
	 * Code of this method was ported from C++ original RoboplantMtr sources. 
	 * Needs get refactored! Do not repeat this style!
	 */
	@Override
	public boolean execute() {
		
		if( !chnlIn.isOk() ) {
			tagClosed.setInt( tagClosed.getInt() > 0? 0: 1);
			tagOpened.setInt( tagOpened.getInt() > 0? 0: 1);
			return false;
		}
		
		int blok = tagBlok.getInt();
		int opened, closed;
		if( blok > 0 ) {
			opened = blok != 1? 1: 0;
			closed = blok == 1? 1: 0;
		} else {
			closed = (chnlIn.tagValue.getInt() > 0) == ((tagFlags.getInt() & 0x80) > 0)? 1: 0;
			opened = 1 - closed;
		}
		
		if( tagClosed.getInt() != closed )
			tagClosed.setInt(closed);

		if( tagOpened.getInt() != opened )
			tagOpened.setInt(opened);

		return true;
	}

}