package promauto.jroboplc.plugin.roboplant;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import promauto.jroboplc.core.State;
import promauto.jroboplc.core.api.Tag;
import promauto.jroboplc.core.tags.TagPlain;

public class DeviceMCHB extends Device {
	private static final int FL_IGNORE_SUSPEND_ON_START = 0x200;
	private static final int FL_ALARM_ON_SUSPEND = 0x100;
	private static final int FL_MASTEROUT_BY_PLATA = 4;
	private static final int FL_INSTANT_STOP_DISABLED = 0x10;
	private static final int FL_PLATA_CTRL_LEVEL = 0x80;
	private static final int FL_EXTINPUT_DISABLED = 8;
	private static final int FL_MODE = 3;
	List<Input> 		inpTrigres 	= new LinkedList<>();
	List<Input> 		inpSuspend 	= new LinkedList<>();
	List<List<Input>> 	inpInput 	= new LinkedList<>();
	Input inputZ;

	Tag tagMasterOut;
	Tag tagSost;
    Tag tagAlarm;
    Tag tagPlata;
    Tag tagCnt;
    Tag tagTrigRes;
    Tag tagControl;
    Tag tagBlok;
    Tag tagTimeStart;
    Tag tagTimeStop;
    Channel channelOut;
    Channel channelIn;
    Tag tagFlags;
    Tag tagInput;
	Tag tagInputExt;
	Tag tagCtrlBeep; // Note that this tag may be missing for backward compatibility.

	int oldControl;
	int cntManAlarm;
	int sost;
	int cnt;

	int ctrlDlyCnt;
	int ctrlValue;


	@Override
	public void prepareTags(RefBool res) {
		createTagDescr();

		tagMasterOut 	= getOutputTag(		"MasterOut"	, res);
		tagSost 		= getOutputTag(		"Sost" 		, res);     
		tagAlarm 		= getOutputTag(		"Alarm" 		, res);
		tagPlata 		= getOutputTag(		"Plata" 		, res);
		tagCnt 			= getOutputTag(		"Cnt" 		, res);      
		tagTrigRes 		= getOutputTag(		"TrigRes" 	, res);  
		tagControl 		= getOutputTag(		"Control" 	, res);  
		tagBlok 		= getOutputTag(		"Blok" 		, res);     
		tagTimeStart 	= getOutputTag(		"TimeStart"	, res);
		tagTimeStop 	= getOutputTag(		"TimeStop" 	, res); 
		channelOut 		= createChannel(	"Channel1"	, Channel.Type.Out, res); 
		channelIn 		= createChannel(	"Channel2"	, Channel.Type.In, res); 
		tagFlags 		= getOutputTag(		"Flags" 		, res);
		tagInput 		= getOutputTag(		"Input" 		, res);
		tagInputExt 	= getOutputTag(		"InputExt"	, res);

		tagCtrlBeep 	= getOutputTagOrFake("CtrlBeep");

		prepareInputArrays();
		
//		if (res.value)
//			resetState();

		sost = 0;
		cnt = 0;
		oldControl = 0;
	}
	
    
//	protected void resetState() {
//		tagMasterOut.setInt(0);
//		tagSost.setInt(0);
//		tagAlarm.setInt(0);
//		tagPlata.setInt(0);
//		tagCnt.setInt(0);
//		tagTrigRes.setInt(0);
//		tagInput.setInt(0);
//		tagInputExt.setInt(0);
//
//		sost = 0;
//		cnt = 0;
//		oldControl = 0;
//	}

	
    protected void prepareInputArrays() {
    	inpTrigres.clear(); 	
    	inpSuspend.clear(); 	
    	inpInput.clear(); 	
    	inputZ = null;
    	
    	String curInpName = "";
    	List<Input> curList = null;
    	
    	for(Input inp: inputs) 
    		switch (inp.name) {
    		case "TrigRes":	inpTrigres.add(inp); break;
    		case "Suspend":	inpSuspend.add(inp); break;
    		case "InputZ":	inputZ = inp; break;
    		default:
	       		if (inp.name.startsWith("Input")) {
	       			if (!inp.name.equals(curInpName)) {
	       				curList = null;
	       				curInpName = inp.name;
	       			}
	       			
	       			if (curList == null) {
	       				curList = new LinkedList<>();
	       				inpInput.add(curList);
	       			}
	       			curList.add(inp);
	       		}
    		}
	}
    
    
    public boolean getValueAnd(List<Input> list) {
    	for (Input inp: list) 
    		if (inp.getInt() == 0)
    			return false;
    	return list.size() > 0;
    }

    public boolean getValueOr(List<Input> list) {
    	for (Input inp: list) 
    		if (inp.getInt() > 0)
    			return true;
    	return false;
    }


	/* Warning!
	 * Code of this method was ported from C++ original RoboplantMtr sources. 
	 * Needs get refactored! Do not repeat this style!
	 */
	@Override
	public boolean execute() {
    	
		  boolean pTrig = false;
		  boolean pSusp = false;
		  boolean pSuspInput = false;
		  boolean pInput = false;
		  int sostNew = 0;
		  boolean mstin;
		  boolean mstin_nosusp;
		  boolean pin;
		  boolean hand_stop = false;
		  boolean plataIn = false;
		  boolean plataOut = false;
		  int flags_mode;
		  
		  int flags = tagFlags.getInt();
		  int control = tagControl.getInt();

		  int ctrlDlyTime = (control >> 8) & 0xff;
		  if( ctrlDlyTime == 0) {
		  	ctrlDlyCnt = 0;
			ctrlValue = control;
		  } else {
		  	if( ctrlDlyCnt == ctrlDlyTime ) {
				ctrlDlyCnt = 0;
				ctrlDlyTime = 0;
				control &= 0xff;
				tagControl.setInt(control);
			} else {
				ctrlDlyCnt++;
				control = ctrlValue;
			}
		  }
		  tagCtrlBeep.setBool( ctrlDlyTime > 0 );
	
	
	
		  flags_mode = flags & FL_MODE;

		  tagAlarm.setBool( sost == 4 ); 
		  
		  
		  if (!linkChannels(flags_mode))
			  return true;
		  
    	  
    	  // 2. Определение pTrig
    	  pTrig = getValueOr(inpTrigres);
    	  if ((control & 4) > 0) {
    		  pTrig = true;
    		  control &= 0xFFFB;
    		  tagControl.setInt(control);
      	  }

    	  if (control != oldControl) {
	    	  if (((control & 3)==3) && (((control ^ oldControl)&3) > 0)) {
	    		  pTrig = true;
	    	  }
			  oldControl = control;
    	  }
    	  
    	  
    	  // 3. Определение pSusp
    	  pSuspInput = getValueOr(inpSuspend);
    	  if( pSuspInput  &&  (sost == 0 || sost == 2)  &&  (flags & FL_IGNORE_SUSPEND_ON_START) > 0 )
    		  pSusp = false;
    	  else
    		  pSusp = pSuspInput;
    	        	  
    	  
    	  // 4. Определение pInput
		  boolean inputOutExt = (control & 0x10) > 0;
		  for (List<Input> list: inpInput)
			  if (getValueAnd(list)) {
				  pInput = true;
				  break;
			  }
		  
		  if (inputOutExt) 
			  if ((flags & FL_EXTINPUT_DISABLED)==0)
				  pInput = true;
				  
		  
		  
    	  // 5. Расчет Sost
		  if ((control & 3) > 0) {
			  mstin = (control & 2) > 0;
			  hand_stop = !mstin;
		  } else
			  mstin = pInput;
		  mstin_nosusp = mstin;
		  
		  
    	  if (pSusp)
      	    mstin = false;
      	  else 
      	    if (sost==5)   
      	    	sost=0;
      	  
		  
		if (inputZ != null)
			plataIn = inputZ.getInt() > 0;
		else if (flags_mode == 3)
			plataIn = mstin;
		else if ((flags & FL_PLATA_CTRL_LEVEL) > 0)
			plataIn = channelIn.tagValue.getInt() > 0;
		else
			plataIn = channelIn.tagValue.getInt() == 0;    		  
		  
		  
    	  if (tagBlok.getInt() > 0)
    		  pin = mstin;
    	  else
    		  pin = plataIn;
    	  
		  
		switch (sost) {
		case 0: // Выключено
			if (mstin) {
				sostNew = 2;
				plataOut = true;
				cnt = 1;
			} else if (pin) {
				sostNew = 7;
				plataOut = false;
			} else {
				sostNew = 0; 
				plataOut = false;
			}
			break;

		case 1: // Включено
			if (mstin) {
				if (pin) {
					sostNew = 1;
					plataOut = true;
					cnt = 0;
				} else {
					if (((flags & FL_INSTANT_STOP_DISABLED) == 0) || (cnt >= tagTimeStart.getInt())) {
						sostNew = 4;
						plataOut = false;
						cnt = 0;
					} 
					else {
						sostNew = 1;
						plataOut = true;
						cnt++;
					}
				}
			} else {
				sostNew = 3;
				plataOut = (tagTimeStop.getInt() > 0);
				cnt = 1;
			}
			break;

		case 2: // Запуск
			if (mstin)
				if (pin)
					if ((cnt >= tagTimeStart.getInt()) || ((flags & FL_MASTEROUT_BY_PLATA) > 0)) {
						sostNew = 1;
						plataOut = true;
						cnt = 0;
					} else {
						sostNew = 2;
						plataOut = true;
						cnt++;
					}
				else if (cnt >= tagTimeStart.getInt()) {
					sostNew = 4;
					plataOut = false;
					cnt = 0;
				} 
				else {
					sostNew = 2;
					plataOut = true;
					cnt++;
				}
			else {
				sostNew = 0;
				plataOut = false;
				cnt = 0;
			}
			break;

		case 3: // Останов
			if (mstin) {
				sostNew = 2;
				plataOut = true;
				cnt = 1;
			} else if ((cnt >= tagTimeStop.getInt()) || (hand_stop)) {
				if (pin) {
					sostNew = 7;
					plataOut = false;
					cnt = 0;
				} else {
					sostNew = 0;
					plataOut = false;
					cnt = 0;
				}
			} else {
				sostNew = 3;
				plataOut = true;
				cnt++;
			}
			break;

		case 4: // Авария
			if (pTrig) {
				sostNew = 0;
				plataOut = false;
			} else {
				sostNew = 4;
				plataOut = false;
			} 
			break;

		case 5: // Авария от Suspend'ов
			plataOut = false;
			cnt = 0;
			break;	// why no break???

		case 7: // Вкл. без команды
			if (mstin) {
				sostNew = 2;
				plataOut = true;
				cnt = 1;
			} else if (pin) {
				sostNew = 7;
				plataOut = false;
				cnt = 0;
			} else {
				sostNew = 0;
				plataOut = false;
			}
			break;
		}  
    	  
    	  
    	  
		if( (pSuspInput  &&  sostNew == 1)  ||  (pSusp  &&  sost != 4  &&  mstin_nosusp) ) {
			sostNew = 5;
			cnt = 0;
			plataOut = false;
		} 
//		if (pSusp && (sost != 4) && (mstin_nosusp)) {
//			sostNew = 5;
//			cnt = 0;
//		}
		

		if (flags_mode == 2) {
			sostNew = plataIn? 1: 0;
			cnt = 0;
		} else if ((control & 8) > 0) {
			sostNew = 4;
			plataOut = false;
			control &= 0xFFF7;
			tagControl.setInt(control);

			cntManAlarm = cntManAlarm + 1;
		}

		sost = sostNew;

		boolean masterOut = (sost == 1) && (pInput);
		
		if ((((control & 3) == 1) || ((control & 3) == 3)) && (pInput))
			masterOut = true;

		if (flags_mode < 2)
			channelOut.tagValue.setBool(plataOut);

		tagTrigRes.setBool(pTrig);

		tagAlarm.setBool( sost == 4  ||  (sost==5  &&  (flags & FL_ALARM_ON_SUSPEND) > 0) ); 
		
		
		// post
        tagSost.setInt(sost);
       	tagPlata.setBool(plataIn);
       	tagCnt.setInt(cnt);
        tagMasterOut.setBool(masterOut);
        tagInput.setBool( pInput );
        tagInputExt.setBool( inputOutExt );
       
		return true;
	}


	
	protected boolean linkChannels(int flags_mode) {
		if((flags_mode == 1 ||  flags_mode == 2)  &&  channelIn.classic  &&  channelOut.classic)
			if (channelIn.tagAddrNum.getInt() != channelOut.tagAddrNum.getInt())
				channelOut.tagAddrNum.copyValueTo(channelIn.tagAddrNum);

		boolean chnl_ok = true;
		if (flags_mode != 3)
			chnl_ok = chnl_ok && channelIn.isOk();

		if (flags_mode < 2)
			chnl_ok = chnl_ok && channelOut.isOk();

		if (!chnl_ok) {
			tagSost.setInt(8);
			return false;
		}

		return chnl_ok;
	}
	

	
	
	@Override
	public void saveStateExtra(State state) {
		state.saveVar("oldControl", 	oldControl );
		state.saveVar("cntManAlarm", 	cntManAlarm);
		state.saveVar("sost", 		    sost       );
		state.saveVar("cnt", 			cnt        );
		state.saveVar("ctrlDlyCnt", 	ctrlDlyCnt );
		state.saveVar("ctrlValue", 		ctrlValue  );
	}

	@Override
	public void loadStateExtra(State state) {
	    oldControl  = state.loadVar("oldControl", 	oldControl 	);
	    cntManAlarm = state.loadVar("cntManAlarm", 	cntManAlarm	);
	    sost        = state.loadVar("sost", 		sost       	);
		cnt         = state.loadVar("cnt", 			cnt        	);
		ctrlDlyCnt  = state.loadVar("ctrlDlyCnt", 	ctrlDlyCnt  );
		ctrlValue   = state.loadVar("ctrlValue", 	ctrlValue   );
	}

	
}