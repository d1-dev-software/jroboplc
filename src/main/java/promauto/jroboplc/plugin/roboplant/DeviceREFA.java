package promauto.jroboplc.plugin.roboplant;

import promauto.jroboplc.core.api.Tag;

import java.util.ArrayList;
import java.util.List;

public class DeviceREFA  extends ConnectDevice {
    private static final String CHANNEL_PROVIDER        = "channelProvider";
    private static final String CHANNEL_PROVIDER_IN     = CHANNEL_PROVIDER + "In";
    private static final String CHANNEL_PROVIDER_OUT    = CHANNEL_PROVIDER + "Out";
    private static final String CHANNEL_PROVIDER_INOUT  = CHANNEL_PROVIDER + "InOut";
    private static final String EXECUTE_BIND_MODULE     = "executeBindModule";

    private List<Input> inpInput = new ArrayList<>();
    private List<Output> tagOutput = new ArrayList<>();
    private Output outputExecuteBindModule;

    @Override
    public void prepareTags(RefBool res) {

        boolean need_create_tags = false;
        for(Input input: inputs) {
            if( !input.isEmpty() ) {
                 Output output = getOutput( input.name );
                 if( output == null) {
                     output = new Output();
                     output.name = input.name;
                     output.num = -1;
                     output.access_readonly = false; //!!!

                     outputs.add(output);
                     need_create_tags = true;
                 }

                 inpInput.add(input);
                 tagOutput.add(output);
            }
        }

        if( need_create_tags ) {
            createTagForEachOutputs();
            sortInputOutputArrays();
        }

        for(Output output: outputs) {
            if( output.name.startsWith(CHANNEL_PROVIDER)  ||
                output.name.equals(EXECUTE_BIND_MODULE) )
            {
                continue;
            }

            connector.connect(
                    output.name,
                    output.name,
                    (output.access_readonly ? Connector.Mode.READ : Connector.Mode.WRITE),
                    res);
        }

        if( getOutput(CHANNEL_PROVIDER_IN) != null )
            registerAsChannelProvider( CHANNEL_PROVIDER_IN, Channel.Type.In, res);
        else if( getOutput(CHANNEL_PROVIDER_OUT) != null )
            registerAsChannelProvider( CHANNEL_PROVIDER_OUT, Channel.Type.Out, res);
        else if( getOutput(CHANNEL_PROVIDER_INOUT) != null )
            registerAsChannelProvider( CHANNEL_PROVIDER_INOUT, Channel.Type.InOut, res);

        outputExecuteBindModule = getOutput(EXECUTE_BIND_MODULE);
    }



    @Override
    public boolean execute() {

        for(int i=0; i<inpInput.size(); ++i)
            tagOutput.get(i).tag.setInt( inpInput.get(i).getInt() );

        super.execute();

        if( outputExecuteBindModule != null  &&  outputExecuteBindModule.tag.getBool() )
            if( connector.getBindmod() != null ) {
                connector.getBindmod().execute();
                super.execute();
            }

        return true;
    }


}