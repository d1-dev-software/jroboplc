package promauto.jroboplc.plugin.roboplant;

import promauto.jroboplc.core.api.EnvironmentInst;
import promauto.jroboplc.core.api.Tag;
import promauto.jroboplc.core.tags.Ref;

public class DeviceTAGA extends Device {
	Input 	inpInput;
	Input 	inpInpDisable;

	Tag 	tagOutput;
	Tag 	tagDisable;
	Tag		tagLink;
	
	Ref		refTarget = EnvironmentInst.get().getRefFactory().createRef(); // new Ref();

	private String refModuleTagname = "";


	@Override
	public void prepareTags(RefBool res) {

    	inpInput     = getInput("Input"	, res);

    	inpInpDisable = getInput("InpDisable"); // this input can be null!!!
    	
    	tagOutput 	 = getOutputTag( "Output"		, res);
    	tagDisable 	 = getOutputTag( "Disable"		, res);

    	if( outputsByNum.size() > 2 ) {
    		Output outLink = outputsByNum.get(2);
    		tagLink = outLink.tag;
    		refModuleTagname = outLink.name;
    		if( refTarget.init( refModuleTagname ) ) {
				if (refTarget.prepare())
					refTarget.link();
				else
					EnvironmentInst.get().printError(logger, module.getName(), "Bad reference:", devtype + '.' + tagname + " --> " + outLink.name);
			}
    	}

//		tagOutput.setInt(0);
	}



	@Override
	public boolean execute() {
		boolean linked = refTarget.linkIfNotValid();
		tagLink.setBool( linked );
		if( !linked )
			return false;

		if( inpInpDisable != null  &&  !inpInpDisable.isEmpty() )
			tagDisable.setInt( inpInpDisable.getInt() );
		
		if( !inpInput.isEmpty()  &&  tagDisable.getInt() == 0 ) {
			refTarget.getTag().setInt( inpInput.getInt() );
		}
		
		tagOutput.setInt( refTarget.getInt() );

		return true;
	}


	@Override
	public String checkErrors() {
    	if( refTarget.isValid() )
			return "";
    	else
    		return refModuleTagname + " nolink";
	}
}