package promauto.jroboplc.plugin.roboplant;

import java.util.ArrayList;
import java.util.List;

import promauto.jroboplc.core.api.Tag;

public class DeviceSimpleLogic extends Device {
	Tag tagOutput;
	List<Input> inpInput 	= new ArrayList<>();
	
	
    @Override
	public void prepareTags(RefBool res) {
		tagOutput = getOutputTag( "Output", res);

		inpInput.clear(); 
    	for(Input inp: inputs) 
    		if (inp.name.startsWith("Input"))
    			inpInput.add( inp ); 

		if (res.value) 
			resetState();
	}


	protected void resetState() {
//		tagOutput.setInt(0);
	}
}
