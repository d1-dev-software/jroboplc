package promauto.jroboplc.plugin.roboplant;

import promauto.jroboplc.core.State;
import promauto.jroboplc.core.api.Tag;

public class DeviceMDTA extends Device {
	public static final int FLAG_TRIGGER_ENABLED 		= 1;
	public static final int FLAG_MCHASOST_MODE 			= 0b110; //(1 << 1) + (1 << 2); //2+4;
	public static final int FLAG_NO_DLYOUTPUT 			= 0b1000; //1 << 3;
	public static final int FLAG_DREBEZG_MODE          	= 0x30; //(1 << 4) + (1 << 5);
	public static final int FLAG_NO_OUTPUT 				= 0x40; //1 << 6;
	public static final int FLAG_NO_ALARM 				= 0x80; //1 << 7;
	public static final int FLAG_INVERTED_ALARM 		= 0x100; //1 << 8;
	public static final int FLAG_ALARM_EQUALS_OUTPUT 	= 0x200; //1 << 9;
	public static final int FLAG_USE_STATUS 			= 0x400; //1 << 10;

	public static final int SOST_OK 		= 0;
	public static final int SOST_BAD 		= 1;
	public static final int SOST_OK_OUTPUT 	= 2;
	public static final int SOST_BAD_OUTPUT = 3;
	public static final int SOST_BAD_BLOK 	= 4;
	public static final int SOST_BAD_STATUS	= 5;
	public static final int SOST_BAD_CHANNEL = 0xFE;

	Input inpTrigRes;
	Input inpMchaSost;
	
	Tag tagOutput;
	Tag tagSost;
    Tag tagCnt;
    Tag tagValue;
    Tag tagLow;
    Tag tagHigh;
    Tag tagBlok;
    Tag tagDlyDrebezg;
    Tag tagDlyOutputOff;
    Tag tagDlyOutputOn;
    Channel channel;
    Tag tagFlags;
    Tag tagDebugValue;
    Tag tagPointV;
    Tag tagPointP;
    Tag tagPointV1;
    Tag tagPointP1;
    Tag tagPrimeval;
	Tag tagAlarm;
	Tag tagTrigRes;

	int sost;
	boolean val_ok_old;
	int cntDrebezg;
	int cntStart;
	int cntOutputOn;
	int cntOutputOff;


	@Override
	public void prepareTags(RefBool res) {
    	createTagDescr();


    	inpTrigRes		= getInput("TrigRes", res);
    	inpMchaSost		= getInput("MchaSost", res);
    	
    	tagOutput		= getOutputTag(		"Output"			, res);      
    	tagSost			= getOutputTag(		"Sost"			, res);
    	tagCnt			= getOutputTag(		"Cnt"				, res);         
    	tagValue		= getOutputTag(		"Value"			, res);
    	tagLow			= getOutputTag(		"Low"				, res);         
    	tagHigh			= getOutputTag(		"High"			, res);
    	tagBlok			= getOutputTag(		"Blok"			, res);
    	tagDlyDrebezg	= getOutputTag(		"DlyDrebezg"		, res);  
    	tagDlyOutputOff = getOutputTag(		"DlyStart"		, res);
    	tagDlyOutputOn  = getOutputTag(		"DlyOutput"		, res);
    	channel			= createChannel(	"Channel"			, Channel.Type.In, res);    
    	tagFlags	 	= getOutputTag(		"Flags"			, res);
    	tagDebugValue 	= getOutputTag(		"DebugValue"		, res);
    	tagPointV		= getOutputTag(		"PointV"			, res);      
    	tagPointP		= getOutputTag(		"PointP"			, res);      
    	tagPointV1		= getOutputTag(		"PointV1"			, res);     
    	tagPointP1		= getOutputTag(		"PointP1"			, res);     
    	tagPrimeval		= getOutputTag(		"Primeval"		, res);
    	tagAlarm		= getOutputTag(		"Alarm"			, res);

		tagTrigRes      = getOutputTagOrFake("TrigRes");
    	
		if (res.value)
			resetState(); 
	}


	protected void resetState() {
//		tagOutput.setInt(0);
//		tagSost.setInt(0);
//		tagCnt.setInt(0);
//		tagValue.setInt(0);
//		tagPrimeval.setInt(0);
//		tagAlarm.setInt(0);
		tagDebugValue.setInt(0xFFFF);

		sost = SOST_OK;
		val_ok_old = true;
		cntDrebezg = 0;
		cntStart = 0;
		cntOutputOn = 0;
		cntOutputOff = 0;
	}
	
	
	/*
	 * Warning! Code of this method was ported from C++ original RoboplantMtr
	 * sources. Needs get refactored! Do not repeat this style!
	 */
	@Override
	public boolean execute() {

		boolean val_bad, val_ok;
		boolean en_output = false;
		int mchasost_val;
		boolean trr;
		int status;

		int flags = tagFlags.getInt();
//		int sost = tagSost.getInt();
		int output = tagOutput.getInt();

			
		if( !channel.isOk() ) {
			tagSost.setInt(0xfe);
			return false;
		}

		int value = getTagValue().getInt();
//		int value = channel.tagValue.getInt();
//		if (tagDebugValue.getInt() != 0xFFFF)
//			value = tagDebugValue.getInt();


		// линейное преобразование по одной точке
		if (tagPointV.getInt() > 0 && tagPointP.getInt() > 0  &&  (tagPointV.getInt() != tagPointP.getInt())
				&& (tagPointV1.getInt() == 0)  &&  (tagPointP1.getInt() == 0)
		) {
//			value = (int) Math.round((float) value * (float) tagPointV.getInt()	/ tagPointP.getDouble());
			value = (int) Math.round(getTagValue().getDouble() * tagPointV.getDouble()	/ tagPointP.getDouble());
		}

		// линейное преобразование по двум точкам
		else
		if (tagPointV.getInt() > 0 || tagPointP.getInt() > 0  || tagPointV1.getInt() > 0 || tagPointP1.getInt() > 0 ) {
			double r = 0;
			double r1 = tagPointP1.getDouble() - tagPointP.getDouble();
			double r2 = getTagValue().getDouble() - tagPointP.getDouble();
			double r3 = tagPointV1.getDouble() - tagPointV.getDouble();

			if (r1 != 0) {
				value = (int) Math.round(r2 / r1 * r3 + tagPointV.getDouble());
				if (r < 0)
					value = 0;
			} else
				value = 0;
		}

		// Плохой статус полученого из канала значения
		boolean badStatus = (flags & FLAG_USE_STATUS) > 0  &&  channel.tagValue.getStatus() != Tag.Status.Good;
		if( badStatus ) {
			val_ok = false;
		} else {
			// 3. Определение попадания Value в заданный диапазон
			val_ok = (value >= tagLow.getInt()) && (value <= tagHigh.getInt());
		}

		if (val_ok != val_ok_old) {
			int drebezg_mode = flags & FLAG_DREBEZG_MODE;

			if (((drebezg_mode == 0x00) && (val_ok))
					|| ((drebezg_mode == 0x10) && (val_ok_old))
					|| (drebezg_mode == 0x30))
				val_ok_old = val_ok;
		}

		// 4.Антидребезг
		if (val_ok != val_ok_old) {

			if (cntDrebezg < tagDlyDrebezg.getInt()) {
				val_ok = val_ok_old;
				cntDrebezg++;
			} else {
				val_ok_old = val_ok;
				cntDrebezg = 0;
			}

		} else {
			cntDrebezg = 0;
		}

		val_bad = (!val_ok) && (!tagBlok.getBool());

		// 5. MchaSost
		mchasost_val = inpMchaSost.getInt();

		// 6. en_output
		int mchaSostMode = flags & FLAG_MCHASOST_MODE; //6;

		if (mchaSostMode == 0)
			en_output = true;
		else if (mchaSostMode == 2)
			en_output = (mchasost_val == 1);
		else if (mchaSostMode == 4)
			en_output = (mchasost_val == 1) || (mchasost_val == 2);
		else
			en_output = true;

		// 7. Trigres
		trr = inpTrigRes.tag.getBool()  ||  tagTrigRes.getBool()  ||
				(mchaSostMode > 0  &&  (mchasost_val == 0 || mchasost_val == 7));

		// 8. output
		boolean new_output = false;
		boolean useTrig = (flags & FLAG_TRIGGER_ENABLED) > 0;
		boolean outputAlwaysZero = (flags & FLAG_NO_OUTPUT) > 0;
		if( output>0 ) {
			if( trr  ||  (!useTrig  &&  !(val_bad  &&  en_output)) ) {
				if( cntOutputOff++ >= tagDlyOutputOff.getInt() ) {
					output = 0;
					cntOutputOff = 0;
				}
			} else {
				cntOutputOff = 0;
			}
		} else {
			if( !trr  &&  val_bad  &&  en_output  &&  !outputAlwaysZero) {
				new_output = true;
			}
			cntOutputOff = 0;
		}
//		Old approach:
//		boolean new_output = false;
//		status = ((trr ? 1 : 0) << 3) + ((flags & 1) << 2) + ((val_bad ? 1 : 0) << 1) + (en_output ? 1 : 0);
//		if (output > 0) {
//			if( (status >= 0 && status < 3)  ||  (status > 7 && status < 16) )
//				output = 0;
//		} else {
//			if( (status == 3 || status == 7)  &&  (flags & 0x40) == 0 )
//				new_output = true;
//		}
		if (((flags & (FLAG_TRIGGER_ENABLED + FLAG_MCHASOST_MODE)) == 5) && (!en_output))
			new_output = false;

		// 10. Задержка DlyOutput
		if (new_output) {
			if (((flags & 0xF) == 0xD) && (mchasost_val == 2) && (output == 0))
				cntOutputOn = tagDlyOutputOn.getInt();

			if (cntOutputOn < tagDlyOutputOn.getInt())
				cntOutputOn++;
			else {
				output = 1;
				cntOutputOn = 0;
			}
		} else
			cntOutputOn = 0;

		// 11. Sost
		if( !tagBlok.getBool()  &&  badStatus) {
			sost = SOST_BAD_STATUS;
		} else
		if( tagBlok.getBool()  &&  !val_ok) {
			sost = SOST_BAD_BLOK;
		} else
		if (val_bad) {
			sost = output > 0 ? SOST_BAD_OUTPUT : SOST_BAD;
		} else {
			sost = output > 0 ? SOST_OK_OUTPUT : SOST_OK;
		}

		// 12. Alarm
		boolean new_alarm = false;
		if( (flags & FLAG_ALARM_EQUALS_OUTPUT) > 0) {
			new_alarm = (output > 0)  &&  ((flags & FLAG_NO_ALARM) == 0);
		} else {
			if (en_output || output > 0) {
				if ((flags & FLAG_NO_ALARM) == 0) {
					new_alarm = (val_bad) ^ ((flags & FLAG_INVERTED_ALARM) > 0);
				}
			}
		}
		new_alarm &= !tagBlok.getBool();
		tagAlarm.setBool(new_alarm);


		tagCnt.setInt(cntDrebezg + cntOutputOn);

		tagOutput.setInt(output);
		tagSost.setInt(sost);
		tagValue.setInt(value);
		tagPrimeval.setInt(channel.tagValue.getInt());
		tagTrigRes.setInt(0);

		return true;
	}


	private Tag getTagValue() {
		if (tagDebugValue.getInt() == 0xFFFF)
			return channel.tagValue;
		else
			return tagDebugValue;
	}

	
	@Override
	public void saveStateExtra(State state) {
		state.saveVar("sost", 		sost       );
		state.saveVar("val_ok_old", val_ok_old );
		state.saveVar("cntDrebezg", cntDrebezg );
		state.saveVar("cntStart", 	cntStart   );
		state.saveVar("cntOutputOn", cntOutputOn);
		state.saveVar("cntOutputOff", 	cntOutputOff  );
	}

	@Override
	public void loadStateExtra(State state) {
	    sost       = state.loadVar("sost", 		 sost       );
	    val_ok_old = state.loadVar("val_ok_old", val_ok_old );
	    cntDrebezg = state.loadVar("cntDrebezg", cntDrebezg );
	    cntStart   = state.loadVar("cntStart", 	 cntStart   );
		cntOutputOn = state.loadVar("cntOutputOn", cntOutputOn);
		cntOutputOff  = state.loadVar("cntOutputOff",  cntOutputOff  );
	}
   
}