package promauto.jroboplc.plugin.roboplant;

import promauto.jroboplc.core.api.Tag;

public class DeviceOPER extends Device {
	public static final int OPER_EQUAL = 0;
	public static final int OPER_GREATER_OR_EQUAL = 1;
	public static final int OPER_LOWER_OR_EQUAL = 2;
	public static final int OPER_GREATER = 3;
	public static final int OPER_LOWER = 4;
	public static final int OPER_SUM = 5;
	public static final int OPER_SUB = 6;
	public static final int OPER_XOR = 7;
	public static final int OPER_MUL = 8;
	public static final int OPER_DIV = 9;
	public static final int OPER_MOD = 10;
	public static final int OPER_AND = 11;
	public static final int OPER_OR = 12;
	public static final int OPER_PERCENT = 13;
	public static final int OPER_TEST_BITS = 14;
	Input 	inpInput1;
	Input 	inpInput2;
	Tag 	tagOutput;
	Tag 	tagOper;

	Runnable oper;
	int operValue;

    @Override
	public void prepareTags(RefBool res) {

    	inpInput1 = getInput( "Input1", res);
    	inpInput2 = getInput( "Input2", res);
    	
    	tagOutput	 = getOutputTag( "Output"		, res);
    	tagOper		 = getOutputTag( "Oper"		, res);

		if (res.value) 
			resetState();
	}


	protected void resetState() {
//		tagOutput.setInt(0);
		setOper();
	}

	private void setOper() {
		Tag.Type operType;

		if( (inpInput1.tag.getType() == Tag.Type.STRING  ||  inpInput2.tag.getType() == Tag.Type.STRING) ) {
			operType = Tag.Type.STRING;
		} else

		if( (inpInput1.tag.getType() == Tag.Type.DOUBLE  ||  inpInput2.tag.getType() == Tag.Type.DOUBLE) ) {
			operType = Tag.Type.DOUBLE;
		} else

		if( (inpInput1.tag.getType() == Tag.Type.LONG  ||  inpInput2.tag.getType() == Tag.Type.LONG) ) {
			operType = Tag.Type.LONG;
		} else

		{
			operType = Tag.Type.INT;
		}

		oper = null;
		operValue = tagOper.getInt();

		switch (operValue) {

			case OPER_EQUAL:
				// cmp =
				switch (operType) {
					case INT:
						oper = () -> tagOutput.setBool( inpInput1.getInt() == inpInput2.getInt() );
						break;
					case LONG:
						oper = () -> tagOutput.setBool( inpInput1.getLong() == inpInput2.getLong() );
						break;
					case DOUBLE:
						oper = () -> tagOutput.setBool( inpInput1.getDouble() == inpInput2.getDouble() );
						break;
				}
				break;

			case OPER_GREATER_OR_EQUAL:
				// cmp >=
				switch (operType) {
					case INT:
						oper = () -> tagOutput.setBool( inpInput1.getInt() >= inpInput2.getInt() );
						break;
					case LONG:
						oper = () -> tagOutput.setBool( inpInput1.getLong() >= inpInput2.getLong() );
						break;
					case DOUBLE:
						oper = () -> tagOutput.setBool( inpInput1.getDouble() >= inpInput2.getDouble() );
						break;
				}
				break;

			case OPER_LOWER_OR_EQUAL:
				// cmp <=
				switch (operType) {
					case INT:
						oper = () -> tagOutput.setBool( inpInput1.getInt() <= inpInput2.getInt() );
						break;
					case LONG:
						oper = () -> tagOutput.setBool( inpInput1.getLong() <= inpInput2.getLong() );
						break;
					case DOUBLE:
						oper = () -> tagOutput.setBool( inpInput1.getDouble() <= inpInput2.getDouble() );
						break;
				}
				break;

			case OPER_GREATER:
				// cmp >
				switch (operType) {
					case INT:
						oper = () -> tagOutput.setBool( inpInput1.getInt() > inpInput2.getInt() );
						break;
					case LONG:
						oper = () -> tagOutput.setBool( inpInput1.getLong() > inpInput2.getLong() );
						break;
					case DOUBLE:
						oper = () -> tagOutput.setBool( inpInput1.getDouble() > inpInput2.getDouble() );
						break;
				}
				break;

			case OPER_LOWER:
				// cmp <
				switch (operType) {
					case INT:
						oper = () -> tagOutput.setBool( inpInput1.getInt() < inpInput2.getInt() );
						break;
					case LONG:
						oper = () -> tagOutput.setBool( inpInput1.getLong() < inpInput2.getLong() );
						break;
					case DOUBLE:
						oper = () -> tagOutput.setBool( inpInput1.getDouble() < inpInput2.getDouble() );
						break;
				}
				break;

			case OPER_SUM:
				// +
				switch (operType) {
					case INT:
						oper = () -> tagOutput.setInt( inpInput1.getInt() + inpInput2.getInt() );
						break;
					case LONG:
						oper = () -> tagOutput.setLong( inpInput1.getLong() + inpInput2.getLong() );
						break;
					case DOUBLE:
						oper = () -> tagOutput.setDouble( inpInput1.getDouble() + inpInput2.getDouble() );
						break;
				}
				break;

			case OPER_SUB:
				// -
				switch (operType) {
					case INT:
						oper = () -> tagOutput.setInt( inpInput1.getInt() - inpInput2.getInt() );
						break;
					case LONG:
						oper = () -> tagOutput.setLong( inpInput1.getLong() - inpInput2.getLong() );
						break;
					case DOUBLE:
						oper = () -> tagOutput.setDouble( inpInput1.getDouble() - inpInput2.getDouble() );
						break;
				}
				break;
				//value = ((inp1 < inp2) ? 0 : (inp1 - inp2));

			case OPER_XOR:
				// xor
				switch (operType) {
					case INT:
						oper = () -> tagOutput.setInt( inpInput1.getInt() ^ inpInput2.getInt() );
						break;
					case LONG:
					case DOUBLE:
						oper = () -> tagOutput.setLong( inpInput1.getLong() ^ inpInput2.getLong() );
						break;
				}
				break;

			case OPER_MUL:
				// *
				switch (operType) {
					case INT:
						oper = () -> tagOutput.setInt( inpInput1.getInt() * inpInput2.getInt() );
						break;
					case LONG:
						oper = () -> tagOutput.setLong( inpInput1.getLong() * inpInput2.getLong() );
						break;
					case DOUBLE:
						oper = () -> tagOutput.setDouble( inpInput1.getDouble() * inpInput2.getDouble() );
						break;
				}
				break;

			case OPER_DIV:
				// div
				switch (operType) {
					case INT:
						oper = () -> tagOutput.setInt( inpInput2.getInt() == 0? 0: inpInput1.getInt() / inpInput2.getInt() );
						break;
					case LONG:
						oper = () -> tagOutput.setLong( inpInput2.getLong() == 0? 0:  inpInput1.getLong() / inpInput2.getLong() );
						break;
					case DOUBLE:
						oper = () -> tagOutput.setDouble( inpInput2.getDouble() == 0? 0:  inpInput1.getDouble() / inpInput2.getDouble() );
						break;
				}
				break;

			case OPER_MOD:
				// mod
				switch (operType) {
					case INT:
						oper = () -> tagOutput.setInt( inpInput2.getInt() == 0? 0: inpInput1.getInt() % inpInput2.getInt() );
						break;
					case LONG:
						oper = () -> tagOutput.setLong( inpInput2.getLong() == 0? 0:  inpInput1.getLong() % inpInput2.getLong() );
						break;
					case DOUBLE:
						oper = () -> tagOutput.setDouble( inpInput2.getDouble() == 0? 0:  inpInput1.getDouble() % inpInput2.getDouble() );
						break;
				}
				break;

			case OPER_AND:
				// &
				switch (operType) {
					case INT:
						oper = () -> tagOutput.setInt( inpInput1.getInt() & inpInput2.getInt() );
						break;
					case LONG:
					case DOUBLE:
						oper = () -> tagOutput.setLong( inpInput1.getLong() & inpInput2.getLong() );
						break;
				}
				break;

			case OPER_OR:
				// |
				switch (operType) {
					case INT:
						oper = () -> tagOutput.setInt( inpInput1.getInt() | inpInput2.getInt() );
						break;
					case LONG:
					case DOUBLE:
						oper = () -> tagOutput.setLong( inpInput1.getLong() | inpInput2.getLong() );
						break;
				}
				break;

			case OPER_PERCENT:
				// % - WRONG FORMULA!!!
				switch (operType) {
					case INT:
						oper = () -> tagOutput.setInt( inpInput1.getInt() * inpInput2.getInt() / 100 );
						break;
					case LONG:
						oper = () -> tagOutput.setLong( inpInput1.getLong() * inpInput2.getLong() / 100 );
						break;
					case DOUBLE:
						oper = () -> tagOutput.setDouble( inpInput1.getDouble() * inpInput2.getDouble() / 100 );
						break;
				}
				break;
				//value = inp1 * inp2 / 100;

			case OPER_TEST_BITS:
				// test bits
				switch (operType) {
					case INT:
						oper = () -> tagOutput.setBool( (inpInput1.getInt() & inpInput2.getInt()) == inpInput2.getInt() );
						break;
					case LONG:
					case DOUBLE:
						oper = () -> tagOutput.setBool( (inpInput1.getLong() & inpInput2.getLong()) == inpInput2.getLong() );
						break;
				}
				break;

		}

		if( oper == null )
			oper = () -> tagOutput.setInt(0);

	}

	@Override
	public boolean execute() {
		if( operValue != tagOper.getInt() )
			setOper();

		oper.run();
		return true;
	}

}