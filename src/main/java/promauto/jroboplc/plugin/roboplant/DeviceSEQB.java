package promauto.jroboplc.plugin.roboplant;

import java.util.Map;

import promauto.jroboplc.core.State;
import promauto.jroboplc.core.api.Tag;

/**
 * @author denis
 */
public class DeviceSEQB extends Device {
	Input inpStart;
	Input inpStop;
	Input inpStopEmrg;
	Input inpSusp;
	Input inpSuspStart;
	Input inpZdvOk;
	Input inpDvuOk;
	Input inpAuxOk;
	Input inpAspOk;
	
	Tag tagSost;
    Tag tagSetZdv;
    Tag tagCntOn;
    Tag tagCntOff;
    Tag tagCnt;
    Tag tagCntStop;
    Tag tagZdvBad;
    Tag tagDvuBad;
    Tag tagAuxBad;
    Tag tagAspBad;
    Tag tagReadyToStart;
    Tag tagStoping;
    Tag tagWaitZdvOk;
    Tag tagQntOn;
    Tag tagQntOff;
    Tag tagDlyStart;
    Tag tagDlyStopZdv;
    Tag tagDlyStopDvu;
    Tag tagDlyStopAux;
    Tag tagDlyStopAsp;
    Tag tagFlags;
    Tag tagStartSpeed;
    Tag tagStopSpeed;
    Tag tagDlyMesZdv;
    Tag tagDlyMesDvu;
    Tag tagDlyMesAux;
    Tag tagDlyMesAsp;
    
    int cntStopZdv;
    int cntStopDvu;
    int cntStopAux;
    int cntStopAsp;
    int cntMesZdv;
    int cntMesDvu;
    int cntMesAux;
    int cntMesAsp;
    
    int sost;
    int cntOn;  
    int cntOff;
    int cnt;    
    int cntStop;


    
	@Override
	public void prepareTags(RefBool res) {
		inpStart     = getInput("Start"		, res);
		inpStop      = getInput("Stop"		, res);
		inpStopEmrg  = getInput("StopEmrg"	, res);
		inpSusp      = getInput("Susp"		, res);
		inpSuspStart = getInput("SuspStart"	, res);
		inpZdvOk     = getInput("ZdvOk"		, res);
		inpDvuOk     = getInput("DvuOk"		, res);
		inpAuxOk     = getInput("AuxOk"		, res);
		inpAspOk     = getInput("AspOk"		, res);

		tagSost            		= getOutputTag("Sost"		, res);
		tagSetZdv          		= getOutputTag("SetZdv"		, res);
		tagCntOn           		= getOutputTag("CntOn"		, res);
		tagCntOff          		= getOutputTag("CntOff"		, res);
		tagCnt             		= getOutputTag("Cnt"		, res);
		tagCntStop         		= getOutputTag("CntStop"	, res);
		tagZdvBad          		= getOutputTag("ZdvBad"		, res);
		tagDvuBad          		= getOutputTag("DvuBad"		, res);
		tagAuxBad          		= getOutputTag("AuxBad"		, res);
		tagAspBad          		= getOutputTag("AspBad"		, res);
		tagReadyToStart    		= getOutputTag("ReadyToStart", res);
		tagStoping         		= getOutputTag("Stoping"	, res);
		tagWaitZdvOk       		= getOutputTag("WaitZdvOk"	, res);
		tagQntOn           		= getOutputTag("QntOn"		, res);
		tagQntOff          		= getOutputTag("QntOff"		, res);
		tagDlyStart        		= getOutputTag("DlyStart"	, res);
		tagDlyStopZdv      		= getOutputTag("DlyStopZdv"	, res);
		tagDlyStopDvu      		= getOutputTag("DlyStopDvu"	, res);
		tagDlyStopAux      		= getOutputTag("DlyStopAux"	, res);
		tagDlyStopAsp      		= getOutputTag("DlyStopAsp"	, res);
		tagFlags           		= getOutputTag("Flags"		, res);
		tagStartSpeed      		= getOutputTag("StartSpeed"	, res);
		tagStopSpeed       		= getOutputTag("StopSpeed"	, res);
		tagDlyMesZdv       		= getOutputTag("DlyMesZdv"	, res);
		tagDlyMesDvu       		= getOutputTag("DlyMesDvu"	, res);
		tagDlyMesAux       		= getOutputTag("DlyMesAux"	, res);
		tagDlyMesAsp       		= getOutputTag("DlyMesAsp"	, res);
	
//		if (res.value)
//			resetState();

	    cntStopZdv = 0;
	    cntStopDvu = 0;
	    cntStopAux = 0;
	    cntStopAsp = 0;
	    cntMesZdv = 0;
	    cntMesDvu = 0;
	    cntMesAux = 0;
	    cntMesAsp = 0;

	    sost = 0;
	    cntOn = 0;
	    cntOff = 0;
	    cnt = 0;
	    cntStop = 0;
	}
	
//	protected void resetState() {
//		tagSost.setInt(0);
//		tagSetZdv.setInt(0);
//		tagCntOn.setInt(0);
//		tagCntOff.setInt(0);
//		tagCnt.setInt(0);
//		tagCntStop.setInt(0);
//		tagZdvBad.setInt(0);
//		tagDvuBad.setInt(0);
//		tagAuxBad.setInt(0);
//		tagAspBad.setInt(0);
//		tagReadyToStart.setInt(0);
//		tagStoping.setInt(0);
//		tagWaitZdvOk.setInt(0);
//
//	    cntStopZdv = 0;
//	    cntStopDvu = 0;
//	    cntStopAux = 0;
//	    cntStopAsp = 0;
//	    cntMesZdv = 0;
//	    cntMesDvu = 0;
//	    cntMesAux = 0;
//	    cntMesAsp = 0;
//
//	    sost = 0;
//	    cntOn = 0;
//	    cntOff = 0;
//	    cnt = 0;
//	    cntStop = 0;
//	}
	
	
	/*
	 * Warning! The source code of this method was ported from C++ original
	 * RoboplantMtr sources. Needs get refactored! Do not repeat this style!
	 */
	// @Override
	public boolean execute() {

		int flags = tagFlags.getInt();

		boolean valStart = inpStart.getInt() > 0;
		boolean valStop = inpStop.getInt() > 0;
		boolean valStopEmrg = inpStopEmrg.getInt() > 0;
		boolean valSusp = inpSusp.getInt() > 0;
		boolean valSuspStart = inpSuspStart.getInt() > 0;
		boolean valZdvOk = inpZdvOk.getInt() > 0;
		boolean valDvuOk = inpDvuOk.getInt() > 0;
		boolean valAuxOk = inpAuxOk.getInt() > 0;
		boolean valAspOk = inpAspOk.getInt() > 0;
		boolean valZdvDvuAuxOk = (valZdvOk) && (valDvuOk) && (valAuxOk);

		if (valStopEmrg)
			sost = 0;

		// Стоп / Выключен
		if (sost == 0) {
			cnt = 0;
			cntOn = 0;
			cntOff = 0;
			cntStop = 0;

			if ((valStart) && (!valStop) && (!valStopEmrg)) {
				sost = 1;
			}
		}

		// Ожидание установки задвижек / уровней бункеров / вспомогательного
		// оборудования
		if (sost == 1) {
			if (valZdvDvuAuxOk) {
				if (cnt < tagDlyStart.getInt())
					sost = 31;
				else
					sost = 2;
			} else if (cnt < tagDlyStart.getInt())
				cnt++;
			if (valStop)
				sost = 61;
		}

		// Готов к старту
		if (sost == 2) {
			if (!valZdvDvuAuxOk) {
				sost = 1;
			} else {
				if (valStart) {
					sost = 31;
				}
			}
			if (valStop)
				sost = 61;
		}

		// Подготовка к старту
		if (sost == 31) {
			sost = 3;

			cntStopZdv = 0;
			cntStopDvu = 0;
			cntStopAux = 0;
			cntStopAsp = 0;

			cntOn = 0;
			cnt = 0;
		}

		// Если запуск или работа, проверить на необходимость останова
		if ((sost >= 3) && (sost <= 5)) {
			int tmpCntStop = 0xFFFF;

			if ((flags & 1) > 0) {
				if (check_zdv_dvu(valZdvOk, tagDlyStopZdv.getInt(), cntStopZdv,
						tmpCntStop))
					sost = 61;
				cntStopZdv = check_zdv_dvu_cntStop;
				tmpCntStop = check_zdv_dvu_tmpCntStop;
			}

			if ((flags & 2) > 0) {
				if (check_zdv_dvu(valDvuOk, tagDlyStopDvu.getInt(), cntStopDvu,
						tmpCntStop))
					sost = 61;
				cntStopDvu = check_zdv_dvu_cntStop;
				tmpCntStop = check_zdv_dvu_tmpCntStop;
			}

			if ((flags & 4) > 0) {
				if (check_zdv_dvu(valAuxOk, tagDlyStopAux.getInt(), cntStopAux,
						tmpCntStop))
					sost = 61;
				cntStopAux = check_zdv_dvu_cntStop;
				tmpCntStop = check_zdv_dvu_tmpCntStop;
			}

			if (sost > 4)
				if ((flags & 8) > 0) {
					if (check_zdv_dvu(valAspOk, tagDlyStopAsp.getInt(),
							cntStopAsp, tmpCntStop))
						sost = 61;
					cntStopAsp = check_zdv_dvu_cntStop;
					tmpCntStop = check_zdv_dvu_tmpCntStop;
				}

			cntStop = tmpCntStop;

			if (valStop)
				sost = 61;

		}

		// Проверка на приостановку запуска
		if (sost == 3)
			if ((valSusp) || (valSuspStart))
				sost = 4;

		// Запуск
		if (sost == 3) {

			while (cntOn < tagQntOn.getInt()) {
				if (cnt < tagStartSpeed.getInt()) {
					cnt++;
					break;
				} else {
					cntOn++;
					cnt = 0;
				}
			}

			if (cntOn >= tagQntOn.getInt())
				sost = 5;
		}

		// Запуск приостановлен
		if (sost == 4) {
			if (valStart)
				sost = 3;
		}

		// Работает
		if (sost == 5) {
			cntOff = 0;
		}

		// подготовка к останову
		if (sost == 61) {
			cntStop = 0;
			sost = 6;
		}

		// Останов
		if (sost == 6) {
			while (cntOff < tagQntOff.getInt()) {
				if (cnt < tagStopSpeed.getInt()) {
					cnt++;
					break;
				} else {
					cntOff++;
					cnt = 0;
				}
			}

			if (cntOff >= tagQntOff.getInt())
				sost = 0;
		}

		// Останов приостановлен
		if (sost == 7) {
			if (valStop)
				sost = 6;
			if (valStart)
				sost = 31;
		}

		// ------------------------------------------------------------------------------------------------
		if ((sost >= 3) && (sost <= 5)) {
			tagZdvBad.setBool(check_mes_dly(valZdvOk, tagDlyMesZdv.getInt(),
					cntMesZdv));
			cntMesZdv = check_mes_dly_cnt;

			tagDvuBad.setBool(check_mes_dly(valDvuOk, tagDlyMesDvu.getInt(),
					cntMesDvu));
			cntMesDvu = check_mes_dly_cnt;

			tagAuxBad.setBool(check_mes_dly(valAuxOk, tagDlyMesAux.getInt(),
					cntMesAux));
			cntMesAux = check_mes_dly_cnt;
		} else {
			tagZdvBad.setInt(0);
			tagDvuBad.setInt(0);
			tagAuxBad.setInt(0);

			cntMesZdv = 0;
			cntMesDvu = 0;
			cntMesAux = 0;

			cntStop = 0;
		}

		if (sost == 5) {
			tagAspBad.setBool(check_mes_dly(valAspOk, tagDlyMesAsp.getInt(),
					cntMesAsp));
			cntMesAsp = check_mes_dly_cnt;
		} else {
			tagAspBad.setInt(0);
			cntMesAsp = 0;
		}

		tagReadyToStart	.setBool(sost == 2);
		tagStoping		.setBool(sost == 6);
		tagWaitZdvOk	.setBool(sost == 1);
		tagSetZdv		.setBool(sost > 0);
		
		tagSost   .setInt( sost    );   
		tagCntOn  .setInt( cntOn   );
		tagCntOff .setInt( cntOff  );
		tagCnt    .setInt( cnt     );
		tagCntStop.setInt( cntStop );
		

		return true;
	};

	
	
	
	int check_mes_dly_cnt = 0;

	protected boolean check_mes_dly(boolean valOk, int dly, int cnt) {
		boolean flag = false;

		if (valOk)
			cnt = 0;
		else {
			if (cnt < dly) {
				cnt++;
			} else
				flag = true;
		}

		check_mes_dly_cnt = cnt;
		return flag;
	}

	
	
	
	int check_zdv_dvu_cntStop = 0;
	int check_zdv_dvu_tmpCntStop = 0;

	protected boolean check_zdv_dvu(boolean valOk, int DlyStop, int cntStop,
			int tmpCntStop) {
		boolean flagStop = false;

		if (valOk)
			cntStop = 0;
		else if (cntStop < DlyStop) {
			cntStop++;
			if (tmpCntStop > (DlyStop - cntStop))
				tmpCntStop = DlyStop - cntStop;
		} else
			flagStop = true;

		check_zdv_dvu_cntStop 	 = cntStop 	;
		check_zdv_dvu_tmpCntStop = tmpCntStop;
		return flagStop;
	}
	
	
	
	@Override
	public void saveStateExtra(State state) {
		state.saveVar("cntStopZdv", 	cntStopZdv );
		state.saveVar("cntStopDvu", 	cntStopDvu );
		state.saveVar("cntStopAux", 	cntStopAux );
		state.saveVar("cntStopAsp", 	cntStopAsp );
		state.saveVar("cntMesZdv", 		cntMesZdv  );
		state.saveVar("cntMesDvu", 		cntMesDvu  );
		state.saveVar("cntMesAux", 		cntMesAux  );
		state.saveVar("cntMesAsp", 		cntMesAsp  );
		state.saveVar("sost", 			sost       );
		state.saveVar("cntOn", 			cntOn      );
		state.saveVar("cntOff", 		cntOff     );
		state.saveVar("cnt", 			cnt        );
		state.saveVar("cntStop", 		cntStop    );
	}

	@Override
	public void loadStateExtra(State state) {
	    cntStopZdv = state.loadVar("cntStopZdv", 	cntStopZdv	);
	    cntStopDvu = state.loadVar("cntStopDvu", 	cntStopDvu	);
	    cntStopAux = state.loadVar("cntStopAux", 	cntStopAux	);
	    cntStopAsp = state.loadVar("cntStopAsp", 	cntStopAsp	);
	    cntMesZdv  = state.loadVar("cntMesZdv", 	cntMesZdv 	);
	    cntMesDvu  = state.loadVar("cntMesDvu", 	cntMesDvu 	);
	    cntMesAux  = state.loadVar("cntMesAux", 	cntMesAux 	);
	    cntMesAsp  = state.loadVar("cntMesAsp", 	cntMesAsp 	);
	    sost       = state.loadVar("sost", 			sost      	);
	    cntOn      = state.loadVar("cntOn", 		cntOn     	);
	    cntOff     = state.loadVar("cntOff", 		cntOff    	);
	    cnt        = state.loadVar("cnt", 			cnt       	);
	    cntStop    = state.loadVar("cntStop", 		cntStop   	);
	}
}