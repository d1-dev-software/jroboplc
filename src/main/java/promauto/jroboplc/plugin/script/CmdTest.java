package promauto.jroboplc.plugin.script;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import promauto.jroboplc.core.AbstractCommand;
import promauto.jroboplc.core.api.Console;
import promauto.jroboplc.core.api.Plugin;


public class CmdTest extends AbstractCommand {
	final Logger logger = LoggerFactory.getLogger(CmdTest.class);

	@Override
	public String getName() {
		return "test";
	}

	@Override
	public String getUsage() {
		return "";
	}

	@Override
	public String getDescription() {
		return "for developers";
	}

	
	
	@Override
	public String execute(Console console, Plugin plugin, String args) {

		return "test!";
	}

	

}
