package promauto.jroboplc.plugin.script;
import promauto.jroboplc.core.tags.*;

public class ScriptJavaEmpty extends ScriptJava {
///----- paste begin -----

    private final static String MODULE_RD = "rd";
    private final static String MODULE_FAT = "fat";
//    private static final double KOEFF = 1.0;

    private final static int ENABLE_OFF   = 0;
//    private final static int ENABLE_ON    = 1;
//    private final static int ENABLE_DEBUG = 2;


    Ref refFatRun;
    Ref refFatSet;
    Ref refFatCur;

    Ref refCurFlow;
    Ref refErrorFlag;

    Ref refPercent;

    Ref tagStart;
    Ref tagEnable;

    Ref tagZero;
    Ref tagDvUp;
    Ref tagDvDown;
    Ref tagTimeout;
    Ref tagAlarm;
    private int cnt;


    @Override
    public boolean load() {
        refCurFlow = createRef(getArg("src", ""));
        refErrorFlag = createRef(refCurFlow.getRefModuleName(), "SYSTEM.ErrorFlag");
        refPercent = createRef(MODULE_RD, "prm.L" + getId());

        refFatRun = createRef(MODULE_FAT, getId() + "_run");
        refFatSet = createRef(MODULE_FAT, getId() + "_set");
        refFatCur = createRef(MODULE_FAT, getId() + "_cur");

        tagStart  = createTag("Start",    0);
        tagEnable = createTag("Enable",   0);

        tagZero    = createTag("Zero",    0);
        tagDvUp    = createTag("DvUp",    0);
        tagDvDown  = createTag("DvDown",  0);
        tagTimeout = createTag("Timeout", 0);
        tagAlarm   = createTag("Alarm",   0);

        cnt = 0;

        return true;
    }

    @Override
    public void execute() {
        if(refErrorFlag.getBool()  ||  tagEnable.getInt() == ENABLE_OFF) {
            tagAlarm.setInt(0);
            cnt = 0;
            return;
        }

        if( refCurFlow.getDouble() <= Math.max(tagZero.getDouble(), 0) ) {
            refFatSet.setInt(0);
        } else {
            refFatSet.setInt( (int)Math.round(refCurFlow.getDouble() / 100.0 * refPercent.getDouble()) );
        }

        if( refFatCur.getInt() < (refFatSet.getInt() - tagDvDown.getInt())  ||  refFatCur.getInt() > (refFatSet.getInt() + tagDvUp.getInt()) ) {
            if( cnt < tagTimeout.getInt() ) {
                tagAlarm.setInt(0);
                cnt++;
            } else {
                tagAlarm.setInt(1);
            }
        } else {
            tagAlarm.setInt(0);
            cnt = 0;
        }

//        if(tagStart.getBool()) {
//          refFatSet.setInt( (int)Math.round(refCurFlow.getDouble() / 100.0 * refPercent.getDouble()) );
//          refFatRun.setBool(tagStart.getBool());
//        } else {
//         refFatSet.setInt(0);
//         refFatRun.setInt(0);
//        }
    }



///----- paste end -----
}