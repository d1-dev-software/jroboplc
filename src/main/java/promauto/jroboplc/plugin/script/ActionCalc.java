package promauto.jroboplc.plugin.script;

import static promauto.jroboplc.core.api.Tag.Type.*;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import parsii.eval.Expression;
import parsii.eval.Parser;
import parsii.eval.Scope;
import parsii.eval.Variable;
import parsii.tokenizer.ParseException;
import promauto.jroboplc.core.api.Configuration;
import promauto.jroboplc.core.api.EnvironmentInst;
import promauto.jroboplc.core.api.Tag;
import promauto.jroboplc.core.tags.Ref;

public class ActionCalc extends Action {
	private final Logger logger = LoggerFactory.getLogger(ActionCalc.class);
	
	
	static {
		Parser.registerFunction("AND", new FunctionAND());
		Parser.registerFunction("OR", new FunctionOR());
	}

	public static class Expr {
//		public Scope scope = Scope.create();
		public Scope scope = new Scope();
		public Expression expr;
		
		public Expr(String expr) throws ParseException {
			this.expr = Parser.parse(expr, scope);
		}
	}
	
	protected static class Argument {
		Ref ref = EnvironmentInst.get().getRefFactory().createRef(); //new Ref();
		Variable var;
		double value;
		
		public Argument(String nameVar, Scope scope) {
			var = scope.getVariable(nameVar);
		}
	}

	
	protected Expr expr;
	private double lastExprResult = 0.0D;
	private boolean hasExprResult = false;
	
	
	protected Ref rslt = EnvironmentInst.get().getRefFactory().createRef(); //new Ref();
	protected List<Argument> args = new LinkedList<>();
	

	
	private ScriptModule module;

	
	public static void loadExprs(Object conf, Map<String, Expr> calcexprs) throws ParseException {
		Configuration cm = EnvironmentInst.get().getConfiguration();
		
		Map<String,Object> conf_calcexpr = cm.toMap( cm.get(conf, "calcexpr") );
		for(Map.Entry<String,Object> ent: conf_calcexpr.entrySet() ) {
			Expr expr = new Expr(ent.getValue().toString());
			calcexprs.put( ent.getKey().toString(), expr);
		}
	}

	
	public ActionCalc(ScriptModule module) {
		this.module = module;
	}

	@Override
	public boolean load(Object conf) {
		super.load(conf);
		
		Configuration cm = EnvironmentInst.get().getConfiguration();
		
		String calc = cm.get(conf, "expr", "");
		expr = module.getCalcExpr(calc);
		if( expr == null ) 
			try {
				expr = new Expr(calc);
			} catch (ParseException e) {
				EnvironmentInst.get().printError(logger, e, module.getName(), getId(), "calc:", calc);
				return false;
			}
		
		if( !rslt.init(conf, "result", module) )
			return false;
		
		if( rslt.getRefModuleName().equals(module.getName()) )
			if( module.getTagTable().get(rslt.getRefTagName()) == null )
				module.getTagTable().createDouble(rslt.getRefTagName(), 0d);
		
		Set<String> varnames = expr.scope.getLocalNames();
		for(String varname: varnames) {
			Argument arg = new Argument(varname, expr.scope);
			if( !arg.ref.init(conf, varname, module) )
				return false;
			args.add(arg);
		}
		
		return true;
	}

	@Override
	public boolean prepare() {
		lastExprResult = 0.0D;
		hasExprResult = false;
		
		if(!rslt.prepare())
			return false;
		rslt.link();
		
		for(Argument arg: args)
			if(arg.ref.prepare())
				arg.ref.link();
			else
				return false;
		
		return true;
	}

	@Override
	public void execute() {
		if( !rslt.linkIfNotValid() )
			return;
		
		for(Argument arg: args)
			if( !arg.ref.linkIfNotValid() )
				return;
			else
				if( arg.value != arg.ref.getTag().getDouble() ) {
					arg.value = arg.ref.getTag().getDouble();
					hasExprResult = false;
				}
		
		if( hasExprResult ) {
			if( rslt.getTag().getDouble() != lastExprResult ) 
				updateResult();
			
			return;
		}

		for(Argument arg: args)
			arg.var.setValue( arg.value );

		try {
			lastExprResult = expr.expr.evaluate();
		}catch (Exception e) {
			EnvironmentInst.get().printError(logger, e, module.getName(), getId() );
		}
		
		Tag.Type rsltType = rslt.getTag().getType();
		if( rsltType != DOUBLE ) {
			if( rsltType == INT )
				lastExprResult = (int)lastExprResult;
			else 
			if( rsltType == LONG )
				lastExprResult = (long)lastExprResult;
			else 
			if( rsltType == BOOL )
				lastExprResult = lastExprResult != 0? 1.0: 0.0;
		}
		
		hasExprResult = true;
		
		updateResult();
	}


	private void updateResult() {
		if( delay_ms > 0) {
			if( timeDelay == 0)
				timeDelay = System.currentTimeMillis();
			else {
				if( (System.currentTimeMillis() - timeDelay) > delay_ms ) {
					rslt.getTag().setDouble( lastExprResult );
//					lastExprResult = rslt.getTag().getDouble();
					timeDelay = 0;
				}
			}
		} else {
			rslt.getTag().setDouble( lastExprResult );
//			lastExprResult = rslt.getTag().getDouble();
		}
	}

	@Override
	public boolean isValid() {
		if( !rslt.isValid() )
			return false;
		
		for(Argument arg: args)
			if( !arg.ref.isValid() )
				return false;
		
		return true;
	}

	@Override
	public String getError() {
		String res = "calc " + getId() + ": ";
		
		if( !rslt.isValid() )
			res = "result=" + rslt.getName() + " ";
		
		for(Argument arg: args)
			if( !arg.ref.isValid() )
				res = res + arg.var.getName() +  "=" + arg.ref.getName() + " ";
		
		return res;
	}


	/////////////////////////////////////////// FUNCTIONS /////////////////////////////////////////////////
	
	public static class FunctionAND implements parsii.eval.Function {
		@Override
		public int getNumberOfArguments() {
			return 2;
		}

		@Override
		public double eval(List<Expression> args) {
			return (double)(((int)(args.get(0).evaluate()))  &  ((int)(args.get(1).evaluate())));
		}

		@Override
		public boolean isNaturalFunction() {
			return true;
		}
	}

	
	
	public static class FunctionOR implements parsii.eval.Function {
		@Override
		public int getNumberOfArguments() {
			return 2;
		}

		@Override
		public double eval(List<Expression> args) {
			return (double)(((int)(args.get(0).evaluate()))  |  ((int)(args.get(1).evaluate())));
		}

		@Override
		public boolean isNaturalFunction() {
			return true;
		}
	}

}

