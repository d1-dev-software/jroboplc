package promauto.jroboplc.plugin.script.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import promauto.jroboplc.core.api.Database;
import promauto.jroboplc.core.api.Flags;
import promauto.jroboplc.core.tags.Ref;
import promauto.jroboplc.plugin.script.ScriptJava;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.format.DateTimeFormatter;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/*
Usage example:
 */

public class CsvToSqlImportScript extends ScriptJava {
    private static final int MAX_FILES_IN_DIR = 80;
    private final Logger logger = LoggerFactory.getLogger(CsvToSqlImportScript.class);

    static private final DateTimeFormatter dtFormatter = DateTimeFormatter.ofPattern("_yyyyMMdd_HHmmss");

    Ref tagCnt;
    Ref tagEnable;

    Database db;
    String dbname;
    String sql;
    String dir;
    String filemask;



    @Override
    public boolean load() {
        tagCnt = this.createTag("cnt", 0);
        tagEnable = this.createTag("enable", 0, Flags.AUTOSAVE);
        dbname = getArg("db", "db");
        db = null;
        sql = getArg("sql", "");
        dir = getArg("dir", "");
        filemask = getArg("filemask", "");

        return true;
    }

    @Override
    public boolean prepare() {
        db = getDatabase(dbname);
        if (db == null) {
            printError("Database module is not found: " + dbname);
            return false;
        }
        return true;
    }

    @Override
    public void execute() {
        if (!db.isConnected()  ||  !tagEnable.getBool() )
            return;

        // iterate over directory
        Set<String> filenames = new TreeSet<String>();
        try (DirectoryStream<Path> ds = Files.newDirectoryStream(Paths.get(dir), filemask)) {
            ds.forEach(path -> filenames.add(path.toString()));
            for (String filename : filenames) {
                tagCnt.setInt( tagCnt.getInt() + 1 );
                printInfo("Process file: " + filename);
                if (!processFile(Paths.get(filename))) {
                    printError("Import from file failed: " + filename);
                    return;
                }
            }
        } catch (IOException e) {
            printError(e, "Iterate over directory failed: " + dir.toString() + ", mask: " + filemask);
            return;
        }

    }


    private boolean processFile(Path path) {
        String sqlPrepared = "";
        try (Statement st = db.getConnection().createStatement(); Stream<String> lines = Files.lines(path)) {

            for (String line : lines.collect(Collectors.toList())) {
                printInfo(line);
                String[] values = line.split(";");
                for(int i = 0; i < values.length; ++i)
                    if (values[i].isEmpty())
                        values[i] = "0";
                sqlPrepared = String.format(sql, (Object[]) values);
//                System.out.println(sqlPrepared);
                st.executeUpdate(sqlPrepared);
            }
            db.commit();

            Files.delete(path);

        } catch (IOException | SQLException e) {
            printError(e, "Process file failed: " + path.toString() + ", sql: " + sqlPrepared);
            try {
                Files.move(path, Paths.get(path.toString() + ".failed"));
            } catch (IOException ex) {
                printError(e, "Rename failed: " + path.toString());
            }
            return false;
        }
/*
update or insert rd_product (code, name, prodtype_id, fin, deleted) values ('%s', '%s', %s, %s, %s) matching (code)
*/
        return true;
    }


}

