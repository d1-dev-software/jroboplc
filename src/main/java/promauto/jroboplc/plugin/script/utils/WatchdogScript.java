package promauto.jroboplc.plugin.script.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import promauto.jroboplc.plugin.script.*;
import promauto.jroboplc.core.tags.*;
import promauto.jroboplc.core.api.*;

import java.io.IOException;

/*
Usage example:

watchdog.yml
============

plugin.script:
  module.watchdog:
    actions:
      - java:
          id:      wd
          class:   WatchdogScript
          livetag: jrcl:rj_task_robo.timecounter
          timeout: 30
          cmd:     D:\asutp\visscada\prj-ksnrj\jrobo-datasvr\ctl.win\jroboplc.bat restart

plugin.task:
  module.task_watchdog:
    period:   1000
    modules:
       - watchdog
 */

public class WatchdogScript extends ScriptJava {
    private final Logger logger = LoggerFactory.getLogger(WatchdogScript.class);

    Ref tagAlarm;
    Ref refLiveTag;
    int lastValue;
    long lastTime;
    long timeout;
    String cmd;
    String liveTagName;

    @Override
    public boolean load() {
        tagAlarm = this.createTag("alarm", 0);
        liveTagName = getArg("livetag", "");
        refLiveTag = this.createRef(liveTagName);
        cmd = getArg("cmd", "");
        timeout = getArg("timeout", 10) * 1000L;
        lastValue = 0;
        lastTime = System.currentTimeMillis();
        return true;
    }

    @Override
    public void execute() {
        if( !refLiveTag.isValid() ) {
            tagAlarm.setInt(-1);
            return;
        }

        if (refLiveTag.getInt() == lastValue) {
            if(tagAlarm.getInt() != 1) {
                if( lastTime > System.currentTimeMillis() )
                    lastTime = System.currentTimeMillis();
                if ((System.currentTimeMillis() - lastTime) > timeout) {
                    tagAlarm.setInt(1);
                    if(!cmd.isEmpty()) {
                        try {
                            EnvironmentInst.get().printInfo(logger, "Watchdog fired: " + liveTagName + " = " + refLiveTag.getString());
                            Runtime.getRuntime().exec(cmd);
                        } catch (IOException e) {
                            EnvironmentInst.get().logError(logger, e);
                        }
                    }
                }
            }
        } else {
            lastValue = refLiveTag.getInt();
            tagAlarm.setInt(0);
            lastTime = System.currentTimeMillis();
        }
    }

}