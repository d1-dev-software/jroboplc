package promauto.jroboplc.plugin.script.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import promauto.jroboplc.core.api.Database;
import promauto.jroboplc.core.api.Flags;
import promauto.jroboplc.core.tags.Ref;
import promauto.jroboplc.plugin.script.ScriptJava;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.ResultSet;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/*
Usage example:

plugin.script:
  module.scr:
    actions:
      - java:
          id:      exportShifts
          class:   SqlToCsvExportScript
          db:      db
          filename: d:\1c\shifts
          schedule: "810, 2010"

          table:    rd_statsh
          sql: |
            select
              s.id,
              s.period / 10 dt,
              mod(s.period, 10) shift,
              l.id line_id,
              l.name line_name,
              p.id product_id,
              p.name product_name,
              s.weight,
              r.id recipe_id,
              r.name recipe_name,
              p.prodtype_id
            from rd_statsh s
            join rd_product p on p.id=s.product_id
            join rd_line l on l.id=s.line_id
            left join rd_recipe r on r.id=s.recipe_id
            where s.data_export_mark = 0
            order by s.period, s.id

      - java:
          id:      exportHours
          class:   SqlToCsvExportScript
          db:      db
          filename: d:\1c\hours
          schedule: "0010, 0110, 0210, 0310, 0410, 0510, 0610, 0710, 0810, 0910, 1010, 1110, 1210, 1310, 1410, 1510, 1610, 1710, 1810, 1910, 2010, 2110, 2210, 2310"

          table:    rd_stathr
          sql: |
            select
              s.id,
              s.period / 100 dt,
              mod(s.period, 100) hr,
              l.id line_id,
              l.name line_name,
              p.id product_id,
              p.name product_name,
              s.weight,
              r.id recipe_id,
              r.name recipe_name,
              p.prodtype_id
            from rd_stathr s
            join rd_product p on p.id=s.product_id
            join rd_line l on l.id=s.line_id
            left join rd_recipe r on r.id=s.recipe_id
            where s.data_export_mark = 0
            order by s.period, s.id

 */

public class SqlToCsvExportScript extends ScriptJava {
    private static final int MAX_FILES_IN_DIR = 80;
    private final Logger logger = LoggerFactory.getLogger(SqlToCsvExportScript.class);

    static private final DateTimeFormatter dtFormatter = DateTimeFormatter.ofPattern("_yyyyMMdd_HHmmss");

    Ref tagPeriod;
    Ref tagStart;
    Ref tagSchedule;

    Database db;
    String dbname;
    String sql;
    String filename;
    String table;

    private final List<Integer> times = new ArrayList<>();


    @Override
    public boolean load() {
        tagStart = this.createTag("start", 0);
        tagPeriod = this.createTag("period", 0, Flags.AUTOSAVE);
        table = getArg("table", "");
        dbname = getArg("db", "db");
        db = null;
        sql = getArg("sql", "");
        filename = getArg("filename", "export");
        times.clear();
        Matcher m = Pattern.compile("\\s*(\\d+)\\s*").matcher(getArg("schedule", ""));
        while( m.find() )
            times.add(Integer.parseInt(m.group(1)));

        tagSchedule = this.createRWTag("schedule",
                times.stream().map(String::valueOf).collect(Collectors.joining(";")));

        return true;
    }

    @Override
    public boolean prepare() {
        db = getDatabase(dbname);
        if (db == null) {
            printError("Database module is not found: " + dbname);
            return false;
        }
        return true;
    }

    public int calcPeriod(LocalDateTime dt) {
        int hhmm = dt.getHour() * 100 + dt.getMinute();

        int i = times.size();
        int j = 0;
        for(; j < times.size(); ++j) {
            if( hhmm < times.get(j) ) {
                break;
            }
            i = j + 1;
        }

        if(j < i)
            dt = dt.minusDays(1);

        return dt.getYear() * 100000 + dt.getMonthValue() * 1000 + dt.getDayOfMonth() * 10 + i;
    }

    @Override
    public void execute() {
        if( !db.isConnected() )
            return;

        LocalDateTime dt = LocalDateTime.now();
        int period = calcPeriod( dt );
        if( tagStart.getInt() == 0  &&  period == tagPeriod.getInt() )
            return;

        tagStart.setInt(0);
        tagPeriod.setInt(period);

        String filenameCsv = filename + dt.format(dtFormatter) + ".csv";
        String filenameTmp = filenameCsv + ".tmp";

        filenameCsv = makeFilenameNotExists(filenameCsv);
        filenameTmp = makeFilenameNotExists(filenameTmp);

        if( hasTooManyFiles() ) {
            printInfo("Too many files in the directory!");
            return;
        }

        int n = 0;
        boolean res = false;

        try (Statement st = db.getConnection().createStatement(); FileOutputStream os = new FileOutputStream(filenameTmp)) {

            try (ResultSet rs = st.executeQuery(sql)) {
                while (rs.next()) {
                    StringBuilder sb = new StringBuilder();
                    for(int i = 1; i <= rs.getMetaData().getColumnCount(); ++i) {
                        if( i > 1 )
                            sb.append(';');
                        sb.append(rs.getString(i));
                    }
                    sb.append("\r\n");
                    os.write( sb.toString().getBytes(StandardCharsets.UTF_8));
                    ++n;
                }
            }

            st.executeUpdate(String.format("update %s set data_export_mark=%d where data_export_mark=0", table, period));

            db.getConnection().commit();

            printInfo(String.format("%d: Exported %d records into %s", period, n, filenameCsv));

            res = true;
        } catch (Exception e) {
            printError(e, "Request error");
        }

        try {
            Path tmp = Paths.get(filenameTmp);
            Path csv = Paths.get(filenameCsv);


            if( res ) {
                Files.move(tmp, csv);
            } else {
                Files.delete(tmp);
            }
        } catch (IOException e) {
            printError(e, "File error");
        }

    }

    private boolean hasTooManyFiles() {
        Path p = Paths.get(filename).getParent();
        if( p == null )
            return false;

        int cnt = 0;
        try(DirectoryStream<Path> ds = Files.newDirectoryStream(p))  {
            for (Path ignored : ds) {
                cnt++;
            }
        } catch (IOException ignored) {
        }
        return cnt >= MAX_FILES_IN_DIR;
    }

    private String makeFilenameNotExists(String filename) {
        int i = 1;
        while( Files.exists(Paths.get(filename)) ) {
            filename =filename + i;
        }
        return filename;
    }

}

