package promauto.jroboplc.plugin.script;

import promauto.jroboplc.core.api.EnvironmentInst;
import promauto.jroboplc.core.tags.Ref;

public class ActionCopy extends Action {
//	private final Logger logger = LoggerFactory.getLogger(ActionCopy.class);

	private ScriptModule module;
	
	protected Ref src = EnvironmentInst.get().getRefFactory().createRef(); //new Ref();
	protected Ref dst = EnvironmentInst.get().getRefFactory().createRef(); //new Ref();;
	
	

	public ActionCopy(ScriptModule module) {
		this.module = module;
	}
	

	@Override
	public boolean load(Object conf) {
		super.load(conf);

		boolean res = true;
		res &= src.init(conf, "src", module);
		res &= dst.init(conf, "dst", module);
		
		return res;
	}

	@Override
	public boolean prepare() {
		if( !src.prepare()  ||  !dst.prepare() ) 
			return false;
		
		src.link();
		dst.link();
		return true;
	}

	@Override
	public void execute() {
		if( src.linkIfNotValid()  &&  dst.linkIfNotValid() )
			if( !src.getTag().equalsValue( dst.getTag() ) ) {
				if( delay_ms > 0) {
					if( timeDelay == 0)
						timeDelay = System.currentTimeMillis();
					else {
						if( (System.currentTimeMillis() - timeDelay) > delay_ms ) {
							src.getTag().copyValueTo( dst.getTag() );
							timeDelay = 0;
						}
					}
				} else
					src.getTag().copyValueTo( dst.getTag() );
			} 
	}

	@Override
	public boolean isValid() {
		return src.isValid()  &&  dst.isValid();
	}

	@Override
	public String getError() {
		return "copy " + getId() + ": "+ 
			(src.isValid()? "": "src = " + src.getName() + " ") + 
			(dst.isValid()? "": "dst = " + dst.getName()); 
	}

}
