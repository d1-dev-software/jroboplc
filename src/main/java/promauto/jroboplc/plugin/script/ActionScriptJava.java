package promauto.jroboplc.plugin.script;

import static java.nio.charset.Charset.defaultCharset;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.tools.ToolProvider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import promauto.jroboplc.core.api.Configuration;
import promauto.jroboplc.core.api.Environment;
import promauto.jroboplc.core.api.EnvironmentInst;
import promauto.jroboplc.core.api.Tag;
import promauto.jroboplc.core.tags.Ref;
import promauto.utils.CRC;
import promauto.utils.Numbers;

public class ActionScriptJava extends Action {
	private final Logger logger = LoggerFactory.getLogger(ActionScriptJava.class);

	public static final String IMPORTS =
			"import promauto.jroboplc.plugin.script.ScriptJava;\r\n" +
			"import promauto.jroboplc.core.tags.*;\r\n" +
			"import promauto.jroboplc.core.api.*;\r\n";

	private final Environment env;
	private final ScriptModule module;
	protected ScriptJava instance;
	protected List<Ref> refs = null;
	protected List<Tag> vars = null; 

	private boolean valid;

	
	public ActionScriptJava(ScriptModule module) {
		this.module = module;
		env = EnvironmentInst.get();
	}

	@Override
	public boolean load(Object conf) {
		super.load(conf);

		Configuration cm = env.getConfiguration();
		Class<?> cls = getJavaClass( cm.get(conf, "class", "") );
		if( cls == null ) 
			return false;
		
		boolean res = false;
		try {
			instance = (ScriptJava)cls.newInstance();
			res = instance.load(module, this, conf);
		} catch (Exception e) {
			env.printError(logger, e, module.getName(), "Id:", getId());
		}
		
		return res;
	}

	@Override
	public boolean prepare() {
		try {
			if( instance.prepare() ) {

				if( refs != null)
					for(Ref ref: refs)
						if( ref.prepare() )
							ref.link();
						else
							break;
				
				return true;
			}
		} catch (Exception e) {
			env.printError(logger, e, module.getName(), "Id:", getId());
		}
		return false;
	}

	@Override
	public void execute() {
		boolean flag = true;
		if( refs != null )
			for(Ref ref: refs)
				if( !ref.linkIfNotValid() )
					flag = false;
		valid = flag;
		try {
			instance.execute();
		} catch (Exception e) {
			env.printError(logger, e, module.getName(), "Id:", getId());
		}
	}



	@Override
	public boolean isValid() {
		return valid;
	}

	@Override
	public String getError() {
		StringBuilder res = new StringBuilder("java " + getId() + ": ");
		if( refs != null )
			for(Ref ref: refs)
				if( !ref.isValid() )
					res.append(ref.getName()).append(" ");
		
		return res.toString();
	}
	
	
	protected void addRef(Ref ref) {
		if( refs == null )
			refs = new LinkedList<>();
		refs.add(ref);
	}
	
	protected Tag addVar(Tag tag) {
		if( vars == null )
			vars = new LinkedList<>();
		vars.add(tag);
		return tag;
	}
	

	@Override
	public void saveState(Map<String, String> state) {
		if( vars != null )
			vars.forEach(tag -> state.put(tag.getName(), tag.getString()));
	}

	@Override
	public void loadState(Map<String, String> state) {
		if( vars != null )
			for(Tag tag: vars) {
				String value = state.get(tag.getName());
				if( value != null)
					tag.setString(value);
			}
	}


	protected Class<?> getJavaClass(String javaClassName) {
		Class<?> cls = null;

		try {
			cls = Class.forName("promauto.jroboplc.plugin.script." + javaClassName);
			module.javaClasses.put(javaClassName, cls);
			return cls;
		} catch (ClassNotFoundException e) {
		}

		try {
			cls = Class.forName("promauto.jroboplc.plugin.script.samples." + javaClassName);
			module.javaClasses.put(javaClassName, cls);
			return cls;
		} catch (ClassNotFoundException e) {
		}

		try {
			cls = Class.forName("promauto.jroboplc.plugin.script.utils." + javaClassName);
			module.javaClasses.put(javaClassName, cls);
			return cls;
		} catch (ClassNotFoundException e) {
		}


		try {
			if( javaClassName.isEmpty() ) {
				env.printError(logger, module.getName(), "Class name is empty");
				return null;
			}
			
			// already loaded?
			cls = module.javaClasses.get(javaClassName);
			if (cls != null)
				return cls;

			// file
			String fname = javaClassName.replaceAll("\\.", "/");
			Path pathBinClass = module.javaDirBin.resolve( fname + ".class");
			Path pathBinHash = module.javaDirBin.resolve( fname + ".hash");
			Path pathBinSrc = module.javaDirBin.resolve( fname + ".java");
			Path pathSrc = module.javaDirSrc.resolve( fname + ".java");
			if (!Files.exists(pathSrc)) {
				env.printError(logger, module.getName(), "Source not found:", javaClassName);
				return null;
			}
			
			String header = constructHeader(javaClassName);
			
			// compile
			if( !checkJavaHash(header, pathSrc, pathBinClass, pathBinHash)) {
				if (module.javaCompiler == null) 
					if( (module.javaCompiler = ToolProvider.getSystemJavaCompiler()) == null ) {
						env.printError(logger, module.getName(), "Java compiler not found");
						return null;
					}
				
				
				// construct bin source
				if( !constructBinSource(header, pathSrc, pathBinSrc) )
					return null;

				
				List<String> options = new ArrayList<String>();
				options.add("-Xlint:all");
				options.add("-encoding");
				options.add("utf8");

				/* todo !!!
				String plugdir = env.getModuleManager().getPluginDir();
				if( !plugdir.isEmpty() ) {
					options.add("-classpath");
					options.add( 
							System.getProperty("java.class.path") + 
							//":" +
							File.pathSeparatorChar +
							plugdir + "/" + module.getPlugin().getPluginName() + ".jar");
				}
				 */
				
				options.add( pathBinSrc.toString() );
				
				OutputStream sout = new ByteArrayOutputStream();
				OutputStream serr = new ByteArrayOutputStream();
				if( module.javaCompiler.run(null, sout, serr, options.toArray(new String[options.size()])) != 0 ) {
					env.printError(logger, module.getName(), javaClassName, "Compiler error:\r\n", serr.toString());
					return null;
				} else
					env.printInfo(logger, module.getName(), "Compiled:", javaClassName, sout.toString(), serr.toString() );
					
				
				deleteBinSource(pathBinSrc);
				saveJavaHash(header, pathSrc, pathBinClass, pathBinHash);
			}
			
			// load
			if( module.javaURLClassLoader == null ) {
				module.javaURLClassLoader = URLClassLoader.newInstance(new URL[] { 
						module.javaDirBin.toUri().toURL()}, this.getClass().getClassLoader());
			}
			
			cls = Class.forName(javaClassName, true, module.javaURLClassLoader);
			module.javaClasses.put(javaClassName, cls);
			
		} catch (Exception e) {
			env.printError(logger, e, module.getName(), "Java class name: ", javaClassName);
			return null;
		}

		return cls;
	}


	private String constructHeader(String javaClassName) {
		Matcher m = Pattern.compile("(.*)\\.(\\w*$)").matcher(javaClassName);
		boolean hasPackage = m.find();
		String namePackage = hasPackage? m.group(1): ""; 
		String nameClass = hasPackage? m.group(2): javaClassName;
		
		StringBuilder header = new StringBuilder();
		
		if( !namePackage.isEmpty() )
			header.append("package ")
					.append(namePackage)
					.append(";\r\n\r\n");
		
		header.append(IMPORTS)
				.append("\r\npublic class ")
				.append(nameClass)
				.append(" extends ScriptJava {");
		
		return header.toString();
	}

	
	private void deleteBinSource(Path pathBinSrc) {
		try {
			Files.delete(pathBinSrc);
		} catch (IOException e) {
			env.printError(logger, e, module.getName(), pathBinSrc.toString() );
		}
		
	}

	
	private boolean constructBinSource(String header, Path pathSrc, Path pathBinSrc) {
		String footer = "\r\n}\r\n";
		
		try {
			Files.createDirectories(pathBinSrc.getParent());
		} catch (IOException e) {
			env.printError(logger, e, module.getName(),
					"Create directories error:", pathBinSrc.getParent().toString() );
			return false;
		}
		
		boolean res = true;
		int linecnt = 1;

		StringBuilder sb = new StringBuilder();
		Pattern p = Pattern.compile("^\\s*public\\s+class.+extends\\s+ScriptJava.*");
		try (
				BufferedReader reader = Files.newBufferedReader(pathSrc, StandardCharsets.UTF_8);
				BufferedWriter writer = Files.newBufferedWriter(pathBinSrc, StandardCharsets.UTF_8))
		{
			String line = null;
			while( (line = reader.readLine()) != null) {
				sb.append(line);
				sb.append("\r\n");

				if( p.matcher(line).matches() ) {
					header = IMPORTS + sb.toString();
					footer = "";
					sb.setLength(0);
				}
				linecnt++;
			}

			writer.write(header);
			writer.write(sb.toString());
			writer.write(footer);
		} catch (IOException e) {
			env.printError(logger, e, module.getName(), pathSrc.toString(), pathBinSrc.toString() );
			res = false;
		}
		
		return res;
	}


	private boolean checkJavaHash(String header, Path pathSrc, Path pathBinClass, Path pathBinHash) {
		if( !Files.exists(pathBinClass)  ||  !Files.exists(pathBinHash) ) 
			return false;
		
		String hashCalculated = calcHash(header, pathSrc, pathBinClass);
		if( hashCalculated.isEmpty() )
			return false;
		
		String hashSaved;
		try( BufferedReader reader = Files.newBufferedReader(pathBinHash, StandardCharsets.UTF_8) ) {
			hashSaved = reader.readLine();
		} catch (IOException e) {
			env.printError(logger, e, module.getName(), "Hash:", pathBinHash.toString() );
			return false;
		}

		return hashCalculated.equals(hashSaved);
	}

	
		
	private void saveJavaHash(String header, Path pathSrc, Path pathBinClass, Path pathBinHash) {
		String hash = calcHash(header, pathSrc, pathBinClass);
		if( hash.isEmpty() )
			return;
			
		try( BufferedWriter writer = Files.newBufferedWriter(pathBinHash, StandardCharsets.UTF_8) ) {
			writer.write(hash);
		} catch (IOException e) {
			env.printError(logger, e, module.getName(), "Hash:", pathBinHash.toString() );
		}
	}
	

	private String calcHash(String header, Path pathSrc, Path pathBinClass) {
		StringBuilder sb = new StringBuilder();
		sb.append(  ScriptJava.getVersion() );
		sb.append('.');
		sb.append(  Numbers.toHexString( header.hashCode() ) );
		sb.append('.');
		if( !CRC.getMD5(pathSrc, sb) )
			return "";
		sb.append('.');
		if( !CRC.getMD5(pathBinClass, sb) )
			return "";
		return sb.toString();
	}

	public List<Ref> getRefs() {
		return refs;
	}
}











