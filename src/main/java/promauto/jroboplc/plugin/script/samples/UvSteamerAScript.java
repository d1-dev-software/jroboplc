package promauto.jroboplc.plugin.script.samples;

import promauto.jroboplc.core.api.Flags;
import promauto.jroboplc.core.api.Tag;
import promauto.jroboplc.core.tags.Ref;
import promauto.jroboplc.core.tags.TagRW;
import promauto.jroboplc.plugin.script.ScriptJava;

import java.util.function.Consumer;


/*
yml example:

plugin.script:
  module.steamer:
    actions:
      - java:
          class:         UvSteamerAScript

 */

public class UvSteamerAScript extends ScriptJava {
//public class UvSteamerA extends ScriptJava {


    protected static final int STATE_IDLE            = 0;
    protected static final int STATE_WARM_DRAIN_OPEN = 1;
    protected static final int STATE_WARM_PREPARE    = 2;
    protected static final int STATE_WARM_INFL       = 3;
    protected static final int STATE_WARM_EXP        = 4;
    protected static final int STATE_WARM_DEFL       = 5;
    protected static final int STATE_WARM_DRAIN_CLOSE= 6;
    protected static final int STATE_LOAD_BUNK_FULL  = 7;
    protected static final int STATE_BUNK_FLOW_STOP  = 8;
    protected static final int STATE_LOAD_ENABLE     = 9;
    protected static final int STATE_LOAD_PREPARE    = 10;
    protected static final int STATE_LOAD            = 11;
    protected static final int STATE_LOAD_AFTER_DLY  = 12;
    protected static final int STATE_INFL_PREPARE = 13;
    protected static final int STATE_INFL            = 14;
    protected static final int STATE_EXPOSE          = 15;
    protected static final int STATE_DEFL            = 16;
    protected static final int STATE_CAN_UNLOAD      = 17;
    protected static final int STATE_UNLOAD          = 18;
    protected static final int STATE_FINISH          = 19;


    protected static final String[] STATES = new String[20];
    static {
        STATES[STATE_IDLE             ] = "Выключен";
        STATES[STATE_WARM_DRAIN_OPEN  ] = "Прогрев: открытие дренажного клапана";
        STATES[STATE_WARM_PREPARE     ] = "Прогрев: закрытие задвижек над/под пропаривателем и выпускных клапанов";
        STATES[STATE_WARM_INFL        ] = "Прогрев: набор пара";
        STATES[STATE_WARM_EXP         ] = "Прогрев: экспозиция";
        STATES[STATE_WARM_DEFL        ] = "Прогрев: сброс пара";
        STATES[STATE_WARM_DRAIN_CLOSE ] = "Прогрев: закрытие дренажного клапана";
        STATES[STATE_LOAD_BUNK_FULL   ] = "Подготовка к загрузке: ожидание заполнения бункера над пропаривателем";
        STATES[STATE_BUNK_FLOW_STOP   ] = "Подготовка к загрузке: останов подачи продукта в бункер над пропаривателем";
        STATES[STATE_LOAD_ENABLE      ] = "Подготовка к загрузке: ожидание разрешения от оператора";
        STATES[STATE_LOAD_PREPARE     ] = "Подготовка к загрузке: закрытие задвижек над верхним бункером, под пропаривателем, клапана набора пара";
        STATES[STATE_LOAD             ] = "Загрузка: ожидание опустошения бункера над пропаривателем";
        STATES[STATE_LOAD_AFTER_DLY   ] = "Загрузка: задержка после опустошения бункера над пропаривателем";
        STATES[STATE_INFL_PREPARE     ] = "Подготовка к набору пара: закрытие задвижек над/под пропаривателем, клапана сброса пара большого";
        STATES[STATE_INFL             ] = "Набор пара";
        STATES[STATE_EXPOSE           ] = "Экспозиция";
        STATES[STATE_DEFL             ] = "Сброс пара";
        STATES[STATE_CAN_UNLOAD       ] = "Подготовка к разгрузке: ожидание разрешения от оборудования";
        STATES[STATE_UNLOAD           ] = "Разгрузка";
        STATES[STATE_FINISH           ] = "Цикл завершен";
    }




    // inputs
    Ref inpStart;
    Ref inpDvu;
    Ref inpDnu;
    Ref inpVlvInflClosed;
    Ref inpVlvInflOpened;
    Ref inpVlvDefl1Closed;
    Ref inpVlvDefl1Opened;
    Ref inpVlvDefl2Closed;
    Ref inpVlvDefl2Opened;
    Ref inpVlvDrnClosed;
    Ref inpVlvDrnOpened;
    Ref inpZdvBunkClosed;
    Ref inpZdvBunkOpened;
    Ref inpZdvLoadClosed;
    Ref inpZdvLoadOpened;
    Ref inpZdvUnloadClosed;
    Ref inpZdvUnloadOpened;
    Ref inpPLine;
    Ref inpPIn;
    Ref inpPOut;
    Ref inpBunkFlowStopped;
    Ref inpCanUnload;

    // outputs ro
    Ref tagState;
    Ref tagStateMsg;
    Ref tagTimer;
    Ref tagCycleCnt;
    Ref tagWarmPassCnt;

    Ref tagVlvInflOpen;
    Ref tagVlvDefl1Open;
    Ref tagVlvDefl2Open;
    Ref tagVlvDrnOpen;
    Ref tagZdvBunkOpen;
    Ref tagZdvLoadOpen;
    Ref tagZdvUnloadOpen;

    Ref tagAlarmWarmTimeInfl;
    Ref tagAlarmWarmTimeDefl;

    Ref tagAlarmTimeLoad;
    Ref tagAlarmTimeInfl;
    Ref tagAlarmTimeDefl;
    Ref tagAlarmPLine;

    Ref tagCmdBunkFlowStop;
    Ref tagCmdUnloadSeq;

    Ref tagWarming;


    // outputs rw
    Ref tagEnable;
    Ref tagReset;

    Ref tagWarmSkip;
    Ref tagWarmRepeat;
    Ref tagWarmPassQnt;
    Ref tagWarmPInfl;
    Ref tagWarmTimeInfl;
    Ref tagWarmTimeExp;
    Ref tagWarmPDefl;
    Ref tagWarmPDefl2;
    Ref tagWarmTimeDefl;

    Ref tagTimeLoad;
    Ref tagTimeAfterLoad;
    Ref tagPInfl;
    Ref tagPInfl1;
    Ref tagTimeInfl;
    Ref tagTimeExp;
    Ref tagPDefl;
    Ref tagPDefl2;
    Ref tagTimeDefl;
    Ref tagTimeCanReinfl;
    Ref tagTimeUnload;

    Ref tagPLineMin;

    Tag varTimerTime;
    private Runnable updateTimerFunc;
    private Consumer<Ref> initTimerFunc;


    @Override
    public boolean load() {

        inpStart             = createTag("inpStart",           false); // сигнал от логики маршрута - работать
        inpDvu               = createTag("inpDvu",             false); // сигнал от ДВУ бункера над пропаривателем
        inpDnu               = createTag("inpDnu",             false); // сигнал от ДНУ бункера над пропаривателем
        inpVlvInflClosed     = createTag("inpVlvInflClosed",   false); // сигнал датчика положения ЗАКРЫТО клапана набора пара
        inpVlvInflOpened     = createTag("inpVlvInflOpened",   false); // сигнал датчика положения ОТКРЫТО клапана набора пара
        inpVlvDefl1Closed    = createTag("inpVlvDefl1Closed",  false); // сигнал датчика положения ЗАКРЫТО клапана сброса пара малого
        inpVlvDefl1Opened    = createTag("inpVlvDefl1Opened",  false); // сигнал датчика положения ОТКРЫТО клапана сброса пара малого
        inpVlvDefl2Closed    = createTag("inpVlvDefl2Closed",  false); // сигнал датчика положения ЗАКРЫТО клапана сброса пара большого
        inpVlvDefl2Opened    = createTag("inpVlvDefl2Opened",  false); // сигнал датчика положения ОТКРЫТО клапана сброса пара большого
        inpVlvDrnClosed      = createTag("inpVlvDrnClosed",    false); // сигнал датчика положения ЗАКРЫТО дренажного клапана
        inpVlvDrnOpened      = createTag("inpVlvDrnOpened",    false); // сигнал датчика положения ОТКРЫТО дренажного клапана
        inpZdvBunkClosed     = createTag("inpZdvBunkClosed",   false); // сигнал датчика положения ЗАКРЫТО задвижки над бункером над пропаривателем
        inpZdvBunkOpened     = createTag("inpZdvBunkOpened",   false); // сигнал датчика положения ОТКРЫТО задвижки над бункером над пропаривателем
        inpZdvLoadClosed     = createTag("inpZdvLoadClosed",   false); // сигнал датчика положения ЗАКРЫТО задвижки над пропаривателем
        inpZdvLoadOpened     = createTag("inpZdvLoadOpened",   false); // сигнал датчика положения ОТКРЫТО задвижки над пропаривателем
        inpZdvUnloadClosed   = createTag("inpZdvUnloadClosed", false); // сигнал датчика положения ЗАКРЫТО задвижки под пропаривателем
        inpZdvUnloadOpened   = createTag("inpZdvUnloadOpened", false); // сигнал датчика положения ОТКРЫТО задвижки под пропаривателем
        inpPLine             = createTag("inpPLine",           0);     // значение датчика давления линии
        inpPIn               = createTag("inpPIn",             0);     // значение датчика давления на входе в пропариватель
        inpPOut              = createTag("inpPOut",            0);     // значение датчика давления на выходе из пропаривателя
        inpBunkFlowStopped   = createTag("inpBunkFlowStopped", false); // сигнал от оборудования - подача в бункер над пропаривателем остановлена
        inpCanUnload         = createTag("inpCanUnload",       false); // сигнал от оборудования - можно разгружать пропариватель

        // outputs ro
        tagState             = createRWTag("State"             , 0);     // код состояния
        tagStateMsg          = createRWTag("StateMsg"          , "");    // текст состояния
        tagTimer             = createRWTag("Timer"             , 0);     // обратный таймер в секундах
        tagWarmPassCnt       = createRWTag("WarmPassCnt"       , 0);     // кол-во выполненных проходов прогрева
        tagVlvInflOpen       = createRWTag("VlvInflOpen"       , false); // управление клапана набора пара
        tagVlvDefl1Open      = createRWTag("VlvDefl1Open"      , false); // управление клапана сброса пара малого
        tagVlvDefl2Open      = createRWTag("VlvDefl2Open"      , false); // управление клапана сброса пара большого
        tagVlvDrnOpen        = createRWTag("VlvDrnOpen"        , false); // управление дренажного клапана
        tagZdvBunkOpen       = createRWTag("ZdvBunkOpen"       , false); // управление задвижки над бункером над пропаривателем
        tagZdvLoadOpen       = createRWTag("ZdvLoadOpen"       , false); // управление задвижки над пропаривателем
        tagZdvUnloadOpen     = createRWTag("ZdvUnloadOpen"     , false); // управление задвижки под пропаривателем
        tagAlarmWarmTimeInfl = createRWTag("AlarmWarmTimeInfl" , false); // авария превышено время набора пара при прогреве
        tagAlarmWarmTimeDefl = createRWTag("AlarmWarmTimeDefl" , false); // авария превышено время сброса пара при прогреве
        tagAlarmTimeLoad     = createRWTag("AlarmTimeLoad"     , false); // авария превышено время загрузки в пропариватель
        tagAlarmTimeInfl     = createRWTag("AlarmTimeInfl"     , false); // авария превышено время набора пара
        tagAlarmTimeDefl     = createRWTag("AlarmTimeDefl"     , false); // авария превышено время сброса пара
        tagAlarmPLine        = createRWTag("AlarmPLine"        , false); // авария недостаточное давления трассы
        tagCmdBunkFlowStop   = createRWTag("CmdBunkFlowStop"   , false); // управление приостановкой подачи продукта в бункер над пропаривателем
        tagCmdUnloadSeq      = createRWTag("CmdUnloadSeq"      , false); // управление оборудование выгрузки из пропаривателя
        tagWarming           = createRWTag("Warming"           , false); // индикатор - режим прогрева

        // outputs rw
        tagEnable            = createTag("Enable"         , false, Flags.AUTOSAVE); // чекбокс - разрешение загрузки продукта в пропариватель
        tagReset             = createTag("Reset"          , false);                 // кнопка - сброс цикла
        tagWarmSkip          = createTag("WarmSkip"       , false);                 // кнопка - пропустить прогрев
        tagWarmRepeat        = createTag("WarmRepeat"     , false);                 // кнопка - повторить прогрев
        tagWarmPassQnt       = createTag("WarmPassQnt"    , 0, Flags.AUTOSAVE);     // заданное кол-во циклов прогрева
        tagWarmPInfl         = createTag("WarmPInfl"      , 0, Flags.AUTOSAVE);     // заданное давление набора пара при прогреве
        tagWarmTimeInfl      = createTag("WarmTimeInfl"   , 0, Flags.AUTOSAVE);     // заданное максимальное время набора пара при прогреве
        tagWarmTimeExp       = createTag("WarmTimeExp"    , 0, Flags.AUTOSAVE);     // заданное время экспозиции при прогреве
        tagWarmPDefl         = createTag("WarmPDefl"      , 0, Flags.AUTOSAVE);     // заданное давление полного сброса пара при прогреве
        tagWarmPDefl2        = createTag("WarmPDefl2"     , 0, Flags.AUTOSAVE);     // заданное давление частичного сброса пара при прогреве для открытия большого клапана сброса пара
        tagWarmTimeDefl      = createTag("WarmTimeDefl"   , 0, Flags.AUTOSAVE);     // заданное максимальное время сброса пара при прогреве
        tagTimeLoad          = createTag("TimeLoad"       , 0, Flags.AUTOSAVE);     // заданное максимальное время загрузки продукта в пропариватель
        tagTimeAfterLoad     = createTag("TimeAfterLoad"  , 0, Flags.AUTOSAVE);     // заданное время задержки после завершения загрузки в пропариватель
        tagPInfl             = createTag("PInfl"          , 0, Flags.AUTOSAVE);     // заданное давление набора пара для пропаривания
        tagPInfl1            = createTag("PInfl1"         , 0, Flags.AUTOSAVE);     // заданное давление закрытия малого клапана сброса пара при наборе пара
        tagTimeInfl          = createTag("TimeInfl"       , 0, Flags.AUTOSAVE);     // заданное максимальное время набора пара
        tagTimeExp           = createTag("TimeExp"        , 0, Flags.AUTOSAVE);     // заданное время экспозиции
        tagPDefl             = createTag("PDefl"          , 0, Flags.AUTOSAVE);     // заданное давление полного сброса пара
        tagPDefl2            = createTag("PDefl2"         , 0, Flags.AUTOSAVE);     // заданное давление частичного сброса пара для открытия большого клапана сброса пара
        tagTimeDefl          = createTag("TimeDefl"       , 0, Flags.AUTOSAVE);     // заданное максимальное время сброса пара
        tagTimeCanReinfl     = createTag("TimeCanReinfl"  , 0, Flags.AUTOSAVE);     // заданное время до окончания экспозиции, после которого не делать донабор пара
        tagTimeUnload        = createTag("TimeUnload"     , 0, Flags.AUTOSAVE);     // заданное время выгрузки
        tagPLineMin          = createTag("PLineMin"       , 0, Flags.AUTOSAVE);     // заданное минимально необходимое давление трассы
        tagCycleCnt          = createTag("CycleCnt"       , 0);                     // счетчик циклов

        varTimerTime = createVar("TimerTime", 0L);

        useTimerSimple();
//        useTimerRealTime();

        return true;
    }


    @Override
    public void execute() {
        updateTimer();

        int state = tagState.getInt();
        boolean vlvInflCl = inpVlvInflClosed.getBool();
//        boolean vlvInflOp = inpVlvInflOpened.getBool();
        boolean vlvDefl1Cl = inpVlvDefl1Closed.getBool();
        boolean vlvDefl1Op = inpVlvDefl1Opened.getBool();
        boolean vlvDefl2Cl = inpVlvDefl2Closed.getBool();
        boolean vlvDefl2Op = inpVlvDefl2Opened.getBool();
        boolean vlvDrnCl = inpVlvDrnClosed.getBool();
        boolean vlvDrnOp = inpVlvDrnOpened.getBool();
        boolean zdvBunkCl = inpZdvBunkClosed.getBool();
//        boolean zdvBunkOp = inpZdvBunkOpened.getBool();
        boolean zdvLoadCl = inpZdvLoadClosed.getBool();
//        boolean zdvLoadOp = inpZdvLoadOpened.getBool();
        boolean zdvUnloadCl = inpZdvUnloadClosed.getBool();
//        boolean zdvUnloadOp = inpZdvUnloadOpened.getBool();

        boolean vlvInfl = tagVlvInflOpen.getBool();
        boolean vlvDefl1 = tagVlvDefl1Open.getBool();
        boolean vlvDefl2 = tagVlvDefl2Open.getBool();
        boolean vlvDrn = tagVlvDrnOpen.getBool();
        boolean zdvBunk = tagZdvBunkOpen.getBool();
        boolean zdvLoad = tagZdvLoadOpen.getBool();
        boolean zdvUnload = tagZdvUnloadOpen.getBool();


        // reset
        if( tagReset.getBool() ) {
            state = STATE_IDLE;
            tagReset.setInt(0);
            tagEnable.setInt(0);
            varTimerTime.setLong(0L);
            setrw(tagTimer, 0);
            setrw(tagWarmPassCnt, 0);
//            setrw(tagCycleCnt, 0);

            setrw(tagAlarmWarmTimeInfl, false);
            setrw(tagAlarmWarmTimeDefl, false);
            setrw(tagAlarmTimeLoad, false);
            setrw(tagAlarmTimeInfl, false);
            setrw(tagAlarmTimeDefl, false);

            setrw(tagCmdBunkFlowStop, true);
            setrw(tagCmdUnloadSeq, true);


        }

        // 0
        if( state == STATE_IDLE ) {
            vlvDrn    = false;
            vlvInfl   = false;
            vlvDefl1  = false;
            vlvDefl2  = false;
            zdvBunk   = true;
            zdvLoad   = false;
            zdvUnload = false;

            if( inpStart.getBool() ) {
                state = STATE_WARM_DRAIN_OPEN;
            }
        }


        // 1
        if( state == STATE_WARM_DRAIN_OPEN ) {
            vlvDrn    = true;
            vlvInfl   = false;
            vlvDefl1  = false;
            vlvDefl2  = false;
            zdvBunk   = true;
            zdvLoad   = false;
            zdvUnload = false;

            setrw(tagWarmPassCnt, 0);

            if( vlvDrnOp ) {
                state = STATE_WARM_PREPARE;
            }

            if( tagWarmSkip.getBool() )
                state = STATE_WARM_DRAIN_CLOSE;
        }


        // 2
        if( state == STATE_WARM_PREPARE ) {
            vlvDrn    = true;
            vlvInfl   = false;
            vlvDefl1  = false;
            vlvDefl2  = false;
            zdvBunk   = true;
            zdvLoad   = false;
            zdvUnload = false;

            if( vlvDefl1Cl  &&  vlvDefl2Cl  &&  zdvLoadCl  &&  zdvUnloadCl ) {
                state = STATE_WARM_INFL;
                initTimer(tagWarmTimeInfl);
            }

            if( tagWarmSkip.getBool() )
                state = STATE_WARM_DRAIN_CLOSE;
        }


        // 3
        if( state == STATE_WARM_INFL ) {
            vlvDrn    = true;
            vlvInfl   = true;
            vlvDefl1  = false;
            vlvDefl2  = false;
            zdvBunk   = true;
            zdvLoad   = false;
            zdvUnload = false;


            if( inpPOut.getInt() >= tagWarmPInfl.getInt() ) {
                state = STATE_WARM_EXP;
                initTimer(tagWarmTimeExp);
            } else

            if (isTimerOver()) {
                setrw(tagAlarmWarmTimeInfl, true);
            }

            if( tagWarmSkip.getBool() )
                state = STATE_WARM_DEFL;
        } else {
            setrw(tagAlarmWarmTimeInfl, false);
        }


        // 4
        if( state == STATE_WARM_EXP ) {
            vlvDrn    = true;
            vlvInfl   = false;
            vlvDefl1  = false;
            vlvDefl2  = false;
            zdvBunk   = true;
            zdvLoad   = false;
            zdvUnload = false;

            if( isTimerOver() ) {
                state = STATE_WARM_DEFL;
                initTimer(tagWarmTimeDefl);
            }

            if( tagWarmSkip.getBool() )
                state = STATE_WARM_DEFL;
        }


        // 5
        if( state == STATE_WARM_DEFL ) {
            vlvDrn    = true;
            vlvInfl   = false;
            vlvDefl1  = true;
            vlvDefl2  = inpPOut.getInt() <= tagWarmPDefl2.getInt();
            zdvBunk   = true;
            zdvLoad   = false;
            zdvUnload = false;

            if( inpPOut.getInt() <= tagWarmPDefl.getInt() ) {
                incrw(tagWarmPassCnt);
                if( tagWarmPassCnt.getInt() >= tagWarmPassQnt.getInt()  ||  tagWarmSkip.getBool()) {
                    state = STATE_WARM_DRAIN_CLOSE; // finish warming
                } else {
                    state = STATE_WARM_PREPARE; // continue warming
                    vlvDefl1  = false;
                    vlvDefl2  = false;
                }
                resetTimer();
            } else

            if (isTimerOver()) {
                setrw(tagAlarmWarmTimeDefl, true);
            }

        } else {
            setrw(tagAlarmWarmTimeDefl, false);
        }


        // 6
        if( state == STATE_WARM_DRAIN_CLOSE ) {
            vlvDrn    = false;
            vlvInfl   = false;
            vlvDefl1  = true;
            vlvDefl2  = true;
            zdvBunk   = true;
            zdvLoad   = false;
            zdvUnload = false;

            if( vlvDrnCl ) {
                if(inpStart.getBool()) {
                    state = STATE_LOAD_BUNK_FULL;
                } else {
                    state = STATE_IDLE;
                }
            }
        }


        // warm repeat
        if( tagWarmRepeat.getBool()  &&  (state >= STATE_LOAD_BUNK_FULL  &&  state <= STATE_LOAD_PREPARE) ) {
            state = STATE_WARM_DRAIN_OPEN;
            tagWarmRepeat.setBool(false);
        }


        // 7
        if( state == STATE_LOAD_BUNK_FULL ) {
            vlvDrn    = false;
            vlvInfl   = false;
            vlvDefl1  = true;
            vlvDefl2  = true;
            zdvBunk   = true;
            zdvLoad   = false;
            zdvUnload = false;

            if( inpDvu.getBool()  &&  inpDnu.getBool() ) {
                state = STATE_BUNK_FLOW_STOP;
            }
        }


        // 8
        if(  state == STATE_BUNK_FLOW_STOP) {
            vlvDrn    = false;
            vlvInfl   = false;
            vlvDefl1  = true;
            vlvDefl2  = true;
            zdvBunk   = true;
            zdvLoad   = false;
            zdvUnload = false;

            if( inpBunkFlowStopped.getBool() ) {
                state = STATE_LOAD_ENABLE;
            }
        }


        // 9
        if( state == STATE_LOAD_ENABLE ) {
            vlvDrn    = false;
            vlvInfl   = false;
            vlvDefl1  = true;
            vlvDefl2  = true;
            zdvBunk   = false;
            zdvLoad   = false;
            zdvUnload = false;

            if( tagEnable.getBool() ) {
                state = STATE_LOAD_PREPARE;
            }
        }


        // 10
        if( state == STATE_LOAD_PREPARE ) {
            vlvDrn    = false;
            vlvInfl   = false;
            vlvDefl1  = true;
            vlvDefl2  = true;
            zdvBunk   = false;
            zdvLoad   = false;
            zdvUnload = false;

            if( vlvInflCl  &&  vlvDefl1Op  &&  vlvDefl2Op  &&  zdvBunkCl  &&  zdvUnloadCl ) {
                state = STATE_LOAD;
                initTimer(tagTimeLoad);
            }
        }


        // 11
        if( state == STATE_LOAD ) {
            vlvDrn    = false;
            vlvInfl   = false;
            vlvDefl1  = true;
            vlvDefl2  = true;
            zdvBunk   = false;
            zdvLoad   = true;
            zdvUnload = false;

            if( !inpDvu.getBool()  &&  !inpDnu.getBool() ) {
                state = STATE_LOAD_AFTER_DLY;
                initTimer(tagTimeAfterLoad);
            } else

            if (isTimerOver()) {
                setrw(tagAlarmTimeLoad, true);
            }

        } else {
            setrw(tagAlarmTimeLoad, false);
        }


        // 12
        if( state == STATE_LOAD_AFTER_DLY ) {
            vlvDrn    = false;
            vlvInfl   = false;
            vlvDefl1  = true;
            vlvDefl2  = true;
            zdvBunk   = false;
            zdvLoad   = true;
            zdvUnload = false;

            if (isTimerOver()) {
                state = STATE_INFL_PREPARE;
            }
        }


        // 13
        if( state == STATE_INFL_PREPARE) {
            vlvDrn    = false;
            vlvInfl   = false;
            vlvDefl1  = true;
            vlvDefl2  = false;
            zdvBunk   = false;
            zdvLoad   = false;
            zdvUnload = false;

            if( vlvDefl2Cl  &&  zdvLoadCl  &&  zdvUnloadCl ) {
                state = STATE_INFL;
                initTimer(tagTimeInfl);
            }
        }


        // 14
        if( state == STATE_INFL ) {
            vlvDrn    = false;
            vlvInfl   = true;
            vlvDefl1  = vlvDefl1  &&  (inpPOut.getInt() < tagPInfl1.getInt());
            vlvDefl2  = false;
            zdvBunk   = true;
            zdvLoad   = false;
            zdvUnload = false;

            if( inpPOut.getInt() >= tagPInfl.getInt() ) {
                state = STATE_EXPOSE;
                initTimer(tagTimeExp);
            } else

            if (isTimerOver()) {
                setrw(tagAlarmTimeInfl, true);
                tagEnable.setBool(false);
            }

        } else {
            setrw(tagAlarmTimeInfl, false);
        }


        // 15
        if( state == STATE_EXPOSE ) {
            vlvDrn    = false;
            if( inpPOut.getInt() < tagPInfl.getInt() ) {
                if( tagTimer.getInt() > tagTimeCanReinfl.getInt() )
                    vlvInfl = true;
            } else {
                vlvInfl = false;
            }
            vlvDefl1  = false;
            vlvDefl2  = false;
            zdvBunk   = true;
            zdvLoad   = false;
            zdvUnload = false;

            if (isTimerOver()) {
                state = STATE_DEFL;
                initTimer(tagTimeDefl);
            }
        }


        // 16
        if( state == STATE_DEFL ) {
            vlvDrn    = false;
            vlvInfl   = false;
            vlvDefl1  = true;
            vlvDefl2  = inpPOut.getInt() <= tagPDefl2.getInt();
            zdvBunk   = true;
            zdvLoad   = false;
            zdvUnload = false;

            if( inpPOut.getInt() <= tagPDefl.getInt() ) {
                state = STATE_CAN_UNLOAD;
                resetTimer();
            } else

            if (isTimerOver()) {
                setrw(tagAlarmTimeDefl, true);
            }

        } else {
            setrw(tagAlarmTimeDefl, false);
        }


        // 17
        if( state == STATE_CAN_UNLOAD ) {
            vlvDrn    = false;
            vlvInfl   = false;
            vlvDefl1  = true;
            vlvDefl2  = true;
            zdvBunk   = true;
            zdvLoad   = false;
            zdvUnload = false;

            if( inpCanUnload.getBool() ) {
                state = STATE_UNLOAD;
                setrw(tagAlarmTimeDefl, false);
                initTimer(tagTimeUnload);
            }
        }


        // 18
        if( state == STATE_UNLOAD ) {
            vlvDrn    = false;
            vlvInfl   = false;
            vlvDefl1  = true;
            vlvDefl2  = true;
            zdvBunk   = true;
            zdvLoad   = false;
            zdvUnload = true;

            if (isTimerOver()) {
                state = STATE_FINISH;
                tagCycleCnt.setInt( tagCycleCnt.getInt() + 1);
            }
        } else
        // 19
        if( state == STATE_FINISH ) {
            vlvDrn    = false;
            vlvInfl   = false;
            vlvDefl1  = true;
            vlvDefl2  = true;
            zdvBunk   = true;
            zdvLoad   = false;
            zdvUnload = false;

            if(inpStart.getBool()) {
                state = STATE_LOAD_BUNK_FULL;
            } else {
                state = STATE_IDLE;
            }
        }

        boolean pLineOk = inpPLine.getInt() >= tagPLineMin.getInt();
        setrw(tagAlarmPLine, !pLineOk  &&  state != STATE_IDLE );


        if (!vlvDefl2Cl  ||  !zdvLoadCl  ||  !zdvUnloadCl  ||  !pLineOk) {
            vlvInfl = false;
        }

        if (!vlvInflCl) {
            zdvLoad = false;
            zdvUnload = false;
        }

        setrw(tagVlvDrnOpen, vlvDrn);
        setrw(tagVlvInflOpen, vlvInfl);
        setrw(tagVlvDefl1Open, vlvDefl1);
        setrw(tagVlvDefl2Open, vlvDefl2);
        setrw(tagZdvBunkOpen, zdvBunk);
        setrw(tagZdvLoadOpen, zdvLoad);
        setrw(tagZdvUnloadOpen, zdvUnload);

        setrw(tagWarming, state >= STATE_WARM_DRAIN_OPEN  &&  state <= STATE_WARM_DRAIN_CLOSE);

        if(!tagWarming.getBool())
            tagWarmSkip.setBool(false);

        setrw(tagCmdBunkFlowStop, state >= STATE_BUNK_FLOW_STOP  &&  state <= STATE_INFL_PREPARE);
        setrw(tagCmdUnloadSeq, state >= STATE_CAN_UNLOAD  &&  state <= STATE_UNLOAD);

        setrw(tagState, state);
        setrw(tagStateMsg, STATES[state]);
//        System.out.println(STATES[state]);
    }



    // helpers
    private void setrw(Ref ref, boolean value) {
        TagRW tag = (TagRW)ref.getTag();
        if( tag != null  &&  tag.getBool() != value)
            tag.setReadValBool(value);
    }

    private void setrw(Ref ref, int value) {
        TagRW tag = (TagRW)ref.getTag();
        if( tag != null  &&  tag.getInt() != value )
            tag.setReadValInt(value);
    }

    private void setrw(Ref ref, String value) {
        TagRW tag = (TagRW)ref.getTag();
        if( tag != null  &&  !tag.getString().equals(value) )
            tag.setReadValString(value);
    }

    private void incrw(Ref ref) {
        setrw(ref, ref.getInt() + 1);
    }

    private void decrw(Ref ref) {
        setrw(ref, ref.getInt() - 1);
    }


    // timer
    private boolean isTimerOver() {
        return tagTimer.getInt() == 0;
    }

    private void resetTimer() {
        setrw(tagTimer, 0);
        varTimerTime.setLong(0L);
    }



    private void updateTimerRealTime() {
        long curtime = System.currentTimeMillis();
        if( varTimerTime.getLong() > curtime ) {
            setrw(tagTimer, (int)Math.round((varTimerTime.getLong() - curtime) / 1000.0) );
        } else {
            setrw(tagTimer, 0);
        }
    }

    private void initTimerRealTime(Ref time) {
        varTimerTime.setLong( System.currentTimeMillis() + time.getLong() * 1000L );
        updateTimerRealTime();
    }

    private void updateTimerSimple() {
        if( tagTimer.getInt() > 0 )
            decrw(tagTimer);
    }

    private void initTimerSimple(Ref time) {
        setrw(tagTimer, time.getInt());
    }

    protected void useTimerRealTime() {
        updateTimerFunc = this::updateTimerRealTime;
        initTimerFunc = this::initTimerRealTime;
    }

    protected void useTimerSimple() {
        updateTimerFunc = this::updateTimerSimple;
        initTimerFunc = this::initTimerSimple;
    }

    private void updateTimer() {
        updateTimerFunc.run();
    }

    private void initTimer(Ref time) {
        initTimerFunc.accept(time);
    }

}
