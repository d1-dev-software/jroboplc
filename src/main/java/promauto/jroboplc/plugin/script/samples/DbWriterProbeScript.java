package promauto.jroboplc.plugin.script.samples;

import promauto.jroboplc.core.api.Database;
import promauto.jroboplc.core.api.EnvironmentInst;
import promauto.jroboplc.plugin.script.ScriptJava;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;


public class DbWriterProbeScript extends ScriptJava {

    private static final String TABLE = "MY_REPO";
    Database db1;
    Database db2;

    boolean connected;
    private boolean firstPass;
    private PreparedStatement pstInsert;
    private PreparedStatement pstDelete;

    @Override
    public boolean load() {
        firstPass = true;
        connected = false;
        return true;
    }


    @Override
    public void execute() {
        db1 = getDatabase("db1");
        db2 = getDatabase("db2");
        if (db1 == null  ||  db2 == null) {
            printError("Database module is not found");
            return;
        }

        if (db1 == null || db2 == null || !isValid())
            return;

        if (!connected  &&  db1.isConnected()  &&  db2.isConnected()) {
            connected = true;
        }

        if (connected  &&  (!db1.isConnected()  ||  !db2.isConnected())) {
            connected = false;
        }

        if (connected) {
            long t = System.currentTimeMillis();
            for (int i = 0; i < 10000; ++i) {
                try {
                    update();
                } catch (SQLException e) {
                    throw new RuntimeException(e);
                }
            }
            System.out.println("" + (System.currentTimeMillis() - t));
        }
    }

    private void update() throws SQLException {
        if( firstPass ) {
            createTable(db1);
            createTable(db2);

            initPst();

            firstPass = false;
        }

        String name = "my_tag_name";
        String val = System.currentTimeMillis() + "";

        // 1
//        try (java.sql.Statement st = db1.getConnection().createStatement()) {
//            String sql = String.format("delete from %s where name='%s'", TABLE, name);
//            st.executeUpdate(sql);
//
//            sql = String.format("insert into %s (name, val) values ('%s', '%s')", TABLE, name, val);
//            st.executeUpdate(sql);
//        } catch (Exception e) {
//            printError(e, "Request error 1");
//        }

        // 1 with prepared statement
//        pstDelete.setString(1, name);
//        pstDelete.executeUpdate();
//
//        pstInsert.setString(1, name);
//        pstInsert.setString(2, val);
//        pstInsert.executeUpdate();

        // 2
        try (java.sql.Statement st = db1.getConnection().createStatement()) {
            String sql = String.format("update or insert into %s (name, val) values ('%s', '%s')", TABLE, name, val);
            st.executeUpdate(sql);
        } catch (Exception e) {
            printError(e, "Request error 2");
        }
    }

    private void initPst() throws SQLException {
        String sql = String.format("delete from %s where name=?", TABLE);
        pstDelete = db1.getConnection().prepareStatement(sql);

        sql = String.format("insert into %s (name, val) values (?, ?)", TABLE);
        pstInsert = db1.getConnection().prepareStatement(sql);
    }

    private void createTable(Database db) {

        try(Statement st = db.getConnection().createStatement()) {
            if (!db.hasTable(st, "", TABLE)) {
                String sql = String.format(
                        "create table %s (" +
                                "name varchar(128) not null primary key, " +
                                "val varchar(128) not null)",
                        TABLE);
                st.executeUpdate(sql);
            }
            db.commit();
        } catch (SQLException e) {
            printError(e, "");
        }
    }


}
