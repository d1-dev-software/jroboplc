package promauto.jroboplc.plugin.script.samples;
/*
plugin.script:
  module.mixscr:
    actions:
      - java:
          class:         RadugaMixer
          id:            16_3
          dvu:           14_5
          wbunk:         W14_5
 */

import promauto.jroboplc.core.api.Flags;
import promauto.jroboplc.core.api.Tag;
import promauto.jroboplc.core.tags.Ref;
import promauto.jroboplc.plugin.script.ScriptJava;

public class RadugaMixerScript extends ScriptJava {

    public static final String UVRD = "UVRD";

    protected static final int STATE_IDLE              = 0;
    protected static final int STATE_START_OPEN        = 1;
    protected static final int STATE_START_OPEN_DELAY  = 2;
    protected static final int STATE_START_MIXER       = 3;
    protected static final int STATE_START_CLOSE       = 4;
    protected static final int STATE_LOAD              = 5;
    protected static final int STATE_EXPOSITION        = 6;
    protected static final int STATE_UNLOAD_READY      = 7;
    protected static final int STATE_UNLOAD_OPEN       = 8;
    protected static final int STATE_UNLOAD_DELAY      = 9;
    protected static final int STATE_UNLOAD_DVU        = 10;
    protected static final int STATE_CLEAN_START_MIXER = 11;
    protected static final int STATE_CLEAN_DELAY       = 12;
    protected static final int STATE_FINISH            = 13;

    protected static final String[] STATES = new String[14];
    static {
        STATES[STATE_IDLE             ] = "Готов к старту цикла";
        STATES[STATE_START_OPEN       ] = "Открытие задвижки перед стартом смесителя";
        STATES[STATE_START_OPEN_DELAY ] = "Задержка перед включением смесителя";
        STATES[STATE_START_MIXER      ] = "Включение смесителя";
        STATES[STATE_START_CLOSE      ] = "Закрытие задвижки перед загрузкой";
        STATES[STATE_LOAD             ] = "Ожидание завершения загрузки";
        STATES[STATE_EXPOSITION       ] = "Смешивание";
        STATES[STATE_UNLOAD_READY     ] = "Ожидание готовности к разгрузке";
        STATES[STATE_UNLOAD_OPEN      ] = "Открытие задвижки";
        STATES[STATE_UNLOAD_DELAY     ] = "Разгрузка";
        STATES[STATE_UNLOAD_DVU       ] = "Ожидание отключения ДВУ";
        STATES[STATE_CLEAN_START_MIXER] = "Включение миксера для очистки";
        STATES[STATE_CLEAN_DELAY      ] = "Очистка";
        STATES[STATE_FINISH           ] = "Цикл завершен";
    }



    // roboplant refs
    Ref refZdvClosedA;
    Ref refZdvOpenedA;
    Ref refZdvClosedB;
    Ref refZdvOpenedB;
    Ref refZdvClosedC;
    Ref refZdvOpenedC;
    Ref refMixerMasterOut;
    Ref refDvuOutput;
    Ref refCurWeight;

    // inputs
    Ref inpStart;
    Ref inpLoaded;
    Ref inpCanUnload;

    // outputs ro
    Ref tagMixer;
    Ref tagState;
    Ref tagBusy;
    Ref tagUnloading;
    Ref tagStateMsg;
    Ref tagTimer;
    Ref tagOverrun;
    Tag varTimerOverrun;
    Ref tagClosed;
    Ref tagOpened;

    Ref tagZdvEnA;
    Ref tagZdvOpA;
    Ref tagZdvEnB;
    Ref tagZdvOpB;
    Ref tagZdvEnC;
    Ref tagZdvOpC;

    // outputs rw
    Ref tagReset;
    Ref tagDoClose;
    Ref tagDoOpen;

    // outputs rw autosave
    Ref tagTimeExp;
    Ref tagTimeOverrun;
    Ref tagTimeUnload;
    Ref tagTimeClean;
    Ref tagWeightMin;


    private int stateOpen;
    private int stateClose;




    @Override
    public boolean load() {
//        String id = getId();
        String mix = getArg("mix", "");
        String wbunk = getArg("wbunk", "");
        String dvu = getArg("dvu", "");

        // roboplant refs
        String zdva = "ZDVA_" + mix;
        refZdvClosedA = createRef(UVRD, zdva + "A_Closed");
        refZdvOpenedA = createRef(UVRD, zdva + "A_Opened");
        refZdvClosedB = createRef(UVRD, zdva + "B_Closed");
        refZdvOpenedB = createRef(UVRD, zdva + "B_Opened");
        refZdvClosedC = createRef(UVRD, zdva + "C_Closed");
        refZdvOpenedC = createRef(UVRD, zdva + "C_Opened");
        refMixerMasterOut = createRef(UVRD, "MCHB_" + mix + "_MasterOut");
        refDvuOutput = createRef(UVRD, "MDTA_" + dvu + "_DVU_Sost");

        refCurWeight = wbunk.isEmpty()? null: createRef(wbunk, "CurWeight");

        inpStart       = createTag("Start"      , 0);
        inpLoaded      = createTag("Loaded"     , 0);
        inpCanUnload   = createTag("CanUnload"  , 0);
        tagMixer       = createTag("Mixer"      , 0);
        tagState       = createTag("State"      , 0);
        tagBusy        = createTag("Busy"       , 0);
        tagUnloading   = createTag("Unloading"  , 0);
        tagStateMsg    = createTag("StateMsg"   , "");
        tagTimer       = createTag("Timer"      , 0);
        tagOverrun     = createTag("Overrun"    , 0);
        tagClosed      = createTag("Closed"     , 0);
        tagOpened      = createTag("Opened"     , 0);
        tagZdvEnA      = createTag("ZdvEnA"     , 0);
        tagZdvOpA      = createTag("ZdvOpA"     , 0);
        tagZdvEnB      = createTag("ZdvEnB"     , 0);
        tagZdvOpB      = createTag("ZdvOpB"     , 0);
        tagZdvEnC      = createTag("ZdvEnC"     , 0);
        tagZdvOpC      = createTag("ZdvOpC"     , 0);
        tagReset       = createTag("Reset"      , 0);
        tagDoClose     = createTag("DoClose"    , 0);
        tagDoOpen      = createTag("DoOpen"     , 0);
        tagTimeExp     = createTag("TimeExp"    , 0, Flags.AUTOSAVE);
        tagTimeOverrun = createTag("TimeOverrun", 0, Flags.AUTOSAVE);
        tagTimeUnload  = createTag("TimeUnload" , 0, Flags.AUTOSAVE);
        tagTimeClean   = createTag("TimeClean"  , 0, Flags.AUTOSAVE);
        tagWeightMin   = createTag("WeightMin"  , 0, Flags.AUTOSAVE);

        varTimerOverrun = createVar("TimerOverrun", 0);

        return true;
    }

    @Override
    public void execute() {
        boolean cmdClose = false;
        boolean cmdOpen = false;
        int state = tagState.getInt();

        if( tagReset.getBool() ) {
            state = STATE_IDLE;
            tagReset.setInt(0);
        }

        if( state == STATE_IDLE  &&  inpStart.getBool() ) {
//            if( !refMixerMasterOut.getBool()  &&  !tagOpened.getBool() ) {
//                state = STATE_START_OPEN;
//            } else {
//                state = STATE_START_MIXER;
//            }
            state = STATE_START_MIXER;
        }

        if( state == STATE_START_OPEN  &&  tagOpened.getBool() ) {
            tagTimer.setInt( tagTimeUnload.getInt() );
            state = STATE_START_OPEN_DELAY;
        }

        if( state == STATE_START_OPEN_DELAY  &&  timer(tagTimer) ) {
            state = STATE_START_MIXER;
        }

        if( state == STATE_START_MIXER  &&  refMixerMasterOut.getBool() ) {
            state = STATE_START_CLOSE;
        }

        if( state == STATE_START_CLOSE  &&  tagClosed.getBool() ) {
            state = STATE_LOAD;
        }

        if( state == STATE_LOAD  &&  inpLoaded.getBool() ) {
            tagTimer.setInt( tagTimeExp.getInt() );
            state = STATE_EXPOSITION;
        }

        if( state == STATE_EXPOSITION  &&  timer(tagTimer) ) {
            state = STATE_UNLOAD_READY;
            varTimerOverrun.setInt( tagTimeOverrun.getInt() );
        }

        if( state == STATE_UNLOAD_READY ) {
            if( refCurWeight == null  ||  refCurWeight.getLong() <= tagWeightMin.getInt() ) {
                if (inpCanUnload.getBool()) {
                    state = STATE_UNLOAD_OPEN;
                }
            }
        }

        if( state == STATE_UNLOAD_OPEN  &&  tagOpened.getBool() ) {
            state = STATE_UNLOAD_DELAY;
            tagTimer.setInt( tagTimeUnload.getInt() );
        }

        if( state == STATE_UNLOAD_DELAY  &&  timer(tagTimer) ) {
            state = STATE_UNLOAD_DVU;
        }

        if( state == STATE_UNLOAD_DVU ) {
            if( refDvuOutput.getInt() == 0 ) {
                if( refMixerMasterOut.getBool() ) {
                    state = STATE_FINISH;
                } else {
                    state = STATE_CLEAN_START_MIXER;
                    varTimerOverrun.setInt(0);
                }
            }
        }

        if( state == STATE_CLEAN_START_MIXER  &&  refMixerMasterOut.getBool() ) {
            state = STATE_CLEAN_DELAY;
            tagTimer.setInt( tagTimeClean.getInt() );
        }

        if( state == STATE_CLEAN_DELAY  &&  timer(tagTimer) ) {
            state = STATE_FINISH;
        }

        if( state == STATE_FINISH ) {
            state = STATE_IDLE;
        }

        tagBusy.setBool( state >= STATE_EXPOSITION  &&  state <= STATE_FINISH );
        tagUnloading.setBool( state >= STATE_UNLOAD_OPEN  &&  state <= STATE_UNLOAD_DELAY );
        tagMixer.setBool( state >= STATE_START_MIXER  &&  state <= STATE_CLEAN_DELAY );
        tagOverrun.setBool( state >= STATE_UNLOAD_READY  &&  state <= STATE_UNLOAD_DVU  &&  timerOverrun() );

        cmdClose = state >= STATE_START_CLOSE  &&  state <= STATE_UNLOAD_READY;
        cmdOpen = state != STATE_IDLE  &&  !cmdClose;


        // process zdva
        // manual control
        if (tagDoClose.getBool()) {
            cmdClose = true;
            cmdOpen = false;
        } else if (tagDoOpen.getBool()) {
            cmdClose = false;
            cmdOpen = true;
        }

        if( cmdClose ) {
            stateOpen = 0;
            if( stateClose == 0 )
                stateClose++;

            if( stateClose == 1  &&  refZdvClosedA.getBool() )
                stateClose++;

            if( stateClose == 2  &&  refZdvClosedB.getBool() )
                stateClose++;

            if( stateClose == 3  &&  refZdvClosedC.getBool() )
                stateClose++;
        } else
        if( cmdOpen ) {
            stateClose = 0;
            if( stateOpen == 0 )
                stateOpen++;

            if( stateOpen == 1  &&  refZdvOpenedC.getBool() )
                stateOpen++;

            if( stateOpen == 2  &&  refZdvOpenedB.getBool() )
                stateOpen++;

            if( stateOpen == 3  &&  refZdvOpenedA.getBool() )
                stateOpen++;
        } else {
            stateClose = 0;
            stateOpen = 0;
        }

        tagZdvEnA.setBool( stateClose >= 1  ||  stateOpen >= 3);
        tagZdvOpA.setBool( stateOpen >= 3);

        tagZdvEnB.setBool( stateClose >= 2  ||  stateOpen >= 2);
        tagZdvOpB.setBool( stateOpen >= 2);

        tagZdvEnC.setBool( stateClose >= 3  ||  stateOpen >= 1);
        tagZdvOpC.setBool( stateOpen >= 1);

        tagClosed.setBool( refZdvClosedA.getBool()  &&  refZdvClosedB.getBool()  &&  refZdvClosedC.getBool() );
        tagOpened.setBool( refZdvOpenedA.getBool()  &&  refZdvOpenedB.getBool()  &&  refZdvOpenedC.getBool() );

        tagState.setInt(state);
        tagStateMsg.setString(STATES[state]);
//        System.out.println(STATES[state]);
    }

    private boolean timerOverrun() {
        if( varTimerOverrun.getInt() > 0 ) {
            varTimerOverrun.setInt( varTimerOverrun.getInt() - 1 );
            return false;
        }
        return true;
    }

    private boolean timer(Ref timer) {
        if( timer.getInt() > 0 ) {
            timer.setInt( timer.getInt() - 1 );
            return false;
        }
        return true;
    }

}
