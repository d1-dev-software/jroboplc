package promauto.jroboplc.plugin.script.samples;


/*
plugin.script:
  module.scrpg:
    actions:
      - java:
          class:         Otgrel
          id:            otgrel1
          filename:      otgrel1
          bruttoMin:     300000
          module:        WZD1

 */


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import promauto.jroboplc.core.tags.Ref;
import promauto.jroboplc.plugin.script.ScriptJava;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static java.nio.file.StandardOpenOption.APPEND;
import static java.nio.file.StandardOpenOption.CREATE;

public class TenzomRailwayExportCsvScript extends ScriptJava {
    private final Logger logger = LoggerFactory.getLogger(TenzomRailwayExportCsvScript.class);
    static private DateTimeFormatter dtFormatter1 = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    static private DateTimeFormatter dtFormatter2 = DateTimeFormatter.ofPattern("yyyyMMdd");

    // refs
    Ref refBrutto;
    Ref refNetto;
    Ref refErrorFlag;

    // tags
    Ref loading;
    Ref dtBeg;
    Ref brutto;
    Ref netto;
    Ref tara;

    String filename;
    int bruttoMin;
    int nettoMin;

    int version;

    @Override
    public boolean load() {
        version = getArg("version", 1);
        bruttoMin = getArg("bruttoMin", 0);
        nettoMin = getArg("nettoMin", 0);
        filename = getArg("filename", "otgrel");

        String mod = getArg("module", "");
        refBrutto    = createRef(mod, "Brutto");
        refNetto     = createRef(mod, "Netto");
        refErrorFlag = createRef(mod, "SYSTEM.ErrorFlag");

        loading   = createTag("Loading", false);
        dtBeg     = createTag("DtBeg", "");
        brutto    = createTag("Brutto", 0);
        netto     = createTag("Netto", 0);
        tara      = createTag("Tara", 0);

        return true;
    }

    @Override
    public void execute() {
        if( !isValid() ) {
            dtBeg.setString("not linked");
            return;
        }

        if( version == 1) {
            if (loading.getBool()) {
                if (refBrutto.getInt() <= 0) {
                    if (brutto.getInt() > bruttoMin)
                        if (!saveRecordToFile())
                            return;
                    loading.setBool(false);
                    brutto.setInt(0);
                    netto.setInt(0);
                    dtBeg.setString("");
                } else {
                    if (refBrutto.getInt() > brutto.getInt()) {
                        brutto.setInt(refBrutto.getInt());
                        netto.setInt(refNetto.getInt());
                    }
                    if (refNetto.getInt() > netto.getInt()) {
                        netto.setInt(refNetto.getInt());
                    }
                }
            } else {
                if (refBrutto.getInt() > 0) {
                    loading.setBool(true);
                    brutto.setInt(refBrutto.getInt());
                    netto.setInt(refNetto.getInt());
                    dtBeg.setString(LocalDateTime.now().format(dtFormatter1));
                }
            }
        }

        if( version == 2) {
            if (loading.getBool()) {
                if (netto.getInt() > 0  &&  refNetto.getInt() <= 0) {
                    if (netto.getInt() > nettoMin)
                        if (!saveRecordToFile())
                            return;
                    loading.setBool(false);
                    netto.setInt(0);
                    dtBeg.setString("");
                } else {
                    if (refNetto.getInt() > netto.getInt()) {
                        brutto.setInt(refBrutto.getInt());
                        netto.setInt(refNetto.getInt());
                    }
                    if (refBrutto.getInt() > brutto.getInt()) {
                        brutto.setInt(refBrutto.getInt());
                    }
                }

                if (refBrutto.getInt() <= 0  &&  refNetto.getInt() <= 0) {
                    loading.setBool(false);
                    brutto.setInt(0);
                    netto.setInt(0);
                    dtBeg.setString("");
                }
            } else {
                if (refBrutto.getInt() >= bruttoMin  &&  refNetto.getInt() == 0) {
                    loading.setBool(true);
                    netto.setInt(0);
                    dtBeg.setString(LocalDateTime.now().format(dtFormatter1));
                }
                brutto.setInt(refBrutto.getInt());
            }
        }

        tara.setInt(brutto.getInt() - netto.getInt());
    }

    private boolean saveRecordToFile() {
        String dtEnd = LocalDateTime.now().format(dtFormatter1);
        String s = String.format("%s,%s,%s,%s",
                dtBeg.getString(),
                dtEnd,
                brutto.getString(),
                netto.getString()
        ) + System.lineSeparator();

        try {
            Files.write(
                    Paths.get(filename + "_" + LocalDateTime.now().format(dtFormatter2) + ".csv"),
                    s.getBytes(),
                    CREATE, APPEND
            );
        } catch (IOException e) {
            return false;
        }
        return true;
    }

}
