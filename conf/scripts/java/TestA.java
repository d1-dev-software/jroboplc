Tag var1; // internal variable, visible for this script only

Ref tag1; // tag, accessible for other modules

Ref ref1; // references to a tag of any of the modules
Ref ref2;


@Override
public boolean load() {
    var1 = createVar("var1", 33);

    int prm1 = getArg("prm1", 0);
    tag1 = createTag("tag1", prm1);

    ref1 = createRef("MyVar3");

    ref2 = createRef(getModuleName(), "MyVar2");


//    printInfo("Id=" + getId() + ", prm2=" + getArg("prm2","") );
    return true;
}


@Override
public void execute(){

    var1.setInt( var1.getInt() + 1);

    tag1.setInt( var1.getInt() * 10);

    ref1.setInt( ref1.getInt() + 1);

    ref2.setDouble( ref2.getDouble() + 0.1d);

}

