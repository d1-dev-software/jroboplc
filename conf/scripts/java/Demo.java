//Tag var1; // internal variable, visible for this script only

//Ref tag1; // tag, accessible for other modules

//Ref ref1; // references to any tags of any modules
//Ref ref2;

Ref tagSost;
Ref tagBlok;
Ref tagChannelOut;
Ref tagChannelIn;

@Override
public boolean load() {
    tagSost = createTag("MCHB_" + getId() + "_Sost", 0);
    tagSost = createTag("MCHB_" + getId() + "_Blok", 0, Flags.AUTOSAVE);
    tagChannelOut = createTag("MCHB_" + getId() + "_Channel1", 0, Flags.AUTOSAVE);
    tagChannelIn = createTag("MCHB_" + getId() + "_Channel2", 0, Flags.AUTOSAVE);

//    var1 = createVar("var1", 33);
//    int prm1 = getArg("prm1", 0);

//    ref1 = createRef("MyVar3");
//    ref2 = createRef(getModuleName(), "MyVar2");
//    printInfo("Id=" + getId() + ", prm2=" + getArg("prm2","") );
    return true;
}


@Override
public void execute(){

//    var1.setInt( var1.getInt() + 1);

//    tag1.setInt( var1.getInt() * 10);

//    ref1.setInt( ref1.getInt() + 1);

//    ref2.setDouble( ref2.getDouble() + 0.1d);

}