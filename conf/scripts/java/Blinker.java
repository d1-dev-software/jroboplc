
Ref inp00;
Ref out00;

@Override
public boolean load() {

    inp00 = createRef("modbus", "inp00");
    out00 = createRef("modbus", "out00");

    return true;
}


@Override
public void execute(){

    if( inp00.getBool() )
      out00.setBool( !out00.getBool() );


}

