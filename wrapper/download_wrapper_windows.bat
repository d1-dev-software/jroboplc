@echo off
set version=3.5.60

set dirname=wrapper-windows-x86-32-%version%
set filename=%dirname%.zip

del %filename%
utils\wget.exe --no-check-certificate http://prom-auto.ru/download/dist/wrapper/%version%/%filename%

del /f /s /q %dirname%

utils\unzip.exe %filename%



