Для запуска приложения jroboplc в качестве сервиса используется два варианта с применением сторонних программ:
  - Tanuki Java Service Wrapper (рекоммендуемый для, всех систем, кроме Windows с Java x64).
  - YAJSW (альтернативный, для всех систем)



Tanuki Java Service Wrapper
===========================

1. Скачивание и установка версии по умолчанию скриптом:
   - Для linux: запустить download_wrapper_linux.sh
   - Для windows: запустить download_wrapper_windows.bat

2. Скачивание и установка произвольной версии вручную:
   - Скачать дистрибутив https://wrapper.tanukisoftware.com/doc/english/download.jsp#stable (выбрать Delta Pack, Community).
   - Распаковать в jroboplc/wrapper и переименовать распакованную папку в wrapper-delta-pack, убрав из имени номер версии.
   - Обновить скрипты запуска, если скаченная версия отличается от той, что используется по умолчанию.


  Обновление скриптов запуска в Linux:
    - Скопировать файл jroboplc/wrapper/wrapper-delta-pack/src/bin/sh.script.in в jroboplc/ctl.lin с переименованием в jroboplc.
    - Отредактировать скопированный файл, установив следующие параметры:
       APP_NAME="jroboplc"
       APP_LONG_NAME="jroboplc"
       WRAPPER_CMD="../wrapper/wrapper-delta-pack/bin/wrapper"
       WRAPPER_CONF="../wrapper/wrapper-conf/wrapper.lin.conf"
       PIDDIR="/run"
    - Для изменения имени сервиса (если на одной машине планируется несколько инсталляций) использовать параметры APP_NAME и APP_LONG_NAME.


  Обновление скриптов запуска в Windows:
    - Скопировать файл jroboplc/wrapper/wrapper-delta-pack/src/bin/AppCommand.bat.in в jroboplc/ctl.win с переименованием в jroboplc.bat.
    - Отредактировать скопированный файл, установив следующие параметры:
       set _WRAPPER_BASE=wrapper
       set _WRAPPER_DIR=..\wrapper\wrapper-delta-pack\bin
       set _WRAPPER_CONF=..\..\..\wrapper\wrapper-conf\wrapper.win.conf
    - Для изменения имени сервиса (если на одной машине планируется несколько инсталляций) использовать параметры в файле jroboplc/wrapper/wrapper-conf/wrapper.win.conf:
       wrapper.name=jroboplc
       wrapper.displayname=jroboplc
       wrapper.description=jroboplc

       # Mode in which the service is installed.  AUTO_START, DELAY_START or DEMAND_START
       wrapper.ntservice.starttype=DEMAND_START


3. Управление:
  - Для windows - в jroboplc/ctl.win находятся необходимые скрипты для управления сервисом
  - Для linux - jroboplc/ctl.lin



YAJSW (Yet Another Java Service Wrapper)
========================================
Скачивание и установка:
1. Скачать дистрибутив последней версии https://sourceforge.net/projects/yajsw/files/yajsw/
2. Распаковать в эту папку и переименовать распакованную папку в jroboplc/wrapper/yajsw
3. Обновить скрипт запуска (см. далее)
 

Обновление скриптов запуска:
1. Скопировать файл jroboplc/wrapper/yajsw-conf/wrapper.conf в jroboplc/wrapper/yajsw/conf. Дальнейшие изменения в конфигурации производить в jroboplc/wrapper/yajsw/conf/wrapper.conf
2. Для изменения имени сервиса (если на одной машине планируется несколько инсталляций) использовать параметр appname
3. Для изменения способа запуска сервиса использовать параметр starttype
   Внимание! В Windows автоматический старт не работает. Для автоматического запуска использовать режим DEMAND_START и создать ярлык в папке Автозагрузка на файл bat/startService.bat

Для работы yajsw требуется, чтобы команда java выполнялась из любого каталога на любом уровне для всех пользователей. Вариант осуществления:
 - в windows: добавить javaBin в path системы
 - в linux:   добавить ссылки на java и javac в /usr/bin


Управление:
1. В каталогах jroboplc/wrapper/yajsw/bat находятся необходимые скрипты для управления сервисом в windows
2. Для linux - jroboplc/wrapper/yajsw/bin
